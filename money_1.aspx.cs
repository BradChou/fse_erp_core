﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;

public partial class money_1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;
            
            #region
            Suppliers.Items.Insert(0, new ListItem("001-零擔", "001"));
            Suppliers.Items.Insert(1, new ListItem("002-流通", "002"));
            #endregion

            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            }

            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if (Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }
            lbdate.Text = dates.Text + "~" + datee.Text;

            if (Request.QueryString["close_type"] != null)
            {
                dlclose_type.SelectedValue = Request.QueryString["close_type"];
            }
            dlclose_type_SelectedIndexChanged(null, null);
            if (Request.QueryString["closeday"] != null)
            {
                dlCloseDay.SelectedValue = Request.QueryString["closeday"];
            }

            readdata();

            string sScript = string.Empty;
            switch (dlclose_type.SelectedValue)
            {
                case "1":
                    sScript = " $('._close').show(); " +
                               " $('._daterange').html('請款期間');  ";
                    break;

                case "2":
                    sScript = " $('._close').hide(); " +
                              " $('._daterange').html('發送期間'); ";
                    break;
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

        }
    }

    private void readdata()
    {


        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsReceivablebyHCT";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null , "");
                if (dt != null && dt.Rows.Count > 0 && _info != null)
                {
                    DataRow row = dt.NewRow();
                    row["code_name"] = "總計";
                    row["supplier_fee"] = _info.subtotal;
                    row["remote_fee"] = _info.remote_fee;
                    dt.Rows.Add(row);
                }

                New_List_01.DataSource = dt;
                New_List_01.DataBind();

                if (_info != null)
                {
                    subtotal_01.Text = _info.subtotal.ToString("N0");
                    tax_01.Text = _info.tax.ToString("N0");
                    total_01.Text = _info.total.ToString("N0");
                }
            }
        }


        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsReceivable";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        New_List.DataSource = dt;
                        New_List.DataBind();
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }



    }


    protected void Suppliers_TextChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text  != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);
                wherestr = " and customer_code = @customer_code";
            }
            cmd.CommandText = string.Format(@"select customer_code,customer_name  from tbCustomers where 0=0 {0} ", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    lbSuppliers.Text = dt.Rows[0]["customer_name"].ToString();
                }
            }
        }   
        #endregion
    }

    protected void dlclose_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        if (dlclose_type.SelectedValue == "1")
        {
            string wherestr = "";
            SqlCommand cmd2 = new SqlCommand();
            if (Suppliers.SelectedValue != "")
            {
                cmd2.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                wherestr = " and supplier_code = @supplier_code";
            }
            cmd2.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd2.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd2.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from tcDeliveryRequests with(nolock)   
                                                   where 0=0 and checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
            dlCloseDay.DataSource = dbAdapter.getDataTable(cmd2);
            dlCloseDay.DataValueField = "checkout_close_date";
            dlCloseDay.DataTextField = "checkout_close_date";
            dlCloseDay.DataBind();
            dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));

        }
        #endregion
    }

    protected void btQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {

        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        querystring += "&close_type=" + dlclose_type.SelectedValue.ToString();

        if (dlCloseDay.SelectedValue != "")
        {
            querystring += "&closeday=" + dlCloseDay.SelectedValue.ToString();
        }

        Response.Redirect(ResolveUrl("~/money_1.aspx?search=yes" + querystring));
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        if (Suppliers.SelectedValue == "" )
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇客戶代號');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue );  //客代
                cmd.Parameters.AddWithValue("@close_type", "2");                          //結帳程序類別(1:自動 2:手動)
                //dbAdapter.execNonQuery(cmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {           
            strErr = "acc_post" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }
        return;
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = null;
        string Title = Suppliers.SelectedItem.Text;       

        Title = "峻富物流股份有限公司";        
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsReceivablebyHCT";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            dt = dbAdapter.getDataTable(cmd);
            _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null, "");
        }


        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("竹運應收帳款");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 18;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 10;
            sheet1.Column(8).Width = 10;
            sheet1.Column(9).Width = 10;
            sheet1.Column(10).Width = 10;
            sheet1.Column(11).Width = 10;
            sheet1.Column(12).Width = 10;
            sheet1.Column(13).Width = 10;
            sheet1.Column(14).Width = 30;
            sheet1.Column(15).Width = 10;
            sheet1.Column(16).Width = 10;
            sheet1.Column(17).Width = 10;
            sheet1.Column(18).Width = 10;
            sheet1.Column(19).Width = 10;
            sheet1.Column(20).Width = 15;



            sheet1.Cells[1, 1, 1, 20].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 20].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 20].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 20].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 20].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 20].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 20].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 20].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 20].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
            sheet1.Cells[3, 2, 3, 2].Value = "客戶代號：" + Suppliers.SelectedItem.Text;
            sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            sheet1.Cells[4, 1, 4, 20].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 20].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 20].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 1, 4, 20].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 20].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 20].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 2].Value = "特殊狀況說明";
            sheet1.Cells[4, 3].Value = "發送日期";
            sheet1.Cells[4, 4].Value = "配送日期";
            sheet1.Cells[4, 5].Value = "明細貨號";
            sheet1.Cells[4, 6].Value = "發送站客代";
            sheet1.Cells[4, 7].Value = "發送站";           
            sheet1.Cells[4, 8].Value = "寄件人";
            sheet1.Cells[4, 9].Value = "收件人";
            sheet1.Cells[4, 10].Value = "配送縣市";
            sheet1.Cells[4, 11].Value = "配送區域";
            sheet1.Cells[4, 12].Value = "件數";
            sheet1.Cells[4, 13].Value = "板數";
            sheet1.Cells[4, 14].Value = "收貨人住址";
            sheet1.Cells[4, 15].Value = "區配商代碼";
            sheet1.Cells[4, 16].Value = "區配廠商";
            sheet1.Cells[4, 17].Value = "特殊配送說明";
            sheet1.Cells[4, 18].Value = "B段總計";
            sheet1.Cells[4, 19].Value = "偏遠區費用";
            sheet1.Cells[4, 20].Value = "JUNFU費用總計(不打折)";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();                
                sheet1.Cells[i + 5, 3].Value = dt.Rows[i]["print_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["print_date"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 4].Value = dt.Rows[i]["supplier_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["supplier_date"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5,5].Value = dt.Rows[i]["check_number"].ToString();
                sheet1.Cells[i + 5, 6].Value = dt.Rows[i]["customer_code"].ToString();
                sheet1.Cells[i + 5, 7].Value = dt.Rows[i]["發送站"].ToString();
                sheet1.Cells[i + 5, 8].Value = dt.Rows[i]["send_contact"].ToString();
                sheet1.Cells[i + 5, 9].Value = dt.Rows[i]["receive_contact"].ToString();
                sheet1.Cells[i + 5, 10].Value = dt.Rows[i]["receive_city"].ToString();
                sheet1.Cells[i + 5, 11].Value = dt.Rows[i]["receive_area"].ToString();
                sheet1.Cells[i + 5, 12].Value = Convert.ToInt32(dt.Rows[i]["pieces"]);
                sheet1.Cells[i + 5, 12].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 13].Value = Convert.ToInt32(dt.Rows[i]["plates"]);
                sheet1.Cells[i + 5, 13].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 14].Value = dt.Rows[i]["receive_city"].ToString()+ dt.Rows[i]["receive_area"].ToString()+ dt.Rows[i]["receive_address"].ToString();
                sheet1.Cells[i + 5, 15].Value = dt.Rows[i]["area_arrive_code"].ToString();
                sheet1.Cells[i + 5, 16].Value = dt.Rows[i]["區配廠商"].ToString();
                sheet1.Cells[i + 5, 17].Value = dt.Rows[i]["code_name"].ToString();
                sheet1.Cells[i + 5, 18].Value = Convert.ToInt32(dt.Rows[i]["supplier_fee"]);
                sheet1.Cells[i + 5, 18].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 19].Value = Convert.ToInt32(dt.Rows[i]["remote_fee"]);
                sheet1.Cells[i + 5, 19].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 19].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 20].Value = Convert.ToInt32(dt.Rows[i]["貨件費用"]);
                sheet1.Cells[i + 5, 20].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 20].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    sheet1.Cells[i + 6, 17].Value = "小計";               
                    sheet1.Cells[i + 6, 18].Value = _info.subtotal;
                    sheet1.Cells[i + 6, 18].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 6, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 6, 1, i + 6, 20].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 6, 1, i + 6, 20].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 20].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                    sheet1.Cells[i + 7, 17].Value = "5%營業稅";
                    sheet1.Cells[i + 7, 18].Value = _info.tax;
                    sheet1.Cells[i + 7, 18].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 7, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 7, 1, i + 7, 20].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 7, 1, i + 7, 20].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 20].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[i + 8,17].Value = "應收帳款";
                    sheet1.Cells[i + 8, 18].Value = _info.total;
                    sheet1.Cells[i + 8, 18].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 8, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 8, 1, i + 8, 20].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 8, 1, i + 8, 20].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 20].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                }
            }



            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("竹運應收帳款明細表.xlsx", Encoding.UTF8) );
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}