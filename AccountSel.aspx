﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccountSel.aspx.cs" Inherits="AccountSel" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇人員</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">
                                查詢條件：ＭＥＭ
                                
                                <div class="margin-right-15 templatemo-inline-block">
                                    <asp:RadioButtonList ID="rbmanager_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" CssClass="radio radio-success">
                                    </asp:RadioButtonList>
                                </div>
                                <label>帳號：</label>
                                <asp:TextBox ID="account_code" runat="server" class="form-control"></asp:TextBox>
                                <label>姓名：</label>
                                <asp:TextBox ID="user_name" runat="server" class="form-control"></asp:TextBox>
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                            </div>
                        </div>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>選取</th>
                                <th>管理單位</th>
                                <th>公司別</th>
                                <th>職稱</th>
                                <th>人員</th>
                                <th>帳號</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="選取">
                                            <asp:HiddenField ID="Hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_id").ToString())%>' />
                                            <asp:HiddenField ID="Hid_mangtype" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.manager_type").ToString())%>' />
                                            <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                        </td>
                                        <td data-th="管理單位">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.manager_unit").ToString())%>
                                        </td>
                                        <td data-th="公司別">
                                            <asp:Literal ID="LiCus_name" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_shortname").ToString())%>' ></asp:Literal> </td>
                                        <td data-th="職稱"><asp:Literal ID="Lijob_title" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.job_title").ToString())%>' ></asp:Literal> </td>
                                        <td data-th="人員"><asp:Literal ID="LiName" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%>' ></asp:Literal></td>
                                        <td data-th="帳號"><asp:Literal ID="LiAccount" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_code").ToString())%>' ></asp:Literal> </td>
                                       
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="6" style="text-align: center">查無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
