﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public partial class fileupload_Rorder : System.Web.UI.Page
{
    public string _check_number
    {
        get { return ViewState["_check_number"].ToString(); }
        set { ViewState["_check_number"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
        }
        _check_number = Request.QueryString["check_number"] != null ? Request.QueryString["check_number"].ToString() : "";
        file01.AllowMultiple = true;
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;
      
        string fileurl = "";
        string imagess = "";
        string fileExtension = "";
        bool fileok = false;
        string tErrStr = "";
        int R_count = 1;

        string strFileName = "";
        string strimagess = "";
        if (file01.HasFile)
        {
            foreach (HttpPostedFile file in file01.PostedFiles)
            { 
             Int32 fileSize = file.ContentLength;
                //if (fileSize > 20971520) // 20* 1024 * 1024
                //{
                //    tErrStr = "檔案 [ " + file.FileName + " ] 限傳20MB   請重新選擇檔案";
                //}
                if (fileSize > 1048576) {
                    tErrStr += "檔案 [ " + file.FileName + " ] 限傳1MB   請重新選擇檔案";
                }

            fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower().ToString();
            //取得檔案格式
            string[] allowedExtensions = { ".gif", ".jpg", ".jpe*", ".png", ".tif", ".bmp", ".pdf" };
            //定義允許的檔案格式
            //逐一檢查允許的格式中是否有上傳的格式
            for (int i = 0; i <= allowedExtensions.Length - 1; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileok = true;
                }
            }

            if (!fileok)
            {
                tErrStr = "上傳檔案格式錯誤，請上傳gif、jpg、jpe*、png、tif、bmp、pdf格式";
            }
            }
        }
        else
        {
            if (hidf_id.Value == "")
            {
                tErrStr = "請選擇上傳檔案";
            }
        }

        if (tErrStr == "")
        {
            try
            {
                if ((file01.HasFile))
                {
                    foreach (HttpPostedFile file in file01.PostedFiles)
                    {
                        string filepath = "~/files/receipt/";
                        string guid = Guid.NewGuid().ToString();
                        imagess = guid + fileExtension;

                        if (fileExtension == ".pdf")
                        {
                            file.SaveAs(Server.MapPath(filepath + imagess));
                        }
                        else
                        {
                            byte[] FileBytes;
                            MemoryStream ms = new MemoryStream();
                            file.InputStream.CopyTo(ms);
                            FileBytes = ms.GetBuffer();
                            System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(FileBytes));

                            if (!makethumbnail(image, Server.MapPath(filepath + imagess), 500, 500, "w", "jpg"))
                            {
                                tErrStr = "上傳失敗：";
                            }
                        }
                        strFileName += file.FileName + ";";
                        strimagess += imagess + ";";
                        R_count++;
                    }

                }

            }
            catch (Exception ex)
            {
                tErrStr = ex.Message;
            }
        }



        if (tErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + tErrStr + "');", true);
        }
        else
        {

            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["filename"] + "').val('" + file01.FileName + "');" +
            //                                                                    "self.parent.$('#" + Request.QueryString["fileid"] + "').val('" + imagess + "');" +                                                                            
            //                                                                    "parent.$.fancybox.close();</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["filename"] + "').val('" + strFileName + "');" +
                                                                               "self.parent.$('#" + Request.QueryString["fileid"] + "').val('" + strimagess + "');" +
                                                                               "parent.$.fancybox.close();</script>", false);
        }
    }

    /// <summary>
    /// 压缩图片并存储
    /// </summary>
    /// <param name="originalimagepath">源图路径（物理路径）</param>
    /// <param name="thumbnailpath">压缩面存储路径（物理路径）</param>
    /// <param name="width">压缩图宽度</param>
    /// <param name="height">压缩图高度</param>
    /// <param name="mode">生成压缩图的方式</param> 
    /// <param name="type">保存压缩图类型</param> 
    public bool makethumbnail(System.Drawing.Image originalimagepath, string thumbnailpath, int width, int height, string mode, string type)
    {

        System.Drawing.Image originalimage = originalimagepath;
        bool isture = true; ;
        int towidth = width;
        int toheight = height;
        int x = 0;
        int y = 0;
        int ow = originalimage.Width;
        int oh = originalimage.Height;
        switch (mode)
        {
            case "hw"://指定高宽缩放（可能变形） 
                break;
            case "w"://指定宽，高按比例 
                toheight = originalimage.Height * width / originalimage.Width;
                break;
            case "h"://指定高，宽按比例
                towidth = originalimage.Width * height / originalimage.Height;
                break;
            case "cut"://指定高宽裁减（不变形） 
                if ((double)originalimage.Width / (double)originalimage.Height > (double)towidth / (double)toheight)
                {
                    oh = originalimage.Height;
                    ow = originalimage.Height * towidth / toheight;
                    y = 0;
                    x = (originalimage.Width - ow) / 2;
                }
                else
                {
                    ow = originalimage.Width;
                    oh = originalimage.Width * height / towidth;
                    x = 0;
                    y = (originalimage.Height - oh) / 2;
                }
                break;
            case "db"://等比缩放（不变形，如果高大按高，宽大按宽缩放） 
                if ((double)originalimage.Width / (double)towidth < (double)originalimage.Height / (double)toheight)
                {
                    toheight = height;
                    towidth = originalimage.Width * height / originalimage.Height;
                }
                else
                {
                    towidth = width;
                    toheight = originalimage.Height * width / originalimage.Width;
                }
                break;
            default:
                break;
        }
        //新建一个bmp图片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);
        //新建一个画板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
        //设置高质量插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
        //设置高质量,低速度呈现平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //清空画布并以透明背景色填充
        g.Clear(System.Drawing.Color.Transparent);
        //在指定位置并且按指定大小绘制原图片的指定部分
        g.DrawImage(originalimage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
        new System.Drawing.Rectangle(x, y, ow, oh),
        System.Drawing.GraphicsUnit.Pixel);
        try
        {
            //保存缩略图
            if (type == "jpg")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            if (type == "bmp")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            if (type == "gif")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Gif);
            }
            if (type == "png")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Png);
            }

            //using (System.IO.MemoryStream oMS = new System.IO.MemoryStream())
            //{
            //    //將oTarImg儲存（指定）到記憶體串流中
            //    bitmap.Save(oMS, System.Drawing.Imaging.ImageFormat.Jpeg);
            //    //將串流整個讀到陣列中，寫入某個路徑中的某個檔案裡
            //    using (System.IO.FileStream oFS = System.IO.File.Open(thumbnailpath, System.IO.FileMode.OpenOrCreate))
            //    { oFS.Write(oMS.ToArray(), 0, oMS.ToArray().Length); }
            //}

        }
        catch (System.Exception e)
        {
            isture = false;
        }
        finally
        {
            originalimage.Dispose();
            bitmap.Dispose();
            g.Dispose();
        }
        return isture;
    }




}