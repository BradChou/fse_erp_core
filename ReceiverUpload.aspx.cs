﻿using NPOI.HSSF.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReceiverUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String str_Receiver = "";
            if (Request.QueryString["type"] != null) str_Receiver = Request.QueryString["type"].ToString();
            if (str_Receiver != "receiver")
            {
                MsgShow("資訊有誤，請重新確認!");
                return;
            }

        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;

        Boolean IsChk = true;//檢查是否OK
        lbMsg.Items.Clear();

        #region 檢查

        if (!file01.HasFile)
        {
            MsgShow("請選擇要上傳的檔案!");
            IsChk = false;
            return;
        }
        else
        {
            int fileMaxSize = 5242880;
            string[] allowedExtensions = { ".xls", ".xlsx" }; //定義允許的檔案格式
            string fileExtension = "";

            if (file01.PostedFile.ContentLength > fileMaxSize)
            {
                MsgShow("檔案 [ " + file01.PostedFile.FileName + " ] 限傳5MB, 請重新選擇檔案!");
                IsChk = false;
                return;
            }

            //驗證上傳檔案的格式
            fileExtension = System.IO.Path.GetExtension(file01.FileName).ToLower().ToString();
            if (!allowedExtensions.Contains(fileExtension))
            {
                MsgShow("上傳檔案格式錯誤，請上傳excel格式!");
                IsChk = false;
                return;
            }
        }

        #endregion

        if (IsChk)
        {
            try
            {
                //開始匯入
                Boolean IsExcelToDB = ExcelToDB(file01);
                if (!IsExcelToDB)
                {
                    lbMsg.Items.Add("匯入失敗，請重新確認! ");
                }
                else
                {
                    //匯入完成後，客戶頁重新整理，運價表才會秀出最近的資料
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('匯入完成!');parent.$.fancybox.close(); parent.location.reload(true);", true);
                }

            }
            catch (Exception ex)
            {
                MsgShow("發生錯誤，請聯絡系統人員! " + ex.Message);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>parent.$.fancybox.close();</script>", false);
                return;
            }
        }
    }

    /// <summary>回傳訊息至使用者介面</summary>
    /// <param name="str_msg">訊息內容</param>
    /// <param name="other_strJS">其他自訂JS語法，可不填</param>
    public void MsgShow(string str_msg, string other_strJS = "")
    {
        if (!string.IsNullOrEmpty(str_msg))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + str_msg + "');" + other_strJS, true);
        }
    }


    /// <summary>ExcelToDB </summary>
    /// <param name="this_file">Excel file</param>
    /// <returns></returns>
    public Boolean ExcelToDB(FileUpload this_file)
    {
        Boolean IsUpdateToDB = true;
        try
        {
            lbMsg.Items.Clear();
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
          
            #region 自訂
            if (this_file.HasFile)
            {
                using (ExcelPackage workbook = new ExcelPackage(this_file.FileContent))
                {
                    foreach (ExcelWorksheet u_sheet in workbook.Workbook.Worksheets)
                    {
                        string ttSelectSheetName = u_sheet.Name;
                        int row = u_sheet.Dimension.End.Row;
                        int col = u_sheet.Dimension.End.Column;
                        object[,] valueArray = u_sheet.Cells.GetValue<object[,]>();
                        string[] Row_title = new string[col];
                        //foreach (object[,] _this in u_sheet.Cells.GetValue<object[,]>())

                        if (row < 2)
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName + "】欄位資訊有誤，請重新確認!");
                            continue;
                        }
                        else
                        {
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
                        }

                        //標題列
                        for (int i = 0; i < col; i++) Row_title[i] = valueArray[0, i].ToString();

                        #region 定義 foreach sheet

                        //Table:tbReceiver (收貨人匯入)
                        Boolean IsSheetOk = true;//工作表驗證
                        string tablename = "tbReceiver";
                        string str_user = Session["account_code"] == null ? "" : Session["account_code"].ToString();//建立人員
                        string strRow = string.Empty;

                        string receiver_code = string.Empty;//收貨人代碼 
                        string customer_code = string.Empty;//客戶代碼 
                        string receiver_name = string.Empty;//客戶    
                        string tel = string.Empty;
                        string tel_ext = string.Empty;
                        string tel2 = string.Empty;
                        string address_city = string.Empty;
                        string address_area = string.Empty;
                        string address_road = string.Empty;
                        string memo = string.Empty;
                        string strInsCol = string.Empty;
                        string strInsVal = string.Empty;
                        string strUpdate = string.Empty;

                        //寫入DB的資訊
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        #endregion

                        //每一列 
                        for (int j = 1; j < row; j++)
                        {
                            #region 初始化                            
                            receiver_code = "";
                            customer_code = "";
                            receiver_name = "";
                            tel = "";
                            tel_ext = "";
                            tel2 = "";
                            address_city = "";
                            address_area = "";
                            address_road = "";
                            memo = "";
                            strRow = (j + 1).ToString();
                            #endregion

                            #region 驗證&取值 IsSheetOk

                            for (int k = 0; k < col; k++)
                            {
                                Boolean IsChkOk = true;
                                switch (Row_title[k])
                                {
                                    case "客戶代碼":
                                        if (valueArray[j, k] != null) customer_code = valueArray[j, k].ToString();
                                        if (!IsRegex(4, customer_code, false))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "收貨人代碼":
                                        if (valueArray[j, k] != null) receiver_code = valueArray[j, k].ToString();
                                        if (!IsRegex(4, receiver_code, false))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;

                                    case "收貨人":
                                        if (valueArray[j, k] != null) receiver_name = valueArray[j, k].ToString();
                                        if (!IsRegex(4, receiver_name, false))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;

                                    case "電話1":
                                        if (valueArray[j, k] != null) tel = valueArray[j, k].ToString();
                                        if (!IsRegex(1, tel, false))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "分機1":
                                        if (valueArray[j, k] != null) tel_ext = valueArray[j, k].ToString();
                                        if (!IsRegex(3, tel_ext))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "電話2":
                                        if (valueArray[j, k] != null) tel2 = valueArray[j, k].ToString();
                                        if (!IsRegex(1, tel2))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "地址-縣市":
                                        if (valueArray[j, k] != null) address_city = valueArray[j, k].ToString();
                                        if (!IsRegex(4, address_city))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "地址-鄉鎮區":
                                        if (valueArray[j, k] != null) address_area = valueArray[j, k].ToString();
                                        if (!IsRegex(4, address_area))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "地址-內容":
                                        if (valueArray[j, k] != null) address_road = valueArray[j, k].ToString();
                                        if (!IsRegex(4, address_road))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "備註":
                                        if (valueArray[j, k] != null) memo = valueArray[j, k].ToString();
                                        break;
                                }

                                if (!IsChkOk)
                                {
                                    lbMsg.Items.Add("請確認【" + Row_title[k] + "】 第" + strRow + "列是否正確!");
                                }

                            }

                            //地址(縣市、鄉鎮區) 驗證
                            if (!string.IsNullOrEmpty(address_area) && !string.IsNullOrEmpty(address_city))
                            {
                                string str_address = GetValueStrFromSQL(@"SELECT 1 FROM tbPostCity city With(Nolock)
                                                                       LEFT JOIN tbPostCityArea area With(Nolock) ON city.city = area.city
                                                                           WHERE city.city=N'" + address_city + "' AND area.area =N'" + address_area + "' ");
                                if (string.IsNullOrEmpty(str_address))
                                {
                                    lbMsg.Items.Add("請確認【地址(縣市、鄉鎮區)】 第" + strRow + "列是否正確!");
                                    IsSheetOk = false;
                                }
                            }


                            #endregion

                            #region 組新增
                            if (IsSheetOk)
                            {
                                strInsCol = "customer_code, receiver_code, receiver_name, tel, tel_ext, tel2, address_city, address_area,address_road, memo, cuser, cdate ";
                                strInsVal = "N\'" + customer_code + "\', " +
                                            "N\'" + receiver_code + "\', " +
                                            "N\'" + receiver_name + "\', " +
                                            "N\'" + tel + "\', " +
                                            "N\'" + tel_ext + "\', " +
                                            "N\'" + tel2 + "\', " +
                                            "N\'" + address_city + "\', " +
                                            "N\'" + address_area + "\', " +
                                            "N\'" + address_road + "\', " +
                                            "N\'" + memo + "\', " +
                                            "N\'" + str_user + "\', " +
                                            " GETDATE() ";
                                //customer_code, receiver_code, receiver_name, tel, tel_ext, tel2, address_city, address_area,address_road, memo, uuser, udate 
                                strUpdate = "UPDATE rv ";
                                strUpdate += "SET receiver_name = N\'" + receiver_name + "\' ," +
                                                " tel = N\'" + tel + "\' ," +
                                                " tel_ext = N\'" + tel_ext + "\' ," +
                                                " tel2=N\'" + tel2 + "\' ," +
                                                " address_city=N\'" + address_city + "\' ," +
                                                " address_area=N\'" + address_area + "\' ," +
                                                " address_road=N\'" + address_road + "\' ," +
                                                " memo=N\'" + memo + "\' ," +
                                                " uuser=N\'" + str_user + "\' ," +
                                                " udate = GETDATE() ";
                                strUpdate += "FROM " + tablename + " rv With(Nolock) WHERE customer_code=N'" + customer_code + "' AND receiver_code=N'" + receiver_code + "' ;";
                                // sb_temp_ins.Append("INSERT INTO " + tablename + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");

                                sb_temp_ins.Append(@"BEGIN
                                                    IF (SELECT COUNT(1) FROM tbReceiver With(Nolock) WHERE customer_code=N'" + customer_code + "' AND receiver_code=N'" + receiver_code + @"') = 0
                                                       INSERT INTO " + tablename + "(" + strInsCol + ") VALUES(" + strInsVal + "); " + @"
                                                    ELSE
                                                       " + strUpdate + @"
                                                 END; ");


                                //每100筆組成一字串
                                if (j % 100 == 0 || j == (row - 1))
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }
                            }
                            #endregion

                        }

                        if (IsSheetOk)
                        {
                            //新增自訂
                            String strSQL;
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                }
                            }
                        }
                        else
                        {
                            IsUpdateToDB = false;
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                    }

                }
                lbMsg.Items.Add("匯入完畢");
            }
            else
            {
                IsUpdateToDB = false;
            }
            #endregion

        }
        catch (Exception ex)
        {
            IsUpdateToDB = false;
        }
        return IsUpdateToDB;
    }


    /// <summary>正則表達驗資料 </summary>
    /// <param name="type">驗證類型</param>
    /// <param name="str_data">欲驗證的資料</param>
    /// <param name="CanNull">是否可為空值 true:可為空(預設) false:不可為空</param>
    /// <returns></returns>
    protected Boolean IsRegex(int type, String str_data, Boolean CanNull = true)
    {
        Boolean IsOK = true;
        string str_reg = "";
        str_data = str_data.Trim();
        switch (type)
        {
            case 1://電話、手機
                str_reg = @"^\(?(\d{2,4})\)?[\s\-]?(\d{7,10})$";
                break;
            case 2://手機
                str_reg = @"^\(?(\d{4})\)?[\s\-]?(\d{6,10})$";
                break;
            case 3://分機
                str_reg = @"^[0-9]+(\d{1,5})?$";
                break;
            case 4://字串不為空
                if (string.IsNullOrEmpty(str_data)) IsOK = false;
                break;
            case 5://數字
                if (!IsNumeric(str_data)) IsOK = false;
                break;
            default:
                IsOK = false;//沒指明要驗什麼
                break;
        }
        if (IsOK && str_reg.Length > 0)
        {
            Regex rgx = new Regex(str_reg);

            if (!CanNull)
            {
                //不為空
                IsOK = IsRegex(4, str_data) && rgx.IsMatch(str_data) ? true : false;
            }
            else
            {
                //可空
                IsOK = !IsRegex(4, str_data) || rgx.IsMatch(str_data) ? true : false;
            }
        }

        return IsOK;
    }

    protected String GetValueStrFromSQL(string strSQL)
    {
        String result = "";
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0][0].ToString();
                    }
                }
            }

        }

        return result;
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }
}