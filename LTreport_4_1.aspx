﻿<%@Page Title="" Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreport_4_1.aspx.cs" Inherits="LTreport_4_1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />   
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });
              $('.fancybox').fancybox({
                'width': '100%',
               

            });
            $("#fancybox").css({ 'width': '500px', 'height': '500px' });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
          $.fancybox.update();
        $(document).on("click", "._new_addr", function () {
            var _url = "LT_transport_2_edit.aspx?r_type=1&request_id=" + $(this).parent("td").attr("_the_id");
            $.fancybox({
                'width': '60%',
                'height': '95%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': _url
            });
        });

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>
    
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

       
        
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">EDI 訂單管理</h2>
                <div class="form-group form-inline">
                     <label>站所：</label>
                     <span  class="text-danger"><asp:Label ID="lbSuppliers" runat="server"></asp:Label></span>
                    <label> 總筆數：</label>
                    <span  class="text-danger"><asp:Label ID="totle" runat="server"></asp:Label></span>
                </div>
            <div class="templatemo-login-form" >        
                 <div class="form-group form-inline">
                       <label>發送類別：</label>
                      <asp:RadioButton id="DeliveryType1" Text="" GroupName="rdDeliveryType" RepeatDirection="horizontal"  Checked="true" runat="server" /><asp:Label ID="Label1" runat="server" Text="正物流"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton id="DeliveryType2" Text=""  RepeatDirection="horizontal" GroupName="rdDeliveryType" runat="server"/><asp:Label ID="Label2" runat="server" Text="逆物流"></asp:Label><br />
                      
                   <%-- <asp:RadioButtonList ID="rdDeliveryType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="D" Text="正物流" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="R" Text="逆物流"></asp:ListItem>
                    </asp:RadioButtonList>--%>
                      </div>
                <div class="form-group form-inline">
                   
                    <label>發送日期：</label>
                    <asp:TextBox ID="date1" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                    <asp:TextBox ID="date2" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    <label>部門：</label>
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack ="true"  OnSelectedIndexChanged ="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                    <label>選擇客代：</label>
                    <asp:DropDownList ID="second_code" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                    <asp:DropDownList ID="issendcontact" runat="server" CssClass="form-control chosen-select" >
                    </asp:DropDownList>
                     <label>貨號：</label>
                    <asp:TextBox ID ="strCheck_number" runat="server"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnQry" CssClass="btn btn-primary btn_view" runat="server" Text="查　詢" OnClick="btnQry_Click"  />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="匯　出" OnClick="btExport_Click"  />
                </div>
            </div>
            <div style="overflow: auto; height: 550px; width: 100%">
                <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">發送日期</th>
                    <th style="white-space: nowrap;">配送日期</th>
                    <th style="white-space: nowrap;">明細貨號</th>
                    <th style="white-space: nowrap;">訂單編號</th>
                    <th style="white-space: nowrap;">發送站</th>
                    <th style="white-space: nowrap;">寄件人</th>
                    <th style="white-space: nowrap;">收件人</th>
                    <th style="white-space: nowrap;">收貨人電話號碼<br /></th>
                    <th style="white-space: nowrap;">配送<br>縣市</th>
                    <th style="white-space: nowrap;">配送<br>區域</th>
                    <th style="white-space: nowrap;">重量</th>
                    <th style="white-space: nowrap;">件數</th>
                    <th style="white-space: nowrap;">收件人地址</th>
                    <th style="white-space: nowrap;">站所<br>代碼</th>
                    <th style="white-space: nowrap;">站所<br>名稱</th>
                    <th style="white-space: nowrap;">傳票<br>區分</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">備註</th>
                    <th style="white-space: nowrap;">接駁碼</th>
                    <th style="white-space: nowrap;">司機碼</th>
                    <th style="white-space: nowrap;">堆疊區</th>
                    <th style="white-space: nowrap;">功　　能</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server"  OnItemDataBound="New_List_ItemDataBound">
                    <ItemTemplate>
                        <tr class ="paginate">
                            <td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;"  data-th="發送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送日期").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="明細貨號">
                                <a href="LT_trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%></a>
                            </td>
                            <td style="white-space: nowrap;" data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單編號").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收貨人電話號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收貨人電話號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送縣市別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送縣市別").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送區域").ToString())%></td>
                            <td style="white-space: nowrap;" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.重量").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人地址").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="站所代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所代碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="站所名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所名稱").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.傳票區分").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.備註").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="接駁碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.接駁碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="司機碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.司機碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="堆疊區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.堆疊區").ToString())%></td>
                            <td class="td_btn" _the_id='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>'>
                    <%#Server.HtmlDecode((DataBinder.Eval(Container, "DataItem.cancel_date").ToString()) != ""  ?                     
"<a href='LT_transport_2_edit.aspx?r_type=2&request_id="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString()) +"&scan="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.logNum").ToString()) + "' class='fancybox fancybox.iframe btn btn-warning ' id='updclick'>已銷單</a>" :
"<a href='LT_transport_2_edit.aspx?r_type=0&request_id="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString()) +"&scan="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.logNum").ToString()) + "' class='fancybox fancybox.iframe btn btn-primary _d_btn ' id='updclick'>修改</a>")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="41" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            </div>
            
             共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              <div id="page-nav" class="page"></div>
                      
        </div>
    </div>
</asp:Content>

