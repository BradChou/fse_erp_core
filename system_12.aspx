﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterSystem.master"  CodeFile="system_12.aspx.cs" Inherits="system_12" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script src="js/jquery-1.11.2.min.js"></script>


    <script>
        //限制textbox的最大最小值

        function minmax(value, min, max) {
            value = value.replace(/[^\d]/g, '');
            if (value == "") {
                return value;
            }
            else {
                if (parseInt(value) < min || isNaN(parseInt(value)))
                    return min;
                else if (parseInt(value) > max)
                    return max;
                else return value;
            }
        }        
    </script>

    <style type="text/css">
        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .td_th {
            text-align: center;
        }
         .td_up {
           width: 180px;
        }
        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        #ContentPlaceHolder1_adjust:before {
            content: "* ";
            color: red;
            margin-left: 79.285px;
        }

        .important:before {
            margin-left: -10px;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }
        .auto-style1 {
            height: 114px;
            width: 26%;
        }

        .title1 {
            margin-left: -80px;
        }
        .auto-style2 {
            width: 23%;
        }
        .auto-style3 {
            width: 26%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h2 class="margin-bottom-10">
        <asp:Label ID="lbl_title" Text="預購袋區間" runat="server"></asp:Label></h2>



    <asp:Panel ID="pan_view" runat="server">
        <div class="div_view">
            <!-- 查詢 -->
            <div class="form-group form-inline">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 70%;">
                            <label>查詢條件：</label>
                            <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control" ToolTip="所屬站別"></asp:DropDownList>
                            <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 客戶代號 或 名稱"></asp:TextBox>
                            <%--<asp:TextBox ID="tb_searchNum" runat="server" class="form-control" placeholder="ex: 貨號"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control" ToolTip="是否生效"></asp:DropDownList>
                            <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                        </td>
                        <td style="width: 30%; float: right;">
                            <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                        </td>
                    </tr>
                </table>
            </div>

            <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr>
                    <th class="td_th td_no">No</th>
                    <th class="td_th">站別</th>
                    <th class="td_th td_name">客戶</th>
                    <th class="td_th td_yn">總件數</th>
                    <th class="td_th td_yn">剩餘件數</th>
                    <th class="td_th td_yn">是否生效</th>
                    <th class="td_th td_up">更新人員</th>
                    <th class="td_th td_up">更新日期</th>
                    <th class="td_th td_edit">功　　能</th>
                </tr>
                <asp:Repeater ID="list_customer" runat="server" OnItemCommand="list_customer_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                            <td class="td_data" data-th="站別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <td class="td_data td_name text-left  " data-th="客戶"><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString() + "　" + DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                            <td class="td_data td_no" data-th="總件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_count").ToString())%></td>
                            <td class="td_data td_no" data-th="剩餘件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rest_count").ToString())%></td>
                            <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.is_active").ToString())%></td>
                            <td class="td_data td_up" data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.update_user").ToString())%></td>
                            <td class="td_data td_up" data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.update_date" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            <td class="td_data td_edit" data-th="編輯">
                                <asp:HiddenField ID="emp_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>' />
                                <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" Visible ='<%# _Auth.insert_auth %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (list_customer.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="11" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style=" text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>
            <!--內容-end -->
        </div>
    </asp:Panel>

    <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
        <hr />
        <table class="tb_edit">
            <tr class="form-group">
                <th class="auto-style3">
                    <label class="_tip_important">站別</label>
                </th>
                <td class="_edit_data form-inline ">
                    <asp:DropDownList ID="ddl_staion" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="所屬站別" OnSelectedIndexChanged="Customer_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_customer_code" runat="server" ControlToValidate="ddl_staion" ForeColor="Red" ValidationGroup="validate">請選擇所屬站別</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="form-group">
                <th class="auto-style3">
                    <label class="_tip_important">客代</label>
                </th>
                <td class="_edit_data form-inline ">
                    <asp:DropDownList ID="ddl_Customer" runat="server" CssClass="form-control" ToolTip="所屬客代"></asp:DropDownList>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_Customer" ForeColor="Red" ValidationGroup="validate">請選擇所屬客代</asp:RequiredFieldValidator>
                </td>
            </tr>
               <tr>
                <th class="auto-style1">
                    <label class="_tip_important">調整數量</label>
                </th>
                <td>
                  <label class="title1" style="display: inline-block ">總共數量</label>&nbsp;<asp:Label ID="showNum" runat="server" Text=""></asp:Label>
                    &nbsp;&nbsp;<label class="title1" style="display: inline-block ">剩餘數量</label>&nbsp;<asp:Label ID="rest" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<asp:TextBox ID="num" runat="server" onkeyup="value = this.value.replace(/[^0-9\-]/g, '')" Width="65px" AutoPostBack="True" OnTextChanged="num_TextChanged" ></asp:TextBox>&nbsp;&nbsp;<asp:Button ID="Button1" Text="+" runat="server" class="btn btn-primary" OnClick="Button1_Plus_Click"/>&nbsp;<asp:Button ID="Button2" runat="server" Text="-" class="btn btn-primary" OnClick="Button2_Minus_Click" />
                         <asp:HiddenField ID="original_total_count" Value="" runat="server" /><asp:HiddenField ID="original_used_count" Value="" runat="server" /><asp:HiddenField ID="original_rest_count" Value="" runat="server" />
                </td>
            </tr>
             <tr>
                <th class="auto-style3">
                    <asp:label class="_tip_important" ID="adjust" runat="server">調整原因</asp:label>
                </th>
                <td class="_edit_data form-inline">
                    <asp:TextBox ID="TextBox1" runat="server" ValidationGroup="validate" Width="482px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <th class="auto-style3">
                    <label class="_tip_important">是否生效</label>
                </th>
                <td class="_edit_data form-inline">
                    <asp:RadioButtonList ID="rb_Active" runat="server" ToolTip="請設定是否生效!" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                    <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="history" runat="server" CssClass="div_edit" Visible="False">
                 <label class="important">歷史調整紀錄</label>
                 <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr>
                    <th class="td_th td_yn">總數量</th>
                    <th class="td_th td_yn">剩餘數量</th>
                    <th class="td_th td_yn">是否生效</th>
                    <th class="td_th td_yn">修改原因</th>
                    <th class="td_th td_yn">更新人員</th>
                    <th class="td_th td_yn">更新日期</th>
                </tr>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="td_data td_no" data-th="總數量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_count").ToString())%></td>
                            <td class="td_data td_no" data-th="剩餘數量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rest_number").ToString())%></td>
                            <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.is_active_chinese").ToString())%></td>
                            <td class="td_data td_no" data-th="修改原因"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.adjustment_reason").ToString())%></td>
                            <td class="td_data td_up" data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.update_user").ToString())%></td>
                            <td class="td_data td_up" data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.update_date" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (Repeater1.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="6" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
   </asp:Panel>
</asp:Content>
