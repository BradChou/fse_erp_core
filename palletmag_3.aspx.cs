﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using BarcodeLib;
using System.Drawing;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;   //-- XSSF 用來產生Excel 2007檔案（.xlsx）
using System.IO;
using BObject.Bobjects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class palletmag_3 : System.Web.UI.Page
{

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }

    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }
    }

    public DataTable DT_col
    {
        get { return (DataTable)ViewState["DT_col"]; }
        set { ViewState["DT_col"] = value; }

    }


    public DataTable DT_cus
    {
        get { return (DataTable)ViewState["DT_cus"]; }
        set { ViewState["DT_cus"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
                ssMaster_code = "0000000";
            #region 
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";
                if (ssManager_type == "4" || ssManager_type == "5")
                {
                    if (ssMaster_code.Substring(3, 4) == "0000")
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code.Substring(0, 3));
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code);
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                }

                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = 0 {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , master_code + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 order by  master_code", wherestr);
                second_code.DataSource = dbAdapter.getDataTable(cmd);
                second_code.DataValueField = "master_code";
                second_code.DataTextField = "showname";
                second_code.DataBind();
            }
            second_code.SelectedValue = ssMaster_code;

            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                second_code.Enabled = true;
                second_code.Items.Insert(0, new ListItem("全部", ""));
            }

            #endregion

            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            #region 客代
            using (SqlCommand cmd_cus = new SqlCommand())
            {
                cmd_cus.CommandText = string.Format(@"select SUBSTRING(customer_code,1,8) 'customer_code' , customer_shortname   from tbCustomers with(nolock) ");

                using (DataTable dt = dbAdapter.getDataTable(cmd_cus))
                {
                    DT_cus = dt;
                }
            }
            #endregion

            #region 區配      
            string id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "";
            SqlCommand cmd_sup = new SqlCommand();
            cmd_sup.Parameters.AddWithValue("@supplier_code", id);
            cmd_sup.CommandText = @"select supplier_code ,supplier_name , CASE WHEN  CHARINDEX(supplier_code,@supplier_code) > 0 THEN 1 ELSE 0 END 'chk'
                                     from tbSuppliers with(nolock)
                                     where active_flag = 1 and ISNULL(supplier_code ,'') <> ''  ";
            rp_sup.DataSource = dbAdapter.getDataTable(cmd_sup);
            rp_sup.DataBind();

            #endregion

            sdate.Text = DateTime.Now.ToString("2018/01/01");
            readdata();
        }
    }

    private void readdata()
    {
        string wherestr = "";
        #region 欠板統計數
        using (SqlCommand cmd = new SqlCommand())
        {
            wherestr = "";
            cmd.Parameters.AddWithValue("@date", sdate.Text);
            cmd.Parameters.AddWithValue("@customer_code", second_code.SelectedValue != "" ? second_code.SelectedValue : (object)DBNull.Value);
            cmd.CommandText = string.Format(@"select SUBSTRING(A.customer_code,1,8) 'customer_code', A.area_arrive_code , sum(A.plates) 'plates',  
                                                        sum(CASE WHEN P.pallet_id is not null then A.plates else 0 end)  'rtn_plates',
                                                        sum(A.plates- CASE WHEN P.pallet_id is not null then A.plates else 0 end) 'owe_plates'
                                                        from tcDeliveryRequests A With(Nolock)
                                                        left join ttPlletLog P with(Nolock) on A.request_id = P.request_id
                                                        where 1=1  and A.pallet_recycling_flag = 1 and A.cancel_date  is NULL  and A.plates >0 
                                                        and A.print_date >= @date
                                                        and (@customer_code is null or A.customer_code like + @customer_code + '%')
                                                        group by SUBSTRING(A.customer_code,1,8) , A.area_arrive_code
                                                        order by SUBSTRING(A.customer_code,1,8) , A.area_arrive_code", wherestr);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                DT = dt;
            }
        }
        #endregion

        #region
        SqlCommand objCommand = new SqlCommand();
        wherestr = "";
        string id = "";

        foreach (RepeaterItem item in this.rp_sup.Items)
        {
            CheckBox chk = item.FindControl("chkcus") as CheckBox;
            if (chk.Checked)
            {
                string supplier_code = (item.FindControl("Hid_id") as HiddenField).Value;
                id += supplier_code + ",";                
            }
        }
        if (id != "") id = id.Substring(0, id.Length - 1);       
        string[] aryPara = id.Split(',');
        string strParas = "";
        if (id != "")
        {
            //wherestr += dbAdapter.genWhereCommIn(ref objCommand, "A.area_arrive_code", id.Split(','));
            for (int i = 0; i < aryPara.Length; i++)
            {
                strParas += "'''+@" + "area_arrive_code" + i.ToString() + "+''',";   //Sql Command
                objCommand.Parameters.AddWithValue("@" + "area_arrive_code" + i.ToString(), aryPara[i]);
            }

            if (strParas.Length > 0)
            {
                strParas = strParas.Substring(0, strParas.Length - 1);
                wherestr += string.Format(" and {0} in ({1})", "A.area_arrive_code", strParas);   //Sql Command
            }
        }

        objCommand.Parameters.AddWithValue("@customer_code", second_code.SelectedValue != "" ? second_code.SelectedValue : (object)DBNull.Value);
        objCommand.Parameters.AddWithValue("@date", sdate.Text);
        objCommand.CommandText = string.Format(@"declare @pvtheaders nvarchar(max) ,@sql nvarchar(max) --@sumsql     動態組出樞紐資料行相加
                                                        with  cus AS
                                                        (
                                                        select  SUBSTRING ( C.customer_code,1,8) as  customer_code, C.customer_shortname,
                                                         ROW_NUMBER() OVER (PARTITION BY SUBSTRING ( C.customer_code,1,8) ORDER BY SUBSTRING ( C.customer_code,1,8) DESC) AS rn
                                                        from tcDeliveryRequests A With(Nolock)
                                                        left join ttPlletLog P with(Nolock) on A.request_id = P.request_id
                                                        inner join tbCustomers C with(nolock) on  A.customer_code  = C.customer_code
                                                        where 1=1  and A.pallet_recycling_flag = 1 and A.cancel_date  is NULL  and A.plates >0 
                                                        and A.print_date >= @date
                                                        and (@customer_code is null or A.customer_code like + @customer_code + '%')
                                                        )

                                                        SELECT   @pvtheaders=ISNULL(@pvtheaders,'')+'['+ customer_code + '-應還' +'],' +'['+ customer_code + '-實還' +'],' +'['+ customer_code + '-尚欠' +'],'
                                                        from cus
                                                        where rn = 1
                                                        GROUP BY customer_code
                                                        set @pvtheaders = left(@pvtheaders, len(@pvtheaders) - 1)
                                                        --select @pvtheaders


                                                        --動態PIVOT
                                                        set @sql = 'SELECT * FROM  (
                                                        select SUBSTRING (A.customer_code ,1,8)  ''customer_code'' , A.area_arrive_code ,S.supplier_name,  sum(A.plates) ''plates''
                                                        from tcDeliveryRequests A With(Nolock)
                                                        left join ttPlletLog P with(Nolock) on A.request_id = P.request_id
                                                        left join tbSuppliers S with(nolock) on A.area_arrive_code  = S.supplier_code 
                                                        where 1=1  and A.pallet_recycling_flag = 1 and A.cancel_date  is NULL  and A.plates >0 
                                                        and A.print_date >= '''+@date+'''
                                                        and ('''+ ISNULL(@customer_code,'') +''' ='''' or A.customer_code like '''+ ISNULL(@customer_code,'') + '%'')
                                                        {0}
                                                        group by SUBSTRING (A.customer_code ,1,8) , A.area_arrive_code, S.supplier_name
                                                        ) p
                                                        pivot
                                                        (SUM(plates)
                                                        for customer_code in ('+ @pvtheaders +')) AS pvt'
                                                        --select @sql
                                                        exec sp_executesql @sql", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            //if (dt.Rows.Count > 0)
            //{
            //    DataList1.RepeatColumns = dt.Columns.Count;
            //}
            //DataList1.DataSource = dt;
            //DataList1.DataBind();

            if (dt.Rows.Count > 0)
            {
                for (int i = 2; i <= dt.Columns.Count - 1; i++)
                {
                    //BoundField AddColumn = new BoundField();
                    //AddColumn.DataField = dt.Columns[i].ColumnName;
                    //AddColumn.HeaderText = dt.Columns[i].ColumnName;
                    //GridView1.Columns.Add(AddColumn);

                    TemplateField tfield = new TemplateField();
                    tfield.HeaderText = dt.Columns[i].ColumnName.Split('-')[0] + "<br>" + DT_cus.Select("customer_code='" + dt.Columns[i].ColumnName.Split('-')[0] + "'")[0]["customer_shortname"].ToString();                    
                    //tfield.HeaderStyle.Width = 50;
                    GridView1.Columns.Add(tfield);


                }
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();
            DT_col = dt;


        }

        #endregion

    }




    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        string id = "";

        foreach (RepeaterItem item in this.rp_sup.Items)
        {
            CheckBox chk = item.FindControl("chkcus") as CheckBox;
            if (chk.Checked)
            {
                string supplier_code = (item.FindControl("Hid_id") as HiddenField).Value;
                id += supplier_code + ",";
            }
        }
        if (id != "") id = id.Substring(0, id.Length - 1);
        querystring += "&second_code=" + second_code.SelectedValue + "&id=" + id;
        Response.Redirect(ResolveUrl("~/palletmag_3.aspx?search=yes" + querystring));
    }


    //protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item ||e.Item.ItemType == ListItemType.AlternatingItem)
    //    {
    //        DataRowView drv = (DataRowView)(e.Item.DataItem);

    //        string status = drv.Row["Status"].ToString();
    //        if (status == "Available")
    //            e.Item.BackColor = System.Drawing.Color.LightGreen;
    //        if (status == "Assigned")
    //            e.Item.BackColor = System.Drawing.Color.LightSteelBlue;
    //        if (status == "BR")
    //            e.Item.BackColor = System.Drawing.Color.LightSalmon;
    //    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            int idx = 1;
            for (int i = 1; i <= e.Row.Cells.Count - 1; i++)
            {
                if (idx == 1)
                {
                    e.Row.Cells[i].ColumnSpan = 3;
                    e.Row.Cells[i + 1].Visible = false;
                    e.Row.Cells[i + 2].Visible = false;
                }
                if (idx < 3)
                {
                    idx += 1;
                }
                else
                {
                    idx = 1;
                }
            }

        }

        //GridViewRow headrow = GridView1.HeaderRow;
        //headrow.Cells[1].ColumnSpan = 3;



    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow tRow = e.Row;
        if (tRow.RowType == DataControlRowType.Footer)
        {
            GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            TableHeaderCell cell = new TableHeaderCell();
            cell.Text = "區配";
            row.Controls.Add(cell);
            int idx = 1;
            for (int i = 1; i <= e.Row.Cells.Count - 1; i++)
            {
                cell = new TableHeaderCell();
                switch (idx)
                {
                    case 1:
                        cell.Text = "應還";
                        break;
                    case 2:
                        cell.Text = "實還";
                        break;
                    case 3:
                        cell.Text = "尚欠";
                        break;
                }
                row.Controls.Add(cell);
                if (idx < 3)
                {
                    idx += 1;
                }
                else
                {
                    idx = 1;
                }
            }

            row.BackColor = ColorTranslator.FromHtml("#3AC0F2");
            GridView1.HeaderRow.Parent.Controls.AddAt(1, row);
        }
        else
        if (tRow.RowType == DataControlRowType.DataRow)
        {
            DataRowView dr = (DataRowView)e.Row.DataItem;

            for (int i = 1; i <= GridView1.Columns.Count - 1; i++)
            {
                string[] customer = dr.DataView.Table.Columns[i + 1].ToString().Split('-');
                string area_arrive_code = DataBinder.Eval(e.Row.DataItem, "area_arrive_code").ToString();
                DataRow[] rows = DT.Select("customer_code='" + customer[0] + "' and area_arrive_code='" + area_arrive_code + "'");
                if (rows.Length > 0)
                {
                    switch (customer[1])
                    {
                        case "應還":
                            //e.Row.Cells[i].Text = rows[0]["plates"].ToString();
                            HyperLink link = new HyperLink();
                            link.Target = "_blank";
                            link.Text = rows[0]["plates"].ToString();
                            link.NavigateUrl = "palletmag_3_detail.aspx?kind=all&customer_code=" + customer[0] + "&supplier_code=" + area_arrive_code +"&date="+ sdate.Text ;
                            e.Row.Cells[i].Controls.Add(link);
                            break;
                        case "實還":
                            //e.Row.Cells[i].Text = rows[0]["rtn_plates"].ToString();
                            link = new HyperLink();
                            link.Target = "_blank";
                            link.Text = rows[0]["rtn_plates"].ToString();
                            link.NavigateUrl = "palletmag_3_detail.aspx?kind=rtn&customer_code=" + customer[0] + "&supplier_code=" + area_arrive_code + "&date=" + sdate.Text;
                            e.Row.Cells[i].Controls.Add(link);
                            break;
                        case "尚欠":
                            //e.Row.Cells[i].Text = rows[0]["owe_plates"].ToString();
                            link = new HyperLink();
                            link.Target = "_blank";
                            link.Text = rows[0]["owe_plates"].ToString();
                            link.NavigateUrl = "palletmag_3_detail.aspx?kind=owe&customer_code=" + customer[0] + "&supplier_code=" + area_arrive_code + "&date=" + sdate.Text;
                            e.Row.Cells[i].Controls.Add(link);
                            break;
                    }
                }

            }
        }

    }

    protected void export_Click(object sender, EventArgs e)
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        Response.ClearContent();
        Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>");
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        Response.HeaderEncoding = System.Text.Encoding.UTF8;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=test.xls"));
        this.GridView1.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
    }

    protected void export2_Click(object sender, EventArgs e)
    {
        string sheet_title = "欠板統計表";
        string file_name = "欠板統計表" + DateTime.Now.ToString("yyyyMMdd");
        DataTable dt = DT_col;
        if (dt != null && dt.Rows.Count > 0)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Title = sheet_title;
                p.Workbook.Worksheets.Add(sheet_title);
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                ws.Row(1).Height = 40;
                int colIndex = 1;
                int rowIndex = 1;
                for (int i = 0; i <= dt.Columns.Count - 1; i++)
                {
                    var cell = ws.Cells[rowIndex, colIndex];
                    var fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(Color.LightGray);


                    //Setting Top/left,right/bottom borders.
                    var border = cell.Style.Border;
                    border.Bottom.Style =
                        border.Top.Style =
                        border.Left.Style =
                        border.Right.Style = ExcelBorderStyle.Thin;

                    cell.Style.WrapText = true;       //自动换行

                    //Setting Value in cell
                    switch (i)
                    {
                        case 0:
                            cell.Value = "";
                            for (int j = 0; j <= dt.Rows.Count - 1; j++)
                            {
                                ws.Cells[rowIndex + j + 2, colIndex].Value = dt.Rows[j]["area_arrive_code"].ToString() + " " + dt.Rows[j]["Supplier_name"].ToString();
                            }
                            colIndex++;
                            break;
                        case 1:
                            break;
                        default:
                            if (dt.Columns[i].ColumnName.Split('-')[1] == "應還")
                            {
                                cell.Value = dt.Columns[i].ColumnName.Split('-')[0] + (char)13 + (char)10 + DT_cus.Select("customer_code='" + dt.Columns[i].ColumnName.Split('-')[0] + "'")[0]["customer_shortname"].ToString();
                                ws.Cells[rowIndex, colIndex, rowIndex, colIndex + 2].Merge = true; //合併儲存格
                                ws.Cells[rowIndex + 1, colIndex].Value = "應還";
                                ws.Cells[rowIndex + 1, colIndex + 1].Value = "實還";
                                ws.Cells[rowIndex + 1, colIndex + 2].Value = "尚欠";

                                //for (int k = 1; k <= 3; k++)
                                //{   
                                for (int j = 0; j <= dt.Rows.Count - 1; j++)
                                {
                                    string[] customer = dt.Columns[i].ColumnName.Split('-');
                                    string area_arrive_code = dt.Rows[j]["area_arrive_code"].ToString();
                                    DataRow[] rows = DT.Select("customer_code='" + customer[0] + "' and area_arrive_code='" + area_arrive_code + "'");
                                    if (rows.Length > 0)
                                    {
                                        ws.Cells[rowIndex + j + 2, colIndex].Value = rows[0]["plates"].ToString();
                                        ws.Cells[rowIndex + j + 2, colIndex + 1].Value = rows[0]["rtn_plates"].ToString();
                                        ws.Cells[rowIndex + j + 2, colIndex + 2].Value = rows[0]["owe_plates"].ToString();

                                    }


                                }
                                //}
                            }
                            colIndex++;
                            break;
                    }

                }
                ws.Cells[rowIndex + 1, 1, rowIndex + 1, colIndex - 1].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ws.Cells[rowIndex + 1, 1, rowIndex + 1, colIndex - 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);//设置背景色
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                ws.Cells[rowIndex, 1, dt.Rows.Count + 2, colIndex - 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex++;

                //using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                //{
                //    col.Style.Numberformat.Format = "yyyy/MM/dd";
                //    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                //}


                //foreach (DataRow dr in dt.Rows)
                //{
                //    colIndex = 1;
                //    foreach (DataColumn dc in dt.Columns)
                //    {
                //        var cell = ws.Cells[rowIndex, colIndex];
                //        cell.Value = dr[dc.ColumnName];

                //        //Setting borders of cell
                //        var border = cell.Style.Border;
                //        border.Left.Style =
                //            border.Right.Style = ExcelBorderStyle.Thin;
                //        colIndex++;
                //    }

                //    rowIndex++;
                //}
                ws.Cells.AutoFitColumns();
                ws.View.FreezePanes(2, 1);
                ws.Cells.Style.Font.Size = 12;
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.BinaryWrite(p.GetAsByteArray());
                Response.End();

                //str_retuenMsg += "下載完成!";
            }

        }
        //else
        //{
        //    str_retuenMsg += "查無此資訊，請重新確認!";
        //}


    }
}
