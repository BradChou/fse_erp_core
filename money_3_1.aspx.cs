﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class money_3_1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Boolean IsManager = false;
            if (Session["account_code"] != null & Session["account_code"].ToString() == "skyeyes")
                IsManager = true;
           
            InitializeOilPrice();

            PublishBalanceTable();
            HightLightCurrentOilPriceLevel();
        }
    }

    protected void InitializeOilPrice()
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = @"SELECT TOP 100 [create_user]
      ,[create_date]
      ,[oil_price]
      ,[step_distance]
FROM [JunFuReal].[dbo].[oil_price_update_log]
ORDER BY create_date DESC";

        DataTable oilPriceHistory = dbAdapter.getDataTable(cmd);

        if (oilPriceHistory.Rows.Count > 0)
        {
            lbOilPrice.Text = oilPriceHistory.Rows[0]["oil_price"].ToString();
            txtOilPrice.Text = oilPriceHistory.Rows[0]["oil_price"].ToString();
            lbCurrentLevel.Text = oilPriceHistory.Rows[0]["step_distance"].ToString();
            History.DataSource = oilPriceHistory;
            History.DataBind();
        }
    }

    protected void PublishBalanceTable()
    {
        SqlCommand cmd = new SqlCommand();

        cmd.CommandText = @"SELECT [step_distance]
      ,[min_oil_price]
      ,[max_oil_price]
      ,[one_pallet_price_balance]
      ,[two_pallet_price_balance]
      ,[three_pallet_price_balance]
      ,[four_pallet_price_balance]
      ,[five_pallet_price_balance]
      ,[six_pallet_price_balance]
  FROM [JunFuReal].[dbo].[oil_price_balance]";

        oilPriceBalance.DataSource = dbAdapter.getDataTable(cmd);
        oilPriceBalance.DataBind();
    }

    protected void HightLightCurrentOilPriceLevel()
    {
        string level = lbCurrentLevel.Text;

        foreach (GridViewRow row in oilPriceBalance.Rows)
        {
            row.BackColor = System.Drawing.Color.White;
            string levelInGridRow = ((Label)row.FindControl("lbStepDistance")).Text;
            if(levelInGridRow == level)
                row.BackColor = System.Drawing.Color.Yellow;
        }       
    }

    protected void oilPriceBalance_RowEditing(object sender, GridViewEditEventArgs e)
    {
        oilPriceBalance.EditIndex = e.NewEditIndex;
        PublishBalanceTable();
    }

    protected void oilPriceBalance_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        oilPriceBalance.EditIndex = -1;
        PublishBalanceTable();
    }

    protected void oilPriceBalance_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        SqlCommand cmd = new SqlCommand();

        cmd.CommandText = @"  UPDATE oil_price_balance SET one_pallet_price_balance = @One, 
  two_pallet_price_balance = @Two,
  three_pallet_price_balance = @Three,
  four_pallet_price_balance = @Four, 
  five_pallet_price_balance = @Five, 
  six_pallet_price_balance = @Six WHERE step_distance = @StepDistance";

        cmd.Parameters.AddWithValue("@One", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtOnePallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@Two", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtTwoPallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@Three", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtThreePallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@Four", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtFourPallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@Five", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtFivePallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@Six", (oilPriceBalance.Rows[e.RowIndex].FindControl("txtSixPallet") as TextBox).Text.Trim());
        cmd.Parameters.AddWithValue("@StepDistance", Convert.ToInt32(oilPriceBalance.DataKeys[e.RowIndex].Value.ToString()));

        dbAdapter.execNonQuery(cmd);

        oilPriceBalance.EditIndex = -1;
        PublishBalanceTable();
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        EditMode.Visible = true;
        ShowMode.Visible = false;
    }

    protected void btnSaveOilPrice_Click(object sender, EventArgs e)
    {
        EditMode.Visible = false;
        ShowMode.Visible = true;

        SqlCommand cmd = new SqlCommand();

        cmd.CommandText = @"INSERT INTO oil_price_update_log (create_user, create_date, oil_price, step_distance)
VALUES (@user, GETDATE(), @price, @stepDistance)";

        cmd.Parameters.AddWithValue("@user", Session["account_code"].ToString());
        cmd.Parameters.AddWithValue("@price", txtOilPrice.Text);

        int stepDistance = 0;
        foreach (GridViewRow row in oilPriceBalance.Rows)
        {
            Label lbStepDistance = (Label)row.FindControl("lbStepDistance");
            Label lbMinOilPrice = (Label)row.FindControl("lbMinOilPrice");

            if (Convert.ToInt32(txtOilPrice.Text) >= Convert.ToInt32(lbMinOilPrice.Text))
                stepDistance = Convert.ToInt32(lbStepDistance.Text);
            else
                break;
        }

        cmd.Parameters.AddWithValue("@stepDistance", stepDistance);

        dbAdapter.execNonQuery(cmd);
        InitializeOilPrice();
        HightLightCurrentOilPriceLevel();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        EditMode.Visible = false;
        ShowMode.Visible = true;
    }
}