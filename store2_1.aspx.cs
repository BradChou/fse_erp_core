﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class store2_1 : System.Web.UI.Page
{
        

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 場區
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                Area.DataSource = dt;
                Area.DataValueField = "seq";
                Area.DataTextField = "code_name";
                Area.DataBind();
                //Area.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 初始化日期
            sdate.Text = DateTime.Now.ToString("yyy/MM/01");
            edate.Text = DateTime.Now.ToString("yyy/MM/dd");
            #endregion
           
            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "where 1=1";

        if (sdate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
            wherestr += "  and indate >=@sdate";
        }
        if (edate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
            wherestr += "  and indate <=@edate";
        }
        if (Area.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@area", Area.SelectedValue.ToString());
            wherestr += "  and Area =@area";
        }
        if (car_retailer_text.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_retailer_text", car_retailer_text.Text.ToString());
            wherestr += "  and car_retailer_text =@car_retailer_text";
        }
      
        objCommand.CommandText = string.Format(@"select convert(varchar,ROW_NUMBER() OVER (ORDER BY [PaymentObject] ASC)) as '序號',PaymentObject as '車老闆', driver as '運務士', car_license as '車號',isnull(sum(主線),0) as '應收主線',isnull(sum(爆量),0) as '應收爆量',
                                                        isnull(sum(店到店),0) as '應收店到店',isnull(sum(回頭車),0) as '應收回頭車',isnull(sum(加派車),0) as '應收加派車',
                                                        isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0) as '應收總計',
                                                        isnull(sum(主線1),0) as '應付主線',isnull(sum(爆量1),0) as '應付爆量',isnull(sum(店到店1),0) as '應付店到店',isnull(sum(回頭車1),0) as '應付回頭車',
                                                        isnull(sum(加派車1),0) as '應付加派車',isnull(sum(主線1),0) +isnull(sum(爆量1),0)+isnull(sum(店到店1),0)+isnull(sum(回頭車1),0)+isnull(sum(加派車1),0)  as '應付總計',
                                                        isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0)-isnull(sum(主線1),0) -isnull(sum(爆量1),0)-isnull(sum(店到店1),0)-isnull(sum(回頭車1),0)-isnull(sum(加派車1),0) as '收入總計'
                                                 from (select *
                                                       from (select  PaymentObject, driver, car_license, SUM(income) AS intot, SUM(expenses) AS outtot, Items_Text,Items_Text+'1' as item_txt
                                                             from    ttAssetsSB
                                                             {0}
                                                             group by   PaymentObject, driver, car_license, Items_Text) t
                                                       pivot
                                                       (sum(intot)
                                                       for items_text in ([主線],[爆量],[店到店],[回頭車],[加派車])
                                                       ) as t2
                                                       pivot
                                                       (sum(outtot)
                                                       for item_txt in ([主線1],[爆量1],[店到店1],[回頭車1],[加派車1] )
                                                       ) as t3) finaltb
                                                 group by PaymentObject, driver, car_license
                                                 union all
                                                 select '' as '序號','' as '車老闆','' as '運務士','總計' as '車號',sum(應收主線) as '應收主線',sum(應收爆量) as '應收爆量',sum(應收店到店) as '應收店到店',sum(應收回頭車) as '應收回頭車',sum(應收加派車) as '應收加派中',sum(應收總計) as '應收總計',
                                                        sum(應付主線) as '應付主線',sum(應付爆量) as '應付爆量',sum(應付店到店) as '應付店到店',sum(應付回頭車) as '應付回頭車',sum(應付加派車) as '應付加派車',sum(應付總計) as '應付總計',sum(收入總計) as '收入總計'
                                                 from (select PaymentObject as '車老闆', driver as '運務士', car_license as '車號',isnull(sum(主線),0) as '應收主線',isnull(sum(爆量),0) as '應收爆量',isnull(sum(店到店),0) as '應收店到店',isnull(sum(回頭車),0) as '應收回頭車',isnull(sum(加派車),0) as '應收加派車',
                                                              isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0) as '應收總計',
                                                              isnull(sum(主線1),0) as '應付主線',isnull(sum(爆量1),0) as '應付爆量',isnull(sum(店到店1),0) as '應付店到店',isnull(sum(回頭車1),0) as '應付回頭車',isnull(sum(加派車1),0) as '應付加派車',
                                                              isnull(sum(主線1),0) +isnull(sum(爆量1),0)+isnull(sum(店到店1),0)+isnull(sum(回頭車1),0)+isnull(sum(加派車1),0)  as '應付總計',
                                                              isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0)-isnull(sum(主線1),0) -isnull(sum(爆量1),0)-isnull(sum(店到店1),0)-isnull(sum(回頭車1),0)-isnull(sum(加派車1),0) as '收入總計'
                                                       from (select *
                                                             from (select  PaymentObject, driver, car_license, SUM(income) AS intot, SUM(expenses) AS outtot, Items_Text,Items_Text+'1' as item_txt
                                                                   from    ttAssetsSB
                                                                   {0}
                                                                   group by   PaymentObject, driver, car_license, Items_Text) t
                                                                   pivot
                                                                   (sum(intot)
                                                                   for items_text in ([主線],[爆量],[店到店],[回頭車],[加派車])
                                                                   ) as t2
                                                                   pivot
                                                                   (sum(outtot)
                                                                   for item_txt in ([主線1],[爆量1],[店到店1],[回頭車1],[加派車1] )
                                                                   ) as t3) finaltb
                                                 group by PaymentObject, driver, car_license) lasttb", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {     
            New_List.DataSource = dt;
            New_List.DataBind();  
            //因為最後一筆是總數計數，所以筆數要-1
            ltotalpages.Text = (dt.Rows.Count - 1).ToString();
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }
    

    protected void btExport2_Click(object sender, EventArgs e)
    {
        string sheet_title = "趟次總表_" + Area.SelectedItem.Text.ToString() + Convert.ToDateTime(sdate.Text.Trim().ToString()).ToString("yyyyMMdd") + "~" + Convert.ToDateTime(edate.Text.Trim().ToString()).ToString("yyyyMMdd");
        string file_name = "趟次總表_" + Area.SelectedItem.Text.ToString() + "_" + car_retailer_text.Text.ToString() + Convert.ToDateTime(sdate.Text.Trim().ToString()).ToString("yyyyMMdd") + "_" + Convert.ToDateTime(edate.Text.Trim().ToString()).ToString("yyyyMMdd");
        string cell_name = "趟次總表　場區："+Area.SelectedItem.Text.ToString() + "　車行：" + car_retailer_text.Text.ToString() + "　統計期間：" + Convert.ToDateTime(sdate.Text.Trim().ToString()).ToString("yyyy/MM/dd") + "~" + Convert.ToDateTime(edate.Text.Trim().ToString()).ToString("yyyy/MM/dd");
        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "where 1=1";

            if (sdate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
                wherestr += "  and indate >=@sdate";
            }
            if (edate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
                wherestr += "  and indate <=@edate";
            }
            if (Area.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@area", Area.SelectedValue.ToString());
                wherestr += "  and Area =@area";
            }
            if (car_retailer_text.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@car_retailer_text", car_retailer_text.Text.ToString());
                wherestr += "  and car_retailer_text =@car_retailer_text";
            }

            objCommand.CommandText = string.Format(@"select convert(varchar,ROW_NUMBER() OVER (ORDER BY [PaymentObject] ASC)) as '序號',PaymentObject as '車老闆', driver as '運務士', car_license as '車號',isnull(sum(主線),0) as '應收主線',isnull(sum(爆量),0) as '應收爆量',
                                                        isnull(sum(店到店),0) as '應收店到店',isnull(sum(回頭車),0) as '應收回頭車',isnull(sum(加派車),0) as '應收加派車',
                                                        isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0) as '應收總計',
                                                        isnull(sum(主線1),0) as '應付主線',isnull(sum(爆量1),0) as '應付爆量',isnull(sum(店到店1),0) as '應付店到店',isnull(sum(回頭車1),0) as '應付回頭車',
                                                        isnull(sum(加派車1),0) as '應付加派車',isnull(sum(主線1),0) +isnull(sum(爆量1),0)+isnull(sum(店到店1),0)+isnull(sum(回頭車1),0)+isnull(sum(加派車1),0)  as '應付總計',
                                                        isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0)-isnull(sum(主線1),0) -isnull(sum(爆量1),0)-isnull(sum(店到店1),0)-isnull(sum(回頭車1),0)-isnull(sum(加派車1),0) as '收入總計'
                                                 from (select *
                                                       from (select  PaymentObject, driver, car_license, SUM(income) AS intot, SUM(expenses) AS outtot, Items_Text,Items_Text+'1' as item_txt
                                                             from    ttAssetsSB
                                                             {0}
                                                             group by   PaymentObject, driver, car_license, Items_Text) t
                                                       pivot
                                                       (sum(intot)
                                                       for items_text in ([主線],[爆量],[店到店],[回頭車],[加派車])
                                                       ) as t2
                                                       pivot
                                                       (sum(outtot)
                                                       for item_txt in ([主線1],[爆量1],[店到店1],[回頭車1],[加派車1] )
                                                       ) as t3) finaltb
                                                 group by PaymentObject, driver, car_license
                                                 union all
                                                 select '' as '序號','' as '車老闆','' as '運務士','總計' as '車號',sum(應收主線) as '應收主線',sum(應收爆量) as '應收爆量',sum(應收店到店) as '應收店到店',sum(應收回頭車) as '應收回頭車',sum(應收加派車) as '應收加派中',sum(應收總計) as '應收總計',
                                                        sum(應付主線) as '應付主線',sum(應付爆量) as '應付爆量',sum(應付店到店) as '應付店到店',sum(應付回頭車) as '應付回頭車',sum(應付加派車) as '應付加派車',sum(應付總計) as '應付總計',sum(收入總計) as '收入總計'
                                                 from (select PaymentObject as '車老闆', driver as '運務士', car_license as '車號',isnull(sum(主線),0) as '應收主線',isnull(sum(爆量),0) as '應收爆量',isnull(sum(店到店),0) as '應收店到店',isnull(sum(回頭車),0) as '應收回頭車',isnull(sum(加派車),0) as '應收加派車',
                                                              isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0) as '應收總計',
                                                              isnull(sum(主線1),0) as '應付主線',isnull(sum(爆量1),0) as '應付爆量',isnull(sum(店到店1),0) as '應付店到店',isnull(sum(回頭車1),0) as '應付回頭車',isnull(sum(加派車1),0) as '應付加派車',
                                                              isnull(sum(主線1),0) +isnull(sum(爆量1),0)+isnull(sum(店到店1),0)+isnull(sum(回頭車1),0)+isnull(sum(加派車1),0)  as '應付總計',
                                                              isnull(sum(主線),0) +isnull(sum(爆量),0)+isnull(sum(店到店),0)+isnull(sum(回頭車),0)+isnull(sum(加派車),0)-isnull(sum(主線1),0) -isnull(sum(爆量1),0)-isnull(sum(店到店1),0)-isnull(sum(回頭車1),0)-isnull(sum(加派車1),0) as '收入總計'
                                                       from (select *
                                                             from (select  PaymentObject, driver, car_license, SUM(income) AS intot, SUM(expenses) AS outtot, Items_Text,Items_Text+'1' as item_txt
                                                                   from    ttAssetsSB
                                                                   {0}
                                                                   group by   PaymentObject, driver, car_license, Items_Text) t
                                                                   pivot
                                                                   (sum(intot)
                                                                   for items_text in ([主線],[爆量],[店到店],[回頭車],[加派車])
                                                                   ) as t2
                                                                   pivot
                                                                   (sum(outtot)
                                                                   for item_txt in ([主線1],[爆量1],[店到店1],[回頭車1],[加派車1] )
                                                                   ) as t3) finaltb
                                                 group by PaymentObject, driver, car_license) lasttb", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    
                    New_List.DataSource = dt;
                    New_List.DataBind();
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 3;
                        ws.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;//水平置中
                        ws.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;//垂直置中
                        ws.Cells[1, 1].Value = cell_name;
                        ws.Cells[2, 1].Value = "序號";
                        ws.Cells[2, 2].Value = "車老闆";
                        ws.Cells[2, 3].Value = "運務士";
                        ws.Cells[2, 4].Value = "車號";
                        ws.Cells[2, 5].Value = "應收項目";
                        ws.Cells[3, 5].Value = "主線";
                        ws.Cells[3, 6].Value = "爆量";
                        ws.Cells[3, 7].Value = "店到店";
                        ws.Cells[3, 8].Value = "回頭車";
                        ws.Cells[3, 9].Value = "加派車";
                        ws.Cells[3, 10].Value = "應收總計";
                        ws.Cells[2, 11].Value = "應付項目";
                        ws.Cells[3, 11].Value = "主線";
                        ws.Cells[3, 12].Value = "爆量";
                        ws.Cells[3, 13].Value = "店到店";
                        ws.Cells[3, 14].Value = "回頭車";
                        ws.Cells[3, 15].Value = "加派車";
                        ws.Cells[3, 16].Value = "應付總計";
                        ws.Cells[2, 17].Value = "收入總計";

                        ws.Cells[1, 1, 1, 17].Merge = true;  //合併儲存格
                        ws.Cells[2, 1, 3, 1].Merge = true;   //合併儲存格
                        ws.Cells[2, 2, 3, 2].Merge = true;   //合併儲存格
                        ws.Cells[2, 3, 3, 3].Merge = true;   //合併儲存格
                        ws.Cells[2, 4, 3, 4].Merge = true;   //合併儲存格
                        ws.Cells[2, 5, 2, 10].Merge = true;  //合併儲存格
                        ws.Cells[2, 11, 2, 16].Merge = true; //合併儲存格
                        ws.Cells[2, 17, 3, 17].Merge = true; //合併儲存格

                        for (int i=2;i<=3;i++)
                        { 
                            for(int j=1;j<=17;j++)
                            { 
                                ws.Cells[i, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;//水平置中
                                ws.Cells[i, j].Style.VerticalAlignment = ExcelVerticalAlignment.Center;//垂直置中
                                var fill = ws.Cells[i, j].Style.Fill;
                                    fill.PatternType = ExcelFillStyle.Solid;
                                    fill.BackgroundColor.SetColor(Color.LightGray);
                                var border = ws.Cells[i, j].Style.Border;
                                    border.Bottom.Style =
                                    border.Top.Style =
                                    border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                            }
                        }
                        //foreach (DataColumn dc in dt.Columns)
                        //{
                        //    var cell = ws.Cells[rowIndex, colIndex];
                        //    var fill = cell.Style.Fill;
                        //    fill.PatternType = ExcelFillStyle.Solid;
                        //    fill.BackgroundColor.SetColor(Color.LightGray);

                        //    //Setting Top/left,right/bottom borders.
                        //    var border = cell.Style.Border;
                        //    border.Bottom.Style =
                        //        border.Top.Style =
                        //        border.Left.Style =
                        //        border.Right.Style = ExcelBorderStyle.Thin;

                        //    //Setting Value in cell
                        //    cell.Value = dc.ColumnName;
                        //    colIndex++;
                        //}
                        rowIndex++;

                        
                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Bottom.Style =
                                    border.Top.Style =
                                    border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        //最後一列的總計欄文字置中
                        ws.Cells[dt.Rows.Count+3, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;//水平置中
                        ws.Cells[dt.Rows.Count+3, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;//垂直置中
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(4, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }
}