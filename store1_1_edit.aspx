﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_1_edit.aspx.cs" Inherits="store1_1_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"/>
<script src="js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script src="js/timepicker/jquery-ui-sliderAccess.js" type="text/javascript"></script>
<script src="js/timepicker/jquery-ui-timepicker-zh-TW.js" type="text/javascript"></script>

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $('.fancybox').fancybox();

            $("#carsel").fancybox({
                'width': 1200,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
            $(".date_picker").datetimepicker({
                showSecond: true, //顯示秒  
                timeFormat: "HH:mm",
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            var times = new Date();
            $('.datetimepicker').prop("readonly", true).datetimepicker({
                //showSecond: true, //顯示秒  
                timeFormat: 'HH:mm', //格式化時間  
                dateFormat: 'yy/mm/dd',
                defaultDate: (new Date()),  //預設當日
                hour: times.getHours(),
                minute: times.getMinutes(),
                controlType:"select" 
            });

            
        });

        function ValidateFloat2(e, pnumber) {
            if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
                var newValue = /\d+[.]?[1-9]?/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                }
            }
            return false;
        }

    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

            
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2 class="margin-bottom-10">主線運費-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
    <div class="templatemo-login-form">
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>主線/爆量</label>                            
                            <asp:DropDownList ID="Items" runat="server" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="Items" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇主線/爆量</asp:RequiredFieldValidator>
                        </div>                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>交易日期</label>
                            <asp:TextBox ID="InDate" runat="server" class="form-control date_picker" ></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="InDate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入交易日期</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand"><span class="REDWD_b">*</span>車牌</label>
                            <asp:TextBox ID="car_license" runat="server"  CssClass="form-control" MaxLength="20" placeholder="請選擇車牌" required  autocomplete="off" Enable="false"></asp:TextBox>
                            <asp:HiddenField ID="car_license_no" runat="server" /> 
                            <a id="carsel"  href="assets1_9_carsel.aspx?car_license_no=<%= car_license_no.ClientID %>&car_license=<%= car_license.ClientID %>&Hid_assets_id=<%=Hid_assets_id.ClientID%>&cabin_type_text=&driver=<%=driver.ClientID%>&car_retailer_text=<%=car_retailer_text.ClientID%>&RequestObject=<%=RequestObject.ClientID%>" class="fancybox fancybox.iframe " ><span class="btn btn-link small  ">...</span></a>
                            <asp:HiddenField ID="Hid_assets_id" runat="server" />                            
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇車牌</asp:RequiredFieldValidator>
                        </div>                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>出場時間</label>
                            <asp:TextBox ID="starttime" runat="server" class="form-control datetimepicker" ></asp:TextBox>                            
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="starttime" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入出廠時間</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>返場時間</label>
                            <asp:TextBox ID="endtime" runat="server" class="form-control datetimepicker" ></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="endtime" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入返場時間</asp:RequiredFieldValidator>
                        </div>                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand">車型</label>
                            <asp:DropDownList ID="car_type" runat="server" class="form-control " ></asp:DropDownList>
                            <%--<asp:TextBox ID="cabin_type_text" runat="server" class="form-control" ></asp:TextBox>--%>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>首店時間</label>
                            <asp:TextBox ID="startstore_time" runat="server" class="form-control datetimepicker" ></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="startstore_time" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入首店時間</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>末店時間</label>
                            <asp:TextBox ID="endstore_time" runat="server" class="form-control datetimepicker" ></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="endstore_time" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入末店時間</asp:RequiredFieldValidator>
                        </div>              
                       
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand">店數</label>
                            <asp:TextBox ID="Stores" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                         <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand"><span class="REDWD_b">*</span>場區</label>
                            <asp:DropDownList ID="Area" runat="server" class="form-control " onselectedindexchanged="Area_SelIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req9" runat="server" ControlToValidate="Area" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇場區</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="Route"><span class="REDWD_b">*</span>路線</label>
                            <%--<asp:TextBox ID="Route" runat="server" class="form-control"></asp:TextBox>--%>
                            <asp:DropDownList ID="Route" runat="server" class="form-control " onselectedindexchanged="Route_SelIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            <asp:Literal ID="Route_name" runat="server"></asp:Literal>
                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="Route" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇路線</asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>日/夜</label>
                            <asp:DropDownList ID="DayNight" runat="server" class="form-control">
                                <asp:ListItem Value="1">日</asp:ListItem>
                                <asp:ListItem Value="2">夜</asp:ListItem>
                            </asp:DropDownList>
                        </div>                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="milage">公里</label>
                            <asp:TextBox ID="milage" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="RequestObject">請款對象</label>
                            <asp:TextBox ID="RequestObject" runat="server" class="form-control" ></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tonnes"><span class="REDWD_b">*</span>溫層</label>
                            <asp:DropDownList ID="Temperature" runat="server" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="Temperature" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇溫層</asp:RequiredFieldValidator>
                        </div>                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="driver">司機</label>
                            <asp:TextBox ID="driver" runat="server" class="form-control" ></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="car_retailer_text">車行</label>
                            <asp:TextBox ID="car_retailer_text" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="supplier">供應商</label>
                            <asp:DropDownList ID="supplier" runat="server" CssClass="form-control chosen-select" onselectedindexchanged="Supplier_SelIndexChanged" AutoPostBack="True"></asp:DropDownList>                            
                            <asp:TextBox ID="PaymentObject" runat="server" class="form-control" Width="150px" ></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req_supplier" runat="server" ControlToValidate="supplier" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇供應商</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="income"><span class="REDWD_b">*</span>承攬價(收入)</label>
                            <asp:TextBox ID="income" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req11" runat="server" ControlToValidate="income" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入承攬價</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="expenses"><span class="REDWD_b">*</span>發包價(支出)</label>
                            <asp:TextBox ID="expenses" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/^(\d*\.?\d{0,2}).*/,'$1')"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req12" runat="server" ControlToValidate="expenses" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入發包價</asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="memo">備註</label>
                            <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

        <div class="form-group text-center" style="margin-top:5px;">
            <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
            <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/store1_1.aspx">取消</asp:LinkButton>
            <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
        </div>

    </div>
    </div>
</asp:Content>

