﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dispatch1_1_edit : System.Web.UI.Page
{
    public string id
    {
        get { return ViewState["id"].ToString(); }
        set { ViewState["id"] = value; }
    }

    public int task_type
    {
        get { return (int)ViewState["type"]; }
        set { ViewState["type"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "";
            task_type = Request.QueryString["type"] != null ? Convert.ToInt32(Request.QueryString["type"]) : 0;
            switch (task_type)
            {
                case 1:
                    task_type_text.Text = "B段";
                    break;
                case 2:
                    task_type_text.Text = "專車";
                    break;
                default:
                    task_type_text.Text = "自營客戶";
                    break;
            }

            #region 
            manager_type = Session["manager_type"].ToString();        //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier.DataSource = Utility.getSupplierDT2(supplier_code, manager_type);
            supplier.DataValueField = "supplier_no";
            supplier.DataTextField = "showname";
            supplier.DataBind();
            #endregion

            //#region 路線
            //SqlCommand objCommandRoad = new SqlCommand();
            //string wherestrRoad = "";
            //var StrRoad = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=3", "Account_Code", Session["account_code"].ToString());
            //if (!string.IsNullOrEmpty(StrRoad))
            //{
            //    wherestrRoad += " and (";
            //    var StrRoadAry = StrRoad.Split(',');
            //    var i = 0;
            //    foreach (var item2 in StrRoadAry)
            //    {
            //        if (!string.IsNullOrEmpty(item2))
            //        {
            //            if (i > 0)
            //            {
            //                wherestrRoad += " or ";
            //            }
            //            objCommandRoad.Parameters.AddWithValue("@code_id" + item2, item2);
            //            wherestrRoad += "  seq=@code_id" + item2;
            //            i += 1;
            //        }
            //    }
            //    wherestrRoad += " )";
            //}
            //objCommandRoad.CommandText = string.Format(@"select seq, code_name from ttItemCodesRoad with(nolock) where active_flag = 1 {0} order by seq", wherestrRoad);
            //using (DataTable dtRoad = dbAdapter.getDataTable(objCommandRoad))
            //{
            //    task_route.DataSource = dtRoad;
            //    task_route.DataValueField = "seq";
            //    task_route.DataTextField = "code_name";
            //    task_route.DataBind();
            //    task_route.Items.Insert(0, new ListItem("請選擇", ""));
            //}
            //#endregion

            readdata(id);

        }
    }

    private void readdata(string id)
    {  
        if (id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@t_id", id);

            cmd.CommandText = @"Select * from ttDispatchTask A with(nolock) 
                                Where t_id = @t_id ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                Task_id.Text = dt.Rows[0]["task_id"].ToString().Trim();
                task_date.Text = Convert.ToDateTime(dt.Rows[0]["task_date"]).ToString("yyyy/MM/dd");                                
                //task_route.SelectedValue = dt.Rows[0]["task_route"].ToString();
                driver.Text = dt.Rows[0]["driver"].ToString().Trim();
                car_license.Text = dt.Rows[0]["car_license"].ToString().Trim();
                price.Text = dt.Rows[0]["price"].ToString().Trim();
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();
                supplier.SelectedValue = dt.Rows[0]["supplier_no"].ToString().Trim();
            }

            string wherestr = "";
            SqlCommand cmd2 = new SqlCommand();
            cmd2.Parameters.AddWithValue("@t_id", id);
            cmd2.CommandText = string.Format(@"Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
                                                A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name'
                                                from ttDispatchTask T with(Nolock) 	
		                                        INNER JOIN ttDispatchTaskDetail F with(nolock) on T.t_id = F.t_id
		                                        INNER JOIN tcDeliveryRequests A with(nolock) on A.request_id = F.request_id
                                                LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
                                                LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                WHERE A.pricing_type <> '05'
		                                        AND  T.t_id= @t_id
                                                ORDER BY A.print_date, A.customer_code , A.area_arrive_code  ", wherestr);

            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
            {
                New_List.DataSource = dt2;
                New_List.DataBind();
            }

        }
        else
        {
            ApStatus = "Add";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@task_date", DateTime.Today);
                cmd.CommandText = @"Select ISNULL(max(serial),0) from ttDispatchTask with(nolock) where task_date = @task_date ";
                DataTable  dt = dbAdapter.getDataTable(cmd);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string serial = (Convert.ToInt32(dt.Rows[0][0]) + 1).ToString("000");
                    Task_id.Text = DateTime.Today.ToString("yyyyMMdd") +"-" + serial;   //任務編號：日期+"-"+ 3碼流水號
                }
            }


            string request_id = Request.QueryString["request_id"] != null ? Request.QueryString["request_id"].ToString() : "";
            if (request_id != "")
            {
                string wherestr = "";
                SqlCommand cmd = new SqlCommand();
                wherestr += dbAdapter.genWhereCommIn(ref cmd, "A.request_id", request_id.Split(','));


                cmd.CommandText = string.Format(@"Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
                                                    A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name'
                                                    from tcDeliveryRequests A with(nolock)  
                                                    LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                    LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
                                                    LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                    WHERE 1=1
                                                    {0}
                                                    ORDER BY A.print_date, A.customer_code , A.area_arrive_code  ", wherestr);

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List.DataSource = dt;
                    New_List.DataBind();
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選貨號');parent.$.fancybox.close();</script>", false);
                return;
            }
            
        }

       
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                task_date.Text = DateTime.Now.ToString("yyyy/MM/dd");
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        int t_id = 0;
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":
                SqlCommand cmdadd = new SqlCommand();
                cmdadd.Parameters.AddWithValue("@task_type", task_type);
                cmdadd.Parameters.AddWithValue("@task_id", Task_id.Text);
                cmdadd.Parameters.AddWithValue("@task_date", task_date.Text);
                //cmdadd.Parameters.AddWithValue("@supplier", supplier.SelectedValue);
                cmdadd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue);
                //cmdadd.Parameters.AddWithValue("@task_route", task_route.SelectedValue);
                cmdadd.Parameters.AddWithValue("@serial", Task_id.Text.Split('-')[1]);
                cmdadd.Parameters.AddWithValue("@driver", driver.Text);
                cmdadd.Parameters.AddWithValue("@car_license", car_license.Text);
                cmdadd.Parameters.AddWithValue("@price", price.Text);               
                cmdadd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@c_user", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@c_time", DateTime.Now);                                   //建立時間
                //cmdadd.CommandText = dbAdapter.SQLdosomething("ttDispatchTask", cmdadd, "insert");
                //try
                //{
                //    dbAdapter.execNonQuery(cmdadd);
                //}
                //catch (Exception ex)
                //{
                //    ErrStr = ex.Message;
                //}

                cmdadd.CommandText = dbAdapter.genInsertComm("ttDispatchTask", true, cmdadd);        //新增
                if (int.TryParse(dbAdapter.getScalarBySQL(cmdadd).ToString(), out t_id))
                {
                    for (int i = 0; i <= New_List.Items.Count - 1; i++)
                    {
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            string request_id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value;
                            cmd2.Parameters.AddWithValue("@t_id", t_id);
                            cmd2.Parameters.AddWithValue("@request_id", request_id);
                            cmd2.CommandText = dbAdapter.SQLdosomething("ttDispatchTaskDetail", cmd2, "insert");
                            try
                            {
                                dbAdapter.execNonQuery(cmd2);
                            }
                            catch (Exception ex)
                            {
                                ErrStr = ex.Message;
                            }
                        }
                    }                    
                }

                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@task_date", task_date.Text);
                //cmdupd.Parameters.AddWithValue("@supplier", supplier.SelectedValue);
                cmdupd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue);
                //cmdupd.Parameters.AddWithValue("@task_route", task_route.SelectedValue);
                cmdupd.Parameters.AddWithValue("@driver", driver.Text);
                cmdupd.Parameters.AddWithValue("@car_license", car_license.Text);
                cmdupd.Parameters.AddWithValue("@price", price.Text);
                cmdupd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@c_user", Session["account_code"]);                        //修改人員
                cmdupd.Parameters.AddWithValue("@c_time", DateTime.Now);                                   //修改時間                
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "t_id", id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttDispatchTask", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }
                break;

        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#ContentPlaceHolder1_btnQry').click();parent.$.fancybox.close();alert('" + statustext.Text + "完成'); </script>", false);
        }        
        return;


    }

    
}