﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fileupload_Rorder.aspx.cs" Inherits="fileupload_Rorder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>峻富雲端物流管理-回單上傳</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
	
    <!-- datepicker CSS -->
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">
    
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/web_style.css">

    <!-- jQuery文件 -->
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table class="table">
        <tr>

            <th >上傳檔案
            </th>
            <td >
                <asp:FileUpload ID="file01" runat="server" Width="100%" accept="image/jpeg,.jpg,.gif,.ppng,.tif,.bmp,.pdf" AllowMultiple="True"  />
             <%--   <asp:RegularExpressionValidator ID="RegExValFileUploadFileType" runat="server"
                        ControlToValidate="file01"
                        ErrorMessage="限gif、jpg、jpe*、png、tif、bmp、pdf格式" ForeColor ="Red" 
                        Font-Size="Small" 
                        ValidationExpression="(.*?)\.(gif|jpg|jpe*|png|tif|bmp|pdf)$">
                    </asp:RegularExpressionValidator>--%>
            </td>
        </tr>
    </table>
        <asp:Image ID="Image1" runat="server" />
        <p >
            <asp:HiddenField ID="hidf_id" runat="server" />
            <asp:Button ID="btnsave" CssClass="btn btn-primary" runat="server" Text="確定" ValidationGroup="validate" OnClick="btnsave_Click"  />
            <asp:Button ID="btncancel" CssClass="btn btn-default" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
        </p>
    </div>
    </form>
</body>
</html>
