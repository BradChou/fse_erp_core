﻿using BarcodeLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class DownloadPagePDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            string print_requestid = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "";
            string cusid = Request.QueryString["cusid"] != null ? Request.QueryString["cusid"].ToString() : "";
            PrintReelLabel(print_requestid, cusid);

        }

        

    }

    /// <summary>
    /// 列印標籤(捲筒列印)(依計價模式為列印張數單位)
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReelLabel(string print_requestid , string customer_id)
    {
        if (print_requestid != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = " and request_id in(" + print_requestid + ")";
                string orderby = " order by  A.check_number";
                if (customer_id == "0000049") orderby = " order by  A.cdate , A.request_id"; //三陽工業，指定依打單的順序列印
                cmd.CommandText = string.Format(@"select isnull(D.code_name,'') + CASE A.receipt_flag WHEN 1 THEN '回單' ELSE '' END + CASE A.pallet_recycling_flag WHEN 1 THEN '棧板回收' ELSE '' END  AS title1,
                                                  A.print_date ,
                                                  A.supplier_date,
                                                  A.area_arrive_code,
                                                  B.code_name as subpoena_category_name,
                                                  CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
                                                  CASE A.pricing_type WHEN '03' THEN '才數' ELSE '總板數' END  as  title2, 
                                                  CASE A.pricing_type WHEN '03' THEN  A.cbm ELSE A.plates  END  as plates,
                                                  A.pieces, 
                                                  A.check_number , 
                                                  'a'+ A.check_number +'a' as barcode,
                                                  A.receive_contact,
                                                  CASE WHEN A.receive_tel1_ext <> '' THEN A.receive_tel1 + ' #' +  A.receive_tel1_ext  ELSE A.receive_tel1 END  +  ' ' + A.receive_tel2 AS receive_tel,
                                                  A.receive_city + A.receive_area + A.receive_address as receive_address,
                                                  A.invoice_desc,
                                                  A.send_contact,
                                                  A.send_tel ,
                                                  A.send_city + A.send_area + A.send_address as send_address,
	                                              CASE A.pricing_type WHEN '01' THEN A.plates WHEN  '02' THEN A.pieces WHEN '03' THEN A.cbm ELSE A.plates END as pages
                                                  from tcDeliveryRequests A WITH(Nolock)
                                                  left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                                  left join tbCustomers C on C.customer_code = A.customer_code
                                                  left join tbItemCodes D on D.code_id  = A.pricing_type  and d.code_sclass  = 'PM'
                                                  where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                DataTable DT = dbAdapter.getDataTable(cmd);
                if (DT != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = DT.Clone();  //複製DT的結構
                    for (int i = 0; i <= DT.Rows.Count - 1; i++)
                    {
                        DT.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["barcode"].ToString()));
                        int pages = DT.Rows[i]["pages"] == DBNull.Value ? 1 : Convert.ToInt32(DT.Rows[i]["pages"]);
                        for (int j = 1; j <= pages; j++)
                        {
                            dt2.ImportRow(DT.Rows[i]);
                        }
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];
                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "ReelLabel3", ParName, ParValue, "dsRPT_DeliveryRequests", dt2);
                }


            }

        }


    }

    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {
        //Barcode test = new Barcode();
        //test. 
        using (var barcode = new Barcode()
        {
            
            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 250,
            Height = 50,
            LabelFont = new Font("verdana", 10f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            try
            {
                return barcode.Encode(TYPE.Codabar, content);
            }
            catch (Exception ex)
            {
                string errstr = ex.Message;
                return null ;
            }
            
        }
    }

}