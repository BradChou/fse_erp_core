﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default3 : System.Web.UI.Page
{
    dbAdapter _adp = new dbAdapter();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!this.IsPostBack)
        {
            //if (loginchk.IsLogin() == false)
            //{
            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='../login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            //    return;
            //}
            string get_data = Context.Items["TextData"].ToString();
            string[] arr_data = get_data.Split(';');
            string _check_number = Request.QueryString["check_number"] != null ? Request.QueryString["check_number"].ToString() : "";
            GetDefaultFile(arr_data[0], arr_data[1],arr_data[2]);
        }
    }

    protected void GetDefaultFile(string check_number,string data1, string data2)
    {
        //try
        //{
            using (SqlCommand cmd = new SqlCommand())
            {
                if (check_number != "")
                {
                    // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                    string wherestr = " and check_number in(" + check_number + ")";
                    string orderby = " order by  A.check_number";

                    cmd.CommandText = string.Format(@"Select
                                                          ROW_NUMBER() OVER(ORDER BY A.check_number,A.cdate) '序號',
                                                          A.order_number '訂單編號' , 
                                                          A.check_number '貨號' , 
                                                          A.receive_contact '收件人',  
                                                          ISNULL(A.pieces,0) '件數',
										                  A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
                                                          E.station_name  '到著站',
                                                          A.receive_tel1 '收件人電話',  
										                  A.collection_money '代收金額',
                                                          B.code_name '付款別',
                                                          A.SendPlatform '平臺名稱',
                                                          A.ArticleName '商品名稱',
                                                          A.invoice_desc '備註'  
                                                          from tcDeliveryRequests A
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          Left join tbSuppliers D with(nolock) on A.area_arrive_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                           Left join tbStation E with(nolock) on A.area_arrive_code = E.station_scode
                                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    { int totlecount = 0;

                    for (int x = 0; x < dt.Rows.Count; x++) {
                        totlecount += Convert.ToInt32(dt.Rows[x]["件數"].ToString());
                    }

                        if (dt != null && dt.Rows.Count > 0)
                        {
                        date.Text = data1 + "~"+ data2;
                        Label1.Text = string.Format(@"全合計：{0} 筆      總件數{1}", dt.Rows.Count, totlecount);
                            New_List.DataSource = dt;
                            New_List.DataBind();
                        }
                    }
                }
            }
        System.IO.StringWriter s_tw = new System.IO.StringWriter();
        HtmlTextWriter h_textw = new HtmlTextWriter(s_tw);
        Panel1.RenderControl(h_textw);//Name of the Panel
        System.IO.StringReader sr = new System.IO.StringReader(s_tw.ToString());

        System.IO.MemoryStream outputStream = new System.IO.MemoryStream();//要把PDF寫到哪個串流
        byte[] data = System.Text.Encoding.UTF8.GetBytes(s_tw.ToString());//字串轉成byte[]
        System.IO.MemoryStream msInput = new System.IO.MemoryStream(data);

        //iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10f, 10f, 10f, 0f);
        //iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, msInput, null, System.Text.Encoding.UTF8, new UnicodeFontFactory());
        //pdfDoc.Close();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=託運總表.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Write(pdfDoc);
        Response.BinaryWrite(data);
        Response.End();
        
    }
}