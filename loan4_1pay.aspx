﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loan4_1pay.aspx.cs" Inherits="loan4_1pay" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">
    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>
      


    <script type="text/javascript">
        $(function () {
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });
        });
    </script>

    <style type="text/css">
        .ui-datepicker select.ui-datepicker-month,
        .ui-datepicker select.ui-datepicker-year {
            color: black;
        }

        .ui-datepicker a {
            color: #0275d8;
        }

        .go-top {
            position: fixed;
            bottom: 2em;
            left: 2em;
            color: white;
            background-color: rgba(0, 0, 0, 0.3);
            font-size: 1em;
            padding: 0.5em;
            text-align: center;
            border-radius: 10px;
            z-index: 9999;
            display: none;
        }
        .hidecalendar table {
            display: none;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">付款</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">                                
                                <label>應付日期：</label>
                                <asp:Label ID="lb_duedate" runat="server"  ></asp:Label>   
                                <label>已繳金額：</label>
                                <asp:Label ID="lb_paid_price" runat="server"  ></asp:Label>                                 
                            </div>
                            <div class="form-group form-inline">                                
                                <label>實際付款日期：</label>
                                <asp:TextBox ID="paydate" runat="server" class="form-control date_picker _Loan_Start"  autocomplete="off"></asp:TextBox>
                            </div>
                            <div class="form-group form-inline">                                
                                <label>付款金額：</label>
                                <asp:TextBox ID="paid_price" runat="server" class="form-control " autocomplete="off" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                            </div>
                        </div>                        
                    </div>
                    <div class="form-group text-center">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="確定付款" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />                        
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
