﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_1.aspx.cs" Inherits="store1_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $("#<%=search.ClientID%>").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }
       
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">主線運費</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label>查詢區間：</label>
                    <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                    <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                    <label>場區：</label>
                     <asp:DropDownList ID="Area" runat="server" class="form-control " ></asp:DropDownList>
                    <label>日/夜：</label>
                     <asp:DropDownList ID="DayNight" runat="server" class="form-control">
                            <asp:ListItem Value="0">請選擇</asp:ListItem>
                            <asp:ListItem Value="1">日</asp:ListItem>
                            <asp:ListItem Value="2">夜</asp:ListItem>
                      </asp:DropDownList>
                    
                    <label>溫層：</label>
                    <asp:DropDownList ID="Temperature" runat="server" class="form-control " ></asp:DropDownList>
                    <label>供應商：</label>
                    <asp:DropDownList ID="supplier" runat="server" CssClass="form-control chosen-select"  ></asp:DropDownList>
                    </div>
                <div class="form-group form-inline">
                    <label>車牌：</label>
                    <asp:TextBox ID="car_license" runat="server" class="form-control "></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1"   />
                </div>
                <div class="form-group" style="float: right;">
                    <a href="store1_1_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>
                
                <p class="text-primary">※點選任一資料可查詢及修改</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide"> 
                        <th class="_th"></th>
                        <th class="_th">主線/爆量</th>
                        <th class="_th">日期</th>
                        <th class="_th">出場<br />時間</th>
                        <th class="_th">返場<br />時間</th>
                        <th class="_th">首店<br />時間</th>
                        <th class="_th">末店<br />時間</th>
                        <th class="_th">日/夜</th>
                        <th class="_th">溫層</th>
                        <th class="_th">車型<br />(計價車型)</th>
                        <th class="_th">店數</th>
                        <th class="_th">場區</th>
                        <th class="_th">公里</th>
                        <th class="_th">請款對象</th>
                        <th class="_th">路線</th>
                        <th class="_th">車號</th>
                        <th class="_th">司機</th>
                        <th class="_th">車行</th>
                        <th class="_th">供應商</th>
                        <th class="_th">承攬價</th>
                        <th class="_th">發包價</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server"　OnItemCommand="rep_ItemCommand">
                        <ItemTemplate>
                            <tr class ="paginate">      
                                <td>
                                    <a href="store1_1_edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>" class="btn btn-info  " id="updclick">修改</a>
                                    <%--<asp:Button ID="btnDel" runat="server" Text="刪除" CommandName="cmdDel" CssClass="btn btn-outline btn-danger" OnClientClick="return confirm('確定要刪除此筆資料嗎?');" CommandArgument ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>'/>--%>
                                    <asp:Button ID="btnDel" CssClass="btn btn-outline btn-danger" CommandName="cmdDel" runat="server" Text="刪除"  CommandArgument ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' OnClientClick="return confirm('確定要刪除此筆資料嗎?');"/>
                                </td>                  
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Items_Text").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.InDate","{0:yyyy/MM/dd}").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.starttime","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.endtime","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.startstore_time","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.endstore_time","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DayNigh_text").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Temperature_Text").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cartype_text").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Stores").ToString())%></td>
                                <td ><%# Func.GetRow("code_name", "ttItemCodesFieldArea", "", "seq=@seq", "seq", Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Area").ToString()))%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.milage").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.RequestObject").ToString())%></td>
                                <%--<td ><%# Func.GetRow("code_name", "ttItemCodesRoad", "", "seq=@seq", "seq", Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Route").ToString()))%></td>--%>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Route_name").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.CarRetailerName").ToString())%></td>
                                <%--<td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.PaymentObject").ToString())%></td>--%>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.SupplierName").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income").ToString())%></td>
                                <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="16" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

