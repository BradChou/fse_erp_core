﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterSystem.master" AutoEventWireup="true" CodeFile="LT_system_6.aspx.cs" Inherits="LT_system_6" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }
        .checkbox, .radio{
            display: inherit;
        }

        .radio label {
           
            text-align :left ;
        }

        ._add_table {
            width: 90%;
        }

        ._add_title {
            width: 9%;
            padding: 5px;
        }

        ._add_data {
            width: 41%;
        }

        ._add_btn {
            text-align: center;
        }

        ._foradd {
            margin: 5px 20px;
        }

        ._setadd {
            right: 10px;
            text-align: right;
        }

        ._hr {
            width: 100%;
            display: inline-table;
        }

        ._btnsh {
            margin: 10px auto;
        }
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">帳密管理</h2>

            <asp:Panel ID="pan_add" runat="server" Visible="false" CssClass="col-lg-12 form-group form-inline">

                <table class="_add_table">
                    <tr>
                        <th class="_add_title">
                            <label for="inputFirstName" class="_tip_important">管理單位</label></th>
                        <td class="_add_data">
                            <div class="margin-right-15 templatemo-inline-block">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="rbmanager_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" OnSelectedIndexChanged="rbmanager_type_SelectedIndexChanged" AutoPostBack="true" CssClass="radio radio-success">
                                            <asp:ListItem Value ="1" >管理者</asp:ListItem>
                                            <asp:ListItem Value ="2" >一般員工</asp:ListItem>
                                            <asp:ListItem Value ="5" >全速配客戶</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                        <th class="_add_title">
                            <label for="inputLastName" class="_tip_important">帳　　號</label></th>
                        <td class="_add_data">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" >
                                <ContentTemplate>
                                    <div id="div1" runat="server" class="form-group">
                                        <asp:TextBox ID="account1" runat="server" class="form-control" Enabled="False"></asp:TextBox>
                                    </div>
                                    <div id="div2" runat="server" class="form-group" visible="false">
                                        <asp:TextBox ID="account2" runat="server" class="form-control" Enabled="False"></asp:TextBox>
                                        <asp:DropDownList ID="dlmaster2" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="dlmaster2_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <div id="div3" runat="server" class="form-group" visible="false">
                                        <asp:DropDownList ID="dlmaster3" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="dlmaster3_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:TextBox ID="account3" runat="server" class="form-control" Enabled="False"></asp:TextBox>
                                    </div>

                                    <div id="div4" runat="server" class="form-group" visible="false">
                                        <asp:DropDownList ID="head_code" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="head_code_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="body_code" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="body_code_SelectedIndexChanged"></asp:DropDownList>
                                    </div>


                                     <div id="div5" runat="server" class="form-group" visible="false">
                                        <asp:DropDownList ID="station_code" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="station_code_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="customer_code" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="customer_code_SelectedIndexChanged"></asp:DropDownList>
                                    </div>





                                    <asp:Label ID="lbErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </ContentTemplate>
                                
                            </asp:UpdatePanel>
                            <asp:Label ID="lbCustomer_name" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <th class="_add_title">
                            <label for="inputLastName" class="_tip_important">職　　稱</label></th>
                        <td class="_add_data">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server"  >
                                <ContentTemplate>
                                    <asp:TextBox ID="job_title" runat="server" class="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req_job" runat="server" ControlToValidate="job_title" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入職稱</asp:RequiredFieldValidator>
                                </ContentTemplate>
                               
                            </asp:UpdatePanel>
                        </td>
                        <th class="_add_title">
                            <label for="inputLastName" class="_tip_important">人　　員</label></th>
                        <td class="_add_data">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server"  >
                                <ContentTemplate>
                                    <asp:TextBox ID="user_name" runat="server" class="form-control"></asp:TextBox>
                                    <asp:DropDownList ID="ddl_user" runat="server" class="form-control chosen-select" Visible="False"></asp:DropDownList>
                                    <asp:Label ID="lbuserErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                    <%--<asp:RequiredFieldValidator ID="req_user" runat="server" ControlToValidate="user_name" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入人員</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator ID="req_ddluser" runat="server" ControlToValidate="ddl_user" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請選取人員</asp:RequiredFieldValidator>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <th class="_add_title">
                            <label for="inputLastName" class="_tip_important">信　　箱</label></th>
                        <td class="_add_data" >
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server" >
                                <ContentTemplate>
                                    <asp:TextBox ID="email" runat="server" class="form-control"></asp:TextBox>
                                    &nbsp;<%--<asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="email" ForeColor="Red" ValidationGroup="validate">請輸入信箱</asp:RequiredFieldValidator></td>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="email" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                    </asp:RegularExpressionValidator>--%>
                                </ContentTemplate>
                               
                            </asp:UpdatePanel>
                        <th class="_add_title">
                            <label class="_tip_important">是否生效：請至人員基本資料修改</label>    
                        <td class="_add_data">
                            <asp:RadioButtonList ID="rb_Active" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" Enabled="false">
                                <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                                <asp:ListItem Value="0">否</asp:ListItem>
                            </asp:RadioButtonList>    
                        </td>
                    </tr>
                    <tr >
                          <th class="_add_title">
                              <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                        <td class="_add_data">
                            <asp:RadioButtonList ID="Chk_mail" runat="server" ToolTip="請設定是否寄密碼確認信!" RepeatDirection="Horizontal" CssClass="radio radio-success">
                                <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                                <asp:ListItem Value="0">否</asp:ListItem>
                            </asp:RadioButtonList>    
                        </td>
                    </tr>



                    <tr>
                        <td class="_add_btn" colspan="4">
                            <asp:Button ID="btnew" class="btn btn-primary _foradd" runat="server" Text="確 認" OnClick="btnew_Click" ValidationGroup="validate" />
                            <asp:Button ID="btn_Cancel" class="btn btn-default _foradd" runat="server" Text="取 消" OnClick="btn_Cancel_Click" />
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                </table>


            </asp:Panel>
           



            <asp:Panel ID="pan_list" runat="server" CssClass="col-lg-12 form-inline form-group">

                <div class="row col-lg-6">

                    查詢條件： <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 帳號 或 人員"></asp:TextBox>

                    <asp:RadioButtonList ID="rbl_typeForSh" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio radio-success">
                        <asp:ListItem Value="1">管理者</asp:ListItem>
                        <asp:ListItem Value="2">一般員工</asp:ListItem>
                        <asp:ListItem Value="5">全速配客戶</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Button ID="btn_Search" class="btn btn-primary _btnsh" runat="server" Text="查 詢" OnClick="btn_Search_Click" />
                </div>
                <div class="row col-lg-6 _setadd">
                    <asp:Button ID="btn_Add" class="btn btn-warning" runat="server" Text="新 增" OnClick="btn_Add_Click" />
                </div>
                <hr class="_hr" />
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th>管理單位</th>
                        <th>公司別</th>
                        <th>職稱</th>
                        <th>人員</th>
                        <th>帳號</th>
                        <%--<th>密碼</th>--%>
                        <th>信箱</th>
                        <th>設定人員</th>
                        <th>修改時間</th>
                        <th>狀態</th>
                        <th class="td_th td_edit">功　　能</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td data-th="管理單位">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_id").ToString())%>' />
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.manager_unit").ToString())%>
                                </td>
                                <td data-th="公司別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_shortname").ToString())%> </td>
                                <td data-th="職稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.job_title").ToString())%> </td>
                                <td data-th="人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="帳號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_code").ToString())%></td>
                                <%--<td data-th="密碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.password").ToString())%></td>--%>
                                <td data-th="信箱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_email").ToString())%></td>
                                <td data-th="設定人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.uuser").ToString())%></td>
                                <td data-th="修改時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.statustext").ToString())%></td>
                                <td class="td_data td_edit" data-th="狀態">
                                    <asp:Button ID="btn_reset" CssClass="btn btn-info " CommandName="reset" runat="server" Text="重置密碼" OnClientClick="return confirm('您確認要重置該人員的密碼嗎?');"  />
                                    <asp:Button ID="btn_edit" CssClass="btn btn-warning " CommandName="Mod" runat="server" Text="編輯"  />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="10" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span> 筆
                </div>
            </asp:Panel>
        </div>


        <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="HiddenField1" PopupControlID="UpdatePanel1" BackgroundCssClass="modalBackground" DropShadow="true"></asp:ModalPopupExtender>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class=" bg-info">
                    <div class="col-lg-12 col-md-12 form-group form-inline">
                        <label for="inputLastName">帳號：</label>
                        <asp:Label ID="lbaccount_code" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12 col-md-12 form-group form-inline">
                        <label for="inputLastName">密碼：</label>
                        <asp:Label ID="lbpassword" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-12 col-md-12 form-group form-inline">
                        <label for="inputLastName">信箱：</label>
                        <asp:Label ID="lbmail" runat="server"></asp:Label>
                    </div>
                    <div style="text-align: center">
                        <asp:Button ID="btMail" CssClass=" btn-sm" runat="server" Text="確認寄出" OnClick="btMail_Click" />
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="HiddenField1" runat="server" />
    </div>
</asp:Content>

