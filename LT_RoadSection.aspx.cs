﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using DocumentFormat.OpenXml.Presentation;
using System.Windows.Forms;

public partial class LT_RoadSection : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            DDLSet();
            DefaultData();
        }
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    protected void DDLSet()
    {
     

        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        ddl_City.DataSource = dbAdapter.getDataTable(cmd1);
        ddl_City.DataValueField = "city";
        ddl_City.DataTextField = "city";
        ddl_City.DataBind();
        ddl_City.Items.Insert(0, new ListItem("請選擇", ""));
        ddl_area.Items.Insert(0, new ListItem("請選擇", ""));


        sta.DataSource = Utility.getArea_Arrive_Code(true);
        sta.DataValueField = "station_scode";
        sta.DataTextField = "showsname";
        sta.DataBind();
        sta.Items.Insert(0, new ListItem("請選擇", ""));
    }

    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["zip"] != null) //郵遞區號
            {
                tb_searchzip.Text = Request.QueryString["zip"];
                querystring += "&zip=" + tb_searchzip.Text;
            }
            if (Request.QueryString["keysaddre"] != null) //地址關鍵字
            {
                tb_searchaddress.Text = Request.QueryString["keysaddre"];
                querystring += "&keysaddre=" + tb_searchaddress.Text;
            }
            if (Request.QueryString["keyspeo"] != null) //司機、工號關鍵字
            {
                tb_searchpeo.Text = Request.QueryString["keyspeo"];
                querystring += "&keyspeo=" + tb_searchaddress.Text;
            }
            if (Request.QueryString["type"] != null) //號碼類別
            {
                type.SelectedValue  = Request.QueryString["type"];
                querystring += "&type=" + type.SelectedItem.Value;
            }

            #endregion



            string strWhere = "";

            strSQL = @"SELECT ROW_NUMBER() OVER(order by zip, city,area,seq DESC) AS num
                             ,A.seq
                             ,A.city
                             ,A.Area
                             ,A.Zip
                             ,A.village
                             ,A.neighborS
                             ,A.neighborE
                             ,A.road
                             ,A.section
                             ,A.laneS
                             ,A.laneE
                             ,A.numberS
                             ,A.numberE
                             ,A.collecting_area
                             ,A.collecting_areaR
                             ,A.arrive_distribution
                             ,A.arrive_stack
                             ,A.MDarrive_distribution
                             ,A.MDarrive_stack
                             ,A.DDarrive_distribution
                             ,A.DDarrive_stack
                             ,A.HDarrive_distribution
                             ,A.HDarrive_stack
                             ,A.Udate
                             ,A.Uuser
                             ,A.MDarrive_distribution
                             ,A.MDarrive_stack
                             ,A.Driver
                             ,A.Emp_code
                            
                             ,CASE A.numbertype WHEN '0' THEN '全'  WHEN '1' THEN '單'   ELSE '雙' END numbertype 
                             ,CASE 
                                    WHEN E.station_name IS NULL THEN ''
                                    WHEN CHARINDEX('*', E.station_name) > 0 THEN  'V'
                                    END  LanYun
                             ,CASE 
                                    WHEN emp_name IS NULL THEN A.driver
                                    ELSE emp_name
                                    END  dri_name
                           
                             ,D.user_name 
                             ,E.station_name 
                         
                         FROM tcRoadSection A With(Nolock)
                    LEFT JOIN tbEmps B With(Nolock) ON A.emp_code = B.emp_code 
                    LEFT JOIN tbStation  E With(Nolock) ON A.sta_code = E.station_scode
                    LEFT JOIN tbAccounts  D With(Nolock) ON D.account_code = A.Cuser
                      where 1=1   ";



            #region 查詢條件
            if (Request.QueryString["keysaddre"] != null) //地址關鍵字
            {
                strWhere += " AND (City like @key or  area like @key  or village like @key  or  (A.road + A.section + '段') like @key  ) ";
                cmd.Parameters.AddWithValue("@key", "%" + tb_searchaddress.Text.Trim() + "%");
               
               

            }

            if (Request.QueryString["zip"] != null) //郵遞區號
            {
                strWhere += " AND (zip = @zip ) ";
          
                cmd.Parameters.AddWithValue("@zip", tb_searchzip.Text.Trim());


            }


            if (Request.QueryString["keyspeo"] != null) //司機、工號關鍵字
            {
                strWhere += " AND (CASE  WHEN emp_name IS NULL THEN A.driver ELSE emp_name END  like @dri_name or A.Emp_code=@Emp_code) ";
                cmd.Parameters.AddWithValue("@dri_name", "%" + tb_searchpeo.Text.Trim() + "%");
                cmd.Parameters.AddWithValue("@Emp_code", tb_searchpeo.Text.Trim());


            }
            if (Request.QueryString["type"] != null) //號碼類別
            {
                strWhere += " AND (Numbertype=@Numbertype) ";
                cmd.Parameters.AddWithValue("@Numbertype", type.SelectedItem.Value );
            }

            #endregion

            cmd.CommandText = string.Format(strSQL + " {0} {1}", strWhere , "order by zip, city,area,seq DESC");


            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "";

        if (!string.IsNullOrEmpty(tb_searchzip.Text))
        {
            str_Query += "&zip=" + tb_searchzip.Text;
        }

        if (!string.IsNullOrEmpty(tb_searchaddress.Text))
        {
            str_Query += "&keysaddre=" + tb_searchaddress.Text;
        }
        if (!string.IsNullOrEmpty(tb_searchpeo.Text))
        {
            str_Query += "&keyspeo=" + tb_searchpeo.Text;
        }
        if(type.SelectedItem.Value  != "")
        {
            str_Query += "&type=" + type.SelectedItem.Value ;
        }



        Response.Redirect(ResolveUrl("~/LT_RoadSection.aspx?" + str_Query));
    }



    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        dlarea = ddl_area;

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
        }
        Zip.Text = "";
    }
    protected void Area_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_area.SelectedValue.ToString() != "")
        {
            using (SqlCommand cmda = new SqlCommand())
            {
                DataTable dta = new DataTable();
                cmda.Parameters.AddWithValue("@city", ddl_City.SelectedValue);
                cmda.Parameters.AddWithValue("@area", ddl_area.SelectedValue);
                cmda.CommandText = "Select top 1 Zip,station_code  from [ttArriveSitesScattered]  With(Nolock) where post_city=@city  AND post_area = @area  ";
                dta = dbAdapter.getDataTable(cmda);
                if (dta.Rows.Count > 0)
                {
                    Zip.Text = dta.Rows[0]["Zip"].ToString();
                    sta.SelectedValue = dta.Rows[0]["station_code"].ToString();
                }
            }
        

            }
        else
        {
            Zip.Text = "";
            sta.SelectedValue = "";
        }




    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";



        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;


        string Err = "";
        string StrWh = "";
        if (laneS.Text != "" && laneE.Text != "" && Convert.ToInt32(laneS.Text) >= Convert.ToInt32(laneE.Text))
        {
            Err = Err + "巷(起) 必須小於 巷(迄)\n";
        }
        if (neighborS.Text != "" && neighborE.Text != "" && Convert.ToInt32(neighborS.Text) >= Convert.ToInt32(neighborE.Text))
        {
            Err = Err + "鄰(起) 必須小於 鄰(迄)\n";
        }
        if (NumberS.Text != "" && NumberE.Text != "" && Convert.ToInt32(NumberS.Text) >= Convert.ToInt32(NumberE.Text))
        {
            Err = Err + "號(起) 必須小於 號(迄)\n";
        }




        using (SqlCommand cmda = new SqlCommand())
        {

            if (road.Text != "")
            {
                StrWh = StrWh + "And (road = @road)";
                cmda.Parameters.AddWithValue("@road", road.Text);
            }

            if (village.Text != "")
            {
                StrWh = StrWh + "And (village = @village)";
                cmda.Parameters.AddWithValue("@village", village.Text);
            }

            if (section.Text != "")
            {
                string Section = "";
                Section = section.Text;
                Section = Section.Replace("1", "一");
                Section = Section.Replace("2", "二");
                Section = Section.Replace("3", "三");
                Section = Section.Replace("4", "四");
                Section = Section.Replace("5", "五");
                Section = Section.Replace("6", "六");
                Section = Section.Replace("7", "七");
                Section = Section.Replace("8", "八");
                Section = Section.Replace("9", "九");
                StrWh = StrWh + "And (section = @section)";
                cmda.Parameters.AddWithValue("@section", Section);
            }

            if (neighborS.Text != "" && neighborE.Text != "")
            {
                StrWh = StrWh + "And (@neighborS between  neighborS   and  neighborE  or   @neighborE between   neighborS and neighborE )";
                cmda.Parameters.AddWithValue("@neighborS", neighborS.Text);
                cmda.Parameters.AddWithValue("@neighborE", neighborE.Text);
            }
            else if (neighborS.Text != "" && neighborE.Text == "")
            {
                StrWh = StrWh + "And (@neighborS <  neighborS  or   @neighborS< neighborE)";
                cmda.Parameters.AddWithValue("@neighborS", neighborS.Text);
            }
            else if (neighborS.Text == "" && neighborE.Text != "")
            {
                StrWh = StrWh + "And (@neighborE >  neighborS  or  @neighborE> neighborE)";
                cmda.Parameters.AddWithValue("@neighborE", neighborE.Text);
            }

            if (laneS.Text != ""   && laneE.Text != "")
            {
                StrWh = StrWh + "And (@laneS between  laneS   and  laneE  or   @laneE between   laneS and laneE )";
                cmda.Parameters.AddWithValue("@laneS", laneS.Text);
                cmda.Parameters.AddWithValue("@laneE", laneE.Text);
            }
            else if (laneS.Text != "" && laneE.Text == "")
            {
                StrWh = StrWh + "And (@laneS <  laneS  or   @laneS< laneE)";
                cmda.Parameters.AddWithValue("@laneS", laneS.Text);
            }
            else if (laneS.Text == "" && laneE.Text != "")
            {
                StrWh = StrWh + "And (@laneE >  laneS  or  @laneS> laneE)";
                cmda.Parameters.AddWithValue("@laneE", laneE.Text);
            }

            if (NumberS.Text != "" && NumberE.Text != "")
            {
                if (numbertype.SelectedItem.Value == "0")
                {
                    StrWh = StrWh + "And ((@NumberS between NumberS and NumberE)   or  (@NumberE  between NumberS and NumberE ))";
                }
                if (numbertype.SelectedItem.Value == "1")
                {
                    StrWh = StrWh + " And ((Numbertype=0 and  (@NumberS between NumberS and NumberE   or  @NumberE  between NumberS and NumberE))";
                    StrWh = StrWh + " or (And Numbertype=1 and  (@NumberS between NumberS and NumberE   or   @NumberE  between NumberS and NumberE)   )))) ";
                }
                if (numbertype.SelectedItem.Value == "2")
                {
                    StrWh = StrWh + " And ((Numbertype=0 and  (@NumberS between NumberS and NumberE   or  @NumberE  between NumberS and NumberE))";
                    StrWh = StrWh + " or (And Numbertype=2 and  (@NumberS between NumberS and NumberE   or   @NumberE  between NumberS and NumberE)   )))) ";
                }
                cmda.Parameters.AddWithValue("@NumberS", NumberS.Text);
                cmda.Parameters.AddWithValue("@NumberE", NumberE.Text);
            }
            else if (NumberS.Text == "" && NumberE.Text != "")
            {
                if (numbertype.SelectedItem.Value == "0")
                {
                    StrWh = StrWh + "And (Numbertype=0 and  (@NumberE >= NumberS or  @NumberE >= NumberE)) ";
                }
                if (numbertype.SelectedItem.Value == "1")
                {
                    StrWh = StrWh + "And ((Numbertype=0 and  (@NumberE >= NumberS or  @NumberE >= NumberE)) ";
                    StrWh = StrWh + " or (Numbertype=1 and  (@NumberE >= NumberS or  @NumberE >= NumberE)))) ";
                }
                if (numbertype.SelectedItem.Value == "2")
                {
                    StrWh = StrWh + "And ((Numbertype=0 and  (@NumberE >= NumberS or  @NumberE >= NumberE)) ";
                    StrWh = StrWh + " or (Numbertype=2 and  (@NumberE >= NumberS or  @NumberE >= NumberE)))) ";
                }

                cmda.Parameters.AddWithValue("@NumberE", NumberE.Text);
            }
            else if (NumberS.Text != "" && NumberE.Text == "")
            {
                if (numbertype.SelectedItem.Value == "0")
                {
                    StrWh = StrWh + "And (Numbertype=0 and  (@NumberS <= NumberS or  @NumberS <= NumberE)) ";
                }
                if (numbertype.SelectedItem.Value == "1")
                {
                    StrWh = StrWh + "And ((Numbertype=0 and  (@NumberS <= NumberS or  @NumberS <= NumberE)) ";
                    StrWh = StrWh + " or (Numbertype=1 and  (@NumberS <= NumberS or  @NumberS <= NumberE)))) ";
                }
                if (numbertype.SelectedItem.Value == "2")
                {
                    StrWh = StrWh + "And ((Numbertype=0 and  (@NumberS <= NumberS or  @NumberS <= NumberE)) ";
                    StrWh = StrWh + " or (Numbertype=2 and  (@NumberS <= NumberS or  @NumberS <= NumberE)))) ";
                }

                cmda.Parameters.AddWithValue("@NumberS", NumberS.Text);
            }




            if (lbl_title.Text.Contains("編輯"))
            {
                StrWh = StrWh + "And (seq <> @seq)";
                cmda.Parameters.AddWithValue("@seq", lbl_id.Text);
            }

            DataTable dta = new DataTable();
            cmda.CommandText = string.Format(@"
                                Select top 1 seq
                                from tcRoadSection With(Nolock) 
                                where 1=1  and  City=@City  and  Area=@Area   {0}  ", StrWh);

         

            cmda.Parameters.AddWithValue("@City", ddl_City.SelectedItem.Value);
            cmda.Parameters.AddWithValue("@Area", ddl_area.SelectedItem.Value);
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                Err = Err + "此路段區間重覆!!\n";
            }
        }



       









        #region 存檔

        if (IsOk && Err == "")
        {
            string strUser = string.Empty;
            if (Session["account_code"] != null) strUser = Session["account_code"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {

                string Section = "";
                Section = section.Text;
                Section = Section.Replace("1", "一");
                Section = Section.Replace("2", "二");
                Section = Section.Replace("3", "三");
                Section = Section.Replace("4", "四");
                Section = Section.Replace("5", "五");
                Section = Section.Replace("6", "六");
                Section = Section.Replace("7", "七");
                Section = Section.Replace("8", "八");
                Section = Section.Replace("9", "九");

                if (IsEdit)
                {
                    cmd.Parameters.AddWithValue("@City", ddl_City.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Area", ddl_area.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Zip", Zip.Text);
                    cmd.Parameters.AddWithValue("@village", village.Text);
                    cmd.Parameters.AddWithValue("@road", road.Text);
                    cmd.Parameters.AddWithValue("@section", Section);
                    if(neighborS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@neighborS", neighborS.Text);
                    }
                    if (neighborE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@neighborE", neighborE.Text);
                    }
                    if (laneS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@laneS", laneS.Text);
                    }
                    if (laneE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@laneE", laneE.Text);
                    }
                    if (NumberS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@NumberS", NumberS.Text);
                    }
                    if (NumberE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@NumberE", NumberE.Text);
                    }

              
                    cmd.Parameters.AddWithValue("@numbertype", numbertype.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@sta_code", sta.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Emp_code", Emp_code.Text);
                    cmd.Parameters.AddWithValue("@Driver", Driver.Text);
                    cmd.Parameters.AddWithValue("@collecting_area", collecting_area.Text);
                    cmd.Parameters.AddWithValue("@collecting_areaR", collecting_areaR.Text);
                    cmd.Parameters.AddWithValue("@arrive_distribution", arrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@arrive_stack", arrive_stack.Text);
                    cmd.Parameters.AddWithValue("@MDarrive_distribution", MDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@MDarrive_stack", MDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@DDarrive_distribution", DDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@DDarrive_stack", DDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@HDarrive_distribution", HDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@HDarrive_stack", HDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm("tcRoadSection", cmd);
                    dbAdapter.execNonQuery(cmd);
                    SetEditOrAdd(0);
                    RetuenMsg("修改成功!");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@City", ddl_City.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Area", ddl_area.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Zip", Zip.Text);
                    cmd.Parameters.AddWithValue("@village", village.Text);
                    cmd.Parameters.AddWithValue("@road", road.Text);
                    cmd.Parameters.AddWithValue("@section", Section);
                    if (neighborS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@neighborS", neighborS.Text);
                    }
                    if (neighborE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@neighborE", neighborE.Text);
                    }
                    if (laneS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@laneS", laneS.Text);
                    }
                    if (laneE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@laneE", laneE.Text);
                    }
                    if (NumberS.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@NumberS", NumberS.Text);
                    }
                    if (NumberE.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@NumberE", NumberE.Text);
                    }

                    cmd.Parameters.AddWithValue("@numbertype", numbertype.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@sta_code", sta.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@Emp_code", Emp_code.Text);
                    cmd.Parameters.AddWithValue("@Driver", Driver.Text);
                    cmd.Parameters.AddWithValue("@collecting_area", collecting_area.Text);
                    cmd.Parameters.AddWithValue("@collecting_areaR", collecting_areaR.Text);
                    cmd.Parameters.AddWithValue("@arrive_distribution", arrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@arrive_stack", arrive_stack.Text);
                    cmd.Parameters.AddWithValue("@MDarrive_distribution", MDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@MDarrive_stack", MDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@DDarrive_distribution", DDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@DDarrive_stack", DDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@HDarrive_distribution", HDarrive_distribution.Text);
                    cmd.Parameters.AddWithValue("@HDarrive_stack", HDarrive_stack.Text);
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", strUser);
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.CommandText = dbAdapter.genInsertComm("tcRoadSection", true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                    SetEditOrAdd(0);
                    RetuenMsg("新增成功!");
                }



            }
        }
        else
        {
            RetuenMsg("請確認路段是否重複!!!");
        }
        #endregion
    }



    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
       
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "路段維護-新增";
                pan_form.Visible = true;
                pan_list.Visible = false;
                lbl_id.Text = "0";
                sta.SelectedValue = "";
                ddl_City.SelectedValue = "";
                ddl_area.SelectedValue = "";
                numbertype.SelectedValue = "0";
                Zip.Text = "";
                village.Text = "";
                road.Text = "";
                section.Text = "";
                neighborS.Text = "";
                neighborE.Text = "";
                laneS.Text = "";
                laneE.Text = "";
                NumberS.Text = "";
                NumberE.Text = "";
                Emp_code.Text = "";
                Driver.Text = "";
                collecting_area.Text = "";
                collecting_areaR.Text = "";
                arrive_distribution.Text = "";
                arrive_stack.Text = "";
                MDarrive_distribution.Text = "";
                MDarrive_stack.Text = "";
                DDarrive_stack.Text = "";
                DDarrive_distribution.Text = "";
                HDarrive_distribution.Text = "";
                HDarrive_stack.Text = "";

                ddl_area.Items.Clear();
              
                #endregion

                break;
            case 2:
 
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "路段維護-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = "";

                        strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY A.city,A.area,A.Zip,A.seq) AS num
                             ,A.seq
                             ,A.city
                             ,A.Area
                             ,A.Zip
                             ,A.village
                             ,A.neighborS
                             ,A.neighborE
                             ,A.road
                             ,A.section
                             ,A.laneS
                             ,A.laneE
                             ,A.numberS
                             ,A.numberE
                             ,A.collecting_area
                             ,A.collecting_areaR
                             ,A.arrive_distribution
                             ,A.arrive_stack
                             ,A.MDarrive_distribution
                             ,A.MDarrive_stack
                             ,A.DDarrive_distribution
                             ,A.DDarrive_stack
                             ,A.HDarrive_distribution
                             ,A.HDarrive_stack
                             ,A.Udate
                             ,A.Uuser
                             ,A.MDarrive_distribution
                             ,A.MDarrive_stack
                             ,A.Driver
                             ,A.Emp_code
                             ,A.numbertype 
                             ,A.sta_code
                         FROM tcRoadSection A With(Nolock)
              
                      where seq=@seq   ";


                        cmd.Parameters.AddWithValue("@seq", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                lbl_id.Text = id.ToString();
                                sta.SelectedValue = row["sta_code"].ToString() ;
                                ddl_City.SelectedValue = row["city"].ToString() ;
                                City_SelectedIndexChanged(ddl_City, null);
                                ddl_area.SelectedValue = row["area"].ToString() ;
                                numbertype.SelectedValue = row["numbertype"].ToString();
                                Zip.Text = row["Zip"].ToString();
                                village.Text = row["village"].ToString();
                                road.Text = row["road"].ToString();
                                section.Text = row["section"].ToString();
                                neighborS.Text = row["neighborS"].ToString();
                                neighborE.Text = row["neighborE"].ToString();
                                laneS.Text = row["laneS"].ToString();
                                laneE.Text = row["laneE"].ToString();
                                NumberS.Text = row["NumberS"].ToString();
                                NumberE.Text = row["NumberE"].ToString();
                                Emp_code.Text = row["Emp_code"].ToString();
                                Driver.Text = row["Driver"].ToString();
                                collecting_area.Text = row["collecting_area"].ToString();
                                collecting_areaR.Text = row["collecting_areaR"].ToString();
                                arrive_distribution.Text = row["arrive_distribution"].ToString();
                                arrive_stack.Text = row["arrive_stack"].ToString();
                                MDarrive_distribution.Text = row["MDarrive_distribution"].ToString();
                                MDarrive_stack.Text = row["MDarrive_stack"].ToString();
                                DDarrive_stack.Text = row["DDarrive_stack"].ToString();
                                DDarrive_distribution.Text = row["DDarrive_distribution"].ToString();
                                HDarrive_distribution.Text = row["HDarrive_distribution"].ToString();
                                HDarrive_stack.Text = row["HDarrive_stack"].ToString();
                            }
                        }
                    }

                    pan_form.Visible = true;
                    pan_list.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "路段維護";
                pan_form.Visible = false;
                pan_list.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }










}