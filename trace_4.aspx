﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="trace_4.aspx.cs" Inherits="trace_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style type="text/css">
        input[type=radio] {
            display: inline-block;
        }

        .radio label {
            margin-right: 15px;
            text-align :left ;
        }

        .radio {
            padding-left: 5px;
        }
        .date_picker{width:105px !important;}
        
        .table__scan,.pager{width:55%;}
        .table__scan tr:hover {
            background-color: lightyellow;
        }
        ._supplier {
            cursor: pointer;
            padding: 3px;
            color: #337ab7;
        }
        ._supplier:hover {            
            color: #2c2c7f;
        }
        .td_th{
            text-align:center;
            padding:5px;
        }
        .td_no{width:15%;}
        .td_supplier{width:30%;}
        .td_num_all{width:15%;}
        .td_num_yes{width:15%;}
        .td_num_no{width:15%;}
        .td_rate{width:20px;}
        .auto-style1 {
            height: 84px;
        }
    </style>
    <script type="text/javascript">       
        $(document).ready(function () {
            
            $("._supplier").click(function () {
               
                var _link = "trace_4_detail.aspx?";
                _link = _link + "type=" + $("._typeset").find("input[type=radio]:checked").val();
                _link = _link + "&dates=" + $(".date_s").val();
                _link = _link + "&datee=" + $(".date_e").val();              
                //_link = _link + "&supplier=" + $(this).parent(".td_supplier").find(".supp_info").val() ;
                _link = _link + "&supplier=" + $(this).parent("td").siblings(".td_supplier").find(".supp_info").val();
                var _scan = $(this).attr("_scan");
                _link = _link + "&scan=" + _scan;
                
                window.open(_link);

            });


        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <div class="templatemo-login-form">
                <div class="row form-group">
                    <div class="col-lg-12 form-group form-inline">
                        <div class="col-lg-5 _typeset">
                            <label for="inputFirstName">查詢類別：</label>
                            <asp:RadioButtonList ID="rbl_type" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" ToolTip="請選擇查詢類型!" style="left: 0px; top: 0px">
                                <asp:ListItem Selected="True" Value="0">簽單</asp:ListItem>
                                <asp:ListItem Value="1">回單</asp:ListItem>
                                <asp:ListItem Value="2">棧板回收</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class=" col-lg-5">
                            <label for="inputLastName">發送日期</label>
                            <asp:TextBox ID="dateStart" runat="server" class="form-control date_picker date_s"></asp:TextBox>                            
                            ~                             
                            <asp:TextBox ID="dateEnd" runat="server" class="form-control date_picker date_e"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="reg_dateStart" runat="server" ControlToValidate="dateStart" ForeColor="Red" ValidationGroup="validate">請選擇發送日期</asp:RequiredFieldValidator>
                            <asp:Button ID="btnSearch" runat="server" Text="查 詢" class="btn btn-primary" ValidationGroup="validate" OnClick="btnSearch_Click" />
                        </div>
                        <div class="col-lg-push-1 form-group form-inline text-right">
                            <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                        </div>
                    </div>

                </div>
            </div>
            <hr>
            <p class="text-primary">查詢類別：<asp:Label ID="lbType" runat="server" ></asp:Label> 發送日期：<asp:Label ID="lbDate" runat="server"></asp:Label>
            </p>
            <table class="table table-striped table-bordered templatemo-user-table table__scan">
                <tr>
                    <th class="td_th">No.</th>
                    <th class="td_th">區配商</th>
                    <th class="td_th">應掃筆數</th>
                    <th class="td_th">已掃筆數</th>
                    <th class="td_th">未掃筆數</th>
                    <th class="td_th">掃描率</th>
                </tr>
                <asp:Repeater ID="list_scan" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="td_data td_no" data-th="No."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.no").ToString())%></td>
                            <td class="td_data td_supplier" data-th="區配商">
                                <span >
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%>
                                </span>
                                <input  class="supp_info" style="display:none;" value="<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_code").ToString())%>" />
                                
                            </td>
                            <td class="td_data td_num_all" data-th="應掃筆數">
                                <span class="_supplier" _scan= "all">
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num_all").ToString())%>
                                </span>
                            </td>
                            <td class="td_data td_num_yes" data-th="已掃筆數">
                                <span class="_supplier" _scan= "yes">
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num_yes").ToString())%>
                                </span>
                            </td>
                            <td class="td_data td_num_no" data-th="未掃筆數">
                                <span class="_supplier" _scan= "no">
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num_no").ToString())%>
                                </span>
                            </td>
                            <td class="td_data td_rate" data-th="掃描率">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rate").ToString())%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <% if (list_scan.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="6" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
                
            </table>
            <%--<div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>--%>
        </div>
    </div>
</asp:Content>

