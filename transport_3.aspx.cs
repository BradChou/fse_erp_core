﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.ServiceModel.Channels;
using System.Windows.Forms;

public partial class transport_3 : System.Web.UI.Page
{
    public int receiver_id
    {
        get { return (int)ViewState["receiver_id"]; }
        set { ViewState["receiver_id"] = value; }
    }

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }

    }
    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            Less.Text = Less_than_truckload;

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別
            ssMaster_code = Session["master_code"].ToString();


            if (Less_than_truckload == "0")
            {
                if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
                    ssMaster_code = "0000000";
                #region 
                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = "";
                    if (ssManager_type == "3" || ssManager_type == "4" || ssManager_type == "5")
                    {
                        if (ssMaster_code.Substring(3, 4) == "0000")
                        {
                            cmd.Parameters.AddWithValue("@master_code", ssMaster_code.Substring(0, 3));
                            wherestr = " and master_code like '%'+@master_code+'%'";
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@master_code", ssMaster_code);
                            wherestr = " and master_code like '%'+@master_code+'%'";
                        }
                    }

                    cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code+ second_id ORDER BY master_code+ second_id DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = 0 {0} and type=@type
                                            )
                                            SELECT supplier_id, master_code, customer_name , master_code + '-' +  customer_name as showname,customer_code , type 
                                            FROM cus
                                            WHERE rn = 1   order by  master_code", wherestr);
                    cmd.Parameters.AddWithValue("@type", Less_than_truckload);
                    second_code.DataSource = dbAdapter.getDataTable(cmd);
                    second_code.DataValueField = "master_code";
                    second_code.DataTextField = "showname";
                    second_code.DataBind();





                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "showname";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));



                }









                #endregion
            }
            else
            {
                string station = Session["management"].ToString();
                string station_level = Session["station_level"].ToString();
                string station_area = Session["station_area"].ToString();
                string station_scode = Session["station_scode"].ToString();


                string managementList = "";
                string station_scodeList = "";
                string station_areaList = "";

                switch (ssManager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        ssSupplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶


                        //一般員工/管理者
                        //有綁定站所只該該站所客戶
                        //無則可查所有站所下的所有客戶
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                            cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                            cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                            if (station_level.Equals("1"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                                DataTable dt1 = dbAdapter.getDataTable(cmda);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        managementList += "'";
                                        managementList += dt1.Rows[i]["station_scode"].ToString();
                                        managementList += "'";
                                        managementList += ",";
                                    }
                                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("2"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                                DataTable dt1 = dbAdapter.getDataTable(cmda);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        station_scodeList += "'";
                                        station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                        station_scodeList += "'";
                                        station_scodeList += ",";
                                    }
                                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("4"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                                DataTable dt1 = dbAdapter.getDataTable(cmda);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        station_areaList += "'";
                                        station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                        station_areaList += "'";
                                        station_areaList += ",";
                                    }
                                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("5") || station_level.Equals(""))
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dt1 = dbAdapter.getDataTable(cmda);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[0]["station_code"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                            }
                            else
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dt = dbAdapter.getDataTable(cmda);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    ssSupplier_code = dt.Rows[0]["station_code"].ToString();
                                }
                            }
                        }
                        break;

                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ssSupplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        ssSupplier_code = Session["account_code"].ToString();   //客戶只能看到自已
                        break;
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = "";
                    if (ssSupplier_code != "")
                    {
                        cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                        wherestr = " and customer_code like ''+@supplier_code+'%' ";
                    }
                    else
                    {
                        if (station_level.Equals("1"))
                        {
                            wherestr = " and station_scode in (" + managementList + ") ";
                        }
                        else if (station_level.Equals("2"))
                        {
                            wherestr = " and station_scode in (" + station_scodeList + ") ";
                        }
                        else if (station_level.Equals("4"))
                        {
                            wherestr = " and station_scode in (" + station_areaList + ") ";
                        }
                    }

                    //wherestr = " and type =@type";
                    //cmd.Parameters.AddWithValue("@type", Less_than_truckload);
                    cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where stop_shipping_code = 0    {0} and type=@type
                                            )
                                            SELECT supplier_id, master_code, customer_name , customer_code + '-' +  customer_name as showname , type 
                                            FROM cus
                                            WHERE rn = 1     order by  master_code", wherestr);
                    cmd.Parameters.AddWithValue("@type", Less_than_truckload);
                    second_code.DataSource = dbAdapter.getDataTable(cmd);
                    second_code.DataValueField = "master_code";
                    second_code.DataTextField = "showname";
                    second_code.DataBind();
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    string wherestr = "";
                    if (ssSupplier_code != "")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                        wherestr = " and customer_code like ''+@supplier_code+'%' ";
                    }

                    cmd2.Parameters.AddWithValue("@type", "1");
                    cmd2.Parameters.AddWithValue("@pricing_code", "02");
                    cmd2.CommandText = string.Format(@"Select customer_code , customer_code+ '-' + customer_shortname as name  
                                           From tbCustomers with(Nolock) 
                                           Where stop_shipping_code = '0'  {0}   and pricing_code = @pricing_code and type =@type
                                            order by customer_code asc ", wherestr);
                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "name";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));
                }
            }
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                second_code.Enabled = true;
                second_code.Items.Insert(0, new ListItem("全部", ""));
            }

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            City.DataSource = dbAdapter.getDataTable(cmd1);
            City.DataValueField = "city";
            City.DataTextField = "city";
            City.DataBind();
            City.Items.Insert(0, new ListItem("請選擇縣市別", ""));
            #endregion

            if (Request.QueryString["receiver_code"] != null)
            {
                receiver_code.Text = Request.QueryString["receiver_code"];
            }

            if (Request.QueryString["receiver_name"] != null)
            {
                receiver_name.Text = Request.QueryString["receiver_name"];
            }
            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            readdata();
        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        if (receiver_code.Text != "")
        {
            cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
            strWhereCmd += " and A.receiver_code like '%'+@receiver_code+'%' ";
            querystring += "&receiver_code=" + receiver_code.Text;
        }

        if (receiver_name.Text != "")
        {
            cmd.Parameters.AddWithValue("@receiver_name", receiver_name.Text);
            strWhereCmd += " and A.receiver_name like '%'+@receiver_name+'%' ";
            querystring += "&receiver_name=" + receiver_name.Text;
        }

        if (second_code.SelectedValue != "")
        {
            strWhereCmd += " and A.customer_code like '" + second_code.SelectedValue + "%' ";
            querystring += "&second_code=" + second_code.SelectedValue;
        }

        if (Less_than_truckload == "0") { }
        else
        {
            string management = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";



            switch (ssManager_type)
            {
                case "0":
                case "1":
                case "2":
                    ssSupplier_code = "";
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", management);
                        cmd.Parameters.AddWithValue("@station_scode", station_scode);
                        cmd.Parameters.AddWithValue("@station_area", station_area);
                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    managementList += "'";
                                    managementList += dt1.Rows[i]["station_scode"].ToString();
                                    managementList += "'";
                                    managementList += ",";
                                }
                                managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            }
                            strWhereCmd += " and B.station_scode in (" + managementList + ")";
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scodeList += "'";
                                    station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                    station_scodeList += "'";
                                    station_scodeList += ",";
                                }
                                station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            }
                            strWhereCmd += " and B.station_scode in (" + station_scodeList + ")";
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_areaList += "'";
                                    station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                    station_areaList += "'";
                                    station_areaList += ",";
                                }
                                station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            }
                            strWhereCmd += " and B.station_scode in (" + station_areaList + ")";
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {

                        }
                        else
                        {

                            cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ssSupplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                    }
                    break;

                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmdA = new SqlCommand())
                    {
                        cmdA.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmdA.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmdA);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            ssSupplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    ssSupplier_code = Session["account_code"].ToString();   //客戶只能看到自已
                    break;
            }
            
            if (ssSupplier_code != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                strWhereCmd += " and A.customer_code like ''+@supplier_code+'%' ";
            }
            
        }



        strWhereCmd += " and B.type = @type";
        cmd.Parameters.AddWithValue("@type", Less_than_truckload);
        #endregion


        cmd.CommandText = string.Format(@"select A. receiver_id, A.customer_code, receiver_code, receiver_name
                                                ,CASE WHEN ISNULL(tel_ext,'') ='' THEN tel ELSE tel + '' + tel_ext END  'tel'
                                                ,tel2
                                                , ISNULL(address_city,'') + ISNULL(address_area,'') + ISNULL(address_road,'') addr
                                                , memo from tbReceiver A With(Nolock)  
                                                left join   tbCustomers B on     A.customer_code=B.customer_code 
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {

        Boolean IsMod = receiver_id > 0 ? true : false;


        if (string.IsNullOrEmpty(receiver_code2.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入收貨人代碼!');</script>", false);
            return;
        }
        if (string.IsNullOrEmpty(receiver_name2.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入收貨人資料!');</script>", false);
            return;
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            string strSQL = @"SELECT 1 FROM tbReceiver WHERE customer_code=@customer_code AND receiver_code=@receiver_code ";
            if (IsMod) strSQL += "AND receiver_id NOT IN('" + receiver_id.ToString() + "')";
            cmd.CommandText = strSQL;
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@receiver_code", receiver_code2.Text.Trim());
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此資料已存在，請重新確認!');</script>", false);
                    return;
                }
            }
        }

        pan_detail.Visible = false;
        if (IsMod)
        {
            #region 修改        
            SqlCommand cmd = new SqlCommand();
            //cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedItem.Value);  //客戶代碼
            cmd.Parameters.AddWithValue("@receiver_code", receiver_code2.Text.Trim()); //收貨人代碼
            cmd.Parameters.AddWithValue("@receiver_name", receiver_name2.Text.Trim()); //收貨人名稱
            cmd.Parameters.AddWithValue("@tel", tel.Text);                             //電話
            cmd.Parameters.AddWithValue("@tel_ext", tel_ext.Text);                     //分機
            cmd.Parameters.AddWithValue("@tel2", tel2.Text);                           //電話2
            cmd.Parameters.AddWithValue("@address_city", City.SelectedValue.ToString());           //地址-縣市
            cmd.Parameters.AddWithValue("@address_area", CityArea.SelectedValue.ToString());        // 地址 - 區域
            cmd.Parameters.AddWithValue("@address_road", address.Text);                             //地址-路段
            cmd.Parameters.AddWithValue("@memo", memo.Value);                                        //備註    
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間             
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "receiver_id", receiver_id);
            cmd.CommandText = dbAdapter.genUpdateComm("tbReceiver", cmd);   //修改
            dbAdapter.execNonQuery(cmd);
            #endregion
        }
        else
        {
            #region 新增        
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);         //客戶代碼
            cmd.Parameters.AddWithValue("@receiver_code", receiver_code2.Text);        //收貨人代碼
            cmd.Parameters.AddWithValue("@receiver_name", receiver_name2.Text);        //收貨人名稱
            cmd.Parameters.AddWithValue("@tel", tel.Text);                             //電話
            cmd.Parameters.AddWithValue("@tel_ext", tel_ext.Text);                     //分機
            cmd.Parameters.AddWithValue("@tel2", tel2.Text);                           //電話2
            cmd.Parameters.AddWithValue("@address_city", City.SelectedValue.ToString());           //地址-縣市
            cmd.Parameters.AddWithValue("@address_area", CityArea.SelectedValue.ToString());        // 地址 - 區域
            cmd.Parameters.AddWithValue("@address_road", address.Text);                             //地址-路段
            cmd.Parameters.AddWithValue("@memo", memo.Value);                                        //備註    
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 
            cmd.CommandText = dbAdapter.SQLdosomething("tbReceiver", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            #endregion
        }

        pan_list.Visible = true;

        //#region 使用者操作log
        //SqlCommand cmd5 = new SqlCommand();
        //cmd5.Parameters.AddWithValue("@p_id", pid);
        //cmd5.Parameters.AddWithValue("@sdate", DateTime.Now);
        //cmd5.Parameters.AddWithValue("@user_id", Session["user_id"]);
        //cmd5.Parameters.AddWithValue("@action", "儲存");
        //cmd5.Parameters.AddWithValue("@ip", Utility.GetIPAddress());
        //cmd5.CommandText = dbAdapter.SQLdosomething("vi_project_log", cmd5, "insert");
        //dbAdapter.execNonQuery(cmd5);

        //#endregion

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='transport_3.aspx';alert('新增完成');</script>", false);
        return;
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "cmdSelect":
                dlcustomer_code.Enabled = false;
                pan_list.Visible = false;
                string wherestr = "";
                SqlCommand cmd = new SqlCommand();

                cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                wherestr += " AND A.receiver_id=@receiver_id";

                cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT.Rows.Count > 0)
                    {
                        receiver_id = Convert.ToInt32(DT.Rows[0]["receiver_id"]);

                        //btnAdd.Text = "修改";
                        if (dlcustomer_code.Items.FindByValue(DT.Rows[0]["customer_code"].ToString().Trim()) == null)
                        {
                            dlcustomer_code.Items.Insert(0, new ListItem(DT.Rows[0]["customer_code"].ToString().Trim(), DT.Rows[0]["customer_code"].ToString().Trim()));
                        }
                        else
                        {
                            dlcustomer_code.SelectedValue = DT.Rows[0]["customer_code"].ToString().Trim();
                        }
                        receiver_code2.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                        receiver_code2.Enabled = (receiver_code2.Text == "");
                        receiver_name2.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                        tel.Text = DT.Rows[0]["tel"].ToString().Trim();
                        tel_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                        tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                        City.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                        City_SelectedIndexChanged(null, null);
                        CityArea.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                        address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                        memo.Value = DT.Rows[0]["memo"].ToString().Trim();
                    }
                }
                pan_detail.Visible = true;
                break;
            case "cmddelete":

                using (SqlCommand cmddel = new SqlCommand())
                {
                    cmddel.Parameters.Add("@where_receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                    cmddel.CommandText = dbAdapter.genDeleteComm("tbReceiver", cmddel);
                    dbAdapter.execNonQuery(cmddel);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='transport_3.aspx';alert('刪除完成');</script>", false);
                return;
                break;

        }
    }


    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (receiver_code.Text.ToString() != "")
        {
            querystring += "&receiver_code=" + receiver_code.Text.ToString();
        }
        if (receiver_name.Text.ToString() != "")
        {
            querystring += "&receiver_name=" + receiver_name.Text.ToString();
        }
        if (second_code.SelectedValue != "")
        {
            querystring += "&second_code=" + second_code.SelectedValue;
        }
        Response.Redirect(ResolveUrl("~/transport_3.aspx?search=yes" + querystring));
    }

    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }

    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void btn_New_Click(object sender, EventArgs e)
    {
        dlcustomer_code.Enabled = true;
        pan_list.Visible = false;
        receiver_id = -1;
        receiver_code2.Enabled = true;
        dlcustomer_code.SelectedValue = "";
        receiver_code2.Text = "";
        receiver_name2.Text = "";
        City.SelectedValue = "";
        City_SelectedIndexChanged(null, null);
        CityArea.SelectedValue = "";
        address.Text = "";
        memo.Value = "";
        tel.Text = "";
        tel_ext.Text = "";
        tel2.Text = "";
        pan_detail.Visible = true;
        //customer_code.Text = ssMaster_code;

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pan_list.Visible = true;
        receiver_id = -1;
        pan_detail.Visible = false;
    }
}