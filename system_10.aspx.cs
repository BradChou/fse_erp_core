﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Script.Serialization;
using System.Globalization;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


public partial class system_10 : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
                                
            }
            readdata("");
        }

        
    }

    private void readdata(string msg)
    {
        SqlCommand objCommand = new SqlCommand();

        string wherestr = "";
        if (keyword.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
            wherestr += "  and (A.supplier_code like '%'+@keyword+'%' or A.supplier_name like '%'+@keyword+'%' or A.supplier_no like '%'+@keyword+'%')"; 
        }

        if (chk_supplier.Checked)
        {   
            wherestr += "  and ISNULL(A.supplier_code,'') <> ''";
        }
        
        objCommand.CommandText = string.Format(@"select A.*,CASE WHEN A.show_trans = 1 THEN '啟用' ELSE '停用' END AS s_status , D.user_name , A.udate
                                                from tbSuppliers A with(nolock) 
                                                left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                where A.active_flag =1 {0}
                                                order by A.show_trans desc, A.supplier_code", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            list_supplier.DataSource = dt;
            list_supplier.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
           
        }

        if (msg != "")
        {
            msg = "alert('" + msg + "');";
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + msg + "</script>", false);


    }


    protected void list_supplier_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdStop")
        {

            string ErrStr = "";
            string supplier_id = ((HiddenField)e.Item.FindControl("Hid_supplier_id")).Value.ToString().Trim();
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@show_trans", 0);
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "supplier_id", supplier_id);
            cmd.CommandText = dbAdapter.genUpdateComm("tbSuppliers", cmd);
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ErrStr = ex.Message;
            }


            if (ErrStr == "")
            {
                readdata("停用完成");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alter('停用失敗：" + ErrStr + ")</script>", false);
            }
        }
        else if (e.CommandName == "cmdActive")
        {
            string ErrStr = "";
            string supplier_id = ((HiddenField)e.Item.FindControl("Hid_supplier_id")).Value.ToString().Trim();
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@show_trans", 1);
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "supplier_id", supplier_id);
            cmd.CommandText = dbAdapter.genUpdateComm("tbSuppliers", cmd);
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                ErrStr = ex.Message;
            }

            if (ErrStr == "")
            {
                readdata("啟用完成");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alter('啟用失敗：" + ErrStr + ")</script>", false);
            }
        }
        else if (e.CommandName == "cmdEdit")
        {
            //string ErrStr = "";
            string supplier_id = ((HiddenField)e.Item.FindControl("Hid_supplier_id")).Value.ToString().Trim();

            Response.Redirect("system_10-edit.aspx?src=system_10.aspx&supplier_id=" + supplier_id);
             
        }
    }
    protected void search_Click(object sender, EventArgs e)
    {        
        readdata("");
    }
    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "區配商資料";
        string file_name = "區配商資料" + DateTime.Now.ToString("yyyyMMdd");
        
        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "";
            if (keyword.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
                wherestr += "  and (supplier_code like '%'+@keyword+'%' or supplier_name like '%'+@keyword+'%')";
            }

            objCommand.CommandText = string.Format(@"select ROW_NUMBER() OVER(ORDER BY show_trans desc, supplier_code) AS 'NO',
                                                supplier_code AS '區配編號',supplier_no '供應商編號', supplier_name AS '區配商',uniform_numbers '統一編號',
                                                id_no '身分證字號', supplier_shortname '名稱', principal '負責人',  contact '聯絡人',
                                                telephone '連絡電話', email 'E-mial', city+area+road '通訊地址', supplier_name '發票抬頭',
                                                receipt_number '發票統一編號', bank_code '收款銀行代號', account '銀行帳戶',bank_name '銀行/分行',
                                                city_receipt+area_receipt+road_receipt '發票寄送地址', 
                                                CASE WHEN show_trans = 1 THEN '啟用' ELSE '停用' END AS '狀態' 
                                                from tbSuppliers  with(nolock) 
                                                where active_flag =1 {0}
                                                order by show_trans desc, supplier_code", wherestr);
            


            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }



}