﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_4.aspx.cs" Inherits="store1_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {

            $(".btn-primary").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });



        });
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
        function Edit(str, num, area, dn, temp) {
            $("#<%=code_id.ClientID%>").val(num);
            $("#<%=code_name.ClientID%>").val(str);
            $("#<%=Area.ClientID%>").val(area);
            $("#<%=DayNight.ClientID%>").val(dn);
            $("#<%=Temperature.ClientID%>").val(temp);
            $("#<%=btnsave.ClientID%>").val("修 改");
            $("#<%=btnsave.ClientID%>").prop("class", "btn btn-info");
            $("#<%=code_name.ClientID%>").focus();
        }

    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }


        .radio label {
            margin-right: 15px;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">
                <asp:Label ID="lbl_title" Text="路線維護" runat="server"></asp:Label>
            </h2>
            <hr />
            <div>
                <div class="row">
                    <div class="col-lg-6 form-group form-inline">
                        <label class="">管理單位：</label>
                        <asp:Label ID="lbitem" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label class="">人　　員：</label>
                        <asp:Label ID="lbitem_employee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label class="">帳　　號：</label>
                        <asp:Label ID="lbaccount_code" runat="server"></asp:Label>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label class="">名　　稱：</label>
                        <asp:Label ID="lbaccount_name" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label class=""><span class="REDWD_b">*</span>場　　區：</label>
                        <asp:DropDownList ID="Area" runat="server" class="form-control "></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="Area" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇溫層</asp:RequiredFieldValidator>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label class=""><span class="REDWD_b">*</span>路線名稱：</label>
                        <asp:TextBox ID="code_name" runat="server" class="form-control "></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="code_name" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入路線名稱</asp:RequiredFieldValidator>
                        <input id="code_id" type="hidden" runat="server" />
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                            <label class="">日 / 夜：</label>
                            <asp:DropDownList ID="DayNight" runat="server" class="form-control">
                                <asp:ListItem Value="1">日</asp:ListItem>
                                <asp:ListItem Value="2">夜</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                            <label class=""><span class="REDWD_b">*</span>溫　　層：</label>
                            <asp:DropDownList ID="Temperature" runat="server" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="Temperature" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇溫層</asp:RequiredFieldValidator>
                            <asp:Button ID="btnsave" CssClass="btn btn-warning" runat="server" Text="新 增" ValidationGroup="validate" OnClick="btnsave_Click" />
                            <a href="store1_4.aspx" class="btn btn-info"><span>取消</span></a>
                            
                        </div>
                </div>
                <hr />
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th" style="width: 10%"></th>
                        <th class="_th">場區名稱</th>
                        <th class="_th">路線名稱</th>
                        <th class="_th">日/夜</th>
                        <th class="_th">溫層</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class="paginate">
                                <td>
                                    <a href="#" class="btn btn-info" id="updclick" onclick="Edit('<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%>',<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>,<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.FieldArea").ToString())%>,<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DayNight").ToString())%>,<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Temperature").ToString())%>)">修改</a>                               
                                </td>
                                <td class ="text-left " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.FieldArea_name").ToString())%></td>
                                <td class ="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                <td class ="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dn").ToString())%></td>
                                <td class ="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.TempName").ToString())%></td>
                                <td class ="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td class ="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="16" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

