﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class MasterMember : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            UserName.Text = Session["user_name"].ToString();
            if ((new string[] { "0", "1", "2" }).Contains(Session["manager_type"])) logo.HRef = "option.aspx";
            string type = Session["type"].ToString();
            #region  上方權限
            DataTable dttop = new DataTable();
            dttop = Utility.getTopMenu(Session["account_code"].ToString(), type);
            if (dttop.Rows.Count > 0)
            {
                string top_func_code = "";
                for (int i = 0; i < dttop.Rows.Count; i++)
                {
                    top_func_code = dttop.Rows[i]["top_func_code"].ToString();
                    switch (top_func_code)
                    {
                        case "D":
                            D.Visible = true;
                            Dhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "T":
                            T.Visible = true;
                            Thref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "R":
                            R.Visible = true;
                            Rhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "C":
                            C.Visible = true;
                            Chref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "F":
                            F.Visible = true;
                            Fhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "S":
                            S.Visible = true;
                            Shref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "A":
                            A.Visible = true;
                            Ahref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "B":
                            B.Visible = true;
                            Bhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "E":
                            E.Visible = true;
                            Ehref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "G":
                            G.Visible = true;
                            Ghref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                    }
                }
            }
            #endregion

            #region  左方權限
            DataTable dt = new DataTable();
            dt = Utility.getAuthDT(Session["account_code"].ToString(), "C", "C", type);
            if (dt.Rows.Count > 0)
            {
                Boolean view_auth = false;
                Boolean insert_auth = false;
                Boolean modify_auth = false;
                Boolean delete_auth = false;
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataTable dtb = new DataTable();
                    dtb = Utility.getAuthDT(Session["account_code"].ToString(), "C", dt.Rows[i]["func_code"].ToString(), type);
                    if (dtb.Rows.Count > 0)
                    {
                        if (dt.Rows[i]["func_link"].ToString() != "")
                        {
                            useritemlist.Text += "<li><a data-ajax-page='" + dt.Rows[i]["func_link"].ToString().Replace(".aspx", "") + "' href='" + dt.Rows[i]["func_link"].ToString().Trim() + "'>" + dt.Rows[i]["func_name"].ToString().Trim() + "<span class='fa arrow'></span></a>";
                            useritemlist.Text += "<ul class='nav nav-second-level in'>";
                        }
                        else
                        {
                            useritemlist.Text += "<li><a href='#'>" + dt.Rows[i]["func_name"].ToString().Trim() + "<span class='fa arrow'></span></a>";
                            useritemlist.Text += "<ul class='nav nav-second-level in'>";
                        }
                        for (int j = 0; j < dtb.Rows.Count; j++)
                        {
                            view_auth = Convert.ToBoolean(dtb.Rows[j]["view_auth"]);
                            insert_auth = Convert.ToBoolean(dtb.Rows[j]["insert_auth"]);
                            modify_auth = Convert.ToBoolean(dtb.Rows[j]["modify_auth"]);
                            delete_auth = Convert.ToBoolean(dtb.Rows[j]["delete_auth"]);
                            if (view_auth || insert_auth || modify_auth || delete_auth)
                            {
                                useritemlist.Text += "<li><a data-ajax-page='" + dtb.Rows[j]["func_link"].ToString().Replace(".aspx", "") + "' href='" + dtb.Rows[j]["func_link"].ToString().Trim() + "'>" + dtb.Rows[j]["func_name"].ToString().Trim() + "</a></li>";
                            }
                        }
                        useritemlist.Text += "</ul>";
                        useritemlist.Text += "</li>";
                    }
                    else
                    {
                        view_auth = Convert.ToBoolean(dt.Rows[i]["view_auth"]);
                        insert_auth = Convert.ToBoolean(dt.Rows[i]["insert_auth"]);
                        modify_auth = Convert.ToBoolean(dt.Rows[i]["modify_auth"]);
                        delete_auth = Convert.ToBoolean(dt.Rows[i]["delete_auth"]);
                        if (view_auth || insert_auth || modify_auth || delete_auth)
                        {
                            useritemlist.Text += "<li><a data-ajax-page='" + dt.Rows[i]["func_link"].ToString().Replace(".aspx", "") + "' href='" + dt.Rows[i]["func_link"].ToString().Trim() + "'>" + dt.Rows[i]["func_name"].ToString().Trim() + "</a></li>";
                        }
                    }
                }
            }
            #endregion

            #region  Log
            try
            {
                string StrAccount = Session["account_code"].ToString();
                if (StrAccount.Length > 0)
                {
                    new PublicFunction().ExeOpLog(StrAccount, System.IO.Path.GetFileName(Request.PhysicalPath), "");
                }
            }
            catch (Exception ex)
            {

            }

            #endregion
        }
    }
}
