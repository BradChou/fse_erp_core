﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class report_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //String yy = DateTime.Now.Year.ToString();
            //String mm = DateTime.Now.Month.ToString();
            //String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            dates.Text = DateTime.Today.ToString("yyyy/MM/dd");
            datee.Text = DateTime.Today.ToString("yyyy/MM/dd");

            string manager_type = Session["manager_type"].ToString(); //管理單位類別
            string supplier_code = Session["master_code"].ToString();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
            {
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000";
                            break;
                    }
                }

            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where ISNULL(supplier_code ,'') <> '' {0} order by supplier_code", wherestr);
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
           
            if (manager_type == "0" || manager_type == "1" || manager_type == "2")
            {
                Suppliers.Items.Insert(0, new ListItem("全公司", ""));
            }

            #endregion

            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if(Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }


            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue  = Request.QueryString["Suppliers"];
            }

            if (Request.QueryString["rb_pricing_type"] != null)
            {
                rb_pricing_type.SelectedValue = Request.QueryString["rb_pricing_type"];
            }

            readdata();

        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;


        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }

        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        if (rb_pricing_type.SelectedValue.ToString() != "")
        {
            querystring += "&rb_pricing_type=" + rb_pricing_type.SelectedValue.ToString();
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetCDistributionReport";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", dates.Text);
            cmd.Parameters.AddWithValue("@print_datee", datee.Text);
            cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            if (rb_pricing_type.SelectedValue == "專車")
            {
                cmd.Parameters.AddWithValue("@pricing_type", "05");
            }
            if (rb_pricing_type.SelectedValue == "論版")
            {
                cmd.Parameters.AddWithValue("@pricing_type", "01");
            }

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                New_List.DataSource = dt;
                New_List.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();

            }
        }

    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }

        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        if (rb_pricing_type.SelectedValue.ToString() != "")
        {
            querystring += "&rb_pricing_type=" + rb_pricing_type.SelectedValue.ToString();
        }

        Response.Redirect(ResolveUrl("~/report_2.aspx?search=yes" + querystring));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "C段配送明細表";
        string file_name = "C段配送明細表" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetCDistributionReport";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@print_dates", dates.Text);
            cmd.Parameters.AddWithValue("@print_datee", datee.Text);
            cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());

            if (rb_pricing_type.SelectedValue == "專車")
            {
                cmd.Parameters.AddWithValue("@pricing_type", "05");
            }
            if (rb_pricing_type.SelectedValue == "論版")
            {
                cmd.Parameters.AddWithValue("@pricing_type", "01");
            }

            cmd.CommandType = CommandType.StoredProcedure;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[ 2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }

                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }
                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }
}