﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_2.aspx.cs" Inherits="assets1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(function () {
                init();
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                init();
            }
            function init() {
                $("#tbscan [id*=chkSend]").click(function () {
                    if ($(this).is(":checked")) {
                        $("#tbscan [id*=cbSelSend]").prop("checked", true);
                    } else {
                        $("#tbscan [id*=cbSelSend]").prop("checked", false);
                    }

                });
                $("#tbscan [id*=cbSelSend]").click(function () {
                    if ($("#tbscan [id*=cbSelSend]").length == $("#tbscan [id*=cbSelSend]:checked").length) {
                        $("#tbscan [id*=chkSend]").prop("checked", true);
                    } else {
                        $("#tbscan [id*=chkSend]").prop("checked", false);
                    }

                });

                $("#tbscan [id*=chkPhoto]").click(function () {
                    if ($(this).is(":checked")) {
                        $("#tbscan [id*=cbSelPhoto]").prop("checked", true);
                    } else {
                        $("#tbscan [id*=cbSelPhoto]").prop("checked", false);
                    }

                });
                $("#tbscan [id*=cbSelPhoto]").click(function () {
                    if ($("#tbscan [id*=cbSelPhoto]").length == $("#tbscan [id*=cbSelPhoto]:checked").length) {
                        $("#tbscan [id*=chkPhoto]").prop("checked", true);
                    } else {
                        $("#tbscan [id*=chkPhoto]").prop("checked", false);
                    }

                });

            }


            $(".btn-primary").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
        
    </script>

    <style>
        ._th {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">使用單位更新維護</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label>車牌：</label>
                    <asp:TextBox ID="car_license" runat="server" class="form-control " ></asp:TextBox>
                    <label>車行：</label>
                    <asp:DropDownList ID="ddl_car_retailer" runat="server" class="form-control "></asp:DropDownList>
                    <label>車主：</label>
                    <asp:TextBox ID="owner" runat="server" class="form-control " ></asp:TextBox>
                    <label>司機：</label>
                    <asp:TextBox ID="driver" runat="server" class="form-control " ></asp:TextBox>
                    <label>使用單位：</label>
                    <asp:DropDownList ID="Dept" runat="server" class="form-control "></asp:DropDownList>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                </div>             
                
                <p class="text-primary">※點選異動日期可查詢異動紀錄</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">                       
                        <th class="_th">車牌</th>
                        <th class="_th">噸數</th>
                        <th class="_th">使用單位</th>
                        <th class="_th">原使用單位</th>
                        <th class="_th">車行</th>
                        <th class="_th">車主</th>
                        <th class="_th">司機</th>
                        <th class="_th">前次異動者</th>
                        <th class="_th">前次異動日期</th>
                        <th class="_th"></th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">                      
                                <td data-th="車牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>                                
                                <td data-th="噸數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                <td data-th="使用單位"><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1","code_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dept").ToString()))%> </td>
                                <td data-th="原使用單位"><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1","code_id",Func.GetRow("dept","ttAssetsDeptChange","order by id desc","a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())))%></td>
                                <td data-th="車行"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_retailer_text").ToString())%></td>
                                <td data-th="車主"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.owner").ToString())%></td>
                                <td data-th="司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td data-th="前次異動者"><%#Func.GetRow("uuser","ttAssetsDeptChange","order by id desc","a_id = @a_id and car_license = @car_license","a_id|car_license",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())+"|"+Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString()))%></td>
                                <td data-th="前次異動日期">
                                    <a href="assets1_2log.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%>" class="fancybox fancybox.iframe " id="logclick">
                                    <%#Func.GetRow("udate","ttAssetsDeptChange","order by id desc","a_id = @a_id and car_license = @car_license","a_id|car_license",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())+"|"+Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString()))==""?"":Convert.ToDateTime(Func.GetRow("udate","ttAssetsDeptChange","order by id desc","a_id = @a_id and car_license = @car_license","a_id|car_license",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())+"|"+Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString()))).ToString("yyyy/MM/dd")%></a>
                                </td>                                
                                <td data-th="" >
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>' /> 
                                    <a href="assets1_2_edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>" class="btn btn-info  " id="updclick">車輛單位異動</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="17" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

