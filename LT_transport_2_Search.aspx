﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LT_transport_2_Search.aspx.cs" Inherits="LT_transport_2_Search" %>



<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>峻富雲端物流管理-託運單修改</title>
<link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
<meta name="description" content="">
<meta name="author" content="templatemo">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/templatemo-style.css" rel="stylesheet">

<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/jquery.ui.datepicker.css">

<script src="js/bootstrap.min.js"></script>
<script src="js/datepicker-zh-TW.js" type="text/javascript"></script>

<link href="css/build.css" rel="stylesheet" />

<link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
<script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
<script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript">

    function loadingback(a) {

        if ($('#<%=delete_memo.ClientID %>').val() != "")
        {
        if (a == "1") {
            document.getElementById("loader").style.display = 'block';
            document.getElementById("loaderbk").style.display = 'block';
        }
        else {
            document.getElementById("loader").style.display = 'none';
            document.getElementById("loaderbk").style.display = 'none';
            }
        }

    }




    $(document).ready(function () {
        $("#loaderbk").css({ "width": $(window).width() + "px" });
        $("#loaderbk").css({ "height": $(window).height() + "px" });
        $(".datepicker").datepicker({
            dateFormat: 'yy/mm/dd',
            changeMonth: true,
            changeYear: true,
            defaultDate: (new Date())  //預設當日
        });
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

        function EndRequestHandler(sender, args) {
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                defaultDate: (new Date())  //預設當日
            });
        }
    });
    $.fancybox.update();

    $(document).on("click", "._new_addr", function () {

        var _url = "LT_transport_2_edit.aspx?r_type=1&request_id=" + $(this).parent("td").attr("_the_id");
        $.fancybox({
            'width': '60%',
            'height': '95%',
            'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'href': _url
        });
    });

    function CopyReturnlocation(request_id) {
        Model_CurrentPage.Session.Set("transport_1_2");
        location.href = "transport_1_2.aspx?request_id=" + request_id;
    }
</script>



<style>
    input[type="radio"] {
        display: inherit;
    }

    input[type="checkbox"] {
        display: inherit;
    }

    .radio label {
        text-align: left;
    }

    .checkbox label {
        margin-right: 15px;
        text-align: left;
    }

    .radio {
        padding-left: 5px;
    }

    .checkbox label {
        margin-right: 15px;
        text-align: left;
        min-width: 0px;
    }

    .checkbox {
        padding-left: 15px;
    }

    ._shortwidth {
        width: 80px;
    }

    .ralign {
        text-align: right;
    }

    .ui-datepicker-title {
        color: black
    }

    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>
<body style="background-color: white !important; margin: 60px !important">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="loader" id="loader" style="top: 47%; left: 47%; position: fixed; z-index: 1000; display: none"></div>
        <div class="" id="loaderbk" style="top: 0%; left: 0%; position: fixed; z-index: 999; background-color: gray; opacity: 0.8; display: none"></div>


        <div id="page-wrapper">
            <div class="row">
                <h2 class="margin-bottom-10">查詢及銷單作業</h2>

                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div id="div_search" runat="server">
                            <div class="templatemo-login-form">
                                <div class="form-group form-inline">
                                    <asp:RadioButton ID="rb2" runat="server" CssClass="radio radio-success" Text="發送區間" GroupName="dategroup" Checked="true"></asp:RadioButton>

                                    <asp:TextBox ID="tb_dateS2" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req_dateS" runat="server" ControlToValidate="tb_dateS" ForeColor="Red" ValidationGroup="validate">請輸入發送區間</asp:RequiredFieldValidator>--%>
                            ~
                        <asp:TextBox ID="tb_dateE2" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req_dateE" runat="server" ControlToValidate="tb_dateE" ForeColor="Red" ValidationGroup="validate">請輸入發送區間</asp:RequiredFieldValidator>--%>
                                    <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" CausesValidation="false" />
                                    <%-- <asp:Button ID="btnAdd" CssClass="templatemo-white-button" runat="server" Text="單筆補單" OnClick="btnAdd_Click" />--%>
                                    <asp:Button ID="btnExport" CssClass="templatemo-blue-button" runat="server" Text="匯出" OnClick="btnExport_Click" Visible="false" />

                                </div>
                                <div class="form-group form-inline">
                                    <asp:RadioButton ID="rb1" runat="server" CssClass="radio radio-success" Text="建單區間" GroupName="dategroup"></asp:RadioButton>
                                    <asp:TextBox ID="tb_dateS1" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_dateS" ForeColor="Red" ValidationGroup="validate">請輸入建單區間</asp:RequiredFieldValidator>--%>
                            ~
                            <asp:TextBox ID="tb_dateE1" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_dateE" ForeColor="Red" ValidationGroup="validate">請輸入建單區間</asp:RequiredFieldValidator>                        --%>






                                    <div class="div_file" style="display: none;">
                                        <asp:FileUpload ID="fileToUpload" runat="server" />
                                        <input type="button" class="btn btn-primary" value="上 傳" />
                                    </div>
                                </div>

                                <div class="form-group form-inline">
                                    <label class="">客戶代號</label>
                                    <asp:DropDownList ID="Customer" CssClass="form-control" runat="server"></asp:DropDownList>
                                    <%--<asp:RadioButton ID="rb3" runat="server" CssClass="radio radio-success" Text="託運單號" GroupName="dategroup"></asp:RadioButton>--%>
                                    <label>託運單號：</label>
                                    <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20"></asp:TextBox>

                                    <label>匯入單號：</label>
                                    <asp:TextBox ID="Import_number" runat="server" class="form-control" placeholder="請輸入匯入單號"></asp:TextBox>
                                </div>

                                <div class="margin-right-15 templatemo-inline-block">
                                    <%--<label for="inputFirstName">託運類別：</label>--%>
                                    <asp:RadioButtonList ID="rb_pricing_type" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio radio-success" Visible="false">
                                    </asp:RadioButtonList>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <hr />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="divAdd" runat="server" visible="false" class="templatemo-login-form panel panel-default">
                            <div class="panel-heading">
                                <h2 class="text-uppercase">單筆補單</h2>
                            </div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <div class="margin-right-15 templatemo-inline-block _div_type">
                                            <span class="_title _tip_important">託運類別</span>
                                            <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged" CssClass="radio radio-success">
                                                <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                                <asp:ListItem Value="02">02 論件</asp:ListItem>
                                                <asp:ListItem Value="03">03 論才</asp:ListItem>
                                                <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label>出貨日期：</label><asp:TextBox ID="Shipments_date" runat="server" class="form-control" CssClass="datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">貨　　號</span>
                                        <asp:DropDownList runat="server" ID="ddl_check_number2" class="form-control ddl_check_number2"></asp:DropDownList>
                                        <asp:Label ID="lbErr_chknum" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">託運類別</span>
                                        <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                        </asp:DropDownList>
                                        <span class="_title _tip_important">傳票類別</span>
                                        <asp:DropDownList ID="subpoena_category" runat="server" class="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">收件人</span>

                                        <asp:TextBox ID="receive_contact" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">寄件人</span>
                                        <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">收件地址</span>

                                        <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:TextBox ID="receive_address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                        <asp:DropDownList ID="area_arrive_code" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title _tip_important">寄件地址</span>
                                        <asp:DropDownList ID="send_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="send_area" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:TextBox ID="send_address" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title">區配商代碼</span>
                                        <asp:TextBox ID="supplier_code" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                        <span class="_title">客戶代號</span>
                                        <asp:DropDownList ID="dlcustomer_code" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title"></span>
                                        <asp:CheckBox ID="receipt_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="回單" />
                                        <asp:CheckBox ID="pallet_recycling_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="棧板回收" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title">貨物數量</span>
                                        <asp:TextBox ID="pieces" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                        件數
                                <asp:TextBox ID="plates" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                        板
                                <asp:TextBox ID="cbm" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                        才
                                <asp:Label ID="lbErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title">備　　註</span>
                                        <asp:TextBox ID="invoice_desc" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="_title">費　　用</span>
                                        <label>代收金</label>
                                        <asp:TextBox ID="collection_money" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                        &nbsp;
                                <label>到付運費</label>
                                        <asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                        &nbsp;
                                <label>到付追加</label>
                                        <asp:TextBox ID="arrive_to_pay_append" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="btn_session">
                                    <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="新增" OnClick="btnSave_Click" />
                                    <asp:Button ID="btnCanle" CssClass="btn templatemo-white-button" runat="server" Text="取消" OnClick="btnCanle_Click" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExport" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div id="div_list" runat="server" style="height: 400px; overflow-y: scroll">
                            共 <span class="text-danger">
                                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                            </span>筆
                        <br>
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="※請勾選要銷單的託運單，有掃讀歷程單號，不允許銷單"></asp:Label>

                            <table id="Search_table" class="table table-striped table-bordered templatemo-user-table">
                                <thead>
                                    <tr class="tr-only-hide">
                                        <td>
                                            <asp:CheckBox ID="chkHeader" runat="server" /></td>
                                        <td>貨　　號</td>
                                        <td>配送日</td>
                                        <td>發送站</td>
                                        <td>寄件人</td>
                                        <td>收件人</td>
                                        <td>地　　址</td>
                                        <td>區配商代碼</td>
                                        <td>建檔人員</td>
                                        <td>更新日期</td>
                                        <td>匯入日期</td>
                                        <td>匯入單號</td>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="New_List" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="列印">
                                                <asp:CheckBox ID="chkRow" runat="server" />
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                                <asp:HiddenField ID="HiddenField2" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%>' />
                                            </td>
                                            <td class="td_ck_number " data-th="貨號">
                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>' />
                                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) %>
                                            </td>
                                            <td data-th="配送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                                            <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation2").ToString())%></td>
                                            <td data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%> </td>
                                            <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%> </td>
                                            <td data-th="地址" class="text-left"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address").ToString())%></td>
                                            <td data-th="區配商代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                            <td data-th="建檔人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                            <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                            <td data-th="匯入日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                            <td data-th="匯入單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.import_randomCode").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="11" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>

                            <%--<div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                    </span>筆
                </div>--%>
                        </div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="H1">請輸入銷單原因(限200字)</h4>
                        </div>
                        <asp:TextBox ID="delete_memo" runat="server" class="form-control" TextMode="MultiLine" Height="200px" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="delete_memo" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入銷單原因</asp:RequiredFieldValidator>
                        <br />

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="width: 100%; text-align: center">
                    <asp:Button ID="btnCancel" CssClass="templatemo-blue-button" runat="server" Text="銷單" OnClick="btnCancel_Click" OnClientClick="loadingback(1)" ValidationGroup="validate" />
                </div>

            </div>
        </div>
    </form>
</body>


<script>
    $(document).ready(function () {
        $("#Search_table [id*=chkHeader]").click(function () {
            if ($(this).is(":checked")) {
                $("#Search_table [id*=chkRow]").prop("checked", true);
            } else {
                $("#Search_table [id*=chkRow]").prop("checked", false);
            }
        });
        $("#Search_table [id*=chkRow]").click(function () {
            if ($("#Search_table [id*=chkRow]").length == $("#Search_table [id*=chkRow]:checked").length) {
                $("#Search_table [id*=chkHeader]").prop("checked", true);
            } else {
                $("#Search_table [id*=chkHeader]").prop("checked", false);
            }
        });



    });
</script>


