﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class system_10_edit_1 : System.Web.UI.Page
{
    string supplierNo;
    DataTable gridData = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        supplierNo = Request.Params["supplierNo"];
    }

    protected void ViewFreightTable(object sender, EventArgs e)
    {
        if(gridData == null)
            GetGridData();

        freight_table.DataSource = gridData;
        freight_table.DataBind();        
    }

    private void GetGridData()
    {
        string query = @"DECLARE @id int;

SELECT TOP 1 @id = id FROM supplier_freight 
WHERE supplier_no = @supplierNo AND effective_date < GETDATE() 
ORDER BY create_date DESC

SELECT 
supply_area AS '發送區',
receive_area AS '到著區',
A AS 'A段1~6板',
B1 AS 'B段1板',B2 AS 'B段2板',B3 AS 'B段3板',B4 AS 'B段4板',B5 AS 'B段5板',B6 AS 'B段6板以上',
C1 AS 'C段1板',C2 AS 'C段2板',C3 AS 'C段3板',C4 AS 'C段4板',C5 AS 'C段5板',C6 AS 'C段6板以上'
FROM supplier_freight_detail WHERE supplier_freight_id = @id";

        using (SqlCommand cmd = new SqlCommand(query))
        {
            cmd.Parameters.AddWithValue("@supplierNo", supplierNo);
            gridData = dbAdapter.getDataTable(cmd);
        }

        if(gridData == null || gridData.Rows.Count == 0)
        {
            // 用公版運價
            string queryForPublicVersion = @"
SELECT 
supply_area AS '發送區',
receive_area AS '到著區',
A AS 'A段1~6板',
B1 AS 'B段1板',B2 AS 'B段2板',B3 AS 'B段3板',B4 AS 'B段4板',B5 AS 'B段5板',B6 AS 'B段6板以上',
C1 AS 'C段1板',C2 AS 'C段2板',C3 AS 'C段3板',C4 AS 'C段4板',C5 AS 'C段5板',C6 AS 'C段6板以上'
FROM supplier_freight_detail WHERE supplier_freight_id = 1";

            using (SqlCommand cmd = new SqlCommand(queryForPublicVersion))
            {
                gridData = dbAdapter.getDataTable(cmd);
            }
        }
    }

    protected void DownLoadCsv(object sender, EventArgs e)
    {
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);

        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=運價表.xls"));

        if(gridData == null)        
            GetGridData();        

        this.freight_table.AllowPaging = false;
        this.freight_table.RenderControl(hw);

        this.freight_table.AllowPaging = true;

        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // 必須覆寫此方法才能用 RenderControl()
    }
}