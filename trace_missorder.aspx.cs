﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using ClosedXML.Excel;

public partial class trace_missorder : System.Web.UI.Page
{
    public int start_number_type1
    {
        get { return Convert.ToInt32(ViewState["start_number_type1"]); }
        set { ViewState["start_number_type1"] = value; }
    }

    public int end_number_type1
    {
        get { return Convert.ToInt32(ViewState["end_number_type1"]); }
        set { ViewState["end_number_type1"] = value; }
    }

    public int start_number_type2
    {
        get { return Convert.ToInt32(ViewState["start_number_type2"]); }
        set { ViewState["start_number_type2"] = value; }
    }

    public int end_number_type2
    {
        get { return Convert.ToInt32(ViewState["end_number_type2"]); }
        set { ViewState["end_number_type2"] = value; }
    }


    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }


    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select city from tbPostCity order by seq asc ";
            receive_city.DataSource = dbAdapter.getDataTable(cmd1);
            receive_city.DataValueField = "city";
            receive_city.DataTextField = "city";
            receive_city.DataBind();
            receive_city.Items.Insert(0, new ListItem("請選擇", ""));

            send_city.DataSource = dbAdapter.getDataTable(cmd1);
            send_city.DataValueField = "city";
            send_city.DataTextField = "city";
            send_city.DataBind();
            send_city.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 託運類別
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT  [code_id] as id,[code_id]+' '+[code_name] as name
                                FROM    [tbItemCodes]
                                where   [code_sclass] = 'PM' and[active_flag] = 1";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                rbcheck_type.Items.Clear();
                rbcheck_type.DataSource = dt;
                rbcheck_type.DataValueField = "id";
                rbcheck_type.DataTextField = "name";
                rbcheck_type.DataBind();
                rbcheck_type.SelectedIndex = 0;
            }
            #endregion



            rbcheck_type_SelectedIndexChanged(null, null);



            #region 託運類別
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
            check_type.DataSource = dbAdapter.getDataTable(cmd2);
            check_type.DataValueField = "code_id";
            check_type.DataTextField = "code_name";
            check_type.DataBind();
            #endregion

            #region 傳票類別
            SqlCommand cmd3 = new SqlCommand();
            cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 order by code_id ";
            subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
            subpoena_category.DataValueField = "code_id";
            subpoena_category.DataTextField = "code_name";
            subpoena_category.DataBind();
            #endregion
            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";
        cmd.Parameters.Clear();

        #region 關鍵字
        if (!String.IsNullOrEmpty(tb_dateS.Text) && !String.IsNullOrEmpty(tb_dateE.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", tb_dateS.Text);
            cmd.Parameters.AddWithValue("@dateE", tb_dateE.Text);
            strWhereCmd += " AND A.scan_date >= convert(varchar, @dateS, 111) and A.scan_date<= convert(varchar, @dateE, 111)";
        }

        if (check_number.Text != "")
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            strWhereCmd += " AND A.check_number =@check_number";
        }
        #endregion

        cmd.CommandText = string.Format(@"
                                        WITH scan AS 
                                        (
                                        select dr.request_id , scan.*  ,
                                        ROW_NUMBER() OVER (PARTITION BY scan.check_number ORDER BY scan.check_number DESC) AS rn
                                        from  ttDeliveryScanLog  scan
                                        left join tcDeliveryRequests dr with(nolock) on scan.check_number = dr.check_number  and dr.print_date >=  DATEADD(MONTH,-6,getdate())
                                        where scan.scan_date  >=  DATEADD(MONTH,-6,getdate())
                                        and LEN(scan.check_number) = 10                                         
                                        and(not(scan.check_number between '8158486615' and '8158586601'))
                                        and(not(scan.check_number between '5920000000' and '5929999999'))
                                        and(not(scan.check_number between '8347120000' and '8347129999'))
                                        and(not(scan.check_number between '8819346760' and '8819406750'))
                                        and(not(scan.check_number between '9000200010' and '9000229999'))
                                        and(not(scan.check_number between '8283490025' and '8286490015'))
                                        and ISNULL(dr.request_id,0) = 0
                                        )
                                        select  ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO ,
                                        A.log_id ,A.check_number , 
                                        P3.code_name as scan_name,  Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                        A.arrive_option,A.exception_option , 
                                        CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE CASE WHEN EO.code_name='無' THEN '' ELSE EO.code_name END  END as status,
                                        A.driver_code , C.driver_name , A.sign_form_image
                                        from scan A
                                        left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> ''
                                        left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                        left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                        left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                        left join tbItemCodes P3 with(nolock) on A.scan_item = P3.code_id  and P3.code_sclass = 'P3' and P3.active_flag = 1 
                                        where rn= 1 {0}", strWhereCmd);

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }

    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (add_check_number.Text != "")
        {

            Clear();
            divAdd.Visible = true;
            #region 貨號區間(一筆式)
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbCheckNumber ";
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT.Rows.Count > 0)
                    {
                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            switch (DT.Rows[i]["type"].ToString())
                            {
                                case "1":   //一筆式
                                    start_number_type1 = Convert.ToInt32(DT.Rows[i]["start_number"]);
                                    end_number_type1 = Convert.ToInt32(DT.Rows[i]["end_number"]);
                                    break;
                                case "2":   //網路
                                    start_number_type2 = Convert.ToInt32(DT.Rows[i]["start_number"]);
                                    end_number_type2 = Convert.ToInt32(DT.Rows[i]["end_number"]);
                                    break;
                            }
                        }
                    }
                }
            }
            #endregion

            div_search.Visible =
            div_list.Visible = false;
        }
        else
        {
            //無未補單的貨號
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇要補單貨號!');</script>", false);
            return;

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        lbErr.Text = "";
        int ttpieces = 0;
        int ttplates = 0;
        int ttcbm = 0;
        int ttcollection_money = 0;
        int ttarrive_to_pay = 0;
        int ttarrive_to_append = 0;
        int remote_fee = 0;


        try
        {
            ttpieces = Convert.ToInt32(pieces.Text);
        }
        catch { }

        try
        {
            ttplates = Convert.ToInt32(plates.Text);
        }
        catch { }

        try
        {
            ttcbm = Convert.ToInt32(cbm.Text);
        }
        catch { }

        switch (rbcheck_type.SelectedValue)
        {
            case "01":
            case "04":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入板數";
                    plates.Focus();
                }
                break;
            case "02":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入件數";
                    pieces.Focus();
                }
                break;
            case "03":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入才數";
                    cbm.Focus();
                }
                break;
        }
        lbErr.Visible = (lbErr.Text != "");
        if (lbErr.Text == "" && lbErr_chknum.Text == "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@pricing_type", rbcheck_type.SelectedValue);                     //計價模式 (01:論板、02:論件、03論才、04論小板)
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
            cmd.Parameters.AddWithValue("@check_number", add_check_number.Text);                         //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
            cmd.Parameters.AddWithValue("@order_number", "");                                              //訂單號碼
            cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
            cmd.Parameters.AddWithValue("@receive_customer_code", "");                                    //收貨人編號
            cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
            cmd.Parameters.AddWithValue("@receive_tel1", "");                                             //電話1
            cmd.Parameters.AddWithValue("@receive_tel1_ext", "");                                         //電話分機
            cmd.Parameters.AddWithValue("@receive_tel2", "");                                             //電話2
            cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
            cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
            cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
            remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
            cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
            cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code.SelectedValue);             //到著碼
            cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", 0);                //到站領貨 0:否、1:是
            //if (cbarrive.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
            //}


            try
            {
                ttcollection_money = Convert.ToInt32(collection_money.Text);
            }
            catch { }

            try
            {
                ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
            }
            catch { }

            try
            {
                ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
            }
            catch { }

            cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
            cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
            cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數
            cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加
            cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
            cmd.Parameters.AddWithValue("@send_tel", "");                           //寄件人電話
            cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
            cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
            cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
            cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
            cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
            cmd.Parameters.AddWithValue("@arrive_mobile", "");                                            //收件人-手機1  
            cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
            cmd.Parameters.AddWithValue("@invoice_memo", "");                                             //備註
            cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Text);                              //說明
            cmd.Parameters.AddWithValue("@product_category", "");                                         //商品種類
            cmd.Parameters.AddWithValue("@special_send", "");                                             //特殊配送
            cmd.Parameters.AddWithValue("@arrive_assign_date", DBNull.Value);                             //指定日
            cmd.Parameters.AddWithValue("@time_period", "");                                              //時段
            cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
            cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
            cmd.Parameters.AddWithValue("@supplier_code", supplier_code.Text);                             //配送商代碼
            //cmd.Parameters.AddWithValue("@supplier_name", supplier_name.Text);                           //配送商名稱
            cmd.Parameters.AddWithValue("@print_date", Shipments_date.Text);                               //出貨日期
            cmd.Parameters.AddWithValue("@add_transfer", 0);
            cmd.Parameters.AddWithValue("@print_flag", 0);
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 

            cmd.CommandText = dbAdapter.SQLdosomething("tcDeliveryRequests", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            cmd.Dispose();
            Clear();
            readdata();

            divAdd.Visible = false;
            div_search.Visible =
            div_list.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');</script>", false);  //parent.location.reload(true);
            return;
        }

    }

    protected void btnCanle_Click(object sender, EventArgs e)
    {
        divAdd.Visible = false;
        div_search.Visible =
        div_list.Visible = true;
    }


    protected void btnQry_Click(object sender, EventArgs e)
    {
        readdata();
    }


    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("選擇鄉鎮區", ""));
        }
    }

    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        //customer_code.Text = dlcustomer_code.SelectedValue;
        //顯示寄件人資料
        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dt;
            //cmd.CommandText = "select * from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue.ToString() + "' ";
            cmd.CommandText = @"Select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_name , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + dlcustomer_code.SelectedValue.ToString() + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                //send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();         //區配商代碼

            }
        }
    }

    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (ssSupplier_code != "")
        {
            switch (ssManager_type)
            {
                case "0":
                case "1":
                case "4":
                    cmd2.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                    break;
                default:
                    cmd2.Parameters.AddWithValue("@master_code", ssSupplier_code);
                    wherestr = " and master_code like ''+@master_code+'%' ";
                    break;
            }
        }

        cmd2.CommandText = string.Format(@"select customer_code , customer_code+ '-' + customer_shortname as name  from tbCustomers with(Nolock) where stop_shipping_code = '0' and pricing_code = '" + rbcheck_type.SelectedValue + "' {0} order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion
        Clear_customer();
    }

    protected void Clear_customer()
    {
        supplier_code.Text = "";
        send_contact.Text = "";   //名稱
        //send_tel.Text = "";           // 電話
        send_city.SelectedValue = "";   //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";   //出貨地址-鄉鎮市區
        send_address.Text = "";        //出貨地址-路街巷弄號
    }

    protected void Clear()
    {
        dlcustomer_code.SelectedIndex = 0;
        supplier_code.Text = "";
        check_type.SelectedIndex = 0;
        subpoena_category.SelectedIndex = 0;
        receive_contact.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        area_arrive_code.Items.Clear();
        pieces.Text = "";
        plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        arrive_to_pay_freight.Text = "";
        arrive_to_pay_append.Text = "";
        send_contact.Text = "";
        send_city.SelectedIndex = 0;
        send_area.Items.Clear();
        send_address.Text = "";
        invoice_desc.Text = "";
        receipt_flag.Checked = false;
        pallet_recycling_flag.Checked = false;
        Shipments_date.Text = "";

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 到著站簡碼
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                               left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                               where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            area_arrive_code.DataSource = dbAdapter.getDataTable(cmd7);
            area_arrive_code.DataValueField = "supplier_code";
            area_arrive_code.DataTextField = "showname";
            area_arrive_code.DataBind();
            if (area_arrive_code.Items.Count > 0) area_arrive_code.SelectedIndex = 0;
        }
        #endregion
    }

    protected Boolean Chk_check_number2()
    {
        Boolean IsOK = true;
        //託運單號
        //暫時僅用10碼，且貨號前方不補零。新竹規則MOD(num, 7) 前9碼 + 0~6檢碼
        lbErr_chknum.Text = "";
        if (add_check_number.Text.Length != 10 || add_check_number.Text.Length != 12)
        {
            lbErr_chknum.Text = "託運單號錯誤，長度應為10。";
            IsOK = false;
        }

        if (IsOK)
        {
            if (add_check_number.Text.Length == 10)
            {
                int num = Convert.ToInt32(add_check_number.Text.Substring(0, 9));
                //竹運
                if (supplier_code.Text != "" && (supplier_code.Text.Substring(0, 3) == "001" || supplier_code.Text.Substring(0, 3) == "002"))
                {
                    //not 8158486615 and 8158586601
                    //not 5920000000 and 5929999999
                    //not 8347120000 and 8347129999
                    //not 8819346760 and 8819406750
                    //not 9000200010 and 9000229999
                    //not 8283490025 and 8286490015


                }
                else
                {
                    //  686355000 ~ 686404999  或
                    //  828349002 ~ 828649001
                    if (num >= start_number_type1 && num <= end_number_type1)
                    {
                        int chk = num % 7;
                        int lastnum = Convert.ToInt32(add_check_number.Text.Substring(9, 1));
                        if (lastnum != chk)
                        {
                            lbErr_chknum.Text = "非有效的託運單號，請檢查編號是否正確";
                            IsOK = false;
                        }
                    }
                    else if (num >= start_number_type2 && num <= end_number_type2)
                    {
                        int chk = num % 7;
                        int lastnum = Convert.ToInt32(add_check_number.Text.Substring(9, 1));
                        if (lastnum != chk)
                        {
                            lbErr_chknum.Text = "非有效的託運單號，請檢查編號是否正確";
                            IsOK = false;
                        }
                    }
                    else
                    {
                        lbErr_chknum.Text = "非有效的託運單號,應落在 " + start_number_type1.ToString() + " ~ " + end_number_type1.ToString() + " <br>或 " + start_number_type2.ToString() + " ~ " + end_number_type2.ToString() + "之間，請檢查編號是否正確";
                        IsOK = false;
                    }
                }
            }
        }

        if (IsOK)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = @"SELECT request_id FROM tcDeliveryRequests With(Nolock) WHERE check_number =N'" + add_check_number.Text + "'";
                cmd.CommandText = strSQL;

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        lbErr_chknum.Text = "此託運單號已設定，請重新確認!";
                        IsOK = false;
                    }
                }
            }
        }


        lbErr_chknum.Visible = !IsOK;
        return IsOK;
    }




    /// <summary>取得匯出DATATABLE</summary>
    /// <returns></returns>
    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         No = p.Field<Int64>("NO"),
                         貨號 = p.Field<string>("check_number"),
                         作業時間 = p.Field<string>("scan_date"),
                         作業 = p.Field<string>("scan_name"),
                         站所 = p.Field<string>("supplier_name"),
                         到著發送站 = "",
                         件數 = "1",
                         班次代號 = "",
                         狀態 = p.Field<string>("status"),
                         運輸方式 = "",
                         註區 = "",
                         員工 = p.Field<string>("driver_code") + p.Field<string>("driver_name")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"竹運應補單據清單";

        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdAdd")
        {
            string log_id = ((HiddenField)e.Item.FindControl("Hid_logid")).Value.ToString().Trim();
            string check_number = ((HiddenField)e.Item.FindControl("Hid_check_number")).Value.ToString().Trim();
            string scan_date = ((Label)e.Item.FindControl("lbscan_date")).Text.ToString().Trim();
            add_check_number.Text = check_number;
            btnAdd_Click(null, null);
            Shipments_date.Text = Convert.ToDateTime(scan_date).ToString("yyyy/MM/dd");

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"竹運應補單據清單{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }
}