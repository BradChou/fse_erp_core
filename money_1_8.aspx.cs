﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using Ionic.Zip;
using System.Collections;
using System.Drawing;

public partial class money_1_8 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = DateTime.Today.ToString("yyyy/MM/") + "01"; //yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            string wherestr = "";
            #region 
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }
            cmd1.CommandText = string.Format(@"Select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  
                                               from tbSuppliers with(nolock)
                                               where active_flag= 1 and ISNULL(supplier_code ,'') <> '' {0} 
                                               order by supplier_code", wherestr);
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (manager_type == "0" || manager_type == "1" || Session["account_code"].ToString() == "9990042") Suppliers.Items.Insert(0, new ListItem("全部", ""));
            Suppliers_SelectedIndexChanged(null, null);

            #endregion

        }

    }


    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text != "")
            {
                cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                wherestr = "  and supplier_code=@Suppliers";
            }


            if (Master_code != "")
            {
                cmd.Parameters.AddWithValue("@Master_code", (manager_type == "3" || manager_type == "5") ? Master_code : "");
                wherestr += "  and customer_code like ''+ @Master_code + '%'";
            }

            //cmd.CommandText = string.Format(@"WITH cus AS
            //                                (
            //                                   SELECT *,
            //                                         ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
            //                                   FROM tbCustomers with(nolock) where 1=1 and supplier_code <> '001' and stop_shipping_code = '0' and supplier_id <> '0000' {0}
            //                                )
            //                                SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname
            //                                FROM cus
            //                                WHERE rn = 1 ", wherestr);
            cmd.CommandText = string.Format(@"WITH cus AS
                                                (
                                                    SELECT *,
                                                            ROW_NUMBER() OVER (PARTITION BY master_code,second_id ORDER BY master_code DESC,pricing_code) AS rn
                                                    FROM tbCustomers where 1=1 and stop_shipping_code = '0' and supplier_id <> '0000' {0}
                                                )
                                                SELECT supplier_id, master_code, master_code + second_id 'branch', customer_name , supplier_id + second_id + '-' +  customer_shortname  as showname
                                                FROM cus
                                                WHERE rn = 1", wherestr);

            second_code.DataSource = dbAdapter.getDataTable(cmd);
            second_code.DataValueField = "branch";
            second_code.DataTextField = "showname";
            second_code.DataBind();
            second_code.Items.Insert(0, new ListItem("全部", ""));
        }
        #endregion

        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
    }


    protected void btExport_Click(object sender, EventArgs e)
    {
        string customer_code = string.Empty;
        string randomCode = Guid.NewGuid().ToString().Replace("-", "");
        randomCode = randomCode.Length > 15 ? randomCode.Substring(0, 15) : randomCode;

        //string path = Request.PhysicalApplicationPath + @"files\money\應收帳款(" + Convert.ToDateTime(dates.Text).ToString("yyyyMMdd") + "-" + Convert.ToDateTime(datee.Text).ToString("yyyyMMdd") + ")";
        string path = Request.PhysicalApplicationPath + @"files\money\應收帳款(" + randomCode + ")";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }


        if (second_code.SelectedValue == "" && second_code.Items.Count > 1)
        {
            for (int i = 1; i <= second_code.Items.Count - 1; i++)
            {
                customer_code = second_code.Items[i].Value;
                tbCustomers _tbCustomers = getCustomers(customer_code);
                #region 匯出報表
                try
                {
                    export(customer_code, _tbCustomers, path);
                }
                catch (Exception ex)
                {
                    string ErrStr = ex.Message;
                }
                
                #endregion

            }
        }
        else if (second_code.SelectedValue != "")
        {
            customer_code = second_code.SelectedValue.ToString();
            tbCustomers _tbCustomers = getCustomers(customer_code);
            #region 匯出報表
            export(customer_code, _tbCustomers, path);
            #endregion
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可出帳客戶');</script>", false);
            return;
        }


        #region 壓縮檔案
        ZipFiles(path, string.Empty, string.Empty);
        #endregion
    }

    private tbCustomers getCustomers(string customer_code)
    {
        tbCustomers _tbCustomers = null;
        #region 客代資訊
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "Select top 1 * from tbCustomers with(nolock) where customer_code like ''+@customer_code +'%' order by customer_code";
            cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                _tbCustomers = DataTableExtensions.ToList<tbCustomers>(_dt).ToList().FirstOrDefault();
            }
        }
        #endregion
        return _tbCustomers;
    }


    protected string export(string customer_code, tbCustomers _tbCustomers, string path)
    {
        string filename = "";
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = new Utility.AccountsReceivableInfo();
        Utility.AccountsReceivableInfo _info_01 = new Utility.AccountsReceivableInfo();
        Utility.AccountsReceivableInfo _info_05 = new Utility.AccountsReceivableInfo();
        string Title = "峻富物流股份有限公司";
        string supplier_name = Suppliers.SelectedItem.Text;
        int ttRow = 0;
        string randomCode = "";

        #region
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"Select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
                                where  type = @type  and customer_code like + @supplier_code + '%' and close_enddate >= @dates and close_enddate < @datee
                                FOR XML PATH('')),1,1,'') 'randomCode'";
            cmd.Parameters.AddWithValue("@type", 0);                      //收入
            cmd.Parameters.AddWithValue("@supplier_code", customer_code); //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);                                                        //日期起
            cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));  //日期迄
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                randomCode = _dt.Rows[0]["randomCode"].ToString();
                if (randomCode != "")
                    using (SqlCommand cmd_a = new SqlCommand())
                    {
                        cmd_a.CommandText = "usp_GetAccountsReceivablebyJUNFU";
                        cmd_a.CommandType = CommandType.StoredProcedure;
                        cmd_a.Parameters.AddWithValue("@pricing_type", "'01','02','03','04','05'");
                        cmd_a.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue);
                        cmd_a.Parameters.AddWithValue("@randomCode", randomCode);
                        dt = dbAdapter.getDataTable(cmd_a);
                        _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, null, customer_code, "'01','02','03','04','05'", "", randomCode);

                        using (SqlCommand cmd_01 = new SqlCommand())
                        {
                            cmd_01.CommandText = "usp_GetSumAccountsReceivable";
                            cmd_01.CommandType = CommandType.StoredProcedure;
                            cmd_01.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                            cmd_01.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                            cmd_01.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                            cmd_01.Parameters.AddWithValue("@randomCode", randomCode);
                            cmd_01.Parameters.AddWithValue("@groupby", "");

                            using (DataTable dt_01 = dbAdapter.getDataTable(cmd_01))
                            {
                                if (dt_01.Rows.Count > 0)
                                {
                                    _info_01.subtotal = dt_01.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["subtotal"]) : 0;
                                    _info_01.tax = dt_01.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["tax"]) : 0;
                                    _info_01.total = dt_01.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["total"]) : 0;
                                    _info_01.plates = dt_01.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["plates"]) : 0;
                                    _info_01.pieces = dt_01.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["pieces"]) : 0;
                                    _info_01.cbm = dt_01.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["cbm"]) : 0;
                                    _info_01.special_fee = dt_01.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["special_fee"]) : 0;
                                }

                            }
                        }

                        using (SqlCommand cmd_05 = new SqlCommand())
                        {
                            cmd_05.CommandText = "usp_GetSumAccountsReceivable";
                            cmd_05.CommandType = CommandType.StoredProcedure;
                            cmd_05.Parameters.AddWithValue("@pricing_type", "05");  //計價模式
                            cmd_05.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                            cmd_05.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                            cmd_05.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                            cmd_05.Parameters.AddWithValue("@randomCode", randomCode);
                            cmd_05.Parameters.AddWithValue("@groupby", "");

                            using (DataTable dt_05 = dbAdapter.getDataTable(cmd_05))
                            {
                                if (dt_05.Rows.Count > 0)
                                {
                                    _info_05.subtotal = dt_05.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["subtotal"]) : 0;
                                    _info_05.tax = dt_05.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["tax"]) : 0;
                                    _info_05.total = dt_05.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["total"]) : 0;
                                    _info_05.plates = dt_05.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["plates"]) : 0;
                                    _info_05.pieces = dt_05.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["pieces"]) : 0;
                                    _info_05.cbm = dt_05.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["cbm"]) : 0;
                                    _info_05.special_fee = dt_05.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["special_fee"]) : 0;
                                }

                            }
                        }

                    }
            }
        }
        #endregion

        if (dt != null && dt.Rows.Count > 0)
        {
            using (ExcelPackage p = new ExcelPackage())
            {
                ExcelPackage pck = new ExcelPackage();
                var sheet1 = pck.Workbook.Worksheets.Add("應收帳款");

                #region sheet1                
                //檔案邊界
                sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                sheet1.PrinterSettings.HorizontalCentered = true;

                //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向                
                //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");
                sheet1.PrinterSettings.RepeatRows = sheet1.Cells["1:8"];       //重複表頭    
                sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小

                //所有欄放入同一頁面
                sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                sheet1.PrinterSettings.FitToWidth = 1;
                sheet1.PrinterSettings.FitToHeight = 0;

                //欄寬
                sheet1.Column(1).Width = 5;
                sheet1.Column(2).Width = 11;
                sheet1.Column(3).Width = 11;
                sheet1.Column(4).Width = 9;
                //sheet1.Column(5).Width = 18;
                sheet1.Column(5).Width = 11;
                sheet1.Column(6).Width = 20;
                sheet1.Column(7).Width = 22;
                sheet1.Column(8).Width = 7;
                sheet1.Column(9).Width = 7;
                sheet1.Column(10).Width = 9;



                sheet1.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
                sheet1.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
                sheet1.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[1, 1, 1, 11].Style.Font.Size = 18;
                sheet1.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                sheet1.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中


                sheet1.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[2, 1, 2, 11].Style.Font.Size = 12;
                sheet1.Cells[2, 1, 2, 11].Merge = true;

                using (SqlCommand cmda = new SqlCommand())
                {
                    cmda.CommandText = @"DECLARE @statement nvarchar(1000)
                                         set @statement='SELECT min( close_begdate)  ''close_begdate'', max(close_enddate) ''close_enddate''' +
                                        ' FROM ttCheckoutCloseLog with(nolock)'+
                                        ' WHERE (src = ''Delivery'') and randomCode IN ('+ @randomCode +')'
                                        EXEC sp_executesql  @statement ";
                    cmda.Parameters.AddWithValue("@randomCode", randomCode);  //結帳碼
                    DataTable _dt = dbAdapter.getDataTable(cmda);
                    if (_dt.Rows.Count > 0)
                    {
                        sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + Convert.ToDateTime(_dt.Rows[0]["close_begdate"]).ToString("yyyy/MM/dd") + "~" + Convert.ToDateTime(_dt.Rows[0]["close_enddate"]).ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    }
                }
                sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                sheet1.Cells[3, 1, 3, 11].Merge = true;     //合併儲存格
                sheet1.Cells[3, 1, 3, 1].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[3, 1, 3, 1].Style.Font.Size = 12;
                sheet1.Cells[3, 1, 3, 1].Value = "自營客戶：" + _tbCustomers.customer_name;
                sheet1.Cells[3, 1, 3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                sheet1.Cells[4, 1, 4, 1].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[4, 1, 4, 1].Style.Font.Size = 12;
                sheet1.Cells[4, 1, 4, 1].Value = "統一編號：" + _tbCustomers.uniform_numbers;
                sheet1.Cells[4, 1, 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet1.Cells[4, 1, 4, 4].Merge = true;

                sheet1.Cells[5, 1, 5, 1].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[5, 1, 5, 1].Style.Font.Size = 12;
                sheet1.Cells[5, 1, 5, 1].Value = "聯絡電話：" + _tbCustomers.telephone;
                sheet1.Cells[5, 1, 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet1.Cells[5, 1, 5, 4].Merge = true;
                sheet1.Cells[5, 7, 5, 7].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[5, 7, 5, 7].Style.Font.Size = 12;
                sheet1.Cells[5, 7, 5, 7].Value = "對帳人：" + _tbCustomers.shipments_principal;
                sheet1.Cells[5, 7, 5, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[5, 7, 5, 11].Merge = true;

                sheet1.Cells[6, 1, 6, 1].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[6, 1, 6, 1].Style.Font.Size = 12;
                sheet1.Cells[6, 1, 6, 1].Value = "地址：" + _tbCustomers.shipments_city + _tbCustomers.shipments_area + _tbCustomers.shipments_road;
                sheet1.Cells[6, 1, 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet1.Cells[6, 1, 6, 5].Merge = true;
                sheet1.Cells[6, 7, 6, 7].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[6, 7, 6, 7].Style.Font.Size = 12;
                sheet1.Cells[6, 7, 6, 7].Value = _tbCustomers.shipments_email;
                sheet1.Cells[6, 7, 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[6, 7, 6, 11].Merge = true;

                sheet1.Cells[7, 1, 7, 1].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[7, 1, 7, 1].Style.Font.Size = 12;
                sheet1.Cells[7, 1, 7, 1].Value = "開立發票號碼：";
                sheet1.Cells[7, 1, 7, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                sheet1.Cells[7, 1, 7, 4].Merge = true;
                sheet1.Cells[7, 7, 7, 7].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[7, 7, 7, 7].Style.Font.Size = 12;
                sheet1.Cells[7, 7, 7, 7].Value = _tbCustomers.billing_special_needs;
                sheet1.Cells[7, 7, 7, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[7, 7, 7, 11].Merge = true;


                sheet1.Cells[8, 1, 8, 11].Style.Font.Name = "微軟正黑體";
                sheet1.Cells[8, 1, 8, 11].Style.Font.Size = 12;
                sheet1.Cells[8, 1, 8, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[8, 1, 8, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                sheet1.Cells[8, 1, 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium;    //框線
                sheet1.Cells[8, 1, 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;    //框線
                sheet1.Cells[8, 11, 8, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                sheet1.Cells[8, 1, 8, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Dotted; //框線
                sheet1.Cells[8, 1, 8, 11].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                sheet1.Cells[8, 1, 8, 11].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);//设置背景色
                sheet1.Row(8).Height = 25;


                sheet1.Cells[8, 1].Value = "序號";
                sheet1.Cells[8, 2].Value = "發送日期";
                sheet1.Cells[8, 3].Value = "配送日期";
                sheet1.Cells[8, 4].Value = "配送模式";
                //sheet1.Cells[8, 5].Value = "客戶名稱";
                sheet1.Cells[8, 5].Value = "貨號";
                sheet1.Cells[8, 6].Value = "發送區";
                sheet1.Cells[8, 7].Value = "到著區";
                sheet1.Cells[8, 8].Value = "件數";
                sheet1.Cells[8, 9].Value = "板數";
                sheet1.Cells[8, 10].Value = "加收費";
                sheet1.Cells[8, 11].Value = "貨件運費";

                ttRow = 9;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sheet1.Cells[ttRow, 1].Value = (i + 1).ToString();
                    sheet1.Cells[ttRow, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet1.Cells[ttRow, 2].Value = dt.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                    sheet1.Cells[ttRow, 3].Value = dt.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                    sheet1.Cells[ttRow, 4].Value = dt.Rows[i]["配送模式"].ToString();
                    //sheet1.Cells[ttRow, 5].Value = dt.Rows[i]["客戶名稱"].ToString();
                    sheet1.Cells[ttRow, 5].Value = dt.Rows[i]["貨號"].ToString();
                    sheet1.Cells[ttRow, 6].Value = dt.Rows[i]["發送區2"].ToString();
                    sheet1.Cells[ttRow, 7].Value = dt.Rows[i]["到著區2"].ToString();
                    sheet1.Cells[ttRow, 8].Value = Convert.ToInt32(dt.Rows[i]["件數"]);
                    sheet1.Cells[ttRow, 8].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow, 9].Value = Convert.ToInt32(dt.Rows[i]["板數"]);
                    sheet1.Cells[ttRow, 9].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow, 10].Value = Convert.ToInt32(dt.Rows[i]["加收費"]);
                    sheet1.Cells[ttRow, 10].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow, 11].Value = Convert.ToInt32(dt.Rows[i]["貨件費用"]);
                    sheet1.Cells[ttRow, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[ttRow, 1, ttRow, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                    sheet1.Cells[ttRow, 11, ttRow, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線


                    if (i == dt.Rows.Count - 1 && _info != null)
                    {
                        if (_info_01 != null)
                        {
                            sheet1.Cells[ttRow + 1, 7].Value = "棧板收入小計";
                            sheet1.Cells[ttRow + 1, 8].Value = _info_01.pieces;
                            sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 9].Value = _info_01.plates;
                            sheet1.Cells[ttRow + 1, 9].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 11].Value = _info_01.subtotal;
                            sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                            sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ttRow += 1;
                        }

                        if (_info_05 != null)
                        {
                            sheet1.Cells[ttRow + 1, 7].Value = "專車收入小計";
                            sheet1.Cells[ttRow + 1, 8].Value = _info_05.pieces;
                            sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 9].Value = _info_05.plates;
                            sheet1.Cells[ttRow + 1, 9].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 11].Value = _info_05.subtotal;
                            sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                            sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            ttRow += 1;
                        }
                        sheet1.Cells[ttRow + 1, 7].Value = "總計";
                        sheet1.Cells[ttRow + 1, 8].Formula = "=SUM(H9:H" + (ttRow - 2).ToString() + ")";
                        //sheet1.Cells[ttRow + 1, 9].Value = _info.pieces;
                        sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 9].Formula = "=SUM(I9:I" + (ttRow - 2).ToString() + ")";
                        //sheet1.Cells[ttRow + 1, 10].Value = _info.plates;
                        sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Formula = "=SUM(K9:K" + (ttRow - 2).ToString() + ")";
                        //sheet1.Cells[ttRow + 1, 10].Value = _info.subtotal;
                        sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                        sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                        sheet1.Cells[ttRow + 2, 7].Value = "5%營業稅";
                        sheet1.Cells[ttRow + 2, 11].Value = _info.tax;
                        sheet1.Cells[ttRow + 2, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 2, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[ttRow + 2, 1, ttRow + 2, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[ttRow + 2, 1, ttRow + 2, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                        sheet1.Cells[ttRow + 2, 11, ttRow + 3, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 2, 1, ttRow + 2, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                        sheet1.Cells[ttRow + 3, 7].Value = "應收帳款";
                        sheet1.Cells[ttRow + 3, 11].Formula = "=SUM(K" + (ttRow + 1).ToString() + ":K" + (ttRow + 2).ToString() + ")";
                        //sheet1.Cells[ttRow + 3, 11].Value = _info.total;
                        sheet1.Cells[ttRow + 3, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 3, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[ttRow + 3, 1, ttRow + 3, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[ttRow + 3, 1, ttRow + 3, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                        sheet1.Cells[ttRow + 3, 11, ttRow + 3, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 3, 1, ttRow + 3, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;


                    }
                    ttRow += 1;
                }

                sheet1.Cells[ttRow + 3, 1].Value = "滙入行：玉山銀行埔墘分行 808-0174";
                sheet1.Cells[ttRow + 3, 1, ttRow + 3, 4].Merge = true;
                sheet1.Cells[ttRow + 4, 1].Value = "帳號：0174940009888";
                sheet1.Cells[ttRow + 4, 1, ttRow + 4, 4].Merge = true;
                sheet1.Cells[ttRow + 5, 1].Value = "戶名：峻富物流股份有限公司";
                sheet1.Cells[ttRow + 5, 1, ttRow + 5, 4].Merge = true;

                sheet1.Cells[ttRow + 7, 1, ttRow + 7, 10].Style.Font.Size = 14;
                sheet1.Cells[ttRow + 7, 1].Value = "核准：";
                sheet1.Cells[ttRow + 7, 1, ttRow + 7, 3].Merge = true;
                sheet1.Cells[ttRow + 7, 4].Value = "初審：";
                sheet1.Cells[ttRow + 7, 7].Value = "主管：";
                sheet1.Cells[ttRow + 7, 9].Value = "製表：";
                //sheet1.Cells.AutoFitColumns();


                //頁尾加入頁次
                sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                #endregion

                filename = _tbCustomers.customer_shortname.Trim() + @"應收帳款明細表" + ".xlsx";
                //string path = Request.PhysicalApplicationPath + @"files\money\" + filename;
                Stream stream = File.Create(path + @"\" + filename);
                pck.SaveAs(stream);
                stream.Close();

            }
        }



        return filename;
    }

    private void ZipFiles(string path, string password, string comment)
    {
        string zipPath = path + ".zip";
        //ZipFile zip = new ZipFile();
        //if (password != null && password != string.Empty) zip.Password = password;
        //if (comment != null && comment != "") zip.Comment = comment;
        //ArrayList files = GetFiles(path);
        //foreach (string f in files)
        //{
        //    zip.AddFile(f, string.Empty);//第二個參數設為空值表示壓縮檔案時不將檔案路徑加入
        //}
        //zip.Save(zipPath);

        //System.Text.Encoding.Default解决中文文件夹名称乱码
        using (ZipFile zf = new ZipFile(System.Text.Encoding.Default))
        {
            if (password != null && password != string.Empty) zf.Password = password;
            if (comment != null && comment != "") zf.Comment = comment;
            zf.AddDirectory(path);
            //压缩之后保存路径及压缩文件名
            zf.Save(zipPath);
        }


        string filename = System.IO.Path.GetFileName(zipPath);
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + filename));
        //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
        Response.WriteFile(zipPath);
        Response.End();
    }

    //讀取目錄下所有檔案
    private static ArrayList GetFiles(string path)
    {
        ArrayList files = new ArrayList();

        if (Directory.Exists(path))
        {
            files.AddRange(Directory.GetFiles(path));
        }

        return files;
    }

    public class tbCustomers
    {
        public string customer_name { get; set; }
        public string customer_shortname { get; set; }
        public string uniform_numbers { get; set; }
        public string telephone { get; set; }
        public string shipments_principal { get; set; }
        public string shipments_city { get; set; }
        public string shipments_area { get; set; }
        public string shipments_road { get; set; }
        public string shipments_email { get; set; }
        public string billing_special_needs { get; set; }

    }


}