﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_1_3_1.aspx.cs" Inherits="LT_transport_1_3_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label ID="Label1" runat="server" Text="今日出貨件數：">            
    </asp:Label><asp:TextBox ID="Peices" runat="server"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
        ControlToValidate="Peices" runat="server"
        ErrorMessage="出貨件數只能輸入數字"
        ValidationExpression="\d+">
    </asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        ControlToValidate="Peices" runat="server"
        ErrorMessage="出貨件數為必填項目"></asp:RequiredFieldValidator><br />
    <br />
    <br />

    <asp:Label ID="Label3" runat="server" Text="承運載具："></asp:Label>
    <asp:DropDownList ID="Carrier" runat="server">
        <asp:ListItem Text="紙箱" Value="1"></asp:ListItem>
        <asp:ListItem Text="速配袋" Value="2"></asp:ListItem>
    </asp:DropDownList>
    <br /><br />

    <asp:Label ID="Label2" runat="server" Text="貨件規格："></asp:Label>
    <asp:DropDownList ID="PickUpType" runat="server">
        <asp:ListItem Text="箱型貨車" Value="1"></asp:ListItem>
        <asp:ListItem Text="機車" Value="2"></asp:ListItem>
    </asp:DropDownList>
    <br /><br />

    <p>
        <asp:Label ID="Label4" runat="server" Text="收件時間："></asp:Label>

        <asp:DropDownList ID="ReceiveTime" runat="server">
            <asp:ListItem Text="隨時" Value="0"></asp:ListItem>
            <asp:ListItem Text="上午 9:00 ~ 12:00" Value="1"></asp:ListItem>
            <asp:ListItem Text="下午 12:00 ~ 18:00" Value="2"></asp:ListItem>
        </asp:DropDownList>
    </p>
    <br /><br />

    <asp:Label ID="Label5" runat="server" Text="提醒事項"></asp:Label>
    <p>
        <asp:TextBox ID="Memo" TextMode="MultiLine" runat="server" Height="143px" Width="425px"></asp:TextBox>
    </p>
    <asp:Button class="btn btn-primary" ID="Button1" runat="server" Text="派員收件" OnClick="Button1_Click" />
    <!--<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/LT_transport_1_3.aspx">回到整批匯入</asp:HyperLink>-->

</asp:Content>
