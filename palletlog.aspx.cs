﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class palletlog : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            lbcheck_number.Text = Request.QueryString["check_number"].ToString();
            //string request_id = Request.QueryString["request_id"] != null ? Request.QueryString["request_id"].ToString() :"";
            readdata(lbcheck_number.Text);
        }
    }

    private void readdata(string check_number)
    {
        if (check_number != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@check_number", check_number);
                cmd.CommandText = string.Format(@"
                                                DECLARE @statement nvarchar(4000)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                SELECT @ORDER_NUMBER=order_number  FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                 
                                                SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item,  CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code 
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 '
                                                SET @where = ' where 1=1  and  A.scan_date  >=  DATEADD(MONTH,-3,getdate()) and A.scan_item in(''1'',''2'',''3'',''5'',''6'',''7'')'

                                                IF @ORDER_NUMBER <> '' BEGIN 
                                                    SET @statement = 
	                                                'declare @tbl table(check_number nvarchar(10))
	                                                insert into @tbl
	                                                select check_number from tcDeliveryRequests with(nolock) where order_number ='''+ @ORDER_NUMBER + ''';'
	                                                + @statement
	                                                SET @where =@where+ ' and (A.check_number = '+@check_number+'
	                                                            or A.check_number in(select check_number  from @tbl))'
                                                END ELSE BEGIN 
                                                    SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                END

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List_02.DataSource = dt;
                    New_List_02.DataBind();
                }

            }
        }        

    }

    //private void readdata( string request_id)
    //{
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        string strWhereCmd = "order by udate desc";
    //        cmd.Parameters.Clear();
    //        cmd.CommandText = string.Format(@"Select return_cnt, CONVERT(varchar(100),return_date,120) as return_date  , CONVERT(varchar(100),udate,120) as udate,uuser 
    //                                          from ttPlletReturn With(Nolock) 
    //                                          where 0 = 0 and request_id  = @request_id {0} ", strWhereCmd);
    //        cmd.Parameters.AddWithValue("@request_id", request_id);
    //        using (DataTable dt = dbAdapter.getDataTable(cmd))
    //        {
    //            New_List.DataSource = dt;
    //            New_List.DataBind();
    //        }
    //    }           

    //}


}