﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_1_edit.aspx.cs" Inherits="assets1_1_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            $("#<%= stop_type.ClientID%>").change(function() {
                 var n = $(this).val();
                 switch (n)
                 {
                     case '0':
                         $("#<%= stop_date.ClientID%>").hide();
                         $("#<%= stop_date.ClientID%>").val('');
                         break; 
                     default:
                         $("#<%= stop_date.ClientID%>").show();
                         $("#<%= stop_date.ClientID%>").focus();
                         break; 
                     
                 }
            });
        });

        function ValidateFloat2(e, pnumber) {
            var re = /^[+-]?\d*\.?\d{0,3}$/;
            if (!re.test(pnumber)) {
                var newValue = /^[+-]?\d*\.?\d{0,3}$/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                    alert('限小數3位');
                }
            } 
            return false;

        }

        //function ValidateFloat2(e, pnumber) {
        //    if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
        //        var newValue = /\d+[.]?[1-9]?/.exec(e.value);
        //        if (newValue != null) {
        //            e.value = newValue;
        //        }
        //        else {
        //            e.value = "";
        //        }
        //    }
        //    return false;
        //}

        

    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 5px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }
        ._checkboxlist  {
            display: inline ;      
        }
        ._checkboxlist label {
            min-width :30px;     
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2 class="margin-bottom-10">車輛主檔維護-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
    <div class="templatemo-login-form">
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">車輛基本資料</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>車牌</label>
                            <asp:TextBox ID="car_license" runat="server" class="form-control"></asp:TextBox>
                            <asp:HiddenField ID="Hid_a_id" runat="server" />
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入車牌</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year"><span class="REDWD_b">*</span>年度</label>
                            <asp:TextBox ID="year" runat="server" class="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="year" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入年度</asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="month"><span class="REDWD_b">*</span>月份</label>
                            <asp:DropDownList ID="month" runat="server" class="form-control">
                                <asp:ListItem Value="">請選擇</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                                <asp:ListItem>4</asp:ListItem>
                                <asp:ListItem>5</asp:ListItem>
                                <asp:ListItem>6</asp:ListItem>
                                <asp:ListItem>7</asp:ListItem>
                                <asp:ListItem>8</asp:ListItem>
                                <asp:ListItem>9</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="re3" runat="server" ControlToValidate="month" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入月份</asp:RequiredFieldValidator>--%>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>所有權</label>
                            <asp:DropDownList ID="dlownership" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tonnes">噸數</label>
                            <asp:TextBox ID="tonnes" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        
                        <%--<div class="col-lg-4 bottnmargin form-inline">
                            <label for="purchase_date">購入日期</label>
                            <asp:TextBox ID="purchase_date" runat="server" class="form-control date_picker"   autocomplete="off"></asp:TextBox>
                        </div>--%>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand">廠牌</label>
                            <asp:TextBox ID="brand" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="dept">使用單位</label>
                            <asp:DropDownList ID="dept" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="car_retailer">車行</label>
                            <asp:DropDownList ID="car_retailer" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="owner">車主</label>
                            <asp:TextBox ID="owner" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="registration">車籍資料</label>
                            <asp:RadioButtonList ID="registration" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" CssClass="radio radio-success">
                                <asp:ListItem Value="True">有</asp:ListItem>
                                <asp:ListItem Value="False">無</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="engine_number">引擎號碼</label>
                            <asp:TextBox ID="engine_number" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="cc">排氣量 / CC</label>
                            <asp:TextBox ID="cc" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="age">車齡</label>
                            <asp:TextBox ID="age" runat="server" class="form-control" ReadOnly="true" ></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="next_inspection_date">下次驗車日期</label>
                            <asp:TextBox ID="next_inspection_date" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="memo">備註</label>
                            <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="stop_type">狀態</label>
                            <asp:DropDownList ID="stop_type" runat="server" class="form-control">
                                <asp:ListItem Value="0">正常</asp:ListItem>
                                <asp:ListItem Value="1">出售</asp:ListItem>
                                <asp:ListItem Value="2">繳銷</asp:ListItem>
                                <asp:ListItem Value="3">報廢</asp:ListItem>
                                <asp:ListItem Value="4">停牌</asp:ListItem>
                            </asp:DropDownList><asp:TextBox ID="stop_date" CssClass="form-control date_picker" runat="server" Style="display: none"  placeholder="請輸入停用日期"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info ">
                <div class="panel-heading">車輛配備</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tires_number">輪胎數</label>
                            <asp:TextBox ID="tires_number" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="cabin_type">車廂型式</label>
                            <asp:DropDownList ID="cabin_type" runat="server" class="form-control"></asp:DropDownList>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="temperate">溫層</label>
                            <asp:DropDownList ID="temperate" runat="server" class="form-control"></asp:DropDownList>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tailgate">是否有尾門</label>
                            <asp:RadioButtonList ID="tailgate" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" CssClass="radio radio-success">
                                <asp:ListItem Value="True">是</asp:ListItem>
                                <asp:ListItem Value="False">否</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="rb_vision_assist">視野輔助</label>
                            <asp:RadioButtonList ID="rb_vision_assist" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" CssClass="radio radio-success">
                                <asp:ListItem Value="True">是</asp:ListItem>
                                <asp:ListItem Value="False">否</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="ETC">ETC企業用戶</label>
                            <asp:RadioButtonList ID="rb_ETC" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" CssClass="radio radio-success" AutoPostBack="true" OnSelectedIndexChanged="rb_ETC_SelectedIndexChanged">
                                <asp:ListItem Value="True">是</asp:ListItem>
                                <asp:ListItem Value="False">否</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:TextBox ID="ETC_Account" runat="server" class="form-control" Visible ="false" Width="120px" placeholder="請輸入帳戶" MaxLength ="20"></asp:TextBox>
                            <asp:DropDownList ID="dlETC0" runat="server" class="form-control" Visible ="false" Width="120px">
                                <asp:ListItem Value ="" >請選擇</asp:ListItem>
                                <asp:ListItem Value ="1">未申辦</asp:ListItem>
                                <asp:ListItem Value ="2">待加入</asp:ListItem>
                            </asp:DropDownList>
                        </div>                                              

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tires_monthlyfee">輪胎統包</label>
                            <asp:TextBox ID="tires_monthlyfee" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>GPS廠商</label>
                            <div class="_checkboxlist ">
                                <asp:Repeater ID="rp_gps" runat="server">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hid_gps" runat="server" Value='<%# Eval("code_id") %>' />
                                        <asp:CheckBox ID="chkgps" CssClass=" checkbox checkbox-success" Text='<%# Eval("code_name") %>' Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.chk"))%>'   runat="server" />                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>           

                            <%--<asp:CheckBoxList ID="cb_GPS" runat="server" CssClass=" checkbox checkbox-success"  RepeatDirection="Horizontal" ></asp:CheckBoxList>--%>

                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="oil">油料</label>
                            <div class="_checkboxlist ">
                                <asp:Repeater ID="rp_oil" runat="server">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hid_oil" runat="server" Value='<%# Eval("code_id") %>' />
                                        <asp:CheckBox ID="chkoil" CssClass=" checkbox checkbox-success" Text='<%# Eval("code_name") %>' Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.chk"))%>'   runat="server" />
                                        <asp:TextBox ID="oil_cardid" runat="server" Width="120px" Text='<%# Eval("card_id") %>' />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>                            
                            <%--<asp:CheckBoxList ID="cb_oil" runat="server" CssClass=" checkbox checkbox-success"  RepeatDirection="Horizontal" ></asp:CheckBoxList>--%>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="oil_discount">油料折扣</label>
                            <%--<asp:TextBox ID="oil_discount2" runat="server" class="form-control" onkeyup="this.value=this.value.replace(^[0-9]*$,'')"></asp:TextBox>--%>
                            <asp:TextBox ID="oil_discount" runat="server" class="form-control" onkeyup="return ValidateFloat2(this,value)" Width="15%" ></asp:TextBox>(元)

                        </div>
                    </div>
                </div>
            </div>

           <%-- <div class="panel panel-success  ">
                <div class="panel-heading">使用狀況及業務分類</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="driver">司機</label>
                            <asp:TextBox ID="driver" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>業務別</label>
                            <asp:TextBox ID="Business" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="dlbusiness_model">營業模式</label>
                            <asp:DropDownList ID="dlbusiness_model" runat="server" class="form-control">
                            </asp:DropDownList>
                        </div>

                    </div>
                </div>
            </div>--%>

            <div class="panel panel-warning ">
                <div class="panel-heading">費用類別攤提設定</div>
                <div class="panel-body">
                    <div class="row ">                       

                        <div class="col-lg-4 bottnmargin form-inline">                            
                            <div class="_checkboxlist ">
                                <asp:Repeater ID="rp_fee" runat="server">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="Hid_fee" runat="server" Value='<%# Eval("code_id") %>' />
                                        <asp:CheckBox ID="chkfee" CssClass=" checkbox checkbox-success" Text='<%# Eval("code_name") %>' Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.chk"))%>'   runat="server" />
                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>                            
                            <%--<asp:CheckBoxList ID="cb_oil" runat="server" CssClass=" checkbox checkbox-success"  RepeatDirection="Horizontal" ></asp:CheckBoxList>--%>
                        </div>
                       
                    </div>
                </div>
            </div>

            <div class="panel  panel-danger   ">
                <div class="panel-heading">保險/稅務資料</div>
                <div class="panel-body">
                    <div class="row ">
                        <%--<div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>保險單號</label>
                            <asp:TextBox ID="Insurance_id" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="Insurance_name">保單名稱</label>
                            <asp:TextBox ID="Insurance_name" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>費用期間</label>
                            <asp:TextBox ID="Sdate" runat="server" class="form-control date_picker"></asp:TextBox>～
                                <asp:TextBox ID="Edate" runat="server" class="form-control date_picker"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>保費</label>
                            <asp:TextBox ID="premium" runat="server" class="form-control"></asp:TextBox>
                        </div>--%>

                        <div class="col-lg-12">
                            <table class="table table-striped ">
                                <tr class="tr-only-hide">
                                    <th class="_th">表單編號</th>
                                    <th class="_th">費用名稱</th>
                                    <th class="_th">費用期間</th>
                                    <th class="_th">金額</th>
                                    <th class="_th">攤提月份</th>
                                    <th class="_th">月攤提數</th>
                                    <th class="_th">累計攤提金額</th>
                                    <th class="_th">未攤提金額</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <ItemTemplate>
                                        <tr class="paginate">
                                            
                                            <td data-th="表單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.form_id").ToString())%></td>
                                            <td data-th="費用名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Insurance_name").ToString())%></td>                                            
                                            <td data-th="費用期間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sdtate","{0:yyyy/MM/dd}").ToString())%>~<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.edate","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>
                                            <td data-th="攤提月份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.apportion").ToString())%></td>
                                            <td data-th="月攤提數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.apportion_price","{0:N0}").ToString())%></td>
                                            <td data-th="累計攤提金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.amount_paid","{0:N0}").ToString())%></td>
                                            <td data-th="未攤提金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>                                            
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="8" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                        </div>
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>牌照稅</label>
                            <asp:TextBox ID="license_tax" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>燃料稅</label>
                            <asp:TextBox ID="fuel_tax" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>租金</label>
                            <asp:TextBox ID="rent" runat="server" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

           <%-- <div class="panel panel-warning  ">
                <div class="panel-heading">財產資料</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="serial">車輛代號</label>
                            <asp:TextBox ID="serial" runat="server" class="form-control" MaxLength="20"></asp:TextBox>

                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="improved_number">改良修理序號</label>
                            <asp:TextBox ID="improved_number" runat="server" class="form-control" MaxLength="5"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>會計科目</label>
                            <asp:DropDownList ID="acntid" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="quant">數量</label>
                            <asp:TextBox ID="quant" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="unit">單位</label>
                            <asp:TextBox ID="unit" runat="server" class="form-control"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="price">取得原價</label>
                            <asp:TextBox ID="price" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="residual_value">預留殘值</label>
                            <asp:TextBox ID="residual_value" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="useful_life">耐用年限</label>
                            <asp:TextBox ID="useful_life" runat="server" class="form-control" MaxLength="2" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="assets_name">車輛名稱</label>
                            <asp:TextBox ID="assets_name" runat="server" class="form-control" Width="60%"></asp:TextBox>

                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="get_date">取得日期</label>
                            <asp:TextBox ID="get_date" runat="server" class="form-control date_picker"></asp:TextBox>

                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="supplier_id">供應商代號</label>
                            <asp:TextBox ID="supplier_id" runat="server" class="form-control" Width="100px"></asp:TextBox>
                            <asp:TextBox ID="supplier" runat="server" class="form-control"></asp:TextBox>

                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="location">所在地</label>
                            <asp:TextBox ID="location" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>分攤部門</label>
                            <asp:TextBox ID="dept_id" runat="server" class="form-control" Width="100px"></asp:TextBox>
                            <asp:TextBox ID="dept" runat="server" class="form-control"></asp:TextBox>                            
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>保管人</label>
                            <asp:TextBox ID="keeper" runat="server" class="form-control"></asp:TextBox>
                        </div>

                    </div>
                </div>
            </div>
        </div>--%>

        <%-- <!--車輛基本資料-->
                <div class="bs-callout bs-callout-info">
                    <h3>1. 車輛基本資料</h3>
                    <div class="rowform">
                       
                    </div>
                </div>
                <!--車輛配備-->
                <div class="bs-callout bs-callout-info">
                    <h3>2. 車輛配備</h3>
                     <div class="rowform">

                    </div>
                </div>
                <!--使用狀況及業務分類-->
                <div class="bs-callout bs-callout-info">
                    <h3>3. 使用狀況及業務分類</h3>
                    <div class="rowform">
                       
                    </div>
                </div> 
                <!--保險資料-->
                <div class="bs-callout bs-callout-info">
                    <h3>4. 保險資料</h3>
                     <div class="rowform">
                        
                    </div>
                </div>
                <!--財產資料-->
                <div class="bs-callout bs-callout-info">
                    <h3>5. 財產資料</h3>
                    <div class="rowform">
                         
                    </div>
                </div>--%>



        <div class="form-group text-center">
            <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
            <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/assets1_1.aspx">取消</asp:LinkButton>
            <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
        </div>

    </div>

</asp:Content>

