﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTrace.master" AutoEventWireup="true" CodeFile="trace_Rorderupload.aspx.cs" Inherits="trace_Rorderupload" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            // $("#btn_show").click(function () {
            //    $("div.img-box, #LoadBox").show();
            //});

            //$("#LoadBox").click(function () {
            //    $("div.img-box, #LoadBox").hide();
            //});
            $(function () {
                init();
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                init();
            }
        });

        function init() {
            $("#tbscan [id*=chkSend]").click(function () {
                if ($(this).is(":checked")) {
                    $("#tbscan [id*=cbSelSend]").prop("checked", true);
                } else {
                    $("#tbscan [id*=cbSelSend]").prop("checked", false);
                }

            });
            $("#tbscan [id*=cbSelSend]").click(function () {
                if ($("#tbscan [id*=cbSelSend]").length == $("#tbscan [id*=cbSelSend]:checked").length) {
                    $("#tbscan [id*=chkSend]").prop("checked", true);
                } else {
                    $("#tbscan [id*=chkSend]").prop("checked", false);
                }

            });

            $("#tbscan [id*=chkPhoto]").click(function () {
                if ($(this).is(":checked")) {
                    $("#tbscan [id*=cbSelPhoto]").prop("checked", true);
                } else {
                    $("#tbscan [id*=cbSelPhoto]").prop("checked", false);
                }

            });
            $("#tbscan [id*=cbSelPhoto]").click(function () {
                if ($("#tbscan [id*=cbSelPhoto]").length == $("#tbscan [id*=cbSelPhoto]:checked").length) {
                    $("#tbscan [id*=chkPhoto]").prop("checked", true);
                } else {
                    $("#tbscan [id*=chkPhoto]").prop("checked", false);
                }

            });

        }
         $(function () {
            $('.fancybox').fancybox();

            $("#btn_show").click(function () {
                $("div.img-box, #LoadBox").show();
            });

            $("#LoadBox").click(function () {
                $("div.img-box, #LoadBox").hide();
            });
        });

        function checkSeatAdd() {
          $("div.img-box, #LoadBox").show();
        }
    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

        .auto-style3 {
            height: 40px;
        }

        .img-box {
            border: 5px solid #000;
            position: fixed;  
            max-width: 90%;
            padding: 10px;
            left: calc((100vw - 1024px) / 2);
            background: #FFF;
            z-index: 101;
        }

        .img-box a {

        }

        #LoadBox {
            position: fixed;
            width: 100%;
            height: 100%;
            background: #000;
            top: 0;
            left: 0;
            filter: alpha(opacity=70);
            opacity: 0.7;
            display: block;
            cursor: wait;
            z-index: 100;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">回單上傳作業</h2>
            <table id="tbscan" class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide  ">
                    <th class="text-center">編號</th>
                    <th class="text-center">貨號</th>
                    <th class="text-center">回單圖檔</th>
                    <th class="text-center"></th>
                </tr>
                <asp:Repeater ID="New_List" runat="server"  OnItemDataBound ="New_List_ItemDataBound" OnItemCommand ="New_List_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td data-th="編號" style="width:10%"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="貨號" style="width:15%">                                
                                <asp:TextBox ID="check_number" runat="server" Width="100%" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"   ></asp:TextBox>
                            <td data-th="回單圖檔" style="width:60%">
                                <asp:TextBox ID="filename" runat="server" Width="100%" ></asp:TextBox>
                                <asp:HiddenField ID="fileid" runat="server" />
                            </td>
                            <td style="width:10%">
                                <asp:Button ID="btn_show" runat="server" Text="檢視" CommandName="cmdCheck" OnClientClick="return checkSeatAdd();" ClientIDMode="Static" />
                             <%--    <input type="button" id="btn_show" value="檢視" />--%>
                                <%--<asp:Button ID="btn_Sel" CssClass="btn btn-success" runat="server" Text="選擇檔案" />--%>
                                <%--<a href="fileupload_Rorder.aspx?check_number=<%# check_number. %>&filename=<%# filename.%>" class="fancybox fancybox.iframe btn btn-success " id="updclick">選擇檔案</a>--%>
                                <asp:HyperLink runat ="server" id ="btn_upload" class="fancybox fancybox.iframe btn btn-success">選擇檔案</asp:HyperLink>
                            </td>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>                
            </table>
              <div class="img-box" style="display: none;">
                   <asp:Panel ID="myPanel" runat="server">
                    </asp:Panel>
                </div>
                <div id="LoadBox" style="display: none"></div>
                <hr>
            <hr />
            <div class="form-group text-center">
                <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" OnClick="btnsave_Click"   />
            </div>

        </div>
    </div>
</asp:Content>

