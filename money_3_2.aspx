﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_3_2.aspx.cs" Inherits="money_3_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(".btn_view").click(function () {
                showBlockUI();
            });
        });
        $.fancybox.update();

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後....!<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
    <style type="text/css">
        .tb_search {
            width: auto;
        }

        ._td btn {
            margin: auto 3px;
        }

        ._td1 {
            text-align: center;
            padding: 5px 30px 0px 10px;
        }

        ._td2 {
            background-color: #f9f9f9;
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <table>
                <tr>
                    <td>
                        <h2 class="margin-bottom-10">結算表維護 </h2>
                    </td>

                    <td>
                        <span class="text-danger span_tip">※ 開放修改時期間：每月 20~25 日</span>
                    </td>
                </tr>
            </table>



            <div class="templatemo-login-form">

                <table class="tb_search">

                    <tr>
                        <td class="_td _td1">
                            <h3>查 詢 條 件</h3>
                            <hr />
                            <label for="inputLastName">結算表</label>
                            <asp:DropDownList ID="customer_type" runat="server" AutoPostBack="true" OnSelectedIndexChanged="customer_type_SelectedIndexChanged" >
                                <asp:ListItem Value="2">竹運</asp:ListItem>
                                <asp:ListItem Value="1">區配</asp:ListItem>
                                <asp:ListItem Value="4">C配</asp:ListItem>
                                <asp:ListItem Value="5">合發專用</asp:ListItem>
                                <asp:ListItem Value="3">自營</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="pricing_code" runat="server" AutoPostBack="true" >
                                <asp:ListItem Value="01">論板</asp:ListItem>
                                <asp:ListItem Value="02">論件</asp:ListItem>
                                <asp:ListItem Value="03">論才</asp:ListItem>
                                <asp:ListItem Value="04">論小板</asp:ListItem>
                            </asp:DropDownList>
                            <label for="inputLastName">級距</label>
                            <asp:DropDownList ID="level" runat="server"></asp:DropDownList>
                            <asp:Button ID="btQry" class="btn btn-primary btn_view" runat="server" Text="預　覽" OnClick="btQry_Click" />                            
                            <asp:Button ID="btnLog" class="btn btn-danger" runat="server" Text="歷史記錄" OnClick="btnLog_Click" />

                            <asp:Button ID="btndownload" class="btn btn-default" runat="server" Text="下　載" OnClick="btndownload_Click" />
                        </td>
                        <td class="_td _td2 templatemo-content-widget">
                            <div>
                                <h3>使 用 中 運 價</h3>
                                <hr />
                                <div class="form-group form-inline">
                                    <asp:TextBox ID="textbox1" runat="server" class="form-control" Enabled="False"></asp:TextBox>
                                    <asp:DropDownList ID="default_level" runat="server" CssClass="form-control" Enabled="False">
                                        <asp:ListItem Value="0">0級距</asp:ListItem>
                                        <asp:ListItem Value="1">1級距</asp:ListItem>
                                        <asp:ListItem Value="2">2級距</asp:ListItem>
                                        <asp:ListItem Value="3">3級距</asp:ListItem>
                                        <asp:ListItem Value="4">4級距</asp:ListItem>
                                        <asp:ListItem Value="5">5級距</asp:ListItem>
                                        <asp:ListItem Value="6">6級距</asp:ListItem>
                                        <asp:ListItem Value="7">7級距</asp:ListItem>
                                        <asp:ListItem Value="8">8級距</asp:ListItem>
                                        <asp:ListItem Value="9">9級距</asp:ListItem>
                                        <asp:ListItem Value="10">10級距</asp:ListItem>
                                        <asp:ListItem Value="11">11級距</asp:ListItem>
                                        <asp:ListItem Value="12">12級距</asp:ListItem>
                                    </asp:DropDownList>
                                    <label for="inputLastName">運價生效日</label>
                                    <asp:TextBox ID="fee_sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                </div>
                            </div>
                        </td>
                    </tr>

                </table>

                <hr />
                <div class="form-group form-inline" runat="server" id="div_Upload">
                    <a href="PriceUpload.aspx?customer_type=<%=customer_type.SelectedValue  %>" class="fancybox fancybox.iframe" id="uploadclick"><span class="btn btn-primary">上傳新運價</span></a>
                    <span class="text-danger span_tip">※ 請注意！！上傳後會覆蓋舊運價</span>
                </div>
                <div style="overflow: auto; height: calc(100vh - 495px); width: 100%">
                    <asp:Panel ID="pan_PriceList" runat="server">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <%-- <th>序號</th>--%>
                                <th>起點區城</th>
                                <th>迄點區域</th>
                                <th>級距</th>
                                <th>計價模式</th>
                                <th>板數1</th>
                                <th>板數2</th>
                                <th>板數3</th>
                                <th>板數4</th>
                                <th>板數5</th>
                                <th>板數6</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="起點區城"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.start_city").ToString())%> </td>
                                        <td data-th="迄點區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.end_city").ToString())%> </td>
                                        <td data-th="級距"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.class_level").ToString())%></td>
                                        <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                        <td data-th="板數1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate1_price", "{0:N0}").ToString())%></td>
                                        <td data-th="板數2"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate2_price", "{0:N0}").ToString())%></td>
                                        <td data-th="板數3"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate3_price", "{0:N0}").ToString())%></td>
                                        <td data-th="板數4"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate4_price", "{0:N0}").ToString())%></td>
                                        <td data-th="板數5"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate5_price", "{0:N0}").ToString())%></td>
                                        <td data-th="板數6"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plate6_price", "{0:N0}").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="10" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </asp:Panel>

                    <asp:Panel ID="pan_PriceLog" runat="server" Visible="false">
                         <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <%-- <th>序號</th>--%>
                                <th>No.</th>
                                <th>運價類別</th>
                                <th>計價模式</th>
                                <th>級距</th>
                                <th>建立日期</th>
                                <th>建立者</th>   
                            </tr>
                            <asp:Repeater ID="List_log" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="no"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.no").ToString())%> </td>
                                        <td data-th="運價類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.table_type").ToString())%> </td>
                                        <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                        <td data-th="級距"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.class_level").ToString())%></td>
                                        <td data-th="建立日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate").ToString())%></td>
                                        <td data-th="建立者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.newuser").ToString())%></td>                                       
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (List_log.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="10" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>

                    </asp:Panel>

                </div>

            </div>
        </div>
    </div>
</asp:Content>

