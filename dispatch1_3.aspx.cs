﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class dispatch1_3 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4" || ssManager_type == "5")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }
            dateS.Text = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
            dateE.Text = DateTime.Today.ToString("yyyy/MM/dd");

            #region 
            supplier.DataSource = Utility.getSupplierDT2(ssSupplier_code, ssManager_type);
            supplier.DataValueField = "supplier_no";
            supplier.DataTextField = "showname";
            supplier.DataBind();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                supplier.Items.Insert(0, new ListItem("全部", ""));

            }
            #endregion

            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand cmd = new SqlCommand();
        string wherestr = "";

        #region 關鍵字
        
        if (!String.IsNullOrEmpty(dateS.Text) && !String.IsNullOrEmpty(dateE.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", dateS.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime( dateE.Text).AddDays(1).ToString("yyyy/MM/dd"));
            wherestr += " AND A.task_date >=@dateS and  A.task_date <@dateE ";
        }

        if (supplier.SelectedValue  != "")
        {   
            cmd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue);
            wherestr += " AND A.supplier_no =@supplier_no ";
        }

        if (dltask_type.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@task_type", dltask_type.SelectedValue);
            wherestr += " AND A.task_type =@task_type ";
        }

        if (task_id.Text != "")
        {
            cmd.Parameters.AddWithValue("@task_id", task_id.Text );
            wherestr += " AND A.task_id =@task_id ";
        }

        #endregion

        cmd.CommandText = string.Format(@"Select A.* , B.user_name , C.code_name 'route_name', D.supplier_name 
                                            from ttDispatchTask A with(nolock) 
                                            left join tbAccounts B With(Nolock) on B.account_code = A.c_user 
                                            left join ttItemCodesRoad C with(Nolock) on A.task_route = C.seq 
                                            left join tbSuppliers D with(nolock) on A.supplier_no= D.supplier_no 
                                            WHERE 1=1 
                                            {0}
                                            ORDER BY A.task_date  ", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {   
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         派遣日期 = p.Field<string>("task_date"),
                         任務編號 = p.Field<string>("task_id"),
                         供應商 = p.Field<string>("supplier_name"),
                         發包價 = (p["price"] == DBNull.Value) ? 0 : p.Field<Int32>("price"),
                         備註 = p.Field<string>("memo"),
                         建檔人員 = p.Field<string>("user_name")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"派遣任務";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    protected void AddTask_Click(object sender, EventArgs e)
    {
        string ErrStr = string.Empty;
        string request_id = string.Empty;
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                if ((chkRow != null) && (chkRow.Checked))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    request_id += id.ToString() + ",";
                }
            }
        }
        if (request_id != "") request_id = request_id.Substring(0, request_id.Length - 1);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>OpenLinkByFancyBox('../dispatch1_1_edit.aspx?request_id="+ request_id+ "' );</script>", false);


    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string str_id = ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim();
        //string str_suno = ((HiddenField)e.Item.FindControl("hid_rSuNo")).Value.ToString().Trim();
       
        switch (e.CommandName)
        {
            case "cmdDetail":
                string id = e.CommandArgument.ToString();
                string task_type = ((HiddenField)e.Item.FindControl("hid_task_type")).Value.ToString().Trim();
                string wherestr = "";
                switch (task_type)
                {
                    case "1":  //B段
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@t_id", id);
                            cmd2.CommandText = string.Format(@"Select T.task_id, B.date , B.warehouse , C.code_name 'warehouse_text', B.supplier , B.plates
                                                FROM ttDispatchTask T with(Nolock) 	
		                                        INNER JOIN ttDispatchTaskDetail_B B with(nolock) on T.t_id = B.t_id
                                                LEFT JOIN tbItemCodes C with(nolock) on B.warehouse = C.code_id and code_bclass  = '1' and code_sclass  = 'warehouse'
                                                --LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                WHERE T.t_id= @t_id", wherestr);

                            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                            {
                                rp_TaskList_B.DataSource = dt2;
                                rp_TaskList_B.DataBind();
                            }
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "retVal", "$('#modal-default_02').modal();", true);
                        break;

                    case "2":  //專車
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@t_id", id);
                            cmd2.CommandText = string.Format(@"Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
                                                A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name'
                                                from ttDispatchTask T with(Nolock) 	
		                                        INNER JOIN ttDispatchTaskDetail F with(nolock) on T.t_id = F.t_id
		                                        INNER JOIN tcDeliveryRequests A with(nolock) on A.request_id = F.request_id
                                                LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
                                                LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                WHERE A.pricing_type = '05'
		                                        and  T.t_id= @t_id
                                                ORDER BY A.print_date, A.customer_code , A.area_arrive_code  ", wherestr);

                            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                            {
                                rp_TaskList.DataSource = dt2;
                                rp_TaskList.DataBind();
                            }
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "retVal", "$('#modal-default_01').modal();", true);
                        break;
                    default:
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@t_id", id);
                            cmd2.CommandText = string.Format(@"Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
                                                A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name'
                                                from ttDispatchTask T with(Nolock) 	
		                                        INNER JOIN ttDispatchTaskDetail F with(nolock) on T.t_id = F.t_id
		                                        INNER JOIN tcDeliveryRequests A with(nolock) on A.request_id = F.request_id
                                                LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
                                                LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                WHERE A.pricing_type <> '05'
		                                        and  T.t_id= @t_id
                                                ORDER BY A.print_date, A.customer_code , A.area_arrive_code  ", wherestr);

                            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                            {
                                rp_TaskList.DataSource = dt2;
                                rp_TaskList.DataBind();
                            }
                        }
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "retVal", "$('#modal-default_01').modal();", true);
                        break;
                }
                
                break;
        }
    }

    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "派遣任務資料";
        string file_name = "派遣任務資料" + DateTime.Now.ToString("yyyyMMdd");        

        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "";
            if (!String.IsNullOrEmpty(dateS.Text) && !String.IsNullOrEmpty(dateE.Text))
            {
                objCommand.Parameters.AddWithValue("@dateS", dateS.Text);
                objCommand.Parameters.AddWithValue("@dateE", Convert.ToDateTime(dateE.Text).AddDays(1).ToString("yyyy/MM/dd"));
                wherestr += " AND A.task_date >=@dateS and  A.task_date <@dateE ";
            }

            objCommand.CommandText = string.Format(@"Select CONVERT(varchar, A.task_date, 111) as '派遣日期',A.task_id as '任務編號',A.price as '發包價',A.memo as '備註',B.user_name as '建檔人員' 
                                            from ttDispatchTask A with(nolock) 
                                            left join tbAccounts B With(Nolock) on B.account_code = A.c_user 
                                            left join ttItemCodesRoad C with(Nolock) on A.task_route = C.seq 
                                            WHERE 1=1 
                                            {0}
                                            ORDER BY A.task_date  ", wherestr);




            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }
}