﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTreport_3_5 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            
            manager_type = Session["manager_type"].ToString(); //管理單位類別

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    //using (SqlCommand cmd = new SqlCommand())
                    //{
                    //    cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    //    cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                    //                        inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                    //                        left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                    //                        where tbAccounts.account_code  =@account_code";
                    //    DataTable dt = dbAdapter.getDataTable(cmd);
                    //    if (dt != null && dt.Rows.Count > 0)
                    //    {
                    //        supplier_code = dt.Rows[0]["station_code"].ToString();
                    //    }
                    //    else
                    //    {

                    //    }
                    //}
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }

            string wherestr = "";

            #region 站所
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = "( and station_code = @supplier_code || @supplier_code= '')";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}
                                               order by station_code", wherestr);

            Suppliers.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "station_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            Suppliers.Items.Insert(0, new ListItem("全部", ""));
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);
            #endregion

            #region 配送站
            using (SqlCommand cmd = new SqlCommand()) {
                cmd.CommandText = string.Format(@"select station_scode,station_scode + '- '+ station_name as station_name from tbStation order by station_scode asc");

                ddlarrivestation.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
                ddlarrivestation.DataValueField = "station_scode";
                ddlarrivestation.DataTextField = "station_name";
                ddlarrivestation.DataBind();
                ddlarrivestation.Items.Insert(0, new ListItem("全部", ""));
                if (ddlarrivestation.Items.Count > 0) ddlarrivestation.SelectedIndex = 0;
            }
            #endregion

            if (Request.QueryString["dttyp"] != null)
            {
                //rbarrive_option.SelectedValue = Request.QueryString["arrive_option"];
                if (Request.QueryString["dttyp"] != "")
                {
                    if (Request.QueryString["dttyp"].ToString() == "1")
                    {
                        rb1.Checked = true;
                        sdate.Enabled = true;
                        sdate.Enabled = true;
                        rb2.Checked = false;
                        sMatchdt.Enabled = false;
                        eMatchdt.Enabled = false;

                        if (Request.QueryString["sdate"] != null)
                        {
                            if (Request.QueryString["sdate"] != "")
                            {
                                sdate.Text = Request.QueryString["sdate"];
                            }
                        }
                        if (Request.QueryString["edate"] != null)
                        {
                            if (Request.QueryString["edate"] != "")
                            {
                                edate.Text = Request.QueryString["edate"];
                            }
                        }
                    }
                    else
                    {
                        rb1.Checked = false;
                        sdate.Enabled = false;
                        sdate.Enabled = false;
                        rb2.Checked = true;
                        sMatchdt.Enabled = true;
                        eMatchdt.Enabled = true;

                        if (Request.QueryString["sdate"] != null)
                        {
                            if (Request.QueryString["sdate"] != "")
                            {
                                sMatchdt.Text = Request.QueryString["sdate"];
                            }
                        }
                        if (Request.QueryString["edate"] != null)
                        {
                            if (Request.QueryString["edate"] != "")
                            {
                                eMatchdt.Text = Request.QueryString["edate"];
                            }
                        }
                    }
                }
            }

            if (Request.QueryString["arrivestation"] != null)
            {
                if (Request.QueryString["arrivestation"] != "")
                {
                    ddlarrivestation.SelectedValue = Request.QueryString["arrivestation"];
                }
            }

            #region 作業人員
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@station", ddlarrivestation.SelectedValue);
                cmd.CommandText = string.Format(@"select driver_code,driver_code+' '+driver_name as driver_name  
                                                   from tbDrivers where (@station ='' or station = @station ) 
                                                   order by driver_code");

                ddlscanname.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
                ddlscanname.DataValueField = "driver_code";
                ddlscanname.DataTextField = "driver_name";
                ddlscanname.DataBind();
                ddlscanname.Items.Insert(0, new ListItem("全部", ""));
            }
            #endregion
            
           
            if (Request.QueryString["Suppliers"] != null)
            {

                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }
            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }
            if (Request.QueryString["collection_money"] != null)
            {
                collection_money.Checked = Convert.ToBoolean(Request.QueryString["collection_money"]);
            }
            if (Request.QueryString["arrive_to_pay_freight"] != null)
            {
                arrive_to_pay_freight.Checked = Convert.ToBoolean(Request.QueryString["arrive_to_pay_freight"]);
            }
            if (Request.QueryString["arrive_to_pay_append"] != null)
            {
                arrive_to_pay_append.Checked = Convert.ToBoolean(Request.QueryString["arrive_to_pay_append"]);
            }
            if (Request.QueryString["scanname"] != null)
            {
                ddlscanname.SelectedValue = Request.QueryString["scanname"];
            }
            if (Request.QueryString["arrive_option"] != null)
            {
                //rbarrive_option.SelectedValue = Request.QueryString["arrive_option"];
                if (Request.QueryString["arrive_option"] != "")
                {
                    if (Request.QueryString["arrive_option"].ToString() == "3")
                    {
                        arrive_option1.Checked = true;
                        arrive_option2.Checked = false;
                    }
                    else
                    {
                        arrive_option1.Checked = false;
                        arrive_option2.Checked = true;
                    }
                }
            }





            if (Request.QueryString["sdate"] != null)
            {
                readdata();
            }
            else {
                sdate.Text = Distribute_date;
                edate.Text = Distribute_date;
            }
        }
    }
    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd1 = string.Empty;

            string strWhereCmd2 = string.Empty;
            string querystring = string.Empty;
            string money = "";
            cmd.Parameters.Clear();
            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
          
            cmd.Parameters.AddWithValue("@DeliveryType", "D");

            if (rb1.Checked) {
                querystring += "&dttyp=" + "1";
                cmd.Parameters.AddWithValue("@sdate", sdate.Text);
                cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text).AddDays(1).ToString("yyyy/MM/dd"));
                if (sdate.Text.ToString() != "")
                {
                    querystring += "&sdate=" + sdate.Text.ToString();
                }

                if (edate.Text.ToString() != "")
                {
                    querystring += "&edate=" + edate.Text.ToString();
                }
                strWhereCmd1 += @" AND (A.print_date between @sdate and @edate ) and scan_date >= @sdate";
                strWhereCmd2 += @" AND (A.print_date between @sdate and @edate )";
            }
            else 
            {
                querystring += "&dttyp=" + "2";
                cmd.Parameters.AddWithValue("@sdate", sMatchdt.Text);
                cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(eMatchdt.Text).AddDays(1).ToString("yyyy/MM/dd"));
                if (sMatchdt.Text.ToString() != "")
                {
                    querystring += "&sdate=" + sMatchdt.Text.ToString();
                }

                if (eMatchdt.Text.ToString() != "")
                {
                    querystring += "&edate=" + eMatchdt.Text.ToString();
                }
                strWhereCmd1 += @" and scan_date between @sdate and  @edate";
                strWhereCmd2 += @" AND ( Log.scan_date between @sdate and  @edate )";

            }
            
            
            if (ddlarrivestation.SelectedValue.ToString() != "")
            {
                querystring += "&arrivestation=" + ddlarrivestation.SelectedValue.ToString();
            }
            cmd.Parameters.AddWithValue("@arrivestation", ddlarrivestation.SelectedValue.ToString());  //作業站
            cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            strWhereCmd2 += @" AND ( @supplier_id ='' or A.supplier_code =  @supplier_id )";

            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            }
            else
            {
                cmd.Parameters.AddWithValue("@customer_code", "");  //客代
            }
            querystring += "&second_code=" + second_code.SelectedValue.ToString();

            
            cmd.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue);  //作業人

            querystring += "&scanname=" + ddlscanname.SelectedValue;

            if (collection_money.Checked)
            {
                //41代收
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '41' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '41' ";
                }
            }
            if (arrive_to_pay_freight.Checked)
            {
                //11元付21到付
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '21' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '21' ";
                }
            }
            if (arrive_to_pay_freight.Checked)
            {
                //11元付21到付
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '25' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '25' ";
                }
            }
            if (money != "")
            {
                strWhereCmd1 += " AND (" + money + ")";
            }

            if (arrive_option1.Checked) {
                cmd.Parameters.AddWithValue("@arrive_option", "3");  //正常配交
                querystring += "&arrive_option=" + "3";
            }
            else
            {
                cmd.Parameters.AddWithValue("@arrive_option", "4");  //拒收
                querystring += "&arrive_option=" + "4";
            }
            

            querystring += "&collection_money=" + collection_money.Checked;
            querystring += "&arrive_to_pay_freight=" + arrive_to_pay_freight.Checked;
            querystring += "&arrive_to_pay_append=" + arrive_to_pay_append.Checked;
            //
            #endregion
            cmd.CommandText = string.Format(@"
            WITH scanLog AS
(
SELECT B.*,ROW_NUMBER() OVER (PARTITION BY B.check_number ORDER BY B.check_number,B.scan_date DESC) AS rn
FROM ttDeliveryScanLog B 
INNER JOIN tcDeliveryRequests A with(nolock) ON A.check_number = B.check_number
WHERE scan_item IN(1,2,3) 
{0}
And A.check_number is not null and A.check_number<>''
AND A.Less_than_truckload  = 1
AND A.pricing_type in ('02')
And ( @supplier_id ='' or A.supplier_code =  @supplier_id )
and A.cancel_date IS NULL 
AND (@customer_code = '' or  A.customer_code =  @customer_code )
AND (A.subpoena_category <> '11' )
AND (@scanname = '' or  B.driver_code =  @scanname)
AND (@arrivestation = '' or  A.area_arrive_code = @arrivestation)
)
Select DISTINCT
Convert(VARCHAR, Log.scan_date,20) 'scan_date',
  case when Log.driver_code is null then '' 
       when Log.driver_code = '' then '' 
       when Log.driver_code  <> '' then  (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end 'driver_code',
Log.scan_item,
CASE Log.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  END  AS scan_name,
A.send_contact,
Convert(VARCHAR, A.print_date,111) 'print_date' ,
A.check_number ,
Convert(VARCHAR, A.arrive_assign_date,111) 'arrive_assign_date'  ,
case when A.DeliveryType = 'R' then '到付'
else
B.code_name end 'subpoena_category_str',
A.receive_customer_code, 
A.area_arrive_code 'area_arrive_station',
case when A.DeliveryType = 'R' then  (select B1.station_name from 
												  ttArriveSitesScattered A1 With(Nolock) 
												  LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
												  where A1.post_city = A.send_city and A1.post_area= A.send_area ) else
G.station_name end station_name,--發送站所
A.receive_contact,
A.receive_city + A.receive_area+ CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else A.receive_address end 'receive_address',
CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END 'receive_tel',
CASE when A.subpoena_category = '21' THEN  ISNULL(_fee.total_fee,0) ELSE  ISNULL(A.arrive_to_pay_freight,0) END 'arrive_to_pay_freight', 
case  when A.check_number like '990%' then '0'		
else
ISNULL(A.collection_money,0) end  'collection_money', 
ISNULL(A.arrive_to_pay_append,0) 'arrive_to_pay_append', 
A.pieces , A.cbm  ,CASE WHEN A.pricing_type = '01' then A.plates else 0 end 'plates', CASE WHEN A.pricing_type = '04' then A.plates else 0 end 'splates'
,_arr.arrive_item
,_arr.newest_station                                --'到著站簡碼'
,A.area_arrive_code +' '+H.station_name arrivestation --'到著站'
,A.VoucherMoney VoucherMoney --振興卷金額
,A.CashMoney  --現金金額
from  (select * from  tcDeliveryRequests A 
where  A.Less_than_truckload  = 1
And A.check_number is not null and A.check_number<>''
AND A.pricing_type in ('02')
And ( @supplier_id ='' or A.supplier_code =  @supplier_id )
and A.cancel_date IS NULL 
AND (@customer_code = '' or  A.customer_code =  @customer_code )
AND (A.subpoena_category <> '11' )
--AND (@scanname = '' or  B.driver_code =  @scanname)
AND (@arrivestation = '' or  A.area_arrive_code = @arrivestation)
 ) A 
LEFT JOIN (SELECT * FROM scanLog WHERE rn= 1) Log on A.check_number = Log.check_number  and Log.scan_date >=@sdate
LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code          --發送
LEFT JOIN tbStation H With(Nolock) ON A.area_arrive_code = H.station_scode      --到著
CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
LEFT JOIN tbStation M With(Nolock) ON _arr.newest_station = M.station_scode     --作業
WHERE 1 = 1 
AND Log.arrive_option = @arrive_option or Log.arrive_option = 'C'
", strWhereCmd1, strWhereCmd2);
             cmd.CommandTimeout = 900;
            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {
                New_List.DataSource = dt;
                New_List.DataBind();

                ltotalpages.Text = dt.Rows.Count.ToString();
            }
        }
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";

       
        if (rb1.Checked)
        {
            if (sdate.Text != "")
            {
                querystring += "&sdate=" + sdate.Text;
            }

            if (edate.Text != "")
            {
                querystring += "&edate=" + edate.Text;
            }
            querystring += "&dttyp=" + "1";
        }

        if (rb2.Checked)
        {
            if (sMatchdt.Text != "")
            {
                querystring += "&sdate=" + sMatchdt.Text;
            }

            if (eMatchdt.Text != "")
            {
                querystring += "&edate=" + eMatchdt.Text;
            }
            querystring += "&dttyp=" + "2";
        }

        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&second_code=" + second_code.SelectedValue.ToString();
        querystring += "&collection_money=" + collection_money.Checked;
        querystring += "&arrive_to_pay_freight=" + arrive_to_pay_freight.Checked;
        querystring += "&arrive_to_pay_append=" + arrive_to_pay_append.Checked;

        querystring += "&arrivestation=" + ddlarrivestation.SelectedValue.ToString();

        if (arrive_option1.Checked)
        {
            querystring += "&arrive_option=" + "3";
        }
        else {
            querystring += "&arrive_option=" + "4";
        }
        
        querystring += "&scanname=" + ddlscanname.SelectedValue;

        Response.Redirect(ResolveUrl("~/LTreport_3_5.aspx?search=yes" + querystring));
    }



    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@type", "1");
            if (manager_type == "5")
            {
                cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock) where A.customer_code = @customer_code");
            }
            else
            {
                if (Suppliers.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    wherestr = " and A.supplier_code = @supplier_code ";
                }
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    Left join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 
                                                    and A.stop_shipping_code = '0' and A.type =@type {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
            }

            second_code.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
            second_code.DataValueField = "customer_code";
            second_code.DataTextField = "name";
            second_code.DataBind();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2") second_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            //second_code.Items.Insert(0, new ListItem("", ""));
        }
        #endregion
    }

    protected void ddlarrivestation_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@station", ddlarrivestation.SelectedValue);

           
            cmd.CommandText = string.Format(@"select driver_code,driver_code+' '+driver_name as driver_name  
                                                   from tbDrivers where (@station ='' or station = @station ) 
                                                   order by driver_code");//WHERE rn = 1
            ddlscanname.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
            ddlscanname.DataValueField = "driver_code";
            ddlscanname.DataTextField = "driver_name";
            ddlscanname.DataBind();
            ddlscanname.Items.Insert(0, new ListItem("全部", ""));
        }
        #endregion
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        string sheet_title = "代收，到付";
        string strWhereCmd1 = "";
        string strWhereCmd2 = "";
        string file_name = "代收，到付表" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            string money = "";
            cmd.Parameters.Clear();
            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            //cmd.Parameters.AddWithValue("@sdate", sdate.Text);
            //cmd.Parameters.AddWithValue("@edate", edate.Text);
            cmd.Parameters.AddWithValue("@DeliveryType", "D");

            if (rb1.Checked)
            {
                cmd.Parameters.AddWithValue("@sdate", sdate.Text);
                cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text).AddDays(1).ToString("yyyy/MM/dd"));
                if (sdate.Text.ToString() != "")
                {
                    querystring += "&sdate=" + sdate.Text.ToString();
                }

                if (edate.Text.ToString() != "")
                {
                    querystring += "&edate=" + edate.Text.ToString();
                }
                strWhereCmd1 += @" AND (A.print_date between @sdate and @edate ) and scan_date >= @sdate";
                strWhereCmd2 += @" AND (A.print_date between @sdate and @edate )";
            }
            else
            {
                cmd.Parameters.AddWithValue("@sdate", sMatchdt.Text);
                cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(eMatchdt.Text).AddDays(1).ToString("yyyy/MM/dd"));
                if (sMatchdt.Text.ToString() != "")
                {
                    querystring += "&sdate=" + sMatchdt.Text.ToString();
                }

                if (eMatchdt.Text.ToString() != "")
                {
                    querystring += "&edate=" + eMatchdt.Text.ToString();
                }
                strWhereCmd1 += @" and scan_date between @sdate and  @edate ";
                strWhereCmd2 += @" AND ( Log.scan_date between @sdate and  @edate )";

            }




            cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            strWhereCmd2 += @" AND ( @supplier_id ='' or A.supplier_code =  @supplier_id )";


            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            }
            else
            {
                cmd.Parameters.AddWithValue("@customer_code", "");  //客代
            }

            if (collection_money.Checked)
            {
                //41代收
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '41' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '41' ";
                }
            }
            if (arrive_to_pay_freight.Checked)
            {
                //21到付
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '21' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '21' ";
                }
            }
            if (arrive_to_pay_freight.Checked)
            {
                //25到付追加
                if (money == "")
                {
                    money += @"  A.subpoena_category =  '25' ";
                }
                else
                {
                    money += @" or A.subpoena_category =  '25' ";
                }
            }
            if (money != "")
            {
                strWhereCmd1 += " AND (" + money + ")";
            }
            cmd.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue);  //作業人
            

            if (arrive_option1.Checked)
            {
                cmd.Parameters.AddWithValue("@arrive_option", "3");  //正常配交
            }
            else
            {
                cmd.Parameters.AddWithValue("@arrive_option", "4");  //拒收
            }

            cmd.Parameters.AddWithValue("@arrivestation", ddlarrivestation.SelectedValue);

            #endregion
            cmd.CommandText = string.Format(@"
              WITH scanLog AS
(
SELECT B.*,ROW_NUMBER() OVER (PARTITION BY B.check_number ORDER BY B.check_number,B.scan_date DESC) AS rn
FROM ttDeliveryScanLog B 
INNER JOIN tcDeliveryRequests A with(nolock) ON A.check_number = B.check_number
WHERE scan_item IN(1,2,3) 
{0}
AND A.Less_than_truckload  = 1
AND A.pricing_type in ('02')
And ( @supplier_id ='' or A.supplier_code =  @supplier_id )
and A.cancel_date IS NULL 
AND (@customer_code = '' or  A.customer_code =  @customer_code )
AND (A.subpoena_category <> '11' )
AND (@scanname = '' or  B.driver_code =  @scanname)
AND (@arrivestation = '' or  A.area_arrive_code = @arrivestation)
)

Select DISTINCT
Convert(VARCHAR, Log.scan_date,20) '作業時間',
  case when Log.driver_code is null then '' 
       when Log.driver_code = '' then '' 
       when Log.driver_code  <> '' then  (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end '作業人',
Log.scan_item '作業代碼',
CASE Log.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  END  AS '作業',
A.send_contact '出貨人',
Convert(VARCHAR, A.print_date,111) '出貨日期' ,
A.check_number '貨號',
--Convert(VARCHAR, A.arrive_assign_date,111) 'arrive_assign_date'  ,
case when A.DeliveryType = 'R' then '到付'
else
B.code_name end '傳票區分',
case when A.DeliveryType = 'R' then  (select B1.station_scode from 
												  ttArriveSitesScattered A1 With(Nolock) 
												  LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
												  where A1.post_city = A.send_city and A1.post_area= A.send_area ) else
G.station_scode end '站所代碼', 
case when A.DeliveryType = 'R' then  (select B1.station_name from 
												  ttArriveSitesScattered A1 With(Nolock) 
												  LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
												  where A1.post_city = A.send_city and A1.post_area= A.send_area ) else
G.station_name end '發送站所',
A.receive_contact '收貨人',
A.receive_city + A.receive_area+ CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else A.receive_address end '收貨人地址',
CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話', 
CASE when A.subpoena_category = '21' THEN  ISNULL(_fee.total_fee,0) ELSE  ISNULL(A.arrive_to_pay_freight,0) END '到付運費', 
case  when A.check_number like '990%' then '0'		
else
ISNULL(A.collection_money,0) end '代收金額', 
ISNULL(A.VoucherMoney,0) '振興卷', 
ISNULL(A.CashMoney,0) '現金', 
A.pieces '件數',
_arr.arrive_item '配送說明'
,E.station_scode '到著站簡碼'
,E.station_name  '到著站'
from  (select * from  tcDeliveryRequests A 
where  A.Less_than_truckload  = 1
And A.check_number is not null and A.check_number<>''
AND A.pricing_type in ('02')
And ( @supplier_id ='' or A.supplier_code =  @supplier_id )
and A.cancel_date IS NULL 
AND (@customer_code = '' or  A.customer_code =  @customer_code )
AND (A.subpoena_category <> '11' )
--AND (@scanname = '' or  B.driver_code =  @scanname)
AND (@arrivestation = '' or  A.area_arrive_code = @arrivestation)
 ) A 
LEFT JOIN (SELECT * FROM scanLog WHERE rn= 1) Log on A.check_number = Log.check_number  and Log.scan_date >=@sdate
LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code                      --發送
LEFT JOIN tbStation E With(Nolock) ON A.area_arrive_code = E.station_scode                  --到著
CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
LEFT JOIN tbStation H With(Nolock) ON _arr.newest_station = H.station_scode                 --作業
WHERE 1 = 1 
AND Log.arrive_option = @arrive_option or Log.arrive_option = 'C'
{1}
", strWhereCmd1, strWhereCmd2);
            cmd.CommandTimeout = 900;
            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                    }
                }
            }
        }
    }


    protected void rb_CheckedChanged(Object sender, EventArgs e)
    {
        string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
        if (rb1.Checked)
            {
                sdate.Enabled = true;
                edate.Enabled = true;
                sMatchdt.Enabled = false;
                eMatchdt.Enabled = false;
            sdate.Text = Distribute_date;
            edate.Text = Distribute_date;
            sMatchdt.Text = "";
            eMatchdt.Text = "";
        }
        if (rb2.Checked)
        {
                sdate.Enabled = false;
                edate.Enabled = false;
                sMatchdt.Enabled = true;
                eMatchdt.Enabled = true;
            sdate.Text = "" ;
            edate.Text = "";
            sMatchdt.Text = Distribute_date;
            eMatchdt.Text = Distribute_date;
        }
        
    }
}