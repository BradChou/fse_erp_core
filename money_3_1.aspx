﻿<%@ Page Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_3_1.aspx.cs" Inherits="money_3_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input {
            width: 50px;
        }

        table {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <span>目前油價：</span>
    <asp:Label ID="lbCurrentLevel" runat="server" Visible="false" ></asp:Label>
    <asp:Panel ID="ShowMode" runat="server">
        <asp:Label ID="lbOilPrice" runat="server"></asp:Label>
        <asp:Button ID="btnEdit" runat="server" Text="編輯目前油價" OnClick="btnEdit_Click" Style="width: auto" />
    </asp:Panel>
    <asp:Panel ID="EditMode" runat="server" Visible="false">
        <asp:TextBox ID="txtOilPrice" runat="server" TextMode="Number"></asp:TextBox>
        <asp:Button ID="btnSaveOilPrice" runat="server" Text="儲存" OnClick="btnSaveOilPrice_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="取消" OnClick="btnCancel_Click" />
    </asp:Panel>

    <asp:GridView ID="oilPriceBalance" runat="server" AutoGenerateColumns="False" DataKeyNames="step_distance"
        OnRowEditing="oilPriceBalance_RowEditing" OnRowCancelingEdit="oilPriceBalance_RowCancelingEdit"
        OnRowUpdating="oilPriceBalance_RowUpdating" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="級距">
                <ItemTemplate>
                    <asp:Label ID="lbStepDistance" Text='<%# Eval("step_distance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="txtStepDistance" Text='<%# Eval("step_distance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="油價底">
                <ItemTemplate>
                    <asp:Label ID="lbMinOilPrice" Text='<%# Eval("min_oil_price") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="txtMinOilPrice" Text='<%# Eval("min_oil_price") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="油價頂">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("max_oil_price") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Label ID="txtMaxOilPrice" Text='<%# Eval("max_oil_price") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="1板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("one_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtOnePallet" Text='<%# Eval("one_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="2板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("two_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtTwoPallet" Text='<%# Eval("two_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="3板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("three_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtThreePallet" Text='<%# Eval("three_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="4板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("four_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtFourPallet" Text='<%# Eval("four_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="5板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("five_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtFivePallet" Text='<%# Eval("five_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="6板">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("six_pallet_price_balance") %>' runat="server" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtSixPallet" Text='<%# Eval("six_pallet_price_balance") %>' runat="server" />
                </EditItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText=" ">
                <ItemTemplate>
                    <asp:Button runat="server" CommandName="Edit" ToolTip="Edit" Text="編輯" />
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:Button runat="server" CommandName="Update" ToolTip="Update" Text="儲存" />
                    <asp:Button runat="server" CommandName="Cancel" ToolTip-="Cancel" Text="取消" />
                </EditItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>

    <div style="padding:40px"></div>

    <table class="table table-striped table-bordered templatemo-user-table">
        <asp:Repeater ID="History" runat="server">

            <HeaderTemplate>
                <tr>
                    <th>級距</th>
                    <th>油價</th>
                    <th>更新人員</th>
                    <th>生效日期</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# DataBinder.Eval(Container.DataItem, "step_distance") %></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "oil_price") %></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "create_user") %></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "create_date") %></td>
                </tr>
            </ItemTemplate>

        </asp:Repeater>
    </table>

</asp:Content>

