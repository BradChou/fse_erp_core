﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting; 	//Chart Web 伺服器控制項的方法和屬性
using System.Drawing;                   //繪圖功能的存取
using System.Collections;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;
using System.Web.Services;

public partial class store1_2_edit : System.Web.UI.Page
{
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

            #region 客代           
            SqlCommand cmd = new SqlCommand();
            //cmd.CommandText = @"SELECT substring(customer_code,1,8) as customer_code , customer_name,0 as chk 
            //                    FROM  (SELECT *,ROW_NUMBER() OVER(PARTITION BY substring(customer_code, 1, 8) ORDER BY substring(customer_code, 1, 8) DESC) AS rn
            //                           FROM tbCustomers where 1 = 1 and stop_shipping_code = '0') cus
            //                    where rn = 1 order by customer_code ";
            cmd.CommandText = @"select customer_code, customer_shortname  ,0 as chk  
                                    from tbCustomers with(nolock)
                                    where pricing_code = '05' and stop_shipping_code = '0'
                                    order by customer_code
                                    ";
            rp_cus.DataSource = dbAdapter.getDataTable(cmd);
            rp_cus.DataBind();
            #endregion
            readdata(a_id);


        }
    }

    private void readdata(string a_id)
    {
        if (a_id != "")
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);

            cmd.CommandText = @"Select * from ttItemCodesFieldArea A with(nolock) 
                                Where seq = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                Area.Text = dt.Rows[0]["code_name"].ToString().Trim();
                //客代編號                
                SqlCommand cmd5 = new SqlCommand();
                //cmd5.CommandText = @"select a.customer_code ,a.customer_name ,CASE WHEN isNULL(B.chk, 0) > 0 THEN 1 ELSE 0 END AS chk
                //                     from  (SELECT substring(customer_code,1,8) as customer_code , customer_name 
                //                            FROM   (SELECT *,
                //                                    ROW_NUMBER() OVER (PARTITION BY substring(customer_code,1,8) ORDER BY substring(customer_code,1,8) DESC) AS rn
                //                                    FROM tbCustomers where 1=1 and stop_shipping_code = '0' ) cus
                //                            where rn=1) a left join ttAssetsAreaCustomer as  b on a.customer_code =b.customer_code and b.area_id=@a_id
                //                    order by a.customer_code ";
                cmd5.CommandText = @"select a.customer_code ,a.customer_shortname ,CASE WHEN isNULL(B.chk, 0) > 0 THEN 1 ELSE 0 END AS chk
                                     from  (select customer_code, customer_shortname  
                                            from tbCustomers 
                                            where pricing_code = '05' and stop_shipping_code = '0') a left join ttAssetsAreaCustomer as  b on a.customer_code =b.customer_code and b.area_id=@a_id
                                    order by a.customer_code ";
                cmd5.Parameters.AddWithValue("@a_id", a_id);
                rp_cus.DataSource = dbAdapter.getDataTable(cmd5);
                rp_cus.DataBind();

            }
        }
        else
        {
            ApStatus = "Add";
        }


        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {
        switch(ApStatus)
        {
            case "Modity":
                statustext.Text = "修改";
            break;
            case "Add":                
                statustext.Text = "新增";            
            break;
        }

    }
    

    protected void btnsave_Click(object sender, EventArgs e)
    {
        int request_id = 0;
        int get_id = 0;
        var CodeNameStr = Func.GetRow("code_name", "ttItemCodesFieldArea", "", "code_name=@code_name", "code_name", Area.Text.ToString().Trim());
        
        string JStr = "";
        if (!string.IsNullOrEmpty(CodeNameStr) && a_id == "")
        {
            JStr = "alert('場區名稱已存在!');";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
            return;
        }
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        SqlCommand cmdInsert = new SqlCommand();
        SqlCommand cmdupd = new SqlCommand();
        switch (ApStatus)
        {
            case "Add":
                cmdInsert.Parameters.AddWithValue("@code_name", Area.Text.ToString().Trim());
                cmdInsert.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                //cmdInsert.CommandText = dbAdapter.SQLdosomething("ttItemCodesFieldArea", cmdInsert, "insert");
                cmdInsert.CommandText = dbAdapter.genInsertComm("ttItemCodesFieldArea", true, cmdInsert);

                try
                {
                    //dbAdapter.execNonQuery(cmdInsert);
                    if (int.TryParse(dbAdapter.getScalarBySQL(cmdInsert).ToString(), out request_id))
                    {
                        get_id = request_id;
                    }
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }
                if (ErrStr == "")
                {
                    //if (int.TryParse(dbAdapter.getScalarBySQL(cmdInsert).ToString(), out request_id))
                    //{
                    //    get_id = request_id;
                    //}

                    foreach (RepeaterItem item in this.rp_cus.Items)
                    {
                        CheckBox chk = item.FindControl("chkcus") as CheckBox;
                        if (chk.Checked)
                        {
                            string cus = (item.FindControl("Hid_cus") as HiddenField).Value;
                            SqlCommand cmd = new SqlCommand();
                            cmd.Parameters.AddWithValue("@area_id", get_id);
                            cmd.Parameters.AddWithValue("@customer_code", cus);
                            cmd.Parameters.AddWithValue("@chk", 1);
                            cmd.CommandText = dbAdapter.SQLdosomething("ttAssetsAreaCustomer", cmd, "insert");
                            dbAdapter.execNonQuery(cmd);
                        }
                    }

                }

                break;
            case "Modity":                
                cmdupd.Parameters.AddWithValue("@code_name", Area.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", a_id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttItemCodesFieldArea", cmdupd);
                   
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }
                if (ErrStr == "")
                {
                    SqlCommand cmdde = new SqlCommand();
                    cmdde.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "area_id", a_id);
                    cmdde.CommandText = dbAdapter.genDeleteComm("ttAssetsAreaCustomer", cmdde);
                    dbAdapter.execNonQuery(cmdde);
                    get_id = Convert.ToInt32(a_id);

                    foreach (RepeaterItem item in this.rp_cus.Items)
                    {
                        CheckBox chk = item.FindControl("chkcus") as CheckBox;
                        if (chk.Checked)
                        {
                            string cus = (item.FindControl("Hid_cus") as HiddenField).Value;
                            SqlCommand cmd = new SqlCommand();
                            cmd.Parameters.AddWithValue("@area_id", get_id);
                            cmd.Parameters.AddWithValue("@customer_code", cus);
                            cmd.Parameters.AddWithValue("@chk", 1);
                            cmd.CommandText = dbAdapter.SQLdosomething("ttAssetsAreaCustomer", cmd, "insert");
                            dbAdapter.execNonQuery(cmd);
                        }
                    }

                }

                break;

        }
        //if(ErrStr=="")
        //{
        //    if (ApStatus == "Modity")
        //    {
        //        SqlCommand cmdde = new SqlCommand();
        //        cmdde.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "area_id", a_id);
        //        cmdde.CommandText = dbAdapter.genDeleteComm("ttAssetsAreaCustomer", cmdde);
        //        dbAdapter.execNonQuery(cmdde);
        //        get_id = Convert.ToInt32(a_id);
        //    }
        //    else
        //    {             
        //        if (int.TryParse(dbAdapter.getScalarBySQL(cmdInsert).ToString(), out request_id))
        //        {
        //            get_id = request_id;
        //        }
        //    }
        //    foreach (RepeaterItem item in this.rp_cus.Items)
        //    {
        //        CheckBox chk = item.FindControl("chkcus") as CheckBox;
        //        if (chk.Checked)
        //        {
        //            string cus = (item.FindControl("Hid_cus") as HiddenField).Value;
        //            SqlCommand cmd = new SqlCommand();
        //            cmd.Parameters.AddWithValue("@area_id", get_id);
        //            cmd.Parameters.AddWithValue("@customer_code", cus);
        //            cmd.Parameters.AddWithValue("@chk", 1);
        //            cmd.CommandText = dbAdapter.SQLdosomething("ttAssetsAreaCustomer", cmd, "insert");
        //            dbAdapter.execNonQuery(cmd);
        //        }
        //    }

        //}

        if (ErrStr != "")
        {            
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='store1_2.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }
        return;
    }

    

    /// <summary>回傳訊息至前端 </summary>
    /// <param name="strMsg">訊息內容</param>
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            return;

        }

    }
    
    
}


