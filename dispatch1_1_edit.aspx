﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="dispatch1_1_edit.aspx.cs" Inherits="dispatch1_1_edit" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <!-- jQuery文件 -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>

    <link href="js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"></link>
    <script src="js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-timepicker-zh-TW.js" type="text/javascript"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });
        });
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {           

            $('.fancybox').fancybox();

            $("#carsel").fancybox({
                'width': 1200,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            $('.datetimepicker').prop("readonly", true).timepicker({
                showSecond: true, //顯示秒  
                timeFormat: 'HH:mm:ss', //格式化時間  
                controlType: "select"
            });
        });

        function ValidateFloat2(e, pnumber) {
            if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
                var newValue = /\d+[.]?[1-9]?/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                }
            }
            return false;
        }

    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="margin-bottom-10"><asp:Literal ID="task_type_text" runat="server"></asp:Literal>集貨-新增任務-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
            </div>
            <div class="modal-body">
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="Task_id">任務編號</label>
                                    <asp:TextBox ID="Task_id" runat="server"  CssClass="form-control" Enabled ="false" ></asp:TextBox>
                                </div>
                                <%--<div class="col-lg-4 bottnmargin form-inline">
                                    <label for="task_route"><span class="REDWD_b">*</span>路線</label>
                                    <asp:DropDownList ID="task_route" runat="server" CssClass="form-control "></asp:DropDownList>
                                </div>--%>
                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="task_date"><span class="REDWD_b">*</span>日期</label>
                                    <asp:TextBox ID="task_date" runat="server" CssClass="form-control date_picker" ></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="task_date" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入任務日期</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="supplier">供應商</label>
                                    <asp:DropDownList ID="supplier" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                                </div>
                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="driver">司　　機</label>
                                    <asp:TextBox ID="driver" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="car_license">車牌</label>
                                    <asp:TextBox ID="car_license" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>

                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="price">發包價</label>
                                    <asp:TextBox ID="price" runat="server" CssClass="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"></asp:TextBox>
                                </div>

                                <div class="col-lg-4 bottnmargin form-inline">
                                    <label for="memo">備　　註</label>
                                    <asp:TextBox ID="memo" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="max-height: 300px; overflow: auto;">
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table" />
                        <tr class="tr-only-hide">
                            <th class="_th">計價模式</th>
                            <th class="_th">發送日期</th>
                            <th class="_th">貨號</th>
                            <th class="_th">出貨客戶</th>
                            <th class="_th">出貨地點</th>
                            <th class="_th">出貨板數</th>
                            <th class="_th">出貨件數</th>
                            <th class="_th">收件人</th>
                            <th class="_th">配送區配商 </th>
                            
                        </tr>
                        <asp:Repeater ID="New_List" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                    <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pricing_type_name").ToString())%></td>
                                    <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%></td>
                                    <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                    <td data-th="出貨客戶"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                                    <td data-th="出貨地點"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_address").ToString())%></td>
                                    <td data-th="出貨板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                    <td data-th="出貨件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> </td>
                                    <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                    <td data-th="配送區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                   
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="9" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                </table>
                    </div>

                    <div class="form-group text-center" style="margin-top: 5px;">
                        <asp:Button ID="btnsave" CssClass="btn btn-primary " runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:LinkButton ID="btncancel" CssClass="btn btn-default " runat="server" OnClientClick="parent.$.fancybox.close();" >取消</asp:LinkButton>
                    </div>

                </div>
            </div>
        </div>
    </form>
</body>
</html>

