﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="trace_4_detail.aspx.cs" Inherits="trace_4_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style type="text/css">   
        .lbltip{margin-left:5px;}
        .table__scan,.pager{
            width:85%;
        }
        .table__scan tr:hover {
            background-color: lightyellow;
        }

        ._cknumber {
            cursor: pointer;
            padding: 3px;
            color: #337ab7;
        }

            ._cknumber:hover {
                color: #2c2c7f;
            }

        .td_th {
            text-align: center;
            padding: 5px;
        }

        .td_no {
            width: 5%;
        }

        .td_supplier {
            width: 15%;
        }

        .td_date {
            width: 10%;
        }

        .td_send_site {
            width: 10%;
        }

        .td_cknum {
            width: 20px;
        }

        .td_sender {
            width: 16%;
        }

        .td_receive {
            width: 16%;
        }

        .td_arrive {
            width: 8%;
        }
    </style>
    <script type="text/javascript">       
        $(document).ready(function () {

            $(".td_cknum").click(function () {            
                var _link = "trace_1.aspx?";
                _link = _link + "check_number=" + $(this).find("._cknumber").text().trim();              
                window.open(_link);
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
           <p>
                <asp:Label ID="lbl_tip" runat="server" CssClass="lbltip text-primary"></asp:Label>
           </p>
            <table class="table table-striped table-bordered templatemo-user-table table__scan">
                <tr>
                    <th class="td_th">No.</th>
                    <th class="td_th">區配商</th>
                    <th class="td_th">發送日期</th>
                    <th class="td_th">發送站</th>
                    <th class="td_th">貨  號</th>
                    <th class="td_th">寄件人</th>
                    <th class="td_th">收件人</th>
                    <th class="td_th">配達區分</th>
                </tr>
                <asp:Repeater ID="list_scan" runat="server">
                    <ItemTemplate>
                        <tr class ="paginate" >
                            <td class="td_data td_no" data-th="No."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ord").ToString())%></td>
                            <td class="td_data td_supplier" data-th="區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                            <td class="td_data td_date" data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td class="td_data td_send_site" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                            <td class="td_data td_cknum" data-th="貨號">
                                <span class="_cknumber">
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>
                                </span>
                            </td>
                            <td class="td_data td_sender" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td class="td_data td_receive" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td class="td_data td_arrive" data-th="配達區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <% if (list_scan.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="8" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

