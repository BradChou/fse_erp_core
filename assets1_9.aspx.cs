﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_9 : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            
            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (keyword.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
            wherestr += "  and (a.form_id like '%'+@keyword+'%' or a.Insurance_name like '%'+@keyword+'%'  or B.car_license like '%'+@keyword+'%')";
        }
       
        objCommand.CommandText = string.Format(@"select A.* , B.owner , B.business_model , B.[user] , B.dept , B.car_license ,
                                                D.user_name , A.udate
                                                from  ttInsurance A with(nolock)
                                                left join ttAssets B with(nolock) on A.assets_id = B.a_id 
                                                left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                where 1=1  {0} 
                                                ", wherestr);
        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {   
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }
}