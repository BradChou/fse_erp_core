﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_1_4.aspx.cs" Inherits="money_1_4" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript">
        $(document).ready(function () {

            close_typeChange();

            $("#<%= Suppliers.ClientID%>").change(function () {
                SuppliersChange();
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                close_typeChange();
            }
        });

        function SuppliersChange() {
            $("#<%= lbSuppliers.ClientID%>").html($("#<%= Suppliers.ClientID%> option:selected").text());
            var type = $("#<%= dlclose_type.ClientID%> option:selected").val()
            if (type == '1') {
                ChangeCloseDay();
            }
        }

        function close_typeChange() {
            $("#<%= dlclose_type.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '1':
                        $("._close").show();
                        $("._daterange").html('請款期間');
                        //ChangeCloseDay();
                        break;
                    case '2':
                        $("._close").hide();
                        $("._daterange").html('發送期間');
                        break;
                }
            });
        }

        function SetClosedayDDLEmpty() {
            $("#<%= dlCloseDay.ClientID%>").empty();
            $("#<%= dlCloseDay.ClientID%>").append($('<option></option>').val('').text('請選擇'));
        }
        function ChangeCloseDay() {
            var sel_supplier_code = $.trim($("#<%= Suppliers.ClientID%> option:selected").val());
            var sel_dates = $.trim($("#<%= dates.ClientID%>").val());
            var sel_datee = $.trim($("#<%= datee.ClientID%>").val());
            if (sel_supplier_code.length == 0) {
                SetClosedayDDLEmpty();
            }
            else {
                $.ajax(
                {
                    url: "money_1_4.aspx/GetCloseDayDDLHtml",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ dates: sel_dates, datee: sel_datee, supplier_code: sel_supplier_code}),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d.length > 0) {
                        $("#<%= dlCloseDay.ClientID%>").html(data.d);
                        $("#<%= dlCloseDay.ClientID%>").trigger("chosen:updated");
                    }
                    else {
                        SetClosedayDDLEmpty();
                    }
                });
            }
        }
    </script>--%>
    <style>
        ._th {
            text-align: center;
        }
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">超商應收</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label id="lbdaterange" runat="server" class="_daterange" >請款期間</label>
                            <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate" autocomplete="off"></asp:TextBox>
                            ~ 
                            <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate" autocomplete="off"></asp:TextBox>

                            <%--<label for="close_date">客代結帳日</label>
                            <asp:DropDownList ID="close_date" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="close_date_SelectedIndexChanged1">
                                <asp:ListItem Value="">請選擇</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="29">29</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="31">31</asp:ListItem>
                            </asp:DropDownList>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label>客戶代號</label>
                           <%-- <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged"></asp:DropDownList>--%>
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack ="true"  OnSelectedIndexChanged ="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                            次客代 
                            <asp:DropDownList ID="second_code" runat="server" CssClass="form-control chosen-select" AutoPostBack ="true" OnSelectedIndexChanged="second_code_SelectedIndexChanged"   ></asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="second_code" ForeColor="Red" ValidationGroup="validate">請選次客代</asp:RequiredFieldValidator>
                            <asp:DropDownList ID="dlclose_type" CssClass="form-control" runat="server" OnSelectedIndexChanged ="dlclose_type_SelectedIndexChanged" AutoPostBack ="true" >
                                <asp:ListItem Value="1">1. 已出帳</asp:ListItem>
                                <asp:ListItem Value="2">2. 未出帳</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="帳單查詢" OnClick="btQry_Click" />
                            <asp:Button ID="btClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');" />
                            <asp:Button ID="btExport" runat="server" class="templatemo-white-button " Text="匯出總表" OnClick="btExport_Click" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Suppliers" EventName="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btExport" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label id="lbCloseDay" runat ="server"  class="_close" >結帳日</label>
                            <asp:DropDownList ID="dlCloseDay" runat="server" CssClass="form-control _close"></asp:DropDownList>
                            <%--<button type="submit" class="btn btn-primary _close">重寄帳單</button>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <hr>
                

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <p class="text-primary">
                            供應商：<asp:Label ID="lbSuppliers" runat="server"></asp:Label><br>
                            請款期間：<asp:Label ID="lbdate" runat="server"></asp:Label>
                        </p>
                        <asp:Panel ID="Panel1" runat="server">
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th">序號</th>
                                    <th class="_th">日期</th>
                                    <th class="_th">日/夜</th>
                                    <th class="_th">溫層</th>
                                    <th class="_th">車老闆</th>
                                    <th class="_th">運務士</th>
                                    <th class="_th">車號</th>
                                    <th class="_th">車型</th>
                                    <th class="_th">主線</th>
                                    <th class="_th">爆量</th>
                                    <th class="_th">店到店</th>
                                    <th class="_th">回頭車</th>
                                    <th class="_th">加派車</th>
                                </tr>
                                <asp:Repeater ID="New_List_01" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="序號">
                                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                            </td>
                                            <td data-th="日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.InDate","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="日/夜"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DayNight").ToString())%></td>
                                            <td data-th="溫層"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Temperature_Text").ToString())%></td>
                                            <td data-th="車老闆"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.PaymentObject").ToString())%></td>
                                            <td data-th="運務士"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                            <td data-th="車號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                            <td data-th="車型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cabin_type_text").ToString())%></td>
                                            <td data-th="主線"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income_01","{0:N0}").ToString())%></td>
                                            <td data-th="爆量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income_02","{0:N0}").ToString())%></td>
                                            <td data-th="店到店"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income_03","{0:N0}").ToString())%></td>
                                            <td data-th="回頭車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income_04","{0:N0}").ToString())%></td>
                                            <td data-th="加派車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income_05","{0:N0}").ToString())%></td>

                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List_01.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="13" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                            <table class="paytable">
                                <tr>
                                    <th>小計：</th>
                                    <td>NT$ 
                            <asp:Label ID="subtotal_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>5%營業稅：</th>
                                    <td>NT$  
                            <asp:Label ID="tax_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>應收帳款：</th>
                                    <td>NT$   
                            <asp:Label ID="total_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                            </table>

<%--
                            <table class="table table-striped table-bordered templatemo-user-table" style="width: 50%">
                                <tr class="tr-only-hide">
                                    <th>場區</th>
                                    <th>實收運費</th>
                                    <th>稅額</th>
                                    <th>應收帳款</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="場區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                            <td data-th="實收運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subtotal","{0:N0}").ToString())%></td>
                                            <td data-th="稅額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax","{0:N0}").ToString())%></td>
                                            <td data-th="應收帳款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total","{0:N0}").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>--%>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

