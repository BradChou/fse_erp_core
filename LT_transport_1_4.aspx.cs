﻿using BarcodeLib;
using BObject.Bobjects;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RestSharp;
using System.Net;

public partial class LT_transport_1_4 : System.Web.UI.Page
{
    string send_station_code = "";
    bool check_address = true;
    public int start_number
    {
        get { return Convert.ToInt32(ViewState["start_number"]); }
        set { ViewState["start_number"] = value; }
    }

    public int end_number
    {
        get { return Convert.ToInt32(ViewState["end_number"]); }
        set { ViewState["end_number"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            #region 客代編號(套權限)
            //manager_type = Session["manager_type"].ToString(); //管理單位類別   
            //supplier_code = Session["master_code"].ToString();
            master_code = Session["master_code"].ToString();
            //supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            //if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            //{
            //    switch (manager_type)
            //    {
            //        case "1":
            //        case "2":
            //            supplier_code = "";
            //            break;
            //        default:
            //            supplier_code = "000";
            //            break;
            //    }
            //}
            manager_type = Session["manager_type"].ToString(); //管理單位類別   

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            string station_areaList = "";
            string station_scodeList = "";
            string managementList = "";

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    managementList += "'";
                                    managementList += dt1.Rows[i]["station_scode"].ToString();
                                    managementList += "'";
                                    managementList += ",";
                                }
                                managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scodeList += "'";
                                    station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                    station_scodeList += "'";
                                    station_scodeList += ",";
                                }
                                station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_areaList += "'";
                                    station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                    station_areaList += "'";
                                    station_areaList += ",";
                                }
                                station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode += dt1.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }

            using (SqlCommand cmd2 = new SqlCommand())
            {
                string wherestr = "";
                //if (supplier_code != "")
                //{
                //    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                //    wherestr = " and supplier_code = @supplier_code";
                //}
                //if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                //{
                //    cmd2.Parameters.AddWithValue("@master_code", master_code);
                //    wherestr += " and customer_code like @master_code+'%' ";
                //}

                if (supplier_code != "")
                {
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and A.customer_code like ''+@supplier_code+'%' ";
                }

                cmd2.Parameters.AddWithValue("@type", "1");
                //cmd2.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                //                                    from tbCustomers A with(Nolock)
                //                                    inner join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                //                                    where 0=0 and A.stop_shipping_code = '0' and A.type =@type {0} order by customer_code asc", wherestr);
                cmd2.CommandText = string.Format(@"Select customer_code , customer_code+ '-' + customer_shortname as name  
                                           From tbCustomers A with(Nolock) 
                                           Where stop_shipping_code = '0' and pricing_code = '02' and type =@type
                                            {0}
                                         order by customer_code asc", wherestr);
                dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                dlcustomer_code.DataValueField = "customer_code";
                dlcustomer_code.DataTextField = "name";
                dlcustomer_code.DataBind();
                dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }

            date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            #endregion

            readdata();

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('空白託運單已於10/25更新，請重新下載使用')", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('親愛的全速配客戶，您好： \\r\\r    暫停配送服務將從2023年5月1日開始，而最後的收貨日為2023年4月21日，爾後我們將不接受新的宅配訂單。一旦IT營運系統重整完成，我們將立即再次通知您，期能以更好的IT營運系統跟品質，再度為您提供服務。\\r    我們會在暫停配送期間提供運送替代方案的諮詢專線，供您參考使用。\\r諮詢專線：(02) 7733 - 7938#128 \\r專案服務人員：周世文 先生')", true);

        }
    }
    private void readdata()
    {

        string wherestr = "";
        SqlCommand cmd = new SqlCommand();


        if (supplier_code != "")
        {
            cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
            wherestr += " and A.customer_code like @supplier_code+'%' ";
        }

        cmd.Parameters.AddWithValue("@cdate", DateTime.Today);
        wherestr += " and CONVERT(VARCHAR,A.cdate,111)  = CONVERT(VARCHAR,@cdate,111)";


        cmd.CommandText = string.Format(@"Select A.request_id, A.cdate, convert(varchar, convert(dateTime,A.arrive_assign_date,111), 111) arrive_assign_date, A.supplier_date, A.print_date,A.print_flag, A.check_number ,A.receive_contact , 
                                          A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area , A.receive_address,
                                          A.pieces , A.plates , A.cbm ,
                                          B.code_name , A.customer_code , C.customer_shortname ,
                                          A.send_city , A.send_area , A.send_address ,
                                          A.arrive_to_pay_freight,A.collection_money,A.arrive_to_pay_append,A.invoice_desc,
                                          D.type  
                                         from tcDeliveryRequests A with(nolock)
                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C on C.customer_code = A.customer_code
                                          left join tbIORecords D on D.randomCode = A.import_randomCode
                                          where 1= 1  and A.import_randomCode <> '' and D.type = 1  and A.Less_than_truckload = 1 {0}
                                          AND  A.DeliveryType ='R'  
                                          order by  A.cdate desc", wherestr);   //and A.check_number =''
        DataTable dt = dbAdapter.getDataTable(cmd);
        New_List.DataSource = dt;
        New_List.DataBind();
        ltotalpages.Text = dt.Rows.Count.ToString();
    }

    public class ArriveSites
    {
        public string post_city { get; set; }
        public string post_area { get; set; }
        public string station_code { get; set; }

    }


    protected void btImport_Click(object sender, EventArgs e)
    {
        string ttErrStr = "";
        string customer_code = dlcustomer_code.SelectedValue;
        string supplier_code = "";
        string supplier_name = "";

        string receive_contact = "";
        string receive_tel1 = "";
        string receive_tel2 = "";
        string receive_city = "";
        string receive_area = "";
        string receive_address = "";

        string uniform_numbers = "";
        string pricing_type = "";
        List<ArriveSites> List = new List<ArriveSites>();
        string randomCode = Guid.NewGuid().ToString().Replace("-", "");
        randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;
        DateTime? startTime;
        DateTime? endTime;
        int successNum = 0;
        int failNum = 0;
        int totalNum = 0;
        int supplier_fee = 0;  //配送費用
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

        DateTime print_date = new DateTime();
        DateTime supplier_date = new DateTime();
        DateTime default_supplier_date = new DateTime();

        if (!DateTime.TryParse(date.Text, out print_date)) print_date = DateTime.Now;
        default_supplier_date = print_date;
        int Week = (int)print_date.DayOfWeek;
        //配送日自動產生，為 D+1
        default_supplier_date = default_supplier_date.AddDays(1);

        SortedList CK_list = new SortedList();

        //預購袋超值箱不能建逆物流
        //using (SqlCommand cmda = new SqlCommand())
        //{
        //    DataTable dta = new DataTable();
        //    cmda.Parameters.AddWithValue("customer_code", dlcustomer_code.SelectedValue);
        //    cmda.CommandText = @"select product_type from tbCustomers where customer_code = @customer_code";
        //    dta = dbAdapter.getDataTable(cmda);

        //    if (dta.Rows[0]["product_type"].ToString() == "2")
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋帳號無法建立退貨單號');</script>", false);
        //        return;
        //    }
        //    else if (dta.Rows[0]["product_type"].ToString() == "3")
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('超值箱帳號無法建立退貨單號');</script>", false);
        //        return;
        //    }
        //}

        //預購袋/超值箱客代需用同一主客代的月結客代才能打退貨單
        using (SqlCommand cmda = new SqlCommand())
        {
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("customer_code", dlcustomer_code.SelectedValue);
            cmda.CommandText = @"select product_type, MasterCustomerCode from tbCustomers where customer_code = @customer_code";
            dta = dbAdapter.getDataTable(cmda);

            string product_type = dta.Rows[0]["product_type"].ToString();
            string MasterCustomerCode = dta.Rows[0]["MasterCustomerCode"].ToString();

            var product_type_list = new List<string> { "2", "3" };

            if (product_type_list.Contains(product_type) && (dlcustomer_code.SelectedValue.ToString() != MasterCustomerCode))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋/超值箱客代需用同一主客代的月結客代才能打退貨單');</script>", false);
                return;
            }
        }


        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
                              " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                              " where c.customer_code = '" + customer_code + "' ";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
                if (dt.Rows.Count > 0)
                {
                    receive_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
                    receive_tel1 = dt.Rows[0]["telephone"].ToString();                        //電話
                    receive_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
                    receive_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
                    receive_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
                    uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
                    supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
                    if (supplier_code == "001")
                    {
                        supplier_name = "零擔";
                    }
                    else if (supplier_code == "002")
                    {
                        supplier_name = "流通";
                    }
                    else
                    {
                        supplier_name = dt.Rows[0]["supplier_name"].ToString().Trim();  // 區配
                    }
                    pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();        // 計價模式
                }
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select post_city , post_area,  station_code   from ttArriveSitesScattered with(nolock)";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                List = DataTableExtensions.ToList<ArriveSites>(dt).ToList();
            }
        }

        string check_number = "";
        string order_number = "";


        string send_contact = "";
        string send_city = "";
        string send_area = "";
        string send_address = "";
        string send_tel = "";


        string subpoena_code = "11"; //逆物流傳票類別為元付
        string pieces = "1";
        string cbmWeight = "";
        string invoice_memo = "";
        string collection_money = "";
        string arrive_assign_date = "";
        string receipt_flag = "";
        string pallet_recycling_flag = "";
        string invoice_desc = "";
        string check_type = "001";        //託運類別：001一般    
        string product_category = "001";  //商品種類：001一般
        string time_period = "午";        //配送時間：早、午、晚
        string sub_check_number = "000";  //次貨號
        string area_arrive_code = "";     //到著碼
        string receive_by_arrive_site_flag = "0";
        string print_flag = "0";
        int CbmSize = 0;
        string Distributor = "";
        string ProductId = "";
        string SpecCodeId = "";

        lbMsg.Items.Clear();
        if (file01.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            string ttFileName = ttPath + file01.FileName;

            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            IWorkbook workbook = null;
            ISheet u_sheet = null;
            IFormulaEvaluator formulaEvaluator = null;
            string ttSelectSheetName = "";
            if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                if (ttExtName == ".xls")
                {

                    workbook = new HSSFWorkbook(file01.FileContent);
                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new HSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到
                    //版本號檢測
                    string sheet_version = workbook.GetSheetName(1);
                    if (sheet_version != "20221020")
                    {
                        lbMsg.Items.Add("託運單匯入失敗，請下載最新版本空白託運單。");
                        return;
                    }
                }
                else if (ttExtName == ".xlsx")
                {
                    workbook = new XSSFWorkbook(file01.FileContent);
                    u_sheet = (XSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new XSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到

                }

                u_sheet.ForceFormulaRecalculation = true; //要求公式重算結果 

                try
                {

                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);

                    u_sheet.ForceFormulaRecalculation = true; //要求公式重算結果

                    ttSelectSheetName = workbook.GetSheetName(0);

                    if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                    {
                        Boolean IsSheetOk = true;//工作表驗證
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                        //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                        string strRow, strInsCol, strInsVal;
                        List<StringBuilder> sb_del_list = new List<StringBuilder>();
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_del = new StringBuilder();
                        StringBuilder sb_temp_ins = new StringBuilder();
                        StringBuilder stringBuilder = new StringBuilder();
                        List<StringBuilder> stringBuilders = new List<StringBuilder>();


                        IRow Row_title = null;
                        if (u_sheet.LastRowNum > 0) Row_title = u_sheet.GetRow(2);

                        for (int i = u_sheet.FirstRowNum + 3; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                        {
                            IRow Row = u_sheet.GetRow(i);  //取得目前的資料列  
                            if (Row != null)
                            {
                                #region 初始化
                                send_contact = "";
                                send_city = "";
                                send_area = "";
                                send_address = "";
                                send_tel = "";
                                invoice_memo = "";
                                check_number = "";
                                order_number = "";
                                subpoena_code = "11"; //逆物流傳票類別為元付
                                pieces = "";
                                collection_money = "";
                                arrive_assign_date = "";
                                receipt_flag = "0";
                                pallet_recycling_flag = "0";
                                invoice_desc = "";
                                area_arrive_code = "";
                                strInsCol = "";
                                strInsVal = "";
                                sub_check_number = "000";
                                CbmSize = 0;
                                supplier_date = default_supplier_date;
                                strRow = (i + 1).ToString();
                                ProductId = "";
                                SpecCodeId = "";


                                #endregion

                                //if (Row.GetCell(0) == null || Row.GetCell(0).ToString() == "")
                                //{
                                //    sb_ins_list.Add(sb_temp_ins);
                                //    sb_temp_ins = new StringBuilder();
                                //    break;
                                //}
                                totalNum += 1;
                                successNum += 1;
                                #region 驗證&取值 IsSheetOk
                                for (int j = 0; j <= 10; j++)
                                {
                                    Boolean IsChkOk = true;
                                    ICell cell = Row.GetCell(j);
                                    switch (j)
                                    {
                                        case 0:  //託運單號
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) check_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) check_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        check_number = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    check_number = Row.GetCell(j).ToString();
                                                }
                                            }
                                            //不抓正物流單號的規格，改用客戶自己填的


                                            //using (SqlCommand cmdp = new SqlCommand())
                                            //{
                                            //    cmdp.CommandText = "select ProductId,  SpecCodeId  from tcDeliveryRequests where check_number = '" + check_number + "' ";
                                            //    using (DataTable dtp = dbAdapter.getDataTable(cmdp))
                                            //        if (dtp.Rows.Count > 0)
                                            //        {
                                            //            ProductId = dtp.Rows[0]["ProductId"].ToString().Trim();           //產品名稱
                                            //            SpecCodeId = dtp.Rows[0]["SpecCodeId"].ToString().Trim();           //產品規格
                                            //        }
                                            //    if (SpecCodeId == "") 
                                            //    {
                                            //        using (SqlCommand cmds = new SqlCommand())
                                            //        {
                                            //            cmds.CommandText = "select product_type from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue.ToString().Trim() + "' ";
                                            //            using (DataTable dtp = dbAdapter.getDataTable(cmds))
                                            //                if (dtp.Rows.Count > 0)
                                            //                {
                                            //                    string product_type = dtp.Rows[0]["product_type"].ToString().Trim();
                                            //                    if (product_type == "1")  //一般件 沒有的話選 S090
                                            //                    {
                                            //                        SpecCodeId = "S090";
                                            //                    }
                                            //                    else if (product_type == "2")  //預購袋 沒有的話選 B003
                                            //                    {
                                            //                        SpecCodeId = "B003";
                                            //                    }
                                            //                    else if (product_type == "3")  //超值箱 沒有的話選 X003
                                            //                    {
                                            //                        SpecCodeId = "X003";
                                            //                    }
                                            //                    else if (product_type == "4"|| product_type == "5")  //商城、內部文件 沒有的話選 N001
                                            //                    {
                                            //                        SpecCodeId = "N001";
                                            //                    }
                                            //                    else { SpecCodeId = "S090"; }
                                            //                }
                                            //        }
                                            //    }
                                            //}
                                            break;
                                        case 1:  //訂單編號
                                            //if (Row.GetCell(j) != null) order_number = Row.GetCell(j).ToString();
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) order_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) order_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        order_number = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    order_number = Row.GetCell(j).ToString();
                                                }
                                            }

                                            break;
                                        case 2:  //退貨人姓名
                                            #region 退貨人姓名
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) send_contact = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) send_contact = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態

                                                    }
                                                    catch
                                                    {
                                                        send_contact = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    send_contact = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (String.IsNullOrEmpty(send_contact))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 3:  //退貨人電話1
                                            #region 退貨人電話1
                                            if (cell != null)
                                            {
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        send_tel = "";
                                                        if (String.IsNullOrEmpty(send_tel))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        send_tel = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(send_tel))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        send_tel = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                    case CellType.Formula: //公式

                                                        try
                                                        {
                                                            var formulaValue = formulaEvaluator.Evaluate(cell);
                                                            if (formulaValue.CellType == CellType.String) send_tel = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                            else if (formulaValue.CellType == CellType.Numeric) send_tel = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        }
                                                        catch
                                                        {
                                                            send_tel = cell.StringCellValue;
                                                        }
                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 4:  //退貨人電話2 

                                            #region 退貨人電話2  
                                            if (cell != null)
                                            {
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        send_tel += "";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        send_tel += "/" + Row.GetCell(j).ToString();
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        send_tel += "/" + Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                    case CellType.Formula: //公式
                                                        try
                                                        {
                                                            var formulaValue = formulaEvaluator.Evaluate(cell);
                                                            if (formulaValue.CellType == CellType.String) send_tel += "/" + formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                            else if (formulaValue.CellType == CellType.Numeric) send_tel += "/" + formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        }
                                                        catch
                                                        {
                                                            send_tel += "/" + cell.StringCellValue;
                                                        }

                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;

                                        case 5:  //退貨人地址
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) send_address = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) send_address = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        send_address = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    send_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                }
                                            }

                                            //檢查地址
                                            GetAddressParsing(send_address.Trim(), customer_code.Trim(), "1", "1");
                                            if (check_address == false)
                                            {
                                                lbMsg.Items.Add("託運單匯入失敗，請確認地址是否正確。");
                                                return;
                                            }


                                            //呼叫地址正規化SP，拆出縣市、鄉鎮區
                                            using (SqlCommand cmd_addr = new SqlCommand())
                                            {

                                                string ChinaFoTaiwan = PublicFunction.ToTraditional(send_address);
                                                ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                                                ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");

                                                cmd_addr.Parameters.AddWithValue("@address", ChinaFoTaiwan);
                                                cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                                cmd_addr.CommandType = CommandType.StoredProcedure;
                                                try
                                                {
                                                    DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                                    if (dt_addr != null && dt_addr.Rows.Count > 0)
                                                    {
                                                        send_city = dt_addr.Rows[0]["city"].ToString();
                                                        send_area = dt_addr.Rows[0]["area"].ToString();
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    IsChkOk =
                                                    IsSheetOk = false;
                                                }
                                            }
                                            if (String.IsNullOrEmpty(check_number))
                                            {
                                                using (SqlCommand cmd_Station = new SqlCommand())
                                                {
                                                    cmd_Station.Parameters.AddWithValue("@post_city", receive_city);
                                                    cmd_Station.Parameters.AddWithValue("@post_area", receive_area);
                                                    cmd_Station.CommandText = "select station_code from ttArriveSitesScattered where post_city = @post_city and post_area = @post_area ";

                                                    try
                                                    {
                                                        DataTable dt_Station = dbAdapter.getDataTable(cmd_Station);
                                                        if (dt_Station != null && dt_Station.Rows.Count > 0)
                                                        {
                                                            area_arrive_code = dt_Station.Rows[0]["station_code"].ToString();
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                using (SqlCommand cmd2 = new SqlCommand())
                                                {
                                                    DataTable dt2 = new DataTable();

                                                    cmd2.Parameters.AddWithValue("@check_number", check_number.Trim());

                                                    cmd2.CommandText = @"select return_address_flag,station_code,* from tcDeliveryRequests R1
left join ttArriveSitesScattered A1 on send_city=post_city and send_area=post_area
left join tbCustomers C1 on C1.customer_code=R1.customer_code
where check_number=@check_number";
                                                    dt2 = dbAdapter.getDataTable(cmd2);

                                                    if (dt2.Rows.Count > 0)
                                                    {
                                                        string return_address_flag = dt2.Rows[0]["return_address_flag"].ToString();

                                                        if (return_address_flag != "True")
                                                        {
                                                            area_arrive_code = dt2.Rows[0]["supplier_code"].ToString().Replace("F", ""); //到著碼

                                                        }
                                                        else
                                                        {
                                                            area_arrive_code = dt2.Rows[0]["station_code"].ToString();
                                                            receive_city = dt2.Rows[0]["send_city"].ToString();
                                                            receive_area = dt2.Rows[0]["send_area"].ToString();
                                                            receive_address = dt2.Rows[0]["send_address"].ToString();

                                                        }

                                                    }
                                                }

                                            }

                                            send_address = send_city != "" ? send_address.Replace(send_city, "") : send_address;
                                            send_address = send_area != "" ? send_address.Replace(send_area, "") : send_address;

                                            break;
                                        case 6://件數
                                            #region 件數
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) pieces = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) pieces = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        pieces = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    pieces = Row.GetCell(j).ToString();
                                                }
                                            }
                                            if (String.IsNullOrEmpty(pieces) && !Utility.IsNumeric(pieces))
                                            {
                                                IsSheetOk = false;
                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!");
                                            }

                                            #endregion
                                            break;
                                        case 7://產品名稱
                                            #region 產品名稱
                                            //須檢查欄位值是否符合客代類型
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) ProductId = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) ProductId = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        ProductId = cell.StringCellValue.Trim();
                                                    }
                                                }
                                                else
                                                {
                                                    ProductId = Row.GetCell(j).ToString().Trim();
                                                }
                                            }
                                            using (SqlCommand cmda = new SqlCommand())
                                            {
                                                //登入的帳號
                                                cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + Session["account_code"] + "' ";
                                                using (DataTable dta = dbAdapter.getDataTable(cmda))
                                                    if (dta.Rows.Count > 0)
                                                    {
                                                        string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                                                        bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                                                        if (product_type == "1" && is_new_customer == true) //新客代、月結
                                                        {
                                                            if (ProductId != "論S")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else if (product_type == "1") //一般月結
                                                        {
                                                            if (ProductId != "月結")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }

                                                        else if (product_type == "2") //預購袋客代
                                                        {
                                                            if (ProductId != "預購袋" && ProductId != "速配箱")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else if (product_type == "3") //超值箱客代
                                                        {
                                                            if (ProductId != "超值袋" && ProductId != "超值箱")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else if (product_type == "4") //內部文件客代
                                                        {
                                                            if (ProductId != "內部文件")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else if (product_type == "5") //商城出貨客代
                                                        {
                                                            if (ProductId != "包材")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else if (product_type == "6") //商城出貨客代
                                                        {
                                                            if (ProductId != "論件")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                            }
                                                        }
                                                        else { }


                                                    }
                                                    else
                                                    {
                                                        //選定的客代
                                                        using (SqlCommand cmdb = new SqlCommand())
                                                        {
                                                            //登入的帳號
                                                            cmdb.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue.ToString());
                                                            cmdb.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = @customer_code ";
                                                            using (DataTable dtb = dbAdapter.getDataTable(cmdb))
                                                                if (dtb.Rows.Count > 0)
                                                                {
                                                                    string product_type2 = dtb.Rows[0]["product_type"].ToString().Trim();
                                                                    bool is_new_customer2 = (bool)dtb.Rows[0]["is_new_customer"];
                                                                    if (product_type2 == "1" && is_new_customer2 == true) //新客代、月結
                                                                    {
                                                                        if (ProductId != "論S")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "1") //一般月結
                                                                    {
                                                                        if (ProductId != "月結")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "2" && is_new_customer2 == true) //預購袋 新客代
                                                                    {
                                                                        if (ProductId != "預購袋")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 此客代類型僅能選擇預購袋");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "2") //預購袋客代
                                                                    {
                                                                        if (ProductId != "預購袋" && ProductId != "速配箱")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "3") //超值箱客代
                                                                    {
                                                                        if (ProductId != "超值袋" && ProductId != "超值箱")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "4") //內部文件客代
                                                                    {
                                                                        if (ProductId != "內部文件")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "5") //商城出貨客代
                                                                    {
                                                                        if (ProductId != "包材")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type2 == "6") //商城出貨客代
                                                                    {
                                                                        if (ProductId != "論件")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else { }



                                                                }
                                                        }
                                                    }
                                            }
                                            if (String.IsNullOrEmpty(ProductId))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 8://產品規格
                                            #region 產品規格
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) SpecCodeId = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) SpecCodeId = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        SpecCodeId = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    SpecCodeId = Row.GetCell(j).ToString();
                                                }
                                            }
                                            //名稱規格相匹配
                                            if (ProductId.Trim() == "月結" || ProductId.Trim() == "論S") //新舊客代、月結
                                            {
                                                if (SpecCodeId != "1號袋" && SpecCodeId != "2號袋" && SpecCodeId != "3號袋" && SpecCodeId != "S60" && SpecCodeId != "S90" && SpecCodeId != "S110" && SpecCodeId != "S120" && SpecCodeId != "S150")
                                                {
                                                    IsSheetOk = false;
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                }
                                            }
                                            else if (ProductId.Trim() == "預購袋" || ProductId.Trim() == "超值袋") //預購袋、超值袋
                                            {
                                                if (SpecCodeId != "1號袋" && SpecCodeId != "2號袋" && SpecCodeId != "3號袋" && SpecCodeId != "4號袋")
                                                {
                                                    IsSheetOk = false;
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                }
                                            }
                                            else if (ProductId.Trim() == "速配箱" || ProductId.Trim() == "超值箱") //速配箱、超值箱
                                            {
                                                if (SpecCodeId != "1號箱" && SpecCodeId != "2號箱" && SpecCodeId != "5號箱")
                                                {
                                                    IsSheetOk = false;
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                }
                                            }
                                            else if (ProductId.Trim() == "內部文件" || ProductId.Trim() == "包材") //內部文件、包材
                                            {
                                                if (SpecCodeId != "不指定")
                                                {
                                                    IsSheetOk = false;
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                }
                                            }
                                            else if (ProductId.Trim() == "論件") //論件
                                            {
                                                if (SpecCodeId != "1才內" && SpecCodeId != "2才內" && SpecCodeId != "3才內" && SpecCodeId != "4才內" && SpecCodeId != "5才內")
                                                {
                                                    IsSheetOk = false;
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                }
                                            }


                                            using (SqlCommand cmda = new SqlCommand())
                                            {
                                                cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue + "' ";
                                                using (DataTable dta = dbAdapter.getDataTable(cmda))
                                                    if (dta.Rows.Count > 0)
                                                    {
                                                        string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                                                        bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                                                        //if (product_type == "1" && is_new_customer == false) //一般月結
                                                        //{
                                                        //    if ((pieces != "1" && ProductId != "月結多筆") || (pieces == "1" && ProductId != "月結單筆"))
                                                        //    {
                                                        //        IsSheetOk = false;
                                                        //        lbMsg.Items.Add("第" + strRow + "列： 請確認【規格、件數】是否相符!");
                                                        //    }
                                                        //}

                                                        if (product_type == "2" && is_new_customer == true) //預購袋 新客代
                                                        {
                                                            if (SpecCodeId.Trim() != "3號袋")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 此客代類型僅能選擇3號袋");
                                                            }

                                                        }
                                                        if (product_type == "6")
                                                        {
                                                            using (SqlCommand cmd9 = new SqlCommand())
                                                            {

                                                                cmd9.CommandText = "select ProductId, CodeId,CodeName from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec'where ProductId = @ProductId and  CustomerCode = @customerCode and DeliveryType = 'R' order by i.OrderBy asc";
                                                                cmd9.Parameters.AddWithValue("@customerCode", dlcustomer_code.SelectedValue);


                                                                cmd9.Parameters.AddWithValue("@ProductId", "CM000043");

                                                                DataTable dtb = dbAdapter.getDataTableForFSE01(cmd9);
                                                                var SpecId_List = new List<string>();
                                                                for (int k = 0; k < dtb.Rows.Count; k++)
                                                                {
                                                                    SpecId_List.Add(dtb.Rows[k]["CodeName"].ToString());
                                                                }


                                                                bool checkSpec = SpecId_List.Contains(SpecCodeId);

                                                                if (!checkSpec)
                                                                {
                                                                    IsSheetOk = false;
                                                                    lbMsg.Items.Add("第" + strRow + "列： 此客代類型沒有此種產品規格");
                                                                }

                                                            }
                                                        }


                                                    }
                                            }

                                            if (String.IsNullOrEmpty(SpecCodeId))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }

                                            #endregion
                                            break;
                                        case 9:  //收回品項
                                            //if (Row.GetCell(j) != null) invoice_memo = Row.GetCell(j).ToString();
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) invoice_memo = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) invoice_memo = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        invoice_memo = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    invoice_memo = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                        case 10:  //備註
                                            if (Row.GetCell(j) != null) invoice_desc = Row.GetCell(j).ToString();

                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) invoice_desc = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) invoice_desc = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        invoice_desc = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    invoice_desc = Row.GetCell(j).ToString();
                                                }
                                            }
                                            //if (String.IsNullOrEmpty(invoice_desc))
                                            //{
                                            //    IsChkOk =
                                            //    IsSheetOk = false;
                                            //}
                                            break;
                                    }
                                    if (!IsChkOk)
                                    {
                                        //lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!");
                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row.GetCell(j).ToString() + "】 是否為正確元託運單編號!");

                                    }
                                }

                                #endregion

                                #region 組新增
                                //if (area_arrive_code == "")
                                //{
                                //    ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                //    if (_ArriveSites != null)
                                //    {
                                //        area_arrive_code = _ArriveSites.station_code;  //到著碼
                                //        //Distributor = _ArriveSites.supplier_code;          //配送商
                                //    }
                                //    //int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                //    //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
                                //}
                                send_address = send_address.Replace("'", "＇");
                                send_address = send_address.Replace("--", "－");
                                send_tel = (send_tel.Length > 20 ? send_tel.Substring(0, 20) : send_tel);
                                send_contact = send_contact.Replace("\n", "");
                                send_contact = (send_contact.Length > 20 ? send_contact.Substring(0, 20) : send_contact);
                                send_address = send_address.Replace("\n", "");
                                send_address = (send_address.Length > 100 ? send_address.Substring(0, 100) : send_address);


                                if (pieces == "")
                                {
                                    pieces = "1";
                                }

                                if (Convert.ToInt32(pieces) > 1)
                                {


                                    switch (ProductId)    //產品名稱
                                    {
                                        case "論S":
                                            ProductId = "CM000030";
                                            break;
                                        case "月結":
                                            ProductId = "CM000036";
                                            break;
                                        case "論件":
                                            ProductId = "CM000043";
                                            break;
                                        case "內部文件":
                                            ProductId = "CS000037";
                                            break;
                                        case "包材":
                                            ProductId = "CS000038";
                                            break;
                                        case "預購袋":
                                            ProductId = "PS000031";
                                            break;
                                        case "速配箱":
                                            ProductId = "PS000032";
                                            break;
                                        case "超值袋":
                                            ProductId = "PS000033";
                                            break;
                                        case "超值箱":
                                            ProductId = "PS000034";
                                            break;
                                        default:
                                            ProductId = "";
                                            break;
                                    }
                                }
                                else
                                {
                                    switch (ProductId)    //產品名稱
                                    {
                                        case "論S":
                                            ProductId = "CM000030";
                                            break;
                                        case "月結":
                                            ProductId = "CS000035";
                                            break;
                                        case "論件":
                                            ProductId = "CM000043";
                                            break;
                                        case "內部文件":
                                            ProductId = "CS000037";
                                            break;
                                        case "包材":
                                            ProductId = "CS000038";
                                            break;
                                        case "預購袋":
                                            ProductId = "PS000031";
                                            break;
                                        case "速配箱":
                                            ProductId = "PS000032";
                                            break;
                                        case "超值袋":
                                            ProductId = "PS000033";
                                            break;
                                        case "超值箱":
                                            ProductId = "PS000034";
                                            break;
                                        default:
                                            ProductId = "";
                                            break;
                                    }
                                }

                                switch (SpecCodeId)    //產品規格
                                {
                                    case "1號袋":
                                        SpecCodeId = "B001";
                                        CbmSize = 7;
                                        break;
                                    case "2號袋":
                                        SpecCodeId = "B002";
                                        CbmSize = 7;
                                        break;
                                    case "3號袋":
                                        SpecCodeId = "B003";
                                        CbmSize = 7;
                                        break;
                                    case "4號袋":
                                        SpecCodeId = "B004";
                                        CbmSize = 7;
                                        break;
                                    case "1號箱":
                                        SpecCodeId = "X001";
                                        CbmSize = 2;  //箱裝預設S90
                                        break;
                                    case "2號箱":
                                        SpecCodeId = "X002";
                                        CbmSize = 2;  //箱裝預設S90
                                        break;
                                    case "5號箱":
                                        SpecCodeId = "X005";
                                        CbmSize = 2;  //箱裝預設S90
                                        break;
                                    case "S60":
                                        SpecCodeId = "S060";
                                        CbmSize = 1;
                                        break;
                                    case "S90":
                                        SpecCodeId = "S090";
                                        CbmSize = 2;
                                        break;
                                    case "S110":
                                        SpecCodeId = "S110";
                                        CbmSize = 6;
                                        break;
                                    case "S120":
                                        SpecCodeId = "S120";
                                        CbmSize = 3;
                                        break;
                                    case "S150":
                                        SpecCodeId = "S150";
                                        CbmSize = 4;
                                        break;
                                    case "不指定":
                                        SpecCodeId = "N001";
                                        break;
                                    case "固定價格":
                                        SpecCodeId = "9999";
                                        break;
                                    case "1才內":
                                        SpecCodeId = "C001";
                                        break;
                                    case "2才內":
                                        SpecCodeId = "C002";
                                        break;
                                    case "3才內":
                                        SpecCodeId = "C003";
                                        break;
                                    case "4才內":
                                        SpecCodeId = "C004";
                                        break;
                                    case "5才內":
                                        SpecCodeId = "C005";
                                        break;
                                    default:
                                        SpecCodeId = "";
                                        break;
                                }


                                strInsCol = "pricing_type,customer_code,check_number,order_number,check_type,subpoena_category,receive_tel1,receive_tel2,receive_contact,receive_city,receive_area,receive_address,area_arrive_code,receive_by_arrive_site_flag" +
                                            " ,pieces,cbmWeight,collection_money,send_contact,send_city,send_area,send_address,send_tel,product_category,invoice_desc,time_period,arrive_assign_date" +
                                            " ,receipt_flag,pallet_recycling_flag,cuser,cdate,uuser,udate,supplier_code,supplier_name,import_randomCode,print_date, supplier_date, print_flag, turn_board,upstairs,difficult_delivery" +
                                            " ,turn_board_fee,upstairs_fee,difficult_fee, add_transfer, Less_than_truckload, sub_check_number,DeliveryType,invoice_memo,return_check_number,ProductId,SpecCodeId";


                                if (Session["account_code"].ToString() != null && Session["account_code"].ToString().Length == 11 && Session["account_code"].ToString().StartsWith("F"))
                                {
                                    subpoena_code = "21"; //客戶自己建檔的話傳票類別為到付
                                }
                                string insert_check_number = "";
                                using (SqlCommand cmds = new SqlCommand())
                                {
                                    cmds.CommandText = string.Format(@"select NEXT VALUE FOR ttAutoNumeral");
                                    DataTable dts = dbAdapter.getDataTable(cmds);
                                    if (dts != null && dts.Rows.Count > 0)
                                    { insert_check_number = dts.Rows[0][0].ToString(); }
                                }
                                strInsVal = "N\'" + pricing_type + "\', " +
                                            "N\'" + customer_code + "\', " +
                                            "N\'" + insert_check_number + "\', " +
                                            "N\'" + order_number + "\', " +
                                            "N\'" + check_type + "\', " +
                                            "N\'" + subpoena_code + "\', " +
                                            "N\'" + receive_tel1 + "\', " +
                                            "N\'" + receive_tel2 + "\', " +
                                            "N\'" + receive_contact + "\', " +
                                            "N\'" + receive_city + "\', " +
                                            "N\'" + receive_area + "\', " +
                                            "N\'" + receive_address + "\', " +
                                            "N\'" + area_arrive_code + "\', " +
                                            "N\'" + receive_by_arrive_site_flag + "\', " +
                                            "N\'" + pieces + "\', " +
                                            "N\'" + cbmWeight + "\', " +
                                            "N\'" + collection_money + "\', ";
                                strInsVal += "N\'" + send_contact + "\', ";

                                //if (check_number == "")
                                //{
                                //    strInsVal += "N\'" + send_contact + "\', ";
                                //}
                                //else
                                //{
                                //    strInsVal += "N\'" + check_number + "\', ";
                                //}

                                strInsVal += "N\'" + send_city + "\', " +
                                            "N\'" + send_area + "\', " +
                                            "N\'" + send_address + "\', " +
                                            "N\'" + send_tel + "\', " +
                                            "N\'" + product_category + "\', " +
                                            "N\'" + invoice_desc + "\', " +
                                            "N\'" + time_period + "\', ";
                                if (date.Text == "")
                                {
                                    strInsVal += "Null" + ", ";
                                }
                                else
                                {
                                    strInsVal += "N\'" + Convert.ToDateTime(date.Text).AddDays(1).ToString("yyyy/MM/dd hh:mm:ss") + "\', ";
                                }

                                strInsVal += "N\'" + receipt_flag + "\', " +
                                            "N\'" + pallet_recycling_flag + "\', " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + supplier_code + "\', " +
                                            "N\'" + supplier_name + "\', " +
                                            "N\'" + randomCode + "\', " +
                                            "N\'" + print_date.ToString("yyyy/MM/dd") + "\', " +
                                            "N\'" + supplier_date.ToString("yyyy/MM/dd") + "\', " +
                                            "N\'" + print_flag + "\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'" + "1" + "\', " +
                                            "N\'" + sub_check_number + "\' ," +
                                            "N\'R\' ," +
                                            "N\'" + invoice_memo + "\', " +
                                            "N\'" + check_number + "\', " +
                                            "N\'" + ProductId + "\' , " +
                                            "N\'" + SpecCodeId + "\' ";

                                string insertstr = "INSERT INTO tcDeliveryRequests (" + strInsCol + ") VALUES(" + strInsVal + "); ";
                                sb_temp_ins.Append(insertstr);

                                //每100筆組成一字串
                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }

                                string updatestr = "update tcDeliveryRequests set return_check_number = (select check_number from tcDeliveryRequests where return_check_number='" + check_number + "')  where check_number ='" + check_number + "';";
                                stringBuilder.Append(updatestr);

                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    stringBuilders.Add(stringBuilder);
                                    stringBuilder = new StringBuilder();
                                }

                                //寫入CBMDetailLog
                                using (SqlCommand cmdcbm = new SqlCommand())
                                {
                                    cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                                    cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);
                                    cmdcbm.Parameters.AddWithValue("@CBM", SpecCodeId);
                                    cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                                    cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                                    cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                                    dbAdapter.execNonQuery(cmdcbm);
                                }

                                #endregion
                            }
                        }

                        //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                        if (IsSheetOk)
                        {
                            #region 執行SQL

                            startTime = DateTime.Now;   //開始匯入時間
                            //新增
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    String strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }
                                    }
                                }
                            }
                            endTime = DateTime.Now;   //匯入結束時間
                            #endregion

                            #region 匯入記錄
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                cmd.Parameters.AddWithValue("@changeTable", "tcDeliveryRequests");
                                cmd.Parameters.AddWithValue("@fromWhere", "託運資料維護-匯入");
                                cmd.Parameters.AddWithValue("@randomCode", randomCode);
                                cmd.Parameters.AddWithValue("@memo", "");
                                cmd.Parameters.AddWithValue("@successNum", successNum.ToString());
                                cmd.Parameters.AddWithValue("@failNum", failNum.ToString());
                                cmd.Parameters.AddWithValue("@totalNum", totalNum.ToString());
                                cmd.Parameters.AddWithValue("@startTime", startTime);
                                cmd.Parameters.AddWithValue("@endTime", endTime);
                                cmd.Parameters.AddWithValue("@customer_code", customer_code);
                                cmd.Parameters.AddWithValue("@print_date", print_date);
                                cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);
                                cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                            #endregion

                            foreach (StringBuilder s in stringBuilders)
                            {
                                if (s.Length > 0)
                                {
                                    String SQL = s.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = SQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }
                                    }
                                }
                            }
                            readdata();
                        }
                        else
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");
                    }
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    //System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                }
                lbMsg.Items.Add("匯入完畢");
            }
        }
    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        //string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        //string ttFileName = "退貨空白託運單標準格式-零擔.xls";


        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "退貨空白託運單標準格式-零擔.xls";
        //根據客代類型給予不同下載檔案
        string account_code = (string)Session["account_code"];
        using (SqlCommand cmda = new SqlCommand())
        {
            cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + account_code + "' ";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
                if (dta.Rows.Count > 0)
                {
                    string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                    bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                    if (product_type == "1" && is_new_customer == true) //新客代、月結
                    {
                        ttFileName = "退貨空白託運單標準格式-零擔-論S.xls";
                    }
                    else if (product_type == "1") //一般月結
                    {
                        ttFileName = "退貨空白託運單標準格式-月結.xls";
                    }
                    else if (product_type == "2") //預購袋客代
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代無法產生退貨單，請洽客服人員');</script>", false);
                        return;
                    }
                    else if (product_type == "3") //超值箱客代
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代無法產生退貨單，請洽客服人員');</script>", false);
                        return;
                    }
                    else if (product_type == "4") //內部文件客代
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代無法產生退貨單，請洽客服人員');</script>", false);
                        return;
                    }
                    else if (product_type == "5") //商城出貨客代
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代無法產生退貨單，請洽客服人員');</script>", false);
                        return;
                    }
                    else if (product_type == "6") //論件客代
                    {
                        ttFileName = "退貨空白託運單標準格式-零擔-論件.xls";
                    }
                }
                else { ttFileName = "退貨空白託運單標準格式-零擔-內部員工用.xls"; }
        }



        //Clear();
        //Response.AddHeader("content-disposition", "attachment;  filename=" + ttFilePath + ttFileName);
        //Response.ContentType = "application/vnd.ms-excel";
        ////Response.BinaryWrite(p.GetAsByteArray());
        //Response.End();

        FileInfo xpath_file = new FileInfo(ttFilePath + ttFileName);
        // 將傳入的檔名以 FileInfo 來進行解析（只以字串無法做）
        System.Web.HttpContext.Current.Response.Clear(); //清除buffer
        System.Web.HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
        System.Web.HttpContext.Current.Response.Buffer = false;
        System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
        // 檔案類型還有下列幾種"application/pdf"、"application/vnd.ms-excel"、"text/xml"、"text/HTML"、"image/JPEG"、"image/GIF"
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode("退貨空白託運單標準格式-零擔.xls", System.Text.Encoding.UTF8));
        // 考慮 utf-8 檔名問題，以 out_file 設定另存的檔名
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Length", xpath_file.Length.ToString()); //表頭加入檔案大小
        System.Web.HttpContext.Current.Response.WriteFile(xpath_file.FullName);

        // 將檔案輸出
        System.Web.HttpContext.Current.Response.Flush();
        // 強制 Flush buffer 內容
        System.Web.HttpContext.Current.Response.End();


    }

    protected void btnPirntLabel_Click(object sender, EventArgs e)
    {
        string check_number = string.Empty;
        string print_requestid = string.Empty;
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                Boolean Update = false;
                //string print_date = ((Label)New_List.Items[i].FindControl("lbprint_date")).Text;
                //string Supplier_date = ((Label)New_List.Items[i].FindControl("lbSupplier_date")).Text;
                //string arrive_assign_date = ((HiddenField)New_List.Items[i].FindControl("Hid_arrive_assign_date")).Value;
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));

                if ((chkRow != null) && (chkRow.Checked) && (lbcheck_number.Text != ""))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    print_requestid += "'" + id + "',";
                    check_number += "'" + lbcheck_number.Text + "',";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@print_flag", "1");                           //是否列印
                        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                        cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                        dbAdapter.execNonQuery(cmd);
                    }
                }
            }
        }
        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);


        if (print_requestid == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的託運單');</script>", false);
            return;
        }
        else
        {
            Boolean IsChkOk = false;
            if (print_requestid.Length > 0) IsChkOk = true;
            if (IsChkOk)
            {
                NameValueCollection nvcParamters = new NameValueCollection();
                nvcParamters["ids"] = print_requestid.Replace("'", "");

                string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintReturnTagByIds";

                string strForm = PreparePOSTForm(url, nvcParamters);

                Response.Clear();

                Response.Write(strForm);
                Response.End();

                //       using (SqlCommand cmd = new SqlCommand())
                //       {
                //           string wherestr = " and A1.request_id in(" + print_requestid + ")";
                //           //cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                //           //cmd.Parameters.AddWithValue("@starData", date1.Text);
                //           //cmd.Parameters.AddWithValue("@endData", date2.Text);
                //           cmd.CommandText = string.Format(@"
                //                            select 
                //                           (select station_code from ttArriveSitesScattered where post_city = A1.Receive_city and post_area = A1.Receive_area) as area_arrive_code --A1.area_arrive_code	--著簡碼 
                //,convert(nvarchar(4),A1.print_date,120) Print_year
                //,SUBSTRING(convert(nvarchar(10),A1.print_date,120),6,2 ) Print_Mon
                //,SUBSTRING(convert(nvarchar(10),A1.print_date,120),9,2 ) Print_Day
                //                           --,print_date			--通知取件時間
                //                           ,RTRIM(A1.Order_number)	Orderbarcode	--退貨單號
                //                           , A1.Order_number
                //                           ,A1.Receive_contact    --收件人
                //                           ,A1.Receive_city + A1.Receive_area + A1.receive_address	as Reveiveaddress	--收件人地址 
                //                           ,isnull(A2.Receive_contact,A1.Send_contact) Send_contact		--寄件人
                //                           ,A1.Send_city + A1.Send_area+ A1.Send_address  as Sendaddress --寄件人地址
                //                           ,RTRIM(A1.check_number)  as barcode   --條碼
                //                           ,A1.Invoice_desc			--備註
                //                           ,A1.Check_number			--查貨單號
                //                           ,A1.Send_tel 			--寄件人電話 
                //                           ,A1.receive_tel1 		--收件人電話 
                //                           ,A1.pieces 			--件數
                //,(select top 1 ts.station_scode  from ttArriveSitesScattered ta left join tbStation ts on ta.station_code = ts.station_scode
                //where ta.post_city = A1.Send_city and ta.post_area =  A1.Send_area) send_arrive_code --發送站碼
                //                           ,(select top 1 td.driver_name from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number = A1.check_number order by tds.cdate desc) driver_name
                //,(select top 1 td.driver_mobile from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number =  A1.check_number order by tds.cdate desc) driver_mobile
                //                           from tcDeliveryRequests A1 with(nolock)
                //                           left join  tbStation ts on ts.station_scode = A1.area_arrive_code
                //                           Left join tcDeliveryRequests A2 with(nolock) on A1.send_contact = A2.check_number
                //                           where 1=1  {0}", wherestr);

                //           DataTable DT = dbAdapter.getDataTable(cmd);
                //           if (DT != null)
                //           {
                //               DataTable dt2 = new DataTable();
                //               dt2 = DT.Clone();  //複製DT的結構
                //               for (int i = 0; i <= DT.Rows.Count - 1; i++)
                //               {
                //                   DT.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["barcode"].ToString()));
                //                   if (DT.Rows[i]["Orderbarcode"].ToString() != "") {
                //                   DT.Rows[i]["Orderbarcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["Orderbarcode"].ToString()));
                //                   }
                //                   dt2.ImportRow(DT.Rows[i]);
                //               }
                //               string[] ParName = new string[0];
                //               string[] ParValue = new string[0];
                //               if (option.SelectedValue == "1")
                //               {
                //                   PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "Report2", ParName, ParValue, "ReturnsData", dt2);
                //               }
                //               else
                //               {
                //                   PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "SpeedReturnReport", ParName, ParValue, "ReturnsData", dt2);
                //               }
                //           }
                //       }
            }
        }
    }

    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {
        using (var barcode = new Barcode()
        {

            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 530,
            Height = 100,
            LabelFont = new Font("verdana", 20f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式
        })
        {
            return barcode.Encode(TYPE.CODE128, content);
        }
    }

    public bool IsValidCheckNumber(string check_number)
    {
        bool IsVaild = true;
        if (check_number.Length != 10)
        {
            IsVaild = false;
        }
        else
        {
            int num = Convert.ToInt32(check_number.Substring(0, 9));

            int chk = num % 7;
            int lastnum = Convert.ToInt32(check_number.Substring(9, 1));
            if (lastnum != chk)
            {
                IsVaild = false;
            }

        }
        return IsVaild;
    }

    private static String PreparePOSTForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                    send_station_code = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }


}