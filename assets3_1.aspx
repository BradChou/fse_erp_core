﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets3_1.aspx.cs" Inherits="assets3_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".btn-primary").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });


        });


        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">分析表-應收款及其他收入</h2>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">

                    <label>費用類別：</label>
                    <asp:DropDownList ID="ddl_fee" runat="server" class="form-control "></asp:DropDownList>
                    <label>查詢區間：</label>
                    <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                            <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="print" CssClass="btn btn-warning" runat="server" Text="列　印" OnClick="print_Click" />

                </div>

                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th">費用類別</th>
                        <th class="_th">交易日期</th>
                        <th class="_th">車牌</th>
                        <th class="_th">噸數</th>
                        <th class="_th">使用單位</th>
                        <th class="_th">車行</th>
                        <th class="_th">車主</th>
                        <th class="_th">司機</th>
                        <th class="_th">數量</th>
                        <th class="_th">里程數</th>
                        <th class="_th">公升數</th>
                        <th class="_th">牌價單價<br />
                            (未稅)</th>
                        <th class="_th">牌價金額<br />
                            (未稅)</th>
                        <th class="_th">購入單價<br />
                            (未稅)</th>
                        <th class="_th">購入金額<br />
                            (未稅)</th>
                        <th class="_th">收款單價<br />
                            (未稅)</th>
                        <th class="_th">收款金額<br />
                            (未稅)</th>
                        <th class="_th">備註</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class="paginate">
                               <%-- <td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @fee_type and code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1","fee_type",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.fee_type").ToString()))%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.date","{0:yyyy-MM-dd}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%></td>
                                <td><%#Func.GetRow("tonnes","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>
                                <td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1","code_id",Func.GetRow("dept","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())))%></td>
                                <td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass= 'CD'","code_id",Func.GetRow("car_retailer","ttAssets  ","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())))%></td>
                                <td><%#Func.GetRow("owner","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>
                                <td><%#Func.GetRow("driver","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>--%>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.feename").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.date","{0:yyyy-MM-dd}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.deptname").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_retailer_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.owner").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.milage").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.litre").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.amt_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.buy_price_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.buyamt_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receipt_price_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiptamt_notax","{0:N2}").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="19" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>

                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>


            </div>
        </div>
    </div>
</asp:Content>

