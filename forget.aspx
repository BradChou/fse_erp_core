﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="forget.aspx.cs" Inherits="forget" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>峻富雲端物流管理系統</title>
    <!-- InstanceEndEditable -->
    <link rel="alternate icon" type="image/png" href="img/favicon.png">
    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- InstanceBeginEditable name="head" -->
    <!-- InstanceEndEditable -->
    <script src="js/bootstrap.min.js"></script>




</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">忘記密碼</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group form-inline">
                            <div class="col-sm-10">
                                 <asp:Label id="Label1"  style="font-size:18px;" Text="帳號"  runat="server"/>
                                <asp:TextBox ID="account" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-sm-10">
                                   <asp:Label id="Label4"  style="font-size:18px;" Text="請輸入電話或信箱"  runat="server"/>
                                </div>
                            <div class="col-sm-10">
                                <asp:Label id="Label2"  style="font-size:18px;" Text="信箱"  runat="server"/>
                                <asp:TextBox ID="email" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                               
                            <div class="col-sm-10">
                                 <asp:Label id="Label3"  style="font-size:18px;" Text="電話"  runat="server"/>
                                <asp:TextBox ID="phone" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>


                        <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="提示" ValidationGroup="validate" OnClick="btnsend_Click2" />
                        <div class="form-group form-inline">

                            <asp:Label id="Label5"  style="font-size:18px;" Text=""  runat="server"/>
                            <br />
                            <asp:Label id="Label6"  style="font-size:18px;"  Text=""  runat="server"/>
                            <%--<asp:RadioButtonList ID="rdogender" runat="server" RepeatLayout="Flow"></asp:RadioButtonList>--%>


                            <%--<div class="col-sm-10">
                                <asp:RadioButton ID="email" Text="信箱" GroupName="colors" runat="server" />
                            </div>
                            <div class="col-sm-10">
                                <asp:RadioButton ID="phone" Text="電話" GroupName="colors" runat="server" />
                            </div>--%>
                        </div>




                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnsend" CssClass="btn btn-primary" runat="server" Text="送出" ValidationGroup="validate" OnClick="btnsend_Click" />
                <asp:Button ID="btncancel" CssClass="btn btn-default" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />

            </div>
        </div>

    </form>

</body>
</html>
