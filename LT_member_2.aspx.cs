﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class LT_member_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            MultiView1.ActiveViewIndex = 0;

            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            string wherestr = string.Empty;
            if (Less_than_truckload == "1") wherestr = " and code_id  = '02'";
            cmd2.CommandText = string.Format(" select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM' {0}  order by code_id asc", wherestr);
            PM_code.DataSource = dbAdapter.getDataTable(cmd2);
            PM_code.DataValueField = "code_id";
            PM_code.DataTextField = "showname";
            PM_code.DataBind();
            PM_code.Items.Insert(0, new ListItem("選擇計價模式", ""));

            SPM_code.DataSource = dbAdapter.getDataTable(cmd2);
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion

            #region 所有主客代號(要加權限控制)
            string manager_type = Session["manager_type"].ToString(); //管理單位類別   

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            string station_areaList = "";
            string station_scodeList = "";
            string managementList = "";

            string supplier_code = "";
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmda);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    managementList += "'";
                                    managementList += dt1.Rows[i]["station_scode"].ToString();
                                    managementList += "'";
                                    managementList += ",";
                                }
                                managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmda);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scodeList += "'";
                                    station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                    station_scodeList += "'";
                                    station_scodeList += ",";
                                }
                                station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt1 = dbAdapter.getDataTable(cmda);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_areaList += "'";
                                    station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                    station_areaList += "'";
                                    station_areaList += ",";
                                }
                                station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {
                            cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmda);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode += dt1.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                        else
                        {

                            cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dta = dbAdapter.getDataTable(cmda);
                            if (dta != null && dta.Rows.Count > 0)
                            {
                                supplier_code = dta.Rows[0]["station_code"].ToString();
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmda.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dta = dbAdapter.getDataTable(cmda);
                        if (dta != null && dta.Rows.Count > 0)
                        {
                            supplier_code = dta.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }

            using (SqlCommand cmd4 = new SqlCommand())
            {
                wherestr = "";
                if (supplier_code != "")
                {
                    cmd4.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                }
                else if (station_level.Equals("1"))
                {
                    wherestr = " and station_scode in (" + managementList + ")";
                }
                else if (station_level.Equals("2"))
                {
                    wherestr = " and station_scode in (" + station_scodeList + ")";
                }
                else if (station_level.Equals("4"))
                {
                    wherestr = " and station_scode in (" + station_areaList + ")";
                }
                cmd4.Parameters.AddWithValue("@type", Less_than_truckload);
                cmd4.CommandText = string.Format(@"select distinct master_code, master_code +'-'+ customer_name as name  from tbCustomers  
                                                    where second_id= '00' and type=@type  
                                                    {0} order by master_code ", wherestr);
                dlmaster_code.DataSource = dbAdapter.getDataTable(cmd4);
                dlmaster_code.DataValueField = "master_code";
                dlmaster_code.DataTextField = "name";
                dlmaster_code.DataBind();
                dlmaster_code.Items.Insert(0, new ListItem("請選擇", ""));
            }

            #endregion

            initForm(station_level, managementList, station_areaList, station_scodeList);
        }
    }

    private void initForm(string station_level, string managementList, string station_areaList, string station_scodeList)
    {
        is_new_customer.Checked = true;
        OldCustomerOverCBM.Checked = false;

        #region 郵政縣市
        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        City.DataSource = dbAdapter.getDataTable(cmd1);
        City.DataValueField = "city";
        City.DataTextField = "city";
        City.DataBind();
        City.Items.Insert(0, new ListItem("請選擇", ""));

        ddInvoiceCity.DataSource = dbAdapter.getDataTable(cmd1);
        ddInvoiceCity.DataValueField = "city";
        ddInvoiceCity.DataTextField = "city";
        ddInvoiceCity.DataBind();
        ddInvoiceCity.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

        #region 停運原因
        SqlCommand cmd3 = new SqlCommand();
        cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'TS' and active_flag = 1 order by seq asc ";
        stop_code.DataSource = dbAdapter.getDataTable(cmd3);
        stop_code.DataValueField = "code_id";
        stop_code.DataTextField = "code_name";
        stop_code.DataBind();
        #endregion

        #region SD/MD
        ddSD.Items.Add(new ListItem("請選擇SD", "0"));
        ddMD.Items.Add(new ListItem("請選擇MD", "0"));

        for (int i = 21; i <= 30; i++)
        {
            ddSD.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        for (int i = 1; i <= 19; i++)
        {
            ddMD.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        #endregion

        #region 商品類型
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, description from check_number_prehead where id !=3";
            product_type.DataSource = dbAdapter.getDataTable(cmd);
            product_type.DataValueField = "product_type";
            product_type.DataTextField = "description";
            product_type.DataBind();
            product_type.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion
        using (SqlCommand cmd4 = new SqlCommand())
        {
            cmd4.CommandText = "select bank_code, bank_code+' '+bank_name as showname from tbBanks order by bank_code ";
            bank.DataSource = dbAdapter.getDataTable(cmd4);
            bank.DataValueField = "showname";
            bank.DataTextField = "showname";
            bank.DataBind();
            bank.Items.Insert(0, new ListItem("請選擇", ""));
        }


        #region 母客代
        using (SqlCommand cmd = new SqlCommand())
        {
            if (station_level.Equals("1"))
            {
                cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%' and station_scode in (" + managementList + ")";
            }
            else if (station_level.Equals("2"))
            {
                cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%' and station_scode in (" + station_scodeList + ")";
            }
            else if (station_level.Equals("4"))
            {
                cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%' and station_scode in (" + station_areaList + ")";
            }
            else
            {
                cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%'";
            }
            ddlMasterCustomerCode.DataSource = dbAdapter.getDataTable(cmd);
            ddlMasterCustomerCode.DataValueField = "customer_code";
            ddlMasterCustomerCode.DataTextField = "showname";
            ddlMasterCustomerCode.DataBind();
            ddlMasterCustomerCode.Items.Insert(0, new ListItem("請選擇", ""));
        }
    }
    #endregion

    //#region 記績營業員

    //using (SqlCommand cmd4 = new SqlCommand())
    //{
    //    cmd4.CommandText = "select id, name as showname from tbSales where is_active='1' order by id ";
    //    sales_id.DataSource = dbAdapter.getDataTable(cmd4);
    //    sales_id.DataValueField = "id";
    //    sales_id.DataTextField = "showname";
    //    sales_id.DataBind();
    //    sales_id.Items.Insert(0, new ListItem("請選擇", ""));

    //}
    //#endregion
    ////計績站所
    //using (SqlCommand cmd5 = new SqlCommand())
    //{
    //    cmd5.CommandText = "select station_scode, station_code+' '+ station_name as showname from tbStation where active_flag='1' order by id  ";
    //    sale_station.DataSource = dbAdapter.getDataTable(cmd5);
    //    sale_station.DataValueField = "station_scode";
    //    sale_station.DataTextField = "showname";
    //    sale_station.DataBind();
    //    sale_station.Items.Insert(0, new ListItem("請選擇", ""));
    //}

private void clear()
{
    master_code.Text = "";         //主客代完整代碼      
    second_code.Text = "";
    PM_code.SelectedValue = "";
    //PM_code.Items.Clear() ;      //計價模式 
    customer_name.Text = "";       //客戶名稱
    shortname.Text = "";           //客戶簡稱
    uni_number.Text = "";          //統一編號
    principal.Text = "";           //對帳人
    tel.Text = "";                 //電話
    fax.Text = "";                 //傳真

    City.SelectedIndex = -1;       //出貨地址-縣市
    City_SelectedIndexChanged(null, null);
    CityArea.SelectedIndex = -1;     //出貨地址-鄉鎮市區 
    address.Text = "";              //出貨地址-路街巷弄號
    email.Text = "";                //出貨人電子郵件
    SPM_code.SelectedIndex = -1;     //計價模式
    supplier_code.Text = "";        //負責區配商編號
    supplier_name.Text = "";        //負責區配商名稱
    stop_code.SelectedIndex = -1;  //停運原因代碼  0正常、1同業競爭、2公司倒閉、3呆帳、4其他原因
    stop_memo.Text = "";            //停運原因備註
                                    //customer_hcode.Text = "";     //竹運客代
    contract_sdate.Text = "";       //合約生效日
    contract_edate.Text = "";       //合約到期日
    filename.Text = "";  //合約內容             
                         // http://" + Request.Url.Authority + "/files/contract/
                         //fee_sdate.Text = ""; //運價生效日
    close_date.SelectedValue = "";         //結帳日
    tbclose.Text = "";
    //feename.Text = "";                   //運價表
    business_people.Text = "";             //營業人員
    billing_special_needs.Text = "";       //帳單特殊需求
    ticket_period.SelectedValue = "";     //票期
    ticket.Text = "";
    //sales_id.SelectedValue = "";  //記績營業員
    //sale_station.SelectedValue = "";  //記績站所
}

protected void City_SelectedIndexChanged(object sender, EventArgs e)
{
    if (City.SelectedValue != "")
    {
        CityArea.Items.Clear();
        CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
        SqlCommand cmda = new SqlCommand();
        DataTable dta = new DataTable();
        cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
        cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
        dta = dbAdapter.getDataTable(cmda);
        if (dta.Rows.Count > 0)
        {
            for (int i = 0; i < dta.Rows.Count; i++)
            {
                CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
            }
        }
    }
    else
    {
        CityArea.Items.Clear();
        CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
    }
}

protected void PM_code_SelectedIndexChanged(object sender, EventArgs e)
{
    SqlCommand cmd = new SqlCommand();
    DataTable dt;
    string sScript = "";
    cmd.Parameters.AddWithValue("@master_code", dlmaster_code.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
    cmd.CommandText = " select * from  tbCustomers where master_code = @master_code ";  //and pricing_code=@pricing_code
    dt = dbAdapter.getDataTable(cmd);
    if (dt.Rows.Count > 0)
    {
        master_code.Text = dt.Rows[0]["master_code"].ToString();                //主客代完整代碼      
                                                                                //PM_code.SelectedValue = dt.Rows[0]["pricing_code"].ToString().Trim();    //計價模式 
        PM_code.SelectedValue = PM_code.SelectedValue.ToString();    //計價模式 
        customer_name.Text = dt.Rows[0]["customer_name"].ToString();  //客戶名稱
        shortname.Text = dt.Rows[0]["customer_shortname"].ToString();  //客戶簡稱
        uni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();  //統一編號
        principal.Text = dt.Rows[0]["shipments_principal"].ToString();  //對帳人
        tel.Text = dt.Rows[0]["telephone"].ToString();  //電話
        fax.Text = dt.Rows[0]["fax"].ToString();        //傳真
        customer_type.SelectedValue = dt.Rows[0]["customer_type"].ToString();
        try
        {
            City.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
        }
        catch
        { }

        City_SelectedIndexChanged(null, null);
        try
        {
            CityArea.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
        }
        catch
        { }

        address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();   //出貨地址-路街巷弄號
        email.Text = dt.Rows[0]["shipments_email"].ToString().Trim();    //出貨人電子郵件
                                                                         //SPM_code.SelectedValue = dt.Rows[0]["pricing_code"].ToString().Trim();    //計價模式
        SPM_code.SelectedValue = PM_code.SelectedValue.ToString();    //計價模式
        supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();     //負責區配商
        bank.SelectedValue = dt.Rows[0]["bank"].ToString();     //銀行
        bank_branch.Text = dt.Rows[0]["bank_branch"].ToString();     //分行
        bank_account.Text = dt.Rows[0]["bank_account"].ToString();     //帳號

        if (supplier_code.Text.Trim() != "")
        {
            SqlCommand cmds = new SqlCommand();
            DataTable dts;
            cmds.Parameters.AddWithValue("@supplier_code", supplier_code.Text);
            cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
            dts = dbAdapter.getDataTable(cmds);
            if (dts.Rows.Count > 0)
            {
                supplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
            }
        }

        stop_code.SelectedValue = dt.Rows[0]["stop_shipping_code"].ToString().Trim();     //停運原因代碼  0正常、1同業競爭、2公司倒閉、3呆帳、4其他原因
        stop_memo.Text = dt.Rows[0]["stop_shipping_memo"].ToString().Trim();    //停運原因備註
                                                                                //customer_hcode.Text = dt.Rows[0]["customer_hcode"].ToString().Trim();    //竹運客代


        //合約生效日
        if (dt.Rows[0]["contract_effect_date"] != DBNull.Value &&
            (((DateTime)dt.Rows[0]["contract_effect_date"]) > Convert.ToDateTime("1900/01/01")) &&
            (((DateTime)dt.Rows[0]["contract_effect_date"]) < Convert.ToDateTime("9999/12/31")))
        {
            contract_sdate.Text = ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd");
        }

        //合約到期日
        if (dt.Rows[0]["contract_expired_date"] != DBNull.Value &&
            (((DateTime)dt.Rows[0]["contract_expired_date"]) > Convert.ToDateTime("1900/01/01")) &&
            (((DateTime)dt.Rows[0]["contract_expired_date"]) < Convert.ToDateTime("9999/12/31")))
        {
            contract_edate.Text = ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd");
        }

        filename.Text = dt.Rows[0]["contract_content"].ToString().Trim();  //合約內容     
        sScript += "$('#hlFileView').attr('href','" + "http://" + Request.Url.Authority + "/files/contract/" + filename.Text + "');";


        ////運價生效日
        //if (dt.Rows[0]["tariffs_effect_date"] != DBNull.Value &&
        //    (((DateTime)dt.Rows[0]["tariffs_effect_date"]) > Convert.ToDateTime("1900/01/01")) &&
        //    (((DateTime)dt.Rows[0]["tariffs_effect_date"]) < Convert.ToDateTime("9999/12/31")))
        //{
        //    fee_sdate.Text = ((DateTime)dt.Rows[0]["tariffs_effect_date"]).ToString("yyyy/MM/dd");
        //}

        //結帳日
        if (dt.Rows[0]["checkout_date"] != DBNull.Value)
        {
            if (close_date.Items.FindByValue(dt.Rows[0]["checkout_date"].ToString()) == null)
            {
                close_date.SelectedValue = "-1";
                tbclose.Text = dt.Rows[0]["checkout_date"].ToString();
                sScript += "$('#" + tbclose.ClientID + "').show();";
            }
            else
            {
                close_date.SelectedValue = dt.Rows[0]["checkout_date"].ToString();
            }
        }
        else
        {
            close_date.SelectedValue = "";
            tbclose.Text = "";
            sScript += "$('#" + tbclose.ClientID + "').hide();";
        }

        //feename.Text = dt.Rows[0]["tariffs_table"].ToString().Trim();                      //運價表
        //sScript += "$('#hlFeeView').attr('href','" + "http://" + Request.Url.Authority + "/files/fee/" + feename.Text + "');";
        business_people.Text = dt.Rows[0]["business_people"].ToString().Trim();              //營業人員
        billing_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString().Trim();  //帳單特殊需求
                                                                                             //票期
        if (dt.Rows[0]["ticket_period"] != DBNull.Value)
        {
            if (ticket_period.Items.FindByValue(dt.Rows[0]["ticket_period"].ToString()) == null)
            {
                ticket_period.SelectedValue = "-1";
                ticket.Text = dt.Rows[0]["ticket_period"].ToString();
                sScript += "$('#" + ticket.ClientID + "').show();";
            }
            else
            {
                ticket_period.SelectedValue = dt.Rows[0]["ticket_period"].ToString();
            }
        }
        else
        {
            ticket_period.SelectedValue = "";
            ticket.Text = "";
            sScript += "$('#" + ticket.ClientID + "').hide();";
        }


    }
    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
    return;
}




protected void btnew_Click(object sender, EventArgs e)
{
    #region 序號
    string serialnum = "";
    SqlCommand cmd = new SqlCommand();
    DataTable dt;
    cmd.Parameters.AddWithValue("@master_code", dlmaster_code.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
    cmd.CommandText = " select max(second_id)  as serial from tbCustomers where master_code=@master_code and pricing_code=@pricing_code and customer_code <> '' ";
    dt = dbAdapter.getDataTable(cmd);
    if (dt.Rows.Count > 0)
    {
        if (dt.Rows[0]["serial"] != DBNull.Value)
        {
            serialnum = (Convert.ToInt32(dt.Rows[0]["serial"].ToString()) + 1).ToString();
        }
        else
        {
            serialnum = "0";
        }
    }
    else
    {
        serialnum = "0";
    }
    serialnum = serialnum.PadLeft(2, '0');
    second_code.Text = serialnum + PM_code.SelectedValue.ToString();
    btnsave.Enabled = (second_code.Text != "");
    #endregion
}

protected void dlmaster_code_SelectedIndexChanged(object sender, EventArgs e)
{
    clear();

    //SqlCommand cmd2 = new SqlCommand();
    //cmd2.CommandText = " select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM'" +
    //                   " and code_id in(select distinct pricing_code from  tbCustomers where master_code = '" + dlmaster_code.SelectedValue.ToString() + "') order by code_id asc";
    //PM_code.DataSource = dbAdapter.getDataTable(cmd2);
    //PM_code.DataValueField = "code_id";
    //PM_code.DataTextField = "showname";
    //PM_code.DataBind();
    //PM_code.Items.Insert(0, new ListItem("選擇計價模式", ""));
    //if (PM_code.Items.Count == 1)
    //{
    //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無主客代編號：" + dlmaster_code.SelectedValue.ToString() + "');</script>", false);
    //    return;
    //}
}

protected void btnsave_Click(object sender, EventArgs e)
{
    DateTime dt_temp = new DateTime();
    int i_tmp;
    string customer_id = "";
    #region 如果有空的(表示剛新增一筆新的廠商的主客代，但還沒鍵次客代)
    SqlCommand cmdt = new SqlCommand();
    DataTable dtt;
    cmdt.Parameters.AddWithValue("@master_code", master_code.Text);
    cmdt.Parameters.AddWithValue("@pricing_code", SPM_code.SelectedValue.ToString());
    cmdt.CommandText = " select customer_id from tbCustomers where master_code = @master_code and pricing_code = @pricing_code and customer_code = '' ";
    dtt = dbAdapter.getDataTable(cmdt);
    if (dtt.Rows.Count > 0)
    {
        customer_id = dtt.Rows[0]["customer_id"].ToString();
    }
    #endregion

    #region         
    SqlCommand cmd = new SqlCommand();
    SqlCommand cmdfse01 = new SqlCommand();
    if (customer_id == "")
    {
        cmd.Parameters.AddWithValue("@supplier_code", master_code.Text.Substring(0, 3));               //區配商代碼(前3碼)
        cmdfse01.Parameters.AddWithValue("@CLT_DEPT", master_code.Text.Substring(0, 3));
        cmd.Parameters.AddWithValue("@supplier_id", master_code.Text.Substring(3, 4));                  //主客代序號(後4碼)
        cmd.Parameters.AddWithValue("@master_code", master_code.Text);                                 //主客代完整代碼
        cmd.Parameters.AddWithValue("@pricing_code", SPM_code.SelectedValue.ToString());               //計價模式
    }
    cmd.Parameters.AddWithValue("@second_id", second_code.Text.Substring(0, 2));                        //次客代流水號
    cmd.Parameters.AddWithValue("@second_code", second_code.Text);                                     //次客代完整代碼
    string customer_code = master_code.Text.Trim() + second_code.Text.Trim();
    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
    cmdfse01.Parameters.AddWithValue("@CUST_CODE", customer_code);
    cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());        //客戶名稱
    cmdfse01.Parameters.AddWithValue("@CHI_NAME", customer_name.Text.ToString());
    cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());       //客戶簡稱
    cmdfse01.Parameters.AddWithValue("@BRF_NAME", shortname.Text.ToString());
    //customer_type (1:區配 2:竹運 3:自營)
    //if (customer_hcode.Text != "")
    //{
    //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別
    //}
    if (master_code.Text.Substring(3, 4) == "0000")
    {
        cmd.Parameters.AddWithValue("@customer_type", "1");
    }
    else
    {
        cmd.Parameters.AddWithValue("@customer_type", "3");
    }

    cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
    cmdfse01.Parameters.AddWithValue("@CUST_ID", uni_number.Text.ToString());
    cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
    cmdfse01.Parameters.AddWithValue("@CONTACT", principal.Text.ToString());
    cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
    cmdfse01.Parameters.AddWithValue("@TEL_NO1", tel.Text.ToString());
    cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
    cmdfse01.Parameters.AddWithValue("@FAX_NO", fax.Text.ToString());
    cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
    cmdfse01.Parameters.AddWithValue("@CHI_CITY", City.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
    cmdfse01.Parameters.AddWithValue("@CHI_AREA", CityArea.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
    cmdfse01.Parameters.AddWithValue("@CHI_ADDR", address.Text.ToString());
    cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
    cmdfse01.Parameters.AddWithValue("@EMAIL_LIST", email.Text.ToString());
    cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
    if (stop_code.SelectedValue.ToString() == "0")                                        //啟用or停用
    {
        cmdfse01.Parameters.AddWithValue("@ACT_FG", "Y");
    }
    else
    {
        cmdfse01.Parameters.AddWithValue("@ACT_FG", "N");
    }
    cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
                                                                                         //合約生效日
    if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
    {
        cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
        cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", contract_sdate.Text);
    }
    else
    {
        cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
        cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", DBNull.Value);
    }

    //合約到期日
    if (DateTime.TryParse(contract_edate.Text, out dt_temp))
    {
        cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
        cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", contract_edate.Text);
    }
    else
    {
        cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
        cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", DBNull.Value);
    }

    //票期
    if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
    {
        if (i_tmp == -1)
        {
            if (int.TryParse(ticket.Text, out i_tmp))
            {
                cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
        }

    }
    else
    {
        cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
    }
    cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
                                                                                         //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日

    //結帳日
    if (int.TryParse(close_date.SelectedValue, out i_tmp))
    {
        if (i_tmp == -1)
        {
            if (int.TryParse(tbclose.Text, out i_tmp))
            {
                cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
        }
    }
    else
    {
        cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
    }

    //cmd.Parameters.AddWithValue("@tariffs_table", feename.Text.ToString());            //運價表
    cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
    cmd.Parameters.AddWithValue("@customer_hcode", "");                                  //竹運客代
    cmd.Parameters.AddWithValue("@contact_1", "");                                       //竹運-聯絡人1  
    cmd.Parameters.AddWithValue("@email_1", "");                                         //竹運-聯絡人1 email
    cmd.Parameters.AddWithValue("@contact_2", "");                                       //竹運-聯絡人2
    cmd.Parameters.AddWithValue("@email_2", "");                                         //竹運-聯絡人2 email
    cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
    cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
    cmd.Parameters.AddWithValue("@type", Less_than_truckload);                          //棧板/零擔
    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
    cmdfse01.Parameters.AddWithValue("@CREATE_USER", Session["account_code"]);
    cmdfse01.Parameters.AddWithValue("@EDIT_USER", Session["account_code"]);
    cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間
    cmdfse01.Parameters.AddWithValue("@CREATE_TIME", DateTime.Now);
    cmdfse01.Parameters.AddWithValue("@EDIT_TIME", DateTime.Now);
    //cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
    //cmd.Parameters.AddWithValue("@update_date", DateTime.Now);

    cmd.Parameters.AddWithValue("@product_type", product_type.SelectedValue.ToString());          //商品類型
    if ((chkMasterCustomerCode.Checked == true && ddlMasterCustomerCode.SelectedValue != "") || (chkMasterCustomerCode.Checked == false && ddlMasterCustomerCode.SelectedValue == ""))  //兩者皆填or 皆不填
    {
        lbMasterCustomerCode.Visible = true;
        return;
    }
    else if (ddlMasterCustomerCode.SelectedValue != "")
    {
        cmd.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());  //母客代
    }
    else
    {
        cmd.Parameters.AddWithValue("@MasterCustomerCode", customer_code);
    }

    SqlCommand cmdM = new SqlCommand();
    DataTable dtM = new DataTable();
    cmdM.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());
    cmdM.CommandText = "select top 1  customer_name from tbCustomers where customer_code=@MasterCustomerCode";
    dtM = dbAdapter.getDataTable(cmdM);
    if (dtM.Rows.Count > 0)
    {
        cmd.Parameters.AddWithValue("@MasterCustomerName", dtM.Rows[0]["customer_name"].ToString());  //母客代名稱
    }
    else
    {
        cmd.Parameters.AddWithValue("@MasterCustomerName", customer_name.Text.ToString());
    }
    cmd.Parameters.AddWithValue("@is_weekend_delivered", is_weekend_delivered.Checked);  //是否啟用假日配送
    cmd.Parameters.AddWithValue("@weekend_delivery_fee", weekend_delivery_fee.Text);     //假日配送加價
    cmd.Parameters.AddWithValue("@bank", bank.SelectedValue.ToString());                 //銀行
    if (bank.SelectedValue != "")
    {
        cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString().Substring(0, 3));
        cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString().Substring(bank.SelectedValue.IndexOf(' ') + 1));
    }
    else
    {
        cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString());
        cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString());
    }
    cmd.Parameters.AddWithValue("@bank_branch", bank_branch.Text.ToString());            //分行
    cmdfse01.Parameters.AddWithValue("@BANK_BRANCH", bank_branch.Text.ToString());
    cmd.Parameters.AddWithValue("@bank_account", bank_account.Text.ToString());           //帳號
    if (product_type.SelectedValue.ToString() == "4" || product_type.SelectedValue.ToString() == "5")  //內部文件即商城出貨不自動取前三碼為計績站所
    {
    }
    else
    {
        cmd.Parameters.AddWithValue("@sale_station", master_code.Text.Substring(1, 2));
        cmd.Parameters.AddWithValue("@sale_station_cdate", DateTime.Now);
        cmd.Parameters.AddWithValue("@sale_station_udate", DateTime.Now);
    }
    cmd.Parameters.AddWithValue("@is_new_customer", is_new_customer.Checked);  //是否為新客戶
    cmd.Parameters.AddWithValue("@OldCustomerOverCBM", OldCustomerOverCBM.Checked);   //是否為舊制超材費  
    cmdfse01.Parameters.AddWithValue("@BANK_ACCT", bank_account.Text.ToString());

    //cmd.Parameters.AddWithValue("@sales_id", sales_id.SelectedValue.ToString());       //記績營業員
    //if (sales_id.SelectedValue.ToString() != "")
    //{
    //    cmd.Parameters.AddWithValue("@sales_id_cdate", DateTime.Now);  //首次填入記績營業員時間
    //}
    //if (sale_station.SelectedValue.ToString() != "")
    //{
    //    cmd.Parameters.AddWithValue("@sale_station_cdate", DateTime.Now);  //首次填入記績站所時間
    //}

    //cmd.Parameters.AddWithValue("@sales_id", sales_id.SelectedValue.ToString());       //記績營業員
    //if (sales_id.SelectedValue.ToString() != "")
    //{
    //    cmd.Parameters.AddWithValue("@sales_id_cdate", DateTime.Now);  //首次填入記績營業員時間
    //}
    //if (sale_station.SelectedValue.ToString() != "")
    //{
    //    cmd.Parameters.AddWithValue("@sale_station_cdate", DateTime.Now);  //首次填入記績站所時間
    //}

    cmd.Parameters.AddWithValue("@invoice_city", ddInvoiceCity.SelectedValue.ToString());
    cmdfse01.Parameters.AddWithValue("@INV_CITY", ddInvoiceCity.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@invoice_area", ddInvoiceArea.SelectedValue.ToString());
    cmdfse01.Parameters.AddWithValue("@INV_AREA", ddInvoiceArea.SelectedValue.ToString());
    cmd.Parameters.AddWithValue("@invoice_road", txtInvoiceRoad.Text.ToString());
    cmdfse01.Parameters.AddWithValue("@INV_ADDR", txtInvoiceRoad.Text.ToString());




    if (ddMD.SelectedIndex != 0)
        cmd.Parameters.AddWithValue("@assigned_md", ddMD.SelectedValue);

    if (ddSD.SelectedIndex != 0)
        cmd.Parameters.AddWithValue("@assigned_sd", ddSD.SelectedValue);


    if (customer_id == "")
    {
        cmd.CommandText = dbAdapter.SQLdosomething("tbCustomers", cmd, "insert");
        dbAdapter.execNonQuery(cmd);
        customer_id = dbAdapter.finalindex("tbCustomers", "customer_id");
        cmdfse01.CommandText = @"insert into DT_CUST(CUST_CODE, CUST_ID, CHI_NAME,BRF_NAME,CHI_CITY,CHI_AREA,CHI_ADDR,INV_CITY,INV_AREA,INV_ADDR,CONTACT,TEL_NO1,FAX_NO,EMAIL_LIST,BANK_ID,BANK_NAME,BANK_BRANCH,BANK_ACCT,CLT_DEPT,ACT_FG,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME,CONTRACT_EFFECT_DATE,CONTRACT_EXPIRED_DATE) 
values(@CUST_CODE, @CUST_ID, @CHI_NAME,@BRF_NAME,@CHI_CITY,@CHI_AREA,@CHI_ADDR,@INV_CITY,@INV_AREA,@INV_ADDR,@CONTACT,@TEL_NO1,@FAX_NO,@EMAIL_LIST,@BANK_ID,@BANK_NAME,@BANK_BRANCH,@BANK_ACCT,@CLT_DEPT,@ACT_FG,@CREATE_USER,@CREATE_TIME,@EDIT_USER,@EDIT_TIME,@CONTRACT_EFFECT_DATE,@CONTRACT_EXPIRED_DATE)";
        dbAdapter.execNonQueryForFSE01(cmdfse01);
    }
    else
    {
        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", customer_id);
        cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);                    //儲存至空白筆主客代
        dbAdapter.execNonQuery(cmd);
    }

    #endregion

    using (SqlCommand cmdShippingFee = new SqlCommand())
    {
        cmdShippingFee.Parameters.AddWithValue("@customer_code", customer_code);                                 //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
        cmdShippingFee.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱

        string stationCode = master_code.Text.Substring(0, 3);

        SqlCommand cmdStationName = new SqlCommand();
        cmdStationName.CommandText = "select top 1 station_name from tbStation where station_code = @station_code";
        cmdStationName.Parameters.AddWithValue("@station_code", stationCode);

        string stationName = dbAdapter.getScalarBySQL(cmdStationName) as string;
        cmdShippingFee.Parameters.AddWithValue("@station_name", stationName.Trim());

        cmdShippingFee.CommandText = @"insert into CustomerShippingFee(customer_code, customer_name, station_name, 
no1_bag, no2_bag, no3_bag, no4_bag, s60_cm, s90_cm, s110_cm, s120_cm, s150_cm, s180, holiday, piece_rate) 
values(@customer_code, @customer_name, @station_name, 40, 45, 50, 55, 60, 70, 90, 120, 150, 150, 20, 60)";

        dbAdapter.execNonQuery(cmdShippingFee);
    }

    //MultiView1.ActiveViewIndex = 1;
    SetDefaultView2(customer_code, customer_type.SelectedValue, customer_id, PM_code.SelectedValue.ToString());
    //string sScript = " $('#li_01').removeClass('current');" +
    //                 "    $('#li_02').addClass('current');";
    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_3-edit.aspx?customer_id=" + customer_id + "';alert('新增完成');</script>", false);
    //return;
    setFee.Visible = true;
    FinalCheck.Visible = true;
    btnsave.Visible = false;
    btncancel.Visible = false;
    dlmaster_code.Enabled = false;
    PM_code.Enabled = false;
    btnew.Enabled = false;
    lbMasterCustomerCode.Visible = false;
    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('客代編號" + customer_code + "新增完成！');</script>", false);
    return;


}

/// <summary>
/// 運價初始資料設定
/// </summary>
protected void SetDefaultView2(string customer_code, string customer_type, string customer_id, string pricing_code)
{
    //運價生效日
    DateTime dt_temp = new DateTime();
    tbmaster_code.Text = customer_code.Substring(0, 7);
    tbsecond_code.Text = customer_code.Substring(7, 4);
    lbcustomer_type.Text = customer_type;
    lb_cusid.Text = customer_id;
    lb_pricing_code.Text = pricing_code;
    lbl_PriceTip.Text = "";
    fee_sdate.Text = "※目前採用公版運價";
    if (customer_code.Length > 0)
    {
        using (SqlCommand cmd2 = new SqlCommand())
        {
            cmd2.CommandText = "usp_GetTodayPriceBusDefLog";
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.AddWithValue("@customer_code", customer_code);
            cmd2.Parameters.AddWithValue("@return_type", "1");
            using (DataTable the_dt = dbAdapter.getDataTable(cmd2))
            {
                if (the_dt.Rows.Count > 0)
                {
                    if (DateTime.TryParse(the_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                    {
                        fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                    }

                    int i_temp;
                    if (!int.TryParse(the_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                    lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                }
                //else
                //{
                //    // 抓父客代的運價
                //    cmd2.Parameters["@customer_code"].Value = master_code.Text.Trim() + "0" + SPM_code.SelectedValue.ToString();
                //    using (DataTable parent_dt = dbAdapter.getDataTable(cmd2))
                //    {
                //        if (parent_dt.Rows.Count > 0)
                //        {
                //            if (DateTime.TryParse(parent_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                //            {
                //                fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                //            }

                //            int i_temp;
                //            if (!int.TryParse(parent_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                //            lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                //        }
                //    }
                //}
            }
        }
    }

    //自營才開放價格設定
    if (Convert.ToInt32(customer_type) == 3)
    {
        pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(customer_code, customer_id.ToString())));
        pan_price_list.Visible = true;
    }
    else
    {
        pan_price_list.Visible = false;
    }
}

protected void btnQry_view2_Click(object sender, EventArgs e)
{
    SetDefaultView2(tbmaster_code.Text + tbsecond_code.Text, lbcustomer_type.Text, lb_cusid.Text, lb_pricing_code.Text);
}


protected void Button1_Click(object sender, EventArgs e)
{
    string sScript = "";
    using (SqlCommand cmd = new SqlCommand())
    {
        cmd.Parameters.AddWithValue("@individual_fee", individual_fee.Checked);                //是否個別計價  
        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", lb_cusid.Text);
        cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);   //修改
        try
        {
            dbAdapter.execNonQuery(cmd);
        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "新增主客戶" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

            sScript = "alert('儲存失敗：" + ex.Message.ToString() + "')";
        }
    }
    MultiView1.ActiveViewIndex = 2;

    using (SqlCommand cmd = new SqlCommand())
    {
        cmd.Parameters.AddWithValue("@customer_code", tbmaster_code.Text + tbsecond_code.Text);
        cmd.CommandText = " select top 1 tbCustomers.* , B.code_name as pricingname, C.code_name as stopname  from tbCustomers " +
                          " left join  tbItemCodes B on B.code_id = tbCustomers.pricing_code and B.code_sclass = 'PM'" +
                          " left join  tbItemCodes C on C.code_id = tbCustomers.stop_shipping_code and C.code_sclass = 'TS'" +
                          " where customer_code = @customer_code ";
        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
            if (dt.Rows[0]["supplier_code"].ToString().Trim() != "")
            {
                SqlCommand cmds = new SqlCommand();
                DataTable dts;
                cmds.Parameters.AddWithValue("@supplier_code", dt.Rows[0]["supplier_code"].ToString().Trim());
                cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
                dts = dbAdapter.getDataTable(cmds);
                if (dts.Rows.Count > 0)
                {
                    lsupplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
                }
            }
            lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();
            lbshortname.Text = dt.Rows[0]["customer_shortname"].ToString();
            lbuni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();
            lbprincipal.Text = dt.Rows[0]["shipments_principal"].ToString();
            lbtel.Text = dt.Rows[0]["telephone"].ToString();
            lbfax.Text = dt.Rows[0]["fax"].ToString();
            lbCity.Text = dt.Rows[0]["shipments_city"].ToString();
            lbCityArea.Text = dt.Rows[0]["shipments_area"].ToString();
            lbaddress.Text = dt.Rows[0]["shipments_road"].ToString();
            lbbusiness_people.Text = dt.Rows[0]["business_people"].ToString();
            lbticket_period.Text = dt.Rows[0]["ticket_period"].ToString();
            lbemial.Text = dt.Rows[0]["shipments_email"].ToString().Trim();
            lbSPM_code.Text = dt.Rows[0]["pricingname"].ToString().Trim();
            lbstop_code.Text = dt.Rows[0]["stopname"].ToString().Trim();
            lbcontract_sdate.Text = dt.Rows[0]["contract_effect_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd") : "";
            lbcontract_edate.Text = dt.Rows[0]["contract_expired_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd") : "";
            sScript += "$('#hlFileView2').attr('href','" + "http://" + Request.Url.Authority + "/files/contract/" + filename.Text + "');";
            lbclose_date.Text = dt.Rows[0]["checkout_date"].ToString();
            lbbilling_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString();
            lbFee.Text = Convert.ToBoolean(dt.Rows[0]["individual_fee"]) == true ? "獨立計價" : "採運價表計價";

        }
    }


    //sScript += " $('#li_01').removeClass('current');$('#li_02').removeClass('current');" +
    //                "    $('#li_03').addClass('current');";
    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);        
    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_2.aspx';alert('運價新增完成');</script>", false);
    return;
}

protected void btnOK_Click(object sender, EventArgs e)
{
    clear();
    MultiView1.ActiveViewIndex = 0;
}

protected void InvoiceCity_SelectedIndexChanged(object sender, EventArgs e)
{
    if (ddInvoiceCity.SelectedValue != "")
    {
        ddInvoiceArea.Items.Clear();
        ddInvoiceArea.Items.Add(new ListItem("請選擇", ""));
        SqlCommand cmda = new SqlCommand();
        DataTable dta = new DataTable();
        cmda.Parameters.AddWithValue("@city", ddInvoiceCity.SelectedValue.ToString());
        cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
        dta = dbAdapter.getDataTable(cmda);
        if (dta.Rows.Count > 0)
        {
            for (int i = 0; i < dta.Rows.Count; i++)
            {
                ddInvoiceArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
            }
        }
    }
    else
    {
        ddInvoiceArea.Items.Clear();
        ddInvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
    }
}
}