﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTrace.master" AutoEventWireup="true" CodeFile="LT_trace_1_3.aspx.cs" Inherits="LT_trace_1_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

     <link href="css/build.css" rel="stylesheet" />

    

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .tb_title, ._d_function {
            text-align: center;
        }

        .tb_detail {
            text-align: left;
        }

        .btn {
            margin: 3px;
        }

        ._h_function {
            width: 10%;
        }

        ._h_addr {
            width: 30%;
        }

        ._h_auto {
            min-width: 5px;
        }

        .div_sh, ._top_left {
            width: 75%;
            float: left;
        }

        .div_btn {
            width: 30%;
            float: right;
            right: 3%;
        }

        .tb_top {
            width: 100%;
        }

        ._top_right {           
            float: right;
            right: 5px;
        }

        ._hr {
            display: inline-table;
        }

        .detail_btn_area {
            text-align: center;
        }
        ._detail{
            width:68%;
            text-align:left;
            margin:10px;
           
        }
        ._detail_title{
            width:10%;   
        }
        ._detail_data{
            width:40%;    
            padding:3px !important;
        }
        ._ext{width:70px !important;}
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
         .table_list tr:hover {
            background-color: lightyellow;
        }
         textarea{
             margin:5px 0px;
         }
    </style>
    <script type="text/javascript">
        


        function Receiver_dw() {
            var rec_cuscode = $(".rec_cuscode").val();
            var rec_code = $(".rec_code").val();
            var rec_name = $(".rec_name").val();
            var rec_less = document.getElementById('<%=Less.ClientID %>').value;
         
         
            var the_dw = $("input[name=dw");
            var url_dw = "";
            url_dw = "GetReport.aspx?type=receiver&rec_code=" + rec_code + "&rec_name=" + rec_name + "&rec_cuscode=" + rec_cuscode + "&Lesstype=" + rec_less;
           


            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            setTimeout("parent.$.fancybox.close()", 20000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">狀況類別維護</h2>

            <asp:Panel ID="pan_list" runat="server">

                 <div style="display:none"><asp:TextBox ID="Less" runat="server" CssClass="rec_less"></asp:TextBox></div>
                            
                <div class="col-lg-12 form-inline form-group">
                    <table class="tb_top">
                        <tr>

                           

                            <td class="_top_right">
                                <%--<asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click"  />--%>
                                <asp:Button ID="btn_New" runat="server" class="btn btn-warning" Text="新 增" OnClick="btn_New_Click" />

                            </td>
                        </tr>
                    </table>
                    <hr />
                </div>
                <div class="wi-">

                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr class="tr-only-hide">
                        <th class="tb_title _h_function">功　　能</th>
                        <th class="tb_title _h_auto">層級</th>
                        <th class="tb_title _h_auto">類別</th>
                        <th class="tb_title _h_auto">歸屬</th>
                        <th class="tb_title _h_auto">更新時間</th>
        
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="_d_function" data-th="功能">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                    
                                    
                                    <asp:Button ID="btselect" CssClass="btn btn-primary _d_btn " CommandName="cmdSelect" runat="server" Text="修 改" />
                                    <asp:Button ID="btn_del" CssClass="btn btn-danger _d_btn" CommandName="cmddelete" OnClientClick="return confirm('確定要刪除此筆資料嗎?');" runat="server" Text="刪 除" />
                                </td>
                                <td class="tb_detail " data-th="層級"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.level").ToString())%></td>
                                <td class="tb_detail _d_receiver" data-th="類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.category").ToString())%></td>
                                <td class="tb_detail " data-th="歸屬"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.attribution").ToString())%></td>
                                <td class="tb_detail _d_tel" data-th="更新時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString())%></td>

                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="8" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
            </asp:Panel>



            <asp:Panel ID="pan_detail" runat="server"  Visible="false">

                <table class="_detail">
                    <tr class="form-group">
                        <th class="_detail_title">
                            <label class="_tip_important">層級</label></th>
                        <td class="_detail_data form-inline">
                             <asp:DropDownList ID="dllevel" runat="server" Cssclass="form-control dllevel" OnSelectedIndexChanged="level_SelectedIndexChanged" Autopostback="true" >
                                 <asp:ListItem Value="">請選擇</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                            </asp:DropDownList>
                            
                                            
                           <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="dllevel" ForeColor="Red" ValidationGroup="validate">請選擇層級</asp:RequiredFieldValidator>
                        
                        </td>
                        <tr class="form-group">
                        <th class="_detail_title">
                            <label class="_tip_important">類別名稱</label></th>
                        <td class="_detail_data form-inline">
                            <asp:TextBox ID="category" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="category" ForeColor="Red" ValidationGroup="validate">請輸入類別名稱</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="form-group">
                        <th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <asp:Label ID="lbattribution" runat="server" Text="歸屬" Visible="true" Width="150px"></asp:Label></th>
                        <td class="_detail_data form-inline">
                            <asp:DropDownList ID="dlattribution" runat="server" class="form-control chosen-select " CssClass="form-control  _attribution" Enabled="true"  Visible="true">
                            </asp:DropDownList>
                            

                        </td>
                        
                    <tr class="form-group">
                        <td colspan="4" class="detail_btn_area">
                            <asp:Button ID="btnAdd" CssClass="btn btn-primary" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnCancel" CssClass="btn btn-default " runat="server" Text="取 消" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>

            </asp:Panel>
        </div>
    </div>
</asp:Content>

