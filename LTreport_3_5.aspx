﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreport_3_5.aspx.cs" Inherits="LTreport_3_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .header {
            white-space: nowrap;
            text-align: center;
        }

    </style>
      <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });
            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
          });
         <%-- $("input[id*='rb']").click(function () {
                var id = $(this).attr('id')
                //第一天
                if (id.indexOf('rb1') > -1) {
                    var check = $("#<%=rb1.ClientID%>").prop("checked");
                    $("#<%=sMatchdt.ClientID%>").attr('disabled', check);
                    $("#<%=eMatchdt.ClientID%>").attr('disabled', check);
                    
                    $("#<%=sdate.ClientID%>").attr('disabled', !check);
                    $("#<%=edate.ClientID%>").attr('disabled', !check);
                }
                if (id.indexOf('rb2') > -1) {
                    var check = $("#<%=rb2.ClientID%>").prop("checked");
                    $("#<%=sMatchdt.ClientID%>").attr('disabled', !check);
                    $("#<%=eMatchdt.ClientID%>").attr('disabled', !check);
                    
                    $("#<%=sdate.ClientID%>").attr('disabled', check);
                    $("#<%=edate.ClientID%>").attr('disabled', check);
                }
             });--%>

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">代收、到付</h2>
            <hr />
            <div class="templatemo-login-form">
                <div class="row form-group">
                    <div class="form-group form-inline">
                        <label>發送站：</label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack ="true"  OnSelectedIndexChanged ="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                        <label>客代：</label>
                        <asp:DropDownList ID="second_code" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
                         <label>配送站：</label>
                        <asp:DropDownList ID="ddlarrivestation" runat="server" AutoPostBack ="true"  OnSelectedIndexChanged ="ddlarrivestation_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                        <label>作業人：</label>
                         <asp:DropDownList ID="ddlscanname" runat="server"  CssClass="form-control"></asp:DropDownList>
                        <%--<asp:TextBox ID="scanname" runat="server" class="form-control"></asp:TextBox>--%>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group form-inline">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton id="rb1" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged" Checked="true"/>
                          <asp:Label ID="Label3" runat="server" Text="發送日期"></asp:Label>
                        <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                        <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    </div>
                    <div class="form-group form-inline">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton id="rb2" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged"/>
                            <asp:Label ID="Label4" runat="server" Text="配達日期"></asp:Label>
                        <asp:TextBox ID="sMatchdt" runat="server" class="form-control" CssClass="date_picker" Enabled="false"></asp:TextBox>~
                        <asp:TextBox ID="eMatchdt" runat="server" class="form-control" CssClass="date_picker" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="row form-group">
                    <div class=" col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName"></label>
                        <span class="checkbox checkbox-success">
                           <asp:CheckBox ID="collection_money" runat="server" Checked="True" Text="代收金額" /><asp:CheckBox ID="arrive_to_pay_freight" runat="server" Text="到付運費" /><asp:CheckBox ID="arrive_to_pay_append" runat="server"  Text="到付追加" />
                        </span>
                    </div>
                      <div class="form-group form-inline">
<asp:RadioButton id="arrive_option1" Text="" GroupName="rbarrive_option" RepeatDirection="horizontal"  Checked="true" runat="server" />
                          <asp:Label ID="Label1" runat="server" Text="正常配交"></asp:Label>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:RadioButton id="arrive_option2" Text=""  RepeatDirection="horizontal" GroupName="rbarrive_option" runat="server"/>
                          <asp:Label ID="Label2" runat="server" Text="拒收"></asp:Label><br />
                     
                       <%--<asp:RadioButtonList ID="rbarrive_option" runat="server" RepeatDirection="Horizontal" >
                            <asp:ListItem Value="3" Selected="True"> </asp:ListItem>
                            <asp:ListItem Value="4">  </asp:ListItem>
                        </asp:RadioButtonList>--%>
                      </div>
                </div>
                 <div class="row form-group">
                 <div class="col-lg-6 col-md-6 form-group form-inline text-right ">
                        <asp:Button ID="btnQry" CssClass="btn btn-primary btn_view" runat="server" Text="查詢" OnClick="btnQry_Click" />
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click" />
                    </div>
                 </div>
            </div>
            <hr>
        <%--    <p class="text-primary">代收，到付查詢</p>
            <p class="text-primary">出表時間：<asp:Label ID="lbdateTime" runat="server"></asp:Label></p>--%>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th class="header">NO.</th>
                    <th class="header">作業時間</th>
                    <th class="header">作業</th>
                    <th class="header">到著站</th>
                    <th class="header">作業人</th>
                    <th class="header">出貨人</th>
                    <th class="header">出貨日期</th>
                    <th class="header">貨號</th>
                    <th class="header">傳票區分</th>
                    <th class="header">發送站所</th>
                    <th class="header">收貨人</th>
                    <th class="header">帳項異常說明</th>
                    <th class="header">代收金額</th>
                    <th class="header">振興卷</th>
                    <th class="header">現金</th>
                    <th class="header">件數</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                         <tr class ="paginate">
                            <td data-th="NO."><%# Container.ItemIndex +1 %></td><%--<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>--%>
                            <td data-th="作業時間" style="width: 90px"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                            <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                            <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrivestation").ToString())%></td>
                            <td data-th="作業人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="出貨日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_str").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="帳項異常說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="振興卷"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.VoucherMoney","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="現金"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.CashMoney","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="16" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
               <div id="page-nav" class="page"></div>
                       <%-- <nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>--%>
        </div>
    </div>
</asp:Content>
