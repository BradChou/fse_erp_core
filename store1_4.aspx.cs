﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class store1_4 : System.Web.UI.Page
{
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region 基本資料
            using (SqlCommand cmd = new SqlCommand())
            {
                
                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmd.CommandText = " select * from tbAccounts where account_code = @account_code  ";
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lbitem.Text = dt.Rows[0]["manager_unit"].ToString();         //管理單位
                        lbitem_employee.Text = dt.Rows[0]["emp_code"].ToString();    //人　　員
                        lbaccount_code.Text = dt.Rows[0]["account_code"].ToString(); //帳　　號
                        lbaccount_name.Text = dt.Rows[0]["user_name"].ToString();    //名　　稱
                    }
                    
                }
            }

            #endregion

            #region 場區
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                Area.DataSource = dt;
                Area.DataValueField = "seq";
                Area.DataTextField = "code_name";
                Area.DataBind();
                Area.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 常溫
            using (SqlCommand objCommandTemp = new SqlCommand())
            {
                
                objCommandTemp.CommandText = string.Format(@"Select code_id , code_name  from tbItemCodes 
                                                             WHERE code_bclass = '6' and code_sclass = 'temp' and active_flag = 1");
                using (DataTable dtRoad = dbAdapter.getDataTable(objCommandTemp))
                {
                    Temperature.DataSource = dtRoad;
                    Temperature.DataValueField = "code_id";
                    Temperature.DataTextField = "code_name";
                    Temperature.DataBind();
                    Temperature.Items.Insert(0, new ListItem("請選擇", ""));
                }
                //string TemperatureResult = "{ '常溫':'01','鮮一':'02','鮮食':'03','OC':'04','冷凍':'05','文流':'06'}";
                //IDictionary<string, string> TemperatureList = JsonConvert.DeserializeObject<IDictionary<string, string>>(TemperatureResult);
                //Temperature.DataSource = TemperatureList;
                //Temperature.DataTextField = "key";
                //Temperature.DataValueField = "value";
                //Temperature.DataBind();
                //Temperature.Items.Insert(0, new ListItem("請選擇", ""));
            }

            #endregion

            readdata();
        }
    }

    private void readdata()
    {

        SqlCommand objCommand = new SqlCommand();
       
        string wherestr = "";
        var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
        if (!string.IsNullOrEmpty(Str))
        {
            wherestr += " and (";
            var StrAry = Str.Split(',');
            var i = 0;
            foreach (var item2 in StrAry)
            {
                if (!string.IsNullOrEmpty(item2))
                {
                    if (i > 0)
                    {
                        wherestr += " or ";
                    }
                    objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                    wherestr += "  A.FieldArea=@code_id" + item2;
                    i += 1;
                }
            }
            wherestr += " )";
        }       

        objCommand.CommandText = string.Format(@"Select C.code_name as TempName, CASE daynight WHEN 1 THEN '日' WHEN 2 THEN '夜' ELSE '' END AS dn, 
                                                 A.seq, A.FieldArea, A.code_name, A.active_flag, isnull(A.DayNight,0) as DayNight, isnull(A.Temperature,0) as Temperature, B.code_name as FieldArea_name  ,
                                                 D.user_name , A.udate
                                                 from ttItemCodesRoad A with(nolock) 
                                                 Left join ttItemCodesFieldArea B with(nolock) on A.FieldArea = B.seq
                                                 Left join tbItemCodes AS C with(nolock) on A.Temperature = C.code_id 
                                                 and C.code_sclass = 'temp' and C.active_flag = 1 and C.code_bclass = '6'
                                                 left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                 Where A.active_flag = 1 {0} order by A.FieldArea, A.code_name", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        var CodeNameStr = Func.GetRow("code_name", "ttItemCodesRoad", "", "code_name=@code_name and FieldArea=@Area", "code_name|Area", code_name.Text.ToString().Trim() + "|" + Area.SelectedValue );
        var code_seqStr = Func.GetRow("seq", "ttItemCodesRoad", "", "code_name=@code_name and FieldArea=@Area", "code_name|Area", code_name.Text.ToString().Trim() + "|" + Area.SelectedValue);
        var code_idStr = code_id.Value.ToString().Trim();
        if (string.IsNullOrEmpty(code_idStr))
            code_idStr = "";
        string JStr = "";
        //if (!string.IsNullOrEmpty(CodeNameStr)&&string.IsNullOrEmpty(code_idStr))
        if (!string.IsNullOrEmpty(CodeNameStr) && code_seqStr != code_idStr)
        {
            if (!string.IsNullOrEmpty(code_idStr))
            {
                btnsave.Text = "修改";
                btnsave.CssClass = "btn btn-info";
            }
            JStr = "alert('路線名稱已存在!');";            
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
            return;
        }
        else
        {
            try
            {
                if (string.IsNullOrEmpty(code_idStr))
                {
                    using (SqlCommand cmdInsert = new SqlCommand())
                    {
                        cmdInsert.Parameters.AddWithValue("@FieldArea", Area.SelectedValue);                         //場區
                        cmdInsert.Parameters.AddWithValue("@code_name", code_name.Text.ToString().Trim());           //路線
                        cmdInsert.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue);                      //日夜
                        cmdInsert.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue);                //溫層
                        cmdInsert.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                        cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                        cmdInsert.CommandText = dbAdapter.SQLdosomething("ttItemCodesRoad", cmdInsert, "insert");
                        dbAdapter.execNonQuery(cmdInsert);
                        JStr = "alert('新增成功！');";
                    }
                }
                else
                {
                    using (SqlCommand cmdupd = new SqlCommand())
                    {
                        //cmdupd.Parameters.AddWithValue("@FieldArea", Area.SelectedValue );
                        //cmdupd.Parameters.AddWithValue("@code_name", code_name.Text.ToString().Trim());
                        cmdupd.Parameters.AddWithValue("@FieldArea", Area.SelectedValue);                         //場區
                        cmdupd.Parameters.AddWithValue("@code_name", code_name.Text.ToString().Trim());           //路線
                        cmdupd.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue);                      //日夜
                        cmdupd.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue);                //溫層
                        cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                        cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", code_idStr);
                        cmdupd.CommandText = dbAdapter.genUpdateComm("ttItemCodesRoad", cmdupd);
                        dbAdapter.execNonQuery(cmdupd);
                        JStr = "alert('修改成功！');";
                    }
                }
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "location.href='store1_4.aspx';</script>", false);
            }
            catch (Exception ex)
            {
                JStr = "alert('執行異常!" + ex.ToString() + "');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                return;
            }
    }


}




}
