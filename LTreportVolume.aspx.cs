﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTreportVolume : System.Web.UI.Page
{

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }
    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;
            date2.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                        else
                        {

                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}
                                               order by station_code", wherestr);

            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "station_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (manager_type == "0" || manager_type == "1")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && manager_type == "2")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);
            #endregion
            


            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["date2"] != null)
            {
                if (Request.QueryString["date2"] != "")
                {
                    date2.Text = Request.QueryString["date2"];
                }
            }

            if (Request.QueryString["Suppliers"] != null)
            {

                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }
            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }
            readdata();
        }
    }
    private void readdata()
    {
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@sdt", date1.Text);
            cmd.Parameters.AddWithValue("@edt", date2.Text);

            querystring += "&date1=" + date1.Text;
            querystring += "&date2=" + date2.Text;

            cmd.Parameters.AddWithValue("@station_code", Suppliers.SelectedValue.ToString());
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            querystring += "&check_number=" + check_number.Text;


            cmd.CommandText = string.Format(@"
select A.check_number, 
B.cbmLength,
B.cbmWidth,
B.cbmHeight,
B.cbmCont,
B.cbmWeight,
isnull(A.CbmSize,0) * 10 'Weight',
B.cbmWeight - (isnull(A.CbmSize,0) * 10 ) diff,
B.pic,
'' editflag
from tcDeliveryRequests A With(Nolock) 
inner join ttAutomachine B With(Nolock) on A.check_number = B.check_number
where A.print_date>= @sdt and A.print_date <= @edt
and (@check_number ='' or A.check_number = @check_number )
And (@station_code = '' or supplier_code = @station_code )");
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                ltotalpages.Text = dt.Rows.Count.ToString();
                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }


    protected void btnExcel_Click(object sender, EventArgs e) {
        string sheet_title = "材積丈量報表";
        string strWhereCmd1 = "";
        string strWhereCmd2 = "";
        string file_name = "材積丈量報表" + DateTime.Now.ToString("yyyyMMdd");
        if (date1.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入匯出日期!!');</script>", false);
            return;
        }


        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@sdt", date1.Text);
            cmd.Parameters.AddWithValue("@edt", date2.Text);
            cmd.Parameters.AddWithValue("@station_code", Suppliers.SelectedValue);
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            cmd.CommandText = string.Format(@"select 
ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS '序號' ,
'('+s.station_scode+')'+s.station_name as '發送站',
C.customer_shortname as '客戶名稱',
A.supplier_code as '客代編號',
A.check_number as '貨號', 
B.cbmLength as '長',
B.cbmWidth as '寬',
B.cbmHeight as '高',
B.cbmCont  as '才積數',
B.cbmWeight '重量換算',
isnull(A.CbmSize,0) * 10 '系統輸入',
B.cbmWeight - (isnull(A.CbmSize,0) * 10 ) '差異(重量換算-系統重量)',
case when (isnull(A.CbmSize,0) * 10 )< B.cbmWeight then 'Y' else 'N' end '異常（輸入ｗ＜重量換算）（Ｙ／Ｎ）',
'' '是否修正（Ｙ／Ｎ）','' '袋號ＱＲ'
from tcDeliveryRequests A With(Nolock) 
inner join ttAutomachine B With(Nolock) on A.check_number = B.check_number
inner join tbStation s  With(Nolock)  on A.send_station_scode = s.station_scode
inner join tbCustomers C With(Nolock) on A.customer_code = C.customer_code
where A.print_date>= @sdt and A.print_date <= @edt
and (@check_number ='' or A.check_number = @check_number )
And (@station_code = '' or A.supplier_code = @station_code )
");
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                    }
                }
            }
        }


    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string pic = ((HiddenField)e.Item.FindControl("hid_pic")).Value.ToString().Trim();
        string hid_check_number = ((HiddenField)e.Item.FindControl("hid_check_number")).Value.ToString().Trim();
        string script = string.Empty;
        switch (e.CommandName)
        {
            case "cmdEdit":
                //FillData(check_number);
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_01').modal();" + script + "</script>", false);
                break;

            case "cmdView":
                //LinkButton Imglink = new LinkButton();
                //string iconpath = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + pic;

                mcheck_number.Text = hid_check_number;
                using (SqlCommand cmd = new SqlCommand()) {
                    cmd.Parameters.AddWithValue("@check_number", hid_check_number);
                    cmd.CommandText = @"
select A.check_number, 
A.supplier_code, 
E.station_name  as receive_station_name,
C.station_name as  send_station_name,
B.cbmLength,
B.cbmWidth,
B.cbmHeight,
B.cbmCont,
B.cbmWeight,
isnull(A.CbmSize,0) * 10 'Weight',
B.cbmWeight - (isnull(A.CbmSize,0) * 10 ) diff,
B.pic,
'' editflag
from tcDeliveryRequests A With(Nolock) 
inner join ttAutomachine B With(Nolock) on A.check_number = B.check_number
inner join tbStation C With(Nolock) on A.supplier_code = C.station_code
left join [ttArriveSitesScattered] D on A.receive_city = D.post_city and  A.receive_area = D.post_area
left join tbStation E on E.station_scode = D.station_code
Where A.check_number = @check_number 
";
                    cmd.CommandTimeout = 600;

                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        mcbmLength.Text = dt.Rows[0]["cbmLength"].ToString();
                        mcbmWidth.Text = dt.Rows[0]["cbmWidth"].ToString();
                        mcbmHeight.Text = dt.Rows[0]["cbmHeight"].ToString();
                        mcbmCont.Text = dt.Rows[0]["cbmCont"].ToString();
                        mSuppliers.Text = dt.Rows[0]["receive_station_name"].ToString();
                    }
                }




                Image1.ImageUrl = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + pic;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_02').modal();</script>", false);

                break;
        }
    }
    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }
        if (date2.Text.ToString() != "")
        {
            querystring += "&date2=" + date2.Text.ToString();
        }
        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&check_number=" + check_number.Text;

        Response.Redirect(ResolveUrl("~/LTreportVolume.aspx?search=yes" + querystring));
    }
}