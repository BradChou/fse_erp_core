﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class loan4_1_edit : System.Web.UI.Page
{
   
    public string id
    {
        get { return ViewState["id"].ToString(); }
        set { ViewState["id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    public bool del_payment
    {
        get { return ViewState["del_payment"] != null ? (bool)ViewState["del_payment"] : false ; }
        set { ViewState["del_payment"] = value; }
    }

    public bool del_payment_s
    {
        get { return ViewState["del_payment_s"] != null ? (bool)ViewState["del_payment_s"] : false; }
        set { ViewState["del_payment_s"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "";

            readdata(id);

        }
    }

    private void readdata(string id)
    {  
        if (id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@id", id);
            cmd.CommandText = @"Select A.id, A.car_license, A.car_price, A.first_payment, A.loan_price, A.annual_rate, A.loan_period, A.annual_pay_count, A.loan_startdate,
                                A.installment_price, A.installment_count,  A.interest, A.loan_company, A.memo, A.car_price_s, A.first_payment_s, A.loan_price_s, 
                                A.loan_startdate_s, A.annual_rate_s, A.loan_period_s, A.annual_pay_count_s, A.installment_price_s, A.installment_count_s, 
                                A.interest_s, A.deposit_s , info.paid_count , Sinfo.paid_count  'paid_count_s'
                                from  ttLoan A with(nolock) 
                                CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 0) info
                                CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 1) Sinfo
                                Where A.id = @id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                car_license.Text = dt.Rows[0]["car_license"].ToString().Trim();
                car_price.Text = dt.Rows[0]["car_price"].ToString().Trim();
                first_payment.Text = dt.Rows[0]["first_payment"].ToString().Trim();
                loan_price.Text = dt.Rows[0]["loan_price"].ToString().Trim();
                annual_rate.Text = dt.Rows[0]["annual_rate"].ToString();
                loan_startdate.Text = dt.Rows[0]["loan_startdate"] != DBNull.Value ?   Convert.ToDateTime( dt.Rows[0]["loan_startdate"]).ToString("yyyy-MM-dd") :"";
                loan_period.Text = dt.Rows[0]["loan_period"].ToString();
                annual_pay_count.Text = dt.Rows[0]["annual_pay_count"].ToString();
                installment_price.Text = dt.Rows[0]["installment_price"].ToString();
                installment_count.Text = dt.Rows[0]["installment_count"].ToString();
                paid_count.Text = dt.Rows[0]["paid_count"].ToString();
                interest.Text = dt.Rows[0]["interest"].ToString();
                loan_company.Text = dt.Rows[0]["loan_company"].ToString();
                memo.Text = dt.Rows[0]["memo"].ToString();
                if (Convert.ToInt32 (dt.Rows[0]["paid_count"])>0)
                {
                    cal.Visible = false;  // 已付款過則不開放重新試算
                }

                car_price_s.Text = dt.Rows[0]["car_price_s"].ToString().Trim();
                first_payment_s.Text = dt.Rows[0]["first_payment_s"].ToString().Trim();
                loan_price_s.Text = dt.Rows[0]["loan_price_s"].ToString().Trim();
                annual_rate_s.Text = dt.Rows[0]["annual_rate_s"].ToString();
                loan_startdate_s.Text = dt.Rows[0]["loan_startdate_s"]  != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["loan_startdate_s"]).ToString("yyyy-MM-dd") :"";
                loan_period_s.Text = dt.Rows[0]["loan_period_s"].ToString();
                annual_pay_count_s.Text = dt.Rows[0]["annual_pay_count_s"].ToString();
                installment_price_s.Text = dt.Rows[0]["installment_price_s"].ToString();
                installment_count_s.Text = dt.Rows[0]["installment_count_s"].ToString();
                paid_count_s.Text = dt.Rows[0]["paid_count_s"].ToString();
                interest_s.Text = dt.Rows[0]["interest_s"].ToString();
                deposit_s.Text = dt.Rows[0]["deposit_s"].ToString();
                if (Convert.ToInt32(dt.Rows[0]["paid_count_s"]) > 0)
                {
                    calS.Visible = false;  // 已付款過則不開放重新試算
                }

                SqlCommand cmdpay = new SqlCommand();
                {
                    cmdpay.Parameters.AddWithValue("@loan_id", id);
                    cmdpay.Parameters.AddWithValue("@type", 0);
                    cmdpay.CommandText = @"Select * from  ttRepayment  with(nolock) 
                                         Where loan_id = @loan_id and type=@type ";
                    DataTable dtpay = dbAdapter.getDataTable(cmdpay);
                    New_List.DataSource = dtpay;
                    New_List.DataBind();
                }

                SqlCommand cmdpay_s = new SqlCommand();
                {
                    cmdpay_s.Parameters.AddWithValue("@loan_id", id);
                    cmdpay_s.Parameters.AddWithValue("@type", 1);
                    cmdpay_s.CommandText = @"Select * from  ttRepayment  with(nolock) 
                                         Where loan_id = @loan_id and type=@type ";
                    DataTable dtpay = dbAdapter.getDataTable(cmdpay_s);
                    New_List_S.DataSource = dtpay;
                    New_List_S.DataBind();
                }
            }
        }
        else
        {
            ApStatus = "Add";
        }

       
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                car_license.ReadOnly  = true ;
                statustext.Text = "修改";                
                break;
            case "Add":                
                statustext.Text = "新增";
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        int loan_id = 0;
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":
                SqlCommand cmdadd = new SqlCommand();
                cmdadd.Parameters.AddWithValue("@car_license", car_license.Text.Trim());
                cmdadd.Parameters.AddWithValue("@car_price", car_price.Text.Trim());
                cmdadd.Parameters.AddWithValue("@first_payment", first_payment.Text.Trim());
                cmdadd.Parameters.AddWithValue("@loan_price", loan_price.Text.Trim());
                cmdadd.Parameters.AddWithValue("@annual_rate", annual_rate.Text.Trim());
                cmdadd.Parameters.AddWithValue("@loan_period", loan_period.Text.Trim());
                cmdadd.Parameters.AddWithValue("@annual_pay_count", annual_pay_count.Text );
                cmdadd.Parameters.AddWithValue("@loan_startdate", loan_startdate.Text );
                cmdadd.Parameters.AddWithValue("@installment_price", installment_price.Text);
                cmdadd.Parameters.AddWithValue("@installment_count", installment_count.Text);
                cmdadd.Parameters.AddWithValue("@paid_count", 0);
                cmdadd.Parameters.AddWithValue("@interest", interest.Text);
                cmdadd.Parameters.AddWithValue("@loan_company", loan_company.Text);
                cmdadd.Parameters.AddWithValue("@memo", memo.Text);

                //區配&外車
                cmdadd.Parameters.AddWithValue("@car_price_s", car_price_s.Text.Trim());
                cmdadd.Parameters.AddWithValue("@first_payment_s", first_payment_s.Text.Trim());
                cmdadd.Parameters.AddWithValue("@loan_price_s", loan_price_s.Text.Trim());
                cmdadd.Parameters.AddWithValue("@annual_rate_s", annual_rate_s.Text.Trim());
                cmdadd.Parameters.AddWithValue("@loan_period_s", loan_period_s.Text.Trim());
                cmdadd.Parameters.AddWithValue("@annual_pay_count_s", annual_pay_count_s.Text);
                cmdadd.Parameters.AddWithValue("@loan_startdate_s", loan_startdate_s.Text);
                cmdadd.Parameters.AddWithValue("@installment_price_s", installment_price_s.Text);
                cmdadd.Parameters.AddWithValue("@installment_count_s", installment_count_s.Text);
                cmdadd.Parameters.AddWithValue("@paid_count_s", 0);
                cmdadd.Parameters.AddWithValue("@interest_s", interest_s.Text);
                cmdadd.Parameters.AddWithValue("@deposit_s", deposit_s.Text);

                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                //cmdadd.CommandText = dbAdapter.SQLdosomething("ttLoan", cmdadd, "insert");
                cmdadd.CommandText = dbAdapter.genInsertComm("ttLoan", true, cmdadd);        //新增

                if (int.TryParse(dbAdapter.getScalarBySQL(cmdadd).ToString(), out loan_id))
                {
                    id = loan_id.ToString();                    
                }

                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@car_license", car_license.Text.Trim());
                cmdupd.Parameters.AddWithValue("@car_price", car_price.Text.Trim());
                cmdupd.Parameters.AddWithValue("@first_payment", first_payment.Text.Trim());
                cmdupd.Parameters.AddWithValue("@loan_price", loan_price.Text.Trim());
                cmdupd.Parameters.AddWithValue("@annual_rate", annual_rate.Text.Trim());
                cmdupd.Parameters.AddWithValue("@loan_period", loan_period.Text.Trim());
                cmdupd.Parameters.AddWithValue("@annual_pay_count", annual_pay_count.Text);
                cmdupd.Parameters.AddWithValue("@loan_startdate", loan_startdate.Text);
                cmdupd.Parameters.AddWithValue("@installment_price", installment_price.Text);
                cmdupd.Parameters.AddWithValue("@installment_count", installment_count.Text);
                cmdupd.Parameters.AddWithValue("@interest", interest.Text);
                cmdupd.Parameters.AddWithValue("@loan_company", loan_company.Text);
                cmdupd.Parameters.AddWithValue("@memo", memo.Text);

                cmdupd.Parameters.AddWithValue("@car_price_s", car_price_s.Text.Trim());
                cmdupd.Parameters.AddWithValue("@first_payment_s", first_payment_s.Text.Trim());
                cmdupd.Parameters.AddWithValue("@loan_price_s", loan_price_s.Text.Trim());
                cmdupd.Parameters.AddWithValue("@annual_rate_s", annual_rate_s.Text.Trim());
                cmdupd.Parameters.AddWithValue("@loan_period_s", loan_period_s.Text.Trim());
                cmdupd.Parameters.AddWithValue("@annual_pay_count_s", annual_pay_count_s.Text);
                cmdupd.Parameters.AddWithValue("@loan_startdate_s", loan_startdate_s.Text);
                cmdupd.Parameters.AddWithValue("@installment_price_s", installment_price_s.Text);
                cmdupd.Parameters.AddWithValue("@installment_count_s", installment_count_s.Text); 
                cmdupd.Parameters.AddWithValue("@interest_s", interest_s.Text);
                cmdupd.Parameters.AddWithValue("@deposit_s", deposit_s.Text);

                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttLoan", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        DateTime due_date;
        float _opening_balance = 0;     //期初餘額
        float _end_balance = 0;         //期末餘額
        float _total_interest = 0;      //累計利息
        float _installment_price = 0;   //分期付款額
        float _principal = 0;           //本金
        float _interest = 0;            //利息 
        string strCols = "loan_id , type,  due_date, opening_balance, installment_price, principal , interest , end_balance, total_interest, paid, paid_price, memo";
        string strValues = "";
        string strInsSQL = string.Empty;
        string strTable = "ttRepayment";
        string strDelSQL = "";

        if (del_payment)
        {
            strDelSQL = string.Format("Delete {0} where loan_id={1} and type= 0;"
                               , strTable
                               , id);
        }
        
        #region 公司償還計畫表
        if (New_List.Items.Count > 0)
        {   
            SqlCommand cmd = new SqlCommand();

            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                HiddenField hid_id = ((HiddenField)New_List.Items[i].FindControl("hid_id"));
                Label lbdate = ((Label)New_List.Items[i].FindControl("lbdate"));
                Label lbopening_balance = ((Label)New_List.Items[i].FindControl("lbopening_balance"));
                Label lbinstallment_price = ((Label)New_List.Items[i].FindControl("lbinstallment_price"));
                Label lbprincipal = ((Label)New_List.Items[i].FindControl("lbprincipal"));
                Label lbinterest = ((Label)New_List.Items[i].FindControl("lbinterest"));
                Label lbend_balance = ((Label)New_List.Items[i].FindControl("lbend_balance"));
                Label lbtotal_interest = ((Label)New_List.Items[i].FindControl("lbtotal_interest"));

                if (hid_id.Value == "0")
                {
                    cmd.Parameters.AddWithValue("@loan_id" + i.ToString(), id);
                    cmd.Parameters.AddWithValue("@type" + i.ToString(), 0);

                    if (DateTime.TryParse(lbdate.Text, out due_date))
                    {
                        cmd.Parameters.AddWithValue("@due_date" + i.ToString(), due_date);
                    }

                    if (float.TryParse(lbopening_balance.Text, out _opening_balance))
                    {
                        cmd.Parameters.AddWithValue("@opening_balance" + i.ToString(), _opening_balance);
                    }

                    if (float.TryParse(lbinstallment_price.Text, out _installment_price))
                    {
                        cmd.Parameters.AddWithValue("@installment_price" + i.ToString(), _installment_price);
                    }

                    if (float.TryParse(lbprincipal.Text, out _principal))
                    {
                        cmd.Parameters.AddWithValue("@principal" + i.ToString(), _principal);
                    }

                    if (float.TryParse(lbinterest.Text, out _interest))
                    {
                        cmd.Parameters.AddWithValue("@interest" + i.ToString(), _interest);
                    }

                    if (float.TryParse(lbend_balance.Text, out _end_balance))
                    {
                        cmd.Parameters.AddWithValue("@end_balance" + i.ToString(), _end_balance);
                    }

                    if (float.TryParse(lbtotal_interest.Text, out _total_interest))
                    {
                        cmd.Parameters.AddWithValue("@total_interest" + i.ToString(), _total_interest);
                    }

                    strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}, 0, 0, ''", i.ToString());
                    strInsSQL += string.Format("INSERT INTO {0} ({1}) VALUES ({2});SELECT @@Identity;"
                               , strTable
                               , strCols
                               , strValues);
                }
            }
            cmd.CommandText = strDelSQL+ strInsSQL;
            if (strInsSQL != "") dbAdapter.execQuery(cmd, true);

        }
        else
        {

            int count = 0;
            float loanprice = 0;
            float annualrate = 0;       //年利率
            int Num_Pmt_Per_Year = 0;   //每年付款次數

            int.TryParse(installment_count.Text, out count);
            float.TryParse(loan_price.Text, out loanprice);
            float.TryParse(annual_rate.Text, out annualrate);
            int.TryParse(annual_pay_count.Text, out Num_Pmt_Per_Year);
            float.TryParse(installment_price.Text, out _installment_price);

            if (count > 0 && loanprice > 0 && _installment_price > 0 && loan_startdate.Text != "")
            {
                _opening_balance = loanprice;
                DateTime duedate = Convert.ToDateTime(loan_startdate.Text);

                SqlCommand cmd = new SqlCommand();

                for (int i = 0; i <= count - 1; i++)
                {
                    _interest = Convert.ToInt32(_opening_balance * annualrate / 100 / Num_Pmt_Per_Year);         //償還利息
                    _principal = _installment_price - _interest;                                                 //償還本金 = 分期付款額 -償還利息
                    _end_balance = (_installment_price < _opening_balance) ? _opening_balance - _principal : 0;  //期末餘額
                    _total_interest += _interest;                                                                //累計利息

                    cmd.Parameters.AddWithValue("@loan_id" + i.ToString(), id);
                    cmd.Parameters.AddWithValue("@type" + i.ToString(), 0);
                    cmd.Parameters.AddWithValue("@due_date" + i.ToString(), duedate);
                    cmd.Parameters.AddWithValue("@opening_balance" + i.ToString(), _opening_balance);
                    cmd.Parameters.AddWithValue("@installment_price" + i.ToString(), _installment_price);
                    cmd.Parameters.AddWithValue("@principal" + i.ToString(), _principal);
                    cmd.Parameters.AddWithValue("@interest" + i.ToString(), _interest);
                    cmd.Parameters.AddWithValue("@end_balance" + i.ToString(), _end_balance);
                    cmd.Parameters.AddWithValue("@total_interest" + i.ToString(), _total_interest);
                    _opening_balance = _end_balance;
                    duedate = duedate.AddMonths(1);

                    strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}, @paid{0}, @paid_price{0}, @memo{0}", i.ToString());
                    //strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}", i.ToString());
                    strInsSQL += string.Format("INSERT INTO {0} ({1}) VALUES ({2});SELECT @@Identity;"
                               , strTable
                               , strCols
                               , strValues);


                }
                #region  更新利息總額欄位
                cmd.Parameters.AddWithValue("@interest", _total_interest);
                cmd.Parameters.AddWithValue("@id", id);
                strInsSQL += string.Format("UPDATE ttLoan SET interest=@interest WHERE id=@id");
                cmd.CommandText = strDelSQL + strInsSQL;
                dbAdapter.execQuery(cmd, true);
                #endregion
            }
        }
        #endregion

        strDelSQL = "";
        strInsSQL = "";
        if (del_payment_s)
        {
            strDelSQL = string.Format("Delete {0} where loan_id={1} and type= 1;"
                               , strTable
                               , id);
        }

        #region 區配&外車償還計畫表
        if (New_List_S.Items.Count > 0)
        {
            #region 訂金的錢由應付款金額攤提，並從最後一期開始扣除
            List<int> pay = new List<int>() ; 
            
            int _deposit_s = 0;
            int _installment_price_s = 0;
            int.TryParse(deposit_s.Text, out _deposit_s);
            int.TryParse(installment_price_s.Text, out _installment_price_s);
            int x = 0;
            int y = 0;
            
            if (_deposit_s > 0 && _installment_price_s >0)
            {
                x = _deposit_s / _installment_price_s;
                y = _deposit_s % _installment_price_s;
            }
            if (y > 0)
            {
                pay.Add(y);
            }

            for (int i = 1; i <= x; i++)
            {
                pay.Add(_installment_price_s);
            }
            #endregion  

            SqlCommand cmd = new SqlCommand();

            for (int i = 0; i <= New_List_S.Items.Count - 1; i++)
            {
                HiddenField hid_id = ((HiddenField)New_List_S.Items[i].FindControl("hid_id"));
                Label lbdate = ((Label)New_List_S.Items[i].FindControl("lbdate"));
                Label lbopening_balance = ((Label)New_List_S.Items[i].FindControl("lbopening_balance"));
                Label lbinstallment_price = ((Label)New_List_S.Items[i].FindControl("lbinstallment_price"));
                Label lbprincipal = ((Label)New_List_S.Items[i].FindControl("lbprincipal"));
                Label lbinterest = ((Label)New_List_S.Items[i].FindControl("lbinterest"));
                Label lbend_balance = ((Label)New_List_S.Items[i].FindControl("lbend_balance"));
                Label lbtotal_interest = ((Label)New_List_S.Items[i].FindControl("lbtotal_interest"));

                if (hid_id.Value == "0")
                {
                    cmd.Parameters.AddWithValue("@loan_id" + i.ToString(), id);
                    cmd.Parameters.AddWithValue("@type" + i.ToString(), 1);

                    if (DateTime.TryParse(lbdate.Text, out due_date))
                    {
                        cmd.Parameters.AddWithValue("@due_date" + i.ToString(), due_date);
                    }

                    if (float.TryParse(lbopening_balance.Text, out _opening_balance))
                    {
                        cmd.Parameters.AddWithValue("@opening_balance" + i.ToString(), _opening_balance);
                    }

                    if (float.TryParse(lbinstallment_price.Text, out _installment_price))
                    {
                        cmd.Parameters.AddWithValue("@installment_price" + i.ToString(), _installment_price);
                    }

                    if (float.TryParse(lbprincipal.Text, out _principal))
                    {
                        cmd.Parameters.AddWithValue("@principal" + i.ToString(), _principal);
                    }

                    if (float.TryParse(lbinterest.Text, out _interest))
                    {
                        cmd.Parameters.AddWithValue("@interest" + i.ToString(), _interest);
                    }

                    if (float.TryParse(lbend_balance.Text, out _end_balance))
                    {
                        cmd.Parameters.AddWithValue("@end_balance" + i.ToString(), _end_balance);
                    }

                    if (float.TryParse(lbtotal_interest.Text, out _total_interest))
                    {
                        cmd.Parameters.AddWithValue("@total_interest" + i.ToString(), _total_interest);
                    }

                    if (pay.Count == New_List_S.Items.Count - i)
                    {

                        cmd.Parameters.AddWithValue("@memo" + i.ToString(), "(訂金扣除:"+ pay[0].ToString() +")");
                        cmd.Parameters.AddWithValue("@paid_price" + i.ToString(), Convert.ToInt32(pay[0]));

                        if (Convert.ToInt32(pay[0]) == _installment_price)
                        {
                            cmd.Parameters.AddWithValue("@paid" + i.ToString(), 1);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@paid" + i.ToString(), 0);
                        }
                        pay.RemoveAt(0);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@memo" + i.ToString(), "");
                        cmd.Parameters.AddWithValue("@paid_price" + i.ToString(), 0);
                        cmd.Parameters.AddWithValue("@paid" + i.ToString(), 0);
                    }

                    strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}, @paid{0}, @paid_price{0}, @memo{0}", i.ToString());
                    strInsSQL += string.Format("INSERT INTO {0} ({1}) VALUES ({2});SELECT @@Identity;"
                               , strTable
                               , strCols
                               , strValues);
                }

            }
            cmd.CommandText = strDelSQL+ strInsSQL;
            if (strInsSQL != "")  dbAdapter.execQuery(cmd, true);

        }
        else
        {

            int count = 0;
            float loanprice = 0;
            float annualrate = 0;       //年利率
            int Num_Pmt_Per_Year = 0;   //每年付款次數

            int.TryParse(installment_count_s.Text, out count);
            float.TryParse(loan_price_s.Text, out loanprice);
            float.TryParse(annual_rate_s.Text, out annualrate);
            int.TryParse(annual_pay_count_s.Text, out Num_Pmt_Per_Year);
            float.TryParse(installment_price_s.Text, out _installment_price);

            if (count > 0 && loanprice > 0 && _installment_price > 0 && loan_startdate_s.Text != "")
            {
                _opening_balance = loanprice;
                DateTime duedate = Convert.ToDateTime(loan_startdate_s.Text);

                SqlCommand cmd = new SqlCommand();

                for (int i = 0; i <= count - 1; i++)
                {
                    _interest = Convert.ToInt32(_opening_balance * annualrate / 100 / Num_Pmt_Per_Year);         //償還利息
                    _principal = _installment_price - _interest;                                                 //償還本金 = 分期付款額 -償還利息
                    _end_balance = (_installment_price < _opening_balance) ? _opening_balance - _principal : 0;  //期末餘額
                    _total_interest += _interest;                                                                //累計利息

                    cmd.Parameters.AddWithValue("@loan_id" + i.ToString(), id);
                    cmd.Parameters.AddWithValue("@type" + i.ToString(), 1);
                    cmd.Parameters.AddWithValue("@due_date" + i.ToString(), duedate);
                    cmd.Parameters.AddWithValue("@opening_balance" + i.ToString(), _opening_balance);
                    cmd.Parameters.AddWithValue("@installment_price" + i.ToString(), _installment_price);
                    cmd.Parameters.AddWithValue("@principal" + i.ToString(), _principal);
                    cmd.Parameters.AddWithValue("@interest" + i.ToString(), _interest);
                    cmd.Parameters.AddWithValue("@end_balance" + i.ToString(), _end_balance);
                    cmd.Parameters.AddWithValue("@total_interest" + i.ToString(), _total_interest);
                    _opening_balance = _end_balance;
                    duedate = duedate.AddMonths(1);


                    strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}, 0, 0, ''", i.ToString());
                    //strValues = string.Format("@loan_id{0}, @type{0}, @due_date{0}, @opening_balance{0}, @installment_price{0} ,@principal{0}, @interest{0}, @end_balance{0}, @total_interest{0}", i.ToString());
                    strInsSQL += string.Format("INSERT INTO {0} ({1}) VALUES ({2});SELECT @@Identity;"
                               , strTable
                               , strCols
                               , strValues);


                }
                #region  更新利息總額欄位
                cmd.Parameters.AddWithValue("@interest_s", _total_interest);
                cmd.Parameters.AddWithValue("@id", id);
                strInsSQL += string.Format("UPDATE ttLoan SET interest_s=@interest_s WHERE id=@id");
                cmd.CommandText = strDelSQL + strInsSQL;
                dbAdapter.execQuery(cmd, true);
                #endregion

            }
        }
        #endregion




        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='loan4_1.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }


    protected void cal_Click(object sender, EventArgs e)
    {
        Button btncal = (Button)sender;
        if (btncal != null)
        {
            int count = 0;
            int loanprice = 0;
            int Beg_Bal = 0;            //期初餘額
            int endbalance = 0;         //期末餘額
            int totalinterest = 0;      //累計利息
            int Sched_Pay = 0;          //分期付款額        
            float annualrate = 0;       //年利率
            int Num_Pmt_Per_Year = 0;   //每年付款次數
            int principal = 0;          //當期償還本金
            int tot_principal = 0;      //累計償還本金
            int payinterest = 0;
            DateTime paydate = new DateTime() ;
            DataTable dt = new DataTable();
            switch (btncal.ID)
            {
                case "cal":  //公司
                    int.TryParse(installment_count.Text, out count);
                    int.TryParse(loan_price.Text, out loanprice);
                    float.TryParse(annual_rate.Text, out annualrate);
                    int.TryParse(annual_pay_count.Text, out Num_Pmt_Per_Year);
                    int.TryParse(installment_price.Text, out Sched_Pay);
                    DateTime.TryParse(loan_startdate.Text, out paydate);
                    del_payment = true; 
                    break;
                case "calS":   //區配&外車
                    int.TryParse(installment_count_s.Text, out count);
                    int.TryParse(loan_price_s.Text, out loanprice);
                    float.TryParse(annual_rate_s.Text, out annualrate);
                    int.TryParse(annual_pay_count_s.Text, out Num_Pmt_Per_Year);
                    int.TryParse(installment_price_s.Text, out Sched_Pay);
                    DateTime.TryParse(loan_startdate.Text, out paydate);
                    del_payment_s = true;
                    break;
            }


            //int loanprice_s = 0;
            //int Sched_Pay_s = 0;          //分期付款額
            //int.TryParse(loan_price_s.Text, out loanprice_s);
            //int.TryParse(installment_price_s.Text, out Sched_Pay_s);

            #region 
            if (count > 0 && loanprice > 0 && Sched_Pay > 0 && paydate > new DateTime(1911,1,1)) 
            {
                Beg_Bal = loanprice;
                dt.Columns.Add("id", typeof(int));
                dt.Columns.Add("due_date", typeof(DateTime));
                dt.Columns.Add("opening_balance", typeof(int));
                dt.Columns.Add("installment_price", typeof(int));
                dt.Columns.Add("principal", typeof(int));
                dt.Columns.Add("interest", typeof(int));
                dt.Columns.Add("end_balance", typeof(int));
                dt.Columns.Add("total_interest", typeof(int));
                dt.Columns.Add("pay_date", typeof(DateTime));
                dt.Columns.Add("paid", typeof(bool));
                dt.Columns.Add("paid_price", typeof(int));
                dt.Columns.Add("memo", typeof(string));

                for (int i = 0; i <= count - 1; i++)
                {
                    if (i == count - 1){
                        principal = loanprice - tot_principal; //最後一期，償還本金=貸款金額-已償還本金
                        payinterest = Sched_Pay - principal; //償還利息
                    }
                    else {
                        payinterest = Convert.ToInt32(Beg_Bal * annualrate / 100 / Num_Pmt_Per_Year);//償還利息
                        principal = Sched_Pay - payinterest;                               //償還本金 = 分期付款額 -償還利息
                    }

                    tot_principal += principal;
                    endbalance = (Sched_Pay < Beg_Bal) ? Beg_Bal - principal : 0;      //期末餘額
                    totalinterest += payinterest;                                       //累計利息

                    DataRow row = dt.NewRow();
                    row["id"] = 0;
                    row["due_date"] = paydate;
                    row["opening_balance"] = Beg_Bal;
                    row["installment_price"] = Sched_Pay;
                    row["interest"] = payinterest;                  //償還利息
                    row["principal"] = principal;                   //償還本金 = 分期付款額 -償還利息
                    row["end_balance"] = endbalance;                //期末餘額
                    row["total_interest"] = totalinterest;          //累計利息
                    row["paid"] = false;
                    row["paid_price"] = 0;
                    row["memo"] = "";
                    Beg_Bal = endbalance;
                    paydate = paydate.AddMonths(1);
                    dt.Rows.Add(row);
                }
            }
            switch (btncal.ID)
            {
                case "cal":  //公司
                    interest.Text = totalinterest.ToString();
                    New_List.DataSource = dt;
                    New_List.DataBind();
                    break;
                case "calS":   //區配&外車
                    interest_s.Text = totalinterest.ToString();
                    New_List_S.DataSource = dt;
                    New_List_S.DataBind();
                    break;
            }

            #endregion

            //#region 區配&外車
            //totalinterest = 0;
            //Beg_Bal = 0;
            //if (count > 0 && loanprice_s > 0 && Sched_Pay_s > 0 && loan_startdate.Text != "")
            //{
            //    Beg_Bal = loanprice_s;
            //    DateTime paydate = Convert.ToDateTime(loan_startdate.Text);
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add("id", typeof(int));
            //    dt.Columns.Add("due_date", typeof(DateTime));
            //    dt.Columns.Add("opening_balance", typeof(int));
            //    dt.Columns.Add("installment_price", typeof(int));
            //    dt.Columns.Add("principal", typeof(int));
            //    dt.Columns.Add("interest", typeof(int));
            //    dt.Columns.Add("end_balance", typeof(int));
            //    dt.Columns.Add("total_interest", typeof(int));
            //    dt.Columns.Add("pay_date", typeof(DateTime));
            //    dt.Columns.Add("paid", typeof(bool));
            //    dt.Columns.Add("memo", typeof(string));

            //    for (int i = 0; i <= count - 1; i++)
            //    {
            //        int payinterest = Convert.ToInt32(Beg_Bal * annualrate / 100 / Num_Pmt_Per_Year);//償還利息
            //        principal = Sched_Pay_s - payinterest;                                //償還本金 = 分期付款額 -償還利息
            //        endbalance = (Sched_Pay_s < Beg_Bal) ? Beg_Bal - principal : 0;       //期末餘額
            //        totalinterest += payinterest;                                       //累計利息

            //        DataRow row = dt.NewRow();
            //        row["id"] = 0;
            //        row["due_date"] = paydate;
            //        row["opening_balance"] = Beg_Bal;
            //        row["installment_price"] = Sched_Pay_s;
            //        row["interest"] = payinterest;                  //償還利息
            //        row["principal"] = principal;                   //償還本金 = 分期付款額 -償還利息
            //        row["end_balance"] = endbalance;                //期末餘額
            //        row["total_interest"] = totalinterest;          //累計利息
            //        row["paid"] = false;
            //        row["memo"] = "";
            //        Beg_Bal = endbalance;
            //        paydate = paydate.AddMonths(1);
            //        dt.Rows.Add(row);
            //    }

            //    interest_s.Text = totalinterest.ToString();
            //    New_List_S.DataSource = dt;
            //    New_List_S.DataBind();
            //}
            //#endregion
        }


    }



   
}