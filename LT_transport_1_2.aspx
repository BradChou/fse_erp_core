﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_1_2.aspx.cs" Inherits="LT_transport_1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#Receiveclick").fancybox({
                'width': 980,
                'height': 600,
                'autoScale': false,
                'transitionIn': 'none',
                'type': 'iframe',
                'transitionOut': 'none',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

            $(".subpoena_category").change();

            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date())  //預設當日
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".date_picker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    defaultDate: (new Date())  //預設當日
                });
            }


        });
        $.fancybox.update();



        $(document).on("change", ".subpoena_category", function () {
            var _isOK = true;
            var _cateVal = $(this).children("option:selected").val();
            var _setItem;
            var _disItem;
            switch (_cateVal) {
                case '11'://元付
                    //_collection_money
                    _disItem = $("._collection_money");
                    _setItem = $("._arrive_to_pay_freight,._arrive_to_pay_append");
                    break;
                case '21'://到付
                    _disItem = $("._collection_money,._arrive_to_pay_append");
                    _setItem = $("._arrive_to_pay_freight");
                    break;
                case '25'://元付-到付追加
                    _disItem = $("._collection_money,._arrive_to_pay_freight");
                    _setItem = $("._arrive_to_pay_append");
                    break;
                case '41'://代收貨款
                    _disItem = $("._arrive_to_pay_append,._arrive_to_pay_freight");
                    _setItem = $("._collection_money");
                    break;
                default:
                    _isOK = false;
                    break;
            }
            if (_isOK) {

                _disItem.map(function () { $(this).prop('readonly', true); });
                _setItem.map(function () { $(this).prop('readonly', false); });

            }

        });
    </script>


    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        ._ddl {
            min-width: 85px;
        }

        ._addr {
            min-width: 35%;
        }

        .div_addr {
            width: 75%;
        }

        .ralign {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">退貨輸入</h2>
            <hr />
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                           <%-- <label>客代編號：</label>
                            <asp:DropDownList ID="dlcustomer_code" runat="server" class="form-control chosen-select" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lbcustomer_name" runat="server"></asp:Label>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代編號</asp:RequiredFieldValidator>--%>
                            <label>取貨站所：</label>
                            <asp:Label ID="lbstation_scode" runat="server">
                            </asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <label>收貨人：</label>
                            <asp:Label ID="lbreceive_contact" runat="server">  </asp:Label>
                             <label>客代編號：</label>
                            <asp:Label ID="lbcustomer_code" runat="server">  </asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <label>收貨人電話：</label>
                            <asp:Label ID="lbreceive_tel1" runat="server">  </asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label>收貨人地址：</label>
                            <asp:Label ID="lbreceive_address" runat="server">  </asp:Label>
                             <asp:HiddenField ID="hid_receive_city" runat="server" /> 
                             <asp:HiddenField ID="hid_receive_area" runat="server" />
                             <asp:HiddenField ID="hid_receive_address" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                  <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <label>欲退貨貨號：</label>
                            <asp:TextBox ID="strkey" MaxLength="21" runat="server" class="form-control"></asp:TextBox>
                            <asp:Button ID="btnselect" CssClass="templatemo-blue-button" runat="server" Text="查 詢" OnClick="btnselect_click" />
                              <asp:DropDownList ID="ddlcustomer" AutoPostBack="true" class="form-control" runat="server"  OnSelectedIndexChanged="ddlcustomer_SelectedIndexChanged"  Visible ="false"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h3>派遣內容</h3>
                    <div class="rowform">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <div class="row form-inline">
                                    <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>託運單號(12碼)</label>
                                        <asp:TextBox  ID="strcheck_number" runat="server" class="form-control" Enabled="false"  MaxLength="12" BorderColor="Red"></asp:TextBox>
                                         <asp:Label ID="send_station_scode" runat="server" >  </asp:Label>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label>訂單編號(20碼)</label>
                                        <asp:TextBox ID="strorder_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>退(寄)貨人</label>
                                        <asp:TextBox ID="strsend_contact" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>退(寄)貨人電話</label>
                                        <asp:TextBox ID="strsend_tel" runat="server" class="form-control">
                                        </asp:TextBox>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>退(寄)貨人地址</label>
                                        <%--<asp:DropDownList ID="ddlzip" runat="server" class="form-control  subpoena_category" style="background:#f2f2f2;"  Enabled="false">
                                        </asp:DropDownList>--%>
                                        <asp:Label id="ddlzip" runat="server" Text="選擇地址後自動帶出"></asp:Label>
                                        <asp:DropDownList ID="ddlsend_city" runat="server" class="form-control  subpoena_category" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlsend_area" runat="server" class="form-control  subpoena_category"  AutoPostBack="true" OnSelectedIndexChanged="area_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:TextBox ID="strsend_address" runat="server" class="form-control"></asp:TextBox>
                                    </div>

                                    <br>
                                     <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>件數</label>
                                        <asp:TextBox ID="tbPieces" runat="server" class="form-control" MaxLength="20" OnTextChanged="dl_Pieces_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                                                        <br>
                                     <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>產品名稱</label>
                                        <asp:DropDownList ID="dlProductName" runat="server" class="form-control" MaxLength="20" OnSelectedIndexChanged="dl_ProductName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                         <h5 style="color: red; display: inline-block; margin-left: 10px">請先選擇件數後才會出現</h5>
                                    </div>
                                    <br>
                                     <div class="form-group  form-inline">
                                        <label><span class="REDWD_b">*</span>規格</label>
                                        <asp:DropDownList ID="dlProductSpec" runat="server" class="form-control" MaxLength="20"></asp:DropDownList>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label>收回品項</label>
                                        <textarea id="invoice_memo" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                                    </div>
                                    <br>
                                    <div class="form-group  form-inline">
                                        <label>備註(說明)</label>
                                        <textarea id="invoice_desc" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                                    </div>
                                </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group text-center">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="清 除" OnClick="btnRomv_Click"/>
                    </div>
                </div>
            </div>
    </div>
    </div>
</asp:Content>
