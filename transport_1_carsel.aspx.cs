﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class transport_1_carsel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            readdata();
        }
    }


    private void readdata()
    {
       
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.Clear();
            cmd.CommandText = string.Format(@"select A.* from tbItemCodes A with(nolock) WHERE (code_bclass = '2') AND (code_sclass = 'S8')");
            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
            
        }           

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {
            
            string Hid_Pallet_type = ((HiddenField)e.Item.FindControl("Hid_Pallet_type")).Value.ToString().Trim();
            string Pallet_type_text = ((Literal)e.Item.FindControl("Pallet_type_text")).Text.Trim();
            string JavaScriptStr = "";
            //string JavaScriptStr = "self.parent.$('#" + Request.QueryString["Hid_code_id"] + "').val('" + Hid_code_id + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["Pallet_type"] + "').val('" + Hid_Pallet_type + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["Pallet_type_text"] + "').val('" + Pallet_type_text + "');";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>"+ JavaScriptStr + " parent.$.fancybox.close();</script>", false);
            return;

        }
       
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }


    
}