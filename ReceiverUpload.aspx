﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReceiverUpload.aspx.cs" Inherits="ReceiverUpload" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-上傳檔案</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <link href="css/jquery-ui-1.12.1.css" rel="stylesheet" />

    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>

    <style type="text/css">
        .tb_fee {
            margin: auto;
        }

        ._th {
            width: 80px;
        }

        ._td {
            width: 320px;
        }

        ._td_btn {
            text-align: center;
        }

        .btn {
            margin: 3px 8px;
        }

        .bk_color1 {
            background-color: #eff0f1;
        }
    </style>
    <script type="text/javascript">
        $(function () {

            $("input[name=rad_ShipType]:radio").click(function () {
                var _val = $("input[name=rad_ShipType]:checked").val();
                if (!isNaN(_val)) {
                    switch (parseInt(_val)) {
                        case 2:
                            $("._tr_upload").show();
                            break;
                        default:
                            $("._tr_upload").hide();
                            break;
                    }
                }
            });
        });      
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="div_fee">
            <table class="table tb_fee">
                <tr>
                    <th class="_th">匯入方式</th>
                    <td class="_td">                      
                        <span class="text-danger span_tip">※ 針對現有資料更新及寫入新資料</span>
                    </td>
                </tr>
                <tr class="_tr_upload">
                    <th>上傳檔案
                    </th>
                    <td>
                        <asp:FileUpload ID="file01" runat="server" Width="300px" CssClass="_feefile" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="_td_btn">
                        <asp:HiddenField ID="hidf_id" runat="server" />
                        <asp:Button ID="btnsave" CssClass="btn btn-primary _btnsave" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:Button ID="btncancel" CssClass="btn btn-default" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                    </td>
                </tr>
            </table>
            <div style="overflow: auto; height: 500px; vertical-align: top;">
                <asp:ListBox ID="lbMsg" runat="server" Height="490px" Width="100%"></asp:ListBox>
            </div>

        </div>
    </form>
</body>
</html>
