﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterSystem.master" AutoEventWireup="true" CodeFile="LTsystem_3.aspx.cs" Inherits="LTsystem_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="js/jquery-1.11.2.min.js"></script>
    <link href="css/build.css" rel="stylesheet" />
    <style type="text/css">
        input[type="checkbox"] {
            display: inline;
        }

        .tb_area_data tr {
            padding: 5px;
        }

        ._set_unarea {
            cursor: pointer;
            float: right;
            margin: 0px 20px;
            color: #52abd0;
        }

        ._set_unset_un {
            display: inline-block;
            min-width: 80px;
        }

        .t_unset_un_td2 {
            text-align: center;
            background-color: #f8e9e8;
        }

        .tb_area_unset_data tr:hover {
            background-color: lightyellow;
        }

        ._btn_un_save, ._btn_un_cancel {
            margin: 5px 10px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }
    </style>
    <script type="text/javascript">


        $(document).ready(function () {
            $("._set_unarea").click(function () {
                SetUn_area();
            });

        });



        function SetCkb() { } {
            $("span[the_status=-1]").find("input[type=checkbox]").attr("disabled", "disabled");
        }


        $(document).on("click", "._btn_save", function () {
            var result;
            var IsVerify = true;
            var _setNum = $("span[the_status=1],span[the_status=0]").find("input[type=checkbox]").length;

            if (_setNum <= 0) {
                IsVerify = false;
                alert("此區域商無可設定資料，請重新確定!");
                return IsVerify;
            } else {
                var theVal1 = $('span[the_status=1],span[the_status=0]').find("input[type=checkbox]:checked").map(function () { return this.value; }).get().join(',');

                $.ajax({
                    url: 'LTsystem_3.aspx/SetArea',
                    type: 'POST',
                    async: false,//同步 
                    data: JSON.stringify({ supplyer: $(".ddl_station").val(), city: $(".ddl_city").val(), item: theVal1 }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序                        
                    }
                }).done(function (data, statusText, xhr) {

                    result = JSON.parse(data.d);

                    if (result.status == 1) {
                        $('._areashow').html(result.area_data)
                        alert("設定成功");
                        //location.reload();
                        //location.replace(location.href);
                        

                    } else {
                        alert(result.reason);
                    }
                });

                //else end
            }
        });

        //取得未設定的所有區域
        function SetUn_area() {
            var _the_status = $("._set_unarea").attr("_status");
            var _title = "區配商區域設定";
            if (_the_status == '0') {
                //----------------------------                
                $.ajax({
                    url: 'LTsystem_3.aspx/GetUnSetAreaWithHtml',
                    type: 'POST',
                    async: false,//同步                     
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert("success!");
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d == '0') {
                        alert("目前無未設定區域資料!");
                    } else {
                        _title = "未設定區域設定";
                        $('._the_title').html(_title);
                        $('#div_unset_area').html(data.d);
                        $("._set_unarea").attr("_status", '1');
                        $("#div_set_area").hide();
                        $("#div_unset_area").show();
                    }
                });

            } else {
                $('._the_title').html(_title);
                $("._set_unarea").attr("_status", '0');
                $("#div_unset_area").hide();
                $("#div_set_area").show();
            }
        };

        //未設定區域btn setting 
        $(document).on("click", "._btn_un_save", function () {
            var _setNum = $("span[the_unstatus=0],span[the_status=0]").find("input[type=checkbox]:checked").length;
            var IsOK = true;
            if (_setNum == 0) {
                alert("請勾選欲設定區域!");
                IsOK = false;
            }
            if (IsOK == false) {
                return IsOK;
            } else {
                //start save
                var ischk = confirm("確定進行區域設定? ");
                if (ischk) {
                    var un_set_list = [];
                    var un_tr = $(".unset_data_tr").map(function () {
                        var _set = $(this).find("input[type=checkbox]:checked");
                        if (_set.length > 0) {
                            var item = _set.map(function () { return this.value; }).get().join(',');
                            var supplyer = $(this).find("select[name=ddl_supplyer_un]").val();
                            var city = $(this).attr('_t_city');
                            un_set_list.push({
                                'supplyer': supplyer, 'city': city, 'item': item
                            });
                        }
                    });

                    if (un_set_list.length > 0) {
                        $.ajax({
                            url: 'system_3_lt.aspx/SetUnArea',
                            type: 'POST',
                            async: false,//同步 
                            data: JSON.stringify({ unset: un_set_list }),
                            contentType: 'application/json; charset=UTF-8',
                            dataType: "json",      //如果要回傳值，請設成 json
                            error: function (xhr) {//發生錯誤時之處理程序 
                            },
                            success: function (reqObj) {//成功完成時之處理程序 

                            }
                        }).done(function (data, statusText, xhr) {
                            if (data.d == '1') {
                                alert("設定成功");
                                location.reload();
                            } else {
                                alert(data.d);
                            }
                        });
                    }

                }
            }

        });

        $(document).on("click", "._btn_un_cancel", function () {
            $('._the_title').html("區配商區域設定");
            $("._set_unarea").attr("_status", '0');
            $("#div_unset_area").hide();
            $("#div_set_area").show();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h2 class="margin-bottom-10 _the_title">配送行政區歸屬站所設定</h2>

    <div id="div_set_area" class="templatemo-login-form">
        <span class="_set_unarea" _status="0">未設定區域設定</span>
        <div class="row form-group">
            <!-- DDL Area start -->
            <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label for="ddl_station">站所</label>
                    <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control ddl_station" AutoPostBack="True" OnSelectedIndexChanged="ddl_station_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label for="ddl_city">縣市</label>
                    <asp:DropDownList ID="ddl_city" runat="server" CssClass="form-control ddl_city" AutoPostBack="True" OnSelectedIndexChanged="ddl_city_SelectedIndexChanged"></asp:DropDownList>
                </div>

            </div>
        </div>
        <!-- DDL Area end -->

        <!-- Setting Area start -->
        <div class="_set_area">
            <div class="row form-group form-inline">
                <div class="col-lg-12 form-group form-inline">
                    <label for="inputLastName">區域</label>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <asp:CheckBoxList ID="cbl_area" runat="server" RepeatDirection="Horizontal" RepeatColumns="12"></asp:CheckBoxList>
                            <hr />
                            <p class="text-primary">
                                ※狀態說明：
                                <span class="checkbox checkbox-success">
                                    <input type="checkbox">
                                    <label>未設定</label>
                                </span>
                                <span class="checkbox checkbox-success">
                                    <input type="checkbox" checked="checked" disabled>
                                    <label>已使用</label>
                                </span>
                                <span class="checkbox checkbox-success">
                                    <input type="checkbox" checked="checked">
                                    <label>選擇區域</label>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group text-center">
                <button type="button" class="templatemo-blue-button" style="display: none; visibility: hidden;">設　定</button>
                <button type="button" class="templatemo-blue-button _btn_save">確　認</button>
            </div>
            <hr />
        </div>
        <!-- Setting Area end -->

        <!-- Area start -->
        <asp:Panel ID="pan_AreaShow" runat="server" CssClass="_areashow"></asp:Panel>
        <!-- Area end -->

    </div>

    <!-- UnSetting Area start -->
    <div id="div_unset_area" style="display: none;"></div>
    <!-- UnSetting  Area end -->

</asp:Content>

