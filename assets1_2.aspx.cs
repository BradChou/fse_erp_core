﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_2 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 車行
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'CD' and active_flag = 1 order by code_id ";
                ddl_car_retailer.DataSource = dbAdapter.getDataTable(cmd);
                ddl_car_retailer.DataValueField = "code_id";
                ddl_car_retailer.DataTextField = "code_name";
                ddl_car_retailer.DataBind();
                ddl_car_retailer.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 使用單位
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=1", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  code_id=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 {0} order by code_id", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                Dept.DataSource = dt;
                Dept.DataValueField = "code_id";
                Dept.DataTextField = "code_name";
                Dept.DataBind();
                Dept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 使用單位
            //using (SqlCommand cmd = new SqlCommand())
            //{
            //    cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 order by code_id ";
            //    Dept.DataSource = dbAdapter.getDataTable(cmd);
            //    Dept.DataValueField = "code_id";
            //    Dept.DataTextField = "code_name";
            //    Dept.DataBind();
            //    Dept.Items.Insert(0, new ListItem("請選擇", ""));
            //}
            #endregion

        }
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (car_license.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_license", car_license.Text.Trim().ToString());
            wherestr += "  and a.car_license like '%'+@car_license+'%'";
        }


        if (ddl_car_retailer.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_retailer", ddl_car_retailer.SelectedValue.ToString());
            wherestr += "  and a.car_retailer =@car_retailer";
        }

        if (driver.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@driver", driver.Text.Trim().ToString());
            wherestr += "  and a.driver like '%'+@driver+'%'";
        }

        if (owner.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@owner", owner.Text.Trim().ToString());
            wherestr += "  and a.owner like '%'+@owner+'%'";
        }

        if (Dept.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@dept", Dept.SelectedValue.ToString());
            wherestr += "  and a.dept =@dept";
        }
        
        objCommand.CommandText = string.Format(@"select A.* , OS.code_name as ownership_text ,CD.code_name as car_retailer_text , CT.code_name as cabin_type_text
                                                    from ttAssets A with(nolock)
                                                    left join tbItemCodes OS with(nolock) on A.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                    left join tbItemCodes CD with(nolock) on A.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD'
                                                    left join tbItemCodes CT with(nolock) on A.cabin_type  = CT.code_id and CT.code_bclass = '6' and CT.code_sclass= 'cabintype'
                                                where 1=1  {0} 
                                                order by a.get_date asc", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {   
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

}