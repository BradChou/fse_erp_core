﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterMember.master"  CodeFile="LT_member_4.aspx.cs" Inherits="LT_member_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script src="js/jquery-1.11.2.min.js"></script>


    <script>
        //限制textbox的最大最小值

        function minmax(value, min, max) {
            value = value.replace(/[^\d]/g, '');
            if (value == "") {
                return value;
            }
            else {
                if (parseInt(value) < min || isNaN(parseInt(value)))
                    return min;
                else if (parseInt(value) > max)
                    return max;
                else return value;
            }
        }        
    </script>

    <style type="text/css">
        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .td_th {
            text-align: center;
        }
         .td_up {
           width: 180px;
        }
        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        #ContentPlaceHolder1_adjust:before {
            content: "* ";
            color: red;
            margin-left: 79.285px;
        }

        .important:before {
            margin-left: -10px;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }
        .auto-style1 {
            height: 114px;
            width: 26%;
        }

        .title1 {
            margin-left: -80px;
        }
        .auto-style2 {
            width: 23%;
        }
        .auto-style3 {
            width: 26%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h2 class="margin-bottom-10">
        <asp:Label ID="lbl_title" Text="預購袋/超值箱數量查詢" runat="server"></asp:Label></h2>



    <asp:Panel ID="pan_view" runat="server">
        <div class="div_view">
            <!-- 查詢 -->
            <div class="form-group form-inline">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 70%;">
                            <asp:Label ID="lb1" Text="查詢條件：" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control" ToolTip="所屬站別"></asp:DropDownList>
                            <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control" ToolTip="類別">
                            <asp:ListItem Value="">全部</asp:ListItem>
                                            <asp:ListItem Value="2">預購袋</asp:ListItem>
                                            <asp:ListItem Value="3">超值箱</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 客戶代號 或 名稱"></asp:TextBox>
                            <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                        </td>

                    </tr>
                </table>
            </div>

            <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr>
                    <th class="td_th td_no">No</th>
                    <th class="td_th">站別</th>
                    <th class="td_th td_name">客戶</th>
                    <th class="td_th td_yn">總件數</th>
                    <th class="td_th td_yn">剩餘件數</th>
                   
                </tr>
                <asp:Repeater ID="list_customer" runat="server" >
                    <ItemTemplate>
                        <tr>
                            <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                            <td class="td_data" data-th="站別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <td class="td_data td_name text-left  " data-th="客戶"><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString() + "　" + DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                            <td class="td_data td_no" data-th="總件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_count").ToString())%></td>
                            <td class="td_data td_no" data-th="剩餘件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rest_count").ToString())%></td>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (list_customer.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="11" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style=" text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>
            <!--內容-end -->
        </div>
    </asp:Panel>

   
    
</asp:Content>
