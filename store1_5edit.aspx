﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_5edit.aspx.cs" Inherits="store1_5edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            //移到右边
 $('#add').click(function(){
 //先判断是否有选中
 if(!$("#<%=DeptList.ClientID%> option").is(":selected")){  
  alert("請選擇移動項目!")
 }
 //获取选中的选项，删除并追加给对方
 else {
     $('#<%=DeptList.ClientID%> option:selected').appendTo('#<%=DeptListGet.ClientID%>');
     GetVal();
 } 
 });
 
 //移到左边
 $('#remove').click(function(){
 //先判断是否有选中
 if(!$("#<%=DeptListGet.ClientID%> option").is(":selected")){  
  alert("請選擇移動項目!")
 }
 else {
     $('#<%=DeptListGet.ClientID%> option:selected').appendTo('#<%=DeptList.ClientID%>');
     GetVal();
 }
 });
 
 //全部移到右边
 $('#add_all').click(function(){
 //获取全部的选项,删除并追加给对方
     $('#<%=DeptList.ClientID%> option').appendTo('#<%=DeptListGet.ClientID%>');
     GetVal();
 });
 
 //全部移到左边
 $('#remove_all').click(function(){
     $('#<%=DeptListGet.ClientID%> option').appendTo('#<%=DeptList.ClientID%>');
     GetVal();
 });
 
 //双击选项
 $('#<%=DeptList.ClientID%>').dblclick(function(){ //绑定双击事件
 //获取全部的选项,删除并追加给对方
     $("option:selected", this).appendTo('#<%=DeptListGet.ClientID%>'); //追加给对方
     GetVal();
 });
 
 //双击选项
 $('#<%=DeptListGet.ClientID%>').dblclick(function(){
     $("option:selected", this).appendTo('#<%=DeptList.ClientID%>');
     GetVal();
            });

            

        });
        function GetVal() {
            $("#<%=DeptListGet_Val.ClientID%>").val("");
            var DeptListGet_Val = "";
            var select = $('#<%=DeptListGet.ClientID%> option');
            for (var i = 0, j = select.length; i < j; i++) {
                DeptListGet_Val += select[i].value+",";
            }
            $("#<%=DeptListGet_Val.ClientID%>").val(DeptListGet_Val);
        }
    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }

      
        .radio label {
            margin-right: 15px;
            text-align :left ;
        }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="人員路線維護設定" runat="server"></asp:Label>
        </h2>
        <hr />
        <div  >
            <div class="row">
                <div class="col-lg-6 form-group form-inline">
                    <label class="">管理單位：</label>
                    <asp:Label ID="lbitem" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">人　　員：</label>
                    <asp:Label ID="lbitem_employee" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">帳　　號：</label>
                    <asp:Label ID="lbaccount_code" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">名　　稱：</label>
                    <asp:Label ID="lbaccount_name" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 form-group form-inline" style="height:150px;">
                    <label class=""><font color="blue">(未選)</font>路線清單：</label>
                    <select id="DeptList" multiple runat="server"  class="form-control" style="width:100%;height:100%"></select>
                </div>
                <div class="col-lg-1 col-md-1 form-group form-inline" style="text-align:center;padding-top: 30px;">
                  <p><span id="add"><input type="button" class="btn" style="width:100%" value=">" title="移到右側"/></span></p>
                  <p><span id="add_all"><input type="button" class="btn" style="width:100%" value=">>" title="全部移到右側"/></span></p>
                  <p><span id="remove"><input type="button" class="btn" style="width:100%" value="<" title="移到左側"/></span></p>
                  <p><span id="remove_all"><input type="button" class="btn" style="width:100%" value="<<" title="全部移到左側"/></span></p>
                </div>
                <div class="col-lg-5 col-md-5 form-group form-inline" style="height:150px;">
                    <label class=""><font color="red">(已選)</font>路線清單：</label>
                    <input id="DeptListGet_Val" type="hidden" runat="server" />
                    <select id="DeptListGet" multiple runat="server"  class="form-control" style="width:100%;height:100%"></select>
                </div>
            </div>
            <div class="form-group text-center">
                <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲 存" ValidationGroup="validate" OnClick="btnsave_Click"  />
                <a href="store1_3.aspx" class="templatemo-white-button">返回</a>
            </div>
        </div>

        
        
            </div>
    </div>
</asp:Content>

