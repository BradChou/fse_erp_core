﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JunFuTrace.aspx.cs" Inherits="JunFuTrace" %>

<!DOCTYPE html>

<html lang="zh">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>峻富雲端物流管理-貨物追踨</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="" />
    <meta name="author" content="templatemo" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <!-- jQuery文件 -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    
</head>
<body>
    <form id="form1" runat="server">
        <div class="row">
            <div class="templatemo-content-widget white-bg">
                <h2 class="margin-bottom-10">貨號查詢</h2>
                <div class="templatemo-login-form">
                    <div class="row form-group">
                        <div class="col-lg-12 col-md-12 form-group form-inline" >
                            <label for="check_number">貨　號</label>
                            <asp:TextBox ID="check_number" runat="server" MaxLength="20"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="check_number" ForeColor="Red" ValidationGroup="validate">請輸入貨號</asp:RequiredFieldValidator>

                            <label for="inputLastName">次貨號</label>
                            <asp:TextBox ID="sub_chknum" runat="server">000</asp:TextBox>
                            <%--<button type="submit" class="btn btn-darkblue">影像查詢</button>--%>
                            <asp:Button ID="search" CssClass="templatemo-blue-button btn" runat="server" Text="查 詢" ValidationGroup="validate" OnClick="search_Click" />
                        </div>
                    </div>
                    <hr>
                    <table class="table table-striped table-bordered templatemo-user-table">
                        <tr class="tr-only-hide">
                            <th>發送站</th>
                            <th>到著站</th>
                            <th>發送日</th>
                            <th>指配日期</th>
                            <th>指配時間</th>
                            <th>訂單編號</th>
                            <th>件數</th>
                            <th>傳票區分</th>
                            <th>商品種類</th>
                            <th>請款金額(元付)</th>
                            <th>到付追加</th>
                            <th>代收金額</th>
                        </tr>
                        <asp:Repeater ID="New_List_01" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                                    <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.showname").ToString())%></td>
                                    <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                                    <td data-th="指配日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                                    <td data-th="指配時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.time_period").ToString())%></td>
                                    <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                                    <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                                    <td data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_name").ToString())%></td>
                                    <td data-th="商品種類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.product_category_name").ToString())%></td>
                                    <td data-th="請款金額">*****</td>
                                    <td data-th="到付追加">*****</td>
                                    <td data-th="代收金額">*****</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List_01.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="12" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>

                    <table class="table table-striped table-bordered templatemo-user-table">
                        <thead>
                            <tr>
                                <td>寄件人資料</td>
                                <td>收件人資料</td>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Label ID="lbSend" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbReceiver" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="divMaster" runat="server" class="templatemo-login-form panel panel-default">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="auto-style1">NO.</th>
                                <th class="auto-style1">作業時間</th>
                                <th class="auto-style1">作業</th>
                                <th class="auto-style1">站所</th>
                                <th class="auto-style1">到著/發送站</th>
                                <th class="auto-style1">件數</th>
                                <th class="auto-style1">班次代號</th>
                                <th class="auto-style1">狀態</th>
                                <th class="auto-style1">運輸方式</th>
                                <th class="auto-style1">註區</th>
                                <th class="auto-style1">員工</th>
                                <%--<th class="auto-style1">影像查詢</th>--%>
                            </tr>
                            <asp:Repeater ID="New_List_02" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                                        <td data-th="作業時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                                        <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                                        <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                        <td data-th="到著/發送站"></td>
                                        <td data-th="件數">1</td>
                                        <td data-th="班次代號"></td>
                                        <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status").ToString())%></td>
                                        <td data-th="運輸方式"></td>
                                        <td data-th="註區"></td>
                                        <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>  <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                                        <%--<td data-th="影像查詢">
                                            <asp:HyperLink runat="server" ID="Imglink" class="fancybox fancybox.iframe">
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" /></asp:HyperLink>
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List_02.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="11" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                    <div style="overflow: auto; height: 400px; vertical-align: top;">
                        <asp:Image ID="Image1" runat="server" />
                    </div>

                </div>
            </div>

        </div>
    </form>
</body>
</html>
