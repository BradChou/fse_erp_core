﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting; 	//Chart Web 伺服器控制項的方法和屬性
using System.Drawing;                   //繪圖功能的存取
using System.Collections;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;
using System.Web.Services;
using NPOI.SS.Formula.Functions;
using System.Windows.Forms;
public partial class member_3_edit : System.Web.UI.Page
{
    public string manager_type
    {
        // for權限

        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }




    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            Hidetype.Text = Less_than_truckload;


            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            manager_type = Session["manager_type"].ToString();        //管理單位類別
            //計績營業員、計績站所 僅skyeyes、品慧、昀萱帳號可以修改
            using (SqlCommand cmdsales = new SqlCommand())
            {
                cmdsales.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
                cmdsales.CommandText = " SELECT sales_id from tbCustomers where customer_id=@customer_id ";
                DataTable dt_sales = dbAdapter.getDataTable(cmdsales);
                if (dt_sales.Rows.Count > 0)
                {
                    ddl_sales_id.Enabled = false;
                    ddl_sale_station.Enabled = false;
                }

            }
                if (Session["account_code"] != null & (Session["account_code"].ToString() == "skyeyes" || Session["account_code"].ToString() == "9990309" || Session["account_code"].ToString() == "9990332"))
                {
                    ddl_sales_id.Enabled = true;
                    ddl_sale_station.Enabled = true;
                }

            //是否可修改新客代合約勾選->以C1-3修改權限作為判斷基準
            using (SqlCommand cmdc = new SqlCommand())
            {
                cmdc.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmdc.CommandText = "select * from tbFunctionsAuth " +
                    "where account_code = @account_code and func_code = 'C1-3' and func_type = '1' and modify_auth='1'";
                using (DataTable dtc = dbAdapter.getDataTable(cmdc))
                {
                    if (dtc != null && dtc.Rows.Count > 0) 
                    {
                        is_new_customer.Enabled = true;
                        OldCustomerOverCBM.Enabled = true;
                    }                
                }
            }
            //管理單位為站所時，權限更改
            if (Less_than_truckload == "1") IdeChk(manager_type);

            if (Less_than_truckload == "0")
            {
                is_weekend_delivered.Visible = false;
                weekend_delivery_fee.Visible = false;
                product_type.Visible = false;
                product_type_label.Visible = false;
            }

            if (manager_type == "3" || manager_type == "4" || manager_type == "5")
            {
                btnsave.Style.Add("display", "none");
            }

            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = " Select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM'  order by code_id asc";
            SPM_code.DataSource = dbAdapter.getDataTable(cmd2);
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion

            #region 郵政縣市        
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.CommandText = "select * from tbPostCity order by seq asc ";
                City.DataSource = dbAdapter.getDataTable(cmd1);
                City.DataValueField = "city";
                City.DataTextField = "city";
                City.DataBind();
                City.Items.Insert(0, new ListItem("請選擇", ""));

                InvoiceCity.DataSource = dbAdapter.getDataTable(cmd1);
                InvoiceCity.DataValueField = "city";
                InvoiceCity.DataTextField = "city";
                InvoiceCity.DataBind();
                InvoiceCity.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 商品類型
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select product_type, description from check_number_prehead ";
                product_type.DataSource = dbAdapter.getDataTable(cmd);
                product_type.DataValueField = "product_type";
                product_type.DataTextField = "description";
                product_type.DataBind();
                product_type.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 區配商
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select supplier_code, supplier_code + ' ' + supplier_name as showname from tbSuppliers where LEN(supplier_code) > 0 and supplier_code != '000' and active_flag = 1";
                supplier.DataSource = dbAdapter.getDataTable(cmd);
                supplier.DataValueField = "supplier_code";
                supplier.DataTextField = "showname";
                supplier.DataBind();
                supplier.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 記績營業員
            using (SqlCommand cmd4 = new SqlCommand())
            {
                cmd4.CommandText = "select case account_type when 'BF' then account_code when 'DRIVER' then driver_code end as id , name as showname from tbSales Where version_code = (Select max(version_code) From tbSales )";
                ddl_sales_id.DataSource = dbAdapter.getDataTable(cmd4);
                ddl_sales_id.DataValueField = "id";
                ddl_sales_id.DataTextField = "showname";
                ddl_sales_id.DataBind();
                ddl_sales_id.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            //計績站所
            using (SqlCommand cmd5 = new SqlCommand())
            {
                cmd5.CommandText = "select station_scode, station_code+' '+ station_name as showname from tbStation where active_flag='1' order by id  ";
                ddl_sale_station.DataSource = dbAdapter.getDataTable(cmd5);
                ddl_sale_station.DataValueField = "station_scode";
                ddl_sale_station.DataTextField = "showname";
                ddl_sale_station.DataBind();
                ddl_sale_station.Items.Insert(0, new ListItem("請選擇", ""));
            }

            #region 母客代
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%'";
                ddlMasterCustomerCode.DataSource = dbAdapter.getDataTable(cmd);
                ddlMasterCustomerCode.DataValueField = "customer_code";
                ddlMasterCustomerCode.DataTextField = "showname";
                ddlMasterCustomerCode.DataBind();
                ddlMasterCustomerCode.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            if (Less_than_truckload == "1")
            {
                using (SqlCommand cmd4 = new SqlCommand())
                {
                    cmd4.CommandText = "select bank_code,bank_code+' '+bank_name as showname from tbBanks order by bank_code ";
                    bank.DataSource = dbAdapter.getDataTable(cmd4);
                    bank.DataValueField = "showname";
                    bank.DataTextField = "showname";
                    bank.DataBind();
                    bank.Items.Insert(0, new ListItem("請選擇", ""));
                }


            }
            if (Less_than_truckload == "0")
            {
                bank_data.Visible = false;
                return_address_chk.Visible = false;
            }



            readdata();
            //CreateLineChart();

            int customer_id = 0;
            if (Request.QueryString["customer_id"] != null && int.TryParse(Request.QueryString["customer_id"].ToString(), out customer_id)) lb_cusid.Text = customer_id.ToString();
        }
        
    }




    private void readdata()
    {
        SqlCommand cmd = new SqlCommand();

        cmd.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
        cmd.CommandText = @"Select top 1 A.* , B.supplier_name, C.station_name
                                from  tbCustomers A with(nolock) 
                                left join tbSuppliers B with(nolock) on A.supplier_code = B.supplier_code 
                                left join tbStation C with(nolock) on A.supplier_code = C.station_code 
                              
                                where customer_id = @customer_id";
        string sScript = "";
        DateTime dt_temp = new DateTime();
        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["pallet_exclusive_send_station"].ToString() == "0")
                    shippingAddress.Checked = true;
                else if (dt.Rows[0]["pallet_exclusive_send_station"].ToString() != "0" && dt.Rows[0]["pallet_exclusive_send_station"].ToString() != null)
                {
                    exclusiveSendStation.Checked = true;
                    supplier.SelectedValue = dt.Rows[0]["pallet_exclusive_send_station"].ToString();
                }
                master_code.Text = dt.Rows[0]["master_code"].ToString();
                second_code_1.Text = dt.Rows[0]["second_id"].ToString();
                second_code_2.Text = dt.Rows[0]["pricing_code"].ToString();
                customer_name.Text = dt.Rows[0]["customer_name"].ToString();   //客戶名稱
                shortname.Text = dt.Rows[0]["customer_shortname"].ToString();  //客戶簡稱
                uni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();    //統一編號
                principal.Text = dt.Rows[0]["shipments_principal"].ToString(); //對帳人
                tel.Text = dt.Rows[0]["telephone"].ToString();  //電話
                fax.Text = dt.Rows[0]["fax"].ToString();        //傳真
                product_type.Text = dt.Rows[0]["product_type"].ToString(); //商品類別
                ddlMasterCustomerCode.Text = dt.Rows[0]["MasterCustomerCode"].ToString(); //母客代
                if (Less_than_truckload == "1")
                {
                    bank.SelectedValue = dt.Rows[0]["bank"].ToString();     //銀行
                    bank_branch.Text = dt.Rows[0]["bank_branch"].ToString();     //分行
                    bank_account.Text = dt.Rows[0]["bank_account"].ToString();     //帳號
                }


                using (SqlCommand cmd_ser = new SqlCommand())
                {
                    DataTable ser = new DataTable();


                    cmd_ser.Parameters.AddWithValue("@customer_code", dt.Rows[0]["customer_code"].ToString());
                    cmd_ser.CommandText = " SELECT begin_number,end_number from tbDeliveryNumberSetting where customer_code=@customer_code and IsActive=1 order by begin_number";
                    ser = dbAdapter.getDataTable(cmd_ser);
                    if (ser.Rows.Count > 0)
                    {
                        for (int i = 0; i < ser.Rows.Count; i++)
                        {
                            lbNumberRange.Text += ser.Rows[i]["begin_number"].ToString() + " - " + ser.Rows[i]["end_number"].ToString() + "<BR>";        //貨號區間
                        }
                    }
                    else
                    {
                        using (SqlCommand cmd_bag = new SqlCommand())
                        {
                            cmd_bag.Parameters.AddWithValue("@customer_code", dt.Rows[0]["customer_code"].ToString());
                            cmd_bag.CommandText = " SELECT begin_number,end_number from tbDeliveryNumberOnceSetting where customer_code=@customer_code and IsActive=1 order by begin_number";
                            DataTable dt_bag = dbAdapter.getDataTable(cmd_bag);
                            if (dt_bag.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt_bag.Rows.Count; i++)
                                {
                                    lbNumberRange.Text += dt_bag.Rows[i]["begin_number"].ToString() + " - " + dt_bag.Rows[i]["end_number"].ToString() + "<BR>";        //貨號區間
                                }
                            }
                            else
                            {
                                lbNumberRange.Text = "未設定";
                            }
                        }

                    }
                }


                City.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                City_SelectedIndexChanged(null, null);
                CityArea.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();   //出貨地址-路街巷弄號

                InvoiceCity.SelectedValue = dt.Rows[0]["invoice_city"].ToString().Trim();   //發票地址-縣市
                InvoiceCity_SelectedIndexChanged(null, null);
                InvoiceArea.SelectedValue = dt.Rows[0]["invoice_area"].ToString().Trim();   //發票地址-鄉鎮市區
                InvoiceAddress.Text = dt.Rows[0]["invoice_road"].ToString().Trim();   //發票地址-路街巷弄號

                email.Text = dt.Rows[0]["shipments_email"].ToString().Trim();    //出貨人電子郵件
                SPM_code.SelectedValue = dt.Rows[0]["pricing_code"].ToString().Trim();      //計價模式
                                                                                            //dlLT_fee.SelectedValue = dt.Rows[0]["Less_than_truckload_fee"] != DBNull.Value ? dt.Rows[0]["Less_than_truckload_fee"].ToString() : "";  //零擔運價

                supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();       //負責區配商
                stop_code.SelectedValue = dt.Rows[0]["stop_shipping_code"].ToString().Trim();     //停運原因代碼  0正常、1同業競爭、2公司倒閉、3呆帳、4其他原因
                stop_memo.Text = dt.Rows[0]["stop_shipping_memo"].ToString().Trim();       //停運原因備註
                                                                                           //customer_hcode.Text = dt.Rows[0]["customer_hcode"].ToString().Trim();    //竹運客代

                if (Less_than_truckload == "1")
                {
                    SqlCommand cmdsd = new SqlCommand();
                    string stationsd = supplier_code.Text.ToString();
                    cmdsd.Parameters.AddWithValue("@station_code", stationsd);
                    cmdsd.CommandText = "select * from tbStation where station_code = @station_code";
                    var tempsd = dbAdapter.getDataTable(cmdsd);
                    int temp_try_sd;
                    if (int.TryParse(tempsd.Rows[0]["sd_range_start"].ToString(), out temp_try_sd) & int.TryParse(tempsd.Rows[0]["sd_range_end"].ToString(), out temp_try_sd))
                    {
                        int startsd = int.Parse(tempsd.Rows[0]["sd_range_start"].ToString());
                        int endsd = int.Parse(tempsd.Rows[0]["sd_range_end"].ToString());
                        for (var i = startsd; i < endsd + 1; i++)
                        {
                            DropDownListSD.Items.Add(i.ToString());
                        }
                        DropDownListSD.Items.Insert(0, new ListItem("請選擇SD"));
                    }
                    else
                    {
                        DropDownListSD.Items.Add("請先至站所維護設定SD區間再選擇指派SD");
                    }
                    if (DropDownListSD.SelectedValue != "請選擇SD")
                    {
                        DropDownListSD.SelectedValue = dt.Rows[0]["assigned_sd"].ToString().Trim();
                    }

                    SqlCommand cmdmd = new SqlCommand();
                    string stationmd = supplier_code.Text.ToString();
                    cmdmd.Parameters.AddWithValue("@station_code", stationmd);
                    cmdmd.CommandText = "select * from tbStation where station_code = @station_code";
                    var tempmd = dbAdapter.getDataTable(cmdmd);
                    int temp_try_md;
                    if (int.TryParse(tempmd.Rows[0]["md_range_start"].ToString(), out temp_try_md) & int.TryParse(tempmd.Rows[0]["md_range_end"].ToString(), out temp_try_md))
                    {
                        int startmd = int.Parse(tempmd.Rows[0]["md_range_start"].ToString());
                        int endmd = int.Parse(tempmd.Rows[0]["md_range_end"].ToString());
                        for (var i = startmd; i < endmd + 1; i++)
                        {
                            DropDownListMD.Items.Add(i.ToString());
                        }
                        DropDownListMD.Items.Insert(0, new ListItem("請選擇MD"));
                    }
                    else
                    {
                        DropDownListMD.Items.Add("請先至站所維護設定MD區間再選擇指派MD");
                    }
                    
                    if (DropDownListMD.SelectedValue != "請選擇MD")
                    {
                        DropDownListMD.SelectedValue = dt.Rows[0]["assigned_md"].ToString().Trim();
                    }
                }
                else
                {
                    UpdatePanel2.Visible = false;
                    UpdatePanel3.Visible = false;
                }

                //合約生效日
                if (dt.Rows[0]["contract_effect_date"] != DBNull.Value &&
                    (((DateTime)dt.Rows[0]["contract_effect_date"]) > Convert.ToDateTime("1900/01/01")) &&
                    (((DateTime)dt.Rows[0]["contract_effect_date"]) < Convert.ToDateTime("9999/12/31")))
                {
                    contract_sdate.Text = ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd");
                }

                //合約到期日
                if (dt.Rows[0]["contract_expired_date"] != DBNull.Value &&
                    (((DateTime)dt.Rows[0]["contract_expired_date"]) > Convert.ToDateTime("1900/01/01")) &&
                    (((DateTime)dt.Rows[0]["contract_expired_date"]) < Convert.ToDateTime("9999/12/31")))
                {
                    contract_edate.Text = ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd");
                }

                filename.Text = dt.Rows[0]["contract_content"].ToString().Trim();  //合約內容  
                if (filename.Text != "") sScript += "$('#hlFileView').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename.Text + "');";


                //結帳日
                if (dt.Rows[0]["checkout_date"] != DBNull.Value)
                {
                    if (close_date.Items.FindByValue(dt.Rows[0]["checkout_date"].ToString()) == null)
                    {
                        close_date.SelectedValue = "-1";
                        tbclose.Text = dt.Rows[0]["checkout_date"].ToString();
                        sScript += "$('#" + tbclose.ClientID + "').show();";
                    }
                    else
                    {
                        close_date.SelectedValue = dt.Rows[0]["checkout_date"].ToString();
                    }
                }
                else
                {
                    close_date.SelectedValue = "";
                    tbclose.Text = "";
                    sScript += "$('#" + tbclose.ClientID + "').hide();";
                }
                //feename.Text = dt.Rows[0]["tariffs_table"].ToString().Trim();                        //運價表
                //sScript += "$('#hlFeeView').attr('href','" + "http://" + Request.Url.Authority + "/files/fee/" + feename.Text + "');";

                business_people.Text = dt.Rows[0]["business_people"].ToString().Trim();              //營業人員
                ddl_sales_id.SelectedValue = dt.Rows[0]["sales_id"].ToString();                       //記績營業員
                ddl_sale_station.SelectedValue = dt.Rows[0]["sale_station"].ToString();                      //計績站所
                billing_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString().Trim();  //帳單特殊需求
                contract_price.Text = dt.Rows[0]["contract_price"].ToString().Trim();                //發包價
                is_weekend_delivered.Checked = dt.Rows[0]["is_weekend_delivered"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["is_weekend_delivered"]) : false; //是否啟用假日配
                return_address_chk.Checked = dt.Rows[0]["return_address_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["return_address_flag"]) : false;   //是否依照寄件地址退貨
                weekend_delivery_fee.Text = dt.Rows[0]["weekend_delivery_fee"].ToString().Trim();    //假日配加價 
                is_new_customer.Checked = dt.Rows[0]["is_new_customer"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["is_new_customer"]) : false; //是否為新客戶
                OldCustomerOverCBM.Checked = dt.Rows[0]["OldCustomerOverCBM"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["OldCustomerOverCBM"]) : false; //是否為舊制超材費
                                                                                                                                                             //票期
                if (dt.Rows[0]["ticket_period"] != DBNull.Value)
                {
                    if (ticket_period.Items.FindByValue(dt.Rows[0]["ticket_period"].ToString()) == null)
                    {
                        ticket_period.SelectedValue = "-1";
                        ticket.Text = dt.Rows[0]["ticket_period"].ToString();
                        sScript += "$('#" + ticket.ClientID + "').show();";
                    }
                    else
                    {
                        ticket_period.SelectedValue = dt.Rows[0]["ticket_period"].ToString();
                    }
                }
                else
                {
                    ticket_period.SelectedValue = "";
                    ticket.Text = "";
                    sScript += "$('#" + ticket.ClientID + "').hide();";
                }

                //運價生效日
                string str_customer_code = master_code.Text + second_code_1.Text + second_code_2.Text;
                lbl_PriceTip.Text = "";
                fee_sdate.Text = "※目前採用公版運價";
                if (str_customer_code.Length > 0)
                {



                    using (SqlCommand cmd2 = new SqlCommand())
                    {
                        cmd2.CommandText = "usp_GetTodayPriceBusDefLog";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@customer_code", str_customer_code);
                        cmd2.Parameters.AddWithValue("@return_type", "1");
                        using (DataTable the_dt = dbAdapter.getDataTable(cmd2))
                        {
                            if (the_dt.Rows.Count > 0)
                            {
                                if (DateTime.TryParse(the_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                                {
                                    fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                                }

                                int i_temp;
                                if (!int.TryParse(the_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                                lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                            }
                        }
                    }
                }


                int i_customer_type = 0;
                if (!int.TryParse(dt.Rows[0]["customer_type"].ToString(), out i_customer_type)) i_customer_type = 0;
                //if (i_customer_type == 3 && !(manager_type =="3" || manager_type == "5")) //自營才開放價格設定
                //{
                //    pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(master_code.Text + second_code_1.Text + second_code_2.Text, Request.QueryString["customer_id"])));
                //    pan_price_list.Visible = true;
                //}
                //else
                //{
                //    pan_price_list.Visible = false;
                //}

                //2018/12/04 品翰:開放所有都可以上傳自訂運價



                pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(master_code.Text + second_code_1.Text + second_code_2.Text, Request.QueryString["customer_id"])));

                pan_price_list.Visible = true;


                individual_fee.Checked = dt.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["individual_fee"]) : false;        //是否個別計價
                if (individual_fee.Checked)
                {
                    tariffd_table.Style.Add("display", "none");
                    pan_price_list.Style.Add("display", "none");
                }

                switch (Convert.ToInt16(dt.Rows[0]["type"]))
                {
                    case 0:
                        #region 棧板
                        supplier_name.Text = dt.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱 
                                                                                                //CreateLineChart(dt.Rows[0]["customer_code"].ToString());   //結帳趨勢圖限開放棧板
                                                                                                //LineChart.Visible = true;
                        lisupplier_code.Text = "負責區配商";
                        div_range.Visible = false;
                        div_LT_fee.Visible = false;

                        #endregion

                        break;
                    case 1:
                        #region 零擔
                        supplier_name.Text = dt.Rows[0]["station_name"].ToString().Trim();     //負責站所名稱 
                                                                                               //LineChart.Visible = false;
                        supplier.Visible = false;
                        shippingAddress.Visible = false;
                        exclusiveSendStation.Visible = false;
                        lisupplier_code.Text = "站　　　所";
                        div_range.Visible = true;
                        div_LT_fee.Visible = true;
                        using (SqlCommand cmdrange = new SqlCommand())
                        {
                            cmdrange.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
                            cmdrange.CommandText = @"Select top 1 A.* , B.supplier_name, C.station_name  
                                from  tbCustomers A with(nolock) 
                                left join tbSuppliers B with(nolock) on A.supplier_code = B.supplier_code 
                                left join tbStation C with(nolock) on A.supplier_code = C.station_code 
                                where customer_id = @customer_id";
                        }
                        Callprice();
                        #endregion

                        break;
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
                return;
            }

        }
        cmd.Dispose();
    }

    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void InvoiceCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (InvoiceCity.SelectedValue != "")
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", InvoiceCity.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    InvoiceArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    /*protected void SD_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownListSD.SelectedValue != "")
        {   
            string stationCode = supplier_code.Text.ToString();

            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@station_code", stationCode);
            cmda.CommandText = "Select sd_range_start,sd_range_end from tbStation where station_code=@station_code";
            dta = dbAdapter.getDataTable(cmda);

            if (dta.Rows[0]["sd_range_start"].ToString() != "" & dta.Rows[0]["sd_range_end"].ToString() != "")
            {
                for (var i = int.Parse(dta.Rows[0]["sd_range_start"].ToString()); i < int.Parse(dta.Rows[0]["sd_range_end"].ToString()) + 1; i++)
                {
                    DropDownListSD.Items.Add(i.ToString());
                }
            }
        }
        else 
        {
            DropDownListSD.Items.Clear();
            DropDownListSD.Items.Add(new ListItem("請選擇", ""));
        }
    }*/

    protected void insertSameAddress(object sender, EventArgs e)
    {
        InvoiceCity.SelectedIndex = City.SelectedIndex;
        InvoiceCity_SelectedIndexChanged(sender, e);
        InvoiceArea.SelectedIndex = CityArea.SelectedIndex;
        InvoiceAddress.Text = address.Text;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        SqlCommand cmdA = new SqlCommand();
        SqlCommand cmdfse01 = new SqlCommand();
        DateTime dt_temp = new DateTime();
        int i_tmp;
        int sd_md_temp;
        #region 修改 
        SqlCommand change_holiday_fee_command = new SqlCommand();
        change_holiday_fee_command.Parameters.AddWithValue("@holiday", weekend_delivery_fee.Text);
        change_holiday_fee_command.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_code", (master_code.Text.ToString() + second_code_1.Text.ToString() + second_code_2.Text.ToString()));
        change_holiday_fee_command.CommandText = dbAdapter.genUpdateComm("CustomerShippingFee", change_holiday_fee_command);   //修改
        dbAdapter.execNonQuery(change_holiday_fee_command);

        SqlCommand command = new SqlCommand();
        command.Parameters.AddWithValue("@user_email", email.Text.ToString());
        command.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_code", (master_code.Text.ToString() + second_code_1.Text.ToString() + second_code_2.Text.ToString()));
        command.CommandText = dbAdapter.genUpdateComm("tbAccounts", command);   //修改
        dbAdapter.execNonQuery(command);

        SqlCommand cmd = new SqlCommand();
        cmdfse01.Parameters.AddWithValue("@CUST_CODE", (master_code.Text.ToString() + second_code_1.Text.ToString() + second_code_2.Text.ToString()));
        cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());        //客戶名稱
        cmdfse01.Parameters.AddWithValue("@CHI_NAME", customer_name.Text.ToString());
        cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());       //客戶簡稱
        cmdfse01.Parameters.AddWithValue("@BRF_NAME", shortname.Text.ToString());
        //customer_type (1:區配 2:竹運 3:自營)
        //if (customer_hcode.Text != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別
        //}
        if (master_code.Text.Substring(3, 4) == "0000")
        {
            cmd.Parameters.AddWithValue("@customer_type", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer_type", "3");
        }
        cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
        cmdfse01.Parameters.AddWithValue("@CUST_ID", uni_number.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
        cmdfse01.Parameters.AddWithValue("@CONTACT", principal.Text.ToString());
        cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
        cmdfse01.Parameters.AddWithValue("@TEL_NO1", tel.Text.ToString());
        cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
        cmdfse01.Parameters.AddWithValue("@FAX_NO", fax.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
        cmdfse01.Parameters.AddWithValue("@CHI_CITY", City.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
        cmdfse01.Parameters.AddWithValue("@CHI_AREA", CityArea.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
        cmdfse01.Parameters.AddWithValue("@CHI_ADDR", address.Text.ToString());
        cmd.Parameters.AddWithValue("@invoice_city", InvoiceCity.SelectedValue.ToString());       //發票地址-縣市
        cmdfse01.Parameters.AddWithValue("@INV_CITY", InvoiceCity.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@invoice_area", InvoiceArea.SelectedValue.ToString());   //發票地址-鄉鎮市區
        cmdfse01.Parameters.AddWithValue("@INV_AREA", InvoiceArea.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@invoice_road", InvoiceAddress.Text.ToString());             //發票地址-路街巷弄號
        cmdfse01.Parameters.AddWithValue("@INV_ADDR", InvoiceAddress.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
        cmdfse01.Parameters.AddWithValue("@EMAIL_LIST", email.Text.ToString());
        cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
        if (stop_code.SelectedValue.ToString() == "0")                                        //啟用or停用
        {
            cmdfse01.Parameters.AddWithValue("@ACT_FG", "Y");
        }
        else
        {
            cmdfse01.Parameters.AddWithValue("@ACT_FG", "N");
        }
        cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
        cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
        cmd.Parameters.AddWithValue("@product_type", product_type.Text.ToString());          //商品類型
        cmd.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());  //母客代
        SqlCommand cmdM = new SqlCommand();
        DataTable dtM = new DataTable();
        cmdM.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());
        cmdM.CommandText = "select top 1  customer_name from tbCustomers where customer_code=@MasterCustomerCode";
        dtM = dbAdapter.getDataTable(cmdM);
        if (dtM.Rows.Count > 0)
        {
            cmd.Parameters.AddWithValue("@MasterCustomerName", dtM.Rows[0]["customer_name"].ToString());  //母客代名稱
        }
        cmd.Parameters.AddWithValue("@is_weekend_delivered", is_weekend_delivered.Checked);  //是否啟用假日配送
        cmd.Parameters.AddWithValue("@weekend_delivery_fee", weekend_delivery_fee.Text);     //假日配送加價
        cmd.Parameters.AddWithValue("@bank", bank.SelectedValue.ToString());                 //銀行
        cmd.Parameters.AddWithValue("@is_new_customer", is_new_customer.Checked);   //是否為新客戶
        cmd.Parameters.AddWithValue("@OldCustomerOverCBM", OldCustomerOverCBM.Checked);   //是否為舊制超材費
        if (bank.SelectedValue != "")
        {
            cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString().Substring(0, 3));
            cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString().Substring(bank.SelectedValue.IndexOf(' ') + 1));
        }
        else
        {
            cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString());
            cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString());
        }

        if (Less_than_truckload == "1")
        {
            cmd.Parameters.AddWithValue("@bank_branch", bank_branch.Text.ToString());            //分行
            cmdfse01.Parameters.AddWithValue("@BANK_BRANCH", bank_branch.Text.ToString());
            cmd.Parameters.AddWithValue("@bank_account", bank_account.Text.ToString());           //帳號
            cmdfse01.Parameters.AddWithValue("@BANK_ACCT", bank_account.Text.ToString());
            cmd.Parameters.AddWithValue("@return_address_flag", return_address_chk.Checked);       //是否依照寄件地址退貨
            cmdfse01.Parameters.AddWithValue("@RETURN_ADDRESS_FLAG", return_address_chk.Checked);
        }
        if (exclusiveSendStation.Checked == true && supplier.SelectedValue.ToString().Length > 0)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", supplier.SelectedValue.ToString()); //棧板綁定專屬發送站
        if (shippingAddress.Checked == true)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", "0"); //棧板發送站由出貨地址判斷

        if (int.TryParse(DropDownListSD.Text.ToString(), out sd_md_temp))
        {
            cmd.Parameters.AddWithValue("@assigned_sd", DropDownListSD.Text.ToString());   //SD
        }
        else
        {
            cmd.Parameters.AddWithValue("@assigned_sd", DBNull.Value);
        }

        if (int.TryParse(DropDownListMD.Text.ToString(), out sd_md_temp))
        {
            cmd.Parameters.AddWithValue("@assigned_md", DropDownListMD.Text.ToString());   //MD
        }
        else
        {
            cmd.Parameters.AddWithValue("@assigned_md", DBNull.Value);
        }

        //合約生效日
        if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", contract_sdate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", DBNull.Value);
        }

        //合約到期日
        if (DateTime.TryParse(contract_edate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", contract_edate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", DBNull.Value);
        }

        //票期
        if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(ticket.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
            }

        }
        else
        {
            cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
        }

        //結帳日
        if (int.TryParse(close_date.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(tbclose.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
        }

        cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員

        cmd.Parameters.AddWithValue("@sale_station", ddl_sale_station.SelectedValue.ToString());    //記績站所
        cmd.Parameters.AddWithValue("@sale_station_udate", DateTime.Now);    //記績站所更新時間

        SqlCommand cmds = new SqlCommand();
        cmds.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
        cmds.CommandText = @"Select top 1 sales_id_cdate,sales_id
                                from  tbCustomers  with(nolock)                                                            
                                where customer_id = @customer_id";
        using (DataTable dts = dbAdapter.getDataTable(cmds))
        {
            if (string.IsNullOrEmpty(dts.Rows[0]["sales_id_cdate"].ToString()))    //資料庫寫入時間為空時,寫入sales_id_cdate
            {
                cmd.Parameters.AddWithValue("@sales_id_cdate", DateTime.Now);  //首次填入記績營業員時間
                cmd.Parameters.AddWithValue("@sales_id_udate", DateTime.Now);
            }
            else if (dts.Rows[0]["sales_id"].ToString() != ddl_sales_id.SelectedValue)
            {

                cmd.Parameters.AddWithValue("@sales_id_udate", DateTime.Now);  //紀錄記績營業員更新時間
            }
            else { }
        }
        cmd.Parameters.AddWithValue("@sales_id", ddl_sales_id.SelectedValue);



        //cmd.Parameters.AddWithValue("@customer_hcode", customer_hcode.Text.ToString());      //竹運客代
        //cmd.Parameters.AddWithValue("@contact_1", contact_1.Text.ToString());                //竹運-聯絡人1  
        //cmd.Parameters.AddWithValue("@email_1", email_1.Text.ToString());                    //竹運-聯絡人1 email
        //cmd.Parameters.AddWithValue("@contact_2", contact_2.Text.ToString());                //竹運-聯絡人2
        //cmd.Parameters.AddWithValue("@email_2", email_2.Text.ToString());                    //竹運-聯絡人2 email
        cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求  
        cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                  //發包價(C段)
        cmd.Parameters.AddWithValue("@individual_fee", individual_fee.Checked);               //是否個別計價  
        cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                       //更新人員
        cmdfse01.Parameters.AddWithValue("@EDIT_USER", Session["account_code"]);
        cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                  //更新時間
        cmdfse01.Parameters.AddWithValue("@EDIT_TIME", DateTime.Now);
        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", Request.QueryString["customer_id"].ToString());
        cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);   //修改
        dbAdapter.execNonQuery(cmd);
        cmdfse01.CommandText = @"
update DT_CUST set  CUST_ID=@CUST_ID,CHI_NAME=@CHI_NAME,BRF_NAME=@BRF_NAME,CHI_CITY=@CHI_CITY,CHI_AREA=@CHI_AREA,CHI_ADDR=@CHI_ADDR,INV_CITY=@INV_CITY,INV_AREA=@INV_AREA,INV_ADDR=@INV_ADDR,CONTACT=@CONTACT,TEL_NO1=@TEL_NO1,FAX_NO=@FAX_NO,EMAIL_LIST=@EMAIL_LIST,BANK_ID=@BANK_ID,BANK_NAME=@BANK_NAME,BANK_BRANCH=@BANK_BRANCH,BANK_ACCT=@BANK_ACCT,ACT_FG=@ACT_FG,EDIT_USER=@EDIT_USER,EDIT_TIME=@EDIT_TIME,CONTRACT_EFFECT_DATE=@CONTRACT_EFFECT_DATE,CONTRACT_EXPIRED_DATE=@CONTRACT_EXPIRED_DATE ,RETURN_ADDRESS_FLAG=@RETURN_ADDRESS_FLAG

where CUST_CODE=@CUST_CODE";
        dbAdapter.execNonQueryForFSE01(cmdfse01);
        #endregion




        if (Less_than_truckload == "1")
        {
            //客戶代收


            //刪除原本的代收資料
            using (SqlCommand cmd_del = new SqlCommand())
            {
                cmd_del.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());

                cmd_del.CommandText = @"delete from tbMemberCollectionPriceList where customer_code in(select customer_code from tbCustomers where customer_id=@customer_id)";
                try
                {
                    dbAdapter.execNonQuery(cmd_del);
                }
                catch (Exception ex)
                {

                }

            }
            //刪除原本的代收資料

            //寫入新的代收資料
            String SQLstr;
            SQLstr = @"Select seq from  tbMemberCollectionPrice order by Min";
            cmd.Parameters.Clear();
            cmd.CommandText = SQLstr;
            DataTable dt = null;
            dt = dbAdapter.getDataTable(cmd);



            if (dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string hand_fee = this.Request.Form["A_fee_" + dt.Rows[i]["seq"].ToString()];
                    string ord_fee = this.Request.Form["B_fee_" + dt.Rows[i]["seq"].ToString()];
                    if (hand_fee != "" || ord_fee != "")
                    {
                        cmdA.Parameters.Clear();
                        cmdA.Parameters.AddWithValue("@Handling_fee", hand_fee);
                        cmdA.Parameters.AddWithValue("@order_fee", ord_fee);

                        using (SqlCommand cmd_ser = new SqlCommand())
                        {
                            DataTable ser = new DataTable();


                            cmd_ser.Parameters.AddWithValue("@Customer_id", Request.QueryString["customer_id"].ToString());
                            cmd_ser.CommandText = " SELECT Top 1 customer_code from tbCustomers where Customer_id=@Customer_id";
                            ser = dbAdapter.getDataTable(cmd_ser);
                            cmdA.Parameters.AddWithValue("@customer_code", ser.Rows[0]["customer_code"].ToString());

                        }


                        cmdA.Parameters.AddWithValue("@CollectionPrice", dt.Rows[i]["seq"].ToString());
                        cmdA.CommandText = dbAdapter.genInsertComm("tbMemberCollectionPriceList", true, cmdA);
                        int result = 0;
                        if (!int.TryParse(dbAdapter.getDataTable(cmdA).Rows[0][0].ToString(), out result)) result = 0;

                    }


                }
            }
            //寫入新的代收資料
        }


        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('修改完成');</script>", false);
        return;
    }

    //折線圖
    //private void CreateLineChart()
    //{
    //    //sqlstr = "SELECT DATEPART(month,OrderDate) as month,count(DATEPART(month,OrderDate)) as count FROM Orders WHERE DATEPART(year,OrderDate) ='1996' GROUP BY DATEPART(month,OrderDate) order by month";
    //    //DataTableReader reader = GetDTConn(sqlstr).CreateDataReader();

    //    DataTable table = new DataTable();

    //    // Create two columns, ID and Name.
    //    DataColumn idColumn = table.Columns.Add("month", typeof(int));
    //    table.Columns.Add("amt", typeof(int));

    //    // Set the ID column as the primary key column.
    //    table.PrimaryKey = new DataColumn[] { idColumn };

    //    table.Rows.Add(new object[] { 1, 100000 });
    //    table.Rows.Add(new object[] { 2, 161711 });
    //    table.Rows.Add(new object[] { 3, 81935 });
    //    table.Rows.Add(new object[] { 4, 293749 });
    //    table.Rows.Add(new object[] { 5, 172683 });
    //    table.Rows.Add(new object[] { 6, 154272 });
    //    table.Rows.Add(new object[] { 7, 283656 });
    //    table.Rows.Add(new object[] { 8, 121738 });
    //    table.Rows.Add(new object[] { 9, 234165 });
    //    table.Rows.Add(new object[] { 10, 183232 });
    //    table.Rows.Add(new object[] { 11, 127121 });
    //    table.Rows.Add(new object[] { 12, 319171 });



    //    DataTableReader reader = new DataTableReader(
    //           new DataTable[] { table });

    //    //將讀取資料加入至繪圖圖表中
    //    LineChart.Series["Line"].Points.DataBindXY(reader, "month", reader, "amt");



    //    //設定圖形X軸的顯示文字
    //    foreach (DataPoint dp in LineChart.Series["Line"].Points)
    //    {
    //        dp.AxisLabel = dp.XValue + "月";
    //    }

    //    //繪製的樣式
    //    LineChart.Series["Line"]["DrawingStyle"] = "Cylinder";

    //    //線條粗細
    //    LineChart.Series["Line"]["PointWidth"] = "0.5";

    //    //設定顯示圖形的樣式類別
    //    LineChart.Series["Line"].ChartType = SeriesChartType.Line;

    //    //設定圖例顯示的文字
    //    LineChart.Series["Line"].LegendText = "金額";
    //    LineChart.ChartAreas["LineChartArea"].AxisX.LabelStyle.IsStaggered = true;
    //    LineChart.ChartAreas["LineChartArea"].AxisX.IsLabelAutoFit = false;



    //    //DataTable table2 = new DataTable();
    //    //DataColumn idColumn2 = table2.Columns.Add("month", typeof(int));
    //    //table2.Columns.Add("amt", typeof(int));
    //    //table2.PrimaryKey = new DataColumn[] { idColumn2 };
    //    //table2.Rows.Add(new object[] { 1, 10000 });
    //    //table2.Rows.Add(new object[] { 2, 20000 });
    //    //table2.Rows.Add(new object[] { 3, 310000 });
    //    //table2.Rows.Add(new object[] { 4, 70000 });
    //    //table2.Rows.Add(new object[] { 5, 20000 });
    //    //table2.Rows.Add(new object[] { 6, 80000 });
    //    //table2.Rows.Add(new object[] { 7, 60000 });
    //    //table2.Rows.Add(new object[] { 8, 110000 });
    //    //table2.Rows.Add(new object[] { 9, 90000 });
    //    //table2.Rows.Add(new object[] { 10, 80000 });
    //    //table2.Rows.Add(new object[] { 11, 40000 });
    //    //table2.Rows.Add(new object[] { 12, 90000 });

    //    //DataTableReader reader2 = new DataTableReader(
    //    //      new DataTable[] { table2 });
    //    ////將讀取資料加入至繪圖圖表中
    //    //LineChart.Series["Line2"].Points.DataBindXY(reader2, "month", reader2, "amt");

    //    ////設定圖形X軸的顯示文字
    //    //foreach (DataPoint dp in LineChart.Series["Line2"].Points)
    //    //{
    //    //    dp.AxisLabel = dp.XValue + "月";
    //    //}

    //    ////繪製的樣式
    //    //LineChart.Series["Line2"]["DrawingStyle"] = "Cylinder";

    //    ////線條粗細
    //    //LineChart.Series["Line2"]["PointWidth"] = "0.5";

    //    ////設定顯示圖形的樣式類別
    //    //LineChart.Series["Line2"].ChartType = SeriesChartType.Line;

    //    ////設定圖例顯示的文字
    //    //LineChart.Series["Line2"].LegendText = "件數";


    //}

    //public DataTable CreateDS(string customer_code)
    //{
    //    DataTable dt = null;
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.Parameters.AddWithValue("@customer_code", customer_code);
    //        cmd.Parameters.AddWithValue("@sdate", DateTime.Today.AddMonths(0).ToString("yyyy-MM-01"));
    //        cmd.Parameters.AddWithValue("@edate", DateTime.Today.AddDays(1).ToString("yyyy-MM-dd"));
    //        string strSQL = @"Select convert(varchar(7), A.print_date, 126) 'month',  sum( ISNULL(_fee.supplier_fee,0)) 'supplier_fee' 
    //                            from tcDeliveryRequests  A with(nolock) 
    //                            CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
    //                            where A.customer_code = @customer_code and A.print_date >= @sdate and A.print_date < @edate
    //                            AND A.cancel_date IS NULL
    //                            group by  convert(varchar(7), A.print_date, 126)";
    //        cmd.CommandText = strSQL;
    //        cmd.CommandType = CommandType.Text;
    //        dt = dbAdapter.getDataTable(cmd);
    //    }
    //    return dt;
    //}

    //private void CreateLineChart(string customer_code)
    //{
    //    //sqlstr = "SELECT DATEPART(month,OrderDate) as month,count(DATEPART(month,OrderDate)) as count FROM Orders WHERE DATEPART(year,OrderDate) ='1996' GROUP BY DATEPART(month,OrderDate) order by month";
    //    //DataTableReader reader = GetDTConn(sqlstr).CreateDataReader();
    //    DataTable dt_Chart = CreateDS(customer_code);
    //    LineChart.DataSource = dt_Chart;


    //    DataTableReader reader = new DataTableReader(
    //           new DataTable[] { dt_Chart });
    //    //將讀取資料加入至繪圖圖表中
    //    LineChart.Series["Line"].Points.DataBindXY(reader, "month", reader, "supplier_fee");

    //    //設定圖形X軸的顯示文字
    //    //foreach (DataPoint dp in LineChart.Series["Line"].Points)
    //    //{
    //    //    dp.AxisLabel = dp.XValue + "月";
    //    //}

    //    //繪製的樣式
    //    LineChart.Series["Line"]["DrawingStyle"] = "Cylinder";

    //    //線條粗細
    //    LineChart.Series["Line"]["PointWidth"] = "1";

    //    //設定顯示圖形的樣式類別
    //    LineChart.Series["Line"].ChartType = SeriesChartType.Line;

    //    //設定圖例顯示的文字
    //    LineChart.Series["Line"].LegendText = "金額";
    //    LineChart.ChartAreas["LineChartArea"].AxisX.LabelStyle.IsStaggered = true;
    //    LineChart.ChartAreas["LineChartArea"].AxisX.IsLabelAutoFit = false;


    //}

    /// <summary>回傳訊息至前端 </summary>
    /// <param name="strMsg">訊息內容</param>
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            return;

        }

    }




    /// <summary>
    /// 刪除-自營客戶 自訂價格記綠
    /// </summary>
    /// <param name="pid">ttPriceBusinessDefineLog.pid</param>
    /// <param name="type">判斷是棧板還是零擔</param>
    /// <returns>1:執行成功 0:未執行 -1:錯誤</returns>
    [WebMethod]
    public static string PriceDel(string pid, string type)
    {
        int i_log_id = 0;
        if (!int.TryParse(pid, out i_log_id)) i_log_id = 0;

        string str_result = "資訊異常，請重新確認!";
        if (i_log_id > 0)
        {
            /********************************************************************
                 * !刪除: 已使用過的規則 及 正在使用中的規則 皆不可刪 (防呆)
                 *  刪除: 可針對未使用規則進行刪除指令
                 *        公版 - ttPriceBusinessDefineLog
                 *        自訂 - ttPriceBusinessDefineLog 及 tbPriceBusinessDefine
              ********************************************************************/
            str_result = "1";
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {


                    if (type == "0")
                    {
                        String strSQL = @"DECLARE @tariffs_type CHAR(1)
                                      DECLARE @customer_code_this NVARCHAR(10)
                                      DECLARE @tariffs_effect_date DATETIME
                                      DECLARE @tariffs_effect_date_now DATETIME
                                      DECLARE @checkout_close_date_now DATETIME --最新結帳日
                                      
                                      SELECT @tariffs_type = tariffs_type,@customer_code_this = customer_code,@tariffs_effect_date= tariffs_effect_date
                                       FROM　ttPriceBusinessDefineLog WHERE log_id=@log_id
                                      
                                      SELECT @checkout_close_date_now=max(checkout_close_date)
                                       FROM tcDeliveryRequests With(Nolock) where customer_code=@customer_code_this

                                       DECLARE @t_temp TABLE (tariffs_effect_date_today Date )
                                      
                                       INSERT INTO @t_temp (tariffs_effect_date_today)
                                              EXEC usp_GetTodayPriceBusDefLog @customer_code = @customer_code_this;
                                            SELECT @tariffs_effect_date_now = tariffs_effect_date_today FROM @t_temp
                                      
                                      IF @tariffs_type IS NOT NULL AND @tariffs_type = 1
                                        BEGIN
                                           --select '公版'
                                      	   DELETE ttPriceBusinessDefineLog WHERE log_id = @log_id AND (tariffs_effect_date > @tariffs_effect_date_now OR @tariffs_effect_date_now IS NULL);
                                      
                                        END
                                      IF  @tariffs_type IS NOT NULL AND @tariffs_type = 2
                                         BEGIN
                                            --select '自訂'
                                      	    DELETE ttPriceBusinessDefineLog WHERE log_id = @log_id AND (tariffs_effect_date > @tariffs_effect_date_now OR @tariffs_effect_date_now IS NULL OR tariffs_effect_date>@checkout_close_date_now);
                                      	    DELETE tbPriceBusinessDefine 
                                             WHERE customer_code=@customer_code_this AND CONVERT(VARCHAR,active_date,111) = CONVERT(VARCHAR,@tariffs_effect_date,111) 
                                               AND (CONVERT(VARCHAR,active_date,111) > CONVERT(VARCHAR,@tariffs_effect_date_now,111) OR @tariffs_effect_date_now IS NULL);
                                         END ";

                        cmd.CommandText = strSQL;
                        cmd.Parameters.AddWithValue("@log_id", pid);
                        dbAdapter.execNonQuery(cmd);

                    }
                    else
                    {
                        String strSQL = @"DECLARE @tariffs_type CHAR(1)
                                      DECLARE @customer_code_this NVARCHAR(10)
                                      DECLARE @tariffs_effect_date DATETIME
                                      DECLARE @tariffs_effect_date_now DATETIME
                                      DECLARE @checkout_close_date_now DATETIME --最新結帳日
                                      
                                      SELECT @tariffs_type = tariffs_type,@customer_code_this = customer_code,@tariffs_effect_date= tariffs_effect_date
                                       FROM　ttPriceBusinessDefineLog WHERE log_id=@log_id
                                      
                                      SELECT @checkout_close_date_now=max(checkout_close_date)
                                       FROM tcDeliveryRequests With(Nolock) where customer_code=@customer_code_this

                                       DECLARE @t_temp TABLE (tariffs_effect_date_today Date )
                                      
                                       INSERT INTO @t_temp (tariffs_effect_date_today)
                                              EXEC usp_GetTodayPriceBusDefLog @customer_code = @customer_code_this;
                                            SELECT @tariffs_effect_date_now = tariffs_effect_date_today FROM @t_temp
                                      
                                      IF @tariffs_type IS NOT NULL AND @tariffs_type = 1
                                        BEGIN
                                           --select '公版'
                                      	   DELETE ttPriceBusinessDefineLog WHERE log_id = @log_id AND (tariffs_effect_date > @tariffs_effect_date_now OR @tariffs_effect_date_now IS NULL);
                                      
                                        END
                                      IF  @tariffs_type IS NOT NULL AND @tariffs_type = 2
                                         BEGIN
                                            --select '自訂'
                                          DELETE tbPriceLessThan   WHERE customer_code In(select customer_code from  [dbo].[ttPriceBusinessDefineLog]  where log_id =@log_id )  AND CONVERT(VARCHAR,Enable_date,111)in(select CONVERT(VARCHAR,tariffs_effect_date,111) from [dbo].ttPriceBusinessDefineLog where  log_id =@log_id);
                                      	  DELETE ttPriceBusinessDefineLog WHERE log_id = @log_id AND (tariffs_effect_date > @tariffs_effect_date_now OR @tariffs_effect_date_now IS NULL OR tariffs_effect_date>@checkout_close_date_now);
                                      	 
   
                                         END ";

                        //AND (CONVERT(VARCHAR,active_date,111) > CONVERT(VARCHAR,@tariffs_effect_date_now,111) OR @tariffs_effect_date_now IS NULL);

                        cmd.CommandText = strSQL;
                        cmd.Parameters.AddWithValue("@log_id", pid);
                        dbAdapter.execNonQuery(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                str_result = "刪除發生錯誤，請聯絡系統人員!  " + ex.Message.ToString();
            }
        }

        return str_result;
    }

    private void IdeChk(string Manager_type)
    {
        if (Manager_type == "2")
        {

            using (SqlCommand cmda = new SqlCommand())
            {
                DataTable dta = new DataTable();
                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmda.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
                cmda.CommandText = @"select top 1 * from tbCustomers A  with(nolock) 
                                    Left join  tbStation B with(nolock)  on A.supplier_code = B.station_code
                                    Left join  tbEmps C with(nolock)  on C.station = B.id
                                    Left join  tbAccounts D with(nolock)  on D.emp_code = C.emp_code
                                    Where D.account_code  =@account_code  And A.Customer_id=@Customer_id ";
                dta = dbAdapter.getDataTable(cmda);
                /*
                if (dta.Rows.Count == 0)
                {
                    RetuenMsg("該客戶並非屬於此站所管理!!");
                }
                */
            }
            customer_name.Enabled = false;//客戶名稱禁用
            uni_number.Enabled = false;//統一編號禁用
            shortname.Enabled = false;//客戶簡稱禁用
            tel.Enabled = false;//電話禁用
            ticket_period.Enabled = false;//票期禁用
            email.Enabled = false;//信箱禁用
            stop_code.Enabled = false;//停權原因禁用
            stop_memo.Enabled = false; //停權說明禁用
            business_people.Enabled = false;//營業人員禁用
            contract_edate.Enabled = false; //合約到期日禁用
            contract_sdate.Enabled = false; //合約生效日禁用
            close_date.Enabled = false; //結帳日禁用
            contract_price.Enabled = false; //發包價禁用
            individual_fee.Enabled = false; //獨立計價禁用
            billing_special_needs.Enabled = false; //
            buttonA.Visible = false; // 上傳新合約按鈕禁用
            buttonB.Visible = false; //上傳新運價按鈕禁用
            buttonC.Visible = false; //下載公版運價禁用




        }









    }







    private void Callprice()
    {






        SqlCommand objCommand = new SqlCommand();
        DataTable dt = null;
        DataTable dttemp = null;
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmdA = new SqlCommand();
        string SQLstr = "";
        string SQLstrt = "";
        string temp = "";
        SQLstr = @"Select Min,Max,seq from  tbMemberCollectionPrice order by Min";
        cmd.Parameters.Clear();
        cmd.CommandText = SQLstr;
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                temp += " <div class='row'>";
                temp += " <div class='col-lg-12 col-md-12 form-group form-inline'>";
                temp += "<label for='inputLastName' class='_cus_titleA '>" + dt.Rows[i]["min"].ToString() + " ~ " + dt.Rows[i]["max"].ToString() + "</label>";
                cmdA.Parameters.Clear();
                cmdA.Parameters.AddWithValue("@customer_id", Request.QueryString["customer_id"].ToString());
                cmdA.Parameters.AddWithValue("@CollectionPrice", dt.Rows[i]["seq"].ToString());
                SQLstrt = @"Select top 1 B.Handling_fee,B.order_fee  
                                from  tbCustomers A with(nolock) 
                                left join tbMemberCollectionPriceList B with(nolock) on A.customer_code = B.customer_code 
                                where customer_id = @customer_id and  CollectionPrice=@CollectionPrice";
                cmdA.CommandText = SQLstrt;
                dttemp = dbAdapter.getDataTable(cmdA);





                if (dttemp.Rows.Count > 0)
                {
                    if (Session["manager_type"].ToString() == "2")
                    {
                        temp += " <input type='textbox' id='A_fee_" + dt.Rows[i]["seq"].ToString() + "' name='A_fee_" + dt.Rows[i]["seq"].ToString() + "'  value='" + dttemp.Rows[0]["Handling_fee"].ToString() + "' onkeyup='this.value = chknum(this.value)'  Class='form-control'  placeholder='手續費' readonly >";
                        temp += " <input type='textbox' id='B_fee_" + dt.Rows[i]["seq"].ToString() + "' name='B_fee_" + dt.Rows[i]["seq"].ToString() + "'  value='" + dttemp.Rows[0]["order_fee"].ToString() + "'    Class='form-control' onkeyup='this.value = chkfloat(this.value)'  placeholder='報值費' readonly  >%";

                    }
                    else
                    {
                        temp += " <input type='textbox' id='A_fee_" + dt.Rows[i]["seq"].ToString() + "' name='A_fee_" + dt.Rows[i]["seq"].ToString() + "'  value='" + dttemp.Rows[0]["Handling_fee"].ToString() + "' onkeyup='this.value = chknum(this.value)'  Class='form-control'  placeholder='手續費' >";
                        temp += " <input type='textbox' id='B_fee_" + dt.Rows[i]["seq"].ToString() + "' name='B_fee_" + dt.Rows[i]["seq"].ToString() + "'  value='" + dttemp.Rows[0]["order_fee"].ToString() + "'    Class='form-control' onkeyup='this.value = chkfloat(this.value)'  placeholder='報值費' >%";
                    }

                }
                else
                {
                    if (Session["manager_type"].ToString() == "2")
                    {
                        temp += " <input type='textbox' id='A_fee_" + dt.Rows[i]["seq"].ToString() + "' name='A_fee_" + dt.Rows[i]["seq"].ToString() + "'  onkeyup='this.value = chknum(this.value)'  Class='form-control'  placeholder='手續費'  readonly  >";
                        temp += " <input type='textbox' id='B_fee_" + dt.Rows[i]["seq"].ToString() + "' name='B_fee_" + dt.Rows[i]["seq"].ToString() + "'    Class='form-control' onkeyup='this.value = chkfloat(this.value)'  placeholder='報值費' readonly  >%";
                    }
                    else
                    {
                        temp += " <input type='textbox' id='A_fee_" + dt.Rows[i]["seq"].ToString() + "' name='A_fee_" + dt.Rows[i]["seq"].ToString() + "'  onkeyup='this.value = chknum(this.value)'  Class='form-control'  placeholder='手續費'  >";
                        temp += " <input type='textbox' id='B_fee_" + dt.Rows[i]["seq"].ToString() + "' name='B_fee_" + dt.Rows[i]["seq"].ToString() + "'    Class='form-control' onkeyup='this.value = chkfloat(this.value)'  placeholder='報值費' >%";
                    }
                }
                temp += " </div >";
                temp += " </div >";
            }
        }
        Price.Text = temp;

    }
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    //protected void SendStation1_CheckedChanged(object sender, EventArgs e)
    //{
    //    supplier.Visible = true;
    //}
}


