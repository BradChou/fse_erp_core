﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_3_5.aspx.cs" Inherits="report_3_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }

        .header {
            white-space: nowrap;
            text-align :center;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">代收，到付</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label><asp:Label ID="lbSuppliers" runat="server" Text="配送站所"></asp:Label></label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" ></asp:DropDownList>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label >發送日期</label>
                        <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                        <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <asp:Button ID="btnQry" CssClass="btn btn-primary" runat="server" Text="查詢" OnClick="btnQry_Click" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class=" col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">　　　　</label>
                        <span class="checkbox checkbox-success">
                            <asp:CheckBox ID="collection_money" runat="server" Checked="True" Text="代收金額" /><asp:CheckBox ID="arrive_to_pay_freight" runat="server" Checked="True" Text="到付運費" /><asp:CheckBox ID="arrive_to_pay_append" runat="server" Checked="True" Text="到付追加" />
                        </span>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline text-right ">
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click" />
                    </div>
                </div>
               
            </div>
            <hr>
            <p class="text-primary">代收，到付查詢</p>
            <p class="text-primary">出表時間：<asp:Label ID="lbdateTime" runat="server"></asp:Label></p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th class="header">NO.</th>
                    <th class="header">作業時間</th>
                    <th class="header">作業</th>
                    <th class="header">出貨人</th>
                    <th class="header">出貨日期</th>
                    <th class="header">指定日</th>
                    <th class="header">貨號</th> 
                    <th class="header">傳票區分</th>
                    <th class="header">收件人編號</th>
                    <th class="header">到著站所</th>
                    <th class="header">收貨人</th>
                    <th class="header">收貨地址</th>
                    <th class="header">電話</th>
                    <th class="header">異常原因</th>
                    <th class="header">代收金額</th>
                    <th class="header">到付運費</th>
                    <th class="header">到付追加</th>
                    <th class="header">板數</th>
                    <th class="header">才數</th>
                    <th class="header">件數</th>
                    <th class="header">小板數</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="作業時間" style ="width :90px"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                            <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="出貨日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="指定日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_str").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="收件人編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_customer_code").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到著站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_station").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="收貨地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_address").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel").ToString())%></td>
                            <td data-th="異常原因"></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_to_pay_freight","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付追加"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_to_pay_append","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbm").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="小板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.splates").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="21" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
              
                        <nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>
        </div>
    </div>
</asp:Content>
