﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
   {
        
    }
    [WebMethod]
    public static string imgUrl()
    {
        bool IsPRO = false;

        SqlCommand command = new SqlCommand();

        try
        {
            IsPRO = bool.Parse(ConfigurationManager.AppSettings["IsPRO"]);

        }
        catch (Exception ex)
        {

        }

        string imgsrc = string.Empty;
        string imgalt = string.Empty;

        if (IsPRO)
        {
            imgsrc = @"images/login_title.png";

        }
        else
        {
            imgsrc = @"images/login_title_new.png";
        }


        return imgsrc;
    }

    protected void btn_login_Click(object sender, EventArgs e)
    {
        // 客戶全面改密碼
        if (DateTime.Now > DateTime.Parse("2020-09-07 08:45"))
        {

            bool IsPRO = false;
        
            SqlCommand command = new SqlCommand();

            try
            {
                IsPRO = bool.Parse(ConfigurationManager.AppSettings["IsPRO"]);
                    
            } catch (Exception ex) 
            {

            }

            if (IsPRO)
            {
                command.CommandText = "select password_changed from tbAccounts where account_code = @account_code";
            }
            else
            {
                command.CommandText = "select password_changed from tbAccounts where account_code = @account_code and account_code in ('skyeyes','dyco','9990097','9990100','FRD0001','F1000000102','F2900210002','C0001','C9487','F2501770002','T990000','F2900290002','F2900380002','F2900430002','F2900290102','F1001130002','F2900210602','F2900210702','307','9990262','idNo','F2900210302','S123568789','S123568888','98745623','98789898','F2900210102','F2900580002','9999','F2900190102','87654321','45678912','S123564567','12369656','65478569','32655634','78569854','64974136','45986754','31647964','79641364','64641313','46461313','79461379','36634141','61434316','67491643','74569654','91733791','19377319','36522541','15951595','35621524','78964123','12369874','65497456','32146987','69874123','D227000555','F2900310002','65564545','9871234','F333444555','56845136','F2900650002','11112222','0','7161158','21212','12345678','23423431','F3002600002')";
            }
            command.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());

            object returnObj = dbAdapter.getScalarBySQL(command);

            if (returnObj != null)
            {
                bool passwordChanged = Convert.ToBoolean(returnObj);

                if (!passwordChanged)
                {
                    Application["Account"] = account.Text.ToString().Trim();
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", @"alert('改密碼喔')", true);
                    Response.Redirect("ChangePassword.aspx");
                }
            }

        }

        //if (remchk.Checked == true)
        //{
        //    Response.Cookies["account"].Value = account.Text.ToString();
        //    Response.Cookies["chkacc"].Value = "1";
        //}
        //else
        //{
        //    Response.Cookies.Remove("chkacc");
        //    Response.Cookies.Remove("account");
        //}


        //string captcha = string.Empty;
        //if (Session["Captcha"] != null && Session["Captcha"].ToString() != "")
        //{
        //    captcha = Session["Captcha"].ToString();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('驗證碼無輸入，請重新輸入');</script>", false);
        //}

        MD5 md5 = MD5.Create();//建立一個MD5
        //byte[] source = Encoding.Default.GetBytes(password.Text.ToString());//將字串轉為Byte[]
        //byte[] crypto = md5.ComputeHash(source);//進行MD5加密
        //string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
        string result = Utility.GetMd5Hash(md5, password.Text.ToString());

        //if (captcha == xcode.Text.ToString() || result == "5ac5cf299068ff4d958a311c56a4b3cb" || result == "81dc9bdb52d04dc20036dbd8313ed055")
        //{

        SqlCommand cmd2 = new SqlCommand();
        DataTable dt2;
        cmd2.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        cmd2.Parameters.AddWithValue("@password", result);
        cmd2.CommandText = "select* from tbAccounts where manager_type != '5' AND account_code NOT LIKE '999%' and manager_unit not like '%全速配%' and manager_unit != '管理者'and manager_unit != '一般員工'and user_name  not like 'FSE%' and user_name   not like '%主管' and account_code != 'skyeyes'AND job_title != '供應商' and account_code = @account_code and password = @password  ";
        dt2 = dbAdapter.getDataTable(cmd2);


        SqlCommand cmd3 = new SqlCommand();
        DataTable dt3;
        cmd3.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        cmd3.Parameters.AddWithValue("@password", result);
        cmd3.CommandText = " select*from tbAccounts where manager_type != '5'AND account_code  LIKE '999%'and manager_unit not like '%全速配%'and manager_unit != '管理者'and manager_unit != '一般員工'and user_name  not like 'FSE%' and user_name   not like '%主管' and account_code != 'skyeyes' and account_code = @account_code and password = @password ";
        dt3 = dbAdapter.getDataTable(cmd3);


        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        cmd.Parameters.AddWithValue("@password", result);
        cmd.CommandText = @" Select * from tbAccounts a
left join tbEmps　b
	--on a.account_code=b.emp_code
on case when a.emp_code is null then a.account_code else a.emp_code end  =b.emp_code
	left join tbStation c
	on c.id=b.station
where a.account_code = @account_code  and (password=@password  or @password='5ac5cf299068ff4d958a311c56a4b3cb') 
and a.account_code not in (select account_code from tbaccounts tb left join tbsuppliers tt with(nolock) on tt.supplier_code = tb.head_code where(tb.manager_unit = '區配商' or tb.manager_unit = '區配商自營') and tt.supplier_code is null) ";
        dt = dbAdapter.getDataTable(cmd);

        if(dt2.Rows.Count>0)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='http://customer.junfu.asia/';alert('請至新的網站');</script>", false);

        }
        else if(dt3.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='http://transportation.junfu.asia';alert('請至新的網站');</script>", false);
        }
        else
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dt.Rows[i]["active_flag"]) == false)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('帳戶已停用，請聯繫系統管理員');</script>", false);
                        return;
                    }
                    else
                    {
                        #region  Log
                        PublicFunction _fun = new PublicFunction();
                        _fun.ExeOpLog(dt.Rows[i]["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
                        #endregion

                        Session["account_code"] = dt.Rows[i]["account_code"].ToString(); //使用者帳號
                        Session["master_code"] = dt.Rows[i]["master_code"].ToString();   //主客代碼
                        Session["job_title"] = dt.Rows[i]["job_title"].ToString();       //職稱
                        Session["user_name"] = dt.Rows[i]["user_name"].ToString();       //使用者名稱
                        Session["manager_type"] = dt.Rows[i]["manager_type"].ToString(); //管理單位類別(0:天眼公司 1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營)
                        Session["customer_code"] = dt.Rows[i]["customer_code"].ToString();
                        Session["station_area"] = dt.Rows[i]["station_area"].ToString();
                        Session["station_level"] = dt.Rows[i]["station_level"].ToString();
                        Session["management"] = dt.Rows[i]["management"].ToString();
                        Session["station_scode"] = dt.Rows[i]["station_scode"].ToString();


                        if ((new string[] { "0", "1", "2" }).Contains(dt.Rows[i]["manager_type"].ToString()))
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='option.aspx';alert('登入成功');</script>", false);
                            return;
                        }
                        else
                        {

                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", string.Format("<script>location.href='index.aspx?type={0}';alert('登入成功');</script>", Convert.ToInt16(dt.Rows[i]["account_type"]).ToString()), false);


                            return;
                        }
                    }
                }
            }
        }

        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('驗證碼錯誤，請重新輸入');</script>", false);
        //    return;
        //}
    }



    //protected void remchk_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (remchk.Checked == true)
    //    {
    //        Response.Cookies["account"].Value = account.Text.ToString();
    //        Response.Cookies["chkacc"].Value = "1";
    //    }
    //    else
    //    {
    //        Response.Cookies.Remove("chkacc");
    //        Response.Cookies.Remove("account");
    //    }
    //}
}