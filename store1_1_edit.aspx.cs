﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class store1_1_edit : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

            #region 主線/爆量
            string ItemsResult = "{'主線':'01','爆量':'02','店到店':'03','回頭車':'04','加派車':'05'}";
            IDictionary<string, string> ItemsList = JsonConvert.DeserializeObject<IDictionary<string, string>>(ItemsResult);
            Items.DataSource = ItemsList;
            Items.DataTextField = "key";
            Items.DataValueField = "value";
            Items.DataBind();
            Items.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 溫層
            using (SqlCommand objCommandTemp = new SqlCommand())
            {
                objCommandTemp.CommandText = string.Format(@"Select code_id , code_name  from tbItemCodes 
                                                             WHERE code_bclass = '6' and code_sclass = 'temp' and active_flag = 1");
                using (DataTable dtRoad = dbAdapter.getDataTable(objCommandTemp))
                {
                    Temperature.DataSource = dtRoad;
                    Temperature.DataValueField = "code_id";
                    Temperature.DataTextField = "code_name";
                    Temperature.DataBind();
                    Temperature.Items.Insert(0, new ListItem("請選擇", ""));
                }
                //string TemperatureResult = "{ '常溫':'01','鮮一':'02','鮮食':'03','OC':'04','冷凍':'05','文流':'06'}";
                //IDictionary<string, string> TemperatureList = JsonConvert.DeserializeObject<IDictionary<string, string>>(TemperatureResult);
                //Temperature.DataSource = TemperatureList;
                //Temperature.DataTextField = "key";
                //Temperature.DataValueField = "value";
                //Temperature.DataBind();
                //Temperature.Items.Insert(0, new ListItem("請選擇", ""));
            }
                
            #endregion

            #region 場區
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                Area.DataSource = dt;
                Area.DataValueField = "seq";
                Area.DataTextField = "code_name";
                Area.DataBind();
                Area.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 路線
            SqlCommand objCommandRoad = new SqlCommand();
            string wherestrRoad = "";
            var StrRoad = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=3", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(StrRoad))
            {
                wherestrRoad += " and (";
                var StrRoadAry = StrRoad.Split(',');
                var i = 0;
                foreach (var item2 in StrRoadAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestrRoad += " or ";
                        }
                        objCommandRoad.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestrRoad += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestrRoad += " )";
            }  
            objCommandRoad.CommandText = string.Format(@"select seq, code_name from ttItemCodesRoad with(nolock) where active_flag = 1 {0} order by seq", wherestrRoad);
            using (DataTable dtRoad = dbAdapter.getDataTable(objCommandRoad))
            {
                Route.DataSource = dtRoad;
                Route.DataValueField = "seq";
                Route.DataTextField = "code_name";
                Route.DataBind();
                Route.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion


            #region 車型
            using (SqlCommand objCommandTemp = new SqlCommand())
            {
                objCommandTemp.CommandText = string.Format(@"Select code_id , code_name  from tbItemCodes with(nolock)
                                                             WHERE code_bclass = '6' and code_sclass = 'cartype' and active_flag = 1");
                using (DataTable dtRoad = dbAdapter.getDataTable(objCommandTemp))
                {
                    car_type.DataSource = dtRoad;
                    car_type.DataValueField = "code_id";
                    car_type.DataTextField = "code_name";
                    car_type.DataBind();
                    car_type.Items.Insert(0, new ListItem("請選擇", ""));
                }
            }
            #endregion

            #region 
            manager_type = Session["manager_type"].ToString();        //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier.DataSource = Utility.getSupplierDT2(supplier_code, manager_type);
            supplier.DataValueField = "supplier_no";
            supplier.DataTextField = "showname";
            supplier.DataBind();
            supplier.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion


            readdata(a_id);

        }
    }    

    private void readdata(string a_id)
    {
        car_license.ReadOnly = true;
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);

            cmd.CommandText = @"Select * from ttAssetsSB A with(nolock) 
                                Where id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                Items.SelectedValue = dt.Rows[0]["Items"].ToString().Trim();
                DayNight.SelectedValue = dt.Rows[0]["DayNight"].ToString().Trim();
                InDate.Text = dt.Rows[0]["InDate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["InDate"]).ToString("yyyy/MM/dd") :"" ;
                starttime.Text = dt.Rows[0]["starttime"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["starttime"]).ToString("yyyy/MM/dd HH:mm") :"";
                endtime.Text = dt.Rows[0]["endtime"]  !=DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["endtime"]).ToString("yyyy/MM/dd HH:mm") : "";
                startstore_time.Text = dt.Rows[0]["startstore_time"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["startstore_time"]).ToString("yyyy/MM/dd HH:mm") :"";
                endstore_time.Text = dt.Rows[0]["endstore_time"]  !=  DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["endstore_time"]).ToString("yyyy/MM/dd HH:mm") :"";
                Temperature.SelectedValue = dt.Rows[0]["Temperature"].ToString().Trim();
                car_license.Text = dt.Rows[0]["car_license"].ToString().Trim();
                //car_license.ReadOnly = true;
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "carsel", "<script>$('#carsel').attr('style','display:none;')</script>", false);
                //cabin_type_text.Text = dt.Rows[0]["cabin_type_text"].ToString().Trim();
                car_type.SelectedValue = dt.Rows[0]["cabin_type"].ToString().Trim();
                Stores.Text = dt.Rows[0]["Stores"].ToString().Trim();
                Area.SelectedValue = dt.Rows[0]["Area"].ToString();
                //Area.Text = dt.Rows[0]["Area"].ToString().Trim();
                milage.Text = dt.Rows[0]["milage"].ToString().Trim();
                RequestObject.Text = dt.Rows[0]["RequestObject"].ToString().Trim();                
                
                //if (Route.SelectedValue =="") Route_name.Text = dt.Rows[0]["Route_name"].ToString();  //20190107註解
                if (Route.SelectedValue == "")  //如果關連不到，表示可能一開始沒有開權限，要再實際去抓對應的值
                {
                    SqlCommand objCommandRoad = new SqlCommand();
                    string wherestrRoad = "and  FieldArea=" + Area.SelectedValue;
                    if (Area.SelectedValue == "")
                        wherestrRoad = "";
                    objCommandRoad.CommandText = string.Format(@"select seq, code_name from ttItemCodesRoad with(nolock) where active_flag = 1 {0} order by seq", wherestrRoad);
                    using (DataTable dtRoad = dbAdapter.getDataTable(objCommandRoad))
                    {
                        Route.DataSource = dtRoad;
                        Route.DataValueField = "seq";
                        Route.DataTextField = "code_name";
                        Route.DataBind();
                        Route.Items.Insert(0, new ListItem("請選擇", ""));
                    }
                    Route.SelectedValue = dt.Rows[0]["Route"].ToString();
                }
                //Route.Text = dt.Rows[0]["Route"].ToString().Trim();
                driver.Text = dt.Rows[0]["driver"].ToString().Trim();
                car_retailer_text.Text = dt.Rows[0]["car_retailer_text"].ToString().Trim();
                supplier.SelectedValue = dt.Rows[0]["supplier_no"].ToString().Trim();
                PaymentObject.Text = dt.Rows[0]["PaymentObject"].ToString().Trim();
                income.Text = dt.Rows[0]["income"].ToString().Trim();
                expenses.Text = dt.Rows[0]["expenses"].ToString().Trim();
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();
            }
        }
        else
        {
            ApStatus = "Add";
        }

       
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                InDate.Text = DateTime.Now.ToString("yyyy/MM/dd");
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":

                ////#region 判斷重覆
                //SqlCommand cmda = new SqlCommand();
                //DataTable dta = new DataTable();
                //cmda.Parameters.AddWithValue("@car_license", car_license.Text);
                //cmda.CommandText = "Select id from ttAssetsSB with(nolock) where car_license=@car_license ";
                //dta = dbAdapter.getDataTable(cmda);
                //if (dta.Rows.Count > 0)
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                //    return;
                //}
                ////#endregion

                SqlCommand cmdadd = new SqlCommand();
                cmdadd.Parameters.AddWithValue("@Items", Items.SelectedValue);
                cmdadd.Parameters.AddWithValue("@Items_Text", Items.SelectedItem.Text);
                cmdadd.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue);
                cmdadd.Parameters.AddWithValue("@InDate", InDate.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@starttime", starttime.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@endtime", endtime.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@startstore_time", startstore_time.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@endstore_time", endstore_time.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue);
                cmdadd.Parameters.AddWithValue("@Temperature_Text", Temperature.SelectedItem.Text);
                cmdadd.Parameters.AddWithValue("@car_license", car_license.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@cabin_type", car_type .SelectedValue.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@cabin_type_text", car_type.SelectedItem.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@Stores", Stores.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@Area", Area.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@milage", milage.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@RequestObject", RequestObject.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@Route", Route.SelectedValue);
                cmdadd.Parameters.AddWithValue("@Route_name", Route.SelectedItem.Text.ToString().Trim());                
                cmdadd.Parameters.AddWithValue("@driver", driver.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@car_retailer_text", car_retailer_text.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@PaymentObject", PaymentObject.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@income", income.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@expenses", expenses.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                //時間判斷
                string JStr = "";
                if (DateTime.Parse(starttime.Text.ToString().Trim()) > DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr + "出場時間要小於返場時間!\\n";
                }
                if (DateTime.Parse(startstore_time.Text.ToString().Trim()) <= DateTime.Parse(starttime.Text.ToString().Trim()) || DateTime.Parse(startstore_time.Text.ToString().Trim()) >= DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr + "首店時間要介於出場時間和返場時間!\\n";
                }
                if (DateTime.Parse(endstore_time.Text.ToString().Trim()) <= DateTime.Parse(starttime.Text.ToString().Trim()) || DateTime.Parse(endstore_time.Text.ToString().Trim()) >= DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr + "末店時間要介於出場時間和返場時間!";
                }
                if (JStr != "")
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script> alert('" + JStr + "');</script>", false);
                    return;
                }
                cmdadd.CommandText = dbAdapter.SQLdosomething("ttAssetsSB", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@Items", Items.SelectedValue);
                cmdupd.Parameters.AddWithValue("@Items_Text", Items.SelectedItem.Text);
                cmdupd.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue);
                cmdupd.Parameters.AddWithValue("@InDate", InDate.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@starttime", starttime.Text.ToString());
                cmdupd.Parameters.AddWithValue("@endtime", endtime.Text.ToString());
                cmdupd.Parameters.AddWithValue("@startstore_time", startstore_time.Text.ToString());
                cmdupd.Parameters.AddWithValue("@endstore_time", endstore_time.Text.ToString());
                cmdupd.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue);
                cmdupd.Parameters.AddWithValue("@Temperature_Text", Temperature.SelectedItem.Text);
                string mod_carno = car_license.Text.ToString().Trim();
                if (!string.IsNullOrEmpty(car_license_no.Value))
                    mod_carno = car_license_no.Value.Trim();

                cmdupd.Parameters.AddWithValue("@car_license", mod_carno);                
                cmdupd.Parameters.AddWithValue("@cabin_type", car_type.SelectedValue.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@cabin_type_text", car_type.SelectedItem.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@Stores", Stores.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@Area", Area.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@milage", milage.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@RequestObject", RequestObject.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@Route", Route.SelectedValue);
                cmdupd.Parameters.AddWithValue("@Route_name", Route.SelectedItem.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@driver", driver.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@car_retailer_text", car_retailer_text.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@PaymentObject", PaymentObject.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@income", income.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@expenses", expenses.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", a_id);
                JStr = "";
                //時間判斷
                //if (DateTime.Compare(DateTime.Parse(starttime.Text.ToString().Trim()),DateTime.Parse(endtime.Text.ToString().Trim())) > 0)
                if (DateTime.Parse(starttime.Text.ToString().Trim()) > DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr+ "出場時間要小於返場時間!\\n";                    
                }
                if (DateTime.Parse(startstore_time.Text.ToString().Trim()) < DateTime.Parse(starttime.Text.ToString().Trim()) || DateTime.Parse(startstore_time.Text.ToString().Trim()) > DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr + "首店時間要介於出場時間和返場時間!\\n";                    
                }
                if (DateTime.Parse(endstore_time.Text.ToString().Trim()) < DateTime.Parse(starttime.Text.ToString().Trim()) || DateTime.Parse(endstore_time.Text.ToString().Trim()) > DateTime.Parse(endtime.Text.ToString().Trim()))
                {
                    JStr = JStr + "末店時間要介於出場時間和返場時間!";                   
                }
                if(JStr!="")
                { 
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script> alert('" + JStr + "');</script>", false);
                    return;
                }
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttAssetsSB", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='store1_1.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;
    }

    protected void Area_SelIndexChanged(object sender, EventArgs e)
    {
        SqlCommand objCommandRoad = new SqlCommand();
        string wherestrRoad = "and  FieldArea=" + Area.SelectedValue;
        if (Area.SelectedItem.Text.ToString() == "請選擇")
        {
            wherestrRoad = "";
            var StrRoad = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=3", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(StrRoad))
            {
                wherestrRoad += " and (";
                var StrRoadAry = StrRoad.Split(',');
                var i = 0;
                foreach (var item2 in StrRoadAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestrRoad += " or ";
                        }
                        objCommandRoad.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestrRoad += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestrRoad += " )";
            }
        }
        objCommandRoad.CommandText = string.Format(@"select seq, code_name from ttItemCodesRoad with(nolock) where active_flag = 1 {0} order by seq", wherestrRoad);
        using (DataTable dtRoad = dbAdapter.getDataTable(objCommandRoad))
        {
            Route.DataSource = dtRoad;
            Route.DataValueField = "seq";
            Route.DataTextField = "code_name";
            Route.DataBind();
            Route.Items.Insert(0, new ListItem("請選擇", ""));
        }
        
        if (!string.IsNullOrEmpty(car_license_no.Value))            
            car_license.Text = car_license_no.Value.Trim();
    }

    protected void Route_SelIndexChanged(object sender, EventArgs e)
    {
        if (Route.SelectedItem.Text.ToString() == "請選擇")
        {
            DayNight.SelectedValue = "1";
            Temperature.SelectedValue = "1";
        }
        else
        { 
            SqlCommand objCommandRoad = new SqlCommand();                
            objCommandRoad.CommandText = string.Format(@"select seq, code_name,DayNight,Temperature from ttItemCodesRoad with(nolock) 
                                         where active_flag = 1 and  FieldArea={0} and seq={1} order by seq", Area.SelectedValue, Route.SelectedValue);
            using (DataTable dtRoad = dbAdapter.getDataTable(objCommandRoad))
            {
                //DayNight.DataSource = dtRoad;
                DayNight.SelectedValue = dtRoad.Rows[0]["DayNight"].ToString().Trim();
                Temperature.SelectedValue = dtRoad.Rows[0]["Temperature"].ToString().Trim();
            }
        }
        if (!string.IsNullOrEmpty(car_license_no.Value))
            car_license.Text = car_license_no.Value.Trim();
    }
    protected void Supplier_SelIndexChanged(object sender, EventArgs e)
    {
        PaymentObject.Text = supplier.SelectedItem.Text.Substring(4,(supplier.SelectedItem.Text).Length-4);
        if (!string.IsNullOrEmpty(car_license_no.Value))
            car_license.Text = car_license_no.Value.Trim();
    }
    
}