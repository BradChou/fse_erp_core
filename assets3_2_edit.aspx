﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets3_2_edit.aspx.cs" Inherits="assets3_2_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"></link>
    <script src="js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-timepicker-zh-TW.js" type="text/javascript"></script>
    <style>
        
    </style>
    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                    {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
            });
        }

        $(function () {
            //$('.date_pickerYearMonth').datepicker(
            //    {
            //        dateFormat: "yy/mm",
            //        currentText: '當月',
            //        closeText: '確定',
            //        changeMonth: true,
            //        changeYear: true,
            //        showButtonPanel: true,
            //        onClose: function (dateText, inst) {
            //            function isDonePressed() {
            //                return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
            //            }
            //            if (isDonePressed()) {
            //                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            //                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            //                $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            //                $('.date_pickerYearMonth').focusout()//Added to remove focus from datepicker input box on selecting date
            //            }
            //            GetZero();
            //        },
            //        beforeShow: function (input, inst) {
            //            inst.dpDiv.addClass('month_year_datepicker');
            //            if (inst["id"].indexOf("fee_date") > 0) {
            //                inst.dpDiv.addClass('hidecalendar');
            //            }
            //            if ((datestr = $(this).val()).length > 0) {
            //                //alert("a");
            //                year = datestr.substring(datestr.length - 4, datestr.length);
            //                month = datestr.substring(0, 2);
            //                //$(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
            //                //$(this).datepicker('setDate', new Date(year, month-1, 1));
            //                //$(".ui-datepicker-calendar").hide();
            //            }
            //        }
            //    });
        });

        function ValidateFloat2(e, pnumber) {
            if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
                var newValue = /\d+[.]?[1-9]?/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                }
            }
            return false;
        }

    </script>

    <style>
        .ui-datepicker-calendar {
            /*display: none;*/
        }

        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

        ._checkboxlist {
            display: inline;
        }

            ._checkboxlist label {
                min-width: 40px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form-group" style="float: right;">
        <%--<a href="assets2_1_edit.aspx" ><span class="btn btn-danger glyphicon glyphicon-plus">刪除</span></a>--%>
    </div>
    <h2 class="margin-bottom-10">支出證明單/請款單-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
    <div class="templatemo-login-form">
        <asp:UpdatePanel ID="Upd_title" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!--基本設定-->
                <div class="bs-callout bs-callout-info">
                    <h3>1. 基本設定</h3>
                    <div class="rowform">
                        <div class="row ">
                            <div class="col-lg-12 bottnmargin form-inline">
                                <label><span class="REDWD_b">*</span>費用類別</label>
                                <asp:DropDownList ID="ddl_fee" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddl_fee_SelectedIndexChanged" onchange="GetZero()"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_fee" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇費用類別</asp:RequiredFieldValidator>
                                <label>
                                    <asp:Label ID="lb_oil" runat="server" Text="油品公司" Visible="false"></asp:Label></label>
                                <asp:DropDownList ID="ddl_oil" runat="server" Visible="false" onchange="GetZero()">
                                    <asp:ListItem>全國</asp:ListItem>
                                    <asp:ListItem>台塑</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="month"><span class="REDWD_b">*</span>費用年月</label>
                                <asp:TextBox ID="fee_date" runat="server" class="form-control date_pickerYearMonth" MaxLength="7" placeholder="YYYY/MM" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="re3" runat="server" ControlToValidate="fee_date" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇費用年月</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label><span class="REDWD_b">*</span>申請人</label>
                                <asp:TextBox ID="iuser" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="year"><span class="REDWD_b">*</span>申請日期</label>
                                <asp:TextBox ID="idate" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="idate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入申請日期</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="tonnes"><span class="REDWD_b">*</span>支付對象</label>
                                <asp:TextBox ID="company" runat="server" class="form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server" ControlToValidate="company" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入支付對象</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-8 bottnmargin form-inline">
                                <label for="tonnes"><span class="REDWD_b">*</span>支付方式</label>
                                <asp:RadioButtonList ID="pay_kind" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success">
                                    <asp:ListItem Value="1" Selected>現金</asp:ListItem>
                                    <asp:ListItem Value="2">匯款</asp:ListItem>
                                    <asp:ListItem Value="3">票據</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="tonnes">紙本</label>
                                <div class="_checkboxlist ">
                                    <asp:CheckBoxList ID="paper" runat="server" CssClass=" checkbox checkbox-success" RepeatDirection="Horizontal" >
    <asp:ListItem Value="1">發票</asp:ListItem>
    <asp:ListItem Value="2">收據</asp:ListItem>
</asp:CheckBoxList>
                                </div>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="tonnes"><span class="REDWD_b">*</span>預計付款日</label>
                                <asp:TextBox ID="pay_date" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="pay_date" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇預計付款日</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="tonnes">帳號</label>
                                <asp:TextBox ID="pay_account" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="tonnes">票據號碼</label>
                                <asp:TextBox ID="pay_num" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="memo">用途說明</label>
                                <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div>

                        </div>
                    </div>
                </div>
                <!--金額資訊-->
                <div class="bs-callout bs-callout-info">
                    <h3>2. 金額資訊</h3>
                    <div class="rowform">
                        <div class="row ">
                            <div class="col-lg-12 bottnmargin form-inline">
                                <label>請款金額</label>
                                <asp:TextBox ID="money" ForeColor="Red" class="form-control" runat="server"></asp:TextBox>元
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label>銷售額</label>
                                <asp:TextBox ID="price" ForeColor="Red" class="form-control" runat="server"></asp:TextBox>元
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label>稅額</label>
                                <asp:TextBox ID="tax" ForeColor="Red" class="form-control" runat="server"></asp:TextBox>元
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label>合計</label>
                                <asp:TextBox ID="total" ForeColor="Red" class="form-control" runat="server"></asp:TextBox>元
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <div class="form-group text-center">
            <asp:Button ID="printpaper" CssClass="btn btn-info hidden" runat="server" Text="列 印" OnClick="Print_Click" />
            <input type="button" id="ReturnMoneyInfo" value="取得金額資訊" class="btn btn-warning glyphicon glyphicon-plus" />
            <asp:Button ID="btnsave" CssClass="templatemo-blue-button hidden" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
            <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/assets3_2.aspx">取消</asp:LinkButton>
            <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
        </div>

    </div>

    <script type="text/javascript">
        $("#ReturnMoneyInfo").on("click", function () {
            ddl_fee = $("#<%=ddl_fee.ClientID%>").val();
        fee_date = $("#<%=fee_date.ClientID%>").val();
        ddl_oil = "";
        if (ddl_fee != "" && fee_date != "") {
            if ($("#<%=ddl_oil.ClientID%>").val() == "全國" || $("#<%=ddl_oil.ClientID%>").val() == "台塑") {
                ddl_oil = $("#<%=ddl_oil.ClientID%>").val();
            }
            showBlockUI();
            $.ajax({
                url: "assets3_2_edit.aspx/CallAPI",
                type: "Post",
                data: JSON.stringify({ feetype: ddl_fee, feedate: fee_date, oil: ddl_oil }),
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    $.unblockUI();
                    money = 0; price = 0; tax = 0; total = 0;
                    if (data.d["State"]) {
                        money = data.d["Money"];
                        price = data.d["Price"];
                        tax = data.d["Tax"];
                        total = money;
                    }
                        $("#<%=money.ClientID%>").val(money);
                        $("#<%=price.ClientID%>").val(price);
                        $("#<%=tax.ClientID%>").val(tax);
                        $("#<%=total.ClientID%>").val(total);
                        $("#<%=btnsave.ClientID%>").removeClass("hidden");
                    },
                    error: function (e) {
                        $.unblockUI();
                    }
                });
            } else {
                showBlockUIError("未選擇費用類別/費用年月");
            }
        });
        function GetZero() {
            $("#<%=money.ClientID%>").val("");
            $("#<%=price.ClientID%>").val("");
            $("#<%=tax.ClientID%>").val("");
            $("#<%=total.ClientID%>").val("");
            $("#<%=btnsave.ClientID%>").addClass("hidden");
            //$("#<%//=printpaper.ClientID%>").addClass("hidden");
        }
    </script>



</asp:Content>

