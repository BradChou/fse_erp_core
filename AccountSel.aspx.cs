﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class AccountSel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            SetDefaultMagType();

            //readdata();
        }
    }

    /// <summary>設定管理單位初始資料</summary>
    protected void SetDefaultMagType()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            String strSQL = @" SELECT code_id 'id',code_name 'name'
                                 FROM tbItemCodes With(Nolock)
                                WHERE code_sclass ='MT' AND active_flag=1";
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                rbmanager_type.Items.Clear();
                rbmanager_type.DataSource = dt;
                rbmanager_type.DataValueField = "id";
                rbmanager_type.DataTextField = "name";
                rbmanager_type.DataBind();
                rbmanager_type.SelectedIndex = 0;
                
            }
        }

    }


    private void readdata()
    {
       
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = "";
            cmd.Parameters.Clear();

            #region 關鍵字
            if (rbmanager_type.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@manager_type", rbmanager_type.SelectedValue.ToString());
                strWhereCmd += " and A.manager_type = @manager_type";
            }

            if (account_code.Text != "")
            {
                cmd.Parameters.AddWithValue("@account_code", account_code.Text);
                strWhereCmd += " and A.account_code like '%'+@account_code+'%' ";
            }

            if (user_name.Text != "")
            {
                cmd.Parameters.AddWithValue("@user_name", user_name.Text);
                strWhereCmd += " and A.user_name like '%'+@user_name+'%' ";
              
            }
            #endregion

            cmd.CommandText = string.Format(@"Select A.* , B.customer_shortname
                                          from tbAccounts A With(Nolock) 
                                          left join tbCustomers B With(Nolock) on A.master_code = B.master_code
                                          where 0 = 0   {0} order by account_code", strWhereCmd);

            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
            
        }           

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {
            
            string account_id = ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim();
            string account_code = ((Literal)e.Item.FindControl("LiAccount")).Text.Trim();
            string manager_type = ((HiddenField)e.Item.FindControl("Hid_mangtype")).Value.ToString().Trim();
            string customer_shortname = ((Literal)e.Item.FindControl("LiCus_name")).Text.Trim();
            string job_title = ((Literal)e.Item.FindControl("Lijob_title")).Text.Trim();
            string user_name = ((Literal)e.Item.FindControl("LiName")).Text.Trim();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["user_name"] + "').val('" + user_name + "');self.parent.$('#" + Request.QueryString["job_title"] + "').val('" + job_title + "');" +
                                                 "self.parent.$('#" + Request.QueryString["customer_shortname"] + "').val('" + customer_shortname + "'); self.parent.$('#" + Request.QueryString["manager_type"] + "').val('" + manager_type + "'); "+
                                                 "self.parent.$('#" + Request.QueryString["account"] + "').val('" + account_code + "'); parent.$.fancybox.close();</script>", false);

            return;

        }
       
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }



    protected void New_List_ItemCommand1(object source, RepeaterCommandEventArgs e)
    {

    }
}