﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Dapper;

using System.Configuration;


using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

using System.Web.Services;


public partial class JunFuTrace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
                     
            divMaster.Visible = true;
            
            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }

            readdata();
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {



        check_number.Text = check_number.Text.Trim().Replace(" ", "");
        //不能空白
        if (string.IsNullOrEmpty(check_number.Text))
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入貨號');</script><noscript></noscript>");
            return;
        }

        //只能有數字跟英文
        if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(check_number.Text))
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('貨號有誤');</script><noscript></noscript>");
            return;
        }

        List<string> request_id = new List<string>();

        try
        {
            using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
            {
                string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE check_number = @check_number 
";
                request_id = cn.Query<string>(sql, new { check_number = check_number.Text }).ToList();
            }
        }
        catch (Exception exx)
        {

        }


        readdata();
    }

    private void readdata()
    {

        if (check_number.Text != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                #endregion

                cmd.CommandText = string.Format(@"DECLARE  @CHKNUM NVARCHAR(100)  --貨號
                         DECLARE  @GOODS INT   --貨件運費
                         DECLARE  @YUAN  INT   --元付運費
                         DECLARE  @PEI   INT   --配送費用
                         DECLARE  @aUNIT INT   --每板費用
                         DECLARE  @TOTAL INT   --總費用
                         DECLARE  @ttERR   NVARCHAR(100) 
                         --select check_number , 0 '貨件運費',0'元付運費', 0'配送費用',0'每板費用', 0'總費用' 
                         --into 
                         --#mylist	  
                         --from tcDeliveryRequests with(nolock)
                         --where  check_number = @check_number
                         --
                         --DECLARE R_cursor CURSOR FOR
                         -- select * FROM #mylist
                         --OPEN R_cursor
                         --FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
                         --WHILE (@@FETCH_STATUS = 0) 
		                 --    BEGIN
			             --      CREATE TABLE #tmp (supplier_fee INT , supplier_unit_fee  INT , cscetion_fee INT,cscetion_unit_fee  INT,status_code  INT,status_msg NVARCHAR(500));
			             --      INSERT INTO #tmp EXEC  usp_GetShipFeeByCheckNumber @CHKNUM; 
			             --      SELECT @GOODS=supplier_fee, @PEI= cscetion_fee, @aUNIT= supplier_unit_fee ,@YUAN=supplier_fee , @TOTAL=supplier_fee, @ttERR= status_msg FROM #tmp
			             --       IF (@ttERR ='成功') BEGIN
				         --           UPDATE #mylist
				         --             SET 貨件運費 = @GOODS, 元付運費=@YUAN, 配送費用= @PEI, 每板費用 =@aUNIT ,總費用=@TOTAL
				         --           WHERE check_number = @CHKNUM
		                 --    END
                         --    IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
			             --        DROP TABLE #tmp
                         --
                         --  FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
                         --END
                         --CLOSE R_cursor
                         --DEALLOCATE R_cursor

                         Select  CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(D.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation,
                         E.showname,
                         A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                         A.time_period, A.order_number,
                         CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
                         A.product_category, C.code_name as product_category_name, 
                         A.subpoena_category, B.code_name as subpoena_category_name,
                         A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                         isnull(_fee.total_fee,0) '貨件運費' ,
                         isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) '元付運費',
                         A.arrive_to_pay_append '到付追加未稅', A.arrive_to_pay_append  *1.05 '到付追加含稅',
                         A.collection_money '代收金額',
                         isnull(_fee.cscetion_fee,0)  '配送費用',
                         isnull(_fee.supplier_unit_fee,0)  '每板費用',	
                         A.remote_fee '偏遠區加價',
                         isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) '總費用'
                         from tcDeliveryRequests A with(nolock)
                         LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
                         LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = '2' and C.code_sclass = 'S3'
                         LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                         LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                         --LEFT JOIN #mylist Z ON Z.check_number = A.check_number
                         CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
                         where   A.check_number = @check_number

                         IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
                         DROP TABLE #mylist");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    if (dt.Rows.Count > 0)
                    {
                        string customer_code = dt.Rows[0]["customer_code"].ToString();
                        string send_contact = dt.Rows[0]["send_contact"].ToString();
                        send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
                        string send_tel = dt.Rows[0]["send_tel"].ToString();
                        send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                        string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                        receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                        string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                        receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;

                        lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel;
                        lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;
                    }
                    New_List_01.DataSource = dt;
                    New_List_01.DataBind();
                }

            }



            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/" + days);
                //}

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);                   
                }
                #endregion

                cmd.CommandText = string.Format(@"Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                 A.arrive_option,A.exception_option , 
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                 from ttDeliveryScanLog A with(nolock)
                                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 where 1=1  and A.check_number = @check_number
                                                 order by log_id desc");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        string filename = dt.Rows[0]["sign_field_image"].ToString();
                        if (filename != "")
                        {
                            Image1.ImageUrl = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + filename;
                        }
                    }
                    New_List_02.DataSource = dt;
                    New_List_02.DataBind();
                }

            }
        }


    }
}