﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_3_2_s.aspx.cs" Inherits="report_3_2_s" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">配送統計</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-12 col-md-12 form-group form-inline">
                        <label><asp:Label ID="lbSuppliers" runat="server" Text="配送站所"></asp:Label></label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" ></asp:DropDownList>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">發送日期</label>
                        <asp:TextBox ID="dates" runat="server" class="form-control" CssClass="date_picker"  autocomplete="off" ></asp:TextBox>
                        ~ 
                        <asp:TextBox ID="datee" runat="server" class="form-control" CssClass="date_picker"  autocomplete="off" ></asp:TextBox>
                        
                        <asp:Button ID="search" CssClass="btn btn-darkblue btn_view" runat="server" Text="查詢" OnClick ="search_Click" />
                    </div>
                    <%--<div class="col-lg-6 col-md-6 form-group form-inline text-right">
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                    </div>--%>
                </div>
            </div>
            <hr>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>配送站所</th>
                    <th>應配送筆數</th>
                    <th>已配送筆數</th>
                    <th>配送率</th>
                    <th>未配送筆數</th>
                    <th>異常率</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%# Container.ItemIndex + 1 %></td>
                            <td data-th="配送站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_station").ToString())%></td>
                            <td data-th="應配送筆數"><a href="report_3_2.aspx?kind=1&Suppliers=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&sdate=<%=dates.Text%>&edate=<%=datee.Text%>"  target="_blank" class=" btn btn-link " id="link1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.totcnt","{0:N0}").ToString())%></a></td>
                            <td data-th="已配送筆數"><a href="report_3_2.aspx?kind=2&Suppliers=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&sdate=<%=dates.Text%>&edate=<%=datee.Text%>"  target="_blank"class=" btn btn-link " id="link2"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.yescnt","{0:N0}").ToString())%></a></td>
                            <td data-th="配送率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.yesRate").ToString())%></td>
                            <td data-th="未配送筆數"><a href="report_3_2.aspx?kind=3&Suppliers=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&sdate=<%=dates.Text%>&edate=<%=datee.Text%>" target="_blank" class=" btn btn-link " id="link6"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noncnt","{0:N0}").ToString())%></a></td>
                            <td data-th="異常率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noRate").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="7" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            
        </div>
    </div>
</asp:Content>

