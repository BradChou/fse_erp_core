﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using System.Text;
using System.Threading.Tasks;

public partial class LT_transport_1_1 : System.Web.UI.Page
{
    string area_arrive_code = "";
    bool check_address = true;
    bool send_address_parsing = false;
    public Decimal start_number
    {
        get { return Convert.ToDecimal(ViewState["start_number"]); }
        set { ViewState["start_number"] = value; }
    }

    public Decimal end_number
    {
        get { return Convert.ToDecimal(ViewState["end_number"]); }
        set { ViewState["end_number"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }




    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            receive_address.Attributes["onBlur"] = "showAlert();";

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode += dt1.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                        else
                        {

                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                supplier_code = dt1.Rows[0]["station_code"].ToString();
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
                default:
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }


            Shipments_date.Text = DateTime.Today.ToString("yyyy/MM/dd");

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            receive_city.DataSource = dbAdapter.getDataTable(cmd1);
            receive_city.DataValueField = "city";
            receive_city.DataTextField = "city";
            receive_city.DataBind();
            receive_city.Items.Insert(0, new ListItem("請選擇", ""));

            send_city.DataSource = dbAdapter.getDataTable(cmd1);
            send_city.DataValueField = "city";
            send_city.DataTextField = "city";
            send_city.DataBind();
            send_city.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 客代編號
            rbcheck_type_SelectedIndexChanged(null, null);
            #endregion

            #region 託運類別
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
            check_type.DataSource = dbAdapter.getDataTable(cmd2);
            check_type.DataValueField = "code_id";
            check_type.DataTextField = "code_name";
            check_type.DataBind();
            #endregion

            #region 傳票類別
            SqlCommand cmd3 = new SqlCommand();
            cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 and code_id <> 31 and code_id <> 51 order by code_id ";
            subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
            subpoena_category.DataValueField = "code_id";
            subpoena_category.DataTextField = "code_name";
            subpoena_category.DataBind();
            #endregion

            #region 特殊配送
            SqlCommand cmd5 = new SqlCommand();
            cmd5.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S4' and active_flag = 1 ";
            special_send.DataSource = dbAdapter.getDataTable(cmd5);
            special_send.DataValueField = "code_id";
            special_send.DataTextField = "code_name";
            special_send.DataBind();
            #endregion

            #region 配送時段
            SqlCommand cmd6 = new SqlCommand();
            cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
            time_period.DataSource = dbAdapter.getDataTable(cmd6);
            time_period.DataValueField = "code_id";
            time_period.DataTextField = "code_name";
            time_period.DataBind();
            time_period.Items.Insert(2, new ListItem("晚", "晚"));
            time_period.SelectedValue = "不指定";
            #endregion

            #region 配送溫層
            using (SqlCommand cmd10 = new SqlCommand())
            {
                cmd10.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S9' and active_flag = 1 ";
                dltemp.DataSource = dbAdapter.getDataTable(cmd10);
                dltemp.DataValueField = "code_id";
                dltemp.DataTextField = "code_name";
                dltemp.DataBind();
            }

            #endregion


            #region 貨號區間(一筆式)
            SqlCommand cmd7 = new SqlCommand();
            cmd7.CommandText = "select start_number,end_number from tbCheckNumber With(nolock) where type = 1 ";
            DataTable DT = dbAdapter.getDataTable(cmd7);
            if (DT.Rows.Count > 0)
            {
                start_number = Convert.ToInt32(DT.Rows[0]["start_number"]);
                end_number = Convert.ToInt32(DT.Rows[0]["end_number"]);
            }
            #endregion

            //客戶帳號到著站選擇隱藏
            if (manager_type == "5")
            {
                ddlarea_arrive_code.Style.Add("pointer-events", "none");
                //Label11.Visible = false;
            }

            //公司人員不能用自己帳號打單，要用部門客代
            if (manager_type == "1" || manager_type == "2")
            {
                InnerInstruction.Style.Add("display", "inline-block");
            }

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('親愛的全速配客戶，您好： \\r\\r    暫停配送服務將從2023年5月1日開始，而最後的收貨日為2023年4月21日，爾後我們將不接受新的宅配訂單。一旦IT營運系統重整完成，我們將立即再次通知您，期能以更好的IT營運系統跟品質，再度為您提供服務。\\r    我們會在暫停配送期間提供運送替代方案的諮詢專線，供您參考使用。\\r諮詢專線：(02) 7733 - 7938#128 \\r專案服務人員：周世文 先生')", true);

    }

    protected void Clear()
    {
        dlcustomer_code.SelectedIndex = 0;
        customer_code.Text = "";
        order_number.Text = "";
        check_type.SelectedIndex = 0;
        receive_customer_code.Text = "";
        subpoena_category.SelectedIndex = 0;
        receive_tel1.Text = "";
        receive_tel1_ext.Text = "";
        receive_tel2.Text = "";
        receive_contact.Text = "";
        receiver_code.Text = "";
        receiver_id.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        ddlarea_arrive_code.Items.Clear();
        cbarrive.Checked = false;
        pieces.Text = "";
        //plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        //arrive_to_pay_freight.Text = "";
        //arrive_to_pay_append.Text = "";
        Report_fee.Text = "";
        send_contact.Text = "";
        send_tel.Text = "";
        send_city.SelectedIndex = 0;
        send_area.Items.Clear();
        send_address.Text = "";
        //donate_invoice_flag.Checked = false;
        //electronic_invoice_flag.Checked = false;
        //uniform_numbers.Text = "";
        //arrive_mobile.Text = "";
        //arrive_email.Text = "";
        invoice_desc.Value = "";
        //product_category.SelectedIndex = 0;
        special_send.SelectedIndex = 0;
        arrive_assign_date.Text = "";
        receipt_flag.Checked = false;
        WareHouse.Checked = false;
        //pallet_recycling_flag.Checked = false;
        i_supplier_fee.Text = "";
        i_csection_fee.Text = "";
        i_remote_fee.Text = "";
        //turn_board.Checked = false;
        //upstairs.Checked = false;
        //difficult_delivery.Checked = false;
        receipt_round_trip.Checked = false;
        //dlDistributor.SelectedValue  = "";

        ArticleNumber.Text = "";
        SendPlatform.Text = "";
        ArticleName.Text = "";
        holidaydelivery.Checked = false;


        Shipments_date.Text = DateTime.Today.ToString("yyyy/MM/dd");

    }


    protected void ClearbySave()
    {
        check_number.Text = "";
        order_number.Text = "";
        check_type.SelectedIndex = 0;
        receive_customer_code.Text = "";
        subpoena_category.SelectedIndex = 0;
        receive_tel1.Text = "";
        receive_tel1_ext.Text = "";
        receive_tel2.Text = "";
        receive_contact.Text = "";
        receiver_code.Text = "";
        receiver_id.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        ddlarea_arrive_code.SelectedIndex = 0;
        cbarrive.Checked = false;
        pieces.Text = "1";
        //plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        //arrive_to_pay_freight.Text = "";
        //arrive_to_pay_append.Text = "";
        Report_fee.Text = "";
        invoice_desc.Value = "";
        special_send.SelectedIndex = 0;
        arrive_assign_date.Text = "";
        receipt_flag.Checked = false;
        WareHouse.Checked = false;
        //pallet_recycling_flag.Checked = false;
        receipt_round_trip.Checked = false;


        i_supplier_fee.Text = "";
        i_csection_fee.Text = "";
        i_remote_fee.Text = "";
        //turn_board.Checked = false;
        //upstairs.Checked = false;
        //difficult_delivery.Checked = false;
        ArticleNumber.Text = "";
        SendPlatform.Text = "";
        ArticleName.Text = "";
        holidaydelivery.Checked = false;
        chkProductValue.Checked = false;
        tbProductValue.Text = "";
        is_special_area.Text = "";
        lbSpecialAreaId.Text = "";
        lbSpecialAreaFee.Text = "";
        Label12.Visible = false;
        Label13.Visible = false;
        Label14.Text = "";

        dlProductName.SelectedValue = "";
        dlProductSpec.SelectedValue = "";

        //dlDistributor.SelectedValue = "";

    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    //dlDistributor.SelectedValue = "";
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
        }
        is_special_area.Text = "";
        Label14.Text = "";
    }


    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        customer_code.Text = dlcustomer_code.SelectedValue;
        divReceiver.Visible = false;
        New_List.DataSource = null;
        New_List.DataBind();
        var product_type = "";
        bool is_new_customer = false;
        bool is_account_preorder = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type = table.Rows[0]["product_type"].ToString().ToLower();
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
            is_account_preorder = table.Rows[0]["product_type"].ToString().ToLower() == "2" || table.Rows[0]["product_type"].ToString().ToLower() == "3" ? true : false;
        }
        is_account_preorder_hidden.Text = is_account_preorder == true ? "1" : "0";

        #region 到著站簡碼
        ddlarea_arrive_code.DataSource = Utility.getArea_Arrive_Code(true);
        ddlarea_arrive_code.DataValueField = "station_scode";
        ddlarea_arrive_code.DataTextField = "showsname";
        ddlarea_arrive_code.DataBind();
        ddlarea_arrive_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion


        #region 產品
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
        }

        string sqlstr = "";
        if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
        else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
        else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
        else { sqlstr = "' '"; }

        using (SqlCommand cmd8 = new SqlCommand())
        {
            //目前先寫死 (馬丁)
            cmd8.CommandText = "select ProductId,Name from productmanage with(nolock) where ProductId in ( " + sqlstr + " ) and IsActive='1' order by id";

            dlProductName.DataSource = dbAdapter.getDataTableForFSE01(cmd8);
            dlProductName.DataValueField = "ProductId";
            dlProductName.DataTextField = "Name";
            dlProductName.DataBind();
            dlProductName.Items.Insert(0, new ListItem("請選擇", ""));
        }

        #endregion

        if (is_account_preorder)
        {
            receipt_round_trip.Enabled = false;
            pieces.Text = "1";
            pieces.Enabled = false;
        }
        else
        {
            receipt_round_trip.Enabled = true;
            pieces.Enabled = true;
        }

        GetSendData();
    }

    protected void dl_Pieces_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (dlcustomer_code.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請先選擇客戶代碼');</script>", false);
            return;
        }

        var product_type = "";
        bool is_new_customer = false;

        #region 產品
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type = table.Rows[0]["product_type"].ToString().ToLower();
            is_new_customer = (bool)table.Rows[0]["is_new_customer"];
        }

        string sqlstr = "";
        if (pieces.Text != "")
        {
            if (Convert.ToInt32(pieces.Text) > 1)
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶多筆
                else if (product_type == "1") { sqlstr = "'CM000036'"; } //月結舊客戶多筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論件多筆
                else { sqlstr = "' '"; }
            }
            else
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶單筆
                else if (product_type == "1") { sqlstr = "'CS000035'"; } //月結舊客戶單筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論件單筆
                else { sqlstr = "' '"; }
            }
        }

        using (SqlCommand cmd8 = new SqlCommand())
        {
            //目前先寫死 (馬丁)
            cmd8.CommandText = @"select ProductId,
               case ProductId 
               when 'CM000030' then '論S'
               when 'CS000035' then '月結'
               when 'CM000036' then '月結'

               when 'CM000043' then '論件'
               else Name
              end as Name from productmanage 
              where ProductId in ( " + sqlstr + " ) and IsActive='1' order by id";

            dlProductName.DataSource = dbAdapter.getDataTableForFSE01(cmd8);
            dlProductName.DataValueField = "ProductId";
            dlProductName.DataTextField = "Name";
            dlProductName.DataBind();
            dlProductName.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion
    }




    protected void dl_ProductName_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (dlcustomer_code.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('請先選擇客戶代碼');</script>", false);
            return;
        }

        #region 產品規格
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select  is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
        }

        using (SqlCommand cmd9 = new SqlCommand())
        {
            var strsql = "";



            //一般月結客代 - 多筆、單筆、月結多筆、月結單筆
            if (dlProductName.SelectedValue == "CM000030" || dlProductName.SelectedValue == "CS000029" || dlProductName.SelectedValue == "CM000036" || dlProductName.SelectedValue == "CS000035")
            {
                strsql = "'S060','S090','S110','S120','S150','B001','B002','B003'";
            }
            ////一般月結客代 - 月結袋
            //if (dlProductName.SelectedValue == "CS000035")
            //{
            //    strsql = "'B001','B002','B003'";
            //}

            //預購袋客代 - 預購袋 且為新客袋
            else if (dlProductName.SelectedValue == "PS000031" && is_new_customer == true)
            {
                strsql = "'B003'";
            }
            //預購袋客代 - 預購袋
            else if (dlProductName.SelectedValue == "PS000031")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 速配箱
            else if (dlProductName.SelectedValue == "PS000032")
            {
                strsql = "'X001','X002','X005'";
            }
            //預購袋客代 - 超值袋
            else if (dlProductName.SelectedValue == "PS000033")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 超值箱
            else if (dlProductName.SelectedValue == "PS000034")
            {
                strsql = "'X001','X002','X005'";
            }
            //內部文件
            else if (dlProductName.SelectedValue == "CS000037")
            {
                strsql = "'N001'";
            }
            //商城出貨 - 包材
            else if (dlProductName.SelectedValue == "CS000038")
            {
                strsql = "'N001'";
            }
            //一般件 - 論件
            else if (dlProductName.SelectedValue == "CM000043")
            {
                strsql = "'C001','C002','C003','C004','C005'";
            }
            else { }

            if (dlProductName.SelectedValue == "CM000043")
            {
                cmd9.CommandText = "SELECT CodeId,CodeName FROM (select ROW_NUMBER()OVER (PARTITION BY SPEC ORDER BY P.ID DESC)SN,CodeId,CodeName,OrderBy  from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec' where ProductId = @ProductId and  CustomerCode = @customerCode and DeliveryType = 'D')T WHERE SN = '1' order by OrderBy asc";
                cmd9.Parameters.AddWithValue("@customerCode", dlcustomer_code.SelectedValue);
                cmd9.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
            else
            {
                cmd9.CommandText = "select CodeId,CodeName from ItemCodes with(nolock) where CodeType = 'ProductSpec' and CodeId in (" + strsql + ") order by id";
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
        }
        #endregion

    }


    private void GetSendData()
    {
        //顯示寄件人資料
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_shortname , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + customer_code.Text + "' ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            send_contact.Text = dt.Rows[0]["customer_shortname"].ToString();   //名稱
            send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
            send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
            city_SelectedIndexChanged(send_city, null);
            send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
            send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
            lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
            supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();      //區配code
            supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();      //區配name

            bool individualfee = dt.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["individual_fee"]) : false;
            bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
            if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
            {
                individual_fee.Style.Add("display", "block");
                if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
            }
            else
            {
                individual_fee.Style.Add("display", "none");
                if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
            }
        }
    }

    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        string management = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();

        string managementList = "";
        string station_scodeList = "";
        string station_areaList = "";
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (supplier_code != "")
        {
            if (manager_type == "1" || manager_type == "2")
            {
                cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and customer_code like ''+@supplier_code+'00000002%' ";
            }
            else
            {
                cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and customer_code like ''+@supplier_code+'%' ";
            }
        }
        else
        {
            cmd2.Parameters.AddWithValue("@management", management);
            cmd2.Parameters.AddWithValue("@station_scode", station_scode);
            cmd2.Parameters.AddWithValue("@station_area", station_area);

            if (station_level.Equals("1"))
            {
                cmd2.CommandText = @" select * from tbStation
                                          where management = @management ";
                DataTable dt1 = dbAdapter.getDataTable(cmd2);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        managementList += "'";
                        managementList += dt1.Rows[i]["station_scode"].ToString();
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }
                wherestr = " and station_scode in (" + managementList + ")";
            }
            else if (station_level.Equals("2"))
            {
                cmd2.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                DataTable dt1 = dbAdapter.getDataTable(cmd2);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        station_scodeList += "'";
                        station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }
            }
            else if (station_level.Equals("4"))
            {
                cmd2.CommandText = @" select * from tbStation
                                          where station_area = @station_area";
                DataTable dt1 = dbAdapter.getDataTable(cmd2);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        station_areaList += "'";
                        station_areaList += dt1.Rows[i]["station_scode"].ToString();
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }
            }
        }

        cmd2.Parameters.AddWithValue("@type", "1");
        cmd2.Parameters.AddWithValue("@pricing_code", rbcheck_type.SelectedValue);
        cmd2.CommandText = string.Format(@"Select customer_code , customer_code+ '-' + customer_shortname as name  
                                           From tbCustomers with(Nolock) 
                                           Where stop_shipping_code = '0' and pricing_code = @pricing_code and type =@type
                                           {0} order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion
        Clear_customer();

        //switch (rbcheck_type.SelectedValue)
        //{
        //    case "01":
        //    case "04":
        //        pieces.Visible = true;
        //        plates.Visible = false;
        //        cbm.Visible = false;
        //        lbUnit.Text = "板";
        //        break;
        //    case "02":
        //        pieces.Visible = false;
        //        plates.Visible = true;
        //        cbm.Visible = false;
        //        lbUnit.Text = "件";
        //        break;
        //    case "03":
        //        pieces.Visible = false;
        //        plates.Visible = false;
        //        cbm.Visible = true;
        //        lbUnit.Text = "才";
        //        break;
        //}

    }

    protected void arrive_assign_date_TextChanged(object sender, EventArgs e)
    {
        DateTime date;
        DateTime.TryParse(arrive_assign_date.Text, out date);
        bool holidayDelivery = Utility.IsHolidayDelivery(dlcustomer_code.SelectedValue, area_arrive_code, date);
        if (holidayDelivery)
            holidaydelivery.Checked = true;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (receipt_flag.Checked && receipt_round_trip.Checked)
        {
            Label2.Visible = true;
            return;
        }
        if (WareHouse.Checked && receipt_round_trip.Checked)
        {
            Label13.Visible = true;
            return;
        }


        var product_type = "";
        bool is_new_customer = false;
        //寄件地址解析
        send_address_parsing = true;
        var send_info = GetAddressParsing(send_city.Text.Trim() + send_area.Text.Trim() + send_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");
        try
        {
            lbSendCode.Text = send_info.StationCode;
            lbSendSD.Text = send_info.SendSalesDriverCode;
            lbSendMD.Text = send_info.SendMotorcycleDriverCode;
        }
        catch { }
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type = table.Rows[0]["product_type"].ToString().ToLower();
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
        }
        //一般件產品名稱需與件數符合
        if (product_type == "1")
        {
            if ((pieces.Text.Trim() == "1" && dlProductName.SelectedValue != "CS000035" && dlProductName.SelectedValue != "CM000030") || (pieces.Text.Trim() != "1" && dlProductName.SelectedValue != "CM000036" && dlProductName.SelectedValue != "CM000030"))
            {
                Label5.Visible = true;
                return;
            }
        }

        //預購袋、超值箱不可寄外島
        if ((product_type == "2" || product_type == "3") && ddlarea_arrive_code.SelectedValue == "95")
        {
            Response.Write("<script language=JavaScript>alert('此客代類型無法寄送外島');</script>");
            //MessageBox.Show("代收金額超過20,000上限，請重新輸入");
            collection_money.Text = string.Empty;
            return;
        }
        //目前9開頭(91、92、95、99)及9結尾、接駁碼99 不能開回單 
        if ((ddlarea_arrive_code.SelectedValue.Contains("9") && receipt_flag.Checked) || (lbShuttleStationCode.Text == "999" && receipt_flag.Checked))
        {
            Label6.Visible = true;
            return;
        }
        Label5.Visible = false;
        Label6.Visible = false;
        string ErrStr = "";
        lbErrQuant.Text = "";
        string seq = string.Empty;
        // 來回件
        bool isRoundtrip = false;

        /* 20170727 託運類別 ... modify by lisa
         * 論板時、論小板時：【板數】與【件數】一定要提示填入數字
         * 論才時：【板數】與【件數】及【才數】皆要填入
         */
        bool individual = individual_fee.Style.Value.IndexOf("display:block") > -1;
        int i_tmp;
        switch (rbcheck_type.SelectedValue)
        {
            case "01":
            case "04":
                //if (string.IsNullOrWhiteSpace(plates.Text) || string.IsNullOrWhiteSpace(pieces.Text))
                //{
                //    lbErrQuant.Text = "請輸入板數及件數";
                //    plates.Focus();
                //    lbErrQuant.Visible = true;
                //    return;
                //}

                //if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                //if (i_tmp <= 0)
                //{
                //    lbErrQuant.Text = "請輸入板數";
                //    plates.Focus();
                //    lbErrQuant.Visible = true;
                //    return;
                //}

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "02":
                if (string.IsNullOrWhiteSpace(pieces.Text))
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "03":
                if (string.IsNullOrWhiteSpace(cbm.Text)
                    //|| string.IsNullOrWhiteSpace(plates.Text)
                    || string.IsNullOrWhiteSpace(pieces.Text)
                    )
                {
                    lbErrQuant.Text = "請輸入件數及才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                //if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                //if (i_tmp <= 0)
                //{
                //    lbErrQuant.Text = "請輸入板數";
                //    plates.Focus();
                //    lbErrQuant.Visible = true;
                //    return;
                //}

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }
                break;
        }

        if (individual && i_supplier_fee.Text == "")
        {
            lbErrQuant.Text = "請輸入貨件運費";
            lbErrQuant.Visible = true;
            i_supplier_fee.Focus();
            return;
        }


        if (collection_money.Text != "" && (dlcustomer_code.ToString() != "F2900000402"))
        {
            int NumA = 0;

            NumA = Convert.ToInt32(collection_money.Text);

            if (is_new_customer == true && NumA > 50000)  //新客代,上限5萬
            {
                Response.Write("<script language=JavaScript>alert('代收金額超過50,000上限，請重新輸入');</script>");
                //MessageBox.Show("代收金額超過20,000上限，請重新輸入");
                collection_money.Text = string.Empty;
                return;
            }
            else if (is_new_customer == false && NumA > 20000)
            {

                Response.Write("<script language=JavaScript>alert('代收金額超過20,000上限，請重新輸入');</script>");
                //MessageBox.Show("代收金額超過20,000上限，請重新輸入");
                collection_money.Text = string.Empty;
                return;
            }
        }

        //舊客戶不填寫 報值金額及保值費
        if (is_new_customer)
        {


        }
        else
        {

            if ((Report_fee.Text != "0" && Report_fee.Text != "") || (tbProductValue.Text != "" && tbProductValue.Text != "0"))
            {
                Label12.Visible = true;
                Label12.Text = "此客代無須填寫報值金額與保值費";
                return;
            }

            else
            {

            }
            //var str1 = "";
            //var str2 = "";
            //var tt = Report_fee.Text;
            //var yy = tbProductValue.Text;
            //if (Report_fee.Text != "0" || !string.IsNullOrEmpty(Report_fee.Text) || Report_fee.Text != " " || Report_fee.Text != "") 
            //{ str1 = " 報值金額 "; }
            //if (chkProductValue.Checked==true || tbProductValue.Text != "0" || !string.IsNullOrEmpty(tbProductValue.Text) || tbProductValue.Text != "" || tbProductValue.Text != " ")
            //{ str2 = " 保值費 "; }
            //Label12.Visible = true;
            //Label12.Text = "此客代無須填寫" + str1 + str2;
            //return;


        }

        ////花蓮、台東地區 強制輸入才數
        //if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        //if (((receive_city.SelectedValue == "花蓮縣") || (receive_city.SelectedValue == "臺東縣")) && (i_tmp <= 0))
        //{
        //    lbErrQuant.Text = "請輸入才數";
        //    cbm.Focus();
        //    lbErrQuant.Visible = true;
        //    return;
        //}

        lbErrQuant.Visible = (lbErrQuant.Text != "");

        if (lbErrQuant.Text == "" && lbErr.Text == "")
        {
            //檢查該客代的貨號是否已用完
            using (SqlCommand cmd = new SqlCommand())
            {
                DataTable dataTable = new DataTable();
                DataTable dataTable1 = new DataTable();

                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                cmd.CommandText = @"
                       declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock) group by customer_code

                       select * from Customer_checknumber_setting A With(Nolock)
                       join @newest t on A.customer_code = t.customer_code and A.update_date = t.update_date
                       where A.customer_code = @customer_code";
                dataTable = dbAdapter.getDataTable(cmd);

                using (SqlCommand sql = new SqlCommand())
                {
                    sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                    sql.CommandText = @"
                       select product_type from tbCustomers  With(Nolock)
                       where customer_code = @customer_code and product_type <> '2' and product_type <> '3' and stop_shipping_code = '0'";
                    dataTable1 = dbAdapter.getDataTable(sql);
                }

                if (dataTable.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dataTable.Rows[0]["total_count"].ToString()) <= Convert.ToInt32(dataTable.Rows[0]["used_count"].ToString()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('貨號數量使用完畢，請冾系統管理員。');</script>", false);
                        return;
                    }
                    var active = dataTable.Rows[0]["is_active"].ToString();

                    if (active == "False")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代未啟用。');</script>", false);
                        return;
                    }
                }
                //客代是超值箱預購袋客戶且找不到貨號數量
                else if (dataTable.Rows.Count == 0 && dataTable1.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此客代未設定貨號數量。');</script>", false);
                    return;
                }
            }
            //Decimal current_number = 0;
            //using (SqlCommand cmdt = new SqlCommand())
            //{
            //    DataTable dtt;
            //    cmdt.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            //    cmdt.CommandText = " select * from tbDeliveryNumberSetting where customer_code=@customer_code AND IsActive = 1 ";
            //    dtt = dbAdapter.getDataTable(cmdt);
            //    if (dtt.Rows.Count > 0)
            //    {
            //        //start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
            //        //end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
            //        //current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
            //        //if (current_number == 0 || current_number == end_number)
            //        //{
            //        //    current_number = start_number;
            //        //}
            //        //else
            //        //{
            //        //    current_number += 1;
            //        //}
            //        chk = 1;
            //    }

            //}

            //int chk = 0;

            //貨號區間預購袋
            //using (SqlCommand cmdt = new SqlCommand())
            //{
            //    DataTable dtt;
            //    cmdt.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            //    cmdt.CommandText = " select top 1 * from tbDeliveryNumberOnceSetting where customer_code=@customer_code AND IsActive = 1  order by end_number  ";
            //    dtt = dbAdapter.getDataTable(cmdt);
            //    if (dtt.Rows.Count > 0)
            //    {
            //        seq = dtt.Rows[0]["seq"].ToString();
            //        start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
            //        end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
            //        current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;

            //        if (end_number == current_number)
            //        {
            //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋使用完畢，請冾系統管理員。');</script>", false);
            //            return;
            //        }

            //        if (current_number == 0 || current_number == end_number)
            //        {
            //            //未取過號，直接從start_number 開始取
            //            current_number = start_number;
            //        }
            //        else if (!(current_number >= start_number && current_number <= end_number))
            //        {
            //            //如果當前取號不在現在取號區號，那也從當然取區間的start_number開始取
            //            current_number = start_number;
            //        }
            //        else
            //        {
            //            current_number += 1;
            //        }

            //        chk = 2;
            //    }
            //}
            //if (chk == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無託運單貨號區間與預購袋區間，請冾系統管理員。');</script>", false);
            //    return;
            //}

            //到著站若有選擇,不進行地址檢查
            if (ddlarea_arrive_code.SelectedValue != "")
            { area_arrive_code = ddlarea_arrive_code.SelectedValue; }
            else
            {
                //檢查地址
                var receive_info = GetAddressParsing(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");
                if (check_address == false)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查地址是否正確');</script>", false);
                    return;
                }
                lbShuttleStationCode.Text = receive_info.ShuttleStationCode;
                lbReceiveCode.Text = receive_info.StationCode;
                lbReceiveSD.Text = receive_info.SalesDriverCode;
                lbReceiveMD.Text = receive_info.MotorcycleDriverCode;

            }
            if (order_number.Text != "")
            {
                Regex reg = new Regex("^[A-Za-z0-9]+$");
                if (!reg.IsMatch(order_number.Text))
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('訂單號碼不可輸入英文、數字以外的值!!');</script>", false);
                    return;
                }
            }

            try
            {
                int ttpieces = 0;
                int ttplates = 0;
                int ttcbm = 0;
                int ttcollection_money = 0;
                int ttarrive_to_pay = 0;
                int ttarrive_to_append = 0;
                int Reportfee = 0;
                int remote_fee = 0;
                int request_id = 0;
                int supplier_fee = 0;  //配送費用
                int cscetion_fee = 0;  //C配運價
                int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                       //int unit_price = 0;    //單位價格
                                       //int unit_price_c = 0;  //單位價格-C配


                string returnCheckNum = string.Empty;

                #region 新增    
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@check_number", "0");                     //託運單號(貨號) 先給0
                    cmd.Parameters.AddWithValue("@Less_than_truckload", 1);                                       //0：棧板運輸  1：零擔運輸
                                                                                                                  //cmd.Parameters.AddWithValue("@Distributor", dlDistributor.SelectedValue);                   //配送商(全速配、宅配通)
                    cmd.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);                           //產品名稱
                    cmd.Parameters.AddWithValue("@SpecCodeId", dlProductSpec.SelectedValue);                        //規格
                    cmd.Parameters.AddWithValue("@pricing_type", rbcheck_type.SelectedValue);                     //計價模式 (01:論板、02:論件、03論才、04論小板)
                    cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);                              //訂單號碼
                    cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", receive_customer_code.Text);            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
                    cmd.Parameters.AddWithValue("@receive_tel1", receive_tel1.Text);                              //電話1
                    cmd.Parameters.AddWithValue("@receive_tel1_ext", receive_tel1_ext.Text.ToString());           //電話分機
                    cmd.Parameters.AddWithValue("@receive_tel2", receive_tel2.Text);                              //電話2
                    cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
                    cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
                    cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
                    cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
                    if (!individual)
                    {
                        //remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
                    }
                    else
                    {
                        if (int.TryParse(i_remote_fee.Text, out i_tmp)) remote_fee = i_tmp;
                    }
                    cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
                    cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code);             //到著碼
                    cmd.Parameters.AddWithValue("@ShuttleStationCode", lbShuttleStationCode.Text);      //接駁碼
                    cmd.Parameters.AddWithValue("@SendCode", lbSendCode.Text);      //地址解析發送站
                    cmd.Parameters.AddWithValue("@SendSD", lbSendSD.Text);      //發送SD
                    cmd.Parameters.AddWithValue("@SendMD", lbSendMD.Text);      //發送MD
                    cmd.Parameters.AddWithValue("@ReceiveCode", lbReceiveCode.Text);      //地址解析到著站
                    cmd.Parameters.AddWithValue("@ReceiveSD", lbReceiveSD.Text);      //到著SD
                    cmd.Parameters.AddWithValue("@ReceiveMD", lbReceiveMD.Text);      //到著MD

                    cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", Convert.ToInt16(cbarrive.Checked));                //到站領貨 0:否、1:是
                    if (cbarrive.Checked)
                    {
                        cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
                    }

                    try
                    {
                        ttpieces = Convert.ToInt32(pieces.Text);
                    }
                    catch { }

                    //try
                    //{
                    //    ttplates = Convert.ToInt32(plates.Text);
                    //}
                    //catch { }

                    try
                    {
                        ttcbm = Convert.ToInt32(cbm.Text);
                    }
                    catch { }

                    try
                    {
                        ttcollection_money = Convert.ToInt32(collection_money.Text);
                    }
                    catch { }

                    //try
                    //{
                    //    ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
                    //}
                    //catch { }

                    //try
                    //{
                    //    ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
                    //}
                    //catch { }

                    try
                    {
                        Reportfee = Convert.ToInt32(Report_fee.Text);
                    }
                    catch { }

                    cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
                    cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數

                    switch (subpoena_category.SelectedValue)
                    {
                        case "11"://元付
                            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                //到付運費
                            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);              //到付追加
                            break;
                        case "21"://到付
                            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                 //到付運費
                            break;
                        case "25"://元付-到付追加
                            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                //到付追加
                            break;
                        case "41"://代收貨款
                            cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                     //代收金
                            break;
                    }
                    cmd.Parameters.AddWithValue("@ReportFee", Report_fee.Text);                     //報值金額
                    cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
                    cmd.Parameters.AddWithValue("@send_tel", send_tel.Text.ToString());                           //寄件人電話
                    cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
                    cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
                    cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
                    cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                    cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                    cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
                                                                                                                  //cmd.Parameters.AddWithValue("@arrive_mobile", arrive_mobile.Text.ToString());                 //收件人-手機1  
                    cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
                                                                                                                  //cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.SelectedValue.ToString());        //備註
                    cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //說明
                                                                                                                  //cmd.Parameters.AddWithValue("@product_category", product_category.SelectedValue.ToString());  //商品種類
                    cmd.Parameters.AddWithValue("@special_send", special_send.SelectedValue.ToString());          //特殊配送

                    cmd.Parameters.AddWithValue("@ArticleNumber", ArticleNumber.Text);  //產品編號
                    cmd.Parameters.AddWithValue("@SendPlatform", SendPlatform.Text);    //出貨平台
                    cmd.Parameters.AddWithValue("@ArticleName", ArticleName.Text);      //品名

                    //if (isBag.Checked)
                    //    cmd.Parameters.AddWithValue("@bagno", "1");      //袋號
                    //else
                    //    cmd.Parameters.AddWithValue("@bagno", DBNull.Value);

                    DateTime date;
                    cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(arrive_assign_date.Text, out date) ? (object)date : DBNull.Value);                  //指定日
                    cmd.Parameters.AddWithValue("@time_period", time_period.SelectedValue.ToString());            //時段
                    cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
                                                                                                                                   //cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
                    cmd.Parameters.AddWithValue("@round_trip", Convert.ToInt16(receipt_round_trip.Checked));         //是否來回件
                    cmd.Parameters.AddWithValue("@WareHouse", Convert.ToInt16(WareHouse.Checked));         //是否為統倉
                    cmd.Parameters.AddWithValue("@holiday_delivery", holidaydelivery.Checked);         //是否為假日配送

                    //if (pallet_recycling_flag.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@Pallet_type", Pallet_type.ToString());
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@Pallet_type", "");
                    //}
                    //cmd.Parameters.AddWithValue("@turn_board", Convert.ToInt16(turn_board.Checked));                               //翻版
                    //cmd.Parameters.AddWithValue("@upstairs", Convert.ToInt16(upstairs.Checked));                                   //上樓
                    //cmd.Parameters.AddWithValue("@difficult_delivery", Convert.ToInt16(difficult_delivery.Checked));               //困配

                    //if (int.TryParse(turn_board_fee.Text, out i_tmp) && turn_board.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@turn_board_fee", i_tmp.ToString());
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@turn_board_fee", 0);
                    //}

                    //if (int.TryParse(upstairs_fee.Text, out i_tmp) && upstairs.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@upstairs_fee", i_tmp.ToString());
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@upstairs_fee", 0);
                    //}

                    //if (int.TryParse(difficult_fee.Text, out i_tmp) && difficult_delivery.Checked)
                    //{
                    //    cmd.Parameters.AddWithValue("@difficult_fee", i_tmp.ToString());
                    //}
                    //else
                    //{
                    //    cmd.Parameters.AddWithValue("@difficult_fee", 0);
                    //}

                    cmd.Parameters.AddWithValue("@add_transfer", 0);                                              //是否轉址
                    cmd.Parameters.AddWithValue("@sub_check_number", "000");                                      //次貨號
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code_sel);                             //配送商代碼
                    cmd.Parameters.AddWithValue("@supplier_name", supplier_name_sel);                             //配送商名稱
                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間
                    cmd.Parameters.AddWithValue("@print_date", DateTime.TryParse(Shipments_date.Text, out date) ? (object)date : DBNull.Value);   //出貨日期
                    cmd.Parameters.AddWithValue("@print_flag", 0);                                                //是否列印
                    cmd.Parameters.AddWithValue("@ProductValue", tbProductValue.Text);  //保值費
                    cmd.Parameters.AddWithValue("@SpecialAreaId", lbSpecialAreaId.Text);  //特服區
                    cmd.Parameters.AddWithValue("@SpecialAreaFee", lbSpecialAreaFee.Text);  //特服費用                    
                    if (individual)
                    {
                        if (int.TryParse(i_supplier_fee.Text, out i_tmp))
                        {
                            cmd.Parameters.AddWithValue("@supplier_fee", i_tmp.ToString());
                            cmd.Parameters.AddWithValue("@total_fee", i_tmp.ToString());
                        }
                        if (int.TryParse(i_csection_fee.Text, out i_tmp))
                            cmd.Parameters.AddWithValue("@csection_fee", i_tmp.ToString());
                    }

                    if (dlcustomer_code.SelectedValue != null && dlcustomer_code.SelectedValue.Length > 0)
                        cmd.Parameters.AddWithValue("@send_station_scode", dlcustomer_code.SelectedValue.Substring(1, 2));
                    else
                        cmd.Parameters.AddWithValue("@send_station_scode", txtSendScode.Text);


                    if (receipt_round_trip.Checked)
                    {
                        isRoundtrip = true;
                        using (SqlCommand cmdr = new SqlCommand())
                        {
                            cmdr.CommandText = "select next value for ttAutoNumeral";
                            var table = dbAdapter.getDataTable(cmdr);
                            returnCheckNum = table.Rows[0][0].ToString();
                            cmd.Parameters.AddWithValue("@roundtrip_checknumber", returnCheckNum);
                        }
                    }

                    //cmd.CommandText = dbAdapter.SQLdosomething("tcDeliveryRequests", cmd, "insert");
                    //dbAdapter.execNonQuery(cmd);
                    cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                    if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                    {

                        //if (chk == 2)//更新預購袋 目前取號
                        //{
                        //    using (SqlCommand cmdupd = new SqlCommand())
                        //    {
                        //        cmdupd.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                        //        if (current_number == end_number)
                        //        {
                        //            cmdupd.Parameters.AddWithValue("@IsActive", 0);                          //預購袋取號完畢，直接改停用
                        //        }
                        //        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_code", dlcustomer_code.SelectedValue);
                        //        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", seq);
                        //        cmdupd.CommandText = dbAdapter.genUpdateComm("tbDeliveryNumberOnceSetting", cmdupd);         //修改
                        //        dbAdapter.execNonQuery(cmdupd);
                        //    }
                        //}

                        #region 
                        if (!individual)
                        {
                            SqlCommand cmd2 = new SqlCommand();
                            cmd2.CommandText = "usp_GetLTShipFeeByRequestId";
                            cmd2.CommandType = CommandType.StoredProcedure;
                            cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                            using (DataTable dt = dbAdapter.getDataTable(cmd2))
                            {
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                    if (status_code == 1)
                                    {
                                        supplier_fee = dt.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["supplier_fee"]) : 0;       //配送費用

                                        //回寫到託運單的費用欄位
                                        using (SqlCommand cmd3 = new SqlCommand())
                                        {
                                            cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                            cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id.ToString());
                                            cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                            try
                                            {
                                                dbAdapter.execNonQuery(cmd3);
                                            }
                                            catch (Exception ex)
                                            {
                                                string strErr = string.Empty;
                                                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                                strErr = "零擔託運單-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                                //錯誤寫至Log
                                                PublicFunction _fun = new PublicFunction();
                                                _fun.Log(strErr, "S");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    // 組新增回件SQL
                    if (isRoundtrip)
                    {
                        Dictionary<string, object> returnParameter = new Dictionary<string, object>();
                        foreach (SqlParameter parameter in cmd.Parameters)
                        {
                            returnParameter.Add(parameter.ParameterName.Trim(), parameter.Value);
                        }

                        // 回程代收貨款改成0
                        returnParameter["@collection_money"] = 0;
                        returnParameter["@DeliveryType"] = "R";
                        //特服費相關清空
                        returnParameter["@SpecialAreaId"] = "0";
                        returnParameter["@SpecialAreaFee"] = "0";

                        // 收件人，寄件人互換
                        string[] receiveInfo = new string[] { "@check_number", "@receive_tel1", "@receive_contact", "@receive_city", "@receive_area", "@receive_address", "@area_arrive_code", "@ReceiveCode", "@ReceiveSD", "@ReceiveMD" };
                        string[] sendInfo = new string[] { "@roundtrip_checknumber", "@send_tel", "@send_contact", "@send_city", "@send_area", "@send_address", "@send_station_scode", "@SendCode", "@SendSD", "@SendMD" };
                        for (var i = 0; i < receiveInfo.Length; i++)
                        {
                            object receiveData = returnParameter[receiveInfo[i]];
                            object sendData = returnParameter[sendInfo[i]];

                            returnParameter[sendInfo[i]] = receiveData;
                            returnParameter[receiveInfo[i]] = sendData;
                        }

                        using (SqlCommand cmdRetrun = new SqlCommand())
                        {
                            foreach (var param in returnParameter)
                            {
                                cmdRetrun.Parameters.AddWithValue(param.Key, param.Value);
                            }

                            cmdRetrun.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmdRetrun);
                            dbAdapter.execNonQuery(cmdRetrun);

                            //寫入CBMDetailLog (來回件)
                            using (SqlCommand cmdcbm = new SqlCommand())
                            {
                                cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                                cmdcbm.Parameters.AddWithValue("@CheckNumber", returnCheckNum);
                                cmdcbm.Parameters.AddWithValue("@CBM", dlProductSpec.SelectedValue);
                                cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                                cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                                cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                                dbAdapter.execNonQuery(cmdcbm);
                            }
                        }
                    }
                }

                #endregion

                //更新checknumber為0的那筆資料→request id 建立的貨號
                string prefixCheckNumber = "103";

                using (SqlCommand sql = new SqlCommand())
                {
                    sql.CommandText = string.Format(@"select check_number_prehead  
                        from tbCustomers c
                        join check_number_prehead pre on c.product_type = pre.product_type
                        where customer_code = '{0}'", dlcustomer_code.SelectedValue);
                    DataTable dataTable = dbAdapter.getDataTable(sql);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        prefixCheckNumber = dataTable.Rows[0]["check_number_prehead"].ToString();
                    }
                }
                //要補的checknumber
                string insert_check_number = string.Empty;

                using (SqlCommand sql = new SqlCommand())
                {
                    sql.CommandText = string.Format(@"Select * From tcDeliveryRequests with(nolock) where customer_code ='" + dlcustomer_code.SelectedValue + "' and  check_number = '0' order by request_id desc");
                    DataTable dataTable = dbAdapter.getDataTable(sql);
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        string requestId = dataTable.Rows[0]["request_id"].ToString();

                        requestId = string.Format("{0,8:00000000}", int.Parse(requestId));

                        long check_code = Convert.ToInt64(string.Concat(prefixCheckNumber, requestId)) % 7;

                        insert_check_number = string.Concat(prefixCheckNumber, requestId, check_code);

                        using (SqlCommand sqlCommand2 = new SqlCommand())
                        {
                            sqlCommand2.Parameters.AddWithValue("@check_number", insert_check_number);
                            sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", requestId);
                            sqlCommand2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", sqlCommand2);
                            dbAdapter.execNonQuery(sqlCommand2);
                        }
                        //寫入CBMDetailLog
                        using (SqlCommand cmdcbm = new SqlCommand())
                        {
                            cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                            cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);
                            cmdcbm.Parameters.AddWithValue("@CBM", dlProductSpec.SelectedValue);
                            cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                            cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                            cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                            dbAdapter.execNonQuery(cmdcbm);
                        }
                        #region 95/99 接駁碼999 產生郵局流水號  
                        if (area_arrive_code == "95" || area_arrive_code == "99" || lbShuttleStationCode.Text == "999")
                        {
                            var addr = receive_city.SelectedValue.ToString() + receive_area.SelectedValue.ToString() + receive_address.Text;
                            var zipcode = Func.GetZipCode(addr);

                            using (SqlCommand sql2 = new SqlCommand())
                            {
                                var cdate = dataTable.Rows[0]["cdate"];
                                var udate = dataTable.Rows[0]["udate"];
                                var cuser = dataTable.Rows[0]["cuser"];
                                var uuser = dataTable.Rows[0]["uuser"];
                                var strpost_number = Func.GetPostNumber();
                                var post_check_code = Func.GetPostNumberCheckCode(strpost_number, zipcode);
                                sql2.Parameters.AddWithValue("@jf_request_id", requestId);
                                sql2.Parameters.AddWithValue("@jf_check_number", insert_check_number);
                                sql2.Parameters.AddWithValue("@post_number", strpost_number);
                                sql2.Parameters.AddWithValue("@zipcode5", zipcode);
                                sql2.Parameters.AddWithValue("@check_code", post_check_code);
                                sql2.Parameters.AddWithValue("@cdate", cdate);
                                sql2.Parameters.AddWithValue("@udate", udate);
                                sql2.Parameters.AddWithValue("@cuser", cuser);
                                sql2.Parameters.AddWithValue("@uuser", uuser);
                                sql2.CommandText = dbAdapter.genInsertComm("Post_request", false, sql2);
                                dbAdapter.execNonQuery(sql2);
                            }
                        }
                        #endregion
                    }
                }


                if (isRoundtrip)
                {
                    //來回件 (補roundtrip_checknumber)
                    PublicFunction _fun = new PublicFunction();
                    _fun.RepairRoundtrip_checknumber(returnCheckNum, insert_check_number);
                }

                using (SqlCommand cmdt = new SqlCommand())
                {
                    cmdt.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                    cmdt.Parameters.AddWithValue("@count", 1);
                    cmdt.CommandText = "update Customer_checknumber_setting set used_count = used_count + @count  where customer_code=@customer_code and is_active = 1";
                    dbAdapter.execNonQuery(cmdt);
                }
                using (SqlCommand sql = new SqlCommand())
                {
                    DataTable dataTable = new DataTable();
                    sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                    sql.CommandText = @"
                       declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock) group by customer_code
                       
                       select A.is_active, A.total_count - A.used_count as rest_count from Customer_checknumber_setting A With(Nolock)
                       join @newest t on A.customer_code = t.customer_code and A.update_date = t.update_date
                       where A.customer_code = @customer_code";

                    dataTable = dbAdapter.getDataTable(sql);
                    if (dataTable.Rows.Count > 0)
                    {
                        var active = dataTable.Rows[0]["is_active"].ToString();
                        long remain_count;
                        if (active == "False")
                        { remain_count = 0; }
                        else
                        { remain_count = Convert.ToInt64(dataTable.Rows[0]["rest_count"].ToString()); }

                        SqlCommand cmd6 = new SqlCommand();
                        cmd6.CommandText = "select check_number from tcDeliveryRequests r1 left join tbcustomers c1 on  c1.customer_code=r1.customer_code where request_id = @request_id";
                        cmd6.Parameters.AddWithValue("@request_id", request_id);
                        DataTable dt6;
                        dt6 = dbAdapter.getDataTable(cmd6);
                        string check_number = dt6.Rows[0]["check_number"].ToString();

                        if (is_account_preorder_hidden.Text == "1")
                        {
                            using (SqlCommand cmd1 = new SqlCommand())
                            {
                                cmd1.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                cmd1.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                cmd1.Parameters.AddWithValue("@pieces_count", -1);
                                cmd1.Parameters.AddWithValue("@function_flag", "D1-1");
                                cmd1.Parameters.AddWithValue("@adjustment_reason", "單筆打單");
                                cmd1.Parameters.AddWithValue("@check_number", check_number);
                                cmd1.Parameters.AddWithValue("@remain_count", remain_count);
                                cmd1.CommandText = dbAdapter.genInsertComm("checknumber_record_for_bags_and_boxes", false, cmd1);
                                dbAdapter.execNonQuery(cmd1);
                            }
                        }
                    }
                }


                #region 常用收件人通訊錄
                if (cbSaveReceive.Checked && receiver_id.Text == "")
                {
                    if (!string.IsNullOrEmpty(customer_code.Text.Trim()) && !string.IsNullOrEmpty(receive_contact.Text.Trim()))
                    {
                        //先判斷，如果有姓名、編號、地址、已存在就不再新增
                        using (SqlCommand cmd5 = new SqlCommand())
                        {
                            cmd5.Parameters.AddWithValue("@customer_code", customer_code.Text.Trim());
                            cmd5.Parameters.AddWithValue("@receiver_code", receiver_code.Text.Trim());
                            cmd5.Parameters.AddWithValue("@receiver_name", receive_contact.Text.Trim());
                            cmd5.Parameters.AddWithValue("@address_city", receive_city.SelectedValue);
                            cmd5.Parameters.AddWithValue("@address_area", receive_area.SelectedValue);
                            cmd5.Parameters.AddWithValue("@address_road", receive_address.Text.Trim());


                            cmd5.CommandText = string.Format(@"Select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  and A.receiver_code =@receiver_code and  A.receiver_name=@receiver_name 
                                          and A.customer_code =@customer_code and  A.address_city =@address_city and  address_area = @address_area 
                                          and address_road = @address_road ");
                            using (DataTable dt = dbAdapter.getDataTable(cmd5))
                            {
                                if (dt == null || dt.Rows.Count == 0)
                                {
                                    SqlCommand cmd4 = new SqlCommand();
                                    cmd4.Parameters.AddWithValue("@customer_code", customer_code.Text);                     //客戶代碼
                                    cmd4.Parameters.AddWithValue("@receiver_code", receiver_code.Text);                     //收貨人代碼
                                    cmd4.Parameters.AddWithValue("@receiver_name", receive_contact.Text);                   //收貨人名稱
                                    cmd4.Parameters.AddWithValue("@tel", receive_tel1.Text);                                //電話
                                    cmd4.Parameters.AddWithValue("@tel_ext", receive_tel1_ext.Text);                        //分機
                                    cmd4.Parameters.AddWithValue("@tel2", receive_tel2.Text);                               //電話2
                                    cmd4.Parameters.AddWithValue("@address_city", receive_city.SelectedValue.ToString());   //地址-縣市
                                    cmd4.Parameters.AddWithValue("@address_area", receive_area.SelectedValue.ToString());   //地址 - 區域
                                    cmd4.Parameters.AddWithValue("@address_road", receive_address.Text);                    //地址-路段
                                    cmd4.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                                    cmd4.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                                    cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                                    cmd4.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                                    cmd4.CommandText = dbAdapter.SQLdosomething("tbReceiver", cmd4, "insert");
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd4);
                                    }
                                    catch (Exception ex)
                                    {
                                        string strErr = string.Empty;
                                        if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                        strErr = "零擔託運單-儲存常用通訊人" + Request.RawUrl + strErr + ": " + ex.ToString();


                                        //錯誤寫至Log
                                        PublicFunction _fun = new PublicFunction();
                                        _fun.Log(strErr, "S");
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                string strErr = string.Empty;
                ErrStr += ex.ToString();
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "零擔託運單" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");
            }
            if (ErrStr == "")
            {
                ClearbySave();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
            }
            return;

        }
    }

    //protected void check_number_TextChanged(object sender, EventArgs e)
    //{
    //    //託運單號
    //    //暫時僅用10碼，且貨號前方不補零。新竹規則MOD(num, 7) 前9碼 + 0~6檢碼
    //    //2019/09/20 修正 可10或12碼 
    //    //2019/09/27 修正 只有圓通可12碼其餘依舊10碼 
    //    lbErr.Text = "";
    //    if (dlcustomer_code.SelectedValue.Length <= 0)
    //    {
    //        lbErr.Text = "請先選擇客代編號。";
    //    }
    //    else
    //    {
    //        String value = dlcustomer_code.SelectedValue.Substring(0, 7);

    //        if (value == "0000185")
    //        {
    //            if (check_number.Text.Length != 12)
    //            {
    //                lbErr.Text = "託運單號錯誤，長度應為12碼。";
    //            }
    //        }
    //        else
    //        {
    //            if (check_number.Text.Length != 10)
    //            {
    //                lbErr.Text = "託運單號錯誤，長度應為10。";
    //            }
    //            if (check_number.Text.Length == 10)
    //            {
    //                //Regex regex = new Regex("^[0-9]*$");
    //                Regex reg = new Regex("^[0-9]*$");
    //                if (!reg.IsMatch(check_number.Text))
    //                {
    //                    lbErr.Text = "非有效的託運單號，請檢查編號是否正確";
    //                }
    //                else
    //                {
    //                    int num = Convert.ToInt32(check_number.Text.Substring(0, 9));
    //                    //if (num >= start_number && num <= end_number)
    //                    //{
    //                    int chk = num % 7;
    //                    int lastnum = Convert.ToInt32(check_number.Text.Substring(9, 1));
    //                    if (lastnum != chk)
    //                    {
    //                        lbErr.Text = "非有效的託運單號，請檢查編號是否正確";
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    //}
    //    //else
    //    //{
    //    //    lbErr.Text = "非有效的託運單號,應落在" + start_number.ToString() + " ~ " + end_number.ToString() + "之間，請檢查編號是否正確";
    //    //}

    //    //}
    //    lbErr.Visible = (lbErr.Text != "");
    //}

    protected void cbarrive_CheckedChanged(object sender, EventArgs e)
    {
        if (cbarrive.Checked && ddlarea_arrive_code.SelectedValue != "")
        {
            receive_address.Text = ddlarea_arrive_code.SelectedItem + "站址";
        }

        //area_arrive_code.Enabled = cbarrive.Checked;

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        is_special_area.Text = "";
        Label14.Text = "";
        if (receive_address.Text != "")
        {
            receive_address.Text = "";
            ddlarea_arrive_code.SelectedValue = "";
            //CheckAreaIncorrectly_sub();
        }

        #region 到著站簡碼
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.station_code  + ' '+  B.station_name as showname    
                                                   from ttArriveSitesScattered A
                                                   left join tbStation B on A.station_code  = B.station_code 
                                                   where A.station_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            //using (DataTable dt = dbAdapter.getDataTable(cmd7))
            //{
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        try
            //        {
            //            area_arrive_code.SelectedValue = dt.Rows[0]["station_code"].ToString();
            //            if (area_arrive_code.SelectedValue == "")
            //            {
            //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請先選擇客代編號');</script>", false);
            //                Clear();

            //            }
            //        }
            //        catch { }
            //    }
            //}
        }
        #endregion

        #region 到著站簡碼
        //using (SqlCommand cmd7 = new SqlCommand())
        //{
        //    cmd7.CommandText = string.Format(@"select A.* , A.area_arrive_code  + ' '+  B.ship_name as showname    
        //                                           from ttArriveSitesScattered A
        //                                           left join tbShip B on A.area_arrive_code  = B.ship_id 
        //                                           where A.area_arrive_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
        //    using (DataTable dt = dbAdapter.getDataTable(cmd7))
        //    {
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            try
        //            {
        //                area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();
        //            }
        //            catch { }
        //        }
        //    }
        //}
        //#endregion

        //#region 取得零擔運輸配送商
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue);
        //    cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue);
        //    cmd.CommandText = "usp_GetDistributor";            
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    using (DataTable dt = dbAdapter.getDataTable(cmd))
        //    {
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            try
        //            {
        //                if (Convert.ToInt32(dt.Rows[0]["status_code"]) == 1)
        //                {
        //                    dlDistributor.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
        //                }
        //            }
        //            catch { }
        //        }
        //    }

        //}

        #endregion

        cbarrive_CheckedChanged(null, null);
    }

    protected void send_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 到著站簡碼
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.station_code   
                                                   from ttArriveSitesScattered A
                                                   where A.station_code <> '' and  post_city='{0}' and post_area='{1}'", send_city.SelectedValue, send_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        txtSendScode.Text = dt.Rows[0]["station_code"].ToString();
                    }
                    catch { }
                }
            }
        }
        #endregion
    }

    protected void Clear_customer()
    {
        customer_code.Text = "";
        send_contact.Text = "";          //名稱
        send_tel.Text = "";              // 電話
        send_city.SelectedValue = "";    //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";    //出貨地址-鄉鎮市區
        send_address.Text = "";          //出貨地址-路街巷弄號
        lbcustomer_name.Text = "";
    }

    protected void ddlarea_arrive_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbarrive_CheckedChanged(null, null);
    }

    protected void search_Click(object sender, EventArgs e)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                if (keyword.Text != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_code", keyword.Text);
                    cmd.Parameters.AddWithValue("@receiver_name", keyword.Text);
                    strWhereCmd += " and (A.receiver_code like '%'+@receiver_code+'%' or A.receiver_name like '%'+@receiver_name+'%') ";
                }

                //if (customer_code.Text != "")
                //{
                //cmd.Parameters.AddWithValue("@customer_code", customer_code.Text.Length >= 7 ? customer_code.Text.Substring(0, 7) : customer_code.Text);
                //strWhereCmd += " and (A.customer_code like ''+@customer_code+'%') ";
                //}

                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                strWhereCmd += " and A.customer_code = @customer_code";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List.DataSource = dt;
                    New_List.DataBind();
                }
            }
        }


    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.receiver_id=@receiver_id";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                // DT.Rows[0]["customer_code"].ToString().Trim();
                receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                city_SelectedIndexChanged(receive_city, null);
                receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                receive_area_SelectedIndexChanged(null, null);
                receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                divReceiver.Visible = false;
            }
        }

    }

    protected void btnRecsel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = true;
        search_Click(null, null);
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = false;
    }

    protected void receive_contact_TextChanged(object sender, EventArgs e)
    {
        if (receive_contact.Text != "")
        {
            entertry("receive_contact");
        }
    }


    protected void receiver_code_TextChanged(object sender, EventArgs e)
    {
        if (receiver_code.Text != "")
        {
            entertry("receiver_code");

        }
    }

    protected void entertry(string control)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                switch (control)
                {
                    case "receive_contact":
                        cmd.Parameters.AddWithValue("@receiver_name", receive_contact.Text);
                        strWhereCmd += " and  A.receiver_name like '%'+@receiver_name+'%' ";
                        break;
                    case "receiver_code":
                        cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
                        strWhereCmd += " and  A.receiver_code like '%'+@receiver_code+'%' ";
                        break;
                }
                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";

                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        if (DT.Rows.Count == 1)
                        {
                            receiver_id.Text = "";
                            receiver_code.Text = "";
                            receive_contact.Text = "";
                            receive_tel1.Text = "";
                            receive_tel1_ext.Text = "";
                            receive_tel2.Text = "";
                            receive_city.SelectedValue = "";
                            receive_area.SelectedValue = "";
                            receive_address.Text = "";

                            receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                            receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                            receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                            receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                            receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                            receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                            receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                            city_SelectedIndexChanged(receive_city, null);
                            receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                            receive_area_SelectedIndexChanged(null, null);
                            receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                            divReceiver.Visible = false;
                        }
                        else
                        {
                            New_List.DataSource = DT;
                            New_List.DataBind();
                            divReceiver.Visible = true;
                        }

                    }

                }
            }
        }

    }
    //公司地址到著指定
    protected void CheckAreaIncorrectly(object sender, EventArgs e)
    {
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            string aaa = table.Rows[0]["is_new_customer"].ToString().ToLower();
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;

        }

        //if (receive_address.Text == "文化三路二段457號4樓" || receive_address.Text == "文化三路二段457號5樓" || receive_address.Text == "文化三路二段451號4樓" || receive_address.Text == "文化三路二段451號5樓")
        //{
        //    ddlarea_arrive_code.SelectedValue = "28";
        //}
        //判斷特服區
        if (GetSpecialAreaFee(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1").id.ToString() != "0")
        {

        }
        else
        {
            //重選填地址後清空特服費相關
            lbSpecialAreaId.Text = "";
            lbSpecialAreaFee.Text = "";
            CheckAreaIncorrectly_sub();
        }
    }

    void CheckAreaIncorrectly_sub()
    {
        //var address = GetAddressParsing(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");

        var address = PreGetAddressParsing(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1");


        //走原本拆地址邏輯
        if (address.StationCode == null)
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#showalert').text('');</script><script>alert('請檢查地址是否正確');</script>", false);

            using (SqlCommand uspcmd = new SqlCommand())
            {
                DataTable dtGetDistributor = new DataTable();
                uspcmd.CommandText = @"usp_GetDistributor";
                uspcmd.CommandType = CommandType.StoredProcedure;
                uspcmd.Parameters.AddWithValue("@receive_city", receive_city.Text.Trim());
                uspcmd.Parameters.AddWithValue("@receive_area", receive_area.Text.Trim());
                dtGetDistributor = dbAdapter.getDataTable(uspcmd);

                ddlarea_arrive_code.SelectedValue = dtGetDistributor.Rows[0]["area_arrive_code"].ToString();
                lbReceiveCode.Text = dtGetDistributor.Rows[0]["area_arrive_code"].ToString();

            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#showalert').text('');</script>", false);
            ddlarea_arrive_code.SelectedValue = address.StationCode.ToString();
            lbShuttleStationCode.Text = address.ShuttleStationCode;
            lbReceiveCode.Text = address.StationCode;
            lbReceiveSD.Text = address.SalesDriverCode;
            lbReceiveMD.Text = address.MotorcycleDriverCode;
            if (ddlarea_arrive_code.SelectedValue == "99") { Label14.Text = "此區為聯運區"; }
            else if (ddlarea_arrive_code.SelectedValue == "95") { Label14.Text = "此為外島區域"; }
            else { Label14.Text = ""; }
        }
    }

    protected void CheckMoneyLimit(object sender, EventArgs e)
    {
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            is_new_customer = Convert.ToBoolean(table.Rows[0]["is_new_customer"]);
        }

        //新客戶且代收金額大於等於1萬 自動勾選保值費 
        if (string.IsNullOrEmpty(collection_money.Text)) { collection_money.Text = "0"; }
        if (string.IsNullOrEmpty(Report_fee.Text)) { Report_fee.Text = "0"; }
        if (is_new_customer == true && Convert.ToInt32(collection_money.Text) >= 10000)
        {
            chkProductValue.Checked = true;

            //取大者顯示
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }


        }
        //有勾選保值費，取大值顯示
        else if (chkProductValue.Checked == true)
        {
            //取大者顯示
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }
        }
    }

    //檢查報值金額  有勾選保值費取大值
    protected void CheckReportLimit(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(collection_money.Text)) { collection_money.Text = "0"; }
        if (string.IsNullOrEmpty(Report_fee.Text)) { Report_fee.Text = "0"; }

        if (chkProductValue.Checked == true)
        {
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }
        }
    }

    //加入超過2秒即往下執行
    private AddressParsingInfo PreGetAddressParsing(string address, string customCode, string comefrom)
    {
        //return new Tool().GetAddressParsing(address, customCode, comeform);
        AddressParsingInfo addressParse = new AddressParsingInfo();
        var tArr = new List<Task>();

        var t = Task.Factory.StartNew(() =>
        {
            return addressParse = GetAddressParsing(address, customCode, comefrom);
        });
        tArr.Add(t);
        Task.WaitAll(tArr.ToArray(), 2000);

        return addressParse;
    }



    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                    area_arrive_code = Info.StationCode;

                    //如果到著站空白,不給過
                    if (string.IsNullOrEmpty(area_arrive_code))
                    {
                        check_address = false;
                        return null;
                    }
                }
            }

            catch (Exception e)
            {

            }
        }
        else
        {

        }
        return Info;
    }


    public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        SpecialAreaManage Info = new SpecialAreaManage();
        var AddressParsingURL = "http://map2.fs-express.com.tw/Address/GetSpecialAreaDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);
        request.Timeout = 5000;
        request.ReadWriteTimeout = 5000;
        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

        var response = client.Post<ResData<SpecialAreaManage>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                Info = response.Data.Data;
                if (Info.id > 0)
                {

                    //新客戶才顯示特服區提示 
                    bool is_new_customer = false;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select is_new_customer from tbCustomers where customer_code = @customer_code";
                        cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                        var table = dbAdapter.getDataTable(cmd);
                        is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
                    }
                    if (is_new_customer == true) { is_special_area.Text = "此區為特服區，費用請洽官網"; }
                    //舊客代提示該區為特服區
                    else { is_special_area.Text = "此區為特服區"; }
                    ddlarea_arrive_code.SelectedValue = Info.StationCode;
                    area_arrive_code = ddlarea_arrive_code.SelectedValue;
                    lbSpecialAreaId.Text = Info.id.ToString();
                    lbSpecialAreaFee.Text = Info.Fee.ToString();
                    lbReceiveCode.Text = Info.StationCode;
                    lbReceiveSD.Text = Info.SalesDriverCode;
                    lbReceiveMD.Text = Info.MotorcycleDriverCode;
                }
            }
            catch (Exception e)
            {
                throw e;

            }

        }
        else
        {
            //throw new Exception("地址解析有誤");
        }

        return Info;
    }

    //保值費
    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        //代收金額、報值金額 取大者*1%
        try
        {

            string strcollection_money = string.IsNullOrEmpty(collection_money.Text) ? "0" : collection_money.Text;
            string strReport_fee = string.IsNullOrEmpty(Report_fee.Text) ? "0" : Report_fee.Text;
            if (Convert.ToInt32(strcollection_money) >= Convert.ToInt32(strReport_fee))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strcollection_money) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(strcollection_money) < Convert.ToInt32(strReport_fee))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strReport_fee) * 0.01)).ToString();
            }
            if (chkProductValue.Checked == false) { tbProductValue.Text = ""; }
            //若取消勾選,檢查代收金額欄位是否>1萬欄位
            if (chkProductValue.Checked == false && Convert.ToInt32(collection_money.Text.Trim()) >= 10000)
            {
                chkProductValue.Checked = true;
                if (Convert.ToInt32(strcollection_money) >= Convert.ToInt32(strReport_fee))
                {
                    tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strcollection_money) * 0.01)).ToString();
                }
                else if (Convert.ToInt32(strcollection_money) < Convert.ToInt32(strReport_fee))
                {
                    tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strReport_fee) * 0.01)).ToString();
                }
            }
        }
        catch { }

    }
}