﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_3_7.aspx.cs" Inherits="report_3_7" %>

<%@ Register src="uc/uc_PeidaReport.ascx" tagname="uc_PeidaReport" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">逆物流配達統計</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">

                    <div class="col-lg-8 col-md-8 form-group form-inline">
                        <label >發送日期</label>
                        <asp:TextBox ID="dates" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        ~ 
                        <asp:TextBox ID="datee" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <asp:Label ID="Label2" runat="server" Text="客代"></asp:Label>
                        <asp:DropDownList ID="dlcustomer_code" runat="server"  CssClass="chosen-select" ></asp:DropDownList>

                        <asp:Label ID="Label1" runat="server" Text="類型"></asp:Label>
                        <asp:DropDownList ID="ddlRtype" runat="server"  CssClass="chosen-select" ></asp:DropDownList>
                        
                        <asp:Button ID="search" CssClass="btn btn-darkblue btn_view" runat="server" Text="查詢" OnClick ="search_Click" />
                    </div>

                    <div class="col-lg-4 col-md-4 form-group form-inline text-right">
                        
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                    </div>
                </div>
            </div>
            <hr>
            <p>※午前配達筆數：於13:00前掃讀配達之筆數；當日配達筆數：於17:00前掃讀配達之筆數</p>
            <uc1:uc_PeidaReport ID="uc_PeidaReport1" runat="server" />
        </div>
    </div>
</asp:Content>

