﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterDispatch.master" AutoEventWireup="true" CodeFile="dispatch1_2.aspx.cs" Inherits="dispatch1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

        });
        $(document).ready(function () {

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 15;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

            var _width = $(document).width() * 0.8;  //850;
            var _height = $(document).height() * 0.8;//300;

            $("#editclick").fancybox({
                'width': _width,
                'height': _height,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
        });


        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        function OpenLinkByFancyBox(the_url, df_width, df_height) {
            if ($.trim(the_url) != "") {

                if (the_url.indexOf('?') >= 0) {
                    the_url = the_url + '&r=' + Math.random();
                } else {
                    the_url = the_url + '?r=' + Math.random();
                }

                var _width = $(document).width() * 0.8;  //850;
                var _height = $(document).height() * 0.8;//300;
                if (typeof (df_width) != "undefined") {
                    _width = parseInt(df_width);
                }
                if (typeof (df_height) != "undefined") {
                    _height = parseInt(df_height);
                }

                $.fancybox({
                    'type': 'iframe',
                    'width': _width,
                    'height': _height,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'href': the_url
                });
            }
        }

    </script>

    <style>
        ._th {
            text-align: center;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">自營客戶集貨</h2>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label>發送日期：</label>
                            <asp:TextBox ID="date" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>
                            <%--<asp:TextBox ID="dateS" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                            <asp:TextBox ID="dateE" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>--%>
                            <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="search_Click" ValidationGroup="validate" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <div>
                        <label>關鍵字：</label>
                        <asp:TextBox ID="keyword" runat="server" class="form-control " autocomplete="off"></asp:TextBox>
                    </div>
                    <div class="text-right ">
                        <%--<a href="dispatch1_1_edit.aspx"  ><span class="btn btn-warning glyphicon glyphicon-plus">新增任務</span></a>--%>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="AddTask" CssClass="btn btn-warning glyphicon glyphicon-plus" runat="server" Text="新增任務" OnClick="AddTask_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="max-height: 600px; overflow: auto;">
                            <table id="custom_table" class="table table-striped table-bordered templatemo-user-table" />
                            <tr class="tr-only-hide">
                                <th class="_th">計價模式</th>
                                <th class="_th">發送日期</th>
                                <th class="_th">貨號</th>
                                <th class="_th">出貨站所</th>
                                <th class="_th">出貨地點</th>
                                <th class="_th">出貨板數</th>
                                <th class="_th">出貨件數</th>
                                <th class="_th">收件人</th>
                                <th class="_th">配送區配商 </th>
                                <th class="_th">
                                    <asp:CheckBox ID="chkHeader" runat="server" class="font-weight-400 checkbox checkbox-success" Text="批次派遣" /></th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                        <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pricing_type_name").ToString())%></td>
                                        <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%></td>
                                        <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                        <td data-th="出貨站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                                        <td data-th="出貨地點"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_address").ToString())%></td>
                                        <td data-th="出貨板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                        <td data-th="出貨件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> </td>
                                        <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                        <td data-th="配送區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                        <td data-th="批次派遣">

                                            <asp:CheckBox ID="chkRow" class="font-weight-400 checkbox checkbox-success" runat="server" Text="&nbsp;" Visible='<%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_id").ToString())==""? true :false  %>' />
                                            <a href="dispatch1_1_edit.aspx?id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.t_id").ToString())%>" class="fancybox fancybox.iframe " id="editclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_id").ToString())%></a>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="10" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                </table>
                        </div>

                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
                    <div id="page-nav" class="page"></div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnQry" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</asp:Content>

