﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>峻富雲端物流管理系統</title>
    <!-- InstanceEndEditable -->
    <link rel="alternate icon" type="image/png" href="img/favicon.png">
    <!-- Core CSS - Include with every page -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- SB Admin CSS - Include with every page -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- InstanceBeginEditable name="head" -->
    <!-- InstanceEndEditable -->
    <script src="js/bootstrap.min.js"></script>


</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <div>
                <div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">為了帶給您更穩定可靠的物流管理品質，即日起首次登入峻富物流系統，我們將會寄發一組新密碼給您，<br/>
                            往後請使用新密碼登入系統。請在下方輸入帳號與您註冊帳號時所登記的 E-mail我們將寄發新密碼通知信給您</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="form-group form-inline">
                                <label class="col-sm-2 control-label">帳號</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="account" CssClass="form-control" runat="server"></asp:TextBox><asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="account" ForeColor="Red" ValidationGroup="validate">請輸入帳號</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <label class="col-sm-2 control-label">E-Mail</label>
                                <div class="col-sm-10">
                                    <asp:TextBox ID="email" CssClass="form-control" runat="server"></asp:TextBox><asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="email" ForeColor="Red" ValidationGroup="validate">請輸入電子郵件</asp:RequiredFieldValidator>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnsend" CssClass="btn btn-primary" runat="server" Text="送出" ValidationGroup="validate" OnClick="btnsend_Click" />
                    <asp:Button ID="btncancel" CssClass="btn btn-default" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />

                </div>
            </div>
        </div>
    </form>
    
</body>
</html>
