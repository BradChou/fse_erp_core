﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class assets1_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 車行
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "Select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'CD' and active_flag = 1 order by code_id ";
                ddl_car_retailer.DataSource = dbAdapter.getDataTable(cmd);
                ddl_car_retailer.DataValueField = "code_id";
                ddl_car_retailer.DataTextField = "code_name";
                ddl_car_retailer.DataBind();
                ddl_car_retailer.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 所有權
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'OS' and active_flag = 1 ";
            dlownership.DataSource = dbAdapter.getDataTable(cmd2);
            dlownership.DataValueField = "code_id";
            dlownership.DataTextField = "code_name";
            dlownership.DataBind();
            dlownership.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 使用單位
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=1", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  code_id=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 {0} order by code_id", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                dldept.DataSource = dt;
                dldept.DataValueField = "code_id";
                dldept.DataTextField = "code_name";
                dldept.DataBind();
                dldept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (car_license.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_license", car_license.Text.ToString());
            wherestr += "  and REPLACE ( a.car_license,'-','')  like '%'+Replace(@car_license,'-','') +'%'";
        }


        if (ddl_car_retailer.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_retailer", ddl_car_retailer.SelectedValue.ToString());
            wherestr += "  and a.car_retailer =@car_retailer";
        }

        if (driver.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@driver", driver.Text.ToString());
            wherestr += "  and a.driver like '%'+@driver+'%'";
        }

        if (keyword.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
            wherestr += "  and (a.memo like '%'+@keyword+'%' or a.assets_name like '%'+@keyword+'%')";
        }

        if (dlownership.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@ownership", dlownership.SelectedValue.ToString());
            wherestr += "  and a.ownership =@ownership";
        }

        if (dldept.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@dept", dldept.SelectedValue.ToString());
            wherestr += "  and a.dept =@dept";
        }

        if (chk_inspection.Checked)
        {
            DateTime FirstDay = DateTime.Now.AddDays(-DateTime.Now.Day + 1);
            DateTime LastDay = DateTime.Now.AddMonths(1).AddDays(-DateTime.Now.AddMonths(1).Day);


            objCommand.Parameters.AddWithValue("@sdate", FirstDay);
            objCommand.Parameters.AddWithValue("@edate", LastDay);
            wherestr += "  and a.next_inspection_date >=@sdate and a.next_inspection_date <=@edate";
        }
        //objCommand.CommandText = string.Format(@"select A.* , B.AcntName 
        //                                            from ttAssets A with(nolock)
        //                                            left join tbAccountingSubject B with(nolock) on A.acntid  = B.Acntid
        //                                        where 1=1  {0} 
        //                                        order by a.get_date asc", wherestr);
        objCommand.CommandText = string.Format(@"select A.* , OS.code_name as ownership_text ,CD.code_name as car_retailer_text  , CT.code_name  as cabin_type_text,
                                                    CASE WHEN A.ETC = 'True' then '是' else '否' end 'isETC',
                                                    DT.code_name as 'dept_name', D.user_name , A.udate
                                                    from ttAssets A with(nolock)
                                                    left join tbItemCodes OS with(nolock) on A.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                    left join tbItemCodes CD with(nolock) on A.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD'
                                                    left join tbItemCodes CT with(nolock) on A.cabin_type  = CT.code_id and CT.code_bclass = '6' and CT.code_sclass= 'cabintype'
                                                    left join tbItemCodes DT with(nolock) on A.dept  = DT.code_id and DT.code_bclass = '6' and DT.code_sclass= 'dept'
                                                    left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                where 1=1  {0} 
                                                order by a.get_date asc", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {   
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = (p["tires_number"] == DBNull.Value ) ? 0 : p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = (p["oil_discount"] == DBNull.Value) ? 0 : p.Field<double>("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = (p["tires_monthlyfee"] == DBNull.Value) ? 0 : p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    protected void btExport2_Click(object sender, EventArgs e)
    {
        string sheet_title = "車輛主檔";
        string file_name = "車輛主檔" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "";
            if (car_license.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@car_license", car_license.Text.ToString());
                wherestr += "  and a.car_license like '%'+@car_license+'%'";
            }


            if (ddl_car_retailer.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@car_retailer", ddl_car_retailer.SelectedValue.ToString());
                wherestr += "  and a.car_retailer =@car_retailer";
            }

            if (driver.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@driver", driver.Text.ToString());
                wherestr += "  and a.driver like '%'+@driver+'%'";
            }

            if (keyword.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
                wherestr += "  and (a.memo like '%'+@keyword+'%' or a.assets_name like '%'+@keyword+'%')";
            }

            if (dlownership.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@ownership", dlownership.SelectedValue.ToString());
                wherestr += "  and a.ownership =@ownership";
            }

            if (dldept.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@dept", dldept.SelectedValue.ToString());
                wherestr += "  and a.dept =@dept";
            }

            if (chk_inspection.Checked)
            {
                objCommand.Parameters.AddWithValue("@sdate", DateTime.Today);
                objCommand.Parameters.AddWithValue("@edate", DateTime.Today.AddDays(1));
                wherestr += "  and a.next_inspection_date >=@sdate and a.next_inspection_date <@edate";
            }
            objCommand.CommandText = string.Format(@"Select A.car_license '車牌', OS.code_name '所有權', DT.code_name '使用單位', A.tonnes '噸數', A.brand '廠牌', CD.code_name '車行', A.owner '車主', A.year '年份', A.month '月份'
                                                    , A.engine_number '引擎號碼', A.cc '排氣量' , A.tires_number '輪胎數', CT.code_name '車廂型式', TP.code_name '溫層'
                                                    , CASE WHEN A.tailgate =1 then '是' else '否' end '是否有尾門',  CASE WHEN A.vision_assist =1 then '是' else '否' end '視野輔助'
                                                    , A.oil '油卡', A.oil_discount '油料折扣', CASE WHEN A.ETC = 'True' then '是' else '否' end 'ETC', A.tires_monthlyfee '輪胎統包'
                                                    , A.driver '司機' , A.purchase_date '購入日期', A.next_inspection_date '下次驗車日期', A.memo '備註'
                                                    from ttAssets A with(nolock)
                                                    left join tbItemCodes OS with(nolock) on A.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                    left join tbItemCodes CD with(nolock) on A.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD'
                                                    left join tbItemCodes CT with(nolock) on A.cabin_type  = CT.code_id and CT.code_bclass = '6' and CT.code_sclass= 'cabintype'
                                                    left join tbItemCodes DT with(nolock) on A.dept  = DT.code_id and DT.code_bclass = '6' and DT.code_sclass= 'dept'
                                                    left join tbItemCodes TP with(nolock) on A.temperate  = TP.code_id and TP.code_bclass = '6' and TP.code_sclass= 'temp'
                                                    where 1=1  {0} 
                                                    order by a.get_date asc", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }
}