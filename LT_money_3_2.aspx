﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterMoney.master" AutoEventWireup="true" CodeFile="LT_money_3_2.aspx.cs" Inherits="LT_money_3_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(".btn_view").click(function () {
                showBlockUI();
            });

            $("#uploadclick").fancybox({
                'width': 980,
                'height': 600,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'afterClose': function () {
                    parent.location.reload(true);
                },
                'onClosed': function () {
                    
                   
                }
             });

        });
        $.fancybox.update();

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後....!<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        
    </script>
    <style type="text/css">
        .tb_search {
            width: auto;
        }

        ._td btn {
            margin: auto 3px;
        }

        ._td1 {
            text-align: center;
            padding: 5px 30px 0px 10px;
        }

        ._td2 {
            background-color: #f9f9f9;
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <table>
                <tr>
                    <td>
                        <h2 class="margin-bottom-10">運價表維護 </h2>
                    </td>
                    <td>
                       <%-- <span class="text-danger span_tip">※ 開放修改時期間：每月 20~25 日</span>--%>
                    </td>
                </tr>
            </table>

            <div class="templatemo-login-form">
                <table class="tb_search">
                    <tr>
                        <td class="_td _td1">
                            <h3>查 詢 </h3>
                            <hr />
                            <div class="form-group form-inline ">
                               <%-- <label for="dlbusiness">運價表</label>--%>
                                <asp:DropDownList ID="dlbusiness" runat="server" CssClass=" form-control" Visible="false">
                                    <asp:ListItem Value="1">經理</asp:ListItem>
                                    <asp:ListItem Value="2">協理</asp:ListItem>
                                    <asp:ListItem Value="3">總經理</asp:ListItem>
                                </asp:DropDownList>
                                 <label for="">定價日期</label> <asp:DropDownList ID="money_date" runat="server" CssClass=" form-control" >                                 
                                </asp:DropDownList>
                               <%-- <asp:LinkButton ID="lbRefreshDate" runat="server" CssClass=" text-hide " OnClick="lbRefreshDate_Click"></asp:LinkButton>--%>

                                <label for="dlCbmSize">材積大小</label>
                                <asp:DropDownList ID="dlCbmSize" runat="server" CssClass=" form-control"></asp:DropDownList>

                                <asp:Button ID="btQry" class="btn btn-primary btn_view" runat="server" Text="預　覽" OnClick="btQry_Click" />                                
                                <asp:Button ID="btndownload" class="btn btn-default" runat="server" Text="下　載" OnClick="btndownload_Click" />
                            </div>
                        </td>
                    </tr>

                </table>

                <hr />
                <div class="form-group form-inline" runat="server" id="div_Upload">
                    <a href="LTPriceUpload.aspx?fee_type=<%=dlbusiness.SelectedValue  %>" class="fancybox fancybox.iframe" id="uploadclick"><span class="btn btn-primary">上傳新運價</span></a>
                    <span class="text-danger span_tip">※ 請注意！！上傳後會覆蓋舊運價</span>
                </div>
                <div style="overflow: auto; height: calc(100vh - 495px); width: 100%">
                    <asp:Panel ID="pan_PriceList" runat="server">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>起點區城</th>
                                <th>迄點區域</th>
                                <th>材積大小</th>
                                <th>首件費用</th>
                                <th>續件費用</th>  
                                <th></th>             
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="起點區城"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.start_city").ToString())%> </td>
                                        <td data-th="迄點區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.end_city").ToString())%> </td>            
                                        <td data-th="材積大小"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sizename").ToString())%></td>
                                        <td data-th="首件費用">
                                            <asp:TextBox runat="server" ID="txt_first_price" CssClass="form-control" MaxLength="10"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"
                                                ReadOnly='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.first_price").ToString()) != "" %>'
                                                Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.first_price", "{0:N0}").ToString())%>'></asp:TextBox>
                                        </td>
                                        <td data-th="續件費用">
                                            <asp:TextBox runat="server" ID="txt_add_price" CssClass="form-control" MaxLength="10"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"
                                                ReadOnly='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.add_price").ToString()) != "" %>'
                                                Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.add_price", "{0:N0}").ToString())%>'></asp:TextBox>
                                        </td>
                                        <td data-th=""> 
                                            <asp:Button runat="server" ID="btnMob" Text="修 改" CommandName="Mod" CssClass="btn templatemo-white-button" />
                                            <asp:Button runat="server" ID="btnSet" CommandName="Set" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' 
                                                Text="確 認" CssClass="btn  btn-primary" Visible="false" OnClientClick ="return confirm('確定修改運價嗎?');" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="6" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </asp:Panel>


                </div>

            </div>
        </div>
    </div>
</asp:Content>

