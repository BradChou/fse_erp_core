﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LTPriceUpload.aspx.cs" Inherits="LTPriceUpload" %>

<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>峻富雲端物流管理-上傳新運價</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">   
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>

    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>
    <script>
        $(function () {
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date()),  //預設當日
                beforeShow: function () {
                    if ($(this).attr('maxDate')) {
                        var dateItem = $('.' + $(this).attr('maxDate'));
                        if (dateItem.val() !== "") {
                            $(this).datepicker('option', 'maxDate', dateItem.val());
                        }
                    }

                    if ($(this).attr('minDate')) {
                        var dateItem = $('.' + $(this).attr('minDate'));
                        if (dateItem.val() !== "") {
                            $(this).datepicker('option', 'minDate', dateItem.val());
                        }
                    }
                }
            });
       
        });

     </script>
    <style>
        .tb_fee {
            margin: auto;
        }

        ._th {
            width: 80px;
        }

        ._td {
            width: 320px;
        }

        ._td_btn {
            text-align: center;
        }

        .btn {
            margin: 3px 8px;
        }

        .bk_color1 {
            background-color: #eff0f1;
        }
        .ui-datepicker-title{
            color:black
        }
    </style>
</head>
<body>
     <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇新運價檔案</h4>
                    </div>
                    <table class="table tb_fee">
                            <tr>
                                <th>運價生效日</th>
                                <td>
                                    <asp:TextBox ID="Enable_date" runat="server" CssClass="form-control date_picker" autocomplete="off"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_date" runat="server" ControlToValidate="Enable_date" ForeColor="Red" ValidationGroup="validate">啟用日期</asp:RequiredFieldValidator>

                                    <%--類型：
                            <asp:DropDownList ID="dlbusiness" runat="server" CssClass=" form-control" Visible="false">
                                <asp:ListItem Value="1">經理</asp:ListItem>
                                <asp:ListItem Value="2">協理</asp:ListItem>
                                <asp:ListItem Value="3">總經理</asp:ListItem>
                            </asp:DropDownList>--%>
                                </td>
                            </tr>
                            <tr class="_tr_upload">
                                <th>上傳檔案
                                </th>
                                <td>
                                    <asp:FileUpload ID="file02" runat="server" Width="300px" CssClass="_feefile"/> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="_td_btn">
                                    <asp:Button ID="btImport" runat="server" CssClass="btn btn-primary " Text="確定" OnClick="btImport_Click" ValidationGroup="validate" />
                                    <asp:Button ID="btncancel" runat="server" CssClass="btn btn-default  " Text="取消" OnClientClick="parent.$.fancybox.close();" />
                                    <asp:DropDownList ID="dpZIP" runat="server" Visible="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    <div class="modal-body">
                        <div style="overflow: auto; height:500px; vertical-align :top;">
                            <asp:ListBox ID="lbMsg" runat="server" Height="490px" Width="100%"></asp:ListBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
