﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets2_1.aspx.cs" Inherits="assets2_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn-primary").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".btn-primary").click(function () {
                    showBlockUI();
                });

                jQuery(function ($) {
                    var pageParts = $(".paginate");
                    var numPages = pageParts.length;
                    var perPage = 10;
                    pageParts.slice(perPage).hide();
                    $("#page-nav").pagination({
                        items: numPages,
                        itemsOnPage: perPage,
                        cssStyle: "light-theme",
                        prevText: '上一頁',
                        nextText: '下一頁',
                        onPageClick: function (pageNum) {
                            var start = perPage * (pageNum - 1);
                            var end = start + perPage;

                            pageParts.hide()
                                     .slice(start, end).show();
                            Gio951753

                        }
                    });
                });
            }
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">廠商請款資料</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="Upd_title" runat="server" UpdateMode ="Conditional">
                        <ContentTemplate>
                            <label>費用類別：</label>
                            <asp:DropDownList ID="ddl_fee" runat="server" class="form-control " AutoPostBack ="true" OnSelectedIndexChanged="ddl_fee_SelectedIndexChanged"></asp:DropDownList>
                            <asp:Label ID="lb_oil" runat="server" Text="油品公司" Visible="false"></asp:Label>
                            <asp:DropDownList ID="ddl_oil" runat="server" class="form-control " Visible="false">
                                <asp:ListItem Value ="">全部</asp:ListItem>
                                <asp:ListItem>全國</asp:ListItem>
                                <asp:ListItem>台塑</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lb_company" runat="server" Text="廠商" Visible="false"></asp:Label>
                            <asp:TextBox ID="company" runat="server" class="form-control "  Visible="false"></asp:TextBox>
                            <label>查詢區間：</label>
                            <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                            <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                            <label>車牌：</label>
                            <asp:TextBox ID="car_license" runat="server" class="form-control "></asp:TextBox>
                            <label>使用單位：</label>
                            <asp:DropDownList ID="ddl_dept" runat="server" class="form-control "></asp:DropDownList>
                            <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                            <asp:Button ID="export" CssClass="btn btn-success" runat="server" Text="下　載" OnClick="export_Click" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID ="export" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <label>車行：</label>
                    <asp:DropDownList ID="ddl_car_retailer" runat="server" class="form-control "></asp:DropDownList>
                    <label>車主：</label>
                    <asp:TextBox ID="owner" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="form-group" style="float: right;">
                    <a href="assets2_1_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>
                
                <p class="text-primary">※點選任一資料可查詢及修改</p>
                <asp:UpdatePanel ID="Upd_data" runat="server">
                    <ContentTemplate>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="_th"></th>
                                <th class="_th">費用類別</th>
                                <th class="_th">交易日期</th>
                                <th class="_th">車牌</th>
                                <th class="_th">噸數</th>
                                <th class="_th">使用單位</th>
                                <th class="_th">車行</th>
                                <th class="_th">車主</th>
                                <th class="_th">司機</th>
                                <th class="_th">數量</th>
                                <th class="_th">里程數</th>                                
                                <th class="_th">牌價單價<br />
                                    (未稅)</th>
                                <th class="_th">牌價單價<br />
                                    (含稅)</th>
                                <th class="_th">牌價總額<br />
                                    (未稅)</th>
                                <th class="_th">購入單價<br />
                                    (未稅)</th>
                                <th class="_th">購入單價<br />
                                    (含稅)</th>
                                <th class="_th">購入總額<br />
                                    (未稅)</th>
                                <th class="_th">收款單價<br />
                                    (未稅)</th>
                                <th class="_th">收款單價<br />
                                    (含稅)</th>
                                <th class="_th">收款總額<br />
                                    (未稅)</th>                               
                                <th class="_th">備註</th>
                                <th class="_th">異動人員</th>
                                <th class="_th">異動日期</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr class="paginate">
                                        <td>
                                            <a href="assets2_1_edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>" class="btn btn-info  " id="updclick">修改</a>
                                        </td>
                                        <%--<td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @fee_type and code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1","fee_type",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.fee_type").ToString()))%></td>--%>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.feename").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.date","{0:yyyy-MM-dd}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%></td>
                                        <%--<td><%#Func.GetRow("tonnes","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>--%>                                        
                                        <%--<td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1","code_id",Func.GetRow("dept","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())))%></td>
                                        <td><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass= 'CD'","code_id",Func.GetRow("car_retailer","ttAssets  ","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())))%></td>
                                        <td><%#Func.GetRow("owner","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>
                                        <td><%#Func.GetRow("driver","ttAssets","","car_license = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%></td>--%>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.deptname").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_retailer_name").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.owner").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.showquant").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.milage").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price_notax","{0:N2}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N2}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_price","{0:N0}").ToString())%></td>                                        
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.buy_price_notax","{0:N2}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.buy_price","{0:N2}").ToString())%></td>                                        
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_buy","{0:N0}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receipt_price_notax","{0:N2}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receipt_price","{0:N2}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_receipt","{0:N0}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="23" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>

                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
                <div id="page-nav" class="page"></div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </div>
        </div>
    </div>
</asp:Content>

