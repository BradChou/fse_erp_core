﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;

public partial class money_2 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            //string supplier_code = Session["master_code"].ToString();
            //supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            //string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where ISNULL(supplier_code ,'') <> '' and active_flag=1 order by supplier_code");
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion

            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if (Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }
            lbdate.Text = dates.Text + "~" + datee.Text;

            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            }

            if (Request.QueryString["close_type"] != null)
            {
                dlclose_type.SelectedValue = Request.QueryString["close_type"];
            }
            dlclose_type_SelectedIndexChanged(null, null);
            if (Request.QueryString["closeday"] != null)
            {
                dlCloseDay.SelectedValue = Request.QueryString["closeday"];
            }
            
            readdata();

            string sScript = string.Empty;
            switch (dlclose_type.SelectedValue)
            {
                case "1":
                    sScript = " $('._close').show(); " +
                               " $('._daterange').html('請款期間');  ";
                    break;

                case "2":
                    sScript = " $('._close').hide(); " +
                              " $('._daterange').html('發送期間'); ";
                    break;
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

        }
    }

    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            //cmd.CommandText = "usp_GetAccountsPayable";
            cmd.CommandText = "usp_GetAccountsPayablebyJUNFU";   
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue );  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable_C(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null , "");
                if ( dt!= null && dt.Rows.Count >0 && _info != null  )
                {
                    DataRow row = dt.NewRow();
                    row["到著站"] = "總計";
                    row["板數"] = _info.plates;
                    row["件數"] = _info.pieces;
                    row["cscetion_fee"] = _info.subtotal;
                    row["remote_fee"] = _info.remote_fee;
                    dt.Rows.Add(row);
                }


                New_List_01.DataSource = dt;
                New_List_01.DataBind();

                if (_info != null)
                {
                    subtotal_01.Text = _info.subtotal.ToString("N0");
                    tax_01.Text = _info.tax.ToString("N0");
                    total_01.Text = _info.total.ToString("N0");
                }


            }
        }


        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsPayablebyJUNFU";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        New_List.DataSource = dt;
                        New_List.DataBind();
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        //for (int i = Suppliers.SelectedIndex; i <= Suppliers.Items.Count - 1; i++)
        //{
        //    if (Suppliers.Items[i].Value != "")
        //    {
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.CommandText = "usp_GetAccountsPayable";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@customer_code", Suppliers.Items[i].Value);  //客代
        //            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
        //            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
        //            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
        //            using (DataTable dt = dbAdapter.getDataTable(cmd))
        //            {
        //                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null);
        //                New_List_01.DataSource = dt;
        //                New_List_01.DataBind();

        //                if (_info != null)
        //                {
        //                    subtotal_01.Text = _info.subtotal.ToString("N0");
        //                    tax_01.Text = _info.tax.ToString("N0");
        //                    total_01.Text = _info.total.ToString("N0");
        //                }


        //            }
        //        }


        //        try
        //        {
        //            using (SqlCommand cmd = new SqlCommand())
        //            {
        //                cmd.CommandText = "usp_GetSumAccountsPayable";
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
        //                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
        //                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
        //                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
        //                using (DataTable dt = dbAdapter.getDataTable(cmd))
        //                {
        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        New_List.DataSource = dt;
        //                        New_List.DataBind();
        //                    }

        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            string strErr = string.Empty;
        //            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

        //            PublicFunction _fun = new PublicFunction();
        //            _fun.Log(strErr, "S");
        //        }


        //        if (Suppliers.Items[i].Value == Suppliers.SelectedValue)
        //        {
        //            break;
        //        }
        //    }
        //}

    }

    protected void Suppliers_TextChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);
                wherestr = " and customer_code = @customer_code";
            }
            cmd.CommandText = string.Format(@"select customer_code,customer_shortname  from tbCustomers where 0=0 {0} ", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                lbSuppliers.Text = dt.Rows[0]["customer_shortname"].ToString();
            }
        }
        #endregion
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbSuppliers.Text = Suppliers.SelectedItem.Text;

    }

    protected void btQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {

        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        querystring += "&close_type=" + dlclose_type.SelectedValue.ToString();

        if (dlCloseDay.SelectedValue != "")
        {
            querystring += "&closeday=" + dlCloseDay.SelectedValue.ToString();
        }

        Response.Redirect(ResolveUrl("~/money_2.aspx?search=yes" + querystring));
    }

    protected void dlclose_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        if (dlclose_type.SelectedValue == "1")
        {
            string wherestr = "";
            SqlCommand cmd2 = new SqlCommand();
            if (Suppliers.SelectedValue != "")
            {
                cmd2.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                wherestr = " and supplier_code = @supplier_code";
            }
            cmd2.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd2.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd2.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from tcDeliveryRequests with(nolock)   
                                                   where 0=0 and checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
            dlCloseDay.DataSource = dbAdapter.getDataTable(cmd2);
            dlCloseDay.DataValueField = "checkout_close_date";
            dlCloseDay.DataTextField = "checkout_close_date";
            dlCloseDay.DataBind();
            dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));

        }
        #endregion
    }

    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        if (Suppliers.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇客戶代號');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue);   //客代
                cmd.Parameters.AddWithValue("@close_type", "2");                          //結帳程序類別(1:自動 2:手動)
                //dbAdapter.execNonQuery(cmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {

            strErr = "acc_post" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }
        return;
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = null;
        string Title = Suppliers.SelectedItem.Text;     

        Title = "峻富物流股份有限公司";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue);  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            dt = dbAdapter.getDataTable(cmd);
            _info = Utility.GetSumAccountsPayable_C(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null, "");
        }


        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("應付帳款");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 30;
            sheet1.Column(6).Width = 20;
            sheet1.Column(7).Width = 10;
            sheet1.Column(8).Width = 10;
            sheet1.Column(9).Width = 10;
            sheet1.Column(10).Width = 10;
            sheet1.Column(11).Width = 10;



            sheet1.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 11].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 11].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 11].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
            sheet1.Cells[3, 2, 3, 2].Value = "區配商：" + Suppliers.SelectedItem.Text;
            sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            sheet1.Cells[4, 1, 4, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 11].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 1, 4, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 2].Value = "發送日期";
            sheet1.Cells[4, 3].Value = "配送日期";
            sheet1.Cells[4, 4].Value = "客戶代碼";
            sheet1.Cells[4, 5].Value = "客戶名稱";
            sheet1.Cells[4, 6].Value = "貨號";
            sheet1.Cells[4, 7].Value = "發送區";
            sheet1.Cells[4, 8].Value = "到著區";
            sheet1.Cells[4, 9].Value = "件數";
            sheet1.Cells[4, 10].Value = "板數";
            sheet1.Cells[4, 11].Value = "貨件運費";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 5, 2].Value = dt.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 3].Value = dt.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 4].Value = dt.Rows[i]["客戶代碼"].ToString();
                sheet1.Cells[i + 5, 5].Value = dt.Rows[i]["客戶名稱"].ToString();
                sheet1.Cells[i + 5, 6].Value = dt.Rows[i]["貨號"].ToString();
                sheet1.Cells[i + 5, 7].Value = dt.Rows[i]["發送站"].ToString();
                sheet1.Cells[i + 5, 8].Value = dt.Rows[i]["到著站"].ToString();
                sheet1.Cells[i + 5, 9].Value = Convert.ToInt32(dt.Rows[i]["件數"]);
                sheet1.Cells[i + 5, 9].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 10].Value = Convert.ToInt32(dt.Rows[i]["板數"]);
                sheet1.Cells[i + 5, 10].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 11].Value = Convert.ToInt32(dt.Rows[i]["cscetion_fee"]);

                sheet1.Cells[i + 5, 11].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    sheet1.Cells[i + 6, 8].Value = "小計";
                    sheet1.Cells[i + 6, 9].Value = _info.plates;
                    sheet1.Cells[i + 6, 10].Value = _info.pieces;
                    sheet1.Cells[i + 6, 11].Value = _info.subtotal;
                    sheet1.Cells[i + 6, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 6, 1, i + 6, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 6, 1, i + 6, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                    sheet1.Cells[i + 7, 8].Value = "5%營業稅";
                    sheet1.Cells[i + 7, 11].Value = _info.tax;
                    sheet1.Cells[i + 7, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 7, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 7, 1, i + 7, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 7, 1, i + 7, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[i + 8, 8].Value = "應付帳款";
                    sheet1.Cells[i + 8, 11].Value = _info.total;
                    sheet1.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 8, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 8, 1, i + 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 8, 1, i + 8, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                }
            }



            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("應付帳款明細表.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}