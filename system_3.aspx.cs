﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class system_3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            DDLSet();
            CBLSet_Area();
        }
        GetAreaForSupplier();

        //已被設定的區域設唯讀
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CallSetCkb", "<script>$(\"span[the_status = -1]\").find(\"input[type = checkbox]\").attr(\"disabled\", \"disabled\");</script>", false);

    }

    protected void DDLSet()
    {
        string strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            //縣市
            strSQL = @"select seq sn,city 'name'  from  tbPostCity With(Nolock)";
            cmd.CommandText = strSQL;
            ddl_city.DataSource = dbAdapter.getDataTable(cmd);
            ddl_city.DataValueField = "sn";
            ddl_city.DataTextField = "name";
            ddl_city.DataBind();
            //ddl_city.Items.Insert(0, new ListItem("請選擇縣市別", ""));

            //區配商
            strSQL = @"SELECT supplier_id sn,supplier_name+'('+supplier_code+')' 'name' FROM  tbSuppliers With(Nolock) WHERE active_flag =1 and supplier_code != ''";
            cmd.CommandText = strSQL;
            ddl_supplyer.DataSource = dbAdapter.getDataTable(cmd);
            ddl_supplyer.DataValueField = "sn";
            ddl_supplyer.DataTextField = "name";
            ddl_supplyer.DataBind();
            //ddl_supplyer.Items.Insert(0, new ListItem("請選擇區配商", ""));
        }
    }

    protected void CBLSet_Area()
    {
        string strSQL = string.Empty;
        cbl_area.Items.Clear();
        using (SqlCommand cmd = new SqlCommand())
        {
            //區域設定 
            strSQL = @"SELECT area.seq area_sn ,area.area
                             ,ISNULL(supp.supplier_name,'') +' ' +ISNULL(arr.supplier_code,'') supplier
                             ,CASE WHEN ISNULL(supp.supplier_id,'0')  ='0' THEN '0'
                                   WHEN ISNULL(supp.supplier_id,'0') = @supper THEN '1'
                                   ELSE '-1' END  'status'
                             FROM tbPostCity city With(Nolock)
                             LEFT JOIN tbPostCityArea area With(Nolock) ON city.city = area.city
                             LEFT JOIN ttArriveSites arr With(Nolock) ON arr.post_city = city.city AND area.area=arr.post_area
                             LEFT JOIN tbSuppliers supp With(Nolock) ON supp.supplier_code = arr.supplier_code
                             WHERE city.seq = @city OR @city IS NULL
                             ORDER BY CASE  WHEN ISNULL(supp.supplier_id,'0')  ='0' THEN '0'
                                            WHEN ISNULL(supp.supplier_id,'0') = @supper THEN '1'
                                            ELSE '-1'  END ";
            cmd.CommandText = strSQL;
            cmd.Parameters.AddWithValue("@city", ddl_city.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@supper", ddl_supplyer.SelectedItem.Value);
            StringBuilder sb_html = new StringBuilder();
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                int i_status = 0;
                string str_title = string.Empty;
                string str_class = string.Empty;
                Boolean IsSelect = false;
                Boolean IsEdit = false;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    if (!int.TryParse(row["status"].ToString(), out i_status)) i_status = -1;
                    str_title = row["supplier"].ToString();
                    IsSelect = false;
                    IsEdit = false;
                    str_class = "checkbox checkbox-success _set";
                    //區域設定 status: 0.尚未設定(可編輯) 1.已設定(自己-可編輯) -1.已設定(其他Supper-僅可檢示)
                    switch (i_status)
                    {
                        case 0:
                            str_title = "尚未設定";
                            str_class += "_unset";
                            IsEdit = true;
                            break;
                        case 1:
                            IsSelect = true;
                            IsEdit = true;
                            str_class += " _set";
                            break;
                        default:
                            str_class += " _set";
                            IsSelect = true;
                            break;
                    }
                    #region Area checkbox list setting
                    //ListItem item = new ListItem(row["area"].ToString(), row["area_sn"].ToString(), IsEdit);
                    ListItem item = new ListItem(row["area"].ToString(), row["area_sn"].ToString());
                    item.Selected = IsSelect;
                    item.Attributes["title"] = str_title;
                    item.Attributes["class"] = str_class;
                    item.Attributes["the_status"] = i_status.ToString();
                    cbl_area.Items.Add(item);
                    #endregion

                }
            }
        }
    }

    /// <summary>針對區域商顯示已設定的區域 </summary>
    protected void GetAreaForSupplier()
    {
        int supplier = 0;
        if (int.TryParse(ddl_supplyer.SelectedValue, out supplier) && supplier > 0)
        {
            String str_html = GetAreaHtml(supplier).ToString();
            if (str_html.Length > 0)
            {
                pan_AreaShow.Controls.Add(new LiteralControl(str_html));
                pan_AreaShow.Visible = true;
            }
            else
            {
                pan_AreaShow.Visible = false;
            }
        }
    }



    /// <summary>組區域商設定區域的HTML</summary>
    /// <param name="supper">區域商ID</param>
    /// <returns></returns>
    public static StringBuilder GetAreaHtml(int supper)
    {
        StringBuilder sb_html = new StringBuilder();
        if (supper > 0)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strSQL = @"SELECT city.seq,city.city,area.area 
                             INTO #t_area
                             FROM tbPostCity city With(Nolock)
                             LEFT JOIN tbPostCityArea area With(Nolock) ON city.city = area.city
                             LEFT JOIN ttArriveSites arr With(Nolock) ON arr.post_city = city.city AND area.area=arr.post_area
                             LEFT JOIN tbSuppliers supp With(Nolock) ON supp.supplier_code = arr.supplier_code
                             WHERE supp.supplier_id = @supper
                             
                             SELECT city,
                              ( SELECT area + '|' FROM #t_area
                                WHERE city = hb.city
                                FOR XML PATH ('')
                               ) AS area 
                             FROM #t_area hb
                             GROUP BY hb.city,hb.seq
                             ORDER BY hb.seq
                             
                             IF OBJECT_ID('tempdb..#t_area') IS NOT NULL
                                 DROP TABLE #t_area";
                cmd.CommandText = strSQL;
                cmd.Parameters.AddWithValue("@supper", supper.ToString());

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    String str_city = string.Empty;
                    String str_area = string.Empty;
                    String str_supplier_name = "";
                    String str_supplier_code = "";

                    SqlCommand cmd_sup = new SqlCommand();
                    strSQL = @"SELECT supplier_name,supplier_code FROM  tbSuppliers With(Nolock) WHERE supplier_id=@supplier_id AND active_flag =1";
                    cmd_sup.Parameters.AddWithValue("@supplier_id", supper.ToString());
                    cmd_sup.CommandText = strSQL;
                    DataTable dt_supplier = dbAdapter.getDataTable(cmd_sup);
                    if (dt_supplier.Rows.Count > 0)
                    {
                        str_supplier_name = dt_supplier.Rows[0]["supplier_name"].ToString();
                        str_supplier_code = dt_supplier.Rows[0]["supplier_code"].ToString();
                    }
                    dt_supplier.Dispose();
                    cmd_sup.Dispose();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        #region Area text html setting    
                        str_city = dt.Rows[i]["city"].ToString();
                        str_area = dt.Rows[i]["area"].ToString();
                        if (str_area.Length > 0)
                        {
                            str_area = str_area.Substring(0, str_area.Length - 1);
                            str_area.Replace("|", " | ");
                        }

                        if (i == 0)
                        {
                            sb_html.Append("<hr/><p class='text-primary'>※區配區域表(區配商：" + str_supplier_name + ") </p >");
                            sb_html.Append("<div class='bs-callout2 bs-callout-danger'>");
                            sb_html.Append(" <h3>" + str_supplier_name + "<br /> " + str_supplier_code + "</h3 >");
                            sb_html.Append("<div class='rowform'>");
                            sb_html.Append("<table class='table table-striped table-bordered tb_area_data'>");
                        }
                        sb_html.Append("<tr> <th class='warning'>" + str_city + "</th><td> " + str_area + "</td></tr>");

                        if (i == (dt.Rows.Count - 1))
                        {
                            sb_html.Append("</table></div></div>");
                        }

                        #endregion
                    }
                }
            }
        }
        return sb_html;
    }


    protected void ddl_supplyer_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i_temp;
        if (int.TryParse(ddl_supplyer.SelectedItem.Value, out i_temp)) CBLSet_Area();
    }

    protected void ddl_city_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i_temp;
        if (int.TryParse(ddl_city.SelectedItem.Value, out i_temp)) CBLSet_Area();
    }


    /// <summary>區域設定存取 </summary>
    /// <param name="supplyer"></param>
    /// <param name="city"></param>
    /// <param name="item"></param>
    /// <returns></returns>
    [WebMethod]
    public static string SetArea(string supplyer, string city, string item)
    {
        Boolean IsOK = true;//簡單驗證
        int[] i_item = new int[] { };
        int i_temp = 0;

        AreaSetInfo _set = new AreaSetInfo();

        try
        {
            #region 驗證 IsOK

            if (!int.TryParse(supplyer, out i_temp) || i_temp <= 0)
            {
                IsOK = false;
                _set.reason += "請確認區域商是否正確! ";
            }

            if (!int.TryParse(city, out i_temp) || i_temp <= 0)
            {
                IsOK = false;
                _set.reason += "請確認縣市是否正確! ";
            }

            #endregion

            //當i_item.length = 0→ 該區域原本已歸屬的item全取消
            if (item.Length > 0)
            {
                i_item = Array.ConvertAll(item.Split(','), s => int.Parse(s));
            }

            if (IsOK)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    //主要異動 Table:ttArriveSites
                    String strSQL = @"DECLARE @supplier_code nvarchar(3)
                                  DECLARE @city_cn nvarchar(10)

                                SELECT @supplier_code = supplier_code FROM tbSuppliers WHERE supplier_id = @supper; 
                                SELECT @city_cn = city FROM tbPostCity city With(Nolock) WHERE city.seq=@city; 

                                UPDATE ts1 SET ts1.supplier_code = NULL 
                                FROM ttArriveSites ts1 With(Nolock) 
                                WHERE ts1.post_city = @city_cn AND ts1.supplier_code = @supplier_code; ";

                    if (i_item.Length > 0)
                    {
                        strSQL += @"UPDATE ts2 SET ts2.supplier_code = @supplier_code
                                      FROM ttArriveSites ts2 With(Nolock)
                                     WHERE ts2.post_city = @city_cn
                                       AND ISNULL(ts2.supplier_code,'') ='' 
                                       AND ts2.post_area IN(SELECT area FROM tbPostCityArea With(Nolock) WHERE city = @city_cn AND seq IN(" + item + @"));";

                    }
                    strSQL += "SELECT @@ROWCOUNT; ";
                    cmd.CommandText = strSQL;
                    cmd.Parameters.AddWithValue("@city", city);
                    cmd.Parameters.AddWithValue("@supper", supplyer);
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0) int.TryParse(dt.Rows[0][0].ToString(), out i_temp);
                        if (i_temp >= 0)
                        {
                            _set.status = 1;
                            _set.reason = "ok";
                            _set.area_data = GetAreaHtml(Convert.ToInt32(supplyer)).ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            _set.status = 0;
            _set.reason = "發生錯誤，請聯絡系統相關人員! " + ex.Message.ToString();
            _set.area_data = "";
        }
        return new JavaScriptSerializer().Serialize(_set);
    }


    /// <summary>動態-取得未設定區域的資訊Html String</summary>
    /// <returns>未設定區域Html String</returns>
    [WebMethod]
    public static string GetUnSetAreaWithHtml()
    {
        StringBuilder sb_html = new StringBuilder();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                #region 組SQL
                string strSQL = @"SELECT city.seq city_sn
                                        ,CONVERT(VARCHAR,city.seq) +','+city.city city
                                        ,CONVERT(VARCHAR,area.seq) +','+ area.area  area
                                    INTO #t_area_u
                                    FROM tbPostCity city With(Nolock)
                               LEFT JOIN tbPostCityArea area With(Nolock) ON city.city = area.city
                               LEFT JOIN ttArriveSites arr With(Nolock) ON arr.post_city = city.city AND area.area=arr.post_area
                               LEFT JOIN tbSuppliers supp With(Nolock) ON supp.supplier_code = arr.supplier_code
                                   WHERE supp.supplier_id IS NULL;
                                                               
                                  SELECT city,
                                        (SELECT area + '|' FROM #t_area_u
                                        WHERE city = hb.city
                                        FOR XML PATH ('')
                                        ) AS area 
                                    FROM #t_area_u hb
                                GROUP BY hb.city,hb.city_sn
                                ORDER BY hb.city_sn;
                                                               
                                IF OBJECT_ID('tempdb..#t_area_u') IS NOT NULL
                                    DROP TABLE #t_area_u ";
                #endregion
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count == 0)
                    {
                        //無未設定資料
                        return "0";
                    }
                    else
                    {
                        #region 組區配商 DDL Html (sb_supplier_html)
                        strSQL = @"SELECT supplier_id sn,supplier_name+'('+supplier_code+')' 'name' FROM  tbSuppliers With(Nolock) WHERE active_flag =1";
                        cmd.CommandText = strSQL;
                        StringBuilder sb_supplier_html = new StringBuilder();
                        using (DataTable dt_supplier = dbAdapter.getDataTable(cmd))
                        {
                            if (dt_supplier.Rows.Count > 0)
                            {
                                sb_supplier_html.Append(@"<select name='ddl_supplyer_un'  class='form-control ddl_supplyer_un'>");
                                foreach (DataRow row in dt_supplier.Rows)
                                {
                                    sb_supplier_html.Append(@"<option value='" + row["sn"].ToString() + "'>" + row["name"].ToString() + "</option>");
                                }
                                sb_supplier_html.Append(@"</select>");
                            }
                        }
                        #endregion

                        #region 動態組Html ... @city(list all area with checkbox for @city) @supplyer_ddl
                        //city:17,臺東縣
                        //area:314,臺東市|315,綠島鄉|316,蘭嶼鄉|317,延平鄉|318,卑南鄉|319,鹿野鄉|320,關山鎮|321,海端鄉|322,池上鄉|323,東河鄉|324,成功鎮|325,長濱鄉|326,太麻里鄉|327,金峰鄉|328,大武鄉|329,達仁鄉|
                        //組未設定區
                        string[] str_city = new string[] { };
                        List<string[]> list_area = new List<string[]> { };
                        sb_html.Append(@"<table class='table table - striped table - bordered tb_area_unset_data'>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            str_city = dt.Rows[i]["city"].ToString().Split(',');
                            list_area = (dt.Rows[i]["area"].ToString().Substring(0, dt.Rows[i]["area"].ToString().Length - 1))
                                       .Split('|')
                                       .Select(p => p.Split(',')).ToList();

                            sb_html.Append("<tr class='unset_data_tr' _t_city='" + str_city[0] + "' title='" + str_city[1] + "'>");
                            sb_html.Append("<th class='warning t_unset_un_td1'>" + str_city[1] + "</th>");
                            sb_html.Append("<td class='t_unset_un_td2'>" + sb_supplier_html + "</td>");//加入區配商的DDL HTML
                            sb_html.Append("<td class='t_unset_un_td3'>");
                            foreach (string[] _area in list_area)
                            {
                                string str_area_seq = _area[0];
                                string str_area_cn = _area[1];
                                string str_id = "_unarea_" + i.ToString() + "_" + str_area_seq;
                                sb_html.Append(@"<span title='尚未設定' class='checkbox checkbox-success _set_unset_un' the_unstatus='0'>
                                                   <input id='cbl_" + str_id + @"' type='checkbox' name='cbl" + str_id + @"' value='" + str_area_seq + @"'>
                                                   <label>" + str_area_cn + @"</label>
                                                 </span>");
                            }

                            sb_html.Append("</td>");
                            sb_html.Append("</tr>");
                        }

                        sb_html.Append("</table>");
                        sb_html.Append(@"<div class='text-center'><button type='button' class='templatemo-blue-button _btn_un_save'>確　認</button>");
                        sb_html.Append(@"<button type='button' class='btn btn-default _btn_un_cancel'>取　消</button></div>");

                        #endregion
                    }

                }

            }
        }
        catch (Exception ex)
        {

        }

        return sb_html.ToString();
    }


    /// <summary>未設定區域匹次存檔</summary>
    /// <param name="unset"></param>
    /// <returns></returns>
    [WebMethod]
    public static string SetUnArea(List<unset_area> unset)
    {
        string str_result = "";//1:成功 !1:錯誤訊息           
        try
        {
            int i_effect_row = 0;
            foreach (unset_area _this in unset)
            {
                if (_this.item.Length > 0)
                {
                    String strSQL = @"DECLARE @supplier_code nvarchar(3)
                                          DECLARE @city_cn nvarchar(10)

                                SELECT @supplier_code = supplier_code FROM tbSuppliers WHERE supplier_id = @supper; 
                                SELECT @city_cn = city FROM tbPostCity city With(Nolock) WHERE city.seq=@city; 

                                UPDATE ts1 SET ts1.supplier_code = @supplier_code  
                                FROM ttArriveSites ts1 With(Nolock) 
                                WHERE ISNULL(ts1.supplier_code,'')=''  AND ts1.post_city = @city_cn 
                                  AND ts1.post_area in (SELECT area FROM tbPostCityArea With(Nolock) WHERE seq in(" + _this.item + ")) ; ";
                    strSQL += " SELECT @@ROWCOUNT;";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSQL;
                        cmd.Parameters.AddWithValue("@city", _this.city);
                        cmd.Parameters.AddWithValue("@supper", _this.supplyer);
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                //此更新影響的筆數
                                if (!int.TryParse(dt.Rows[0][0].ToString(), out i_effect_row)) i_effect_row = 0;
                            }
                        }
                    }
                }

            }


            str_result = "1";
        }
        catch (Exception ex)
        {
            str_result = "發生錯誤，請聯絡系統相關人員! " + ex.Message.ToString();
        }

        return str_result;
    }


    #region 區域設定相關class 宣告
    public class unset_area
    {
        public string supplyer { get; set; }
        public string city { get; set; }
        public string item { get; set; }
    }

    public class AreaSetInfo
    {
        /// <summary>狀態1:成功 0:失敗 </summary>
        public int status { get; set; }

        /// <summary>>原因:成功→OK ; 失敗→失敗原因</summary>
        public string reason { get; set; }

        /// <summary>該區域商已設定好的區域HTML資料 </summary>
        public string area_data { get; set; }

        public AreaSetInfo()
        {
            status = 0;
            reason = "尚未執行";
            area_data = "";
        }
    }

    #endregion
}