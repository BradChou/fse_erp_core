﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Text;

public partial class LTreport_1_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;
            date2.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                        else {

                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            if (manager_type != "5")
                {
                SqlCommand cmd1 = new SqlCommand();

                if (supplier_code != "")
                {
                    cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and station_code = @supplier_code";
                }

                cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

                Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
                Suppliers.DataValueField = "station_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
            }
            if (manager_type == "0" || manager_type == "1" || manager_type == "5")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if(supplier_code == "" && manager_type=="2")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);
            #endregion

            issendcontact.Items.Insert(0, new ListItem("全部", ""));
            issendcontact.Items.Insert(1, new ListItem("未銷單", "1"));
            issendcontact.Items.Insert(2, new ListItem("已銷單", "2"));

            ddlscanitem.Items.Insert(0, new ListItem("全部", ""));
            ddlscanitem.Items.Insert(1, new ListItem("集貨", "5"));
            ddlscanitem.Items.Insert(2, new ListItem("卸集", "6"));
            ddlscanitem.Items.Insert(2, new ListItem("發送", "7"));
            ddlscanitem.Items.Insert(2, new ListItem("到著", "1"));
            ddlscanitem.Items.Insert(2, new ListItem("配送", "2"));
            ddlscanitem.Items.Insert(2, new ListItem("配達", "3"));


            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["date2"] != null)
            {
                if (Request.QueryString["date2"] != "")
                {
                    date2.Text = Request.QueryString["date2"];
                }
            }

            if (Request.QueryString["Suppliers"] != null)
            {
                
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }

            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            if (Request.QueryString["issendcontact"] != null)
            {
                if (Request.QueryString["issendcontact"] != "")
                {
                    issendcontact.SelectedValue = Request.QueryString["issendcontact"];
                }
            }
            if (Request.QueryString["scanname"] != null)
            {
                scanname.Text = Request.QueryString["scanname"];
            }

            if (Request.QueryString["scanitem"] != null)
            {
                ddlscanitem.SelectedValue = Request.QueryString["scanitem"];
            }
            if (Request.QueryString["scanitem"] != null)
            {
                ddlscanitem.SelectedValue = Request.QueryString["scanitem"];
            }
            
            
            if (Request.QueryString["date1"] != null)
            {

                SqlCommand cmds = new SqlCommand();
                string sqlwhere = "";
                cmds.CommandTimeout = 600;
                if (issendcontact.SelectedValue != "2")
                {
                    sqlwhere = " AND A.cancel_date is not null ";
                }
                else
                {
                    sqlwhere = " AND A.cancel_date is null ";
                }

                cmds.CommandText = @" 
            SELECT count(A.request_id)
			FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbCustomers C With(Nolock) ON A.customer_code = C.customer_code
			LEFT JOIN tbItemCodes D With(Nolock) on D.code_id =A.pricing_type and D.code_sclass = 'PM'
			LEFT JOIN tbItemCodes _special With(Nolock) ON A.special_send = _special.code_id AND _special.code_sclass ='S4'	
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN ttArriveSitesScattered sta With(nolock) on A.send_city = sta.post_city and A.send_area = sta.post_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code = @supplier_id OR @supplier_id ='')
            AND (A.customer_code = @customer_code OR @customer_code ='')
            AND (@scanname ='' OR _arr.newest_driver_code= @scanname  )
            AND (@scanitem ='' OR _arr.arrive_state = @scanitem   )
            AND A.pricing_type in ( '02' )
            AND (_arr.arrive_state is not null)   ";
                cmds.Parameters.AddWithValue("@Less_than_truckload", 1);
                cmds.Parameters.AddWithValue("@DeliveryType", "D");
                cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue);
                cmds.Parameters.AddWithValue("@customer_code", second_code.SelectedValue);
                cmds.Parameters.AddWithValue("@scanname", scanname.Text);
                if (ddlscanitem.SelectedValue == "")
                {
                    cmds.Parameters.AddWithValue("@scanitem", "");
                }
                else
                {
                    cmds.Parameters.AddWithValue("@scanitem", ddlscanitem.SelectedItem.Text);
                }
                DataTable dtsum = dbAdapter.getDataTable(cmds);
                if (dtsum.Rows.Count > 0)
                {
                    lbSuppliers.Text = Suppliers.SelectedValue;
                    totle.Text = dtsum.Rows[0][0].ToString() + "筆";
                }

                readdata();
            }
        }
    }

    private void readdata()
    {
        string sqlwhere = "";
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            cmd.Parameters.AddWithValue("@DeliveryType", "D");

            sqlwhere += @"AND A.DeliveryType =  @DeliveryType";

            if (issendcontact.SelectedValue != "")
            {
                if(issendcontact.SelectedValue=="1")
                sqlwhere += @" AND A.cancel_date is null ";
                else
                    sqlwhere += @" AND A.cancel_date is not null  ";
            }

            if (Suppliers.SelectedValue != "" && manager_type != "5")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND A.supplier_code  =   @supplier_id ";
            }
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                sqlwhere += @" AND A.customer_code =  @customer_code  ";
            }
            querystring += "&second_code=" + second_code.SelectedValue.ToString();


            if (date1.Text.ToString() != "")
            {
                querystring += "&date1=" + date1.Text.ToString();
            }

            if (date2.Text.ToString() != "")
            {
                querystring += "&date2=" + date2.Text.ToString();
            }
            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";

            if (scanname.Text != "") {

                cmd.Parameters.AddWithValue("@scanname", scanname.Text);
                sqlwhere += @" AND (_arr.newest_driver_code= @scanname )";
            }

            if (ddlscanitem.SelectedValue != "")
            {

                cmd.Parameters.AddWithValue("@scanitem", ddlscanitem.SelectedItem.Text);
                sqlwhere += @" AND ( _arr.arrive_state = @scanitem  )";
            }

            querystring += "&rb_pricing_type=" + pricing_type;
            querystring += "&issendcontact=" + issendcontact.SelectedValue;
            querystring += "&scanname=" + scanname.Text;
            querystring += "&scanitem=" + ddlscanitem.SelectedValue;


            cmd.CommandText = string.Format(@"
            SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
			A.print_date '發送日期', 
            A.ship_date '出貨日期',
			CONVERT(CHAR(10),DATEADD(day,1,A.ship_date),111) '配送日期',  
			A.check_number '明細貨號' , 
			A.order_number '訂單編號' 
            ,case when a.customer_code = 'F1300600002' or a.customer_code = 'F2900210002' or a.customer_code = 'F2900720002' then F2.station_name 
            else sta1.station_name end '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數'
            ,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收件人地址'
			,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end '寄件件人地址'
			,A.area_arrive_code  '站所代碼',G.station_name '站所名稱',
			B.code_name '傳票區分',
			ISNULL(A.collection_money,0) '代收金額',
			A.invoice_desc '備註'
			
			,_arr.arrive_date '配達時間'
			,S.code_name '配送異常說明'
            ,_arr.newest_station+' '+H.station_name '作業站',
		      case when _arr.newest_driver_code is null then '' 
                 when _arr.newest_driver_code = '' then '' 
                 when  _arr.newest_driver_code <> '' then (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end '作業人員',
			--_arr.arrive_state '貨態情形',
            --case T.receive_option when TB.code_id then TB.code_name end '集貨異常情形',
            _arr.arrive_state '貨態狀況',
            case a.latest_scan_item  when '5' then isnull(TB.code_name,'') 
                       when '3' then isnull(_arr.arrive_item,'')
					   when '4' then isnull(_arr.arrive_item,'')
            end  '貨態情形',  
			_arr.newest_scandate '掃讀日期'
            --case when   jf_check_number is not null and  post_number is not null and zipcode5 is not null and zipcode5 != '' and  check_code is not null 
			--then  isnull(post_number+'30022118'+zipcode5+check_code,'')  end '郵局編碼'
			FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			LEFT JOIN tbItemCodes S With(Nolock) ON A.latest_scan_arrive_option = S.code_id and S.code_bclass = '5' and S.code_sclass='AO'
            LEFT JOIN pickup_request_for_apiuser p1 with(nolock) on p1.check_number = A.check_number
            LEFT JOIN tbStation F2 with(nolock) on  F2.station_code = p1.supplier_code
            CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
            LEFT JOIN tbStation H With(Nolock) ON _arr.newest_station = H.station_scode
            LEFT JOIN Post_request P with(nolock) on  P.jf_check_number = A.check_number
            left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and t.scan_item = '5' and A.latest_scan_date = T.scan_date
			left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			  AND A.print_date IS NOT NULL
			  AND A.Less_than_truckload= 1 
            {0}
            AND (_arr.arrive_state is not null)   


            ", sqlwhere);
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                SqlCommand cmds = new SqlCommand();
                string wherestr = "";
                cmds.CommandTimeout = 600;
                wherestr += dbAdapter.genWhereCommIn(ref cmds, "pricing_type", "02".Split(','));
                cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbCustomers C With(Nolock) ON A.customer_code = C.customer_code
			LEFT JOIN tbItemCodes D With(Nolock) on D.code_id =A.pricing_type and D.code_sclass = 'PM'
			LEFT JOIN tbItemCodes _special With(Nolock) ON A.special_send = _special.code_id AND _special.code_sclass ='S4'	
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN ttArriveSitesScattered sta With(nolock) on A.send_city = sta.post_city and A.send_area = sta.post_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code = @supplier_id OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            AND (@scanname ='' OR _arr.newest_driver_code= @scanname  )
            AND (@scanitem ='' OR _arr.arrive_state = @scanitem   )
            AND (_arr.arrive_state is not null)   
)
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                cmds.Parameters.AddWithValue("@DeliveryType", "D");
                cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                cmds.Parameters.AddWithValue("@scanname", scanname.Text);
                if (ddlscanitem.SelectedValue != "")
                {

                    cmds.Parameters.AddWithValue("@scanitem", ddlscanitem.SelectedItem.Text);
                }
                else
                {
                    cmds.Parameters.AddWithValue("@scanitem", "");
                }
                if (Suppliers.SelectedValue != "")
                {
                    cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                }
                else
                {
                    cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                }

                if (second_code.SelectedValue != "")
                {
                    string customer_code = second_code.SelectedValue.Trim();
                    cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }
                else
                {
                    cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                }

                DataTable dtsum = dbAdapter.getDataTable(cmds);
                ltotalpages.Text = dt.Rows.Count.ToString();
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    row["配送區域"] = "總計";
                    row["件數"] = dtsum.Rows[0]["件數"].ToString();

                    dt.Rows.Add(row);
                }

                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }


    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }

        if (date2.Text.ToString() != "")
        {
            querystring += "&date2=" + date2.Text.ToString();
        }

        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&second_code=" + second_code.SelectedValue.ToString();
        querystring += "&issendcontact=" + issendcontact.SelectedValue;

        querystring += "&scanname=" + scanname.Text;
        querystring += "&scanitem=" + ddlscanitem.SelectedValue;


        Response.Redirect(ResolveUrl("~/LTreport_1_1.aspx?search=yes" + querystring));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void New_List_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{

        //    string receive_city =(DataBinder.GetPropertyValue(e.Item.DataItem, "receive_city")).ToString();
        //    string send_city = (DataBinder.GetPropertyValue(e.Item.DataItem, "send_city")).ToString();
        //    int arrive_to_pay_freight = Convert.ToInt32 ( DataBinder.GetPropertyValue(e.Item.DataItem, "arrive_to_pay_freight"));
        //    int arrive_to_pay_append = Convert.ToInt32(DataBinder.GetPropertyValue(e.Item.DataItem, "arrive_to_pay_append"));
        //    int collection_money = Convert.ToInt32( DataBinder.GetPropertyValue(e.Item.DataItem, "collection_money"));
        //    int plates = Convert.ToInt32(DataBinder.GetPropertyValue(e.Item.DataItem, "plates"));
        //    string  platesstr = (plates >6) ? "plate6_price" : "plate" + plates.ToString() +  "_price";


        //    string customer_code = DataBinder.GetPropertyValue(e.Item.DataItem, "customer_code").ToString();
        //    string customer_type = "1";
        //    SqlCommand cmd = new SqlCommand();
        //    DataTable dtmerge = new DataTable();
        //    cmd.CommandText = "select customer_type  from tbCustomers  where customer_code  ='" + customer_code + "'";
        //    dtmerge = dbAdapter.getDataTable(cmd);
        //    if (dtmerge.Rows.Count > 0)
        //    {
        //        customer_type = dtmerge.Rows[0]["customer_type"].ToString();
        //    }

        //    SqlCommand cmdt = new SqlCommand();
        //    DataTable dt;
        //    switch (customer_type)
        //    {
        //        case "1":  //區配論板
        //            cmdt.CommandText = string.Format(@"select ISNULL(,0) from tbPriceSupplier where start_city = '"+ send_city + "  and receive_city = '"+ receive_city + "' and  class_level =(
        //                                                SELECT TOP 1 class_level FROM ttPriceClassLog With(Nolock)where active_date <= CONVERT(DATETIME, '2017/04/26', 102)  ORDER BY active_date DESC
        //                                                )""
        //            break;
        //        case "2":  //竹運論板
        //            cmd1.CommandText = "select A.*, B.code_name , '0' as class_level from tbPriceHCT A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "3":  //自營論板
        //            cmd1.CommandText = "select A.*, B.code_name , '0' as class_level from tbPriceBusiness A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "4":  //C配
        //            cmd1.CommandText = "select A.*, B.code_name from tbPriceCSection A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "5":  //合發專用
        //            cmd1.CommandText = "select A.*, B.code_name from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //    }


        //    cmdt.CommandText = "SELECT TOP 1 class_level  FROM ttPriceClassLog With(Nolock) where active_date <= CONVERT (DATETIME, '2017/04/11', 102)  ORDER BY active_date DESC";
        //    dt = dbAdapter.getDataTable(cmdt);
        //    if (dt.Rows.Count > 0)
        //    {
        //        if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
        //    }
        //    ((Label)e.Item.FindControl("statustext")).Text = DataBinder.GetPropertyValue(e.Item.DataItem, "statustext").ToString();
        //    if (DataBinder.GetPropertyValue(e.Item.DataItem, "Status").ToString() == "20")
        //    {
        //        ((Label)e.Item.FindControl("statustext")).ForeColor = System.Drawing.Color.Red;
        //    }

        //    string ttTime = DataBinder.GetPropertyValue(e.Item.DataItem, "goTime").ToString();
        //    if (ttTime.Length < 8)
        //    { ttTime += ":00"; }
        //    ((Literal)e.Item.FindControl("goTime")).Text = DateTime.Parse(ttTime).ToString("HH:mm");

        //}
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "發送總表";
        string sqlwhere = "";
        string file_name = "發送總表" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand cmd = new SqlCommand())
        {
         
            if (issendcontact.SelectedValue != "")
            {
                if (issendcontact.SelectedValue == "1")
                    sqlwhere += @" AND A.cancel_date is null ";
                else
                    sqlwhere += @" AND A.cancel_date is not null  ";
            }

            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND A.supplier_code  =   @supplier_id ";
            }

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code + "%");  //客代
                sqlwhere += @" AND A.customer_code like  @customer_code  ";
            }
            
            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";

            if (scanname.Text != "")
            {
                cmd.Parameters.AddWithValue("@scanname", scanname.Text);
                sqlwhere += @" AND (_arr.newest_driver_code= @scanname )";
            }

            if (ddlscanitem.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@scanitem", ddlscanitem.SelectedItem.Text);
                sqlwhere += @" AND ( _arr.arrive_state = @scanitem  )";
            }


            //cmd.CommandText = "usp_GetLTDistributionReportD";
            cmd.CommandText =string.Format(@"SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
			A.print_date '發送日期', 
            A.ship_date '出貨日期',
			CONVERT(CHAR(10),DATEADD(day,1,A.ship_date),111) '配送日期',  
			A.check_number '明細貨號' , 
            case
            when A.check_number like '990%' then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = A.send_contact order by cdate desc)
            when (A.customer_code = 'F3500010002' and A.DeliveryType = 'D') then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = A.send_contact order by cdate desc)
            when (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = A.check_number order by cdate desc) <> '' then (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = A.check_number order by cdate desc)
            else
            '' end '逆物流單號',
			A.order_number '訂單編號' ,
            case when a.customer_code = 'F1300600002' or a.customer_code = 'F2900210002 ' or a.customer_code = 'F2900720002 '  then F2.station_name 
            else sta1.station_name end '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數',

			CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收件人地址',
            A.send_city '發送縣市別', 
            A.send_area '發送縣區域'
			,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end '寄件件人地址'
			,A.area_arrive_code  '站所代碼',G.station_name '站所名稱',
			B.code_name '傳票區分',
			ISNULL(A.collection_money,0) '代收金額',
			A.invoice_desc '備註'
			
			,convert(nvarchar,_arr.arrive_date,120) '配達時間'
			,S.code_name '配送異常說明' ,
            _arr.newest_station+' '+H.station_name '作業站',
		      case when _arr.newest_driver_code is null then '' 
                 when _arr.newest_driver_code = '' then '' 
                 when  _arr.newest_driver_code <> '' then (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end '作業人員',
			--_arr.arrive_state '貨態情形',
            --case T.receive_option when TB.code_id then TB.code_name end '集貨異常情形',
            _arr.arrive_state '貨態狀況',
            case latest_scan_item  when '5' then isnull(TB.code_name,'') 
                       when '3' then isnull(_arr.arrive_item,'')
					   when '4' then isnull(_arr.arrive_item,'')
            end  '貨態情形' ,
			convert(nvarchar(19), _arr.newest_scandate,120) '掃讀日期',
case when A.cancel_date is null then ''
else 'V'    end '銷單'
            --case when   jf_check_number is not null and post_number is not null and zipcode5 is not null and zipcode5 != '' and  check_code is not null 
			--then  isnull(post_number+'30022118'+zipcode5+check_code,'')  end '郵局編碼'



            FROM tcDeliveryRequests A With(Nolock) 
		    LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
            LEFT JOIN tbStation H With(Nolock) ON _arr.newest_station = H.station_scode
			LEFT JOIN tbItemCodes S With(Nolock) ON A.latest_scan_arrive_option = S.code_id and S.code_bclass = '5' and S.code_sclass='AO'
            LEFT JOIN pickup_request_for_apiuser p1 with(nolock) on p1.check_number = A.check_number
            LEFT JOIN tbStation F2 with(nolock) on  F2.station_code = p1.supplier_code 
            LEFT JOIN Post_request P with(nolock) on  P.jf_check_number = A.check_number
            left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and  t.scan_item = '5' and A.latest_scan_date = T.scan_date
			left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
            WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= 1 
            AND A.DeliveryType= @DeliveryType
            {0} 
            AND (_arr.arrive_state is not null)   

",
            sqlwhere);

            //cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            cmd.Parameters.AddWithValue("@DeliveryType", "D");

            //if (issendcontact.SelectedValue != "")
            //{
            //    cmd.Parameters.AddWithValue("@issendcontact", issendcontact.SelectedValue);
            //}

            //if (Suppliers.SelectedValue != "")
            //{
            //    cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            //}

            //if (second_code.SelectedValue != "")
            //{
            //    string customer_code = second_code.SelectedValue.Trim();
            //    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            //}

            //string pricing_type = "02";

            //cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {

                    SqlCommand cmds = new SqlCommand();
                    string wherestr = "";
                    cmds.CommandTimeout = 600;
                    pricing_type = "'02'";
                    wherestr += dbAdapter.genWhereCommIn(ref cmds, "pricing_type", pricing_type.Split(','));

                    cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code = @supplier_id OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            AND (@scanname ='' OR _arr.newest_driver_code= @scanname  )
            AND (@scanitem ='' OR _arr.arrive_state = @scanitem   )
                                            
            AND (_arr.arrive_state is not null)   
     )
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                    cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                    cmds.Parameters.AddWithValue("@DeliveryType", "D");
                    cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                    cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                    cmds.Parameters.AddWithValue("@scanname", scanname.Text);
                    if (ddlscanitem.SelectedValue != "")
                    {

                        cmds.Parameters.AddWithValue("@scanitem", ddlscanitem.SelectedItem.Text);
                    }
                    else {
                        cmds.Parameters.AddWithValue("@scanitem", "");
                    }

                    if (Suppliers.SelectedValue != "")
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());

                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                    }

                    if (second_code.SelectedValue != "")
                    {
                        string customer_code = second_code.SelectedValue.Trim();
                        cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                    }


                    DataTable dts = dbAdapter.getDataTable(cmds);
                    if (dts != null && dts.Rows.Count > 0)
                    {
                        DataRow row = dt.NewRow();
                        row["配送區域"] = "總計";
                        row["件數"] = dts.Rows[0]["件數"].ToString();

                        dt.Rows.Add(row);
                    }

                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                       // Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.AddHeader("content-disposition",string.Format("attachment;  filename={0}.xlsx",HttpUtility.UrlEncode(file_name,Encoding.UTF8), Encoding.UTF8));
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }
                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            
            cmd.Parameters.AddWithValue("@type", "1");
            if (manager_type == "5")
            {
                cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock) where A.customer_code = @customer_code");
            }
            else {
                if (Suppliers.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    wherestr = " and A.supplier_code = @supplier_code ";
                }
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    Left join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 
                                                    and A.stop_shipping_code = '0' and A.type =@type {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
            }
           
            second_code.DataSource = dbAdapter.getDataTable(cmd);
            second_code.DataValueField = "customer_code";
            second_code.DataTextField = "name";
            second_code.DataBind();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2") second_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            //second_code.Items.Insert(0, new ListItem("", ""));
        }
        #endregion
    }
}