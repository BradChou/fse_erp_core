﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_3.aspx.cs" Inherits="money_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        ._oil, ._avgoil {
            width: 80px !important;
        }

        ._hide {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            // $("._oil").on("propertychange change keyup paste input", function () {
            $("._oil").on("change paste", function () {
                var numberRegex = /^\d{1,3}(\.\d+)?$/;
                var oilChk = false;//4個以上才符合計算規則
                var theVal = $("._oil").map(function () {
                    if (numberRegex.test(this.value)) {
                        return this.value;
                    } else {
                        return;
                    }
                }).get().join(',');

                //console.log(theVal);
                var _list = theVal.split(',');
                var __maxweek = parseInt($("._maxweek").text());
                //console.log(__maxweek);
                if (_list.length == __maxweek) {
                    GetOil(_list);
                } else {
                    $("._lbl_level").text("").attr("value", "");
                    $("._avgoil,._txt_avg,_class_level").val("");
                }
            });
            
            $("._btnsave").on("click", function () {
                if ($("._lberr").text().length > 0) {
                    $("._lberr").focus();
                    return false;
                }
            });

        });

        function GetOil(oil_list) {
            var result;
            $.ajax({
                url: 'money_3.aspx/GetOilAvg',
                type: 'POST',
                data: JSON.stringify({ oil: oil_list }),
                async: false,//同步                     
                contentType: 'application/json; charset=UTF-8',
                dataType: "json",      //如果要回傳值，請設成 json
                error: function (xhr) {//發生錯誤時之處理程序 
                }
            }).done(function (data, statusText, xhr) {
                //console.log(data);
                result = JSON.parse(data.d);
                if (result.state == 1) {
                    $("._avgoil").val(result.avgoil);
                    $("._txt_avg").val(result.avgoil).attr("value", result.avgoil);
                    $("._class_level").val(result.level).attr("value", result.level);
                    $("._lbl_level").text(result.level).attr("value", result.level);
                    $("._lberr").text("").hide();
                } else {
                    $("._avgoil,._class_level").val("").attr("value", "");
                    $("._lbl_level").text("").attr("value", "");
                    $("._txt_avg").val("");
                    $("._lberr").text(result.reason).show();
                }
            });
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <table>
                <tr>
                    <td>
                        <h2 class="margin-bottom-10">油價級距管理</h2>
                    </td>

                    <td>
                        <span class="text-danger span_tip">※ 開放修改時期間：每月 20~25 日</span>
                    </td>
                </tr>
            </table>


            <div class="templatemo-login-form">
                <div runat="server" id="div_oil">
                <div class="form-group form-inline">
                    <div>                       
                        <asp:TextBox ID="new_Oil1" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W1" CssClass="form-control _oil _oil_require"></asp:TextBox>
                        <asp:TextBox ID="new_Oil2" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W2" CssClass="form-control _oil _oil_require"></asp:TextBox>
                        <asp:TextBox ID="new_Oil3" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W3" CssClass="form-control _oil _oil_require"></asp:TextBox>
                        <asp:TextBox ID="new_Oil4" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W4" CssClass="form-control _oil _oil_require"></asp:TextBox>
                        <asp:TextBox ID="new_Oil5" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W5" CssClass="form-control _oil"></asp:TextBox>
                        <asp:TextBox ID="new_Oil6" runat="server" onkeydown="if(event.keyCode==13)event.keyCode=9" onkeyup="this.value=this.value.replace(/([0-9]+\.[0-9]{2})[0-9]*/,'$1');" placeholder="W6" CssClass="form-control _oil"></asp:TextBox>
                        <asp:TextBox Style="pointer-events: none;" ID="new_avgOil" runat="server" placeholder="平均油價" CssClass="form-control _avgoil"></asp:TextBox>

                    </div>
                    <asp:RequiredFieldValidator ID="req_w1" runat="server" ControlToValidate="new_Oil1" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W1油價</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="req_w2" runat="server" ControlToValidate="new_Oil2" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W2油價</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="req_w3" runat="server" ControlToValidate="new_Oil3" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W3油價</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="req_w4" runat="server" ControlToValidate="new_Oil4" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W4油價</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="req_w5" runat="server" ControlToValidate="new_Oil5" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W5油價</asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="req_w6" runat="server" ControlToValidate="new_Oil6" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入W6油價</asp:RequiredFieldValidator>
                    <asp:Label ID="lbl_maxWeek" runat="server" CssClass="_hide _maxweek"></asp:Label>
                   <asp:TextBox ID="class_level" runat="server" class="_hide _class_level"></asp:TextBox>
                    <asp:TextBox ID="txt_avgoil" runat="server"  CssClass="_hide _txt_avg"></asp:TextBox>

                </div>
                <div class="form-group form-inline">
                    <label for="inputLastName">生效日期</label>
                    <asp:TextBox ID="active_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="active_date" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請輸入生效日期</asp:RequiredFieldValidator>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label for="inputFirstName">油價級距</label>
                            <asp:Label ID="lbl_level" runat="server" CssClass="_lbl_level" ></asp:Label>
                            <asp:Label ID="lbErr" runat="server" CssClass="_hide _lberr" ForeColor="Red"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%--<input class="form-control" placeholder="表定級距" type="text"> </input>--%>
                </div>
                </div>
                <div class="form-group form-inline">
                    <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary _btnsave" ValidationGroup="validate" OnClick="btnsave_Click" Text="確認儲存" />
                    <%--<asp:Button ID="btnMail" runat="server" CssClass="btn btn-darkblue" Text="發送E-mail通知" />--%>
                    <asp:Button ID="btLog" runat="server" CssClass="btn btn-danger" Text="歷史修改記錄" OnClick="btLog_Click" />
                </div>
                <div style="overflow: auto; height: calc(100vh - 500px); width: 100%">
                    <table class="table table-striped table-bordered templatemo-user-table">
                        <tr class="tr-only-hide">
                            <th>級距</th>
                            <th>生效日</th>
                            <th>W1</th>
                            <th>W2</th>
                            <th>W3</th>
                            <th>W4</th>
                            <th>W5</th>
                            <th>W6</th>
                            <th>平均油價</th>
                            <th>是否通知</th>
                            <th>更新人員</th>
                            <th>更新日期</th>
                            <th>E-mail通知</th>
                        </tr>
                        <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="級距">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.class_level").ToString())%>
                                   
                                    </td>
                                    <td data-th="生效日">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.active_date","{0:yyyy/MM/dd}").ToString())%>
                                    </td>
                                    <td data-th="W1">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week1").ToString())%>
                                    </td>
                                    <td data-th="W2">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week2").ToString())%>
                                    </td>
                                    <td data-th="W3">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week3").ToString())%>
                                    </td>
                                    <td data-th="W4">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week4").ToString())%>
                                    </td>
                                    <td data-th="W5">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week5").ToString())%>
                                    </td>
                                     <td data-th="W6">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.week6").ToString())%>
                                    </td>
                                    <td data-th="平均油價">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive").ToString())%>
                                    </td>
                                    <td data-th="是否通知">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.notify").ToString())%>
                                    </td>
                                    <td data-th="更新人員">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%>
                                    </td>
                                    <td data-th="更新日期">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate","{0:yyyy/MM/dd}").ToString())%>
                                    </td>
                                    <td data-th="">
                                        <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>' />
                                        <asp:Button ID="btmail" CssClass=" btn btn-darkblue" CommandName="cmdmail" runat="server" Text="發 送" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="13" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>
                </div>

            </div>

        </div>
    </div>
</asp:Content>

