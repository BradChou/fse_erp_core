﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


public partial class system_12 : System.Web.UI.Page
{
    public string func_code = "S12";  //預購袋區間設定

    public Auth _Auth
    {
        get { return (Auth)ViewState[ this.ClientID + "_Auth"]; }
        set { ViewState[this.ClientID + "_Auth"] = value; }
    }

    public string manager_type 
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            #region 權限
            _Auth = getAuth(Session["account_code"].ToString(), func_code, Less_than_truckload);
            btn_Add.Visible = _Auth.insert_auth;

            #endregion

            DDLSet();
            DefaultData();
        }
    }

    [Serializable]
    public class Auth
    {
        public int auth_id { get; set; }
        public string func_code { get; set; }
        public string func_name { get; set; }
        public bool view_auth { get; set; }
        public bool insert_auth { get; set; }
        public bool modify_auth { get; set; }
        public bool delete_auth { get; set; }

        public Auth()
        {
            view_auth = false;
            insert_auth = false;
            modify_auth = false;
            delete_auth = false;
        }
    }


    public Auth getAuth(string account_code, string func_code, string type = "0")
    {
        string wherestr = string.Empty;
        string wherestr2 = string.Empty;
        Auth _Auth = new Auth();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@account_code", account_code);
            cmd.Parameters.AddWithValue("@type", type);
            if (func_code != "")
            {
                cmd.Parameters.AddWithValue("@func_code", func_code);
                wherestr += " and func_code like '' + @func_code + '%'";
                wherestr2 += " and func.func_code like '' + @func_code + '%'";
                wherestr2 += " and func.func_code <> 'TA'";
                cmd.CommandText = string.Format(@"declare @@count int;
                                                  declare @@manager_type nvarchar(2);
                                                  select @@count =count(*) from tbFunctionsAuth where account_code  = @account_code {0};
                                                  IF (@@count =0) BEGIN
                                                  select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                  select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type and auth.func_type =@type
                                                   where 0=0 and func.type=@type  {1}
                                                   order by func.sort ,func.level
                                                  END ELSE BEGIN 
                                                   select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.account_code  = @account_code and auth.func_type =@type
                                                   where 0=0 and func.type=@type {2}
                                                   order by func.sort,func.level 
                                                  END; ", wherestr, wherestr2, wherestr2);

                DataTable dt  = dbAdapter.getDataTable(cmd);
                if (dt != null && dt.Rows.Count >0) _Auth = DataTableExtensions.ToList<Auth>(dt).ToList().FirstOrDefault();
                
            }
        }
        return _Auth;
    }

    protected void DDLSet()
    {

        String strSQL = @"SELECT code_id id,code_name name FROM tbItemCodes item With(Nolock) WHERE item.code_sclass=N'H1' AND active_flag IN(1)";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = strSQL;
            DataTable dt = dbAdapter.getDataTable(cmd);
        }

        ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));


        ddl_station.DataSource = Utility.getArea_Arrive_Code(true);
        ddl_station.DataValueField = "station_code";
        ddl_station.DataTextField = "showname";
        ddl_station.DataBind();
        ddl_station.Items.Insert(0, new ListItem("--站別--", ""));
        ddl_station.SelectedValue = "";




        ddl_staion.DataSource = Utility.getArea_Arrive_Code(true);
        ddl_staion.DataValueField = "station_code";
        ddl_staion.DataTextField = "showname";
        ddl_staion.DataBind();
        ddl_staion.Items.Insert(0, new ListItem("請選擇", ""));
        ddl_staion.SelectedValue = "";



    }


    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
            //if (Request.QueryString["num"] != null)
            //{
            //    tb_searchNum.Text = Request.QueryString["num"];
            //    querystring += "&num=" + tb_searchNum.Text;
            //}

            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }

            if (Request.QueryString["station"] != null)
            {
                ddl_station.SelectedValue = Request.QueryString["station"];
                querystring += "&station=" + ddl_station.SelectedValue;
            }

            #endregion

            string strWhere = "";

            strSQL = @"declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock)                      
                       group by customer_code

                            SELECT ROW_NUMBER() OVER(ORDER BY A.customer_code,A.id) AS num,
							  A.id as seq
							  ,C.station_name 
							  ,A.customer_code
							  ,B.customer_name
						      ,A.total_count
							  ,A.total_count - A.used_count as rest_count
                             ,CASE A.is_active WHEN '1' THEN '是' ELSE '否' END is_active  
                             ,A.update_user
							 ,A.update_date
                         FROM Customer_checknumber_setting A With(Nolock)
				         INNER JOIN @newest N  ON A.customer_code = N.customer_code and A.update_date = N.update_date 
                         LEFT JOIN tbCustomers B With(Nolock) ON A.customer_code = B.customer_code 
                         LEFT JOIN tbStation  C With(Nolock) ON B.supplier_code = C.station_code					
                         where 1=1 and B.product_type = 2";

            #region 查詢條件
            //if (tb_searchNum.Text.Trim().Length > 0)
            //{
            //    strWhere += " AND (@num between begin_number AND  End_number)";
            //    cmd.Parameters.AddWithValue("@num", tb_searchNum.Text.Trim());
            //}
            if (tb_search.Text != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", tb_search.Text);
                cmd.Parameters.AddWithValue("@customer_name", tb_search.Text);
                strWhere += " AND(B.customer_code like '%'+@customer_code+'%' or B.customer_name like '%'+@customer_name+'%') ";
            }

            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND A.is_active IN (" + ddl_active.SelectedValue.ToString() + ")";
            }

            if (ddl_station.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", ddl_station.SelectedValue.ToString());
                strWhere += " AND B.supplier_code =@supplier_code";
            }
            #endregion

            cmd.CommandText = string.Format(strSQL + " {0}", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "active=" + ddl_active.SelectedValue
                          + "&station=" + ddl_station.SelectedValue;

        //if (!string.IsNullOrEmpty(tb_searchNum.Text))
        //{
        //    str_Query += "&num=" + tb_searchNum.Text;
        //}
        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/system_12.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("emp_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢視(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        ddl_staion.Enabled = true;
        ddl_Customer.Enabled = true;
        switch (type)
        {
            case 1:
                #region 新增   
                TextBox1.Text = "";
                lbl_title.Text = "預購袋區間-新增";
                TextBox1.Visible = true;
                adjust.Visible = true;
                adjust.Text = "新增原因";
                TextBox1.Enabled = true;
                pan_edit.Visible = true;
                history.Visible = false;
                pan_view.Visible = false;
                lbl_id.Text = "0";
                showNum.Text = "";
                num.Text = "";
                rest.Text = "";
                ddl_staion.SelectedValue = "";
             
                rb_Active.SelectedValue = "1";
                ddl_Customer.Items.Clear();
                num.Enabled = true;
                #endregion

                break;
            case 2:
                num.Text = "";
                ddl_staion.Enabled = false;
                ddl_Customer.Enabled = false;
                num.Enabled = true;
                TextBox1.Enabled = true;
                adjust.Text = "調整原因";
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "預購袋區間-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = "";

                        strSQL = @"SELECT TOP(1) A.customer_code
                         
                             ,A.is_active 
                             ,C.station_code 
                             ,B.customer_shortname
                             ,A.total_count
                             ,A.used_count
                             ,A.total_count-A.used_count as rest_count
                         FROM Customer_checknumber_setting A With(Nolock)
                    LEFT JOIN tbCustomers B With(Nolock) ON A.customer_code = B.customer_code 
                    LEFT JOIN tbStation  C With(Nolock) ON B.supplier_code = C.station_code
                        WHERE A.id = @seq 
                        ORDER BY A.update_date desc";

                        cmd.Parameters.AddWithValue("@seq",id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];

                                showNum.Text = row["total_count"].ToString();
                                original_total_count.Value = row["total_count"].ToString();
                                rest.Text = row["rest_count"].ToString();
                                original_rest_count.Value = row["rest_count"].ToString();
                                original_used_count.Value = row["used_count"].ToString();

                                CustomerChanged(row["station_code"].ToString());
                                ddl_staion.SelectedValue = row["station_code"].ToString();
                                ddl_Customer.SelectedValue = row["customer_code"].ToString();
                                rb_Active.SelectedValue = Convert.ToInt16(row["is_active"]).ToString();
                                lbl_id.Text = id.ToString();
                                TextBox1.Text = "";
                            }
                        }
                    }

                    //歷史調整紀錄
                    using (SqlCommand sqlcmd = new SqlCommand())
                    {
                        sqlcmd.Parameters.AddWithValue("@seq", id.ToString());
                        sqlcmd.CommandText = @"select *,total_count-history_used_count as rest_number, case when is_active = 1 then '是' when is_active = 0 then '否' end as is_active_chinese from Customer_checknumber_setting 
                                               where customer_code = (select customer_code from Customer_checknumber_setting where id = @seq) and len(adjustment_reason) > 0 order by update_date asc";

                        using (DataTable dt = dbAdapter.getDataTable(sqlcmd))
                        {
                            var count = dt.Rows.Count;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                        }
                    }

                    pan_edit.Visible = true;
                    history.Visible = true;
                    TextBox1.Visible = true;
                    adjust.Visible = true;
                    pan_view.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "預購袋區間";
                pan_edit.Visible = false;
                history.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";
        int _num = 0;
        int used_count = 0;

        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;
        if (!IsEdit && (num.Text == "" || !int.TryParse(num.Text, out _num) || _num <= 0))
        {
            IsOk = false;
            RetuenMsg("新增預購袋，請輸入要新增的數量!!!");
            return;
        }


        if (!IsOk) return;

        #region 存檔
        if (IsOk)
        {
            if (TextBox1.Text.ToString() == "")
            {
                RetuenMsg("請輸入" + adjust.Text + "。");
                return;
            }

            string strUser = string.Empty;
            if (Session["account_code"] != null) strUser = Session["account_code"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {
                if (IsEdit)
                {
                    using (SqlCommand sql = new SqlCommand())
                    {
                        DataTable dataTable;
                        sql.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                        sql.CommandText = @"select top(1) used_count from Customer_checknumber_setting where customer_code = @customer_code order by update_date desc";
                        dataTable = dbAdapter.getDataTable(sql);
                        if (dataTable.Rows.Count > 0)
                            used_count = Convert.ToInt32(dataTable.Rows[0]["used_count"]);

                        cmd.Parameters.AddWithValue("@total_count", showNum.Text.ToString());
                        cmd.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                        cmd.Parameters.AddWithValue("@is_active", rb_Active.SelectedValue.ToString());
                        cmd.Parameters.AddWithValue("@adjustment_reason", TextBox1.Text.ToString());
                        cmd.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        cmd.Parameters.AddWithValue("@update_user", strUser);
                        cmd.Parameters.AddWithValue("@used_count", used_count);
                        cmd.Parameters.AddWithValue("@history_used_count", used_count);
                        using (SqlCommand sqlc = new SqlCommand())
                        {
                            string product_type = "";
                            bool is_new_customer = false;
                            DataTable dtc;
                            sqlc.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                            sqlc.CommandText = @"select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
                            dtc = dbAdapter.getDataTable(sqlc);
                            if (dtc.Rows.Count > 0)
                            {
                                product_type = dtc.Rows[0]["product_type"].ToString();
                                var aaa = dtc.Rows[0]["is_new_customer"].ToString();
                                is_new_customer = dtc.Rows[0]["is_new_customer"].ToString() == "True" ? true : false;

                                if (product_type == "2" && is_new_customer == true)  //預購袋客袋 & 新客戶
                                    cmd.Parameters.AddWithValue("@type", "b3");
                            }

                        }
                        cmd.CommandText = dbAdapter.genInsertComm("Customer_checknumber_setting", false, cmd);
                        dbAdapter.execNonQuery(cmd);
                        SetEditOrAdd(0);
                    }
                    if (rb_Active.SelectedValue.ToString() == "1")
                    {
                        using (SqlCommand cmd1 = new SqlCommand())
                        {
                            cmd1.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                            cmd1.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            cmd1.Parameters.AddWithValue("@pieces_count", num.Text.ToString());
                            cmd1.Parameters.AddWithValue("@function_flag", "S12");
                            cmd1.Parameters.AddWithValue("@adjustment_reason", "貨號數量調整");
                            cmd1.Parameters.AddWithValue("@remain_count", rest.Text);
                            cmd1.CommandText = dbAdapter.genInsertComm("checknumber_record_for_bags_and_boxes", false, cmd1);
                            dbAdapter.execNonQuery(cmd1);
                        }
                    }


                    using (SqlCommand sql = new SqlCommand())
                    {
                        DataTable dataTable;
                        sql.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                        sql.CommandText = @"select top(1) total_count-used_count as rest_number from Customer_checknumber_setting where customer_code = @customer_code order by update_date desc";
                        dataTable = dbAdapter.getDataTable(sql);
                        if (dataTable.Rows.Count > 0)
                            rest.Text = dataTable.Rows[0]["rest_number"].ToString();
                    }

                    //貨號數量修改的歷史紀錄
                    using (SqlCommand sqlcmd = new SqlCommand())
                    {
                        sqlcmd.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                        sqlcmd.CommandText = @"select *,total_count - history_used_count as rest_number, case when is_active = 1 then '是' when is_active = 0 then '否' end as is_active_chinese from Customer_checknumber_setting where customer_code = @customer_code and len(adjustment_reason) > 0 order by update_date asc";

                        using (DataTable dt = dbAdapter.getDataTable(sqlcmd))
                        {
                            var count = dt.Rows.Count;
                            Repeater1.DataSource = dt;
                            Repeater1.DataBind();
                        }
                    }

                    RetuenMsg("修改成功!");
                }
                else //新增
                {
                    //檢查該客代是否已被新增過
                    using (SqlCommand sql = new SqlCommand())
                    {
                        DataTable dataTable;
                        sql.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                        sql.CommandText = @"select top (1) customer_code from Customer_checknumber_setting where customer_code = @customer_code";
                        dataTable = dbAdapter.getDataTable(sql);
                        if (dataTable.Rows.Count > 0)
                        {
                            SetEditOrAdd(0);
                            RetuenMsg("此客代已存在，請勿重複建立。");
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@total_count", showNum.Text.ToString());
                            cmd.Parameters.AddWithValue("@used_count", 0);
                            cmd.Parameters.AddWithValue("@history_used_count", 0);
                            cmd.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@is_active", rb_Active.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@adjustment_reason", TextBox1.Text.ToString());
                            cmd.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            cmd.Parameters.AddWithValue("@update_user", strUser);
                            using (SqlCommand sqlc = new SqlCommand())
                            {
                                string product_type = "";
                                bool is_new_customer = false;
                                DataTable dtc;
                                sqlc.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                                sqlc.CommandText = @"select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
                                dtc = dbAdapter.getDataTable(sqlc);
                                if (dtc.Rows.Count > 0)
                                {
                                    product_type = dtc.Rows[0]["product_type"].ToString();
                                    var aaa = dtc.Rows[0]["is_new_customer"].ToString();
                                    is_new_customer = dtc.Rows[0]["is_new_customer"].ToString() == "True" ? true : false;

                                    if (product_type == "2" && is_new_customer == true)  //預購袋客袋 & 新客戶
                                        cmd.Parameters.AddWithValue("@type", "b3");
                                }

                            }


                            cmd.CommandText = dbAdapter.genInsertComm("Customer_checknumber_setting", true, cmd);
                            int result = 0;
                            if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                            SetEditOrAdd(0);
                            RetuenMsg("新增成功!");

                            if (rb_Active.SelectedValue.ToString() == "1")
                            {
                                using (SqlCommand cmd1 = new SqlCommand())
                                {
                                    cmd1.Parameters.AddWithValue("@customer_code", ddl_Customer.SelectedValue.ToString());
                                    cmd1.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    cmd1.Parameters.AddWithValue("@pieces_count", num.Text.ToString());
                                    cmd1.Parameters.AddWithValue("@function_flag", "S12");
                                    cmd1.Parameters.AddWithValue("@adjustment_reason", "貨號數量調整");
                                    cmd1.Parameters.AddWithValue("@remain_count", rest.Text);
                                    cmd1.CommandText = dbAdapter.genInsertComm("checknumber_record_for_bags_and_boxes", false, cmd1);
                                    dbAdapter.execNonQuery(cmd1);

                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
              
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }
        return result;
    }
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }

    protected void Customer_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlstation = (DropDownList)sender;
        DropDownList dlcustomer = null;
        dlcustomer = ddl_Customer;
        if (dlstation.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@station", dlstation.SelectedValue.ToString());
            cmda.CommandText = @"Select customer_code,customer_name  , customer_code + '-' + customer_name 'showname' 
                                 from tbCustomers With(Nolock) 
                                 where supplier_code =@station  AND type=1  AND stop_shipping_code = 0    
                                 order by customer_id asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                dlcustomer.DataSource = dta;
                dlcustomer.DataValueField = "customer_code";
                dlcustomer.DataTextField = "showname";
                dlcustomer.DataBind();
            }
        }
        else
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void CustomerChanged(string station)
    {

        DropDownList dlcustomer = null;
        dlcustomer = ddl_Customer;
        if (station != "")
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@station", station);
            cmda.CommandText = "Select customer_code,customer_name from tbCustomers With(Nolock) where supplier_code =@station  AND type=1  AND stop_shipping_code = 0    order by customer_id asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlcustomer.Items.Add(new ListItem(dta.Rows[i]["customer_code"].ToString().Trim() + dta.Rows[i]["customer_name"].ToString().Trim(), dta.Rows[i]["customer_code"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void Button1_Plus_Click(object sender, EventArgs e)
    {
        int originalUsedCount = Convert.ToInt32(original_used_count.Value == "" ? "0" : original_used_count.Value);
        int originalTotalCount = Convert.ToInt32(original_total_count.Value == "" ? "0" : original_total_count.Value);
        int originalRestCount = Convert.ToInt32(original_rest_count.Value == "" ? "0" : original_rest_count.Value);

        //新增頁面
        if (showNum.Text.Length == 0)
        { 
            showNum.Text = "0";
            rest.Text = "0";
            num.Text = "0";
        }

        num.Text = (Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text) + 1).ToString();

        int inputCount = Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text);

        showNum.Text = (originalTotalCount + inputCount).ToString();
        rest.Text = (originalRestCount + inputCount).ToString();

        if (Convert.ToInt32(rest.Text) < 0 || Convert.ToInt32(showNum.Text) < 0)
        {
            RetuenMsg("剩餘數量或總共數量不可小於 0 ");
            showNum.Text = originalTotalCount.ToString();
            rest.Text = originalRestCount.ToString();
        }
    }

    protected void Button2_Minus_Click(object sender, EventArgs e)
    {
        int originalUsedCount = Convert.ToInt32(original_used_count.Value == "" ? "0" : original_used_count.Value);
        int originalTotalCount = Convert.ToInt32(original_total_count.Value == "" ? "0" : original_total_count.Value);
        int originalRestCount = Convert.ToInt32(original_rest_count.Value == "" ? "0" : original_rest_count.Value);

        //新增頁面
        if (showNum.Text.Length == 0)
        {
            showNum.Text = "0";
            rest.Text = "0";
            num.Text = "0";
        }
        num.Text = (Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text) - 1).ToString();

        int inputCount = Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text);

        showNum.Text = (originalTotalCount + inputCount).ToString();
        rest.Text = (originalRestCount + inputCount).ToString();

        if (Convert.ToInt32(rest.Text) < 0 || Convert.ToInt32(showNum.Text) < 0)
        {
            RetuenMsg("剩餘數量或總共數量不可小於 0 ");
            showNum.Text = originalTotalCount.ToString();
            rest.Text = originalRestCount.ToString();
        }
    }

    protected void num_TextChanged(object sender, EventArgs e)
    {
        int inputCount = Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text);
        int originalTotalCount = Convert.ToInt32(original_total_count.Value == "" ? "0" : original_total_count.Value);
        int originalUsedCount = Convert.ToInt32(original_used_count.Value == "" ? "0" : original_used_count.Value);
        int originalRestCount = Convert.ToInt32(original_rest_count.Value == "" ? "0" : original_rest_count.Value);

        //編輯頁面
        if (lbl_title.Text.Contains("編輯"))
        {
            rest.Text = (originalRestCount + inputCount).ToString();
            showNum.Text = (originalTotalCount + inputCount).ToString();

            if ((originalRestCount + inputCount) < 0 || (originalTotalCount + inputCount) < 0)
            {
                RetuenMsg("總共數量與剩餘數量不可小於 0。");
                rest.Text = originalRestCount.ToString();
                showNum.Text = originalTotalCount.ToString();
            }
        }
        else //新增頁面
        {
            if (Convert.ToInt32(num.Text.Length == 0 ? "0" : num.Text) < 0)
            {
                RetuenMsg("總共數量與剩餘數量不可小於 0。");
                rest.Text = "";
                showNum.Text = "";
            }
            else
            {
                rest.Text = num.Text.ToString();
                showNum.Text = num.Text.ToString();
            }
        }
    }
}
