﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PublicTrace : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            divMaster.Visible = true;
            
            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }

            readdata();
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    private void readdata()
    {
        if (check_number.Text != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                #endregion

                cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(3000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)
                                                  DECLARE @ORDER_NUMBER NVARCHAR(20)
	                                              
                                                  SET @where = ''
                                                  SET @orderby = ''
                                                  
                                                  SELECT @ORDER_NUMBER = ISNULL(order_number,'') FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                  
                                                  SET @statement = 
                                                  'Select  CASE WHEN A.supplier_code IN(''001'',''002'') THEN REPLACE(REPLACE(D.customer_shortname,''所'',''''),''HCT'','''') ELSE A.supplier_name END as sendstation,
                                                  E.showname,
                                                  A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                                                  A.time_period, A.order_number, A.check_number,
                                                  CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
                                                  A.product_category, C.code_name as product_category_name, 
                                                  A.subpoena_category, B.code_name as subpoena_category_name,
                                                  A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                                                  isnull(_fee.total_fee,0) ''貨件運費'' ,
                                                  isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) ''元付運費'',
                                                  A.arrive_to_pay_append ''到付追加未稅'', A.arrive_to_pay_append  *1.05 ''到付追加含稅'',
                                                  A.collection_money ''代收金額'',
                                                  isnull(_fee.cscetion_fee,0)  ''配送費用'',
                                                  isnull(_fee.supplier_unit_fee,0)  ''每板費用'',	
                                                  A.remote_fee ''偏遠區加價'',
                                                  isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) ''總費用''
                                                  from tcDeliveryRequests A with(nolock)
                                                  LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
                                                  LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
                                                  LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                                                  LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '''') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                                                  CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee'
                                                  SET @where = ' where 1=1 ' 
                                                  
                                                  IF @ORDER_NUMBER = '' BEGIN 
	                                                  SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
                                                  END ELSE BEGIN 
	                                                  SET @where=@where+' and (A.check_number = '''+ @ORDER_NUMBER + ''' OR A.order_number =  '''+ @ORDER_NUMBER + ''')'
                                                  END
                                                  
                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby
                                                  
                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    if (dt.Rows.Count > 0)
                    {
                        string customer_code = dt.Rows[0]["customer_code"].ToString();
                        string send_contact = dt.Rows[0]["send_contact"].ToString();
                        send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
                        string send_tel = dt.Rows[0]["send_tel"].ToString();
                        send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                        string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                        receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                        string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                        receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;

                        lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel;
                        lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;
                    }
                    New_List_01.DataSource = dt;
                    New_List_01.DataBind();
                }

            }



            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/" + days);
                //}

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text); 
                }
                #endregion

                cmd.CommandText = string.Format(@"
                                                DECLARE @statement nvarchar(2000)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	 
                                                SET @where = ''
                                                SET @orderby = ''
                                                DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                SELECT @ORDER_NUMBER=order_number  FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                 
                                                SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item,  CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達''  END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option,A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE EO.code_name  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 '
                                                SET @where = ' where 1=1  and A.scan_item in(''1'',''2'',''3'')'

                                                IF @ORDER_NUMBER <> '' BEGIN 
                                                    SET @statement = 
	                                                'declare @tbl table(check_number nvarchar(10))
	                                                insert into @tbl
	                                                select check_number from tcDeliveryRequests with(nolock) where order_number ='''+ @ORDER_NUMBER + ''';'
	                                                + @statement
	                                                SET @where =@where+ ' and (A.check_number = '+@check_number+'
	                                                            or A.check_number in(select check_number  from @tbl))'
                                                END ELSE BEGIN 
                                                    SET @where = @where + 'and A.check_number = '''+ @check_number+ ''''
                                                END

                                                SET @orderby =' order by log_id desc'
	                                                SET @statement = @statement + @where + @orderby
	                                             
                                                EXEC sp_executesql @statement

                                                ");

                //cmd.CommandText = string.Format(@"Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                //                                 A.scan_item,  CASE A.scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  END  AS scan_name,
                //                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                //                                 A.arrive_option,A.exception_option , 
                //                                 CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                //                                 A.driver_code , C.driver_name , A.sign_form_image, A.sign_field_image
                //                                 from ttDeliveryScanLog A with(nolock)
                //                                 left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code 
                //                                 left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                //                                 left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                //                                 left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                //                                 where 1=1  and A.check_number = @check_number
                //                                 order by log_id desc");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    
                    New_List_02.DataSource = dt;
                    New_List_02.DataBind();
                }



            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字               

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                   
                }
                #endregion

               
                cmd.CommandText = string.Format(@"DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                  DECLARE @sign_form_image nvarchar(50)
                                                  DECLARE @sign_field_image nvarchar(50)
                                                  DECLARE @customer_code NVARCHAR(10)
                                                  DECLARE @supplier_code NVARCHAR(3)

                                                  select top 1 @sign_form_image=A.sign_form_image, @sign_field_image=A.sign_field_image, @customer_code=dr.customer_code ,@supplier_code=dr.supplier_code  
                                                  from ttDeliveryScanLog A with(nolock)
                                                  left join tcDeliveryRequests  dr on dr.check_number = A.check_number 
                                                  where 1=1  and A.check_number = @check_number  and A.scan_item in('4')
                                                  order by A.log_id desc

                                                  IF (@sign_form_image IS null OR @sign_form_image='') BEGIN 
	                                                  SELECT @ORDER_NUMBER=ISNULL(order_number,'')  FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                      IF (@ORDER_NUMBER <> '') BEGIN 
	                                                      SELECT top 1 @sign_form_image=A.sign_form_image, @sign_field_image=A.sign_field_image, @customer_code=dr.customer_code ,@supplier_code=dr.supplier_code  
	                                                      from ttDeliveryScanLog A with(nolock)
	                                                      left join tcDeliveryRequests  dr on dr.check_number = A.check_number 
	                                                      where 1=1  and dr.order_number = @ORDER_NUMBER  and A.scan_item in('4')
	                                                      order by A.log_id desc 
                                                      END
                                                  END

                                                  SELECT @sign_form_image as sign_form_image,@sign_field_image as sign_field_image,@customer_code as customer_code,@supplier_code as supplier_code");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string filename = dt.Rows[0]["sign_form_image"].ToString();
                        if (filename != "")
                        {
                            Image1.ImageUrl = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + filename;
                        }
                    }
                }
            }

        }


    }
}