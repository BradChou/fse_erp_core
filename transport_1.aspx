﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="transport_1.aspx.cs" Inherits="transport_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />   

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#Receiveclick").fancybox({
                'width': 980,
                'height': 600,
                'autoScale': false,
                'transitionIn': 'none',
                'type': 'iframe',
                'transitionOut': 'none',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

            $(".subpoena_category").change();

            $("#<%= turn_board.ClientID%>").change(function () {
                $("._turn_board_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= upstairs.ClientID%>").change(function () {
                $("._upstairs_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= difficult_delivery.ClientID%>").change(function () {
                $("._difficult_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= btnRecsel.ClientID%>").click(function () {
                var customercode = $("#<%= customer_code.ClientID%>").val();
                if (customercode == "")
                {
                    alert('請先選取客代');
                    return false;
                }
            });

            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date())  //預設當日
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".date_picker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    defaultDate: (new Date())  //預設當日
                });
            }
            

            //$(".chosen-select").trigger("chosen:updated");
            //$(".chosen-select").chosen("destroy");

        });
        $.fancybox.update();

        //$(document).ready(function () {
        //    Page_Init();
        //});
        //function Page_Init() {            
        //    $('#page-wrapper :input[type=text]').enter2tab();
        //}

       
        
        $(document).on("change", ".subpoena_category", function () {
            var _isOK = true;
            var _cateVal = $(this).children("option:selected").val();
            var _setItem;
            var _disItem;
            switch (_cateVal)
            {
                case '11'://元付
                    //_collection_money
                    _disItem = $("._collection_money");
                    _setItem = $("._arrive_to_pay_freight,._arrive_to_pay_append");
                    break;
                case '21'://到付
                    _disItem = $("._collection_money,._arrive_to_pay_append");
                    _setItem = $("._arrive_to_pay_freight");
                    break;
                case '25'://元付-到付追加
                    _disItem = $("._collection_money,._arrive_to_pay_freight");
                    _setItem = $("._arrive_to_pay_append");
                    break;
                case '41'://代收貨款
                    _disItem = $("._arrive_to_pay_append,._arrive_to_pay_freight");
                    _setItem = $("._collection_money");
                    break;
                default:
                    _isOK = false;
                    break;
            }
            if (_isOK) {

                _disItem.map(function () { $(this).prop('readonly', true); });
                _setItem.map(function () { $(this).prop('readonly', false); });

            }

        });


        //$(function () {
        //    var baseIndex = 100;

        //    $("#page-wrapper").find("input[type=text]").each(function (r) {
        //        $(this).attr("tabindex", r * 100 + baseIndex).addClass("cGridInput");
        //    });

        //    //$("#page-wrapper").find("input").each(function (r) {
        //    //    $(this).attr("tabindex", r * 100 + baseIndex).addClass("cGridInput");
        //    //});
        //    //$("#page-wrapper")
        //    //.find("div").each(function (r) {
        //    //    $(this).find("input")
        //    //    .attr("tabindex", r * 100  + baseIndex)
        //    //    .addClass("cGridInput");
        //    //});
        //    $("#page-wrapper .cGridInput").on("keydown", function (evt) {
        //        var tabIndex = parseInt($(this).attr("tabindex"));
        //        switch (evt.which) {
        //            case 38: //上
        //                tabIndex -= 100;
        //                break;
        //            case 40: //下
        //                tabIndex += 100;
        //                break;
        //            //case 37: //左(會導致輸入時無法使用左右移)
        //            //    tabIndex--;
        //            //    break;
        //            //case 39: //右(會導致輸入時無法使用左右移)
        //            //    tabIndex++;
        //            //    break;
        //            default:
        //                return;
        //        }
        //        if (tabIndex > 0) {
        //            $(".cGridInput[tabindex=" + tabIndex + "]").focus();
        //            return false;
        //        }
        //        return true;
        //    });
        //});
       

    </script>


    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            margin-right: 15px;
            text-align :left ;
        }

        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }

        ._ddl {
            min-width: 85px;
        }

        ._addr {
            min-width: 35%;
        }

        .div_addr {
            width: 75%;
        }

        .ralign {
           text-align :right ;
        }

        
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">一筆式託運單</h2>
            <hr />
            <!-- 流程 -->
            <%--<ul class="wizard">
                <li class="current">填寫寄件資料</li>
                <li>確認寄件內容</li>
                <li>完成</li>
            </ul>--%>

            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <label for="inputFirstName">託運類別：</label>
                            <div class="margin-right-15 templatemo-inline-block">
                                <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged" CssClass="radio radio-success">
                                    <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                    <asp:ListItem Value="02">02 論件</asp:ListItem>
                                    <asp:ListItem Value="03">03 論才</asp:ListItem>
                                    <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                    <asp:ListItem Value="05">05 專車</asp:ListItem>
                                </asp:RadioButtonList>
                                <label>出貨日期：</label><asp:TextBox ID="Shipments_date" runat="server" class="form-control" CssClass="date_picker"  autocomplete="off" ></asp:TextBox>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label>客代編號：</label>
                            <asp:DropDownList ID="dlcustomer_code" runat="server" class="form-control chosen-select" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:Label ID="lbcustomer_name" runat="server"></asp:Label>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代編號</asp:RequiredFieldValidator>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <!--基本設定-->
                <!--2019/09/20 託運單號 原10碼限制修正為10 || 12 都可-->
                <div class="bs-callout bs-callout-info">
                    <h3>1. 基本設定</h3>
                    <div class="rowform">
                        <div class="row form-inline">
                            <div class="form-group">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" >
                                    <ContentTemplate>
                                        <label for="check_number"><span class="REDWD_b">*</span>託運單號</label>
                                        <asp:TextBox ID="check_number" runat="server" class="form-control" AutoPostBack="True" MaxLength="12" OnTextChanged="check_number_TextChanged"></asp:TextBox>
                                    </ContentTemplate>
                                    
                                </asp:UpdatePanel>
                                <%-- <asp:TextBox ID="check_number" runat="server" class="form-control" AutoPostBack="True" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" OnTextChanged="check_number_TextChanged"></asp:TextBox>--%>
                            </div>
                            <div class="form-group">
                                <label for="order_number">訂單號碼</label>
                                <asp:TextBox ID="order_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="check_type"><span class="REDWD_b">*</span>託運類別</label>
                                <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="receive_customer_code">收貨人編號</label>
                                <asp:TextBox ID="receive_customer_code" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="subpoena_category"><span class="REDWD_b">*</span>傳票類別</label>
                                <asp:DropDownList ID="subpoena_category" runat="server" class="form-control subpoena_category">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="check_number" ForeColor="Red" ValidationGroup="validate">請輸入託運單號</asp:RequiredFieldValidator>
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lbErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </div>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h3>2. 收件人</h3>
                    <div class="rowform">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <label>收件人編號</label>
                                        <asp:TextBox ID="receiver_code" runat="server" class="form-control" AutoPostBack ="true" OnTextChanged ="receiver_code_TextChanged"></asp:TextBox>                                        
                                        <asp:Button ID="btnRecsel" CssClass="templatemo-white-button" runat="server" Text="通訊錄" Font-Size="X-Small" OnClick="btnRecsel_Click" />
                                        <label><span class="REDWD_b">*</span>收件人</label>
                                        <asp:TextBox ID="receive_contact" runat="server" class="form-control" AutoPostBack="True" OnTextChanged="receive_contact_TextChanged" MaxLength="20" ></asp:TextBox>
                                        <asp:Button style="display: none" ID="btnqry" runat="server" Text="" />
                                        <div style="display: none">
                                            <asp:TextBox ID="receiver_id" runat="server" />
                                        </div>
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="cbSaveReceive" runat="server" Text="設為常用收件人" />
                                        </span>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="receive_contact" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入收件人</asp:RequiredFieldValidator>
                                        <%--<span id="btnreceive" runat="server" class="templatemo-white-button"><a id="Receiveclick" class="fancybox fancybox.iframe" style="color: #39ADB4; font-weight: 200;" 
                                    href="ReceiveSel.aspx?id=<%=receiver_id.ClientID %>&tel1=<%=receive_tel1.ClientID%>&ext=<%=receive_tel1_ext.ClientID%>&tel2=<%=receive_tel2.ClientID%>&name=<%=receive_contact.ClientID %>
                                    &city=<%=receive_city.ClientID%>&area=<%=receive_area.ClientID %>&address=<%=receive_address.ClientID %>">...</a></span>--%>
                                    </div>
                                    <div class="form-group">
                                        <label><span class="REDWD_b">*</span>電話1</label>
                                        <asp:TextBox ID="receive_tel1" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10" ></asp:TextBox>
                                        分機
                                    <asp:TextBox ID="receive_tel1_ext" runat="server" class="form-control" MaxLength="5" Width="80"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="receive_tel1" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入電話</asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>電話2</label>
                                        <asp:TextBox ID="receive_tel2" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                    </div>
                                    <div id="divReceiver" runat="server" visible="false" class="templatemo-login-form panel panel-default " style="overflow: auto; height: 300px;  background-color:aliceblue ">
                                        <br />
                                        <div>
                                            <div class="form-group form-inline">
                                                <label>關鍵字：</label>
                                                <asp:TextBox ID="keyword" runat="server" class="form-control"></asp:TextBox>
                                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                                <asp:Button ID="cancel" CssClass="btn btn-default " runat="server" Text="關 閉" OnClick="cancel_Click" />
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered templatemo-user-table tab">
                                            <tr class="tr-only-hide">
                                                <th>選取</th>
                                                <th>收件人代號</th>
                                                <th>收件人名稱</th>
                                                <th>收件人地址</th>
                                            </tr>
                                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td data-th="選取">
                                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                                            <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                                        </td>
                                                        <td data-th="收件人代號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                                        <td data-th="收件人名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                                        <td data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_road").ToString())%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (New_List.Items.Count == 0)
                                                {%>
                                            <tr>
                                                <td colspan="4" style="text-align: center">尚無資料</td>
                                            </tr>
                                            <% } %>
                                        </table>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                    <div class="form-group div_addr">
                                        <label><span class="REDWD_b">*</span>地　址</label>
                                        <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="receive_city" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入縣市</asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control _ddl" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="receive_area" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入行政區</asp:RequiredFieldValidator>
                                        <asp:TextBox ID="receive_address" CssClass="form-control _addr" runat="server" MaxLength ="70" placeholder="請輸入地址"></asp:TextBox>
                                        &nbsp;<asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="receive_address" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入地址</asp:RequiredFieldValidator>
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="cbarrive" runat="server" Text="站址自領" AutoPostBack="True" OnCheckedChanged="cbarrive_CheckedChanged" />
                                        </span>
                                        <asp:DropDownList ID="area_arrive_code" runat="server" CssClass="form-control _ddl"   AutoPostBack="True" OnSelectedIndexChanged="area_arrive_code_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="area_arrive_code" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇到著碼</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h3>3. 才件代收</h3>
                    <div class="rowform">
                        <div class="row form-inline">
                            <div class="form-group">                               
                                <asp:Label ID="Label1" runat="server"  Text="件數" CssClass ="ralign" Width="80px" ></asp:Label><asp:TextBox ID="pieces" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label2" runat="server" Text="板數"  CssClass ="ralign" Width="130px"></asp:Label><asp:TextBox ID="plates" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label3" runat="server" Text="才數"  CssClass ="ralign" Width="80px"></asp:Label><asp:TextBox ID="cbm" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>
                            </div>
                            <asp:Label ID="lbErrQuant" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                        </div>
                        <div class="row form-inline">
                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="＄代收金額" CssClass ="ralign" Width="80px"></asp:Label><asp:TextBox ID="collection_money" CssClass="form-control  _collection_money" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label5" runat="server" Text="＄到付運費(含稅)" CssClass ="ralign" Width="130px"></asp:Label><asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control  _arrive_to_pay_freight" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label6" runat="server" Text="＄到付追加" CssClass ="ralign" Width="80px"></asp:Label><asp:TextBox ID="arrive_to_pay_append" CssClass="form-control  _arrive_to_pay_append" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="form-group" id="individual_fee" runat="server" style="display: none;">
                                        <asp:Label ID="Label7" runat="server" Text="＄貨件運費" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_supplier_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor ="#333333" Font-Size ="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label8" runat="server" Text="＄配送費用" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="i_csection_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor ="#333333" Font-Size ="Medium"  runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label9" runat="server" Text="＄偏遠加價" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_remote_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor ="#333333" Font-Size ="Medium"  runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h3>*<br>
                        特殊設定</h3>
                    <div class="rowform">
                        <%--<div class="row form-inline">
                            <div class="form-group">
                                <asp:CheckBox ID="donate_invoice_flag" class="font-weight-400" runat="server" Text="發票捐贈" />
                                &nbsp;&nbsp;
                                <asp:CheckBox ID="electronic_invoice_flag" class="font-weight-400" runat="server" Text="電子發票" />
                                <asp:TextBox ID="uniform_numbers" CssClass="form-control" runat="server" placeholder="統一編號"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="row form-inline">
                            <%--<div class="form-group">
                                <label>商品種類</label>
                                <asp:DropDownList ID="product_category" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label for="inputFirstName">手機</label>
                                <asp:TextBox ID="arrive_mobile" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                            </div>--%>
                            <div class="form-group">
                                <label for="inputLastName">特殊配送</label>
                                <asp:DropDownList ID="special_send" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <%-- <div class="form-group">
                                <label for="inputFirstName">E-mail</label>
                                <asp:TextBox ID="arrive_email" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>--%>
                            <div class="form-group">
                                <label for="inputLastName">指定日</label>
                                <asp:TextBox ID="arrive_assign_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                <label for="inputLastName">時段</label>
                                <asp:DropDownList ID="time_period" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                &nbsp;
                            </div>

                        </div>
                        <div class="row form-inline">
                            <div class="form-group">
                                <label for="inputLastName">備　　註</label>
                                <%--<asp:DropDownList ID="invoice_memo" runat="server" CssClass="form-control">
                                </asp:DropDownList>--%>
                                <textarea id="invoice_desc" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <span class="checkbox checkbox-success">
                                    <asp:CheckBox ID="receipt_flag" runat="server" Text="回單" /></span>
                                <br />
                                <span class="checkbox checkbox-success">
                                    <asp:CheckBox ID="pallet_recycling_flag" runat="server" Text="棧板回收" onclick="pallet_recycling_flagClick(this)" />
                                    <a id="carsel" href="transport_1_carsel.aspx?Pallet_type=<%=Pallet_type.ClientID %>&Pallet_type_text=<%=Pallet_type_text.ClientID %>" class="fancybox fancybox.iframe " ></a>
                                </span>
                                <div id="Pallet_type_div" style="display:none;">
                                    棧板種類<asp:TextBox ID="Pallet_type_text" CssClass="form-control" runat="server" Width="80px" ReadOnly></asp:TextBox>
                                        <asp:HiddenField ID="Pallet_type" runat="server" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <span class="checkbox checkbox-success">
                                    <asp:CheckBox ID="turn_board" runat="server" Text="翻板" /></span>
                                ＄<asp:TextBox ID="turn_board_fee" CssClass="form-control _turn_board_fee" runat="server"  Enabled="false"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                <br />
                                <span class="checkbox checkbox-success">
                                    <asp:CheckBox ID="upstairs"  runat="server" Text="上樓" /></span>
                                ＄<asp:TextBox ID="upstairs_fee" CssClass="form-control _upstairs_fee" runat="server"  Enabled="false"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                <br />
                                <span class="checkbox checkbox-success">
                                    <asp:CheckBox ID="difficult_delivery" runat="server" Text="困配" /></span>
                                ＄<asp:TextBox ID="difficult_fee" CssClass="form-control _difficult_fee" runat="server"  Enabled="false"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="bs-callout bs-callout-info">
                    <h3>4. 寄件人</h3>
                    <div class="rowform">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <label>&nbsp;客戶代號</label>
                                                <asp:TextBox ID="customer_code" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="form-group">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="chkshowsend" class="font-weight-400" runat="server" Text="顯示寄件人資料" AutoPostBack="True" Checked="True" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                        </span>
                                        &nbsp;
                                <label>寄件人</label>
                                        <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <label>電話/手機</label>
                                        <asp:TextBox ID="send_tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                    <div class="form-group div_addr">
                                        <label>&nbsp;寄件地址</label>
                                        <asp:DropDownList ID="send_city" runat="server" CssClass="form-control _ddl" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="send_area" runat="server" CssClass="form-control _ddl"></asp:DropDownList>
                                        <asp:TextBox ID="send_address" CssClass="form-control _addr" runat="server"></asp:TextBox>
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="chksend_address" class="font-weight-400" runat="server" Text="無地址" />
                                        </span>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnsave_Click" />
                    <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取 消" />
                </div>

            </div>
        </div>
    </div>
    <script>
        function pallet_recycling_flagClick(e) {
            if (e.checked == true) {
                $('a#carsel').trigger('click');
                $("#Pallet_type_div").attr("style", "display:block;");
            } else {
                $("#Pallet_type_div").attr("style", "display:none;");
                $("#<%=Pallet_type.ClientID%>").val();
                $("#<%=Pallet_type_text.ClientID%>").val();
            }
        }
    </script>
</asp:Content>

