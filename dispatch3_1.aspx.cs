﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dispatch3_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }
    }

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    public string ssApStatus
    {
        // for權限
        get { return ViewState["ssApStatus"].ToString(); }
        set { ViewState["ssApStatus"] = value; }
    }

    public List<DispatchTask> DispatchTasks
    {
        get
        {
            if (ViewState["DispatchTasks"] != null)
            {
                return (List<DispatchTask>)ViewState["DispatchTasks"];
            }
            else
            {
                return new List<DispatchTask>();
            }
        }
        set { ViewState["DispatchTasks"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssApStatus = "Browse";
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4" || ssManager_type == "5")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }

            #region   供應商
            supplier.DataSource = Utility.getSupplierDT2(ssSupplier_code, ssMaster_code);
            supplier.DataValueField = "supplier_no";
            supplier.DataTextField = "showname";
            supplier.DataBind();
            supplier.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            

            //readdata();
        }
    }

    //private void readdata()
    //{
    //    SqlCommand cmd = new SqlCommand();
    //    string wherestr = "";

    //    #region 關鍵字

    //    //權限

    //    if (ssSupplier_code != "")
    //    {
    //        cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
    //        wherestr += " and A.supplier_code = @supplier_code";
    //    }

    //    if ((ssMaster_code != "") && (ssManager_type == "3" || ssManager_type == "5"))
    //    {
    //        cmd.Parameters.AddWithValue("@Master_code", ssMaster_code);
    //        wherestr += " and A.customer_code like @Master_code+'%' ";
    //    }

    //    //if (!String.IsNullOrEmpty(dateS.Text) && !String.IsNullOrEmpty(dateE.Text))
    //    //{
    //    //    cmd.Parameters.AddWithValue("@dateS", dateS.Text);
    //    //    cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime( dateE.Text).AddDays(1).ToString("yyyy/MM/dd"));
    //    //    wherestr += " AND A.print_date >=@dateS and  A.cdate <@dateE ";
    //    //}

    //    if (!String.IsNullOrEmpty(date.Text))
    //    {
    //        cmd.Parameters.AddWithValue("@dateS", date.Text);
    //        cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(date.Text).AddDays(1).ToString("yyyy/MM/dd"));
    //        wherestr += " AND A.print_date >=@dateS and  A.cdate <@dateE ";
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇發送日期');</script>", false);
    //        return;
    //    }


    //    if (keyword.Text != "")
    //    {
    //        cmd.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
    //        wherestr += "  and (B.customer_name like '%'+@keyword+'%' or A.send_city +A.send_area + A.send_address like '%'+@keyword+'%' or A.receive_contact like '%'+@keyword+'%' or E.supplier_name like '%'+@keyword+'%' or A.area_arrive_code like '%'+@keyword+'%')";
    //    }

    //    //if (rb_pricing_type.SelectedValue != "")
    //    //{
    //    //    cmd.Parameters.AddWithValue("@pricing_type", rb_pricing_type.SelectedValue);
    //    //    wherestr += " and A.pricing_type = @pricing_type";

    //    //}

    //    //if (Customer.SelectedValue != "")
    //    //{
    //    //    cmd.Parameters.AddWithValue("@customer_code", Customer.SelectedValue);
    //    //    wherestr += " and A.customer_code like @customer_code+'%' ";

    //    //}


    //    #endregion

    //    cmd.CommandText = string.Format(@"WITH task AS
    //                                     (Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
    //                                         A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name',
    //                                         ISNULL(F.t_id, 0) 't_id', ISNULL(G.task_id,'') 'task_id',
    //                                      ROW_NUMBER() OVER (PARTITION BY A.request_id ORDER BY A.request_id DESC) AS rn
    //                                         from tcDeliveryRequests A with(nolock)  
    //                                         LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
    //                                         LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
    //                                         LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
    //                                         LEFT JOIN ttDispatchTaskDetail F with(Nolock) on A.request_id = F.request_id 
    //                                      LEFT JOIN ttDispatchTask G with(Nolock) on F.t_id = G.t_id 
    //                                      inner JOIN ttDeliveryScanLog H with(Nolock) on  A.check_number =  H.check_number  and H.scan_date >= DATEADD(MONTH,-6,getdate()) and H.scan_item = '6'
    //                                         WHERE A.pricing_type <> '05' and A.supplier_code <> '001'
    //                                         {0} )
    //                                     Select * from task where rn = 1  ORDER BY print_date, customer_code , area_arrive_code", wherestr);

    //    using (DataTable dt = dbAdapter.getDataTable(cmd))
    //    {   
    //        New_List.DataSource = dt;
    //        New_List.DataBind();
    //        ltotalpages.Text = dt.Rows.Count.ToString();
    //        DT = dt;
    //    }

    //}

    private void readdata()
    {
        ssApStatus = "Browse";
        SqlCommand cmd = new SqlCommand();
        #region 關鍵字
        if (!String.IsNullOrEmpty(date.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", date.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(date.Text).AddDays(1).ToString("yyyy/MM/dd"));            
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇卸集日期');</script>", false);
            return;
        }

        
        #endregion

        cmd.CommandText = string.Format(@"declare @pvtheaders nvarchar(max) ,@sql nvarchar(max) --@sumsql     動態組出樞紐資料行相加
                                            SELECT   @pvtheaders=ISNULL(@pvtheaders,'')+'['+ supplier_code  +'],'
                                            from tbSuppliers
                                            where supplier_code <> '' and active_flag = 1
                                            GROUP BY supplier_code
                                            set @pvtheaders = left(@pvtheaders, len(@pvtheaders) - 1)
                                            --select @pvtheaders

                                            select A.area_arrive_code 'warehouse' , C.code_name 'warehouse_text' ,  B.plates , B.area_arrive_code ,
                                            ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number DESC) AS rn into #scsn
                                            from ttDeliveryScanLog A with(nolock) 
                                            inner join tcDeliveryRequests B with(nolock) on A.check_number = B.check_number and B.print_date >=  DATEADD(MONTH,-6,getdate())
                                            left join tbItemCodes C with(nolock) on   A.area_arrive_code = C.code_id  and C.code_bclass = '1' and C.code_sclass = 'warehouse'
                                            where A.scan_item = '6' and A.scan_date >= @dateS and A.scan_date < @dateE
                                            --Select * from #scsn where rn = 1 

                                            --動態PIVOT
                                            set @sql = 'SELECT * FROM  (
                                            select A.warehouse, A.warehouse_text, A.area_arrive_code, A.plates -ISNULL( B.plates,0) ''plates''  
	                                        from (Select warehouse,warehouse_text,area_arrive_code,sum(plates) ''plates''   from #scsn where rn= 1 
	                                        group by warehouse,warehouse_text,area_arrive_code ) A
                                            left join (select warehouse, supplier , sum(plates) ''plates''from ttDispatchTaskDetail_B with(nolock) where CONVERT(varchar(100), '''+ @dateS +''', 111)  = CONVERT(varchar(100), date, 111)
	                                        group by warehouse, supplier ) B on  A.warehouse = B.warehouse and A.area_arrive_code= B.supplier
                                            
                                            ) p
                                            pivot
                                            (SUM(plates)
                                            for area_arrive_code in ('+ @pvtheaders +')) AS pvt
                                            order by warehouse'
                                            --select @sql
                                            exec sp_executesql @sql
                                            drop table #scsn");

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            dt.Columns.Add("tot", typeof(int));
            int Cnt = dt.Columns.Count ;
            int[] Tot = new int[Cnt];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    int subtot = 0;
                    for (int j = 2; j <= dt.Columns.Count - 2; j++)
                    {
                        int plates = dt.Rows[i][j] != DBNull.Value ? Convert.ToInt32(dt.Rows[i][j]) : 0;
                        subtot += plates;
                        Tot[j-1] = Tot[j-1] + plates;
                    }
                    dt.Rows[i]["tot"] = subtot;
                }
                DataRow row = dt.NewRow();
                row[1] = "總計";
                int tot = 0;
                for (int j = 2; j <= dt.Columns.Count - 1; j++)
                {   if (j == dt.Columns.Count - 1)
                    {
                        row[j] = tot;
                    }
                    else
                    {
                        row[j] = Tot[j - 1];
                        tot += Tot[j - 1];
                    }
                }
                dt.Rows.Add(row);

            }

            New_List.DataSource = dt;
            New_List.DataBind();            
            DT = dt;
        }

    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = (p["tires_number"] == DBNull.Value ) ? 0 : p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = (p["oil_discount"] == DBNull.Value) ? 0 : p.Field<double>("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = (p["tires_monthlyfee"] == DBNull.Value) ? 0 : p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    
    protected void AddBTask_Click(object sender, EventArgs e)
    {
        ssApStatus = "Edit";
        DataTable dt = DT;
        New_List.DataSource = dt;
        New_List.DataBind();
        if (UpdatePanel2.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel2.Update();
        }
        Addplates.Text = "0";
        Hid_maxplates.Value = "0";
        tbplates.Text = "0";
        DispatchTasks = new List<DispatchTask>();
        pl_AddTadk.Visible = true;
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Add")
        {
            string supplier = e.CommandArgument.ToString();
            string plates = ((LinkButton)e.Item.FindControl("bt"+ supplier )).Text.ToString().Trim();
            string warehouse= ((Literal)e.Item.FindControl("Li_warehouse")).Text.ToString().Trim();
            if (warehouse != "")
            {
                int _plates = 0;
                int.TryParse(plates, out _plates);
                Addplates.Text = _plates.ToString();
                Hid_maxplates.Value = _plates.ToString();
                addwarehouse.Text = warehouse;
                addsupplier.Text = supplier;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "retVal", "$('#modal-default_01').modal();", true);
                return;
            }
        }
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        List<DispatchTask> ttDispatchTasks = DispatchTasks;
        int _plates = 0;
        int _add_plates = 0;
        int.TryParse(tbplates.Text , out _plates);
        int.TryParse( Addplates.Text, out _add_plates);
        tbplates.Text = (_plates+ _add_plates).ToString();
        DispatchTask _DispatchTask = DispatchTasks.Find(x => x.warehouse == addwarehouse.Text && x.supplier == addsupplier.Text);
        if (_DispatchTask != null)
        {
            _DispatchTask.plates += Convert.ToInt32(Addplates.Text);            
        }
        else {
            _DispatchTask = new DispatchTask();
            _DispatchTask.warehouse = addwarehouse.Text;
            _DispatchTask.supplier = addsupplier.Text;
            _DispatchTask.plates = Convert.ToInt32(Addplates.Text);
            ttDispatchTasks.Add(_DispatchTask);
        }
       
        
        DispatchTasks = ttDispatchTasks;


        SqlCommand cmd = new SqlCommand();      
        if (!String.IsNullOrEmpty(date.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", date.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(date.Text).AddDays(1).ToString("yyyy/MM/dd"));
            cmd.CommandText = string.Format(@"declare @pvtheaders nvarchar(max) ,@sql nvarchar(max) --@sumsql     動態組出樞紐資料行相加
                                            SELECT   @pvtheaders=ISNULL(@pvtheaders,'')+'['+ supplier_code  +'],'
                                            from tbSuppliers
                                            where supplier_code <> '' and active_flag = 1
                                            GROUP BY supplier_code
                                            set @pvtheaders = left(@pvtheaders, len(@pvtheaders) - 1)
                                            --select @pvtheaders

                                            select A.area_arrive_code 'warehouse' , C.code_name 'warehouse_text' ,  B.plates , B.area_arrive_code ,
                                            ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number DESC) AS rn into #scsn
                                            from ttDeliveryScanLog A with(nolock) 
                                            inner join tcDeliveryRequests B with(nolock) on A.check_number = B.check_number and B.print_date >=  DATEADD(MONTH,-6,getdate())
                                            left join tbItemCodes C with(nolock) on   A.area_arrive_code = C.code_id  and C.code_bclass = '1' and C.code_sclass = 'warehouse'
                                            where A.scan_item = '6' and A.scan_date >= @dateS and A.scan_date < @dateE
                                            --Select * from #scsn where rn = 1 

                                            --動態PIVOT
                                            set @sql = 'SELECT * FROM  (
                                            select A.warehouse, A.warehouse_text, A.area_arrive_code, A.plates -ISNULL( B.plates,0) ''plates''  
	                                        from (Select warehouse,warehouse_text,area_arrive_code,sum(plates) ''plates''   from #scsn where rn= 1 
	                                        group by warehouse,warehouse_text,area_arrive_code ) A
                                            left join (select warehouse, supplier , sum(plates) ''plates''from ttDispatchTaskDetail_B with(nolock) where CONVERT(varchar(100), '''+ @dateS +''', 111)  = CONVERT(varchar(100), date, 111)
	                                        group by warehouse, supplier ) B on  A.warehouse = B.warehouse and A.area_arrive_code= B.supplier
                                            
                                            ) p
                                            pivot
                                            (SUM(plates)
                                            for area_arrive_code in ('+ @pvtheaders +')) AS pvt
                                            order by warehouse'
                                            --select @sql
                                            exec sp_executesql @sql
                                            drop table #scsn");

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                dt.Columns.Add("tot", typeof(int));
                int Cnt = dt.Columns.Count;
                int[] Tot = new int[Cnt];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        int subtot = 0;
                        string warehouse = dt.Rows[i]["warehouse"].ToString();
                        for (int j = 2; j <= dt.Columns.Count - 2; j++)
                        {   
                            string supplier = dt.Columns[j].ColumnName;
                            int plates = dt.Rows[i][j] != DBNull.Value ? Convert.ToInt32(dt.Rows[i][j]) : 0; 
                            DispatchTask _temp = DispatchTasks.Find(x => x.warehouse == warehouse && x.supplier == supplier);
                            if (_temp != null )
                            {
                                plates = plates - _temp.plates;
                                dt.Rows[i][j] = plates;
                            }
                            subtot += plates;
                            Tot[j - 1] = Tot[j - 1] + plates;
                        }
                        dt.Rows[i]["tot"] = subtot;
                    }
                    DataRow row = dt.NewRow();
                    row[1] = "總計";
                    int tot = 0;
                    for (int j = 2; j <= dt.Columns.Count - 1; j++)
                    {
                        if (j == dt.Columns.Count - 1)
                        {
                            row[j] = tot;
                        }
                        else
                        {
                            row[j] = Tot[j - 1];
                            tot += Tot[j - 1];
                        }
                    }
                    dt.Rows.Add(row);

                }

                New_List.DataSource = dt;
                New_List.DataBind();
                DT = dt;
            }
        }
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        ssApStatus = "Browse";
        pl_AddTadk.Visible = false;
        Clear();
        readdata();
    }

    protected void Clear()
    {   
        tbplates.Text = "0";
        task_date.Text = "";
        supplier.SelectedIndex = -1;
        driver.Text = "";
        Hid_assets_id.Value ="";
        car_license.Text = "";
        price.Text = "0";
        memo.Text = "";
        Addplates.Text = "0";
        Hid_maxplates.Value = "0";
        tbplates.Text = "0";

    }


    protected void btn_save_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        List<DispatchTask> ttDispatchTasks = DispatchTasks;
        int t_id = 0;
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@task_date", DateTime.Today);
            cmd.CommandText = @"Select ISNULL(max(serial),0) from ttDispatchTask with(nolock) where task_date = @task_date ";
            DataTable dt = dbAdapter.getDataTable(cmd);
            if (dt != null && dt.Rows.Count > 0)
            {
                string serial = (Convert.ToInt32(dt.Rows[0][0]) + 1).ToString("000");
                Task_id.Text = DateTime.Today.ToString("yyyyMMdd") + "-" + serial;   //任務編號：日期+"-"+ 3碼流水號
            }
        }

        SqlCommand cmdadd = new SqlCommand();
        cmdadd.Parameters.AddWithValue("@task_type", 1);
        cmdadd.Parameters.AddWithValue("@task_id", Task_id.Text);
        cmdadd.Parameters.AddWithValue("@task_date", task_date.Text);
        //cmdadd.Parameters.AddWithValue("@supplier", supplier.SelectedValue);
        cmdadd.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue);
        cmdadd.Parameters.AddWithValue("@serial", Task_id.Text.Split('-')[1]);
        cmdadd.Parameters.AddWithValue("@driver", driver.Text);
        cmdadd.Parameters.AddWithValue("@car_license", Hid_assets_id.Value );
        cmdadd.Parameters.AddWithValue("@plates", tbplates.Text);
        cmdadd.Parameters.AddWithValue("@price", price.Text);
        cmdadd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
        cmdadd.Parameters.AddWithValue("@c_user", Session["account_code"]);                  //建立人員
        cmdadd.Parameters.AddWithValue("@c_time", DateTime.Now);                             //建立時間
        cmdadd.CommandText = dbAdapter.genInsertComm("ttDispatchTask", true, cmdadd);        //新增
        if (int.TryParse(dbAdapter.getScalarBySQL(cmdadd).ToString(), out t_id))
        {
            if (ttDispatchTasks != null && ttDispatchTasks.Count > 0)
            {
                for (int i = 0; i <= ttDispatchTasks.Count -1; i++)
                {
                    DispatchTask _DispatchTask = ttDispatchTasks[i];
                    using (SqlCommand cmd2 = new SqlCommand())
                    {   
                        cmd2.Parameters.AddWithValue("@t_id", t_id);
                        cmd2.Parameters.AddWithValue("@date", date.Text );
                        cmd2.Parameters.AddWithValue("@warehouse", _DispatchTask.warehouse);
                        cmd2.Parameters.AddWithValue("@supplier", _DispatchTask.supplier);
                        cmd2.Parameters.AddWithValue("@plates", _DispatchTask.plates);
                        cmd2.CommandText = dbAdapter.SQLdosomething("ttDispatchTaskDetail_B", cmd2, "insert");
                        try
                        {
                            dbAdapter.execNonQuery(cmd2);
                        }
                        catch (Exception ex)
                        {
                            ErrStr = ex.Message;
                        }
                    }
                }
            }
        }
        

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            btn_cancel_Click(null, null);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#ContentPlaceHolder1_btnQry').click();parent.$.fancybox.close();alert('儲存完成'); </script>", false);
        }
        return;
    }

    [Serializable]
    public class DispatchTask
    {
        public string warehouse { get; set; }
        public string supplier { get; set; }
        public int plates { get; set; }

    }
    
}