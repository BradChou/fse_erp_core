﻿using NPOI.SS.Formula.Functions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Windows.Forms;

public partial class GetDownload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (!IsPostBack)





        if (!loginchk.IsLogin())
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        else
        {
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板=0 or 零擔=1

         



            int i_dw_type = 0; //0:其他(預設) 1:下載Excel             
            int i_public = 1;//1:取公版(預設) 0:取自定
            int i_customer_id = 0;//客戶序號(ID)
            String str_cus_code = string.Empty;//客戶代碼
            String str_active_date = string.Empty;//生效日

            if (Request.QueryString["cus"] != null && !int.TryParse(Request.QueryString["cus"].ToString(), out i_customer_id)) i_customer_id = 0;
            if (Request.QueryString["dw"] != null && !int.TryParse(Request.QueryString["dw"].ToString(), out i_dw_type)) i_dw_type = 0;
            if (Request.QueryString["pub"] != null && !int.TryParse(Request.QueryString["pub"].ToString(), out i_public)) i_public = 1;
            if (Request.QueryString["cus_c"] != null) str_cus_code = Request.QueryString["cus_c"].ToString();
            if (Request.QueryString["atdate"] != null) str_active_date = Request.QueryString["atdate"].ToString();



            //MessageBox.Show(Convert.ToString(i_customer_id));
            //MessageBox.Show(Convert.ToString(i_dw_type));
            //MessageBox.Show(Convert.ToString(i_public));
            //MessageBox.Show(Convert.ToString(str_cus_code));
            //MessageBox.Show(Convert.ToString(str_active_date));


            switch (i_dw_type)
            {
                case 1:
                    DBToExcel(i_customer_id, str_cus_code, str_active_date, (i_public == 1 ? true : false));
                    break;
                default:
                    break;
            }

        }

    }

    /// <summary>匯出Excel檔FromDB</summary>
    /// <param name="customer_id">客戶序號(ID)</param>
    /// <param name="customer_code">客戶代碼</param>
    /// <param name="active_date">生效日</param>
    /// <param name="IsPublic">是否取得公版資料</param>
    protected void DBToExcel(int customer_id, string customer_code, string active_date, Boolean IsPublic = true)
    {
        Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板=0 or 零擔=1
       
        if (Less_than_truckload=="0")
        {
            // DateTime dt_active_date = new DateTime();
            //DateTime.TryParse(Request.QueryString["atdate"].ToString()
            String str_retuenMsg = string.Empty;
            DateTime dt_active_date = new DateTime();
            try
            {
                Boolean IsChkOk = false;
                lbMsg.Items.Clear();
                if (!IsPublic)
                {
                    if (DateTime.TryParse(active_date, out dt_active_date)
                        && customer_id > 0
                        && customer_code.Length > 0) IsChkOk = true;
                }
                else
                {
                    if (customer_code.Length > 0) IsChkOk = true;
                }

                #region 撈DB寫入EXCEL
                if (IsChkOk)
                {
                    string sheet_title = "公版運價";
                    string file_name = customer_code + DateTime.Now.ToString("yyyyMMdd");

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "usp_GetPublicShipFeeByCustomerId";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@customer_id", customer_id.ToString());
                        cmd.Parameters.AddWithValue("@ForExcel", "1");//0:撈欄位原始資訊 1:僅撈公版匯出欄位，且Title為中文

                        //取自定義價格
                        if (!IsPublic)
                        {
                            cmd.Parameters.AddWithValue("@publicYN", "0");
                            cmd.Parameters.AddWithValue("@active_date", dt_active_date.ToString("yyyy/MM/dd"));
                            sheet_title = "自訂運價";
                            file_name = customer_code + dt_active_date.ToString("yyyyMMdd");
                        }
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                using (ExcelPackage p = new ExcelPackage())
                                {
                                    p.Workbook.Properties.Title = sheet_title;
                                    p.Workbook.Worksheets.Add(sheet_title);
                                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                                    int colIndex = 1;
                                    int rowIndex = 1;
                                    foreach (DataColumn dc in dt.Columns)
                                    {
                                        var cell = ws.Cells[rowIndex, colIndex];
                                        var fill = cell.Style.Fill;
                                        fill.PatternType = ExcelFillStyle.Solid;
                                        fill.BackgroundColor.SetColor(Color.LightGray);

                                        //Setting Top/left,right/bottom borders.
                                        var border = cell.Style.Border;
                                        border.Bottom.Style =
                                            border.Top.Style =
                                            border.Left.Style =
                                            border.Right.Style = ExcelBorderStyle.Thin;

                                        //Setting Value in cell
                                        cell.Value = dc.ColumnName;
                                        colIndex++;
                                    }
                                    rowIndex++;

                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        colIndex = 1;
                                        foreach (DataColumn dc in dt.Columns)
                                        {
                                            var cell = ws.Cells[rowIndex, colIndex];
                                            cell.Value = dr[dc.ColumnName];

                                            //Setting borders of cell
                                            var border = cell.Style.Border;
                                            border.Left.Style =
                                                border.Right.Style = ExcelBorderStyle.Thin;
                                            colIndex++;
                                        }

                                        rowIndex++;
                                    }
                                    ws.View.FreezePanes(2, 1);
                                    ws.Cells.Style.Font.Size = 12;
                                    ws.Column(dt.Columns.Count).Hidden = true;//最後一欄為table name需隱藏→匯入DB比對時使用
                                    Response.Clear();
                                    Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    Response.BinaryWrite(p.GetAsByteArray());
                                    Response.End();

                                    str_retuenMsg += "下載完成!";
                                }
                            }
                            else
                            {
                                str_retuenMsg += "查無此資訊，請重新確認!";
                            }

                            //lbMsg.
                        }
                    }

                }
                #endregion

            }
            catch (Exception ex)
            {
                str_retuenMsg += "系統異常，請洽相關人員!";
                str_retuenMsg += " (" + ex.Message.ToString() + ")";
            }
            finally
            {
                lbMsg.Items.Add(str_retuenMsg);
            }
        }
        else
        {
            String str_retuenMsg = string.Empty;
            DateTime dt_active_date = new DateTime();
            try
            {
                Boolean IsChkOk = false;
                lbMsg.Items.Clear();
                if (!IsPublic)
                {
                    if (DateTime.TryParse(active_date, out dt_active_date)
                        && customer_id > 0
                        && customer_code.Length > 0) IsChkOk = true;
                }
                else
                {
                    if (customer_code.Length > 0) IsChkOk = true;
                }

                #region 撈DB寫入EXCEL
                if (IsChkOk)
                {
                    string sheet_title = "公版運價";
                    string file_name = customer_code + DateTime.Now.ToString("yyyyMMdd");

                    using (SqlCommand cmd = new SqlCommand())
                    {

                        string whereStr = "";
                        //取自定義價格
                        if (!IsPublic)
                        {
                            
                            sheet_title = "自訂運價";
                            file_name = customer_code +"_"+ dt_active_date.ToString("yyyyMMdd");
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@customer_code", customer_code);
                            cmd.Parameters.AddWithValue("@Enable_date", dt_active_date.ToString("yyyy-MM-dd") + "%" );
                            cmd.CommandText = @"Select start_city, end_city, Enable_date  from[dbo].[tbPriceLessThan]  where Enable_date is Not NULL 
                                            AND  convert(varchar,Enable_date,120) like @Enable_date  and  Customer_code = @Customer_code 
                                            group by start_city,end_city,Enable_date";

                        }
                        else
                        {
                            file_name = customer_code + "_"+ dt_active_date.ToString("yyyyMMdd");
                            cmd.CommandText = @"Select start_city, end_city, Enable_date  from[dbo].[tbPriceLessThan]  where Enable_date is Not NULL 
                                            AND   Enable_date  in  (SELECT MAX (Enable_date) FROM tbPriceLessThan where Customer_Code Is NULL) 
                                            group by start_city,end_city,Enable_date";
                        }
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                using (ExcelPackage p = new ExcelPackage())
                                {

                                    p.Workbook.Properties.Title = sheet_title;
                                    p.Workbook.Worksheets.Add(sheet_title);
                                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                                    int colIndex = 1;
                                    int rowIndex = 1;
                                    
                                  
                                        var cell = ws.Cells[rowIndex, colIndex];
                                        var fill = cell.Style.Fill;
                                        fill.PatternType = ExcelFillStyle.Solid;
                                        fill.BackgroundColor.SetColor(Color.LightGray);

                                        //Setting Top/left,right/bottom borders.
                                        var border = cell.Style.Border;
                                        border.Bottom.Style =
                                            border.Top.Style =
                                            border.Left.Style =
                                            border.Right.Style = ExcelBorderStyle.Thin;

                                        //Setting Value in cell
                                        cell = ws.Cells[1, 1];
                                        cell.Value = "起點區域";
                                        cell = ws.Cells[1, 2];
                                        cell.Value = "迄點區域";
                                        cell = ws.Cells[1, 3];
                                        cell.Value = "材積";
                                        cell = ws.Cells[1, 4];
                                        cell.Value = "首件費用";
                                        cell = ws.Cells[1, 5];
                                        cell.Value = "續件費用";
                                        cell = ws.Cells[1, 6];
                                        cell.Value = "材積";
                                        cell = ws.Cells[1, 7];
                                        cell.Value = "首件費用";
                                        cell = ws.Cells[1, 8];
                                        cell.Value = "續件費用";
                                        cell = ws.Cells[1, 9];
                                        cell.Value = "材積";
                                        cell = ws.Cells[1, 10];
                                        cell.Value = "首件費用";
                                        cell = ws.Cells[1, 11];
                                        cell.Value = "續件費用";
                                        cell = ws.Cells[1, 12];
                                        cell.Value = "材積";
                                        cell = ws.Cells[1, 13];
                                        cell.Value = "首件費用";
                                        cell = ws.Cells[1, 14];
                                        cell.Value = "續件費用";

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {

                                        SqlCommand cmdA = new SqlCommand();

                                        if (!IsPublic)
                                        {
                                            string SQLstr = @"Select A.start_city, A.end_city,A.Cbmsize , B.CbmSize 'sizename', A.first_price, A.add_price from tbPriceLessThan A 
                                        Left join tcCbmSize B on B.id =A.Cbmsize 
                                        Where  start_city=@start_city and end_city=@end_city and  convert(varchar,Enable_date,120) like @Enable_date    and  Customer_code = @Customer_code ";

                                            cmdA.Parameters.Clear();
                                            cmdA.Parameters.AddWithValue("@Enable_date", Convert.ToDateTime(dt.Rows[i]["Enable_date"]).ToString("yyyy-MM-dd") + "%" );
                                            cmdA.Parameters.AddWithValue("@start_city", dt.Rows[i]["start_city"].ToString());
                                            cmdA.Parameters.AddWithValue("@end_city", dt.Rows[i]["end_city"].ToString());
                                            cmdA.Parameters.AddWithValue("@customer_code", customer_code);
                                        
                                            cmdA.CommandText = SQLstr;
                                        }
                                        else
                                        {
                                            string SQLstr = @"Select A.start_city, A.end_city,A.Cbmsize , B.CbmSize 'sizename', A.first_price, A.add_price from tbPriceLessThan A 
                                        Left join tcCbmSize B on B.id =A.Cbmsize 
                                        Where  start_city=@start_city and end_city=@end_city and  convert(varchar,Enable_date,120) like @Enable_date   and Customer_code Is NULL";

                                         
                                            cmdA.Parameters.Clear();
                                            cmdA.Parameters.AddWithValue("@Enable_date", Convert.ToDateTime(dt.Rows[i]["Enable_date"]).ToString("yyyy-MM-dd") + "%");
                                            cmdA.Parameters.AddWithValue("@start_city", dt.Rows[i]["start_city"].ToString());
                                            cmdA.Parameters.AddWithValue("@end_city", dt.Rows[i]["end_city"].ToString());
                                            cmdA.CommandText = SQLstr;

                                        }



                                        rowIndex++;
                                        cell = ws.Cells[rowIndex, 1];






                                        cell.Value = dt.Rows[i]["start_city"].ToString();
                                        cell = ws.Cells[rowIndex, 2];
                                        cell.Value = dt.Rows[i]["end_city"].ToString();

                                        DataTable dt1 = dbAdapter.getDataTable(cmdA);
                                        if (dt1.Rows.Count > 0)
                                        {
                                            for (int j = 0; j < dt1.Rows.Count; j++)
                                            {
                                                cell = ws.Cells[rowIndex, Convert.ToInt32(dt1.Rows[j]["Cbmsize"].ToString()) * 3];
                                                cell.Value = dt1.Rows[j]["sizename"].ToString();
                                                cell = ws.Cells[rowIndex, Convert.ToInt32(dt1.Rows[j]["Cbmsize"].ToString()) * 3 + 1];
                                                cell.Value = dt1.Rows[j]["first_price"].ToString();
                                                cell = ws.Cells[rowIndex, Convert.ToInt32(dt1.Rows[j]["Cbmsize"].ToString()) * 3 + 2];
                                                cell.Value = dt1.Rows[j]["add_price"].ToString();


                                            }
                                        }
                                    }
                                   

                                    ws.View.FreezePanes(2, 1);
                                    ws.Cells.Style.Font.Size = 12;
                                    Response.Clear();
                                    Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    Response.BinaryWrite(p.GetAsByteArray());
                                    Response.End();

                                    str_retuenMsg += "下載完成!";
                                }
                            }
                            else
                            {
                                str_retuenMsg += "查無此資訊，請重新確認!";
                            }

                            //lbMsg.
                        }
                    }

                }
                #endregion
            }
            catch(Exception ex)
            {
                str_retuenMsg += "系統異常，請洽相關人員!";
                str_retuenMsg += " (" + ex.Message.ToString() + ")";
            }
            finally
            {
                lbMsg.Items.Add(str_retuenMsg);
            }
        }
    }


    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }



}