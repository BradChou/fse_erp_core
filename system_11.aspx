﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_11.aspx.cs" Inherits="system_11" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script src="js/jquery-1.11.2.min.js"></script>


    <script>
        //限制textbox的最大最小值

        function minmax(value, min, max) {
            value = value.replace(/[^\d]/g, '');
            if (value == "") {
                return value;
            }
            else {
                if (parseInt(value) < min || isNaN(parseInt(value)))
                    return min;
                else if (parseInt(value) > max)
                    return max;
                else return value;
            }
        }

    </script>


    <style type="text/css">
        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .td_th {
            text-align: center;
        }

        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h2 class="margin-bottom-10">
        <asp:Label ID="lbl_title" Text="貨號區間" runat="server"></asp:Label></h2>



    <asp:Panel ID="pan_view" runat="server">
        <div class="div_view">
            <!-- 查詢 -->
            <div class="form-group form-inline">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 70%;">
                            <label>查詢條件：</label>
                            <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control" ToolTip="所屬站別"></asp:DropDownList>
                            <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 客戶代號 或 名稱"></asp:TextBox>
                            <asp:TextBox ID="tb_searchNum" runat="server" class="form-control" placeholder="ex: 貨號"></asp:TextBox>
                            <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control" ToolTip="是否生效"></asp:DropDownList>
                            <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                            </td>
                        <td style="width: 30%; float: right;">
                            <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <!-- 查詢 end -->
            <!--內容-list -->

            <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr>
                    <th class="td_th td_no">No</th>
                    <th class="td_th">站別</th>
                    <th class="td_th td_name">客戶</th>
                    <th class="td_th td_no">號碼(起)</th>
                    <th class="td_th td_no">號碼(迄)</th>
                    <th class="td_th td_yn">目前取號</th>
                    <th class="td_th td_yn">是否生效</th>
                    <th class="td_th">更新人員</th>
                    <th class="td_th">更新日期</th>
                    <th class="td_th td_edit">功　　能</th>
                </tr>
                <asp:Repeater ID="list_customer" runat="server" OnItemCommand="list_customer_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                            <td class="td_data" data-th="站別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <td class="td_data td_name text-left  " data-th="客戶"><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString() + "　" + DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                            <td class="td_data td_no" data-th="區間號碼(起)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.begin_number").ToString())%></td>
                            <td class="td_data td_no" data-th="區間號碼(迄)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.end_number").ToString())%></td>
                            <td class="td_data td_no" data-th="目前取號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.current_number").ToString())%></td>
                            <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.IsActive").ToString())%></td>
                            <td class="td_data " data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cuser").ToString())%></td>
                            <td class="td_data " data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Cdate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            <td class="td_data td_edit" data-th="編輯">
                                <asp:HiddenField ID="emp_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>' />
                                <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" Visible ='<%# _Auth.insert_auth %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (list_customer.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="10" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>
            <!--內容-end -->
        </div>
    </asp:Panel>

    <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
        <hr />
        <table class="tb_edit">
            <tr class="form-group">
                <th class="_edit_title">
                    <label class="_tip_important">站別</label>
                </th>
                <td class="_edit_data form-inline ">
                    <asp:DropDownList ID="ddl_staion" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="所屬站別" OnSelectedIndexChanged="Customer_SelectedIndexChanged"></asp:DropDownList>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_customer_code" runat="server" ControlToValidate="ddl_staion" ForeColor="Red" ValidationGroup="validate">請選擇所屬站別</asp:RequiredFieldValidator>


                </td>

            </tr>

            <tr class="form-group">
                <th class="_edit_title">
                    <label class="_tip_important">客代</label>
                </th>
                <td class="_edit_data form-inline ">
                    <asp:DropDownList ID="ddl_Customer" runat="server" CssClass="form-control" ToolTip="所屬客代"></asp:DropDownList>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_Customer" ForeColor="Red" ValidationGroup="validate">請選擇所屬客代</asp:RequiredFieldValidator>


                </td>

            </tr>



            <tr class="form-group">
                <th class="_edit_title">
                    <label class="_tip_important">貨號區間</label>
                </th>
                <td class="_edit_data form-inline ">
                    <asp:TextBox ID="Bnumber" runat="server" class="form-control" placeholder="1~999999999999" onkeyup="this.value = minmax(this.value, 1, 999999999999)"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_Bnumber" runat="server" ControlToValidate="Bnumber" ForeColor="Red" ValidationGroup="validate">請填寫貨號區間(起)</asp:RequiredFieldValidator>
                    ～
                        <asp:TextBox ID="Enumber" runat="server" class="form-control" placeholder="1~999999999999" onkeyup="this.value = minmax(this.value, 1, 999999999999)"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_Enumber" runat="server" ControlToValidate="Enumber" ForeColor="Red" ValidationGroup="validate">請填寫貨號區間(迄)</asp:RequiredFieldValidator>
                </td>
            </tr>



            <tr>
                <th>
                    <label class="_tip_important">是否生效</label>
                </th>
                <td class="_edit_data form-inline">
                    <asp:RadioButtonList ID="rb_Active" runat="server" ToolTip="請設定是否生效!" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px">
                        <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                        <asp:ListItem Value="0">否</asp:ListItem>
                    </asp:RadioButtonList>
                </td>


            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                    <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>

</asp:Content>
