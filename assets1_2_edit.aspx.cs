﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_2_edit : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

            #region 車行
            SqlCommand cmd4 = new SqlCommand();
            cmd4.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'CD'";
            car_retailer.DataSource = dbAdapter.getDataTable(cmd4);
            car_retailer.DataValueField = "code_id";
            car_retailer.DataTextField = "code_name";
            car_retailer.DataBind();
            car_retailer.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 使用單位
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=1", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  code_id=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 {0} order by code_id", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                dept.DataSource = dt;
                dept.DataValueField = "code_id";
                dept.DataTextField = "code_name";
                dept.DataBind();
                dept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 使用單位
            //SqlCommand cmd6 = new SqlCommand();
            //cmd6.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'dept'";
            //dept.DataSource = dbAdapter.getDataTable(cmd6);
            //dept.DataValueField = "code_id";
            //dept.DataTextField = "code_name";
            //dept.DataBind();
            //dept.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion          

            readdata(a_id);

        }
    }

    private void readdata(string a_id)
    {  
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);
            
            cmd.CommandText = @"Select * from  ttAssets A with(nolock) 
                                Where a_id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                memo.Text = Func.GetRow("memo", "ttAssetsDeptChange", "order by id desc", "a_id = @a_id", "a_id", a_id);
                car_license.Text = dt.Rows[0]["car_license"].ToString();
                driver.Text = dt.Rows[0]["driver"].ToString();
                dept.SelectedValue = dt.Rows[0]["dept_id"].ToString();
                car_retailer.SelectedValue = dt.Rows[0]["car_retailer"].ToString();
                owner.Text = dt.Rows[0]["owner"].ToString();
                Hid_Dept.Value= dt.Rows[0]["dept"].ToString();
                Hid_ID.Value = Func.GetRow("id", "ttAssetsDeptChange", "order by id desc", "a_id = @a_id", "a_id", a_id);
                //udate.Text = dt.Rows[0]["udate"].ToString();
            }

        }
        else
        {
            ApStatus = "Add";
        }

        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                car_license.ReadOnly  = true ;
                car_retailer.Enabled = false;
                owner.ReadOnly = true;
                driver.ReadOnly = true;
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (string.IsNullOrEmpty(dept.SelectedValue))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('使用單位，請重新選擇');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Modity":
                using (SqlCommand cmdInsert = new SqlCommand())
                {
                    cmdInsert.Parameters.AddWithValue("@a_id", a_id);
                    cmdInsert.Parameters.AddWithValue("@car_license", car_license.Text.Trim());
                    cmdInsert.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                    cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                    cmdInsert.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                    cmdInsert.Parameters.AddWithValue("@udate", Convert.ToDateTime(udate.Text.Trim()).ToString("yyyy/MM/dd")); //DateTime.Now               //更新時間 
                    cmdInsert.Parameters.AddWithValue("@memo", memo.Text);
                    try
                    {
                        if (Hid_Dept.Value == dept.SelectedItem.Text)
                        {
                            cmdInsert.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", Hid_ID.Value);
                            cmdInsert.CommandText = dbAdapter.genUpdateComm("ttAssetsDeptChange", cmdInsert);
                            dbAdapter.execNonQuery(cmdInsert);
                        }
                        else
                        {
                            cmdInsert.Parameters.AddWithValue("@dept", Hid_Dept.Value);
                            cmdInsert.Parameters.AddWithValue("@NewDept", dept.SelectedItem.Text);
                            cmdInsert.CommandText = dbAdapter.SQLdosomething("ttAssetsDeptChange", cmdInsert, "insert");
                            dbAdapter.execNonQuery(cmdInsert);
                        }

                        using (SqlCommand cmdupd = new SqlCommand())
                        {
                            cmdupd.Parameters.AddWithValue("@dept_id", dept.SelectedValue);
                            cmdupd.Parameters.AddWithValue("@dept", dept.SelectedValue);//dept.SelectedItem.Text
                            cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "a_id", a_id);
                            cmdupd.CommandText = dbAdapter.genUpdateComm("ttAssets", cmdupd);
                            try
                            {
                                dbAdapter.execNonQuery(cmdupd);
                            }
                            catch (Exception ex)
                            {
                                ErrStr = ex.Message;
                            }
                        }
                            
                    }
                    catch (Exception ex)
                    {
                        ErrStr = ex.Message;
                    }
                }
                break;
        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='assets1_2.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }

    
}