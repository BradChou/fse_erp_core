﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_2_edit.aspx.cs" Inherits="assets1_2_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $(".date_pickerMinMax").datepicker({
                dateFormat: 'yy/mm/dd',
                //changeMonth: true,
                //changeYear: true,
                showOtherMonths : true,
                hideIfNoPrevNext : true,
                //minDate: "-7d",
                maxDate: "+1m",
                defaultDate: (new Date())  //預設當日
            });
        });

        function ValidateFloat2(e, pnumber) {
            if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
                var newValue = /\d+[.]?[1-9]?/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                }
            }
            return false;
        }

    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2 class="margin-bottom-10">使用單位主檔維護-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
    <div class="templatemo-login-form">
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">使用單位基本資料</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label>車牌</label>
                            <asp:TextBox ID="car_license" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入車牌</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="car_retailer">車行</label>
                            <asp:DropDownList ID="car_retailer" runat="server" class="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="owner">車主</label>
                            <asp:TextBox ID="owner" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="driver">司機</label>
                            <asp:TextBox ID="driver" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="dept"><span class="REDWD_b">*</span>使用單位</label>
                            <asp:DropDownList ID="dept" runat="server" class="form-control"></asp:DropDownList>
                            <asp:HiddenField ID="Hid_Dept" runat="server"/> 
                            <asp:HiddenField ID="Hid_ID" runat="server"/>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="dept" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇使用單位</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="udate"><span class="REDWD_b">*</span>生效日期</label>
                            <asp:TextBox ID="udate" runat="server" class="form-control date_pickerMinMax" MaxLength="10" placeholder="YYYY/MM/DD"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="udate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇生效日期</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="memo">備註</label>
                            <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        <div class="form-group text-center">
            <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
            <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/assets1_2.aspx">取消</asp:LinkButton>
            <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
        </div>

    </div>

</asp:Content>

