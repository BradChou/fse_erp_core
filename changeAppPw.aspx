﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="changeAppPw.aspx.cs" Inherits="changeAppPw" %>

<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>峻富雲端物流管理-APP密碼重置</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
     <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">APP密碼重置</h4>
                    </div>
                    <div class="modal-body">
                        <div class=" row form-group  form-inline">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label class="_tip_important">新 密 碼：</label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <asp:TextBox ID="account_password_new" runat="server" CssClass="form-control" MaxLength="4"  placeholder="請輸入新密碼" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class ="modal-footer " >
                        <div class="text-center" >
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary "  Text="確定"  OnClientClick="return confirm('您確認要重置該司機的APP密碼嗎?');" OnClick="btnSave_Click"  /> 
                            <asp:Button ID="btncancel" runat="server" CssClass="btn btn-default " Text="取消" OnClientClick="parent.$.fancybox.close();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
