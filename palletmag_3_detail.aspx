﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="palletmag_3_detail.aspx.cs" Inherits="palletmag_3_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .tb_title, ._d_function {
            text-align: center;
        }

        .tb_detail {
            text-align: left;
        }

        .btn {
            margin: 3px;
        }

        ._h_function {
            width: 10%;
        }

        ._h_addr {
            width: 30%;
        }

        ._h_auto {
            min-width: 5px;
        }

        .div_sh, ._top_left {
            width: 70%;
            float: left;
        }

        .div_btn {
            width: 30%;
            float: right;
            right: 3%;
        }

        .tb_top {
            width: 100%;
        }

        ._top_right {           
            float: right;
            right: 5px;
        }

        ._hr {
            display: inline-table;
        }

        .detail_btn_area {
            text-align: center;
        }
        ._detail{
            width:68%;
            text-align:left;
            margin:10px;
           
        }
        ._detail_title{
            width:10%;   
        }
        ._detail_data{
            width:40%;    
            padding:3px !important;
        }
        ._ext{width:70px !important;}
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
         .table_list tr:hover {
            background-color: lightyellow;
        }
         textarea{
             margin:5px 0px;
         }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 20;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
        function Receiver_dw() {
            var rec_cuscode = $(".rec_cuscode").val();
            var rec_code = $(".rec_code").val();
            var rec_name = $(".rec_name").val();
            var the_dw = $("input[name=dw");
            var url_dw = "";
            url_dw = "GetReport.aspx?type=receiver&rec_code=" + rec_code + "&rec_name=" + rec_name + "&rec_cuscode=" + rec_cuscode;

            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            setTimeout("parent.$.fancybox.close()", 2000);
        }


    </script>
    <style>
        th {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>明細</h2>
            <asp:Panel ID="pan_list" runat="server">
                <div class="col-lg-12 form-inline form-group">
                    <label>期　間：<asp:Label ID="lbsdate" runat="server" ></asp:Label></label>
                </div>
                <div class="col-lg-12 form-inline form-group">
                    <label>客　代：<asp:Label ID="lbcustomer" runat="server" ></asp:Label></label>
                </div>
                <div class="col-lg-12 form-inline form-group">
                    <label>區配商：<asp:Label ID="lbsupplier" runat="server" ></asp:Label></label>                    
                </div>
                <hr />
                <table id="custom_table" class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr class="tr-only-hide">
                        <th class="_th">No.</th>
                        <th class="_th">貨　　號</th>
                        <th class="_th">配送日</th>
                        <th class="_th">發送站</th>
                        <th class="_th">寄件人</th>
                        <th class="_th">客戶代碼</th>
                        <th class="_th">收件人</th>
                        <th class="_th">地　　址</th>
                        <th class="_th">到著碼</th>
                        <th class="_th">建檔人員</th>
                        <th class="_th">板數</th>
                       <%-- <th class="_th">功　　能</th>--%>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">
                                <td class="td_no" data-th="No"><%# Container.ItemIndex + 1 %></td>
                                <td class="td_ck_number " data-th="貨號">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>' />
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) %>
                                </td>
                                <td  data-th="配送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                                <td  data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation2").ToString())%></td>
                                <td  data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%> </td>
                                <td  data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                <td  data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%> </td>
                                <td  data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address").ToString())%></td>
                                <td  data-th="到著碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%></td>
                                <td  data-th="建檔人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td  data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                <%--<td class="td_btn" _the_id='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>'>
                                    
                                </td>--%>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="10" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </asp:Panel>

        </div>
    </div>
</asp:Content>

