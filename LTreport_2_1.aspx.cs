﻿using BarcodeLib;
using BObject.Bobjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTreport_2_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            //tb_dateStart.Text = DateTime.Now.AddDays(-15).ToString("yyyy/MM/dd");
            //tb_dateEnd.Text = DateTime.Now.ToString("yyy/MM/dd");
            SetDefault();
            rb1.Checked = true;
            rb2.Checked = false;
            rb_CheckedChanged(null, null);

            //btn_Search_Click(btn_Search, new EventArgs());
            if (Request.QueryString["dateS"] != null) {
                SetViewData();
            }
        }
    }

    /// <summary>始初資料綁定 </summary>
    protected void SetDefault()
    {
        string strSQL = string.Empty;
     
        
            //區配商

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                        else
                        {

                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";
        
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

        ddl_supplyer.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
        ddl_supplyer.DataValueField = "station_code";
        ddl_supplyer.DataTextField = "showname";
        ddl_supplyer.DataBind();
            if (manager_type == "0" || manager_type == "1")
            {
            ddl_supplyer.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && manager_type == "2")
            {
            ddl_supplyer.Items.Insert(0, new ListItem("全部", ""));
            }
            if (ddl_supplyer.Items.Count > 0) ddl_supplyer.SelectedIndex = 0;
    }

    protected void SetViewData()
    {
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();

        #region Set QueryString

        if (Request.QueryString["supplyer"] != null
            && Request.QueryString["dateS"] != null
            && Request.QueryString["dateE"] != null)
        {
            ddl_supplyer.SelectedValue = Request.QueryString["supplyer"];

            if (DateTime.TryParse(Request.QueryString["dateS"], out dt_s)
                && DateTime.TryParse(Request.QueryString["dateE"], out dt_e)
                && dt_e >= dt_s)
            {
                //tb_dateStart.Text = dt_s.ToString("yyy/MM/dd");
                //tb_dateEnd.Text = dt_e.ToString("yyy/MM/dd");
                if (Request.QueryString["dttype"] == "1")
                {
                    rb1.Checked = true;
                    rb2.Checked = false;
                    rb_CheckedChanged(null, null);
                }
                else if (Request.QueryString["dttype"] == "2")
                {
                    rb1.Checked = false;
                    rb2.Checked = true;
                    rb_CheckedChanged(null, null);
                }
            }
        }
        else
        {
            rb1.Checked = true;
            rb_CheckedChanged(null, null);
            tb_dateStart.Text = DateTime.Now.AddDays(-15).ToString("yyyy/MM/dd");
            tb_dateEnd.Text = DateTime.Now.ToString("yyy/MM/dd");
        }

        if (Request.QueryString["keyword"] != null)
        {
            tb_search.Text = Request.QueryString["keyword"].ToString();
        }

        if (Request.QueryString["strCheck_number"] != null)
        {
            strCheck_number.Text = Request.QueryString["strCheck_number"].ToString();
        }
        #endregion

        //if (ddl_supplyer.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送廠商!');</script>", false);
        //    return;
        //}

        if (rb1.Checked)
        {
            if (!DateTime.TryParse(tb_dateStart.Text, out dt_s) || !DateTime.TryParse(tb_dateEnd.Text, out dt_e))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
        }
        else {
            if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
        }

        //String str_Query = "&supplyer=" + ddl_supplyer.SelectedValue
        //                 + "&dateS=" + tb_dateStart.Text
        //                 + "&dateE=" + tb_dateEnd.Text
        //                 + "&keyword=" + tb_search.Text
        //                 + "&strCheck_number=" + strCheck_number.Text;

        ListData _search = new ListData();
        _search.type = 1;
        _search.arrive = ddl_supplyer.SelectedValue;
        _search.date_start = dt_s;
        _search.date_end = dt_e;
        _search.keyword = tb_search.Text;
        _search.strCheck_number = strCheck_number.Text;
        _search.rbpricingtype = "02";
        if (tb_search.Text != "")
        {
            _search.keyword = tb_search.Text;
        }
        else {
            _search.keyword = "";
        }


        if (rb1.Checked)
        {
            _search.dttype = "1";
        }
        if (rb2.Checked)
        {
            _search.dttype = "2";
        }

        using (DataTable dt = GetDataTableForList(_search))
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                //DataRow row = dt.NewRow();
                //row["send_contact"] = string.Format(@"筆數:{0}", dt.Rows.Count);
                rowcount.Text = string.Format(@"筆數:{0}", dt.Rows.Count);
                rowcount.Visible = true;
                //dt.Rows.Add(row);
            }
           
            New_List.DataSource = dt;
            New_List.DataBind();
        }

    }


    #region @btn
    protected void btn_Search_Click(object sender, EventArgs e)
    {
        String str_Query = "supplyer=" + ddl_supplyer.SelectedValue
                         + "&keyword=" + tb_search.Text
                         + "&strCheck_number=" + strCheck_number.Text;
        if (rb1.Checked)
        {
            str_Query += "&dateS=" + tb_dateStart.Text + "&dateE=" + tb_dateEnd.Text;
            str_Query+= "&dttype=" +"1";
        }else
        {
            str_Query += "&dateS=" + sscan_date.Text + "&dateE=" + escan_date.Text;
            str_Query += "&dttype=" + "2";
        }


        Response.Redirect(ResolveUrl("~/LTreport_2_1.aspx?" + str_Query));
    }

    protected void btn_Print_Click(object sender, EventArgs e)
    {
        if (tb_ckeck.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇欲列印的項目!');</script>", false);
            return;
        }

        //if (ddl_supplyer.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送廠商!');</script>", false);
        //    return;
        //}
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();
        if (rb1.Checked) {
            if (!DateTime.TryParse(tb_dateStart.Text, out dt_s) || !DateTime.TryParse(tb_dateEnd.Text, out dt_e))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
        }
        else {
            if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
        }

        string resule = tb_ckeck.Text;

        ListData _search = new ListData();
        _search.type = 2;
        _search.arrive = ddl_supplyer.SelectedValue;
        _search.date_start = dt_s;
        _search.date_end = dt_e;
        _search.strCheck_number = strCheck_number.Text;
        _search.rbpricingtype = "02";
        if (tb_search.Text != "")
        {
            _search.keyword = tb_search.Text;
        }
        else {
            _search.keyword = "";
        }
        if (rb1.Checked)
        {
            _search.dttype = "1";
        }
        if (rb2.Checked)
        {
            _search.dttype = "2";
        }


        if (tb_ckeck.Text.Trim() == "0") _search.check_all = true;
        else _search.check_item = tb_ckeck.Text.Trim();

        using (DataTable dt = GetDataTableForList(_search))
        {
            try
            {
                #region way2 

                //範本檔路徑
                String strSampleFileFullName = Request.PhysicalApplicationPath + @"report\零擔簽收單.xls"; ;
                if (strSampleFileFullName.EndsWith(".xls") == false) strSampleFileFullName += ".xls";

                #region new
                #region 初始化參數
                NPOITable ttExlApp = new NPOITable(strSampleFileFullName);
                ttExlApp.OpenFile();
                ttExlApp.SRow = 1;
                ttExlApp.SCol = 1;
                ttExlApp.Line = 45;
                ttExlApp.Column = 24;
                ttExlApp.NextPage();
                ttExlApp.GoToLine(1);

                DateTime dt_tmp = new DateTime();
                string print_dateYY = "";
                string print_date_mm = "";
                string print_date_DD = "";
                string check_number = "";
                string check_number_detail = "";
                string send_contact = "";
                string send_tel = "";


                string order_number = "";
                string ArticleName = "";

                string send_addr = "";

                //發送站
                string on_station_code = "";
                //到著站
                string off_station_code = "";

                string receive_contact = "";
                string receive_tel1 = "";
                string receive_addr = "";
                string sendsite = "";
                string supplier_name = "";
                string ticket = "";
                string pricing_type = "";
                string str_unit = "";
                int pieces = 0;//件
                int plates = 0;//板
                int cbm = 0;//才
                int addcolumn = 0;
                bool receipt_flag = false;            // 是否回單
                bool pallet_recycling_flag = false;   // 是否棧板回收
                bool turn_board = false;              // 翻板
                bool upstairs = false;                // 上樓
                bool difficult_delivery = false;      // 困配
                string invoice_desc = "";             // 備註
                string arrive = "";                   // 到著
                string arrive_assign_date = "";       // 指配日期
                int collection_money = 0;
                int total_fee = 0;
                int arrive_to_pay_freight = 0;
                string subpoena_category = "";
                string holiday_delivery = ""; //是否假日配


                #endregion

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    #region 取得資料
                    if (DateTime.TryParse(dt.Rows[i]["print_date"].ToString(), out dt_tmp))
                    {
                        print_dateYY = dt_tmp.Year.ToString();
                        print_date_mm = dt_tmp.Month.ToString();
                        print_date_DD = dt_tmp.Day.ToString();
                    }
                    else
                    {
                        print_dateYY = "";
                        print_date_mm = "";
                        print_date_DD = "";
                    }

                    arrive_assign_date = dt.Rows[i]["arrive_assign_date"].ToString();
                    ArticleName = dt.Rows[i]["ArticleName"].ToString();


                    order_number = dt.Rows[i]["order_number"].ToString();

                    send_contact = dt.Rows[i]["send_contact"].ToString();
                    send_tel = dt.Rows[i]["send_tel"].ToString();
                    send_addr = dt.Rows[i]["send_addr"].ToString();
                    on_station_code = dt.Rows[i]["sta_name"].ToString();
                    off_station_code = dt.Rows[i]["eta_name"].ToString();

                    receive_contact = dt.Rows[i]["receive_contact"].ToString();
                    receive_tel1 = dt.Rows[i]["receive_tel1"].ToString();
                    receive_addr = dt.Rows[i]["receive_addr"].ToString();
                    sendsite = dt.Rows[i]["sendsite"].ToString();
                    supplier_name = dt.Rows[i]["supplier_name"].ToString();
                    pricing_type = dt.Rows[i]["pricing_type"].ToString();
                    check_number = dt.Rows[i]["check_number"].ToString().Trim();
                    ticket = dt.Rows[i]["ticket"].ToString();
                    if (!int.TryParse(dt.Rows[i]["ship_fee"].ToString(), out total_fee)) total_fee = 0;
                    if (!int.TryParse(dt.Rows[i]["arrive_to_pay_freight"].ToString(), out arrive_to_pay_freight)) arrive_to_pay_freight = 0;
                    if (!int.TryParse(dt.Rows[i]["collection_money"].ToString(), out collection_money)) collection_money = 0;
                    if (!int.TryParse(dt.Rows[i]["pieces"].ToString(), out pieces)) pieces = 0;
                    if (!int.TryParse(dt.Rows[i]["plates"].ToString(), out plates)) plates = 0;
                    if (!int.TryParse(dt.Rows[i]["cbm"].ToString(), out cbm)) cbm = 0;
                    receipt_flag = dt.Rows[i]["receipt_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["receipt_flag"]) : false;
                    pallet_recycling_flag = dt.Rows[i]["pallet_recycling_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["pallet_recycling_flag"]) : false;
                    turn_board = dt.Rows[i]["turn_board"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["turn_board"]) : false;
                    upstairs = dt.Rows[i]["upstairs"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["upstairs"]) : false;
                    difficult_delivery = dt.Rows[i]["difficult_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["difficult_delivery"]) : false;
                    invoice_desc = dt.Rows[i]["invoice_desc"].ToString().Trim();
                    arrive = dt.Rows[i]["arrive"].ToString().Trim();
                    subpoena_category = dt.Rows[i]["subpoena_category"].ToString().Trim();
                    holiday_delivery = dt.Rows[i]["holiday_delivery"].ToString();

                    check_number_detail = check_number;
                    if (check_number.Length == 10)
                    {
                        check_number_detail = string.Format(@"{0}-{1}-{2}"
                            , check_number.Substring(0, 3)
                            , check_number.Substring(3, 3)
                            , check_number.Substring(6, 4));
                    }
                    #endregion


                    #region second way

                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, print_dateYY, 0);
                    ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date_mm, 0);
                    ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, print_date_DD, 0);
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_tel1, 0);

                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 0);
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_contact, 0);

                    ttExlApp.RowC += 1;//換行
                    string spec = "";
                    if (receipt_flag == true) spec += "回單/";
                    if (pallet_recycling_flag == true) spec += "棧板回收/";
                    if (turn_board == true) spec += "翻板/";
                    if (upstairs == true) spec += "上樓/";
                    if (difficult_delivery == true) spec += "困配/";
                    if (spec != "") spec = spec.Substring(0, spec.Length - 1);

                  
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_addr + " " + spec + " " + invoice_desc, 0);

                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 0);
                    //ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, sendsite, 0);
                    ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, on_station_code, 0);

                    ttExlApp.RowC += 2;//換行
                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_addr , 0);
                    //ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, arrive, 0);
                    ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, off_station_code, 0);

                    ttExlApp.RowC += 3;//換行
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, holiday_delivery + arrive_assign_date, 0);
                    str_unit = (pieces > 0 ? pieces.ToString() : "") + "件  /  ";
                    if (pricing_type == "01")
                    {
                        str_unit += (plates > 0 ? plates.ToString() : "") + "大板";
                    }
                    else if (pricing_type == "03")
                    {
                        str_unit += (cbm > 0 ? cbm.ToString() : "") + "才";
                    }
                    else if (pricing_type == "04")
                    {
                        str_unit += (plates > 0 ? plates.ToString() : "") + "小板";
                    }

                    //if (pricing_type != "03")
                    //{
                    //    str_unit += (plates > 0 ? plates.ToString() : "") + "板";
                    //}
                    //else
                    //{
                    //    str_unit += (cbm > 0 ? cbm.ToString() : "") + "才";
                    //}

                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, order_number, 0);
                    ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, str_unit, 0);
                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ArticleName, 0);
                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ticket, 0);
                    switch (subpoena_category)
                    {
                        case "21":
                            if (arrive_to_pay_freight > 0)
                                ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight, 0);
                            else if (total_fee > 0)
                                ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, total_fee, 0);
                            break;
                        default:
                            if (collection_money > 0) ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, collection_money, 0);
                            break;
                    }



                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, check_number_detail, 0);

                    ttExlApp.RowC += 1;//換行
                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, "*" + check_number + "*", 0);
                    ttExlApp.RowC += 1;//換行
                    switch (i % 2)
                    {
                        case 0:
                            addcolumn = 12;
                            ttExlApp.RowC -= 13;

                            break;
                        case 1:
                            addcolumn = 0;
                            if ((i + 1) % 6 == 0 && i != dt.Rows.Count - 1)
                            {
                                ttExlApp.NextPage();
                            }
                            else
                            {
                                ttExlApp.RowC += 2;
                            }
                            break;
                    }

                    #endregion


                }
                #endregion
                ttExlApp.DeleteTable(1);

                FileInfo fi = new FileInfo(strSampleFileFullName);
                string FileName = String.Empty;


                //Server→轉Excel
                //Boolean IsTest = false; //是否為測試模試
                //FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);
                //FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                //string ExportPath = "files/" + FileName;
                //ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);
                //string sScript = "var myWindow=window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');" +                                
                //                " myWindow.print();";

                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');" + sScript + "</script>", false);

                //if (Request.Url.Host.IndexOf("localhost") >= 0)
                //{
                //    //本機→轉PDF
                //    FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                //    string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                //    string pathDownload = Path.Combine(pathUser, "Downloads", FileName);
                //    ttExlApp.Save(pathDownload, TableFormat.TFPDF);
                //    BasePage.popDownload(Path.Combine(pathUser, "Downloads"), FileName, true);

                //    //FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".xls");
                //    //string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                //    //string pathDownload = Path.Combine(pathUser, "Downloads", FileName);
                //    //ttExlApp.Save(pathDownload, TableFormat.TFExcel);
                //    //BasePage.popDownload(Path.Combine(pathUser, "Downloads"), FileName, true);
                //}
                //else
                //{
                    //Server→轉Excel
                    Boolean IsTest = false; //是否為測試模試
                    //FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);
                    FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                    string ExportPath = "files/" + FileName;
                    ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);

                    if (!IsTest)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);

                    }
                //}


                #endregion

                //接下來測HTML + PDF

            }
            catch (Exception ex)
            {

                //組錯誤訊息
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "簽收單列印-" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                //導至錯誤頁面
                String _theUrl = string.Format("Oops.aspx?err_title={0}&err_msg={1}", "系統發生錯誤!", "系統資訊異常，請通知系統管理人員。");
                Server.Transfer(_theUrl, true);

            }
        }



    }
    #endregion


    /// <summary>依類型取得標籤列印DataTable</summary>
    /// <param name="search"></param>
    /// <returns></returns>
    protected DataTable GetDataTableForList(ListData search)
    {
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = "";
                string wherestr = "";

                string wherestr1 = "";
                //if (rb_pricing_type.SelectedValue == "專車")
                //{
                //    cmd.Parameters.AddWithValue("@pricing_type", "05");
                //    wherestr += " and dr.pricing_type = @pricing_type";
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("@pricing_type", "05");
                //    wherestr += " and dr.pricing_type <> @pricing_type";
                //}

                cmd.Parameters.AddWithValue("@pricing_type", "02");
                wherestr += " and dr.pricing_type  = @pricing_type";

                cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);


                if (rb1.Checked)
                {
                    wherestr1 += " CONVERT(CHAR(10),dr.print_date,111) ";
                }
                else 
                {
                    wherestr1 += " CONVERT(CHAR(10),_arr.newest_scandate,111) ";
                }

                switch (search.type)
                {
                    case 2:
                        #region 列印標籤資訊
                        strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS rowid
                                          ,CONVERT(CHAR(10), dr.print_date,111) print_date,CONVERT(CHAR(10), dr.arrive_assign_date,111) arrive_assign_date,dr.check_number
										 ,CASE WHEN cus.supplier_code IN('001','002') THEN REPLACE(REPLACE(cus.customer_shortname,'站',''),'HCT','') ELSE dr.supplier_name  END sendsite 
                                         ,CASE WHEN dr.customer_code in ('F1300600002 ','F2900210002 ','F2900720002') THEN tb.station_name  ELSE sta1.station_name END sta_name --'發送站'
                                         ,eta1.station_name eta_name--'到著站'
                                         ,dr.send_contact
										 ,dr.send_tel
                                         ,dr.receive_contact
										 ,CASE WHEN (Len(dr.receive_tel1)>5) THEN SUBSTRING( dr.receive_tel1,0,6 )+'*9'+ SUBSTRING(dr.receive_tel1,6,Len(dr.receive_tel1)) 
                                               END receive_tel1
                                         ,dr.receive_city,dr.receive_area
                                         ,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
                                         ,ISNULL(dr.send_city,'') + ISNULL(dr.send_area,'') + ISNULL(dr.send_address,'') send_addr
                                         ,ISNULL(dr.receive_city,'') + ISNULL(dr.receive_area,'') + ISNULL(dr.receive_address,'') receive_addr
                                         ,sta1.station_scode,
                                         --sup.supplier_name,
                                         _special.code_name
                                         ,CONVERT(CHAR(10),dr.supplier_date,111) supplier_date
                                         ,CASE dr.receipt_flag WHEN 1 THEN 1 ELSE 0 END receipt_flag 
                                         ,_fee.total_fee ship_fee --'貨件運費'
                                         ,dr.arrive_to_pay_freight  --'到付運費'
                                         ,dr.customer_code
                                         ,dr.uniform_numbers
                                         ,NULL account_type --'月結／現收'
                                         ,_ticket.code_name ticket -- '傳票區分'
                                         ,dr.collection_money
                                         ,NULL account_date --'入帳日'
                                         ,_arr.arrive_state --'配達狀況
                                         ,_arr.arrive_date  --'配達時間
                                         ,_arr.arrive_item  arrive_item -- '配送異常說明
                                         ,CASE _arr.arrive_scan WHEN 1 THEN '是' ELSE '否' END arrive_scan --是否已掃瞄
                                         ,'','',''
                                         ,_fee.cscetion_fee -- 配送費用'    
                                         ,dr.receipt_flag
                                         ,dr.pallet_recycling_flag
                                         ,dr.turn_board
                                         ,dr.upstairs
                                         ,dr.difficult_delivery
                                         ,dr.invoice_desc
                                         ,ISNULL(arr.station_Name,'') as arrive
                                         ,dr.subpoena_category
                                         ,case when (cus.is_weekend_delivered = 1 and dr.holiday_delivery = 1) then '假日配'
                                          else ''
                                          end as holiday_delivery
                                         ,dr.order_number
					                     ,dr.ArticleName
                                         
                                    FROM tcDeliveryRequests dr With(Nolock)
                                    --LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.customer_code = cus.customer_code 
                                    LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
									LEFT JOIN ttArriveSitesScattered sta With(nolock) on dr.send_city = sta.post_city and dr.send_area = sta.post_area
									LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
								    LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
									LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode  
                                    LEFT JOIN pickup_request_for_apiuser tt with(nolock) on dr.check_number = tt.check_number
                                    LEFT JOIN tbStation tb with(nolock) on tb.station_code = tt.supplier_code
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr 
                                    left join tbDrivers D on D.driver_code = _arr.newest_driver_code
                                    WHERE  {1} BETWEEN @dateS AND @dateE 
                                      AND (dr.area_arrive_code = Replace (@supplier, 'F', '') OR @supplier ='')
                                     AND dr.supplier_date IS NOT NULL
                                     AND dr.cancel_date IS NULL
                                     AND dr.latest_arrive_option IS NULL
                                     AND (@keyword ='' or _arr.newest_driver_code = @keyword or  D.driver_name = @keyword)
                                     AND (@strCheck_number ='' or dr.Check_number = @strCheck_number )
                                      AND dr.Less_than_truckload =@Less_than_truckload
                                     and dr.customer_code <> 'F3500010002'
                                     and _arr.arrive_state !='配達'
                                     and (_arr.arrive_date ='' or _arr.arrive_date is null)
                                      {0} ";

                        if (!search.check_all && search.check_item.Split(',').Length > 0)
                        {
                            strSQL += " AND dr.request_id IN (" + search.check_item + ") ";
                        }



                        #endregion

                        break;
                    default:
                        #region 網頁list顯示

                        strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS rowid
                                         ,CONVERT(CHAR(10), dr.print_date,111) print_date,dr.check_number
                                         ,CASE WHEN dr.customer_code in ('F1300600002 ','F2900210002 ','F2900720002 ') THEN tb.station_name  ELSE sta1.station_name END sta_name --'發送站'
                                         ,eta1.station_name eta_name--'到著站'
                                         ,send_contact
                                         ,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
                                         --,ISNULL(dr.supplier_code,'') +' '+ISNULL(sup.supplier_name,'') 'supplier' 
                                         ,ISNULL(dr.area_arrive_code,'') +' '+ISNULL(arr.station_name,'') 'arr'  
                                         ,CONVERT(CHAR(10),_arr.send_date,111) supplier_date     --配送日                                    
                                         ,_fee.total_fee ship_fee --'貨件運費'
                                         ,dr.arrive_to_pay_freight  --'到付運費'
                                         ,dr.collection_money
                                         ,_arr.arrive_state --配達狀況 
                                         ,_arr.arrive_date  --配達時間 
                                         ,_arr.arrive_item  arrive_item -- 配送異常說明 
                                         ,_fee.cscetion_fee -- 配送費用 
                                         ,CASE WHEN (Len(dr.receive_contact)>2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'+ SUBSTRING(dr.receive_contact,3,Len(dr.receive_contact)) 
                                               WHEN (Len(dr.receive_contact)=2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'
                                          ELSE dr.receive_contact END 'receive_contact',dr.request_id  
                                    FROM tcDeliveryRequests dr With(Nolock)
                                   --LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.customer_code = cus.customer_code AND cus.supplier_code IN('001','002')
                                    LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
									--LEFT JOIN ttArriveSitesScattered sta With(nolock) on dr.send_city = sta.post_city and dr.send_area = sta.post_area
									LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
								    LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
									LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode
                                    LEFT JOIN pickup_request_for_apiuser tt with(nolock) on dr.check_number = tt.check_number
                                    LEFT JOIN tbStation tb with(nolock) on tb.station_code = tt.supplier_code
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
                                    left join tbDrivers D on D.driver_code = _arr.newest_driver_code
                                    WHERE   {1} BETWEEN @dateS AND @dateE 
                                     AND (dr.area_arrive_code = Replace (@supplier, 'F', '') OR @supplier ='')
                                     AND dr.supplier_date IS NOT NULL
                                     AND dr.cancel_date IS NULL
                                     AND dr.latest_arrive_option IS NULL
                                     AND (@keyword ='' or _arr.newest_driver_code = @keyword or  D.driver_name = @keyword)
                                     AND (@strCheck_number ='' or dr.Check_number = @strCheck_number )
                                      AND dr.Less_than_truckload =@Less_than_truckload
                                     and dr.customer_code <> 'F3500010002'
                                     and _arr.arrive_state !='配達'
                                     and (_arr.arrive_date ='' or _arr.arrive_date is null)
                                      {0} ";

                        #endregion
                        break;
                }

                strSQL += " ORDER BY dr.supplier_date ;";
                cmd.CommandText = string.Format(strSQL, wherestr, wherestr1);
                cmd.Parameters.AddWithValue("@supplier", search.arrive);
                cmd.Parameters.AddWithValue("@dateS", search.date_start.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", search.date_end.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@keyword", search.keyword);
                cmd.Parameters.AddWithValue("@strCheck_number", search.strCheck_number);
                cmd.CommandTimeout = 600;
                dt = dbReadOnlyAdapter.getDataTable(cmd);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
        }
        return dt;
    }

    protected void tbsearch_TextChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        using (SqlCommand cmd = new SqlCommand()) {
            cmd.Parameters.AddWithValue("@driver_code", tb_search.Text.ToUpper());
            cmd.CommandText = @"
  select ts.station_code 
  from [tbDrivers] td With(Nolock) 
  LEFT JOIN tbStation ts With(Nolock) on td.station = ts.station_scode
  where td.driver_code = @driver_code ";
            dt = dbReadOnlyAdapter.getDataTable(cmd);
        }
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["station_code"].ToString() != "")
            {
                ddl_supplyer.SelectedValue = dt.Rows[0]["station_code"].ToString();
            }
            else
            {
                ddl_supplyer.SelectedIndex = 0;
            }
        }
        else {
            ddl_supplyer.SelectedIndex = 0;
        }
    }
    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void rb_CheckedChanged(Object sender, EventArgs e)
    {
        string Distribute_sdate = DateTime.Now.AddDays(-15).ToString("yyyy/MM/dd");
        string Distribute_edate = DateTime.Now.ToString("yyy/MM/dd");
        if (rb1.Checked)
        {
            tb_dateStart.Enabled = true;
            tb_dateEnd.Enabled = true;
            sscan_date.Enabled = false;
            escan_date.Enabled = false;
            tb_dateStart.Text = Distribute_sdate;
            tb_dateEnd.Text = Distribute_edate;
            sscan_date.Text = "";
            escan_date.Text = "";
            rb2.Checked = false;
        }
        else
        {
            tb_dateStart.Enabled = false;
            tb_dateEnd.Enabled = false;
            sscan_date.Enabled = true;
            escan_date.Enabled = true;
            tb_dateStart.Text = "";
            tb_dateEnd.Text = "";
            sscan_date.Text = Distribute_sdate; 
            escan_date.Text = Distribute_edate;
            rb1.Checked = false;
        }

    }

    public class ListData
    {
        /// <summary>1:網頁list顯示 2.列印標籤資訊</summary>
        public int type { get; set; }
        /// <summary>區配商 </summary>
        public string supplyer { get; set; }

        /// <summary>配送廠商 </summary>
        public string arrive { get; set; }

        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        /// <summary>論板專車 </summary>
        public string rbpricingtype { get; set; }
        /// <summary>貨號、寄件人、收貨人(where條件)</summary>
        public String keyword { get; set; }

        /// <summary>單筆託運單號</summary>
        public String strCheck_number { get; set; }

        /// <summary>自選項目(ex:5,6,8)</summary>
        public String check_item { get; set; }
        /// <summary>全選 </summary>
        public Boolean check_all { get; set; }

        /// <summary></summary>
        public String dttype { get; set; }
        public ListData()
        {
            type = 1;
            check_item = "";
            check_all = false;
            keyword = "0";
        }

    }

}