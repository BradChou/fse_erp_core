﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_3_2.aspx.cs" Inherits="system_3_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">任務分派司機設定</h2>
            <div class="templatemo-login-form" method="post" enctype="multipart/form-data">
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">司機</label>
                        <select class="form-control">
                            <option>b2006陳鴻仁</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">歸屬站所</label>
                        <select class="form-control">
                            <option>百通(E-1)</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group form-inline">
                    <div class="col-lg-12 form-group form-inline">
                        <label for="inputLastName">區域</label>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <input type="checkbox" name="server" id="c3" value="">
                                <label for="c3" class="font-weight-400"><span></span>台南市麻豆區</label>
                                <input type="checkbox" name="member" id="c4" value="" checked>
                                <label for="c4" class="font-weight-400"><span></span>台南市永康區</label>
                                <input type="checkbox" name="expired" id="c5" value="">
                                <label for="c5" class="font-weight-400"><span></span>台南市佳里區</label>
                                <input type="checkbox" name="server" id="c6" value="" checked>
                                <label for="c6" class="font-weight-400"><span></span>台南市歸仁區</label>
                                <input type="checkbox" name="member" id="c7" value="">
                                <label for="c7" class="font-weight-400"><span></span>台南市善化區</label>
                                <input type="checkbox" name="expired" id="c8" value="" checked>
                                <label for="c8" class="font-weight-400"><span></span>台南市新化區</label>
                                <input type="checkbox" name="server" id="c9" value="">
                                <label for="c9" class="font-weight-400"><span></span>台南市學甲區</label>
                                <input type="checkbox" name="member" id="c10" value="">
                                <label for="c10" class="font-weight-400"><span></span>台南市關廟區</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="templatemo-blue-button">設定</button>
                    <button type="submit" class="btn btn-darkblue">確認</button>
                </div>
                <hr>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <thead>
                        <tr>
                            <td>員工代號</td>
                            <td>司機</td>
                            <td>歸屬站所</td>
                            <td>責任區域
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>b2006</td>
                            <td>陳鴻仁</td>
                            <td>E-1百通</td>
                            <td>台南市永康區/台南市歸仁區/台南市新化區</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

