﻿<%@ Page Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreportVolume.aspx.cs" Inherits="LTreportVolume" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {


            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                            .slice(start, end).show();
                    }
                });
            });
        });

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align: left;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align: left;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">材積丈量結果報表</h2>
            <hr />
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>查詢日期區間：</label>
                    <asp:TextBox ID="date1" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    ~
                    <asp:TextBox ID="date2" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    <label>發送站所：</label><asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                    <label>託運單號：</label><asp:TextBox ID="check_number" runat="server" class="form-control"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                    <asp:Button ID="btnQry" CssClass="btn btn-primary btn_view" runat="server" Text="查　詢" OnClick="btnQry_Click" />
                    <asp:Button ID="btnExcel" CssClass="btn btn-warning" runat="server" Text="匯　出" OnClick="btnExcel_Click" />
                </div>
                <div class="img-box" style="display: none;">
                    <asp:Panel ID="myPanel" runat="server">
                    </asp:Panel>
                </div>
                <div id="LoadBox" style="display: none"></div>
                <hr>
            </div>
            <div style="overflow: auto; height: 550px; width: 100%">
                <table id="mt" class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th style="white-space: nowrap;">序號</th>
                        <th style="white-space: nowrap;">貨號</th>
                        <th style="white-space: nowrap;">長(mm)</th>
                        <th style="white-space: nowrap;">寬(mm)</th>
                        <th style="white-space: nowrap;">高(mm)</th>
                        <th style="white-space: nowrap;">才積數</th>
                        <th style="white-space: nowrap;">重量換算</th>
                        <th style="white-space: nowrap;">系統輸入重量</th>
                        <th style="white-space: nowrap;">差異(重量換算-系統重量)</th>
                        <th style="white-space: nowrap;">是否修正</th>
                        <th style="white-space: nowrap;">功能</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr class="paginate">
                                <td data-th="序號"><%# Container.ItemIndex +1 %></td>
                                <td style="white-space: nowrap;" data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="長"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbmLength").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="寬"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbmWidth").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="高"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbmHeight").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="才積數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbmCont").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="重量換算"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbmWeight").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="系統輸入重量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Weight").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="差異"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.diff").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="是否修正"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.editflag").ToString())%></td>
                                <td style="white-space: nowrap;" data-th="功能">
                                    <asp:HiddenField ID="hid_check_number" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>' />
                                    <asp:HiddenField ID="hid_pic" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pic").ToString())%>' />
                                    <asp:LinkButton ID="btn_Edit" runat="server" CssClass="btn btn-success" CommandName="cmdEdit" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'>
     <i class="fa fa-wrench"></i>修正</asp:LinkButton>
                                    <asp:LinkButton ID="btn_show" runat="server" CssClass="btn btn-warning" CommandName="cmdView" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'>
     <i class="fa fa-wrench"></i>檢視圖片</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="5" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
            </div>
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
              <div id="page-nav" class="page"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-default_02" data-backdrop="static">
        <div class="modal-dialog">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">實物照片</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">託運單號：</label>
                                <div class="col-sm-2">
                                    <asp:Label ID="mcheck_number" runat="server"></asp:Label>
                                </div>
                                <label class="col-sm-3 control-label">配達站所：</label>
                                <div class="col-sm-2">
                                    <asp:Label ID="mSuppliers" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-10">
                                    <div class="form-group col-sm-12 form-inline">
                                        <asp:Label ID="Label1" runat="server" Text="長"></asp:Label>：
                                        <asp:Label ID="mcbmLength" runat="server"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text="寬"></asp:Label>：    
                                        <asp:Label ID="mcbmWidth" runat="server"></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text="高"></asp:Label>：<asp:Label ID="mcbmHeight" runat="server"></asp:Label>
                                        <asp:Label ID="Label6" runat="server" Text="才數"></asp:Label>：
                                        <asp:Label ID="mcbmCont" runat="server"></asp:Label>
                                    </div>
                                </div>
                            <div class="form-group">
                                <asp:Image ID="Image1" Style="height:  100%; width: 100%;" runat="server" />
                            </div>
                        </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
