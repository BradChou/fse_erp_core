﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;



public partial class LT_member_4 : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion


            string manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = "";
            string wherestr = string.Empty;

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            #region  站所
            using (SqlCommand cmd = new SqlCommand())
            {
                switch (manager_type)
                {
                    case "0":
                        supplier_code = "";
                        break;
                    case "1":
                        supplier_code = "";
                        //管理者
                        //站所主管(user_name like '%主管%')只顯示該站所客戶
                        //無則可查所有站所下的所有客戶
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                            cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                            cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                            if (station_level.Equals("1"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("2"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("4"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code 
                                            and user_name like '%主管%'";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    supplier_code += "'";
                                    supplier_code += dta.Rows[0]["station_scode"].ToString();
                                    supplier_code += "'";
                                }
                            }
                        }
                        break;
                    case "2":
                        supplier_code = "";
                        //一般員工
                        //有綁定站所只顯示該站所客戶
                        //無則可查所有站所下的所有客戶
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                            cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                            cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                            if (station_level.Equals("1"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("2"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("4"))
                            {
                                cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dta.Rows.Count; i++)
                                    {
                                        supplier_code += "'";
                                        supplier_code += dta.Rows[i]["station_scode"].ToString();
                                        supplier_code += "'";
                                        supplier_code += ",";
                                    }
                                    supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("5"))
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    supplier_code += "'";
                                    supplier_code += dta.Rows[0]["station_scode"].ToString();
                                    supplier_code += "'";
                                }
                            }
                            else
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    supplier_code += "'";
                                    supplier_code += dta.Rows[0]["station_scode"].ToString();
                                    supplier_code += "'";
                                }
                            }
                        }
                        break;
                    case "4":
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmda.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dta = dbAdapter.getDataTable(cmda);
                            if (dta != null && dta.Rows.Count > 0)
                            {
                                supplier_code += "'";
                                supplier_code += dta.Rows[0]["station_scode"].ToString();
                                supplier_code += "'";
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();
                        break;
                }

                if (supplier_code != "")
                {
                    if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
                    {
                        cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and station_scode in (" + supplier_code + ")";
                    }

                }
                cmd.CommandText = string.Format(@" SELECT  station_code, station_code + ' ' +  station_name as showname, station_scode, station_scode + ' ' +  station_name as showsname    from tbStation with(nolock)
                                                   WHERE ISNULL(station_scode,'') <> '' {0} and tbStation.station_code <> 'F53' and tbStation.station_code <> 'F71'
                                                   ORDER BY  station_scode", wherestr);

                DataTable dt = dbAdapter.getDataTable(cmd);
                ddl_station.DataSource = dt;
                ddl_station.DataValueField = "station_scode";
                ddl_station.DataTextField = "showname";
                ddl_station.DataBind();
                // if (supplier_code == "")
                //{
                ddl_station.Items.Insert(0, new ListItem("全部", ""));
                //}
            }

            #endregion


            DefaultData(supplier_code);

        }
    }




    protected void DefaultData(string supplier_code)
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";


            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }

            if (Request.QueryString["station"] != null)
            {
                ddl_station.SelectedValue = Request.QueryString["station"];
                querystring += "&station=" + ddl_station.SelectedValue;
            }
            if (!string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                ddl_type.SelectedValue = Request.QueryString["type"];
            }

            #endregion

            string strWhere = "";

            strSQL = @"declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock)                      
                       group by customer_code

                            SELECT ROW_NUMBER() OVER(ORDER BY A.customer_code,A.id) AS num,
							  A.id as seq
							  ,C.station_name 
							  ,A.customer_code
							  ,B.customer_name
						      ,A.total_count
							  ,A.total_count - A.used_count as rest_count
                             ,CASE A.is_active WHEN '1' THEN '是' ELSE '否' END is_active  
                             ,A.update_user
							 ,A.update_date
                         FROM Customer_checknumber_setting A With(Nolock)
				         INNER JOIN @newest N  ON A.customer_code = N.customer_code and A.update_date = N.update_date 
                         LEFT JOIN tbCustomers B With(Nolock) ON A.customer_code = B.customer_code 
                         LEFT JOIN tbStation  C With(Nolock) ON B.supplier_code = C.station_code					
                         where 1=1 ";

            #region 查詢條件

            if (Session["manager_type"].ToString() == "5")
            {
                cmd.Parameters.AddWithValue("@customer_name", Session["customer_code"]);
                strWhere += "AND (B.customer_code= @customer_name)";
                ddl_station.Visible = false;
                tb_search.Visible = false;
                btn_search.Visible = false;
                lb1.Visible = false;
            }


            if (tb_search.Text != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", tb_search.Text);
                cmd.Parameters.AddWithValue("@customer_name", tb_search.Text);
                strWhere += " AND(B.customer_code like '%'+@customer_code+'%' or B.customer_name like '%'+@customer_name+'%') ";
            }

            if (!string.IsNullOrEmpty(Request.QueryString["type"]))
            {
                cmd.Parameters.AddWithValue("@product_type", ddl_type.SelectedValue);
                strWhere += " And B.product_type= @product_type ";
            }
            else 
            {
                strWhere += " And B.product_type in ('2','3') ";

            }



            if (ddl_station.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", ddl_station.SelectedValue.ToString());
                strWhere += " AND B.station_scode =@supplier_code";
            }
            else
            {
                string station = Session["management"].ToString();
                string station_level = Session["station_level"].ToString();
                string station_area = Session["station_area"].ToString();
                string station_scode = Session["station_scode"].ToString();
                if (station_level.Equals("1"))
                {
                    cmd.Parameters.AddWithValue("@management", station);
                    strWhere += " AND C.management =@management";
                }
                else if (station_level.Equals("2"))
                {
                    cmd.Parameters.AddWithValue("@station_scode", station_scode);
                    strWhere += " AND C.station_scode =@station_scode";
                }
                else if (station_level.Equals("4"))
                {
                    cmd.Parameters.AddWithValue("@station_area", station_area);
                    strWhere += " AND C.station_area =@station_area";
                }


            }
            #endregion

            cmd.CommandText = string.Format(strSQL + " {0}", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {


        String str_Query = "&station=" + ddl_station.SelectedValue;


        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }
        if (ddl_type.SelectedValue!="")
        {
            str_Query += "&type=" + ddl_type.SelectedValue;
        }

        Response.Redirect(ResolveUrl("~/LT_member_4.aspx?" + str_Query));
    }



}
