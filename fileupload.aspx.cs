﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class fileupload : System.Web.UI.Page
{

    protected void btnsave_Click(object sender, EventArgs e)
    {
        string file01str = "";
        if (!Page.IsValid)
            return;
        
        string fileExtension = "";
        bool fileok = false;
        string tErrStr = "";

        if (file01.HasFile)
        {
            Int32 fileSize = file01.PostedFile.ContentLength;
            if (fileSize > 5242880)
            {
                tErrStr = "檔案 [ " + file01.PostedFile.FileName + " ] 限傳5MB   請重新選擇檔案";
            }

            fileExtension = System.IO.Path.GetExtension(file01.FileName).ToLower().ToString();
            //取得檔案格式
            string[] allowedExtensions = { ".xls", ".pdf", ".xlsx", ".doc", ".docx", ".jpg", ".gif", ".png", ".tif", ".7z", ".odt", ".ods", ".odp", ".ppt", ".pptx", ".txt" };
            //定義允許的檔案格式
            //逐一檢查允許的格式中是否有上傳的格式
            for (int i = 0; i <= allowedExtensions.Length - 1; i++)
            {
                if (fileExtension == allowedExtensions[i])
                {
                    fileok = true;
                }
            }

            if (!fileok)
            {
                tErrStr = "上傳檔案格式錯誤，請上傳excel、word、pdf、jpg、gif、png、tif、7z、odt、ods、odp、ppt、txt格式";
            }
        }
        else
        {
            if (hidf_id.Value == "")
            {
                tErrStr = "請選擇上傳檔案";
            }
        }

        if (tErrStr == "")
        {
            try
            {
                if ((file01.HasFile))
                {
                    string strFileName = file01.FileName;
                    string strDotExtName = strFileName.Substring(strFileName.IndexOf("."));
                    //string strNewFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff"); 
                    string strNewFileName = file01.FileName.Substring(0, file01.FileName.Length - strDotExtName.Length) + DateTime.Now.ToString("yyyyMMdd");


                    file01.PostedFile.SaveAs(Server.MapPath("~/files/contract/") + strNewFileName + strDotExtName);
                    file01str = strNewFileName + strDotExtName;

                }

            }
            catch (Exception ex)
            {
                tErrStr = ex.Message;
            }
        }


        if (tErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + tErrStr + "');", true);
        }
        else
        {
            
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["filename"] + "').val('" + file01str + "');" +
                                             "self.parent.$('#" + Request.QueryString["fileview"] + "').attr('href','" + "/files/contract/" + file01str + "');" +
                                             "self.parent.$('#" + Request.QueryString["fileview"] + "').text('" + file01.FileName + "');" +
                                             "parent.$.fancybox.close();</script>", false);
        }
    }
}