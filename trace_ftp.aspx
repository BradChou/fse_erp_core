﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTrace.master" AutoEventWireup="true" CodeFile="trace_ftp.aspx.cs" Inherits="trace_ftp" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(function () {
                init();
            });            

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                init();                
            }
        });

        function init() {
            $("#tbscan [id*=chkSend]").click(function () {
                if ($(this).is(":checked")) {
                    $("#tbscan [id*=cbSelSend]").prop("checked", true);
                } else {
                    $("#tbscan [id*=cbSelSend]").prop("checked", false);
                }

            });
            $("#tbscan [id*=cbSelSend]").click(function () {
                if ($("#tbscan [id*=cbSelSend]").length == $("#tbscan [id*=cbSelSend]:checked").length) {
                    $("#tbscan [id*=chkSend]").prop("checked", true);
                } else {
                    $("#tbscan [id*=chkSend]").prop("checked", false);
                }

            });
            
            $("#tbscan [id*=chkPhoto]").click(function () {
                if ($(this).is(":checked")) {
                    $("#tbscan [id*=cbSelPhoto]").prop("checked", true);
                } else {
                    $("#tbscan [id*=cbSelPhoto]").prop("checked", false);
                }

            });
            $("#tbscan [id*=cbSelPhoto]").click(function () {
                if ($("#tbscan [id*=cbSelPhoto]").length == $("#tbscan [id*=cbSelPhoto]:checked").length) {
                    $("#tbscan [id*=chkPhoto]").prop("checked", true);
                } else {
                    $("#tbscan [id*=chkPhoto]").prop("checked", false);
                }

            });

        }


    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }
        .auto-style2 {
            font-size: small;
            color: #CC0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">竹運配達/簽單補傳作業</h2>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="View1" runat="server">
                            <div id="div_search" runat="server">
                                <div class="templatemo-login-form">
                                    <%--<div class="form-group form-inline">
                                <div class="col-lg-1 form-group form-inline">
                                    <label>掃單時間</label>
                                </div>
                                <div class="col-lg-11 col-md-11 form-group form-inline">
                                    <asp:TextBox ID="tb_dateS" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD"></asp:TextBox>
                                    ~
                                <asp:TextBox ID="tb_dateE" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD"></asp:TextBox>
                                    <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" ValidationGroup="validate" />
                                </div>
                            </div>--%>
                                    <div class="form-group form-inline">
                                        <div class="col-lg-1 form-group form-inline">
                                            <label>貨號</label>
                                        </div>
                                        <div class="col-lg-10 col-md-10 form-group form-inline">
                                            <asp:PlaceHolder ID="PlaceHolder1" runat="server">
                                                <asp:TextBox ID="fixserial" runat="server" placeholder="請輸入貨號"></asp:TextBox></asp:PlaceHolder>
                                        </div>
                                        <div class="col-lg-1 form-group form-inline ">
                                            <asp:LinkButton ID="AddTxtButton" runat="server" OnClick="AddTxtButton_Click"><img src="images/add.png" alt="新增輸入欄位" align="absmiddle" /></asp:LinkButton>
                                            <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div id="div_list" runat="server">
                                        <table id="tbscan" class="table table-striped table-bordered templatemo-user-table">
                                            <tr class="tr-only-hide  ">
                                                <th class="text-center" >NO.</th>
                                                <th class="text-center">貨號</th>
                                                <th class="text-center">配達時間</th>
                                                <th class="text-center">站所</th>
                                                <th class="text-center">狀態</th>
                                                <th class="text-center">員工</th>
                                                <th class="text-center">上傳記錄</th>
                                                <th class="text-center"><asp:CheckBox ID="chkSend" runat="server" Checked ="true" /> 補傳配達</th>
                                                <th class="text-center"><asp:CheckBox ID="chkPhoto" runat="server" Checked ="true" /> 補傳簽單</th>
                                                
                                            </tr>
                                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                                <ItemTemplate>
                                                    <tr >
                                                        <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                                                        <td data-th="貨號">
                                                            <asp:HiddenField ID="Hid_logid" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.log_id").ToString())%>' />
                                                            <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label></td>
                                                        <td data-th="配達時間">
                                                            <asp:Label ID="lbscan_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%>'></asp:Label></td>
                                                        <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                                        <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status").ToString())%></td>
                                                        <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>  <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                                                        <td data-th="">
                                                            <%--<asp:HyperLink runat ="server" id ="Imglink" class="fancybox fancybox.iframe"><asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" /></asp:HyperLink>--%>
                                                            <a href="ftplog.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>" class="fancybox fancybox.iframe " id="logclick"><asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" /></a>
                                                        </td>
                                                        <td><asp:CheckBox ID="cbSelSend" runat="server" Checked ="true"  Visible ='<%# Convert.ToInt32(DataBinder.Eval(Container, "DataItem.request_id")) != 0 %>' />
                                                            <asp:Literal ID="LiSend" runat="server" Visible ='<%# Convert.ToInt32(DataBinder.Eval(Container, "DataItem.request_id")) == 0 %>' >無17f單據，不可上傳</asp:Literal>
                                                        </td>
                                                        <td><asp:CheckBox ID="cbSelPhoto" runat="server" Checked ="true" Visible ='<%# Convert.ToInt32(DataBinder.Eval(Container, "DataItem.request_id")) != 0 %>'  />
                                                            <asp:Literal ID="LiPhoto" runat="server" Visible ='<%# Convert.ToInt32(DataBinder.Eval(Container, "DataItem.request_id")) == 0 %>' >無17f單據，不可上傳</asp:Literal>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (New_List.Items.Count == 0)
                                                {%>
                                            <tr>
                                                <td colspan="12" style="text-align: center">尚無資料</td>
                                            </tr>
                                            <% } %>
                                        </table>
                                        共 <span class="text-danger">
                                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                                        </span>筆
                                        
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class ="text-center " >
                                <asp:Button ID="btn_Prev1" CssClass=" templatemo-view-img-btn " runat="server" Text="回上頁" OnClick="btn_Prev1_Click" />
                                <asp:Button ID="btnFtp" CssClass=" templatemo-view-img-btn  " runat="server" Text="上　傳"  OnClientClick="return confirm('確定要上傳嗎?');" OnClick="btnFtp_Click" />
                            </div>
                            <span class="auto-style2">*如無17f單據，不可上傳，請先補託運單後方可補傳</span><br />
                            <span class="auto-style2">*補傳紀錄成功後，下一小時方可於HCT查詢到</span>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <div id="div1" runat="server">
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide  ">
                                        <th class="text-center">NO.</th>
                                        <th class="text-center">貨號</th>
                                        <th class="text-center">配達上傳時間</th>
                                        <th class="text-center">簽單上傳時間</th>

                                    </tr>
                                    <asp:Repeater ID="RpFtoLog" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="NO."><%# Container.ItemIndex + 1 %></td>
                                                <td data-th="貨號">
                                                    <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label></td>
                                                <td data-th="配達上傳時間">
                                                    <a href="ftplog.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>&Type=Send" class="fancybox fancybox.iframe " id="log1click"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.senddate","{0:yyyy-MM-dd HH:mm:ss}").ToString())%></a></td>
                                                <td data-th="簽單上傳時間">
                                                    <a href="ftplog.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>&Type=Photo" class="fancybox fancybox.iframe " id="log2click"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.photodate","{0:yyyy-MM-dd HH:mm:ss}").ToString())%></a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (RpFtoLog.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="4" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                共 <span class="text-danger">
                                    <asp:Literal ID="ltotalpages2" runat="server"></asp:Literal>
                                </span>筆
                                        
                            </div>
                            <div class="text-center ">
                                <asp:Button ID="btnClose" CssClass=" templatemo-view-img-btn " runat="server" Text="關閉" OnClick="btnClose_Click" />
                            </div>
                        </asp:View>
                    </asp:MultiView>

                </ContentTemplate>
            </asp:UpdatePanel>

            <hr />




        </div>
    </div>
</asp:Content>

