﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="trace_3.aspx.cs" Inherits="trace_3" %>

<%@ Register src="uc/uc_PeidaReport.ascx" tagname="uc_PeidaReport" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">配達統計</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">

                    <div class="col-lg-8 col-md-8 form-group form-inline">
                        <label >發送日期</label>
                        <asp:TextBox ID="dates" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        ~ 
                        <asp:TextBox ID="datee" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <asp:Label ID="Label2" runat="server" Text="客代"></asp:Label><asp:DropDownList ID="dlcustomer_code" runat="server"  CssClass="chosen-select" ></asp:DropDownList>
                        
                        <asp:Button ID="search" CssClass="btn btn-darkblue btn_view" runat="server" Text="查詢" OnClick ="search_Click" />
                    </div>

                    <div class="col-lg-4 col-md-4 form-group form-inline text-right">
                        
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                    </div>
                </div>
            </div>
            <hr/>
            <p>※午前配達筆數：於13:00前掃讀配達之筆數；當日配達筆數：於17:00前掃讀配達之筆數
            </p>
            <uc1:uc_PeidaReport ID="uc_PeidaReport1" runat="server" />
            <%--<table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>配送站所</th>
                    <th>應配筆數</th>
                    <th>配送筆數</th>
                    <th>午前配達筆數</th>
                    <th>午前配達率</th>
                    <th>當日配達筆數</th>
                    <th>當日配達率</th>
                    <th>已配達筆數</th>
                    <th>配達率</th>
                    <th>未配達筆數</th>
                    <th>異常率</th>
                    <th>首筆配達</th>
                    <th>末筆配達</th>                        
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="配送站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_name").ToString())%></td>
                            <td data-th="應配筆數"><a href="trace_3_1.aspx?kind=1&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>"  target="_blank" class=" btn btn-link " id="link1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應配筆數","{0:N0}").ToString())%></a></td>
                            <td data-th="配送筆數"><a href="trace_3_1.aspx?kind=2&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>"  target="_blank"class=" btn btn-link " id="link2"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送筆數","{0:N0}").ToString())%></a></td>                            
                            <td data-th="午前配達筆數"><a href="trace_3_1.aspx?kind=3&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>" target="_blank" class=" btn btn-link " id="link3"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.午前配達筆數","{0:N0}").ToString())%></a></td>
                            <td data-th="午前配達率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noonRate").ToString())%></td>
                            <td data-th="當日配達筆數"><a href="trace_3_1.aspx?kind=4&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>" target="_blank" class=" btn btn-link " id="link4"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.當日配達筆數","{0:N0}").ToString())%></a></td>
                            <td data-th="當日配達率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dayRate").ToString())%></td>
                            <td data-th="已配達筆數"><a href="trace_3_1.aspx?kind=5&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>" target="_blank" class=" btn btn-link " id="link5"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.已配達筆數","{0:N0}").ToString())%></a></td>
                            <td data-th="異常率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.yesRate").ToString())%></td>
                            <td data-th="未配達筆數"><a href="trace_3_1.aspx?kind=6&supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&dates=<%=dates.Text%>&datee=<%=datee.Text%>&customer_code=<%=dlcustomer_code.SelectedValue%>" target="_blank" class=" btn btn-link " id="link6"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noArrive","{0:N0}").ToString())%></a></td>
                            <td data-th="異常率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noRate").ToString())%></td>
                            <td data-th="首筆配達"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MIN").ToString())%></td>
                            <td data-th="末筆配達"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MAX").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="12" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>--%>
            
        </div>
    </div>
</asp:Content>

