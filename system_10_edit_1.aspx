﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="system_10_edit_1.aspx.cs" Inherits="system_10_edit_1" %>



<form runat="server">
        <h3 class="modal-title">檢視運價表</h3>

        <asp:Button type="button" runat="server" OnClick="ViewFreightTable" Text="檢視" />
        <asp:Button type="button" runat="server" OnClick="DownLoadCsv" Text="下載" />

        <asp:GridView runat="server" ID="freight_table" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>

</form>

