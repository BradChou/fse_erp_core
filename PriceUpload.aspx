﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PriceUpload.aspx.cs" Inherits="PriceUpload" %>

<!DOCTYPE html>
<html>
<head>
     <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>峻富雲端物流管理-上傳新運價</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">   

</head>
<body>
     <p>
         <span>車輛使用效益總表</span></p>
     <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇新運價檔案</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-inline">
                            類型：
                            <asp:DropDownList ID="customer_type" runat="server" AutoPostBack="true"  >
                                <asp:ListItem Value="2">竹運</asp:ListItem>
                                <asp:ListItem Value="1">區配</asp:ListItem>
                                <asp:ListItem Value="4">C配</asp:ListItem>
                                <asp:ListItem Value="5">合發專用</asp:ListItem>
                                <asp:ListItem Value="3">自營</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-inline" role="form">
                            <asp:FileUpload ID="file02" runat="server" Width="300px" />            
                            <asp:Button ID="btImport" runat="server" Text="確定" OnClick="btImport_Click" /> 
                            <asp:Button ID="btncancel" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                            <asp:DropDownList ID="dpZIP" runat="server" Visible="False">
                            </asp:DropDownList>
                        </div>
                        <div style="overflow: auto; height:500px; vertical-align :top;">
                            <asp:ListBox ID="lbMsg" runat="server" Height="490px" Width="100%"></asp:ListBox>
                        </div>
                       
                     
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
