﻿using BarcodeLib;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Text;

public partial class LTtransport_4 : System.Web.UI.Page
{
    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            #region 
            manager_type = Session["manager_type"].ToString(); //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            string customer_code = string.Empty;
            Master_code = Session["master_code"].ToString();

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();


            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @"select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @"select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5"))
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code += "'";
                                supplier_code += dt.Rows[0]["station_scode"].ToString();
                                supplier_code += "'";
                            }
                        }
                        else
                        {

                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_scode"].ToString();
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_scode"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    if (supplier_code.Length > 3)
                    {
                        customer_code = supplier_code;
                        supplier_code = supplier_code.Substring(0, 3);
                    }

                    break;
            }

            using (SqlCommand cmd2 = new SqlCommand())
            {
                string wherestr = "";
                //if (supplier_code != "")
                //{
                //    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                //    wherestr = " and supplier_code = @supplier_code";
                //}
                //if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                //{
                //    cmd2.Parameters.AddWithValue("@master_code", master_code);
                //    wherestr += " and customer_code like @master_code+'%' ";
                //}

                if (supplier_code != "")
                {
                    if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        //wherestr = " and A.customer_code like ''+@supplier_code+'%' ";
                        wherestr = " and A.station_scode in (" + supplier_code + ") ";
                    }

                }

                if (customer_code.Length > 0)
                {
                    cmd2.Parameters.AddWithValue("@customer_code", customer_code);
                    wherestr += " and A.customer_code = @customer_code ";
                }

                cmd2.Parameters.AddWithValue("@type", "1");
                cmd2.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    -- inner join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where A.type =@type and A.stop_shipping_code = '0' {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);

                customerDropDown.DataSource = dbAdapter.getDataTable(cmd2);
                customerDropDown.DataValueField = "customer_code";
                customerDropDown.DataTextField = "name";
                customerDropDown.DataBind();
                customerDropDown.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }

            using (SqlCommand cmd1 = new SqlCommand())
            {
                string wherestr = string.Empty;
                if (supplier_code != "")
                {
                    if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
                    {
                        cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                        //wherestr = " and station_code = @supplier_code";
                        wherestr = " and station_scode in (" + supplier_code + ")";
                    }

                }

                cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where active_flag =1  {0} and station_code <> 'F53' and station_code <> 'F71'
                                               order by station_code", wherestr);
                Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
                Suppliers.SelectedIndex = 0;
                //if (Suppliers.Items.Count > 0)
                //{
                //    Suppliers.Items.Insert(0, new ListItem("全部", ""));
                //    Suppliers.SelectedIndex = 0;
                //}
                //else {

                //    if (manager_type == "0" || manager_type == "1" || manager_type == "2")
                //    {
                //       

                //    }
                //}


            }


            Suppliers_SelectedIndexChanged(null, null);

            #endregion

            date1.Text = DateTime.Today.ToString("yyyy/MM/dd");
            date2.Text = DateTime.Today.ToString("yyyy/MM/dd");
            cdate1.Text = DateTime.Today.ToString("yyyy/MM/dd");
            cdate2.Text = DateTime.Today.ToString("yyyy/MM/dd");
            lbDate.Text = "建檔日期";
            cdate1.Visible = true;
            cdate2.Visible = true;
            date1.Visible = false;
            date2.Visible = false;

            if (Session["isSuccess"] != null)
            {
                if (bool.Parse(Session["isSuccess"].ToString()))
                {
                    string theDay;
                    DateTime day = DateTime.Now;
                    if (day.Hour < 16)
                    {
                        theDay = "今天";
                    }
                    else
                    {
                        theDay = "明天";
                        day = day.AddDays(1);
                    }

                    string alertMessage = string.Format("派件成功，司機將在{0}({1}/{2}) 12:00~18:00 到場收件", theDay, day.Month, day.Day);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMessage + "')", true);
                }
                Session.Remove("isSuccess");
            }


            //readdata();
            //string sScript = "$('.date_picker').datepicker({ " +
            //                 "  dateFormat: 'yy/mm/dd', " +
            //                 "  changeMonth: true, " +

            //                 " });  ";
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

        }

    }

    protected void ShowPositionOrNot(object sender, EventArgs e)
    {
        if (option.SelectedValue == "0")
        {
            posi.Visible = true;
        }
        else
        {
            posi.Visible = false;
        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        string wherestr = "";
        string orderby = " order by A.cdate desc";

        SqlCommand cmd = new SqlCommand();

        if (manager_type == "5")  //登入帳號為客戶，只搜尋該客戶的貨號
        {
            string login_customer_code = Session["customer_code"].ToString();
            if (login_customer_code.Length > 0)
            {
                cmd.Parameters.AddWithValue("@customer_code", login_customer_code);
                wherestr += " and A.customer_code = @customer_code ";
            }
        }
        else                     //登入帳號為其他，依supplier_code，customer_code 查詢
        {
            if (Suppliers.SelectedValue != "")
            {
                if (check_number.Text == "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue.ToString());
                    wherestr = " and A.send_station_scode = @supplier_code";
                }

            }
            else {
                string management = Session["management"].ToString();
                string station_level = Session["station_level"].ToString();
                string station_area = Session["station_area"].ToString();
                string station_scode = Session["station_scode"].ToString();

                string managementList = "";
                string station_scodeList = "";
                string station_areaList = "";
                if (station_level.Equals("1"))
                {
                    cmd.Parameters.AddWithValue("@management", management);

                    cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            managementList += "'";
                            managementList += dt1.Rows[i]["station_scode"].ToString();
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }
                    if (check_number.Text == "") { wherestr = " and A.send_station_scode in (" + managementList + ")"; }
                        
                }
                else if (station_level.Equals("2"))
                {
                    cmd.Parameters.AddWithValue("@station_scode", station_scode);
                    cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_scodeList += "'";
                            station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }
                    if (check_number.Text == "") { wherestr = " and A.send_station_scode in (" + station_scodeList + ")"; }


                }
                else if (station_level.Equals("4"))
                {
                    cmd.Parameters.AddWithValue("@station_area", station_area);

                    cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_areaList += "'";
                            station_areaList += dt1.Rows[i]["station_scode"].ToString();
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }
                    if (check_number.Text == "")
                    { wherestr += " and A.send_station_scode in (" + station_areaList + ")"; }
                }
            }

            if (Customer.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@Customer", Customer.SelectedValue.ToString());
                wherestr = "and A.customer_code LIKE ''+  ISNULL(@Customer,'') +'%'";
                //if (Suppliers.SelectedValue.ToString() + Customer.SelectedValue.ToString() == "0000049") orderby = " order by  A.cdate , A.request_id";
            }
        }

        if (DropDownList1.SelectedValue == "未列印")
        {
            //wherestr += " and A.check_number =''";
            wherestr += " and A.print_flag =0";

            if (check_number.Text != "")
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                wherestr += " and A.check_number = @check_number";
            }
            else if (cdate1.Text != "" && cdate2.Text != "")
            {
                cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
            }
        }
        else
        {
            //wherestr += " and A.check_number <>''";
            wherestr += " and A.print_flag =1";
            if (check_number.Text != "")
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                wherestr += " and A.check_number = @check_number";
            }
            else if (date1.Text != "" && date2.Text != "")
            {
                cmd.Parameters.AddWithValue("@print_date1", date1.Text);
                cmd.Parameters.AddWithValue("@print_date2", date2.Text);
                wherestr += " and ((CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@print_date2,111) ) " +
                            " or (A.print_date IS NULL and CONVERT(VARCHAR,A.cdate,111)  > CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@print_date2,111) )) ";
            }
        }
        cmd.Parameters.AddWithValue("@pricing_type", "02");

        //cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where 0=0 {0} order by supplier_code", wherestr);
        cmd.CommandText = string.Format(@"select A.request_id, A.cdate, A.arrive_assign_date  , A.supplier_date, A.print_date,A.print_flag, A.check_number ,A.receive_contact , 
                                        A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area ,
                                        CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                        A.pieces , A.plates , A.cbm ,
                                        B.code_name , A.customer_code , C.customer_shortname ,
                                        A.send_city , A.send_area , A.send_address,
                                        ROW_NUMBER() OVER(order by A.cdate desc) AS ROWID
                                        from tcDeliveryRequests A With(Nolock)                                          
                                        left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                        left join tbCustomers C on C.customer_code = A.customer_code
                                        where A.DeliveryType='D' AND A.cancel_date IS NULL and  A.cdate >= DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
        DataTable dt = dbAdapter.getDataTable(cmd);
        New_List.DataSource = dt;
        New_List.DataBind();
        //ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    /// <summary>
    /// 一筆兩式託運單
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReport(string print_requestid)
    {
        string ttErrStr = string.Empty;
        if (print_requestid != "")
        {
            using (DataTable dt = getPrintReportDT(print_requestid))
            {
                if (dt != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = dt.Clone();  //複製DT的結構
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        dt.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["barcode"].ToString()));
                        if (dt.Rows[i]["Orderbarcode"].ToString() != "")
                        {
                            dt.Rows[i]["Orderbarcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["Orderbarcode"].ToString()));
                        }
                        dt2.ImportRow(dt.Rows[i]);
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];

                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "LTReport1_3", ParName, ParValue, "DataSet1", dt2);

                }
            }
        }
    }


    private DataTable getPrintReportDT(string print_requestid)
    {
        DataTable dt = null;
        SqlCommand cmd = new SqlCommand();
        string wherestr = string.Empty;
        wherestr += dbAdapter.genWhereCommIn(ref cmd, "A1.request_id", print_requestid.Split(','));
        cmd.CommandText = string.Format(@"
select                               A1.area_arrive_code --A1.area_arrive_code	--零擔到著簡碼 
									,convert(nvarchar(4),A1.print_date,120) Print_year
									,SUBSTRING(convert(nvarchar(10),A1.print_date,120),6,2 ) Print_Mon
									,SUBSTRING(convert(nvarchar(10),A1.print_date,120),9,2 ) Print_Day
                                    --,print_date			--通知取件時間
                                    ,RTRIM(A1.Order_number)	Orderbarcode	--退貨單號
                                    , A1.Order_number
                                    ,A1.Receive_contact    --收件人
                                    ,A1.Receive_city + A1.Receive_area + A1.receive_address	as Reveiveaddress	--收件人地址 
                                    ,isnull(A2.Receive_contact,A1.Send_contact) Send_contact		--寄件人
                                    ,A1.Send_city + A1.Send_area+ A1.Send_address  as Sendaddress --寄件人地址
                                    ,RTRIM(A1.check_number)  as barcode   --條碼
                                    ,A1.Invoice_desc			--備註
                                    ,A1.Invoice_memo			--備註
                                    ,A1.Check_number			--查貨單號
                                    ,A1.Send_tel 			--寄件人電話 
                                    ,A1.receive_tel1 		--收件人電話 
                                    ,A1.pieces 			--件數
									,(select top 1 ts.station_scode  from ttArriveSitesScattered ta left join tbStation ts on ta.station_code = ts.station_scode
									where ta.post_city = A1.Send_city and ta.post_area =  A1.Send_area) send_arrive_code --發送站碼
                                    ,(select top 1 td.driver_name from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number = A1.check_number order by tds.cdate desc) driver_name
									,(select top 1 td.driver_mobile from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number =  A1.check_number order by tds.cdate desc) driver_mobile
                                    from tcDeliveryRequests A1  with(nolock)
                                    left join  tbStation ts on ts.station_code = A1.supplier_code
                                    Left join tcDeliveryRequests A2 with(nolock) on A1.send_contact = A2.check_number
                                    AND ( A1.cancel_date is null)
                                    where 1=1 
                                {0}
								order by  A1.check_number", wherestr);
        dt = dbAdapter.getDataTable(cmd);

        return dt;
    }


    protected void btnPirntLabel_Click(object sender, EventArgs e)
    {
        string ttErrStr = string.Empty;
        string print_requestid = "";

        if (ttErrStr == "")
        {
            if (New_List.Items.Count > 0)
            {
                for (int i = 0; i <= New_List.Items.Count - 1; i++)
                {

                    Boolean Update = false;
                    string print_date = ((Label)New_List.Items[i].FindControl("lbprint_date")).Text;
                    string Supplier_date = ((Label)New_List.Items[i].FindControl("lbSupplier_date")).Text;
                    string arrive_assign_date = ((HiddenField)New_List.Items[i].FindControl("Hid_arrive_assign_date")).Value;
                    CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                    Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));
                    Boolean print_flag = Convert.ToBoolean(((HiddenField)New_List.Items[i].FindControl("Hid_print_flag")).Value);
                    if ((chkRow != null) && (chkRow.Checked))
                    {
                        string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            #region 發送日期
                            if (print_date == "")
                            {
                                cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日
                                print_date = DateTime.Today.ToString("yyyy/MM/dd");
                                //int Week = (int)DateTime.Today.DayOfWeek;
                                //if (DateTime.Today.ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

                                ////配送日自動產生，平日為 D+1，週五為D+3 ,週六為 D+2 且系統須針對特定日期(假日，如過年)順延
                                //DateTime supplier_date = DateTime.Today;
                                //switch (Week)
                                //{
                                //    case 5:
                                //        supplier_date = supplier_date.AddDays(3);
                                //        break;
                                //    case 6:
                                //        supplier_date = supplier_date.AddDays(2);
                                //        break;
                                //    default:
                                //        supplier_date = supplier_date.AddDays(1);
                                //        break;
                                //}
                                //cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            #endregion

                            #region  配送日期
                            if (Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(print_date);

                                if (arrive_assign_date != "")
                                {
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                }
                                else
                                {
                                    //配送日自動產生，為 D+1
                                    supplier_date = supplier_date.AddDays(1);
                                }

                                cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            else
                            {
                                //1.配達，且配達區分 = 正常配交，就不可改指定日期
                                //2.指配日期不可以設定今天以前(含)的日期
                                if (arrive_assign_date != "" && arrive_assign_date != Supplier_date)
                                {
                                    DateTime supplier_date = Convert.ToDateTime(print_date);
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                    cmd.Parameters.AddWithValue("@supplier_date", supplier_date);          //配送日期
                                    Update = true;
                                }
                            }

                            if (print_flag == false)
                            {
                                cmd.Parameters.AddWithValue("@print_flag", "1");                           //是否列印
                                Update = true;
                            }
                            #endregion

                            if (Update == true)
                            {
                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                                dbAdapter.execNonQuery(cmd);
                            }

                        }
                        print_requestid += id.ToString() + ",";

                    }
                }
            }
            search_Click(null, null);

            if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);

            //公版標籤
            NameValueCollection nvcParamters = new NameValueCollection();
            nvcParamters["ids"] = print_requestid;

            // 有勾不顯示，則showSender=false
            if (!cbShowSender.Checked)
                nvcParamters["showSender"] = "false";

            string url = "";

            //施巴只印捲筒，標籤格式自訂
            if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Customer.SelectedValue)) ||
                    ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
            {
                //自訂標籤
                //捲筒
                //PrintReelLabel(print_requestid, "LTReelLabel_SB");
                url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintMutiTagPDFForSBByIds";
            }
            else
            {

                if (option.SelectedValue == "1")
                {
                    //捲筒
                    //PrintReelLabel(print_requestid);
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0";
                    //url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0_post";
                }
                else if (option.SelectedValue == "0")
                {
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1";
                    //url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1_post";
                    nvcParamters["position"] = Position.SelectedItem.Value;
                    //PrintLabel(print_requestid);
                }
                else
                {
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintReturnTagByIds2_0";
                    //PrintReport(print_requestid);
                    //return;
                }
            }
            string strForm = PreparePOSTForm(url, nvcParamters);

            Response.Clear();

            Response.Write(strForm);
            Response.End();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
            return;
        }
    }

    /// <summary>
    /// 列印標籤(以板列印)
    /// </summary>
    /// <param name="print_requestid">託運單id(以","隔開)</param>
    /// <param name="format">紙張格式(A4,一式6筆)</param>
    //    private void PrintLabel(string print_requestid,List<string> l_request_id, string format = "0")
    //    {
    //        string ttErrStr = "";

    //        DataTable dtClone = new DataTable();
    //        int Rowc = 1;
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandText = @"
    //                                select '' supplier_date  ,
    //                                '' print_date, 
    //                                '' check_numberbarcode,
    //                                '' check_number,
    //                                '' arrive_assign_date,
    //                                '' receive_contact , 
    //                                '' receive_tel1, 
    //                                '' receiveAddress,
    //                                '' subpoena_category,
    //                                '' money,
    //                                '' invoice_memo, 
    //                                '' invoice_desc,
    //                                '' customer_code , 
    //                                '' customer_shortname ,
    //                                '' send_contact,
    //                                '' send_tel,
    //                                '' sendAddress,
    //                                '' station_scode,
    //                                '' pieces
    //                                ";
    //            dtClone = dbAdapter.getDataTable(cmd);
    //        }

    //        DataTable l_LT1 = dtClone.Clone();
    //        DataTable l_LT2 = dtClone.Clone();
    //        DataTable l_LT3 = dtClone.Clone();
    //        DataTable l_LT4 = dtClone.Clone();
    //        DataTable l_LT5 = dtClone.Clone();
    //        DataTable l_LT6 = dtClone.Clone();

    //        DataRow workRow;

    //        for (int i = 0; i < l_request_id.Count; i++) {
    //            if (Rowc <= 6)
    //            {
    //                DataTable dt = new DataTable();
    //                using (SqlCommand cmd = new SqlCommand())
    //                {

    //                    cmd.Parameters.AddWithValue("@request_id", l_request_id[i].ToString());
    //                    cmd.CommandText = @"
    //select  REPLACE (SUBSTRING(convert(nvarchar(10),A.supplier_date,120), 6, 10),'-','') supplier_date  ,
    //A.print_date, 
    //A.check_number check_numberbarcode,
    //A.check_number,
    //A.arrive_assign_date,
    //A.receive_contact , 
    //A.receive_tel1, 
    //A.receive_city+A.receive_area + A.receive_address  receiveAddress,
    //case when A.subpoena_category ='11' then '元付'
    //	 when A.subpoena_category ='21' then '到付' 
    //	 when A.subpoena_category ='41' then '代收貨款' 
    //	 when A.subpoena_category ='25' then '到付追加' 
    //	 end subpoena_category, 
    //CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
    //A.invoice_memo, 
    //A.invoice_desc,
    //A.customer_code , 
    //C.customer_shortname ,
    //A.send_contact,
    //A.send_tel,
    //A.send_city+ A.send_area + A.send_address  sendAddress,
    //D.station_scode,
    //A.pieces
    //from tcDeliveryRequests A WITH(Nolock)
    //                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
    //                                          left join tbCustomers C on C.customer_code = A.customer_code
    //										  left join tbStation D on D.station_scode = A.area_arrive_code
    //                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate())
    //and convert(nvarchar,A.request_id) =convert(nvarchar, @request_id) 
    //										  order by  A.check_number";

    //                    dt = dbAdapter.getDataTable(cmd);
    //                }
    //                if (dt.Rows.Count > 0)
    //                {
    //                    int l_r = Convert.ToInt32(dt.Rows[0]["pieces"].ToString());
    //                    switch (Rowc)
    //                    {
    //                        case 1:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT1.ImportRow(workRow);
    //                            }
    //                            break;
    //                        case 2:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT2.ImportRow(workRow);
    //                            }
    //                            break;
    //                        case 3:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT3.ImportRow(workRow);
    //                            }
    //                            break;
    //                        case 4:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT4.ImportRow(workRow);
    //                            }
    //                            break;
    //                        case 5:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT5.ImportRow(workRow);
    //                            }
    //                            break;
    //                        case 6:
    //                            for (int r = 0; r < l_r; r++)
    //                            {
    //                                workRow = dt.Rows[0];
    //                                l_LT6.ImportRow(workRow);
    //                            }
    //                            break;
    //                    }
    //                }
    //                Rowc++;

    //            }
    //            else {
    //                DataTable dt = new DataTable();
    //                using (SqlCommand cmd = new SqlCommand())
    //                {

    //                    cmd.Parameters.AddWithValue("@request_id", l_request_id[i].ToString());
    //                    cmd.CommandText = @"
    //select  REPLACE (SUBSTRING(convert(nvarchar(10),A.supplier_date,120), 6, 10),'-','') supplier_date  ,
    //A.print_date, 
    //A.check_number check_numberbarcode,
    //A.check_number,
    //A.arrive_assign_date,
    //A.receive_contact , 
    //A.receive_tel1, 
    //A.receive_city+A.receive_area + A.receive_address  receiveAddress,
    //case when A.subpoena_category ='11' then '元付'
    //	 when A.subpoena_category ='21' then '到付' 
    //	 when A.subpoena_category ='41' then '代收貨款' 
    //	 when A.subpoena_category ='25' then '到付追加' 
    //	 end subpoena_category, 
    //CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
    //A.invoice_memo, 
    //A.invoice_desc,
    //A.customer_code , 
    //C.customer_shortname ,
    //A.send_contact,
    //A.send_tel,
    //A.send_city+ A.send_area + A.send_address  sendAddress,
    //D.station_scode
    //from tcDeliveryRequests A WITH(Nolock)
    //                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
    //                                          left join tbCustomers C on C.customer_code = A.customer_code
    //										  left join tbStation D on D.station_scode = A.area_arrive_code
    //                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate())
    //and convert(nvarchar,A.request_id) =convert(nvarchar, @request_id) 
    //										  order by  A.check_number";

    //                    dt = dbAdapter.getDataTable(cmd);
    //                }
    //                if (dt.Rows.Count > 0)
    //                {

    //                  workRow = dt.Rows[0];

    //                  l_LT1.ImportRow(workRow);
    //                }
    //                Rowc = 1;
    //                Rowc++;
    //            }
    //        }

    //        DataTable dtLT1 = new DataTable();
    //        DataTable dtLT2 = new DataTable();
    //        DataTable dtLT3 = new DataTable();
    //        DataTable dtLT4 = new DataTable();
    //        DataTable dtLT5 = new DataTable();
    //        DataTable dtLT6 = new DataTable();

    //        dtLT1 = dtClone.Clone();  //複製DT的結構
    //        dtLT2 = dtClone.Clone();  //複製DT的結構
    //        dtLT3 = dtClone.Clone();  //複製DT的結構
    //        dtLT4 = dtClone.Clone();  //複製DT的結構
    //        dtLT5 = dtClone.Clone();  //複製DT的結構
    //        dtLT6 = dtClone.Clone();  //複製DT的結構

    //        if (l_LT1.Rows.Count > 0) { 
    //        for (int i = 0; i <= l_LT1.Rows.Count - 1; i++)
    //        {
    //                l_LT1.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT1.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT1.ImportRow(l_LT1.Rows[i]);
    //        }
    //        }
    //        if (l_LT2.Rows.Count > 0)
    //        {
    //            for (int i = 0; i <= l_LT2.Rows.Count - 1; i++)
    //            {
    //                l_LT2.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT2.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT2.ImportRow(l_LT2.Rows[i]);
    //            }
    //            if (l_LT2.Rows.Count < l_LT2.Rows.Count) {
    //                DataRow row;
    //                row = dtLT2.NewRow();
    //                dtLT2.Rows.Add(row);
    //            }
    //        }
    //        if (l_LT3.Rows.Count > 0)
    //        {
    //            for (int i = 0; i <= l_LT3.Rows.Count - 1; i++)
    //            {
    //                l_LT3.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT3.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT3.ImportRow(l_LT3.Rows[i]);
    //            }
    //            if (l_LT3.Rows.Count < l_LT3.Rows.Count)
    //            {
    //                DataRow row;
    //                row = dtLT3.NewRow();
    //                dtLT3.Rows.Add(row);
    //            }
    //        }
    //        if (l_LT4.Rows.Count > 0)
    //        {
    //            for (int i = 0; i <= l_LT4.Rows.Count - 1; i++)
    //            {
    //                l_LT4.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT4.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT4.ImportRow(l_LT4.Rows[i]);
    //            }
    //            if (l_LT4.Rows.Count < l_LT4.Rows.Count)
    //            {
    //                DataRow row;
    //                row = dtLT4.NewRow();
    //                dtLT4.Rows.Add(row);
    //            }
    //        }
    //        if (l_LT5.Rows.Count > 0)
    //        {
    //            for (int i = 0; i <= l_LT5.Rows.Count - 1; i++)
    //            {
    //                l_LT5.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT5.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT5.ImportRow(l_LT5.Rows[i]);
    //            }
    //            if (l_LT5.Rows.Count < l_LT5.Rows.Count)
    //            {
    //                DataRow row;
    //                row = dtLT5.NewRow();
    //                dtLT5.Rows.Add(row);
    //            }
    //        }
    //        if (l_LT6.Rows.Count > 0)
    //        {
    //            for (int i = 0; i <= l_LT6.Rows.Count - 1; i++)
    //            {
    //                l_LT6.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(l_LT6.Rows[i]["check_numberbarcode"].ToString()));
    //                dtLT6.ImportRow(l_LT6.Rows[i]);
    //            }
    //            if (l_LT6.Rows.Count < l_LT6.Rows.Count)
    //            {
    //                DataRow row;
    //                row = dtLT6.NewRow();
    //                dtLT6.Rows.Add(row);
    //            }
    //        }

    //        DataSet ds = new DataSet();

    //        ds.Tables.Add(dtLT1);
    //        ds.Tables.Add(dtLT2);
    //        ds.Tables.Add(dtLT3);
    //        ds.Tables.Add(dtLT4);
    //        ds.Tables.Add(dtLT5);
    //        ds.Tables.Add(dtLT6);

    //        string[] ParName = new string[0];
    //        string[] ParValue = new string[0];

    //            PublicFunction.ShowLocalReport_PDF1(this, @"Reports\", "LTReport", ParName, ParValue, "ReturnsData", ds);
    //    }
    private void PrintLabel(string print_requestid)
    {
        string ttErrStr = string.Empty;
        if (print_requestid != "")
        {
            int RowNo = 1;
            DataTable dtLT1 = new DataTable();
            DataTable dtLT2 = new DataTable();
            using (DataTable dt = getPrintDT(print_requestid))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    dtLT1 = dt.Clone();  //複製DT的結構 
                    dtLT2 = dt.Clone();  //複製DT的結構 


                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        int pieces = dt.Rows[i]["pieces"] == DBNull.Value ? 1 : Convert.ToInt32(dt.Rows[i]["pieces"]);
                        dt.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["check_numberbarcode"].ToString()));
                        for (int j = 1; j <= pieces; j++)
                        {
                            switch (RowNo % 2)
                            {
                                case 1:
                                    dtLT1.ImportRow(dt.Rows[i]);
                                    break;
                                case 0:
                                    dtLT2.ImportRow(dt.Rows[i]);
                                    break;
                            }
                            RowNo++;

                        }
                    }
                }
            }

            DataSet ds = new DataSet();

            ds.Tables.Add(dtLT1);
            ds.Tables.Add(dtLT2);


            string[] ParName = new string[0];
            string[] ParValue = new string[0];

            ShowLocalReport_PDFLT(this, @"Reports\", "LTReport2", ParName, ParValue, "ReturnsData", ds);
        }
    }

    private DataTable getPrintDT(string print_requestid)
    {
        DataTable dt = null;
        SqlCommand cmd = new SqlCommand();
        string wherestr = string.Empty;
        wherestr += dbAdapter.genWhereCommIn(ref cmd, "A.request_id", print_requestid.Split(','));
        cmd.CommandText = string.Format(@"select  REPLACE (SUBSTRING(convert(nvarchar(10),A.supplier_date,120), 6, 10),'-','') supplier_date  ,
                                 CONVERT(varchar(100), A.print_date, 111) 'print_date', 
                                  A.check_number  check_numberbarcode,
                                  A.check_number  check_number_SB, A.check_number  check_numberbarcode_SB,
                                A.check_number,
                                A.arrive_assign_date,
                                A.receive_contact , 
                                A.receive_tel1, A.receive_tel2, 
                                A.receive_city+A.receive_area + A.receive_address  receiveAddress,
                                case when A.subpoena_category ='11' then '元付'
	                                 when A.subpoena_category ='21' then '到付' 
	                                 when A.subpoena_category ='41' then '代收貨款' 
	                                 when A.subpoena_category ='25' then '到付追加' 
	                                 end subpoena_category, 
                                CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
                                A.collection_money,
                                A.time_period,
                                A.invoice_memo, 
                                A.invoice_desc,
                                A.customer_code , 
                                C.customer_shortname ,
                                A.send_contact,
                                A.send_tel,
                                A.send_city+ A.send_area + A.send_address  sendAddress,
                                A.round_trip,
                                D.station_scode, D.station_name,
                                IsNULL(C.delivery_Type,0) delivery_Type,
                                A.pieces,A.ArticleNumber, A.SendPlatform, A.ArticleName,A.cbmWeight, A.order_number
                                from tcDeliveryRequests A WITH(Nolock)
                                left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                left join tbCustomers C on C.customer_code = A.customer_code
								left join tbStation D on D.station_scode = A.area_arrive_code
                                where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate())
                                {0}
								order by  A.check_number", wherestr);
        dt = dbAdapter.getDataTable(cmd);

        return dt;
    }

    /// <summary>
    /// 列印標籤(捲筒列印)(依計價模式為列印張數單位)
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReelLabel(string print_requestid, string label_type = "LTReelLabel")
    {
        //rdlc 公版:"LTReelLabel"
        if (print_requestid != "")
        {
            using (DataTable dt = getPrintDT(print_requestid))
            {
                if (dt != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = dt.Clone();  //複製DT的結構

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        int pieces = dt.Rows[i]["pieces"] == DBNull.Value ? 1 : Convert.ToInt32(dt.Rows[i]["pieces"]);
                        dt.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["check_numberbarcode"].ToString()));
                        string check_number = dt.Rows[i]["check_number"].ToString();

                        for (int j = 1; j <= pieces; j++)
                        {
                            string check_number2 = check_number + j.ToString().PadLeft(3, '0');
                            dt.Rows[i]["check_number_SB"] = check_number2;
                            dt.Rows[i]["check_numberbarcode_SB"] = Convert.ToBase64String(MakeBarcodeImage(check_number2, false));
                            dt2.ImportRow(dt.Rows[i]);

                        }
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];
                    //PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "LTReelLabel", ParName, ParValue, "TL1", dt2);
                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", label_type, ParName, ParValue, "TL1", dt2);
                }
            }
        }
    }

    public byte[] MakeBarcodeImage(string datastring, bool printnumber = true)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring, printnumber);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content, bool printnumber)
    {

        using (var barcode = new Barcode()
        {

            IncludeLabel = printnumber,
            Alignment = AlignmentPositions.CENTER,
            Width = 800,
            Height = 100,
            LabelFont = new Font("verdana", 20f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            return barcode.Encode(TYPE.CODE39, content);
        }
    }



    protected void btPrint_Click(object sender, EventArgs e)
    {
        string check_number = string.Empty;
        string print_requestid = "";
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                Boolean Update = false;
                string print_date = ((Label)New_List.Items[i].FindControl("lbprint_date")).Text;
                string Supplier_date = ((Label)New_List.Items[i].FindControl("lbSupplier_date")).Text;
                string arrive_assign_date = ((HiddenField)New_List.Items[i].FindControl("Hid_arrive_assign_date")).Value;
                string request_id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));

                if ((chkRow != null) && (chkRow.Checked) && (lbcheck_number.Text != ""))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    Label lbprint_date = ((Label)New_List.Items[i].FindControl("lbprint_date"));

                    if (lbprint_date.Text == "")
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            #region 發送日期
                            if (print_date == "")
                            {
                                cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日                                
                                Update = true;
                            }
                            #endregion

                            #region  配送日期
                            if (Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(print_date);

                                if (arrive_assign_date != "")
                                {
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                }
                                else
                                {
                                    //配送日自動產生為 D+1
                                    supplier_date = supplier_date.AddDays(1);
                                }


                                cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            #endregion
                            if (Update == true)
                            {
                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                                dbAdapter.execNonQuery(cmd);
                            }
                        }
                    }


                    check_number += "'" + lbcheck_number.Text + "',";
                    print_requestid += request_id.ToString() + ",";

                }

            }
        }
        if (check_number != "") check_number = check_number.Substring(0, check_number.Length - 1);
        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);
        NameValueCollection nvcParamters = new NameValueCollection();
        nvcParamters["ids"] = print_requestid;

        if (check_number == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的託運單');</script>", false);
            return;
        }
        else
        {
            if (ExcelFile.Checked == true)
            {
                DBToExcel(check_number);
            }

            else if (PDFFile.Checked == true)
            {

                string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintConsignmentSummaryPDFByIds";

                string strForm = PreparePrintDeliverySummaryCommitForm(url, nvcParamters);

                //Page.Controls.Add(new LiteralControl(strForm));
                Response.Clear();

                Response.Write(strForm);
                Response.End();
            }
        }

    }

    protected void DBToExcel(string check_number)
    {

        String str_retuenMsg = string.Empty;
        try
        {
            Boolean IsChkOk = false;
            if (check_number.Length > 0) IsChkOk = true;

            #region 撈DB寫入EXCEL
            if (IsChkOk)
            {
                string sheet_title = "FSE託運總表";
                string file_name = "DeliveryReport" + DateTime.Now.ToString("yyyyMMdd");
                int totalcount = 0;
                int totalpieces = 0;

                //從網址下載Html字串
                //Context.Items["TextData"] = check_number + ";" + cdate1.Text + ";" + cdate2.Text + ";";
                //Server.Transfer("Default3.aspx", true);
                //byte[] pdfFile = this.ConvertHtmlTextToPDF(htmlText);


                //Response.Clear();
                //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                //Response.ContentType = "Application/pdf";
                //Response.AddHeader("Content-Disposition", string.Format("Attachment;FileName={0}.pdf", file_name));
                //Response.BinaryWrite(pdfFile);
                //Response.Flush();
                //Response.End();
                using (SqlCommand cmd = new SqlCommand())
                {
                    if (check_number != "")
                    {
                        // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                        string wherestr = " and check_number in(" + check_number + ")";
                        string orderby = " order by  A.check_number";

                        cmd.CommandText = string.Format(@"Select
                                                          ROW_NUMBER() OVER(ORDER BY A.check_number) '序號',
                                                          A.order_number '訂單編號' , 
                                                          A.check_number '貨號' , 
                                                          A.receive_contact '收件人',  
                                                          ISNULL(A.pieces,0) '件數',
                            A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
                                                          E.station_name  '到著站',
                                                          A.receive_tel1 '收件人電話',  
                            A.collection_money '代收金額',
                                                          B.code_name '付款別',
                                                          A.SendPlatform '平臺名稱',
                                                          A.ArticleName '商品名稱',
                                                          A.invoice_desc '備註'  
                                                          from tcDeliveryRequests A with(nolock)
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          Left join tbSuppliers D with(nolock) on A.area_arrive_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                           Left join tbStation E with(nolock) on A.area_arrive_code = E.station_scode
                                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {




                                for (int i = 0; i < dt.Rows.Count; i++)
                                {

                                    totalpieces = totalpieces + Convert.ToInt32(dt.Rows[i]["件數"].ToString());
                                }
                                int rowIndex = 0;
                                ISheet sheet = null;
                                totalcount = dt.Rows.Count;
                                MemoryStream ms = new MemoryStream();
                                //HSSFWorkbook workbook = new HSSFWorkbook(); // .xls
                                IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）

                                IRow headerRow = null;

                                if (string.IsNullOrEmpty(sheet_title))
                                {
                                    //沒有Table Name以預設方式命名Sheet
                                    sheet = workbook.CreateSheet();
                                }
                                else
                                {
                                    //有Table Name以Table Name命名Sheet
                                    sheet = workbook.CreateSheet(sheet_title);
                                }

                                ICellStyle style = workbook.CreateCellStyle();
                                //style.Alignment = HorizontalAlignment.Center;

                                var font1 = workbook.CreateFont();
                                font1.Boldweight = 30;
                                font1.FontHeightInPoints = 24;
                                style.SetFont(font1);

                                IRow titletop = sheet.CreateRow(rowIndex++);
                                ICell cell = sheet.CreateRow(0).CreateCell(0);
                                cell.SetCellValue("FSE託運總表");
                                cell.CellStyle = style;

                                IRow dataRowDate = sheet.CreateRow(rowIndex++);
                                dataRowDate.CreateCell(0).SetCellValue("發送日期");
                                dataRowDate.CreateCell(1).SetCellValue(date1.Text + "~" + date2.Text);


                                if (Customer.SelectedValue != "")
                                {

                                    using (SqlCommand cmda = new SqlCommand())
                                    {
                                        DataTable dta = new DataTable();
                                        cmda.Parameters.AddWithValue("@master_code", Customer.SelectedValue);
                                        cmda.CommandText = @" WITH cus AS(
                                                        SELECT *,
                                                        ROW_NUMBER() OVER(PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                                        FROM tbCustomers with(nolock) where 1 = 1 and supplier_code <> '001' and stop_shipping_code = '0' and master_code=@master_code
                                                        )
                                                        SELECT customer_name
                                                        FROM cus
                                                        WHERE rn = 1";

                                        dta = dbAdapter.getDataTable(cmda);
                                        if (dta.Rows.Count > 0)
                                        {
                                            IRow dataRowCus = sheet.CreateRow(rowIndex++);
                                            dataRowCus.CreateCell(0).SetCellValue("寄件人");
                                            dataRowCus.CreateCell(1).SetCellValue(dta.Rows[0]["customer_name"].ToString());
                                        }
                                    }


                                }



                                rowIndex++;




                                headerRow = sheet.CreateRow(rowIndex++);

                                //自動換行
                                //csHeader.WrapText = true;

                                #region 處理標題列
                                foreach (DataColumn column in dt.Columns)
                                {
                                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                                }
                                #endregion

                                #region 處理資料列
                                foreach (DataRow row in dt.Rows)
                                {
                                    IRow dataRow = sheet.CreateRow(rowIndex++);

                                    foreach (DataColumn column in dt.Columns)
                                    {


                                        dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));

                                    }
                                }
                                IRow dataRowA = sheet.CreateRow(rowIndex + 1);



                                dataRowA.CreateCell(1).SetCellValue(Convert.ToString("總計"));
                                dataRowA.CreateCell(2).SetCellValue(Convert.ToString("共" + totalcount + "筆"));
                                dataRowA.CreateCell(3).SetCellValue(Convert.ToString("共" + totalpieces + "件"));


                                sheet.SetColumnWidth(0, 15 * 256);//序號寬度
                                sheet.SetColumnWidth(1, 30 * 256);//訂單編號寬度
                                sheet.SetColumnWidth(2, 30 * 256);//貨號寬度
                                sheet.SetColumnWidth(3, 30 * 256);//收件人寬度
                                sheet.SetColumnWidth(4, 10 * 256);//件數寬度
                                sheet.SetColumnWidth(5, 100 * 256);//地址寬度
                                sheet.SetColumnWidth(6, 25 * 256);//到著站寬度
                                sheet.SetColumnWidth(7, 30 * 256);//收件人電話寬度
                                sheet.SetColumnWidth(8, 10 * 256);//代收金額寬度
                                sheet.SetColumnWidth(9, 10 * 256);//付款別寬度
                                sheet.SetColumnWidth(10, 30 * 256);//平臺名稱寬度
                                sheet.SetColumnWidth(11, 30 * 256);//商品名稱寬度
                                sheet.SetColumnWidth(12, 60 * 256);//備註寬度


                                #endregion

                                workbook.Write(ms);
                                Response.Clear();
                                //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx", file_name));
                                Response.BinaryWrite(ms.ToArray());
                                Response.Flush();
                                Response.End();
                                ms.Close();
                                ms.Dispose();
                            }
                            else
                            {
                                str_retuenMsg += "查無此資訊，請重新確認!";
                            }

                            //lbMsg.
                        }
                    }



                }

            }
            #endregion

        }
        catch (Exception ex)
        {
            str_retuenMsg += "系統異常，請洽相關人員!";
            str_retuenMsg += " (" + ex.Message.ToString() + ")";
        }
        finally
        {
            lbMsg.Items.Add(str_retuenMsg);

        }

    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue == "未列印")
        {
            lbDate.Text = "建檔日期";
            cdate1.Visible = true;
            cdate2.Visible = true;
            date1.Visible = false;
            date2.Visible = false;
        }
        else
        {
            lbDate.Text = "發送日期";
            cdate1.Visible = false;
            cdate2.Visible = false;
            date1.Visible = true;
            date2.Visible = true;
        }
        // string sScript = "$('.date_picker').datepicker();  ";
        // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {

        string station = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    //0: 系統管理者  1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營
        //    Customer.DataSource = Utility.getCustomerDT(Suppliers.SelectedValue, (manager_type == "3" || manager_type == "5") ? Master_code : "");
        //    Customer.DataValueField = "supplier_id";
        //    Customer.DataTextField = "showname";
        //    Customer.DataBind();
        //}

        using (SqlCommand cmd = new SqlCommand())
        {
            string stationList = "";
            string wherestr = string.Empty;
            if (Suppliers.SelectedValue != "")
            {
                
                    cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                    wherestr = "  and station_scode=@Suppliers";
 
            }
            else {

                cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                if (station_level.Equals("1"))
                {
                    cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            stationList += "'";
                            stationList += dt.Rows[i]["station_scode"].ToString();
                            stationList += "'";
                            stationList += ",";
                        }
                        stationList = stationList.Remove(stationList.ToString().LastIndexOf(','), 1);
                    }
                    wherestr = "  and station_scode in (" + stationList + ")";

                }
                else if (station_level.Equals("2"))
                {
                    cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            stationList += "'";
                            stationList += dt.Rows[i]["station_scode"].ToString();
                            stationList += "'";
                            stationList += ",";
                        }
                        stationList = stationList.Remove(stationList.ToString().LastIndexOf(','), 1);
                    }
                    wherestr = "  and station_scode in (" + stationList + ")";
                }
                else if (station_level.Equals("4"))
                {
                    cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            stationList += "'";
                            stationList += dt.Rows[i]["station_scode"].ToString();
                            stationList += "'";
                            stationList += ",";
                        }
                        stationList = stationList.Remove(stationList.ToString().LastIndexOf(','), 1);
                    }
                    wherestr = "  and station_scode in (" + stationList + ")";

                }
                else if (station_level.Equals("5"))
                {
                    cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                    wherestr = "  and station_scode=@Suppliers";

                }
                else
                {
                    cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                    wherestr = "  and station_scode=@Suppliers";
                }
            }
            

            if (manager_type == "5")
            {
                string customer_code = Session["customer_code"].ToString();
                if (customer_code.Length >= 9 && customer_code.Substring(7, 2) == "00")
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code.Substring(0, 7));
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                }
                wherestr += "  and customer_code like ''+ @customer_code + '%'";

            }

            cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code+ second_id ORDER BY master_code+ second_id DESC) AS rn
                                               FROM tbCustomers with(nolock) where 1=1 and supplier_code  like'F%' and len(customer_code)>10 and stop_shipping_code = '0'  {0}
                                            )
                                            SELECT supplier_id, master_code, master_code + second_id  'customer_code2',  customer_name , supplier_id+second_id  + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 ", wherestr);
            Customer.DataSource = dbAdapter.getDataTable(cmd);
            Customer.DataValueField = "customer_code2";
            Customer.DataTextField = "showname";
            Customer.DataBind();
        }

        if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
        {
            Customer.Items.Insert(0, new ListItem("全選", ""));
        }

    }


    public void ShowLocalReport_PDFLT(Page pg, string ReportPath, string ReportName, string[] ParsName, string[] ParsValue, string reportSourceName, DataSet ds)
    {
        Microsoft.Reporting.WebForms.ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();

        rv.LocalReport.ReportPath = string.Format(@"{0}\{1}.rdlc", ReportPath, ReportName);
        Microsoft.Reporting.WebForms.ReportParameter[] parameters = new Microsoft.Reporting.WebForms.ReportParameter[ParsName.Length];
        for (int i = 0; i < ParsName.Length; i++)
        {
            parameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(ParsName[i].ToString(), ParsValue[i].ToString());
        }
        rv.LocalReport.SetParameters(parameters);
        rv.LocalReport.DataSources.Clear();
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL1", ds.Tables[0]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL2", ds.Tables[1]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL3", ds.Tables[2]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL4", ds.Tables[3]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL5", ds.Tables[4]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL6", ds.Tables[5]));


        Microsoft.Reporting.WebForms.Warning[] tWarnings;
        string[] tStreamids;
        string tMimeType;
        string tEncoding;
        string tExtension;

        //呼叫ReportViewer.LoadReport的Render function，將資料轉成想要轉換的格式，並產生成Byte資料  

        byte[] tBytes = rv.LocalReport.Render("PDF", null, out tMimeType, out tEncoding, out tExtension, out tStreamids, out tWarnings);
        string ss = HttpUtility.UrlEncode(ReportName + ".pdf", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client  

        pg.Response.Clear();
        pg.Response.ContentType = tMimeType;
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));


        pg.Response.BinaryWrite(tBytes);
        pg.Response.End();

        String ED = DateTime.Today.ToString("yyyy/MM/dd HH:mm:ss");
    }

    /// <summary>
    /// This method prepares an Html form which holds all data
    /// in hidden field in the addetion to form submitting script.
    /// </summary>
    /// <param name="url">The destination Url to which the post and redirection
    /// will occur, the Url can be in the same App or ouside the App.</param>
    /// <param name="data">A collection of data that
    /// will be posted to the destination Url.</param>
    /// <returns>Returns a string representation of the Posting form.</returns>
    /// <Author>Samer Abu Rabie</Author>

    private static String PreparePOSTForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    protected void btnAskForTakePackage_Click(object sender, EventArgs e)
    {
        string customerCode;

        if (Session["manager_type"].ToString() == "5")
            customerCode = Session["account_code"].ToString();
        else
            customerCode = customerDropDown.SelectedValue;

        string url = "LT_transport_1_3_1.aspx?customerCode=" + customerCode + "&urlUserComeFrom=" + Request.Url.LocalPath.Trim('/');
        Response.Redirect(url);
    }

    private static String PreparePrintDeliverySummaryCommitForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }
}

