﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CustomerService : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region 基本資料
            using (SqlCommand cmd = new SqlCommand())
            {
                
                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmd.CommandText = " select * from tbAccounts where account_code = @account_code  ";
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lbitem.Text = dt.Rows[0]["manager_unit"].ToString();         //管理單位
                        lbitem_employee.Text = dt.Rows[0]["emp_code"].ToString();    //人　　員
                        lbaccount_code.Text = dt.Rows[0]["account_code"].ToString(); //帳　　號
                        lbaccount_name.Text = dt.Rows[0]["user_name"].ToString();    //名　　稱
                        email.Text = dt.Rows[0]["user_email"].ToString();            //email
                        job_title.Text = dt.Rows[0]["job_title"].ToString();         //職　　稱
                    }
                    
                }
            }
                
            #endregion

        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (DataCheck() == true)
        {
            try
            {
                #region  比對輸入的密碼是否舊密碼正確
                if (rdchangepwd.SelectedValue == "1")
                {
                    MD5 md5 = MD5.Create();//建立一個MD5
                    string str_OldPassword_md5 = Utility.GetMd5Hash(md5, account_password.Text);
                  

                    String strSQL = "SELECT COUNT(1) FROM tbAccounts With(Nolock) WHERE account_code ='" + Session["account_code"].ToString() + "' AND password = N'" + str_OldPassword_md5 + "' ";
                    Boolean Is_OldPasswordOK = dbAdapter.SQLFuncNum(strSQL) >= 1 ? true : false;

                    if (!Is_OldPasswordOK)
                    {
                        string JStr = "";
                        JStr += "$('.td_password').removeClass('hide');";                      
                        JStr += "alert('原密碼不正確，請重新確認!');";
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                        return;
                    }
                }
                #endregion


                #region 修改資料
                int i_Success = 0;
                using (SqlCommand cmdupd = new SqlCommand())
                {
                    cmdupd.Parameters.AddWithValue("@user_email", email.Text.Trim());
                    cmdupd.Parameters.AddWithValue("@job_title", job_title.Text.Trim());
                    if (rdchangepwd.SelectedValue == "1")
                    {
                        MD5 md5 = MD5.Create();//建立一個MD5
                        string str_NewPassword_md5 = Utility.GetMd5Hash(md5, account_password_new.Text);
                        cmdupd.Parameters.AddWithValue("@password", str_NewPassword_md5);
                    }
                    cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);
                    cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"].ToString());
                    cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_code", Session["account_code"].ToString());
                    cmdupd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmdupd);   //修改

                    dbAdapter.getScalarBySQL(cmdupd);
                    string JStr = "alert('修改成功！');";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                }

               
                #endregion
            }
            catch (Exception ex)
            {
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "CustomerService" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                string JStr = "alert('執行異常!" + ex.ToString() + "');";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                return;
            }
        }
    }

    /// <summary>
    /// 資料驗證
    /// </summary>
    /// <returns></returns>
    public Boolean DataCheck()
    {
        Boolean value = true;
        int i = 0;
        lberrormsg.Text = "";

        string JStr = "";

        account_password_new.Attributes.Add("value", account_password_new.Text);
        account_password.Attributes.Add("value", account_password.Text);


        //職稱
        if (string.IsNullOrEmpty(job_title.Text))
        {
            i += 1;
            lberrormsg.Text += "<br>" + i + ".請輸入職稱";
            value = false;
        }

        //修改密碼
        if (rdchangepwd.SelectedValue =="1")
        {
            //新密碼
            if (account_password_new.Text.Length < 8 || PWDCheck(account_password_new.Text) == false)
            {
                i += 1;
                lberrormsg.Text += "<br>" + i + ".密碼至少需為8碼有英文大小寫+數字";
                value = false;
            }

            //原密碼
            if (string.IsNullOrEmpty(account_password.Text))
            {
                i += 1;
                lberrormsg.Text += "<br>" + i + ".請輸入原密碼";
                value = false;
            }

        }

        if (value == false)
        {
            //錯誤資料
            JStr = "$('.alert').removeClass('hide');";

            if (rdchangepwd.SelectedValue == "1")
            {
                JStr += "$('.td_password').removeClass('hide');";
            }

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>"+ JStr + "</script>", false);
        }

        return value;
    }

    /// <summary>
    /// 密碼驗證邏輯
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public Boolean PWDCheck(string password)
    {
        Boolean ret = true;

        int pwdnum = 0;
        int pwdeng = 0;
        int pwdspc = 0;
        for (int s = 0; s < password.Length; s++)
        {
            //數字0~9
            if ((Utility.getAsciiCode(password.Substring(s, 1)) >= 48) && (Utility.getAsciiCode(password.Substring(s, 1)) <= 57))
            {
                pwdnum = 1;
            }
            else if (((Utility.getAsciiCode(password.Substring(s, 1)) >= 65) && (Utility.getAsciiCode(password.Substring(s, 1)) <= 90))
                || ((Utility.getAsciiCode(password.Substring(s, 1)) >= 97) && (Utility.getAsciiCode(password.Substring(s, 1)) <= 122)))
            {
                //英文大小寫 ..A為65，a為97   大寫和小寫差32
                pwdeng = 1;
            }

        }
        //if (pwdnum + pwdeng + pwdspc < 3)
        if (pwdnum + pwdeng + pwdspc < 2)
        {
            ret = false;
        }

        return ret;
    }
}
