﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class store1_2 : System.Web.UI.Page
{
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region 基本資料
            using (SqlCommand cmd = new SqlCommand())
            {
                
                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmd.CommandText = " select * from tbAccounts where account_code = @account_code  ";
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lbitem.Text = dt.Rows[0]["manager_unit"].ToString();         //管理單位
                        lbitem_employee.Text = dt.Rows[0]["emp_code"].ToString();    //人　　員
                        lbaccount_code.Text = dt.Rows[0]["account_code"].ToString(); //帳　　號
                        lbaccount_name.Text = dt.Rows[0]["user_name"].ToString();    //名　　稱
                    }
                    
                }
            }

            #endregion

            readdata();
        }
    }

    private void readdata()
    {

        SqlCommand objCommand = new SqlCommand();

        objCommand.CommandText = string.Format(@"Select A.* , D.user_name , A.udate
                                                 From ttItemCodesFieldArea A with(nolock) 
                                                 Left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                 where A.active_flag = 1 order by A.seq");

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
    }

    //protected void btnsave_Click(object sender, EventArgs e)
    //{
    //    var CodeNameStr = Func.GetRow("code_name", "ttItemCodesFieldArea", "", "code_name=@code_name", "code_name", code_name.Text.ToString().Trim());
    //    var code_idStr = code_id.Value.ToString().Trim();
    //    string JStr = "";
    //    if (!string.IsNullOrEmpty(CodeNameStr)&&string.IsNullOrEmpty(code_idStr))
    //    {
    //        JStr = "alert('場區名稱已存在!');";
    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
    //        return;
    //    }
    //    else
    //    {
    //        try
    //        {
    //            if (string.IsNullOrEmpty(code_idStr))
    //            {
    //                using (SqlCommand cmdInsert = new SqlCommand())
    //                {
    //                    cmdInsert.Parameters.AddWithValue("@code_name", code_name.Text.ToString().Trim());
    //                    cmdInsert.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
    //                    cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
    //                    cmdInsert.CommandText = dbAdapter.SQLdosomething("ttItemCodesFieldArea", cmdInsert, "insert");
    //                    dbAdapter.execNonQuery(cmdInsert);
    //                    JStr = "alert('新增成功！');";
    //                }
    //            }
    //            else
    //            {
    //                using (SqlCommand cmdupd = new SqlCommand())
    //                {
    //                    cmdupd.Parameters.AddWithValue("@code_name", code_name.Text.ToString().Trim());
    //                    cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
    //                    cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
    //                    cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", code_idStr);
    //                    cmdupd.CommandText = dbAdapter.genUpdateComm("ttItemCodesFieldArea", cmdupd);
    //                    dbAdapter.execNonQuery(cmdupd);
    //                    JStr = "alert('修改成功！');";
    //                }
    //            }
    //                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "location.href='store1_2.aspx';</script>", false);
    //        }
    //        catch (Exception ex)
    //        {
    //            JStr = "alert('執行異常!" + ex.ToString() + "');";
    //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
    //            return;
    //        }
    //}


}





