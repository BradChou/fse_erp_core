﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LT_transport_1_3_1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string customerCode = Request.QueryString["customerCode"];
        string whereUserComeFrom = Request.QueryString["urlUserComeFrom"];
        int peices = int.Parse(Peices.Text);
        string carrier = Carrier.SelectedItem.Value;
        string pickUpType = PickUpType.SelectedItem.Value;
        string receiveTime = ReceiveTime.SelectedItem.Value;
        string note = Memo.Text;
        bool isSuccess = false;
        string assignedMd = string.Empty;

        // 取得客戶 assign_md
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select top 1 assigned_md from tbCustomers where customer_code = @customerCode";
            cmd.Parameters.AddWithValue("@customerCode", customerCode);

            assignedMd = dbAdapter.getScalarBySQL(cmd).ToString();
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"insert into pick_up_request_log (request_customer_code, pick_up_pieces, remark, 
                package_type, vechile_type, cdate, udate, pick_up_date, reassign_md)
                values (@customerCode, @pickUpPieces, @remark, @packageType, @vechileType, @cdate, @udate, @pickUpDate, @reassignMd)";

            cmd.Parameters.AddWithValue("@customerCode", customerCode);
            cmd.Parameters.AddWithValue("@pickUpPieces", peices);
            cmd.Parameters.AddWithValue("@remark", note);
            cmd.Parameters.AddWithValue("@packageType", carrier);
            cmd.Parameters.AddWithValue("@vechileType", pickUpType);
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);
            cmd.Parameters.AddWithValue("@pickUpDate", receiveTime);
            cmd.Parameters.AddWithValue("@reassignMd", assignedMd);

            int affectRow = dbAdapter.execQuery(cmd);
            if (affectRow > 0)
            {
                isSuccess = true;
            }
        }

        Session["isSuccess"] = isSuccess.ToString();

        HttpContext.Current.Response.Redirect(whereUserComeFrom, false);
    }
}