﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreportInventory.aspx.cs" Inherits="LTreportInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                            .slice(start, end).show();
                    }
                });
            });
        });


        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align: left;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align: left;
        }
    </style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">站所盤點</h2>
            <hr />
            <div class="form-group form-inline">
                <label>站所：</label>
                <span class="text-danger">
                    <asp:Label ID="lbSuppliers" runat="server"></asp:Label></span>
                <label>庫存總筆數：</label>
                <span class="text-danger">
                    <asp:Label ID="totle" runat="server"></asp:Label></span>
            </div>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>盤點日期：</label>
                    <asp:TextBox ID="date1" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    <label>站所：</label>
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="form-group form-inline">
                    <asp:Button ID="btnQry" CssClass="btn btn-primary btn_view" runat="server" Text="查　詢" OnClick="btnQry_Click" />
                    <asp:Button ID="btnExcel" CssClass="btn btn-warning" runat="server" Text="匯　出" OnClick="btnExcel_Click" />
                </div>
            </div>
            <div style="overflow: auto; height: 550px; width: 100%">
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                    <th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">留庫天數</th>
                    <th style="white-space: nowrap;">貨號</th>
                    <th style="white-space: nowrap;">發送日</th>
                    <th style="white-space: nowrap;">出貨日</th>
                    <th style="white-space: nowrap;">配送日</th>
                    <th style="white-space: nowrap;">盤點日</th>
                    <th style="white-space: nowrap;">傳票類別</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">貨態</th>
                    <th style="white-space: nowrap;">作業人員帳號+姓名</th>
                    <th style="white-space: nowrap;">作業站所代碼</th>
                </tr>
                    <asp:Repeater ID="New_List" runat="server" >
                    <ItemTemplate>
                        <tr class ="paginate">
                            <td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.depot_period").ToString())%></td>
                            <td style="white-space: nowrap;"  data-th="貨號" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ship_date","{0:yyyy/MM/dd}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.delivery_date","{0:yyyy/MM/dd}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="掃讀日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scanning_dt","{0:yyyy/MM/dd}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="最新貨態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_state").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業人員帳號+姓名"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="作業站所代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_code").ToString())%></td>
                            </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="7" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
                </table>
            </div>
             共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              <div id="page-nav" class="page"></div>
        </div>
    </div>
</asp:Content>
