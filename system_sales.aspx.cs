﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Security.Cryptography;

public partial class system_sales : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔



            DDLSet();
            DefaultData();

        }
    }

    protected void DDLSet()
    {

        String strSQL = @"SELECT code_id id,code_name name FROM tbItemCodes item With(Nolock) WHERE item.code_sclass=N'H1' AND active_flag IN(1)";
        using (SqlCommand cmd = new SqlCommand())


            ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));
        manager_type = Session["manager_type"].ToString(); //管理單位類別  
        customer_code = Session["master_code"].ToString();
        string supplier_code = Session["master_code"].ToString();
        switch (manager_type)
        {
            case "0":
            case "1":
                customer_code = "";
                break;
            case "2":
                customer_code = "000000";
                break;
        }

    }


    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }



            #endregion


            string strWhere = "";
            strSQL = @"SELECT 
ROW_NUMBER() OVER(ORDER BY id) AS num,
CASE S.area 
WHEN '0' THEN '總部' 
WHEN '1' THEN '北區' 
WHEN '2' THEN '中區' 
WHEN '3' THEN '南區' 
WHEN '99' THEN '金來'
ELSE '其他' END area,
level_name as type,
name, tel, id, email,
CASE is_active 
WHEN '1' THEN '是' 
ELSE '否' END IsActive, 
cuser,  cdate
FROM tbSales S With(Nolock)
left join tbSales_BaseData BD on S.area= BD.area and S.type= BD.type
WHERE 1=1  ";
            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND name LIKE N'%'+@search+'%' ";
                cmd.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }



            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND is_active IN (" + ddl_active.SelectedValue.ToString() + ")";
            }

            #endregion

            cmd.CommandText = string.Format(strSQL + " {0} order by id", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_sales.DataSource = pagedData;
                list_sales.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "active=" + ddl_active.SelectedValue;


        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/system_sales.aspx?" + str_Query));
    }


    protected void list_sales_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("id")).Value.ToString(), out i_id)) i_id = 0;


                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }



    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢示(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        initForm();
        int i_tmp = 0;
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "營業員基本資料-新增";
                pan_edit.Visible = true;
                pan_view.Visible = false;
                lbl_id.Text = "0";
                ddl_area.SelectedValue = "";
                ddl_type.SelectedValue = "";
                ddl_sales_name.SelectedValue = "";
                email.Text = "";
                rb_Active.SelectedValue = "1";
                tel.Text = "";

                #endregion

                break;
            case 2:
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "營業員基本資料-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @" SELECT id,name,tel,email,is_active,cuser,cdate,bank,bank_branch,bank_account,account_code,idx as type, area2, account_type, driver_code 
FROM tbSales S With(Nolock) left join tbSales_BaseData BD on S.area= BD.area and S.type= BD.type
WHERE id = @id";
                        cmd.Parameters.AddWithValue("@id", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                ddl_area.SelectedValue = row["area2"].ToString().Trim();
                                ddl_type.SelectedValue = row["type"].ToString().Trim();
                                ddl_account_type.SelectedValue = row["account_type"].ToString().Trim();
                                if (row["account_type"].ToString().Trim() == "BF")
                                {
                                    ddl_sales_name.SelectedValue = row["account_code"].ToString().Trim();
                                }
                                else if (row["account_type"].ToString().Trim() == "DRIVER")
                                {
                                    ddl_sales_name.SelectedValue = row["driver_code"].ToString().Trim();
                                }
                                if (!int.TryParse(row["tel"].ToString(), out i_tmp)) i_tmp = 0;
                                tel.Text = i_tmp.ToString();

                                email.Text = row["email"].ToString().Trim();

                                rb_Active.SelectedValue = Convert.ToInt16(row["is_active"]).ToString();
                                //ddl_recommender.SelectedValue = row["supervisor"].ToString().Trim();
                                ddl_bank.SelectedValue = row["bank"].ToString().Trim();
                                bank_branch.Text = row["bank_branch"].ToString().Trim();
                                bank_account.Text = row["bank_account"].ToString().Trim();

                                lbl_id.Text = id.ToString();






                            }
                        }
                    }

                    pan_edit.Visible = true;
                    pan_view.Visible = false;


                }
                #endregion

                break;
            default:
                lbl_title.Text = "營業員基本資料";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";
        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;

        ////驗證        
        //if (new string[] { "0", "" }.Contains(tel.Text.ToString()))
        //{
        //    IsOk = false;
        //    RetuenMsg("請填寫電話!");
        //}






        if (!IsOk) return;

        #region 存檔
        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {



                cmd.Parameters.AddWithValue("@name", ddl_sales_name.SelectedItem.ToString());
                cmd.Parameters.AddWithValue("@tel", tel.Text.ToString());
                cmd.Parameters.AddWithValue("@email", email.Text.ToString());
                cmd.Parameters.AddWithValue("@bank", ddl_bank.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@bank_branch", bank_branch.Text.ToString());
                cmd.Parameters.AddWithValue("@bank_account", bank_account.Text.ToString());
                //cmd.Parameters.AddWithValue("@supervisor", ddl_recommender.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@is_active", rb_Active.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@account_code", ddl_sales_name.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@area", ddl_area.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@type", ddl_type.SelectedValue.ToString());



                if (IsEdit)
                {
                    //cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    //cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm("tbSales", cmd);
                    dbAdapter.execNonQuery(cmd);

                    SetEditOrAdd(0);
                    RetuenMsg("編輯成功!");


                }







                else
                {
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);
                    cmd.CommandText = dbAdapter.genInsertComm("tbSales", true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;



                    SetEditOrAdd(0);
                    RetuenMsg("新增成功!");
                }



            }


        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }



    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }

        return result;
    }
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }



    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "營業員基本資料";
        string file_name = "營業員基本資料" + DateTime.Now.ToString("yyyyMMdd");
        String strSQL = string.Empty;
        using (SqlCommand objCommand = new SqlCommand())
        {



            string strWhere = "";

            if (Less_than_truckload == "1")



            {
                strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY id) AS 'NO',name AS '姓名'
                             ,tel AS '電話'                             
                             ,email AS 'E-mail'
                             ,CASE is_active WHEN '1' THEN '是' ELSE '否' END AS '是否生效' 

                         FROM tbSales  With(Nolock)

                        WHERE 1=1 ";
            }

            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND name LIKE N'%'+@search+'%' ";
                objCommand.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }



            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND is_active IN (" + ddl_active.SelectedValue.ToString() + ")";
            }




            #endregion

            objCommand.CommandText = string.Format(strSQL + " {0} order by id", strWhere);


            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();


                    }

                }

            }

        }
    }

    protected void sales_name_TextChanged(object sender, EventArgs e)
    {

    }

    private void initForm()
    {
        //銀行
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select bank_code, bank_code+' '+bank_name as showname from tbBanks order by bank_code ";
            ddl_bank.DataSource = dbAdapter.getDataTable(cmd);
            ddl_bank.DataValueField = "showname";
            ddl_bank.DataTextField = "showname";
            ddl_bank.DataBind();
            ddl_bank.Items.Insert(0, new ListItem("請選擇", ""));
        }

        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = "select name,account_code from tbSales order by id ";
        //    ddl_recommender.DataSource = dbAdapter.getDataTable(cmd);
        //    ddl_recommender.DataValueField = "account_code";
        //    ddl_recommender.DataTextField = "name";
        //    ddl_recommender.DataBind();
        //    ddl_recommender.Items.Insert(0, new ListItem("請選擇", ""));
        //}

        //營業員姓名
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select account_code+' '+user_name as showname,account_code from tbAccounts　where　active_flag='1' and manager_type not in ('3','4','5') union select  driver_code + ' ' + driver_name as showname,driver_code as account_code from tbdrivers where active_flag = '1' order by account_code ";
            ddl_sales_name.DataSource = dbAdapter.getDataTable(cmd);
            ddl_sales_name.DataValueField = "account_code";
            ddl_sales_name.DataTextField = "showname";
            ddl_sales_name.DataBind();
            ddl_sales_name.Items.Insert(0, new ListItem("請選擇", ""));
        }

        //職稱
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select idx, area, type, level_name  from tbSales_BaseData where type!='0' order by idx ";
            ddl_type.DataSource = dbAdapter.getDataTable(cmd);
            ddl_type.DataValueField = "idx";
            ddl_type.DataTextField = "level_name";
            ddl_type.DataBind();
            ddl_type.Items.Insert(0, new ListItem("請選擇", ""));
        }

    }

    protected void sales_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        string sScript = "";
        cmd.Parameters.AddWithValue("@account_code", ddl_sales_name.SelectedValue.ToString());
        cmd.CommandText = " select user_email from  tbAccounts where account_code = @account_code ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            email.Text = dt.Rows[0]["user_email"].ToString();

        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        return;
    }

    protected void account_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string wherestr = string.Empty;

            if (ddl_account_type.SelectedValue == "BF")
            {
                cmd.CommandText = string.Format(@"select account_code+' '+user_name as showname,account_code from tbAccounts　where　active_flag='1' and manager_type not in ('3','4','5') 　order by account_code", wherestr);
            }
            else if (ddl_account_type.SelectedValue == "DRIVER")
            {
                cmd.CommandText = string.Format(@"select driver_code +' ' + driver_name as showname,driver_code as account_code from tbdrivers where active_flag = '1' order by account_code", wherestr);
            }
            ddl_sales_name.DataSource = dbAdapter.getDataTable(cmd);
            ddl_sales_name.DataValueField = "account_code";
            ddl_sales_name.DataTextField = "showname";
            ddl_sales_name.DataBind();
            ddl_sales_name.Items.Insert(0, new ListItem("請選擇", ""));

        }
    }


}
