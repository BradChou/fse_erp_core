﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fileupload.aspx.cs" Inherits="fileupload" %>

<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <title>峻富雲端物流管理-上傳檔案</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
   
</head>
<body>
    <form id="form1" runat="server">
    <div >
    <table class="table">
        <tr>

            <th >上傳檔案
            </th>
            <td >
                <asp:FileUpload ID="file01" runat="server" Width="300px" />
                
            </td>
        </tr>
    </table>
        <p >
            <asp:HiddenField ID="hidf_id" runat="server" />
            <asp:Button ID="btnsave" CssClass="btn btn-primary" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click"  />
            <asp:Button ID="btncancel" CssClass="btn btn-default" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
        </p>
    </div>
    </form>
</body>
</html>
