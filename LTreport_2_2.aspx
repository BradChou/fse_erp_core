﻿<%@ Page Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreport_2_2.aspx.cs" Inherits="LTreport_2_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
       input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
        ._hide{
            display:none;
            visibility:hidden;
        }

        ._table, .wizard {
            width: 100%;
        }

        .table th {
            text-align: center;
        }

        ._li1 {
            width: 60%;
        }

        ._li2 {
            width: 35%;
        }

        .tb_search {
            width: 40% !important;
        }

        .date_picker {
            width: 110px !important;
        }

        ._ddl {
            min-width: 120px;
        }

        ._data {
            margin: 0px 5px;
            padding: 3px;
        }

        ._title {
            width: 5%;
        }

        ._data1 {
            width: 40%;
        }

        ._data2 {
            width: 25%;
        }

        ._data3 {
            width: 33%;
            padding: 0px 2%;
        }

        .btn {
            margin: 0px 10px;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
        ._unit1,._unit2{
            cursor:help;
            padding:2px;
        }
        .td_ckb{
            width:65px;
        }
        .table_list tr:hover {
            background-color: lightyellow;
        }
        ._td_sender,.td_receiver{
            text-align:left;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
        function SetChk()
        {
            var _ckall = $("._ckb_all:checked").length;
            if (_ckall == 0) {
                $("._ckb_item").prop("checked", false);
            } else {
                $("._ckb_item").prop("checked", true);
            }
            SetPrintActive();
        }

        $(document).on("click", "._ckb_item", function () {           
            SetPrintActive();
        });

        $(document).on("click", ".btn_print", function () {
            var _ckall = $("._ckb_all:checked").length;
            var _ck = $("._ckb_item:checked").length;
            var _return = false;
            if ((_ckall + _ck) == 0) {
                //alert("請勾選欲列印的項目!");                
            }
            else {
                var _result;
                if (_ckall == 0) {
                    _result = $("._ckb_item:checked").map(function () { return $(this).attr("_ckv"); }).get().join(',')

                } else {
                    _result = "0";
                }
                if (_result.length > 0) {
                    $("._tb_ckeck").val(_result).attr("value", _result);
                    _return = true;
                }
            }
            return _return;    
        });


        //$(document).on("click", ".btn_Excel", function () {
        //    var _ckall = $("._ckb_all:checked").length;
        //    var _ck = $("._ckb_item:checked").length;
        //    var _return = false;
        //    if ((_ckall + _ck) == 0) {
        //        alert("請勾選欲列印的項目!");                
        //    }
        //    else {
        //        var _result;
        //        if (_ckall == 0) {
        //            _result = $("._ckb_item:checked").map(function () { return $(this).attr("_ckv"); }).get().join(',')

        //        } else {
        //            _result = "0";
        //        }
        //        if (_result.length > 0) {
        //            $("._tb_ckeck").val(_result).attr("value", _result);
        //            _return = true;
        //        }
        //    }
        //    return _return;    
        //});

        function SetPrintActive() {
            if ($("._ckb_item:checked").length > 0 ) {
                $(".btn_print").removeAttr('disabled');  
                if (!$("._li2").hasClass("current")) {
                    $("._li2").addClass("current");
                }
            } else {
                $(".btn_print").attr('disabled', 'disabled');
                $("._li2").removeClass("current");
            }
        }

        
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">配送簽單列印</h2>
            <div class="form-style-10">
                <!-- 流程 -->
                <ul class="wizard">
                    <li class="current _li1">1. 查 詢 簽 收 單</li>
                    <li class="_li2">2. 選 取 列 印 明 細</li>
                </ul>

                <table class="_table form-group form-inline">
                    <tr>
                        <td class="_data _data1">
                            <asp:TextBox ID="tb_search" placeholder="ex:MD 工號" runat="server" AutoPostBack="true" OnTextChanged="tbsearch_TextChanged"  CssClass="tb_search form-control" MaxLength="20"></asp:TextBox>
                            配送站：
                            <asp:DropDownList ID="ddl_supplyer" runat="server" CssClass="form-control _ddl" placeholder="請選擇配配送站"></asp:DropDownList>
                         <asp:DropDownList ID="sign_paper_print_flag" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="sign_paper_print_flag_SelectedIndexChanged">
                        <asp:ListItem Value= "0">未列印</asp:ListItem>                  
                        <asp:ListItem Value="1">已列印</asp:ListItem>
                             <asp:ListItem Value="2">全部</asp:ListItem>
                    </asp:DropDownList>
                        </td>
                        
                       <td class="_title">
                            <asp:Label ID="Label1" runat="server" Text="掃讀日期"></asp:Label></td>
                         <td class="_data _data2">
                            <asp:TextBox ID="sscan_date" runat="server" CssClass="date_picker form-control"    autocomplete="off" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                            <span>~</span>
                            <asp:TextBox ID="escan_date" runat="server" CssClass="date_picker form-control" autocomplete="off" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                        
                            <asp:Button ID="btn_Search" runat="server" CssClass="templatemo-blue-button btn" ValidationGroup="validate" Text="查 詢" OnClick="btn_Search_Click" />
                        </td>
                        <td class="_data _data3">
                            <span class="text-danger span_tip">※ 列印格式 : B4 (一式6筆簽收單)</span>
                            <asp:Button ID="btn_Print" runat="server" Text="列 印" CssClass="templatemo-blue-button btn btn_print" ToolTip="列　印" OnClick="btn_Print_Click" disabled="disabled" />
                            <asp:Button ID="btn_Excel" runat="server" Text="列印Excel" CssClass="templatemo-blue-button btn" ToolTip="列印Excel" OnClick="btn_Excel_Click" disabled="disabled" />
                        </td>
                    </tr>
                    <tr>
                          <td class="_title">
                              &emsp;單筆貨號：
                              <asp:TextBox ID="strCheck_number" placeholder="ex:MD 貨號" runat="server"  CssClass="tb_search form-control" MaxLength="20"></asp:TextBox>
                          </td>
                        
                    </tr>
                </table>

            </div>
            <hr />
            <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr class="tr-only-hide">
                    <th class="td_ckb">
                        <input type="checkbox" name="ckb_all" class="_ckb _ckb_all" onchange="SetChk()" /><label>全 選</label>
                    </th>
                    <th>No.</th>
                    <th>貨　號</th>
                    <th>寄件者</th>
                    <th>收件者</th>
                    <th>貨態</th>
                    <th>發送日</th>
                    <th>配送日</th>
                    <th>配達日</th>
                    <th>配達區分</th>
                    <th>代收貨款</th>
                    <th>到著站</th>
                    <th>發送站</th>
                    <th>數　量</th>
                    <th>初次列印日期</th>
                    <th>是否列印</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr class ="paginate" title='貨號: <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'>
                             <td class="td_ckb">
                             <input type="checkbox" ID="_ckb_item" class="_ckb _ckb_item" _ckv='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' name='ckb_<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>'  />                     
                            </td>
                            <td data-th="No.">
                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />                               
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rowid").ToString())%>
                               
                            </td>
                            <td data-th="貨號">
                                <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label>
                            </td>
                            <td class="_td_sender" data-th="寄件者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td class="td_receiver" data-th="收件者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="貨態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_state").ToString())%></td>
                            <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%> </td>
                            <td data-th="配送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date","{0:yyyy/MM/dd}").ToString())%></td>                                                                                 
                            <td data-th="配達日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_date","{0:yyyy/MM/dd}").ToString())%></td>                                                                                 
                            <td data-th="配達區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item","{0:yyyy/MM/dd}").ToString())%></td>                                                                                 
                            <td data-th="代收貨款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money").ToString())%></td>                                                                                 
                            <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arr").ToString())%></td>
                            <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sta_name").ToString())%></td>
                            <td data-th="數　量">
                                <span class="_unit _unit1" title="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> 件</span>
                                </span>
                            </td>
                             <td data-th="初次列印日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.FirstPrintDate","{0:yyyy/MM/dd HH:mm:ss}").ToString())%></td>
                            <td data-th="是否列印"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.IsPrint").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="14" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div id="page-nav" class="page">
            </div>
            <asp:Label runat="server" ID="rowcount" Visible="false" Text=""></asp:Label>
            <asp:TextBox ID="tb_ckeck" CssClass="_hide _tb_ckeck" runat="server"></asp:TextBox>
        </div>
    </div>
</asp:Content>
