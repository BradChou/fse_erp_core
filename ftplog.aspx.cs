﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ftplog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbcheck_number.Text = Request.QueryString["check_number"].ToString();
            string Type = Request.QueryString["Type"]!= null ? Request.QueryString["Type"].ToString() :"";
            readdata(Type);
        }
    }

    private void readdata(string Type)
    {
       
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = "";
            cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
            if (Type == "Send")
            {
                cmd.Parameters.AddWithValue("@type", "配達");
                strWhereCmd += " and type =@type";
            }
            else if (Type == "Photo")
            {
                cmd.Parameters.AddWithValue("@type", "簽單");
                strWhereCmd += " and type =@type";
            }

            cmd.CommandText = string.Format(@"Select * from tbHCTFTPLog With(Nolock) where 0 = 0 and check_number  = @check_number {0}
                                              order by logdate", strWhereCmd);

            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
            
        }           

    }

    
}