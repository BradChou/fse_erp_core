﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class LT_trace_1 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            uc_trace_customer.Title.Text = "貨號查詢";
            uc_trace_customer.DeliveryType = "";
        }
    }

    [WebMethod]
    public static string GetPelicanCheckNumber(string checkNumber)
    {
        string pelicanCheckNum;

        //string checkNumber = "";

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select top 1 pelican_check_number from Pelican_request where jf_check_number = @check_number order by cdate desc";
            cmd.Parameters.AddWithValue("@check_number", checkNumber);

            pelicanCheckNum = dbAdapter.getScalarBySQL(cmd).ToString();
        }

        return pelicanCheckNum;
    }
}