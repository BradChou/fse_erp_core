﻿using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class store1_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }
    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 初始化日期
            sdate.Text = DateTime.Now.AddDays(-1).ToString("yyy/MM/dd");
            edate.Text = DateTime.Now.ToString("yyy/MM/dd");
            #endregion

            #region 場區
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                Area.DataSource = dt;
                Area.DataValueField = "seq";
                Area.DataTextField = "code_name";
                Area.DataBind();
                Area.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 溫層
            using (SqlCommand objCommandTemp = new SqlCommand())
            {
                objCommandTemp.CommandText = string.Format(@"Select code_id , code_name  from tbItemCodes 
                                                             WHERE code_bclass = '6' and code_sclass = 'temp' and active_flag = 1");
                using (DataTable dtRoad = dbAdapter.getDataTable(objCommandTemp))
                {
                    Temperature.DataSource = dtRoad;
                    Temperature.DataValueField = "code_id";
                    Temperature.DataTextField = "code_name";
                    Temperature.DataBind();
                    Temperature.Items.Insert(0, new ListItem("請選擇", ""));
                }                
            }
            #endregion

            #region 供應商
            manager_type = Session["manager_type"].ToString();        //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier.DataSource = Utility.getSupplierDT2(supplier_code, manager_type);
            supplier.DataValueField = "supplier_no";
            supplier.DataTextField = "showname";
            supplier.DataBind();
            supplier.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion


            readdata();
        }
    }

    private void readdata()
    {

        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (car_license.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_license", car_license.Text.Trim().ToString());
            wherestr += "  and a.car_license like '%'+@car_license+'%'";
        }

        if (sdate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@sdate", sdate.Text.Trim().ToString());
            wherestr += "  and a.InDate>=@sdate";
        }

        if (edate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@edate", edate.Text.Trim().ToString());
            wherestr += "  and a.InDate<=@edate";
        }

        if (Area.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@Area", Area.SelectedValue.ToString());
            wherestr += "  and a.Area =@Area";
        }

        if (DayNight.SelectedValue.ToString() != "0")
        {
            objCommand.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue.ToString());
            wherestr += "  and a.DayNight =@DayNight";
        }

        if (Temperature.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue.ToString());
            wherestr += "  and a.Temperature =@Temperature";
        }

        if (supplier.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue.ToString());
            wherestr += "  and a.supplier_no =@supplier_no";
        }

        var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
        if (!string.IsNullOrEmpty(Str))
        {
            wherestr += " and (";
            var StrAry = Str.Split(',');
            var i = 0;
            foreach (var item2 in StrAry)
            {
                if (!string.IsNullOrEmpty(item2))
                {
                    if (i > 0)
                    {
                        wherestr += " or ";
                    }
                    objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                    wherestr += "  A.Area=@code_id" + item2;
                    i += 1;
                }
            }
            wherestr += " )";
        }


        objCommand.CommandText = string.Format(@"select A.*, B.code_name 'cartype_text', CASE WHEN A.DayNight =1 then '日' else '夜' end  'DayNigh_text',
                                                    S.supplier_name 'SupplierName', C.code_name  'CarRetailerName', D.user_name , A.udate
                                                    from ttAssetsSB A with(nolock)
                                                    left join tbItemCodes B  with(nolock) on A.cabin_type = B.code_id  and  B.code_bclass = '6' and B.code_sclass = 'cartype' and B.active_flag = 1                                 
                                                    left join tbItemCodes C  with(nolock) on A.car_retailer = C.code_id and  C.code_bclass = '6' and C.code_sclass = 'CD'  and C.active_flag = 1 
                                                    left join tbSuppliers S  with(nolock) on A.supplier_no = S.supplier_no
                                                    left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                where 1=1  {0} 
                                                order by a.InDate asc", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    
    protected void rep_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        
        if (e.CommandName == "cmdDel")
        {
            string id = e.CommandArgument.ToString();
            string ErrStr = string.Empty;
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id.ToString());
            cmd.CommandText = dbAdapter.genDeleteComm("ttAssetsSB", cmd);
            try
            {
                dbAdapter.execNonQuery(cmd);            

            }
            catch (Exception ex)
            {
                ErrStr = ex.Message;
            }

            if (ErrStr == "")
            {
                readdata();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('刪除成功');</script>", false);
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('刪除失敗：" + ErrStr + "');</script>", false);
                return;
            }
            
        }
    }

    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "超商運費";
        string file_name = "超商運費" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "";
            if(car_license.Text.ToString() != "")
        {
                objCommand.Parameters.AddWithValue("@car_license", car_license.Text.Trim().ToString());
                wherestr += "  and a.car_license like '%'+@car_license+'%'";
            }

            if (sdate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@sdate", sdate.Text.Trim().ToString());
                wherestr += "  and a.InDate>=@sdate";
            }

            if (edate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@edate", edate.Text.Trim().ToString());
                wherestr += "  and a.InDate<=@edate";
            }

            if (Area.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@Area", Area.SelectedValue.ToString());
                wherestr += "  and a.Area =@Area";
            }

            if (DayNight.SelectedValue.ToString() != "0")
            {
                objCommand.Parameters.AddWithValue("@DayNight", DayNight.SelectedValue.ToString());
                wherestr += "  and a.DayNight =@DayNight";
            }

            if (Temperature.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@Temperature", Temperature.SelectedValue.ToString());
                wherestr += "  and a.Temperature =@Temperature";
            }

            if (supplier.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@supplier_no", supplier.SelectedValue.ToString());
                wherestr += "  and a.supplier_no =@supplier_no";
            }

            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  A.Area=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }


            objCommand.CommandText = string.Format(@"select A.Items_Text '主線/爆量', A.InDate '日期', CONVERT(varchar(16), A.starttime, 120)  '出場時間' , CONVERT(varchar(16), A.endtime, 120)  '返場時間' , 
                                                    CONVERT(varchar(16), A.startstore_time, 120)  '首店時間' , CONVERT(varchar(16), A.endstore_time, 120)  '末店時間'  , CASE WHEN A.DayNight =1 then '日' else '夜' end '日/夜', A.Temperature_Text '溫層', C.code_name '車型(計價車型)', A.Stores '店數', B.code_name '場區', 
                                                    A.milage '公里', A.RequestObject '請款對象', A.Route_name '路線', A.car_license '車號',
                                                    A.driver '司機', D.code_name '車行', S.supplier_name '供應商' , A.income '承攬價' , A.expenses '發包價'
                                                    from ttAssetsSB A with(nolock)
                                                    left join ttItemCodesFieldArea B with(nolock) on A.Area = B.seq
                                                    left join tbItemCodes C  with(nolock) on A.cabin_type = C.code_id  and  C.code_bclass = '6' and C.code_sclass = 'cartype' and C.active_flag = 1 
                                                    left join tbItemCodes D  with(nolock) on A.car_retailer = D.code_id and  D.code_bclass = '6' and D.code_sclass = 'CD' and D.active_flag = 1 
                                                    left join tbSuppliers S  with(nolock) on A.supplier_no = S.supplier_no
                                                    where 1=1  {0} 
                                                    order by a.InDate asc", wherestr);            
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 2])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }
}