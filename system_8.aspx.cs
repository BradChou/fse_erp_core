﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Script.Serialization;
using System.Globalization;

public partial class system_8 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        
        pan_Calendar.Controls.Add(new LiteralControl(GetCalendarDate()));
    }

    #region Common Function

    /// <summary>取得行事曆預設月 </summary>
    /// <returns></returns>
    protected DateTime GetDefaultMM()
    {
        DateTime dt_Month = new DateTime();

        if (Request.QueryString["mm"] != null && DateTime.TryParse(string.Format("{0}/1", Request.QueryString["mm"]), out dt_Month))
        {
            startMonth.Text = Request.QueryString["mm"];
        }
        else
        {
            dt_Month = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/1"));
        }
        

        CultureInfo ci = new CultureInfo("en-US");//CultureInfo.InstalledUICulture
        //var month = DateTime.Now.ToString("MMMM", ci);
        string month = dt_Month.ToString("MMMM", ci);
        menuitem.Text = string.Format("  <span class='title2'>{0} <em>{1}</em></span>", dt_Month.ToString("yyyy/MM"), month);


        //menuitem.Text = string.Format("<span class='title1'>行事曆</span>  <span class='title2'>{0}</span>", dt_Month.ToString("yyyy/MM"));

        return dt_Month;
    }


    /// <summary>依指定時間取得已設定假日資訊</summary>
    /// <param name="DateRange">取年、月</param>
    /// <returns></returns>
    protected List<CalendarInfo> GetHolidayDataFromDB(DateTime DateRange)
    {
        List<CalendarInfo> _holidays = new List<CalendarInfo>();
        try
        {
            DateTime dt_tmp = new DateTime();
            CalendarInfo _item = new CalendarInfo();
            //,CASE WHEN(hd_date >= DATEADD(DAY,1,CONVERT(VARCHAR,GETDATE(),111))) THEN 1 ELSE 0 END AS 'CanChange'
            string strTable = "dbo.tbCalendar";
            string strSQL = @"SELECT CONVERT(VARCHAR,Date,111) 'date',Description  
                            FROM {0} With(Nolock) 
                            WHERE YEAR(Date) = @thisY AND Month(Date) = @thisM  
                            AND KindOfDay <> 'WORKDAY'
                            ORDER BY Date;";

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = string.Format(strSQL, strTable);
                cmd.Parameters.AddWithValue("@thisY", DateRange.Year.ToString());
                cmd.Parameters.AddWithValue("@thisM", DateRange.Month.ToString());
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (DateTime.TryParse(dr["date"].ToString(), out dt_tmp))
                        {
                            _item = new CalendarInfo();
                            _item.date = dt_tmp;
                            _item.Description = dr["Description"].ToString();
                            _holidays.Add(_item);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return _holidays;
    }


    protected string HolidayOfYear()
    {
        int i_year = GetDefaultMM().Year;
        int i_month = 0;
        int i_holidays = 0;
        List<HolidayOfYearInfo> _total = new List<HolidayOfYearInfo>();
        HolidayOfYearInfo _item = new HolidayOfYearInfo();
        string strSQL = @"SELECT Month(hd_date)'month',COUNT(1) 'holidays'
                            FROM dbo.Tms_tbHolidaySet With(Nolock) 
                           WHERE YEAR(hd_date) = @thisY 
                        GROUP BY Month(hd_date)";
        using (
            SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = string.Format(strSQL);
            cmd.Parameters.AddWithValue("@thisY", i_year.ToString());
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                foreach (DataRow dr in dt.Rows)
                {

                    if (int.TryParse(dr["month"].ToString(), out i_month) && int.TryParse(dr["holidays"].ToString(), out i_holidays))
                    {
                        _item = new HolidayOfYearInfo();
                        _item.month = i_month;
                        _item.holiday = i_holidays;
                        _total.Add(_item);
                    }
                }
            }
        }


        StringBuilder sb_HolidayOfYear = new StringBuilder();
        for (int i = 1; i <= 12; i++)
        {
            HolidayOfYearInfo _this = _total.AsEnumerable().Where(x => x.month.Equals(i)).FirstOrDefault();
            DateTime StartDate = Convert.ToDateTime(string.Format("{0}/{1}/1", i_year.ToString(), i.ToString()));
            DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
            if (_this != null)
            {
                _this.totalday = Convert.ToInt32(EndDate.ToString("dd"));
                _this.workday = _this.totalday - _this.holiday;
            }
            else
            {
                _this = new HolidayOfYearInfo();
                _this.totalday = Convert.ToInt32(EndDate.ToString("dd"));
                _this.holiday = 0;
                _this.workday = _this.totalday;
            }

            //組HTMTL

            sb_HolidayOfYear.Append(string.Format(@"<tr>
                        <td class='text-center'>{0}</td>
                        <td class='text-center'>{1}月</td>
                        <td class='text-center'>{2}</td>
                        <td class='text-center'>{3}</td>
                        <td class='text-center'>{4}</td>
                    </tr>"
                , i_year.ToString()
                , i.ToString()
                , _this.totalday.ToString()
                , _this.holiday.ToString()
                , _this.workday.ToString()
                , _this.creater
                , _this.createddate
                    ));
        }
        //< td >{ 5}</ td >
        //< td >{ 6}</ td >



        return sb_HolidayOfYear.ToString();

    }


    /// <summary>組行事曆內容HTML</summary>
    /// <returns></returns>
    protected string GetCalendarDate()
    {
        StringBuilder sb_Calendar = new StringBuilder();
        try
        {
            DateTime StartDate = GetDefaultMM();
            DateTime EndDate = StartDate.AddMonths(1).AddDays(-1);
            int i_WeekNum = (int)StartDate.DayOfWeek;//0:星期天;星期一 ~ 星期五→ 1~5
            int i_week = i_WeekNum > 0 ? i_WeekNum : 7;
            List<CalendarInfo> _holidays = GetHolidayDataFromDB(StartDate);//已設定的日期

            //填日曆前端空白日期
            sb_Calendar.Append("<tr class='text-center'>");
            for (int j = 1; j < i_week; j++) { sb_Calendar.Append("<td></td>"); }

            //可修改、已設定(check box)
            string str_setY_chkY = @"<div class='checkbox checkbox-danger' title='{0}' autocomplete='off'>
                                         <input id = 'ck_{1}' name='{0}' class='styled week{2} ck_day' type='checkbox' checked='checked'>
                                         <label for='ck_{1}' class='lbl_date'>{3}</label><br/>
                                         <span>{4}</span>
                                     </div>";

            //可修改、未設定(check box)
            string str_setY_chkN = @"<div class='checkbox checkbox-danger' title='{0}' autocomplete='off'>
                                         <input id = 'ck_{1}' name='{0}' class='styled week{2} ck_day' type='checkbox'>
                                         <label for='ck_{1}' class='lbl_date'>{3}</label><br/>
                                         <span >{4}</span>
                                     </div>";

            //不開放修改、已設定(icon)
            string str_setN_chkY = @"<div title='{0}'>
                                         <i class='fa fa-check-square-o' style='font-size:22px;color:#cacaca;margin-top:10px;'></i>  
                                         <label for='lbl_{1}' class='lbl_date'>{3}</label><br/>
                                         <span >{4}</span>
                                     </div>";

            //不開放修改、未設定(icon)
            string str_setN_chkN = @"<div title='{0}'>
                                         <i class='fa fa-square-o' style='font-size:22px;color:#cacaca;margin-top:10px;'></i>  
                                         <label for='lbl_{1}' class='lbl_date'>{3}</label><br/>
                                         <span >{4}</span>
                                     </div>";

            string str_tmp = string.Empty;
            bool IsMoreThanToday = false;
            //填月份@日期
            foreach (DateTime day in EachDay(StartDate, EndDate))
            {
                i_week = (int)day.DayOfWeek;
                if (i_week == 1) sb_Calendar.Append("<tr class='text-center'>");

                CalendarInfo _holiday = _holidays.AsEnumerable().Where(x => x.date.Date.Equals(day.Date)).FirstOrDefault();
                IsMoreThanToday = day.Date >= DateTime.Now.AddDays(1).Date;

                if (IsMoreThanToday)
                {
                    str_tmp = (_holiday == null) ? str_setY_chkN : str_setY_chkY;//可修改+未設定 ; 可修改+已設定
                }
                else
                {
                    str_tmp = (_holiday == null) ? str_setN_chkN : str_setN_chkY;//不可修改+未設定 ; 不可修改+已設定
                }

                sb_Calendar.Append(string.Format(@"<td>" + str_tmp + "</td>"
                                  , day.ToString("yyyy/MM/dd")
                                  , day.ToString("yyyyMMdd")
                                  , ((int)day.DayOfWeek).ToString()
                                  , Convert.ToInt16(day.ToString("dd")).ToString("D2")
                                  , _holiday != null ? _holiday.Description : ""
                                  ));

                if (i_week == 0) sb_Calendar.Append("</tr>");
            }


            //填日曆後端空白日期
            i_WeekNum = (int)EndDate.DayOfWeek;
            i_week = i_WeekNum > 0 ? i_WeekNum : 7;
            for (int j = i_week; j < 7; j++)
            {
                sb_Calendar.Append("<td></td>");
                if (j == 7) sb_Calendar.Append("</tr>");
            }

            //日期天數資訊
            lbl_ODay.Text = EndDate.Day.ToString();
            lbl_HDay.Text = _holidays.Count().ToString();
            lbl_WDay.Text = (EndDate.Day - _holidays.Count()).ToString();
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return sb_Calendar.ToString();
    }




    /// <summary>DateTime For Loop Use</summary>
    /// <param name="from"></param>
    /// <param name="thru"></param>
    /// <returns></returns>
    public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
    {
        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            yield return day;
    }

    #endregion

    #region Ajax Function

    [WebMethod(EnableSession = true)]
    public static string DateSave(string sdate, string yesdates, string nodates)
    {
        ExecInfo _exec = new ExecInfo();
        List<string> _yesdates = new List<string>();//要進DB的日期
        List<string> _nodates = new List<string>(); //要進DB的日期
        int i_thisYear = 0;//本次異動的年、月
        int i_thisMonth = 0;
        string _user_id = string.Empty;

        #region 驗證        
        if (HttpContext.Current.Session["account_code"] == null)
        {
            _exec.status = -1;
            _exec.result = "登入時間逾時，請重新登入!";
        }
        else
        {
            _user_id = HttpContext.Current.Session["account_code"].ToString();
            //日期 >當天→可變更 ; 日期<=當天→不可異動
            DateTime dt_tmp = new DateTime();
            DateTime dtMinDate = DateTime.Now;

            if (!DateTime.TryParse(sdate, out dt_tmp))
            {
                _exec.status = -1;
                _exec.result = "參數異常，請重新確認!";
            }

            i_thisYear = dt_tmp.Year;
            i_thisMonth = dt_tmp.Month;
            if (_exec.status >= 0)
            {
                List<string> _yesitems = yesdates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList<string>();
                foreach (string _item in _yesitems)
                {
                    if (!DateTime.TryParse(_item, out dt_tmp) || dt_tmp <= dtMinDate)
                    {
                        _exec.status = -1;
                        _exec.result = string.Format("日期設定範圍需大於{0}，請重新確認!", dtMinDate.ToString("yyyy/MM/dd"));
                        break;
                    }

                    if (dt_tmp.Month != i_thisMonth || dt_tmp.Year != i_thisYear)
                    {
                        _exec.status = -1;
                        _exec.result = string.Format("日期設定範圍限{0}年{1}月", i_thisYear.ToString(), i_thisMonth.ToString());
                        break;
                    }
                    _yesdates.Add(dt_tmp.ToString("yyyy/MM/dd"));
                }

                List<string> _noitems = nodates.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList<string>();
                foreach (string _item in _noitems)
                {
                    if (!DateTime.TryParse(_item, out dt_tmp) || dt_tmp <= dtMinDate)
                    {
                        _exec.status = -1;
                        _exec.result = string.Format("日期設定範圍需大於{0}，請重新確認!", dtMinDate.ToString("yyyy/MM/dd"));
                        break;
                    }

                    if (dt_tmp.Month != i_thisMonth || dt_tmp.Year != i_thisYear)
                    {
                        _exec.status = -1;
                        _exec.result = string.Format("日期設定範圍限{0}年{1}月", i_thisYear.ToString(), i_thisMonth.ToString());
                        break;
                    }
                    _nodates.Add(dt_tmp.ToString("yyyy/MM/dd"));
                }
            }
        }
        #endregion

        #region Save to DB        
        if (_exec.status >= 0)
        {
            _yesdates.Distinct();
            _nodates.Distinct();
            string strTable = "tbCalendar";
            
            int i_Success = 0;
            if (_yesdates.Count > 0)
            {
                string strUpdSQL = string.Empty;//多筆新增，組SQL
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    foreach (string _date in _yesdates)
                    {
                        i += 1;
                        cmd.Parameters.AddWithValue("@uuser" + i.ToString(), _user_id);
                        cmd.Parameters.AddWithValue("@udate" + i.ToString(), DateTime.Now);
                        cmd.Parameters.AddWithValue("@Date" + i.ToString(), _date);
                        strUpdSQL += string.Format("UPDATE {0} SET KindOfDay='HOLIDAY', uuser={1}, udate={2} WHERE Date= {3};"
                               , strTable
                               , string.Format("@uuser{0}", i.ToString())
                               , string.Format("@udate{0}", i.ToString())
                               , string.Format("@Date{0}", i.ToString())
                               );
                    }
                    cmd.CommandText = strUpdSQL;
                    i_Success = dbAdapter.execQuery(cmd);
                }
            }

            if (_nodates.Count > 0)
            {
                string strUpdSQL = string.Empty;//多筆新增，組SQL
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    foreach (string _date in _nodates)
                    {
                        i += 1;
                        cmd.Parameters.AddWithValue("@uuser" + i.ToString(), _user_id);
                        cmd.Parameters.AddWithValue("@udate" + i.ToString(), DateTime.Now);
                        cmd.Parameters.AddWithValue("@Date" + i.ToString(), _date);
                        strUpdSQL += string.Format("UPDATE {0} SET KindOfDay='WORKDAY', uuser={1}, udate={2} WHERE Date= {3};"
                               , strTable
                               , string.Format("@uuser{0}", i.ToString())
                               , string.Format("@udate{0}", i.ToString())
                               , string.Format("@Date{0}", i.ToString())
                               );
                    }
                    cmd.CommandText = strUpdSQL;
                    i_Success = dbAdapter.execQuery(cmd);
                }
            }

            if (i_Success >= 0)
            {
                _exec.result = "OK";
                _exec.status = 1;
                _exec.memo = "yes:"+ _yesdates.Count().ToString() + "; no:"+ _nodates.Count().ToString();
            }
        }
        #endregion

        return new JavaScriptSerializer().Serialize(_exec);
    }


    #endregion


    #region Common Class

    /// <summary>回傳執行訊息尚用</summary>
    public class ExecInfo
    {
        /// <summary>狀態0:尚未執行 1:成功 -1:失敗 </summary>
        public int status { get; set; }

        /// <summary>>原因:成功→OK ; 失敗→失敗原因</summary>
        public string result { get; set; }

        public string memo { get; set; }

        public ExecInfo()
        {
            status = 0;
            result = "尚未執行";
            memo = string.Empty;
        }
    }


    /// <summary>行事曆資訊</summary>
    public class CalendarInfo
    {
        /// <summary>日期</summary>
        public DateTime date { get; set; }

        public string Description { get; set; }
        ///// <summary>是否可異動</summary>
        //public bool CanChange { get; set; }
    }


    public class HolidayOfYearInfo
    {
        public int month { get; set; }

        public int totalday { get; set; }

        public int workday { get; set; }

        public int holiday { get; set; }

        public string creater { get; set; }

        public string createddate { get; set; }

        public HolidayOfYearInfo()
        {
            month = 0;
            totalday = 0;
            workday = 0;
            creater = string.Empty;
            createddate = string.Empty;
        }


    }

    #endregion
}