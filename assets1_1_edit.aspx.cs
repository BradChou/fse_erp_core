﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_1_edit : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

           

            //#region 會計科目
            //SqlCommand cmd = new SqlCommand();
            //cmd.CommandText = @"select * from tbAccountingSubject with(nolock)";
            //acntid.DataSource = dbAdapter.getDataTable(cmd);
            //acntid.DataValueField = "AcntId";
            //acntid.DataTextField = "AcntName";
            //acntid.DataBind();
            //acntid.Items.Insert(0, new ListItem("請選擇", ""));
            //#endregion

            #region 所有權
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'OS' and active_flag = 1 ";
            dlownership.DataSource = dbAdapter.getDataTable(cmd2);
            dlownership.DataValueField = "code_id";
            dlownership.DataTextField = "code_name";
            dlownership.DataBind();
            dlownership.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion
            
            //#region 營業模式
            //SqlCommand cmd3 = new SqlCommand();
            //cmd3.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'BM'";
            //dlbusiness_model.DataSource = dbAdapter.getDataTable(cmd3);
            //dlbusiness_model.DataValueField = "code_id";
            //dlbusiness_model.DataTextField = "code_name";
            //dlbusiness_model.DataBind();
            //dlbusiness_model.Items.Insert(0, new ListItem("請選擇", ""));
            //#endregion

            #region 車行
            SqlCommand cmd4 = new SqlCommand();
            cmd4.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'CD' and active_flag= 1 ";
            car_retailer.DataSource = dbAdapter.getDataTable(cmd4);
            car_retailer.DataValueField = "code_id";
            car_retailer.DataTextField = "code_name";
            car_retailer.DataBind();
            car_retailer.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 車廂型式
            SqlCommand cmd5 = new SqlCommand();
            cmd5.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'cabintype'";
            cabin_type.DataSource = dbAdapter.getDataTable(cmd5);
            cabin_type.DataValueField = "code_id";
            cabin_type.DataTextField = "code_name";
            cabin_type.DataBind();
            cabin_type.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 使用單位
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=1", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  code_id=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 {0} order by code_id", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                dept.DataSource = dt;
                dept.DataValueField = "code_id";
                dept.DataTextField = "code_name";
                dept.DataBind();
                dept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 使用單位
            //SqlCommand cmd6 = new SqlCommand();
            //cmd6.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'dept'";
            //dept.DataSource = dbAdapter.getDataTable(cmd6);
            //dept.DataValueField = "code_id";
            //dept.DataTextField = "code_name";
            //dept.DataBind();
            //dept.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 油卡
            //SqlCommand cmd7 = new SqlCommand();
            //cmd7.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'oil'";
            //cb_oil.DataSource = dbAdapter.getDataTable(cmd7);
            //cb_oil.DataValueField = "code_id";
            //cb_oil.DataTextField = "code_name";
            //cb_oil.DataBind();

            SqlCommand cmd7 = new SqlCommand();
            cmd7.CommandText = @"select *, 0 as chk, '' as card_id from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'oil'";
            rp_oil.DataSource = dbAdapter.getDataTable(cmd7);
            rp_oil.DataBind();
            #endregion

            #region GPS
            //SqlCommand cmd8 = new SqlCommand();
            //cmd8.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'GPS'";
            //cb_GPS.DataSource = dbAdapter.getDataTable(cmd8);
            //cb_GPS.DataValueField = "code_id";
            //cb_GPS.DataTextField = "code_name";
            //cb_GPS.DataBind();

            SqlCommand cmd8 = new SqlCommand();
            cmd8.CommandText = @"select *, 0 as chk from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'GPS'";
            rp_gps.DataSource = dbAdapter.getDataTable(cmd8);
            rp_gps.DataBind();
            #endregion

            #region 溫層
            SqlCommand cmd9 = new SqlCommand();
            cmd9.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass= 'temp'";
            temperate.DataSource = dbAdapter.getDataTable(cmd9);
            temperate.DataValueField = "code_id";
            temperate.DataTextField = "code_name";
            temperate.DataBind();
            temperate.Items.Insert(0, new ListItem("請選擇", ""));
            if (a_id == "")
            {
                temperate.SelectedValue = "3";
            }
            #endregion

            #region 費用類別           
            SqlCommand cmd10= new SqlCommand();
            cmd10.CommandText = "select code_id, code_name,0 as chk from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1 and code_name <> '其他'";
            rp_fee.DataSource = dbAdapter.getDataTable(cmd10);            
            rp_fee.DataBind();
            #endregion

            readdata(a_id);

        }
    }

    private void readdata(string a_id)    {

        string script = string.Empty;
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);
            
            cmd.CommandText = @"Select * from  ttAssets A with(nolock) 
                                Where a_id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);            
            if (dt.Rows.Count > 0)
            {
                //serial.Text = dt.Rows[0]["serial"].ToString().Trim();
                //improved_number.Text = dt.Rows[0]["improved_number"].ToString().Trim();
                //assets_name.Text = dt.Rows[0]["assets_name"].ToString().Trim();
                //get_date.Text = dt.Rows[0]["get_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["get_date"]).ToString("yyyy/MM/dd") :"";
                //supplier_id.Text = dt.Rows[0]["supplier_id"].ToString().Trim();
                //supplier.Text = dt.Rows[0]["supplier"].ToString().Trim();
                //location.Text = dt.Rows[0]["location"].ToString().Trim();
                //acntid.SelectedValue  = dt.Rows[0]["acntid"].ToString().Trim();
                //quant.Text = dt.Rows[0]["quant"].ToString();
                //unit.Text = dt.Rows[0]["unit"].ToString();
                //price.Text = dt.Rows[0]["get_price"].ToString();
                //residual_value.Text = dt.Rows[0]["residual_value"].ToString();
                //useful_life.Text = dt.Rows[0]["useful_life"].ToString();
                //dept_id.Text= dt.Rows[0]["dept_id"].ToString();
                //dept.Text = dt.Rows[0]["dept"].ToString();
                year.Text = dt.Rows[0]["year"].ToString().Trim();
                month.SelectedValue  = dt.Rows[0]["month"].ToString().Trim();
                brand.Text = dt.Rows[0]["brand"].ToString().Trim();
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();
                stop_type.SelectedValue = dt.Rows[0]["stop_type"].ToString().Trim();
                stop_date.Text = dt.Rows[0]["stop_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["stop_date"]).ToString("yyyy/MM/dd") : "";
                if (stop_type.SelectedValue != "0")
                {
                    script = "$('#"+ stop_date.ClientID+"').show();";
                }
                car_license.Text = dt.Rows[0]["car_license"].ToString();
                //driver.Text = dt.Rows[0]["driver"].ToString();
                tonnes.Text = dt.Rows[0]["tonnes"].ToString();
                dlownership.SelectedValue  = dt.Rows[0]["ownership"].ToString();
                //dlbusiness_model.SelectedValue = dt.Rows[0]["business_model"].ToString();
                dept.SelectedValue = dt.Rows[0]["dept"].ToString();
                car_retailer.SelectedValue = dt.Rows[0]["car_retailer"].ToString();
                owner.Text = dt.Rows[0]["owner"].ToString();
                registration.SelectedValue = dt.Rows[0]["registration"].ToString();
                engine_number.Text = dt.Rows[0]["engine_number"].ToString();
                cc.Text = dt.Rows[0]["cc"].ToString();
                age.Text = (DateTime.Now.Year - Convert.ToInt32(dt.Rows[0]["year"]) + 1).ToString();
                //DateTime zeroTime = new DateTime(1, 1, 1);
                //DateTime a = new DateTime(Convert.ToInt32(dt.Rows[0]["year"]), Convert.ToInt32(dt.Rows[0]["month"]),1);
                //DateTime b = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                //TimeSpan span = b - a;
                //// Because we start at year 1 for the Gregorian
                //// calendar, we must subtract a year here.
                //int years = (zeroTime + span).Year - 1;
                //age.Text = years.ToString();
                //age.Text = dt.Rows[0]["age"].ToString();

                tires_number.Text = dt.Rows[0]["tires_number"].ToString();
                cabin_type.SelectedValue = dt.Rows[0]["cabin_type"].ToString();
                temperate.SelectedValue = dt.Rows[0]["temperate"].ToString();
                tailgate.SelectedValue = dt.Rows[0]["tailgate"].ToString();
                //oil.SelectedValue = dt.Rows[0]["oil"].ToString();
                rb_ETC.SelectedValue =dt.Rows[0]["ETC"].ToString();
                rb_ETC_SelectedIndexChanged(null, null);
                switch (rb_ETC.SelectedValue)
                {
                    case "True":
                        ETC_Account.Text  = dt.Rows[0]["ETC_memo"].ToString();                        
                        break;
                    case "False":                        
                        dlETC0.SelectedValue = dt.Rows[0]["ETC_memo"] != DBNull.Value ? dt.Rows[0]["ETC_memo"].ToString():"";
                        break;
                }

                rb_vision_assist.SelectedValue = dt.Rows[0]["vision_assist"].ToString();
                oil_discount.Text = dt.Rows[0]["oil_discount"].ToString();
                tires_monthlyfee.Text = dt.Rows[0]["tires_monthlyfee"].ToString();
                license_tax.Text = dt.Rows[0]["license_tax"].ToString();
                fuel_tax.Text = dt.Rows[0]["fuel_tax"].ToString();
                rent.Text = dt.Rows[0]["rent"].ToString();
                next_inspection_date.Text = dt.Rows[0]["next_inspection_date"] != DBNull.Value ? Convert.ToDateTime (dt.Rows[0]["next_inspection_date"]).ToString("yyyy/MM/dd"):"";
                //purchase_date.Text = dt.Rows[0]["purchase_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["purchase_date"]).ToString("yyyy/MM/dd") : "";

                //油卡
                SqlCommand cmd2 = new SqlCommand();
                cmd2.CommandText = @"Select A.*, CASE WHEN isNULL(B.id,0)>0 then 1 else 0 end chk , B.card_id 
                                        from tbItemCodes A with(nolock) 
                                        left join ttAssetsOil B with(nolock) on A.code_id= B.oil and B.a_id = @a_id
                                        where A.code_bclass = '6' and A.code_sclass= 'oil'";
                cmd2.Parameters.AddWithValue("@a_id", a_id);
                rp_oil.DataSource = dbAdapter.getDataTable(cmd2);
                rp_oil.DataBind();

                //GPS
                SqlCommand cmd3 = new SqlCommand();
                cmd3.CommandText = @"Select A.*, CASE WHEN isNULL(B.id,0)>0 then 1 else 0 end chk 
                                        from tbItemCodes A with(nolock) 
                                        left join ttAssetsGPS B with(nolock) on A.code_id= B.GPS and B.a_id = @a_id
                                        where A.code_bclass = '6' and A.code_sclass= 'GPS'";
                cmd3.Parameters.AddWithValue("@a_id", a_id);
                rp_gps.DataSource = dbAdapter.getDataTable(cmd3);
                rp_gps.DataBind();

                //保險
                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandText = @"select A.* , B.code_name 
                                        from ttInsurance   A  with(nolock) 
                                        left join tbItemCodes B  with(nolock) on A.kind = B.code_id and B.code_bclass = '6' and B.code_sclass = 'IK'
                                        where A.assets_id = @assets_id";
                cmd4.Parameters.AddWithValue("@assets_id", a_id);
                New_List.DataSource = dbAdapter.getDataTable(cmd4);
                New_List.DataBind();

                //費用類別                
                SqlCommand cmd5 = new SqlCommand();
                cmd5.CommandText = @"SELECT a.seq, a.code_bclass, a.code_sclass, a.code_id, a.code_name, a.active_flag, a.memo, b.car_license, b.prepay, 
                                            CASE WHEN isNULL(B.prepay, 0) > 0 THEN 1 ELSE 0 END AS chk
                                     FROM   tbItemCodes AS a with(nolock) LEFT JOIN
                                            ttAssetsPrepay AS b ON a.code_id = b.fee_type AND b.car_license = @car_license
                                    WHERE   (a.code_bclass = '6') AND (a.code_sclass = 'fee_type') AND (a.active_flag = 1) AND (a.code_name <> '其他')";
                cmd5.Parameters.AddWithValue("@car_license", car_license.Text);
                rp_fee.DataSource = dbAdapter.getDataTable(cmd5);
                rp_fee.DataBind();
            }

        }
        else
        {
            ApStatus = "Add";
        }

       
        SetApStatus(ApStatus);
        if (script != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + script + "</script>", false);
        }
        
    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                car_license.ReadOnly  = true ;
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                year.Text = DateTime.Now.Year.ToString();
                //get_date.Text = DateTime.Now.ToString("yyyy/MM/dd");
                //quant.Text = "1";
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        DateTime _date;
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":
               #region 判斷重覆
                //SqlCommand cmda = new SqlCommand();
                //DataTable dta = new DataTable();
                //cmda.Parameters.AddWithValue("@serial", serial.Text);
                //cmda.Parameters.AddWithValue("@improved_number", improved_number.Text);
                //cmda.CommandText = "Select a_id from ttAssets with(nolock) where serial=@serial and improved_number=@improved_number";
                //dta = dbAdapter.getDataTable(cmda);
                //if (dta.Rows.Count > 0)
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                //    return;
                //}
                SqlCommand cmda = new SqlCommand();
                DataTable dta = new DataTable();
                cmda.Parameters.AddWithValue("@car_license", car_license.Text);                
                cmda.CommandText = "Select a_id from ttAssets with(nolock) where car_license=@car_license ";
                dta = dbAdapter.getDataTable(cmda);
                if (dta.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                    return;
                }

                #endregion

                SqlCommand cmdadd = new SqlCommand();
                //cmdadd.Parameters.AddWithValue("@serial", serial.Text);
                //cmdadd.Parameters.AddWithValue("@improved_number", improved_number.Text);
                //cmdadd.Parameters.AddWithValue("@assets_name", assets_name.Text );
                //cmdadd.Parameters.AddWithValue("@get_date", get_date.Text);
                //cmdadd.Parameters.AddWithValue("@supplier_id", supplier_id.Text);
                //cmdadd.Parameters.AddWithValue("@supplier", supplier.Text);
                //cmdadd.Parameters.AddWithValue("@dept_id", dept_id.Text);
                //cmdadd.Parameters.AddWithValue("@dept", dept.Text);
                //cmdadd.Parameters.AddWithValue("@acntid", acntid.Text);
                //cmdadd.Parameters.AddWithValue("@quant", quant.Text);
                //cmdadd.Parameters.AddWithValue("@unit", unit.Text);
                //cmdadd.Parameters.AddWithValue("@get_price", price.Text);
                //cmdadd.Parameters.AddWithValue("@residual_value", residual_value.Text);
                //cmdadd.Parameters.AddWithValue("@useful_life", useful_life.Text);
                cmdadd.Parameters.AddWithValue("@year", year.Text.ToString());
                cmdadd.Parameters.AddWithValue("@month", month.Text.ToString());
                cmdadd.Parameters.AddWithValue("@brand", brand.Text);                
                cmdadd.Parameters.AddWithValue("@memo", memo.Text);
                if (stop_type.SelectedValue != "0")
                {
                    cmdadd.Parameters.AddWithValue("@stop_type", stop_type.SelectedValue);
                    if (DateTime.TryParse(stop_date.Text, out _date))
                    {
                        cmdadd.Parameters.AddWithValue("@stop_date", stop_date.Text);
                    }
                }
                cmdadd.Parameters.AddWithValue("@car_license", car_license.Text);
                //cmdadd.Parameters.AddWithValue("@driver", driver.Text);
                cmdadd.Parameters.AddWithValue("@tonnes", tonnes.Text);
                cmdadd.Parameters.AddWithValue("@ownership", dlownership.SelectedValue );
                //cmdadd.Parameters.AddWithValue("@business_model", dlbusiness_model.SelectedValue);
                cmdadd.Parameters.AddWithValue("@dept", dept.SelectedValue );
                cmdadd.Parameters.AddWithValue("@car_retailer", car_retailer.SelectedValue);
                cmdadd.Parameters.AddWithValue("@owner", owner.Text);
                cmdadd.Parameters.AddWithValue("@registration", registration.SelectedValue);
                cmdadd.Parameters.AddWithValue("@engine_number", engine_number.Text);
                cmdadd.Parameters.AddWithValue("@cc", cc.Text);
                cmdadd.Parameters.AddWithValue("@age", age.Text);
                cmdadd.Parameters.AddWithValue("@tires_number", tires_number.Text);
                cmdadd.Parameters.AddWithValue("@cabin_type", cabin_type.SelectedValue);
                cmdadd.Parameters.AddWithValue("@temperate", temperate.SelectedValue);
                cmdadd.Parameters.AddWithValue("@tailgate", tailgate.SelectedValue);
                cmdadd.Parameters.AddWithValue("@vision_assist", rb_vision_assist.SelectedValue);
                //cmdadd.Parameters.AddWithValue("@oil", cb_oil.SelectedValue);
                //cmdadd.Parameters.AddWithValue("@gps", cb_gps.SelectedValue);
                cmdadd.Parameters.AddWithValue("@ETC", rb_ETC.SelectedValue );
                switch (rb_ETC.SelectedValue)
                {
                    case "True":
                        cmdadd.Parameters.AddWithValue("@ETC_memo", ETC_Account.Text );
                        break;
                    case "False":
                        cmdadd.Parameters.AddWithValue("@ETC_memo", dlETC0.SelectedValue );
                        break;
                }
                cmdadd.Parameters.AddWithValue("@oil_discount", oil_discount.Text);
                cmdadd.Parameters.AddWithValue("@tires_monthlyfee", tires_monthlyfee.Text);
                cmdadd.Parameters.AddWithValue("@license_tax", license_tax.Text);
                cmdadd.Parameters.AddWithValue("@fuel_tax", fuel_tax.Text);
                cmdadd.Parameters.AddWithValue("@rent", rent.Text);
                if (DateTime.TryParse(next_inspection_date.Text, out _date))
                {
                    cmdadd.Parameters.AddWithValue("@next_inspection_date", next_inspection_date.Text);
                }
                //if (DateTime.TryParse(purchase_date.Text, out _date))
                //{
                //    cmdadd.Parameters.AddWithValue("@purchase_date", purchase_date.Text);
                //}
                
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdadd.CommandText = dbAdapter.SQLdosomething("ttAssets", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }
                
                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@year", year.Text.ToString());
                cmdupd.Parameters.AddWithValue("@month", month.Text.ToString());
                //cmdupd.Parameters.AddWithValue("@assets_name", assets_name.Text);
                //cmdupd.Parameters.AddWithValue("@get_date", get_date.Text);
                //cmdupd.Parameters.AddWithValue("@supplier_id", supplier_id.Text);
                //cmdupd.Parameters.AddWithValue("@supplier", supplier.Text);
                //cmdupd.Parameters.AddWithValue("@dept_id", dept_id.Text);
                //cmdupd.Parameters.AddWithValue("@dept", dept.Text);
                //cmdupd.Parameters.AddWithValue("@acntid", acntid.Text);
                //cmdupd.Parameters.AddWithValue("@quant", quant.Text);
                //cmdupd.Parameters.AddWithValue("@unit", unit.Text);
                //cmdupd.Parameters.AddWithValue("@get_price", price.Text);
                //cmdupd.Parameters.AddWithValue("@residual_value", residual_value.Text);
                //cmdupd.Parameters.AddWithValue("@useful_life", useful_life.Text);
                cmdupd.Parameters.AddWithValue("@brand", brand.Text);
                cmdupd.Parameters.AddWithValue("@memo", memo.Text);
                if (stop_type.SelectedValue != "0")
                {
                    cmdupd.Parameters.AddWithValue("@stop_type", stop_type.SelectedValue);
                    if (DateTime.TryParse(stop_date.Text, out _date))
                    {
                        cmdupd.Parameters.AddWithValue("@stop_date", stop_date.Text);
                    }
                }
                cmdupd.Parameters.AddWithValue("@car_license", car_license.Text);
                //cmdupd.Parameters.AddWithValue("@driver", driver.Text);
                cmdupd.Parameters.AddWithValue("@tonnes", tonnes.Text);
                cmdupd.Parameters.AddWithValue("@ownership", dlownership.SelectedValue);
                //cmdupd.Parameters.AddWithValue("@business_model", dlbusiness_model.SelectedValue);
                cmdupd.Parameters.AddWithValue("@dept", dept.SelectedValue);
                cmdupd.Parameters.AddWithValue("@car_retailer", car_retailer.SelectedValue);
                cmdupd.Parameters.AddWithValue("@owner", owner.Text);
                cmdupd.Parameters.AddWithValue("@registration", registration.SelectedValue);
                cmdupd.Parameters.AddWithValue("@engine_number", engine_number.Text);
                cmdupd.Parameters.AddWithValue("@cc", cc.Text);
                cmdupd.Parameters.AddWithValue("@age", age.Text);
                cmdupd.Parameters.AddWithValue("@tires_number", tires_number.Text);
                cmdupd.Parameters.AddWithValue("@cabin_type", cabin_type.SelectedValue);
                cmdupd.Parameters.AddWithValue("@temperate", temperate.SelectedValue);
                cmdupd.Parameters.AddWithValue("@tailgate", tailgate.SelectedValue);
                cmdupd.Parameters.AddWithValue("@vision_assist", rb_vision_assist.SelectedValue);
                //cmdupd.Parameters.AddWithValue("@oil", cb_oil.SelectedValue);
                //cmdupd.Parameters.AddWithValue("@gps", cb_gps.SelectedValue);
                cmdupd.Parameters.AddWithValue("@ETC", rb_ETC.SelectedValue);
                switch (rb_ETC.SelectedValue)
                {
                    case "True":
                        cmdupd.Parameters.AddWithValue("@ETC_memo", ETC_Account.Text);
                        break;
                    case "False":
                        cmdupd.Parameters.AddWithValue("@ETC_memo", dlETC0.SelectedValue);
                        break;
                }
                cmdupd.Parameters.AddWithValue("@oil_discount", oil_discount.Text);
                cmdupd.Parameters.AddWithValue("@tires_monthlyfee", tires_monthlyfee.Text);
                cmdupd.Parameters.AddWithValue("@license_tax", license_tax.Text);
                cmdupd.Parameters.AddWithValue("@fuel_tax", fuel_tax.Text);
                cmdupd.Parameters.AddWithValue("@rent", rent.Text);
                if (DateTime.TryParse(next_inspection_date.Text, out _date))
                {
                    cmdupd.Parameters.AddWithValue("@next_inspection_date", next_inspection_date.Text);
                }
                //if (DateTime.TryParse(purchase_date.Text, out _date))
                //{
                //    cmdupd.Parameters.AddWithValue("@purchase_date", purchase_date.Text);
                //}
                

                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "a_id", a_id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttAssets", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        if (ErrStr == "")
        {
            if (ApStatus == "Modity")
            {
                SqlCommand cmddel = new SqlCommand();
                cmddel.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "a_id", a_id);
                cmddel.CommandText = dbAdapter.genDeleteComm("ttAssetsOil", cmddel) + dbAdapter.genDeleteComm("ttAssetsGPS", cmddel);
                dbAdapter.execNonQuery(cmddel);

                SqlCommand cmdde2 = new SqlCommand();
                cmdde2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "car_license", car_license.Text);
                cmdde2.CommandText = dbAdapter.genDeleteComm("ttAssetsPrepay", cmdde2);
                dbAdapter.execNonQuery(cmdde2);
            }

            foreach (RepeaterItem item in this.rp_oil.Items)
            {
                CheckBox chk = item.FindControl("chkoil") as CheckBox;
                if (chk.Checked)
                {
                    string cardid = (item.FindControl("oil_cardid") as TextBox).Text;
                    string oil = (item.FindControl("Hid_oil") as HiddenField).Value;
                    SqlCommand cmd1 = new SqlCommand();
                    cmd1.Parameters.AddWithValue("@a_id", a_id);
                    cmd1.Parameters.AddWithValue("@oil", oil);
                    cmd1.Parameters.AddWithValue("@card_id", cardid);
                    cmd1.CommandText = dbAdapter.SQLdosomething("ttAssetsOil", cmd1, "insert");
                    dbAdapter.execNonQuery(cmd1);
                }
            }

            foreach (RepeaterItem item in this.rp_gps.Items)
            {
                CheckBox chk = item.FindControl("chkgps") as CheckBox;
                if (chk.Checked)
                {   
                    string gps = (item.FindControl("Hid_gps") as HiddenField).Value;
                    SqlCommand cmd2 = new SqlCommand();
                    cmd2.Parameters.AddWithValue("@a_id", a_id);
                    cmd2.Parameters.AddWithValue("@GPS", gps);
                    cmd2.CommandText = dbAdapter.SQLdosomething("ttAssetsGPS", cmd2, "insert");
                    dbAdapter.execNonQuery(cmd2);
                }
            }

            foreach (RepeaterItem item in this.rp_fee.Items)
            {
                CheckBox chk = item.FindControl("chkfee") as CheckBox;
                if (chk.Checked)
                {
                    string fee = (item.FindControl("Hid_fee") as HiddenField).Value;
                    SqlCommand cmd3 = new SqlCommand();
                    cmd3.Parameters.AddWithValue("@car_license", car_license.Text);
                    cmd3.Parameters.AddWithValue("@fee_type", fee);
                    cmd3.Parameters.AddWithValue("@prepay", 1);
                    cmd3.CommandText = dbAdapter.SQLdosomething("ttAssetsPrepay", cmd3, "insert");
                    dbAdapter.execNonQuery(cmd3);
                }
            }


        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='assets1_1.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }



    protected void rb_ETC_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (rb_ETC.SelectedValue)
        {
            case "True":
                ETC_Account.Visible = true;
                dlETC0.Visible = false;
                break;
            case "False":
                ETC_Account.Visible = false;
                dlETC0.Visible = true;
                break;
        }
    }
}