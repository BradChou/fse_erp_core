﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterSystem.master" AutoEventWireup="true" CodeFile="LTsystem_4_4.aspx.cs" Inherits="LTsystem_4_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            //$("#resetclick").click(function () {
            //    if (confirm('您確認要重置該司機的APP密碼嗎?') == true) {
            //        $("#resetclick").fancybox({
            //            'width': 350,
            //            'height': 300,
            //            'autoScale': false,
            //            'transitionIn': 'none',
            //            'transitionOut': 'none',
            //            'type': 'iframe',
            //        });
            //    }
            //    else {
            //        return false;
            //    }
            //});
        });
        $.fancybox.update();

        function CarLocation(LastLat, LastLon) {
            window.open('http://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q=' + LastLat + ',' + LastLon + '&z=16&output=embed&t=', '_blank',
                'height=500,width=500,directories=0,location=0,menubar=0,resizable=0,status=0,titlebar=0,toolbar=0');
        }
    </script>

    <style type="text/css">
        .td_th {
            text-align: center;
        }

        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .radio {
            padding-left: 5px;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="templatemo-content-widget white-bg">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="站所維護" runat="server"></asp:Label>
        </h2>



        <asp:Panel ID="pan_view" runat="server">
            <div class="div_view">
                <!-- 查詢 -->
                <div class="form-group form-inline">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 70%;">
                                <label>查詢條件：</label>
                                <asp:TextBox ID="tb_search" runat="server" class="form-control"></asp:TextBox>
                                <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />

                            </td>
                            <td style="width: 30%; float: right;">
                                <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- 查詢 end -->
                <!--內容-list -->

                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr>
                        <th>No</th>
                        <th>站所代碼</th>
                        <th>站所簡碼</th>
                        <th>站所名稱</th>
                        <th>站所類型</th>
                        <th>電話</th>
                        <th>更新人員</th>
                        <th>更新日期</th>
                        <th>功　　能</th>
                    </tr>
                    <asp:Repeater ID="list_station" runat="server" OnItemCommand="list_customer_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                                <td data-th="站所代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_code").ToString())%></td>
                                <td data-th="站所簡碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_scode").ToString())%></td>
                                <td data-th="站所名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                                <td data-th="站所類型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_type").ToString())%></td>
                                <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tel").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td data-th="編輯">
                                    <asp:HiddenField ID="Hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                    <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" />

                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_station.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="6" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總筆數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
                <!--內容-end -->
            </div>
        </asp:Panel>

        <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
            <hr />
            <asp:Label runat="server" ID="lbl_app_login_date" Visible="False" class="text-primary"></asp:Label>
            <table class="tb_edit">
                <tr class="form-group">
                    <th class="_edit_title">
                        <label class="_tip_important">站所代碼</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:TextBox ID="station_code" runat="server" class="form-control" placeholder="ex: F01" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg1" runat="server" ControlToValidate="station_code" ForeColor="Red" ValidationGroup="validate">請輸入站所代碼</asp:RequiredFieldValidator>
                    </td>
                    <th>
                        <label class="_tip_important">站所簡碼</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="station_scode" runat="server" class="form-control" placeholder="ex: F01" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="station_scode" ForeColor="Red" ValidationGroup="validate">請輸入站所簡碼</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th class="_edit_title">
                        <label class="_tip_important">站所名稱</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:TextBox ID="station_name" runat="server" class="form-control" placeholder="ex: 台北" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="station_name" ForeColor="Red" ValidationGroup="validate">請輸入站所名稱</asp:RequiredFieldValidator>
                    </td>
                    <th>
                        <label>營業區</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="BusinessDistrict" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                    </td>

                </tr>

                <tr>
                    <th class="_edit_title">
                        <label class="_tip_important">站所類型</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="station_type" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true">
                            <asp:ListItem Value="">請選擇</asp:ListItem>
                            <asp:ListItem Value="A">直營</asp:ListItem>
                            <asp:ListItem Value="B">加盟</asp:ListItem>
                            <asp:ListItem Value="C">聯營</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <th class="_edit_title">
                        <label>電話</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="tel" runat="server" class="form-control" placeholder="ex: 02-26123456" MaxLength="15"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">市內電話</asp:RequiredFieldValidator>
                    </td>

                    <tr>
                        <th class="_edit_title">
                            <label class="_tip_important">SD號碼起訖</label>
                        </th>
                        <td class="_edit_data form-inline">
                            <asp:TextBox ID="SDStart" runat="server" class="form-control" MaxLength="10" size="3"></asp:TextBox>
                            至
                        <asp:TextBox ID="SDEnd" runat="server" class="form-control" MaxLength="10" size="3"></asp:TextBox>
                        </td>
                        <th class="_edit_title">
                            <label class="_tip_important">MD號碼起訖</label>
                        </th>
                        <td class="_edit_data form-inline">
                            <asp:TextBox ID="MDStart" runat="server" class="form-control" MaxLength="10" size="3"></asp:TextBox>
                            至
                        <asp:TextBox ID="MDEnd" runat="server" class="form-control" MaxLength="10" size="3"></asp:TextBox>
                        </td>
                    </tr>


                <tr>
                    <td colspan="4" style="text-align: center;">
                        <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                        <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>
</asp:Content>

