﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using NPOI.HSSF.UserModel;
using System.Text;
using System.Security.Cryptography;

public partial class changeAppPw : System.Web.UI.Page
{
    public string driver_id
    {
        get { return ViewState["driver_id"].ToString(); }
        set { ViewState["driver_id"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            driver_id = Request.QueryString["driver_id"].ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (driver_id != "")
        {
            string randPwd = account_password_new.Text;
            MD5 md5 = MD5.Create();//建立一個MD5                                     
            string result = Utility.GetMd5Hash(md5, randPwd);
            using (SqlCommand cmdupd = new SqlCommand())
            {
                cmdupd.Parameters.AddWithValue("@login_password", result);
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_id", driver_id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('重置成功，密碼已寄到該帳戶的信箱。'); parent.$.fancybox.close();</script>", false);
                    return;
                }
                catch (Exception ex)
                {
                    string strErr = string.Empty;
                    if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                    strErr = "CustomerService" + Request.RawUrl + strErr + ": " + ex.ToString();

                    //錯誤寫至Log
                    PublicFunction _fun = new PublicFunction();
                    _fun.Log(strErr, "S");

                    string JStr = "alert('執行異常!" + ex.ToString() + "');";
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                    return;
                }
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請指定要重置的司機'); parent.$.fancybox.close();</script>", false);
            return;
        }
    }
}
