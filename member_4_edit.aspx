﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="member_4_edit.aspx.cs" Inherits="member_4_edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-託運單修改</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>

    <link href="css/build.css" rel="stylesheet" />

    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="templatemo-content-widget white-bg" >
            <div class="row">
                <h2 class="margin-bottom-10">新增竹運客戶代號</h2>
                <div class="templatemo-login-form">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 form-group form-inline">
                                    <label for="dl_supplier_code" class="_cus_title">負責區配商</label>
                                    <asp:DropDownList ID="dl_supplier_code" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="001">零擔</asp:ListItem>
                                        <asp:ListItem Value="002">流通</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="supplier_id" runat="server" CssClass="form-control" Width="100px" MaxLength="4" placeholder="4碼代碼"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_supplier_id" runat="server" ControlToValidate="supplier_id" ForeColor="Red" ValidationGroup="validate">請輸入4碼代碼</asp:RequiredFieldValidator>

                                    <label for="SPM_code" class="_cus_title">計價模式</label>
                                    <asp:DropDownList ID="SPM_code" runat="server" CssClass="form-control pricing_code"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="customer_name" class="_cus_title _tip_important">站所名稱</label>
                                    <asp:TextBox ID="customer_name" CssClass="form-control" runat="server" MaxLength="45" placeholder="請輸入站所名稱"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="customer_name" ForeColor="Red" ValidationGroup="validate">請輸入站所名稱</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="shortname" class="_cus_title _tip_important">站所簡稱</label>
                                    <asp:TextBox ID="shortname" CssClass="form-control" runat="server" MaxLength="18" placeholder="請輸入簡稱"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="shortname" ForeColor="Red" ValidationGroup="validate">請輸入簡稱</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%--<div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                    <label for="inputLastName" class="_cus_title _tip_important">統一編號</label>
                                    <asp:TextBox ID="uni_number" CssClass="form-control" runat="server" MaxLength="8" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="ex: 12345678"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_uni_number" runat="server" ControlToValidate="uni_number" ForeColor="Red" ValidationGroup="validate">請輸入統一編號</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="reg_uni_number_type" runat="server" ErrorMessage="統一編號格式有誤" ValidationExpression="(^\d{8}$)"
                                        ControlToValidate="uni_number" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                    </asp:RegularExpressionValidator>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title _tip_important">對　帳　人</label>
                                    <asp:TextBox ID="principal" CssClass="form-control" runat="server" placeholder="ex: 王小明" MaxLength="18"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="principal" ForeColor="Red" ValidationGroup="validate">請輸入對帳人</asp:RequiredFieldValidator>
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title _tip_important">電　　話</label>
                                    <asp:TextBox ID="tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="15" placeholder="ex:0910XXXXXX"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title">傳　　真</label>
                                    <asp:TextBox ID="fax" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="15"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <label class="_cus_title _tip_important">出貨地址</label>
                                            <asp:DropDownList ID="City" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:DropDownList ID="CityArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                            <asp:TextBox ID="address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="City" ForeColor="Red" ValidationGroup="validate">請選擇縣市</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="CityArea" ForeColor="Red" ValidationGroup="validate">請選擇鄉鎮區</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                               <%-- <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="business_people" class="_cus_title _tip_important">營業人員</label>
                                    <asp:TextBox ID="business_people" CssClass="form-control" runat="server" placeholder="ex:王大明" MaxLength="15"></asp:TextBox>

                                </div>--%>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 form-group form-inline">
                                    <label for="email" class="_cus_title _tip_important">E-mail</label>
                                    <asp:TextBox ID="email" CssClass="form-control" runat="server" MaxLength="40" placeholder="ex: aaa@gmail.com"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req9" runat="server" ControlToValidate="email" ForeColor="Red" ValidationGroup="validate">請輸入e-mail</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="email" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                    </asp:RegularExpressionValidator>
                                </div>
                               <%-- <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputFirstName" class="_cus_title _tip_important">票期</label>
                                    <asp:DropDownList ID="ticket_period" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="">請選擇</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                        <asp:ListItem>10</asp:ListItem>
                                        <asp:ListItem>15</asp:ListItem>
                                        <asp:ListItem>20</asp:ListItem>
                                        <asp:ListItem>25</asp:ListItem>
                                        <asp:ListItem>30</asp:ListItem>
                                        <asp:ListItem Value="-1">其他</asp:ListItem>
                                    </asp:DropDownList><asp:TextBox ID="ticket" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入票期天數"></asp:TextBox><label for="inputFirstName" class="_cus_title">(天)</label>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req11" runat="server" ControlToValidate="ticket_period" ForeColor="Red" ValidationGroup="validate">請選擇票期</asp:RequiredFieldValidator>
                                </div>--%>
                            </div>
                            <div class="row">
                                
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title _tip_important">停運原因</label>
                                    <asp:DropDownList ID="stop_code" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="0" Selected="True">正常</asp:ListItem>
                                        <asp:ListItem Value="1">同業競爭</asp:ListItem>
                                        <asp:ListItem Value="2">公司倒閉</asp:ListItem>
                                        <asp:ListItem Value="3">呆帳</asp:ListItem>
                                        <asp:ListItem Value="4">其他原因</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="stop_memo" CssClass="form-control" runat="server" placeholder="其他說明" MaxLength="80"></asp:TextBox>
                                </div>
                            </div>

                            <%--<hr />
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title">合約生效日</label>
                                    <asp:TextBox ID="contract_sdate" runat="server" class="form-control date_picker" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title">到　期　日</label>
                                    <asp:TextBox ID="contract_edate" runat="server" class="form-control date_picker" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title">合約內容</label>
                                    <div style="display: none">
                                        <asp:TextBox ID="filename" runat="server" />
                                    </div>
                                    <a href="#" id="hlFileView" class="fa fa-file-text" aria-hidden="true">合約內容</a>
                                    <a href="fileupload.aspx?filename=<%=filename.ClientID %>&fileview=hlFileView" class="fancybox fancybox.iframe" id="hlFileAdd"><span class="btn btn-primary">上傳新合約</span></a>
                                </div>
                                <div class="col-lg-12 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title _tip_important">結　帳　日</label>
                                    <asp:DropDownList ID="close_date" runat="server" class="form-control">
                                        <asp:ListItem Value="">請選擇</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="31">(31)30</asp:ListItem>
                                        <asp:ListItem Value="-1">其他</asp:ListItem>
                                    </asp:DropDownList><asp:TextBox ID="tbclose" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入結帳日"></asp:TextBox>
                                    <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="tbclose" ErrorMessage="結帳日需需介於 1~31" ForeColor="Red" MaximumValue="31" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req12" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請選擇結帳日</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="individual_fee" runat="server" Text="獨立計價(不採運價表計算託運單價格)" />
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title">帳單特殊需求</label>
                                    <asp:TextBox ID="billing_special_needs" CssClass="form-control" runat="server" MaxLength="220"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div id="tariffd_table" runat="server" class=" col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">運價生效日</label>
                                    <asp:Label ID="fee_sdate" runat="server" class="_fee_sdate _cus_data"></asp:Label>
                                    <asp:Label ID="lbl_PriceTip" runat="server" CssClass="text-primary span_tip"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">發包價</label>
                                    <asp:TextBox ID="contract_price" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                </div>
                            </div>
                            <asp:Panel runat="server" ID="pan_price_list">
                                <hr>
                                <span class="margin-bottom-10 _ship_title">運價表設定</span>
                                <input name="dw_0" type="button" class="btn btn-primary" value="下載公版運價" onclick='price_dw(0, 0)' />

                                <a class="fancybox fancybox.iframe" id="hlFeeAdd">
                                    <span class="btn btn-primary">上傳新運價</span>
                                </a>
                            </asp:Panel>--%>
                        </div>
                    </div>

                    <div class="form-group text-center _btn_area">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button btn" runat="server" Text="確　認" ValidationGroup="validate" OnClick="btnsave_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    
                        <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                        <asp:Label ID="lb_cusid" CssClass="_cus_id hide" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
