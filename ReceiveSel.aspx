﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReceiveSel.aspx.cs" Inherits="ReceiveSel" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-收貨人選取</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇收貨人</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">
                                查詢條件：
                                <span class="section">1</span>
                                <label>查詢代號：</label>
                                <asp:TextBox ID="receiver_code" runat="server" class="form-control"></asp:TextBox>
                                <span class="section">2</span>
                                <label>名稱：</label>
                                <asp:TextBox ID="receiver_name" runat="server" class="form-control"></asp:TextBox>
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                            </div>
                        </div>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>選取</th>
                                <th>客戶代碼</th>
                                <th>客戶名稱</th>
                                <th>地址</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="選取">
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                            <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                        </td>
                                        <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                        <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                        <td data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_road").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="5" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                        <div class="pager">
                            <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                            <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                            <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                            <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                            <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                            客戶總數: <span class="text-danger">
                                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
