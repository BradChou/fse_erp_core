﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_2-edit.aspx.cs" Inherits="store1_2_edit" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();

        });

        $.fancybox.update();

        $(function () {
            $("#<%=chkall.ClientID%>").click(function () {
                if ($("#<%=chkall.ClientID%>").prop("checked")) {
                    $("[id*=chkcus_]").each(function () {
                        var sID_BOX = $(this).attr("id");
                        document.getElementById(sID_BOX).checked = true;
                    });

                } else {
                    $("[id*=chkcus_]").each(function () {
                        var sID_BOX = $(this).attr("id");
                        document.getElementById(sID_BOX).checked = false;
                    });
                }

            });
        });


    </script>
    <style type="text/css">
        .hide {
            display: none;
        }

        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .span_tip {
            margin-left: 15px;
            color: red;
        }

        .col-lg-12 {
            width: 50%;
        }

        .tb_price_log {
            width: 40%;
            margin: 10px 0px;
            border: 1px solid #ddd;
        }

            .tb_price_log th {
                background-color: #39ADB4;
                color: white;
                text-align: center;
                padding: 5px;
            }

            .tb_price_log td {
                text-align: center;
                padding: 5px;
            }

        .bk_color1 {
            background-color: #f9f9f9;
        }

        .tb_price_th {
            width: 15%;
        }

        .p_date {
            width: 27%;
        }

        .p_type {
            width: 23%;
        }

        .p_set {
            width: 35%;
        }

        .p_td_null {
            text-align: center;
            padding: 10px;
        }

        ._price_today {
            color: blue;
            font-weight: bold;
        }

        ._ship_title {
            font-size: 20px;
            margin-right: 30px;
        }

        ._cus_title {
            width: 13%;
        }

        ._cus_data {
            color: #8a9092;
        }

        .cus_main {
            height: 720px;
            overflow-y: auto;
            padding: 5px;
        }

        .row {
            margin: 0px;
        }

        .btn {
            margin: 0px 5px;
        }

        ._btn_area {
            margin-top: 10px;
        }

        .div_price_log {
            max-height: 210px;
            overflow-y: auto;
        }

        .tb_price_log tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .scrollbar::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper" class="cus_main scrollbar">
        <div class="row">
            <h2 class="margin-bottom-10">場區資料-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
            <div class="templatemo-login-form">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">場區名稱：</label>
                                <asp:TextBox ID="Area" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="場區名稱"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="Area" ForeColor="Red" ValidationGroup="validate">請輸入場區名稱</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="panel panel-primary ">
                            <div class="panel-heading">客代資料設定<asp:CheckBox ID="chkall" CssClass="checkbox checkbox-success" Text='全選' runat="server" style="left: 0px; top: 0px" /></div>
                            <div class="panel-body">
                                <div class="row ">
                                    <div class="col bottnmargin form-inline">
                                        <div class="_checkboxlist ">
                                            <asp:Repeater ID="rp_cus" runat="server">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="Hid_cus" runat="server" Value='<%# Eval("customer_code") %>' />
                                                    <asp:CheckBox ID="chkcus" CssClass=" checkbox checkbox-success" Text='<%# Eval("customer_shortname") %>' Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.chk"))%>' runat="server" />
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <%--<asp:CheckBoxList ID="cb_oil" runat="server" CssClass=" checkbox checkbox-success"  RepeatDirection="Horizontal" ></asp:CheckBoxList>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="form-group text-center _btn_area">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button btn" runat="server" Text="確　認" ValidationGroup="validate" OnClientClick="this.disabled=true;"
                            UseSubmitBehavior="False" OnClick="btnsave_Click" />

                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="store1_2.aspx" class="templatemo-white-button btn"><span>取　消</span></a>
                        <asp:Label ID="lb_susid" CssClass="_cus_id hide" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

