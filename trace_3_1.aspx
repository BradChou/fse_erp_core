﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="trace_3_1.aspx.cs" Inherits="trace_3_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">配達明細</h2>
            <div class="row form-group">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label for="inputLastName">發送日期：</label>
                    <asp:Label ID="dates" runat="server" ></asp:Label>                  
                    ~ 
                    <asp:Label ID="datee" runat="server" ></asp:Label>
                    <asp:TextBox ID="Suppliers" runat="server"  style="display: none"></asp:TextBox>
                    <asp:TextBox ID="kind" runat="server"  style="display: none"></asp:TextBox>
                    <asp:HiddenField ID="Hid_customer_code" runat="server" />
                    <asp:HiddenField ID="Hid_type" runat="server" />
                    <asp:HiddenField ID="hid_Rtype" runat="server" />
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline text-right">
                    <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click" />
                </div>
            </div>
            <p>※當日配達筆數：於17:00前掃讀配達之筆數</p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>人員編號</th>
                    <th>作業人員</th>
                    <th>發送日</th>
                    <th>發送站</th>
                    <th>到著站</th>
                    <th>貨號</th>
                    <th>出貨人</th>
                    <th>收貨人</th>
                    <th>商品種類</th>
                    <th>發送件數</th>
                    <th>配送件數</th>
                    <th>配送時間</th>
                    <th>配達時間</th>
                    <th>配達區分</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="人員編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                            <td data-th="作業人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                            <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                            <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrivestation").ToString())%></td>
                            <td data-th="貨號">
                                <%#Server.HtmlDecode((DataBinder.Eval(Container, "DataItem.Less_than_truckload").ToString())  == "True"  ? 
"<a href='LT_trace_1.aspx?check_number="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) +"&date="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString()).Substring(0,7).Replace("/","-") + "' class=' btn btn-link ' id='updclick'>"+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) +"</a>" :
"<a href='trace_1.aspx?check_number="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) +"&date="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString()).Substring(0,7).Replace("/","-") + "' class=' btn btn-link ' id='updclick'>"+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) +"</a>" )%> 
                                <%--<a href="trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>&date=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString()).Substring(0,7).Replace("/","-")%>" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></a>--%>
                            </td>
                            <td data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="商品種類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.product_category_name").ToString())%></td>
                            <td data-th="發送件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                            <td data-th="配送件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                            <td data-th="配送時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                            <td data-th="配達時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_date").ToString())%></td>
                            <td data-th="配達區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_option").ToString())%></td>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="13" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>

        </div>
    </div>
</asp:Content>

