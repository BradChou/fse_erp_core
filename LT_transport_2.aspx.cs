﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;

public partial class LT_transport_2 : System.Web.UI.Page
{
    public bool Less_than_truckload = true;
    public int start_number_type1
    {
        get { return Convert.ToInt32(ViewState["start_number_type1"]); }
        set { ViewState["start_number_type1"] = value; }
    }

    public int end_number_type1
    {
        get { return Convert.ToInt32(ViewState["end_number_type1"]); }
        set { ViewState["end_number_type1"] = value; }
    }

    public int start_number_type2
    {
        get { return Convert.ToInt32(ViewState["start_number_type2"]); }
        set { ViewState["start_number_type2"] = value; }
    }

    public int end_number_type2
    {
        get { return Convert.ToInt32(ViewState["end_number_type2"]); }
        set { ViewState["end_number_type2"] = value; }
    }


    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }


    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }


    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            //ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();


            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();
            switch (ssManager_type)
            {
                case "0":
                case "1":
                case "2":
                    ssSupplier_code = "";
                   // ssMasterCustomerCode = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select* from tbStation
                                          where management = @management ";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    ssSupplier_code += "'";
                                    ssSupplier_code += dt.Rows[i]["station_scode"].ToString();
                                    ssSupplier_code += "'";
                                    ssSupplier_code += ",";
                                }
                                ssSupplier_code = ssSupplier_code.Remove(ssSupplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select* from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    ssSupplier_code += "'";
                                    ssSupplier_code += dt.Rows[i]["station_scode"].ToString();
                                    ssSupplier_code += "'";
                                    ssSupplier_code += ",";
                                }
                                ssSupplier_code = ssSupplier_code.Remove(ssSupplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select* from tbStation
                                          where station_area = @station_area ";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    ssSupplier_code += "'";
                                    ssSupplier_code += dt.Rows[i]["station_scode"].ToString();
                                    ssSupplier_code += "'";
                                    ssSupplier_code += ",";
                                }
                                ssSupplier_code = ssSupplier_code.Remove(ssSupplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5"))
                        {


                            cmd.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                /*
                                ssSupplier_code += "'";
                                ssSupplier_code += dt.Rows[0]["station_scode"].ToString();
                                ssSupplier_code += "'";
                                ssSupplier_code += ",";
                                */
                            }
                            /*
                            ssSupplier_code = ssSupplier_code.Remove(ssSupplier_code.ToString().LastIndexOf(','), 1);
                            */
                            //ssSupplier_code = dt.Rows[0]["station_scode"].ToString();
                        }
                        else
                        {

                            cmd.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            /*
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ssSupplier_code += "'";
                                ssSupplier_code += dt.Rows[0]["station_scode"].ToString();
                                ssSupplier_code += "'";
                                ssSupplier_code += ",";
                            }
                            ssSupplier_code = ssSupplier_code.Remove(ssSupplier_code.ToString().LastIndexOf(','), 1);
                            */
                        }

                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";

                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            ssSupplier_code = dt.Rows[0]["station_scode"].ToString();
                        }
                    }
                    break;
                case "5":
                    ssSupplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select city from tbPostCity order by seq asc ";
            receive_city.DataSource = dbAdapter.getDataTable(cmd1);
            receive_city.DataValueField = "city";
            receive_city.DataTextField = "city";
            receive_city.DataBind();
            receive_city.Items.Insert(0, new ListItem("請選擇", ""));

            send_city.DataSource = dbAdapter.getDataTable(cmd1);
            send_city.DataValueField = "city";
            send_city.DataTextField = "city";
            send_city.DataBind();
            send_city.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            rbcheck_type_SelectedIndexChanged(null, null);

            #region 託運類別
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
            check_type.DataSource = dbAdapter.getDataTable(cmd2);
            check_type.DataValueField = "code_id";
            check_type.DataTextField = "code_name";
            check_type.DataBind();

            //SqlCommand cmd4 = new SqlCommand();
            //cmd4.CommandText = " select code_id, code_name,  code_name as showname from tbItemCodes where code_sclass = 'PM'  order by code_id asc";
            //rb_pricing_type.DataSource = dbAdapter.getDataTable(cmd4);
            //rb_pricing_type.DataValueField = "code_id";
            //rb_pricing_type.DataTextField = "showname";
            //rb_pricing_type.DataBind();
            //rb_pricing_type.Items.Insert(0, new ListItem("全部", ""));
            //rb_pricing_type.SelectedValue = "";
            #endregion

            #region 客戶代號
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";


                //ssMaster_code = (ssManager_type == "3" || ssManager_type == "5") ? ssMaster_code : "";
                //if (ssMaster_code != "")
                //{
                //    cmd.Parameters.AddWithValue("@Master_code", ssMaster_code);
                //    wherestr = "  and customer_code like ''+ @Master_code + '%'";
                //}

                cmd.Parameters.AddWithValue("@type", "1");  // 零擔客戶
                if (ssSupplier_code != "")
                {
                    if (station_level.Equals("1")|| station_level.Equals("2")|| station_level.Equals("4"))
                    {
                        cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                        wherestr = " and station_scode in (" + ssSupplier_code + ")";
                    }
                  
                }
                /*
                if (MasterCustomer!="")
                {
                    cmd.Parameters.AddWithValue("@MasterCustomerCode", MasterCustomerCode);
                    wherestr = " and MasterCustomerCode =@MasterCustomerCode";
                }
                */


                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = '0' and type = @type {0}
                                            )
                                            SELECT supplier_id, customer_code, customer_name , supplier_id + '-' +  customer_name as showname , customer_code + '-' +  customer_name as showname2
                                            FROM cus
                                            WHERE rn = 1 order by master_code", wherestr);
                Customer.DataSource = dbAdapter.getDataTable(cmd);
                Customer.DataValueField = "customer_code";
                Customer.DataTextField = "showname2";
                Customer.DataBind();
            }
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "4")
            {
                Customer.Items.Insert(0, new ListItem("全選", ""));
            }
            #endregion

            #region 主客戶代號
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";


                //ssMaster_code = (ssManager_type == "3" || ssManager_type == "5") ? ssMaster_code : "";
                //if (ssMaster_code != "")
                //{
                //    cmd.Parameters.AddWithValue("@Master_code", ssMaster_code);
                //    wherestr = "  and customer_code like ''+ @Master_code + '%'";
                //}
                /*
                cmd.Parameters.AddWithValue("@type", "1");  // 零擔客戶
                if (ssSupplier_code != "")
                {
                    if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
                    {
                        cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                        wherestr = " and station_scode in (" + ssSupplier_code + ")";
                    }

                }
                
                if (MasterCustomer.Items.ToString()!="")
                {
                    cmd.Parameters.AddWithValue("@MasterCustomerCode", MasterCustomer);
                    wherestr = " and MasterCustomerCode ='F2000010002'";
                }
                


                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = '0' and type = @type {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname , customer_code + '-' +  customer_name as showname2
                                            FROM cus
                                            WHERE rn = 1 order by master_code", wherestr);
                */
                cmd.CommandText = string.Format(@"select distinct MasterCustomerCode,MasterCustomerName,MasterCustomerCode+'-'+MasterCustomerName as showname2 from tbCustomers
											where MasterCustomerCode is not null 
											and MasterCustomerCode <>''
											and stop_shipping_code=0
											order by MasterCustomerCode");
                MasterCustomer.DataSource = dbAdapter.getDataTable(cmd);
                MasterCustomer.DataValueField = "MasterCustomerCode";
                MasterCustomer.DataTextField = "showname2";
                MasterCustomer.DataBind();
                MasterCustomer.Items.Insert(0, new ListItem("全選", ""));
            }
            #endregion

            #region 傳票類別
            SqlCommand cmd3 = new SqlCommand();
            cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 and code_id <>'31' and code_id <> '51' order by code_id ";
            subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
            subpoena_category.DataValueField = "code_id";
            subpoena_category.DataTextField = "code_name";
            subpoena_category.DataBind();
            #endregion

            DateTime dt_S, dt_E;
            string type = Request.QueryString["type"] != null ? Request.QueryString["type"].ToString() : "1";

            switch (type)
            {
                case "0":
                    rb1.Checked = true;
                    rb2.Checked = false;
                    //rb3.Checked = false;
                    if (Request.QueryString["dateS"] != null && DateTime.TryParse(Request.QueryString["dateS"].ToString(), out dt_S))
                    {
                        tb_dateS1.Text = dt_S.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        tb_dateS1.Text = DateTime.Now.ToString("yyyy/MM/dd");
                    }


                    if (Request.QueryString["dateE"] != null && DateTime.TryParse(Request.QueryString["dateE"].ToString(), out dt_E))
                    {
                        tb_dateE1.Text = dt_E.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        tb_dateE1.Text = DateTime.Now.ToString("yyyy/MM/dd");
                    }
                    break;
                case "1":
                    rb1.Checked = false;
                    rb2.Checked = true;
                    //rb3.Checked = false;
                    if (Request.QueryString["dateS"] != null && DateTime.TryParse(Request.QueryString["dateS"].ToString(), out dt_S))
                    {
                        tb_dateS2.Text = dt_S.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        tb_dateS2.Text = DateTime.Now.ToString("yyyy/MM/dd");
                    }


                    if (Request.QueryString["dateE"] != null && DateTime.TryParse(Request.QueryString["dateE"].ToString(), out dt_E))
                    {
                        tb_dateE2.Text = dt_E.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        tb_dateE2.Text = DateTime.Now.ToString("yyyy/MM/dd");
                    }

                    break;
                case "2":
                    rb1.Checked = false;
                    rb2.Checked = false;
                    //rb3.Checked = true ;
                    break;

            }



            if (Request.QueryString["check_number"] != null) check_number.Text = Request.QueryString["check_number"];
            if (Request.QueryString["DeliveryType"] != null) DR_type.SelectedValue = Request.QueryString["DeliveryType"];
            if (Request.QueryString["status"] != null) dlStatus.SelectedValue = Request.QueryString["status"];

            //rb_pricing_type.SelectedValue = Request.QueryString["pricing_type"];

            Customer.SelectedValue = Request.QueryString["Customer"];
            MasterCustomer.SelectedValue = Request.QueryString["Mastercustomer_code"];
            
            if (Request.QueryString["dateS"] != null)
            {
                readdata();
            }
        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

        //權限        
        //if (ssSupplier_code != "")
        //{
        //    cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
        //    strWhereCmd += " and A.supplier_code = @supplier_code";
        //}

        //if ((ssMaster_code != "") && (ssManager_type== "3" || ssManager_type == "5") )
        //{
        //    cmd.Parameters.AddWithValue("@Master_code", ssMaster_code);
        //    strWhereCmd += " and A.customer_code like @Master_code+'%' ";
        //}
        if (ssSupplier_code != "")
        {
            cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
            strWhereCmd = " and C.station_scode in (" + ssSupplier_code + ")";
        }

        if (DR_type.SelectedItem.Value != "")
        {
            cmd.Parameters.AddWithValue("@DeliveryType", DR_type.SelectedItem.Value);
            strWhereCmd += " and A.DeliveryType  = @DeliveryType ";
            querystring += "&DeliveryType=" + DR_type.SelectedItem.Value;
        }

        if (dlStatus.SelectedItem.Value != "")
        {
            if (dlStatus.SelectedValue == "0")
            {
                strWhereCmd += " and A.cancel_date is null  ";
            }
            else if (dlStatus.SelectedValue == "1")
            {
                strWhereCmd += " and A.cancel_date is not null  ";
            }
        }
        querystring += "&status=" + dlStatus.SelectedItem.Value;


        if (!String.IsNullOrEmpty(tb_dateS1.Text) && !String.IsNullOrEmpty(tb_dateE1.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", tb_dateS1.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(tb_dateE1.Text).AddDays(1).ToString("yyyy/MM/dd"));

            //strWhereCmd += " AND CONVERT(CHAR(10),A.cdate,111) BETWEEN  CONVERT(CHAR(10),@dateS,111) AND  CONVERT(CHAR(10),@dateE,111) ";
            strWhereCmd += " AND A.cdate >=@dateS and A.cdate < @dateE  ";
            querystring += "&dateS=" + tb_dateS1.Text + "&dateE=" + tb_dateE1.Text;

        }
        else if (!String.IsNullOrEmpty(tb_dateS2.Text) && !String.IsNullOrEmpty(tb_dateE2.Text))
        {

            cmd.Parameters.AddWithValue("@dateS", tb_dateS2.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(tb_dateE2.Text).AddDays(1).ToString("yyyy/MM/dd"));
            //if (check_number.Text != "")
            //{
            //    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            //    strWhereCmd += " AND (CONVERT(CHAR(10),A.print_date,111) BETWEEN  CONVERT(CHAR(10),@dateS,111) AND  CONVERT(CHAR(10),@dateE,111) or (A.check_number = @check_number  AND A.print_date>=DATEADD(MONTH,-6,GETDATE())))";
            //    querystring += "&check_number=" + check_number.Text;
            //}
            //else
            //{
            //    strWhereCmd += " AND CONVERT(CHAR(10),A.print_date,111) BETWEEN  CONVERT(CHAR(10),@dateS,111) AND  CONVERT(CHAR(10),@dateE,111) ";
            //}
            //strWhereCmd += " AND CONVERT(CHAR(10),A.print_date,111) BETWEEN  CONVERT(CHAR(10),@dateS,111) AND  CONVERT(CHAR(10),@dateE,111) ";
            strWhereCmd += " AND A.print_date >=@dateS and A.print_date < @dateE  ";
            querystring += "&dateS=" + tb_dateS2.Text + "&dateE=" + tb_dateE2.Text;
        }

        if (check_number.Text != "")
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            strWhereCmd += " and A.check_number = @check_number";
            querystring += "&check_number=" + check_number.Text;
        }

        //if (rb_pricing_type.SelectedValue != "")
        //{
        //    cmd.Parameters.AddWithValue("@pricing_type", rb_pricing_type.SelectedValue);
        //    strWhereCmd += " and A.pricing_type = @pricing_type";
        //    querystring += "&pricing_type=" + rb_pricing_type.SelectedValue;
        //}

        if (Customer.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@customer_code", Customer.SelectedValue);
            strWhereCmd += " and A.customer_code like @customer_code+'%' ";
            querystring += "&Customer=" + Customer.SelectedValue;
        }

        if (MasterCustomer.SelectedValue!="")
        {
            cmd.Parameters.AddWithValue("@MasterCustomerCode", MasterCustomer.SelectedValue);
            strWhereCmd += " and C.MasterCustomerCode like @MasterCustomerCode+'%' ";
            querystring += "&MasterCustomerCode=" + MasterCustomer.SelectedValue;
        }

        if (rb1.Checked)
        {
            querystring += "&type=0";
        }
        else
        {
            querystring += "&type=1";
        }

        #endregion

        cmd.CommandText = string.Format(@"SELECT A.pricing_type,A.request_id
                                                ,ROW_NUMBER() OVER(ORDER BY A.print_date, A.check_number,CONVERT(INT,A.sub_check_number) DESC) AS ROWID 
                                                ,CONVERT(CHAR(10),A.supplier_date,111) supplier_date, A.check_number , LEFT( A.customer_code,3) as sendstation , A.receive_contact , 
                                                 --CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(C.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation2,
                                                 --sta1.station_name 'sendstation2',
                                                 case when A.DeliveryType = 'R' then  (select B1.station_name from 
                                                                                         ttArriveSitesScattered A1 With(Nolock) 
												                                         LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
												                                         where A1.post_city = A.send_city and A1.post_area= A.send_area ) 
                                                 else F1.station_name end 'sendstation2',
                                                 A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area ,
                                                 CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                                 A.pieces , A.plates , A.cbm ,
                                                 B.code_name , A.customer_code , C.customer_shortname ,
                                                 A.send_contact,A.send_city , A.send_area , A.send_address , A.cancel_date,A.add_transfer
                                                ,(SELECT COUNT(1) FROM ttDeliveryScanLog With(Nolock) WHERE check_number = A.check_number) logNum 
                                                ,A.uuser, D.user_name , A.udate,A.print_date,A.import_randomCode,A.cdate,
                                                  CASE  A.DeliveryType 
                                                    WHEN 'D' THEN '正物流'   WHEN 'R' THEN '逆物流' ELSE '' END DeliveryType 
                                          from tcDeliveryRequests A With(Nolock)
                                          left join tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C With(Nolock) on C.customer_code = A.customer_code 
                                          left join tbAccounts  D With(Nolock) on D.account_code = A.cuser
                                          LEFT JOIN tbStation F1 with(nolock) on  F1.station_code =  A.supplier_code
                                          LEFT JOIN ttArriveSitesScattered sta With(nolock) on A.send_city = sta.post_city and A.send_area = sta.post_area
                                          LEFT JOIN tbStation sta1 With(nolock) on A.send_station_scode  = sta1.station_scode
                                          where 1= 1 {0} 
                                          and A.Less_than_truckload = 1 and ISNULL(A.Less_than_truckload,0)=@Less_than_truckload 
                                          order by A.print_date, A.check_number", strWhereCmd);
        // ,ROW_NUMBER() OVER(PARTITION BY  A.check_number ORDER BY CONVERT(INT,A.sub_check_number) DESC )  AS 'newAddr' 



        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;
        objPds.PageSize = 10;
        int sumlistpage = 9;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Visible = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Visible = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Visible = true;
        }
        else
        {
            lnklast.Visible = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Visible = false;
        }
        else
        {
            lnkPrev.Visible = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Visible = false;
        }
        else
        {
            lnkNext.Visible = true;
        }


        //跑分頁前五個
        for (int j = (CurPage - 5); j <= (CurPage - 1); j++)
        {

            if (j <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)) && j > 0)
            {
                pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + j.ToString() + querystring + "'>" + j.ToString() + "</a></li>"));
                sumlistpage = sumlistpage - 1;
            }

        }

        //跑分頁後面剩餘數，共十個
        for (int i = CurPage; i <= (CurPage + sumlistpage); i++)
        {
            if (i == CurPage)
            {
                pagelist.Text += "<li class='active'><a href='#'>" + CurPage.ToString() + "</a></li>";
            }
            else
            {
                if (i <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + i.ToString() + querystring + "'>" + i.ToString() + "</a></li>"));
                }

            }
        }


        //if (objPds.PageSize > 0)
        //{
        //    tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        //}

        #endregion

        DT = ds.Tables[0];
        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (SetCheckNumber())
        {

            Clear();
            divAdd.Visible = true;
            #region 貨號區間(一筆式)
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbCheckNumber ";
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT.Rows.Count > 0)
                    {
                        for (int i = 0; i < DT.Rows.Count; i++)
                        {
                            switch (DT.Rows[i]["type"].ToString())
                            {
                                case "1":   //一筆式
                                    start_number_type1 = Convert.ToInt32(DT.Rows[i]["start_number"]);
                                    end_number_type1 = Convert.ToInt32(DT.Rows[i]["end_number"]);
                                    break;
                                case "2":   //網路
                                    start_number_type2 = Convert.ToInt32(DT.Rows[i]["start_number"]);
                                    end_number_type2 = Convert.ToInt32(DT.Rows[i]["end_number"]);
                                    break;
                            }
                        }
                    }
                }
            }
            #endregion

            div_search.Visible =
            div_list.Visible = false;
        }
        else
        {
            //無未補單的貨號
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('目前並無需補單的貨號，請重新確認!');</script>", false);
            return;

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        lbErr.Text = "";
        int ttpieces = 0;
        int ttplates = 0;
        int ttcbm = 0;
        int ttcollection_money = 0;
        int ttarrive_to_pay = 0;
        int ttarrive_to_append = 0;
        int remote_fee = 0;

        if (ddl_check_number2.SelectedValue == "")
        {
            lbErr.Text = "請選擇貨號!";
            ddl_check_number2.Focus();
            return;
        }
        //if (!Chk_check_number2())
        //{
        //    ddl_check_number2.Focus();
        //    return;
        //}
        try
        {
            ttpieces = Convert.ToInt32(pieces.Text);
        }
        catch { }

        try
        {
            ttplates = Convert.ToInt32(plates.Text);
        }
        catch { }

        try
        {
            ttcbm = Convert.ToInt32(cbm.Text);
        }
        catch { }

        switch (rbcheck_type.SelectedValue)
        {
            case "01":
            case "04":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入板數";
                    plates.Focus();
                }
                break;
            case "02":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入件數";
                    pieces.Focus();
                }
                break;
            case "03":
                if (ttplates == 0)
                {
                    lbErr.Text = "請輸入才數";
                    cbm.Focus();
                }
                break;
        }
        lbErr.Visible = (lbErr.Text != "");
        if (lbErr.Text == "" && lbErr_chknum.Text == "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@pricing_type", rbcheck_type.SelectedValue);                     //計價模式 (01:論板、02:論件、03論才、04論小板)
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
            cmd.Parameters.AddWithValue("@check_number", ddl_check_number2.SelectedValue);                //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
            cmd.Parameters.AddWithValue("@order_number", "");                                              //訂單號碼
            cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
            cmd.Parameters.AddWithValue("@receive_customer_code", "");                                    //收貨人編號
            cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
            cmd.Parameters.AddWithValue("@receive_tel1", "");                                             //電話1
            cmd.Parameters.AddWithValue("@receive_tel1_ext", "");                                         //電話分機
            cmd.Parameters.AddWithValue("@receive_tel2", "");                                             //電話2
            cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
            cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
            cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
            remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
            cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
            cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code.SelectedValue);             //到著碼
            cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", 0);                //到站領貨 0:否、1:是
            //if (cbarrive.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
            //}


            try
            {
                ttcollection_money = Convert.ToInt32(collection_money.Text);
            }
            catch { }

            try
            {
                ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
            }
            catch { }

            try
            {
                ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
            }
            catch { }

            cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
            cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
            cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數
            cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加
            cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
            cmd.Parameters.AddWithValue("@send_tel", "");                           //寄件人電話
            cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
            cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
            cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
            cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
            cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
            cmd.Parameters.AddWithValue("@arrive_mobile", "");                                            //收件人-手機1  
            cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
            cmd.Parameters.AddWithValue("@invoice_memo", "");                                             //備註
            cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Text);                              //說明
            cmd.Parameters.AddWithValue("@product_category", "");                                         //商品種類
            cmd.Parameters.AddWithValue("@special_send", "");                                             //特殊配送
            cmd.Parameters.AddWithValue("@arrive_assign_date", DBNull.Value);                             //指定日
            cmd.Parameters.AddWithValue("@time_period", "");                                              //時段
            cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
            cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
            cmd.Parameters.AddWithValue("@supplier_code", supplier_code.Text);                             //配送商代碼
            //cmd.Parameters.AddWithValue("@supplier_name", supplier_name.Text);                           //配送商名稱
            cmd.Parameters.AddWithValue("@print_date", Shipments_date.Text);                               //出貨日期
            cmd.Parameters.AddWithValue("@add_transfer", 0);
            cmd.Parameters.AddWithValue("@print_flag", 0);
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 

            cmd.CommandText = dbAdapter.SQLdosomething("tcDeliveryRequests", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            cmd.Dispose();
            Clear();
            readdata();

            divAdd.Visible = false;
            div_search.Visible =
            div_list.Visible = true;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');</script>", false);  //parent.location.reload(true);
            return;
        }

    }

    protected void btnCanle_Click(object sender, EventArgs e)
    {
        divAdd.Visible = false;
        div_search.Visible =
        div_list.Visible = true;
    }





    protected Boolean SetCheckNumber()
    {
        Boolean IsOK = false;
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = @"SELECT DISTINCT check_number  
                                   FROM ttDeliveryScanLog With(Nolock) 
                                   WHERE check_number NOT IN(SELECT check_number FROM tcDeliveryRequests With(Nolock) WHERE check_number IS NOT NULL AND check_number <> '')
                                   ORDER BY check_number DESC;";
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        ddl_check_number2.Items.Clear();
                        ddl_check_number2.DataSource = dt;
                        ddl_check_number2.DataValueField = "check_number";
                        ddl_check_number2.DataTextField = "check_number";
                        ddl_check_number2.DataBind();
                        ddl_check_number2.Items.Insert(0, new ListItem("請選擇", ""));
                        dt.Dispose();
                        IsOK = true;
                    }
                }
            }

        }
        catch (Exception ex)
        {

        }

        return IsOK;
    }
    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }
    private void paramlocation()
    {
        string querystring = "";

        DateTime dt_S, dt_E;
        if (rb1.Checked)
        {
            if (!DateTime.TryParse(tb_dateS1.Text, out dt_S) || !DateTime.TryParse(tb_dateE1.Text, out dt_E))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入建單區間!');</script>", false);
                return;
            }
            if (dt_S > dt_E)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認建單區間是否正確!');</script>", false);
                return;
            }
            querystring += "&type=0";
            querystring += "&dateS=" + dt_S.ToString("yyyy/MM/dd");
            querystring += "&dateE=" + dt_E.ToString("yyyy/MM/dd");
        }
        else if (rb2.Checked)
        {
            if (!DateTime.TryParse(tb_dateS2.Text, out dt_S) || !DateTime.TryParse(tb_dateE2.Text, out dt_E))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入發送區間!');</script>", false);
                return;
            }
            if (dt_S > dt_E)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認發送區間是否正確!');</script>", false);
                return;
            }
            querystring += "&type=1";
            querystring += "&dateS=" + dt_S.ToString("yyyy/MM/dd");
            querystring += "&dateE=" + dt_E.ToString("yyyy/MM/dd");
        }
        else
        {
            querystring += "&type=2";
        }


        if (check_number.Text.ToString() != "") querystring += "&check_number=" + check_number.Text.ToString();

        //querystring += "&pricing_type=" + rb_pricing_type.SelectedValue;

        querystring += "&Customer=" + Customer.SelectedValue;
        querystring += "&DeliveryType=" + DR_type.SelectedItem.Value;
        querystring += "&Status=" + dlStatus.SelectedItem.Value;

        if (MasterCustomer.SelectedValue!="")
        {
            querystring += "&Mastercustomer_code=" + MasterCustomer.SelectedValue;
        }

        //querystring += "&MasterCustomerCode=" + dlMastercustomer_code.SelectedItem.Value;
        Response.Redirect(ResolveUrl("~/LT_transport_2.aspx?search=yes" + querystring));
    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("選擇鄉鎮區", ""));
        }
    }

    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        //customer_code.Text = dlcustomer_code.SelectedValue;
        //顯示寄件人資料
        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dt;
            //cmd.CommandText = "select * from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue.ToString() + "' ";
            cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_name , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + dlcustomer_code.SelectedValue.ToString() + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                //send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();         //區配商代碼

            }
        }
    }

    protected void dlMastercustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        //customer_code.Text = dlcustomer_code.SelectedValue;
        //顯示寄件人資料
        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dt;
            //cmd.CommandText = "select * from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue.ToString() + "' ";
            cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_name , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + dlMastercustomer_code.SelectedValue.ToString() + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                //send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();         //區配商代碼

            }
        }
    }

    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (ssSupplier_code != "")
        {
            switch (ssManager_type)
            {
                case "0":
                case "1":
                case "4":
                    cmd2.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                    break;
                default:
                    cmd2.Parameters.AddWithValue("@master_code", ssSupplier_code);
                    wherestr = " and master_code like ''+@master_code+'%' ";
                    break;
            }
        }

        cmd2.CommandText = string.Format(@"select customer_code , customer_code+ '-' + customer_shortname as name  from tbCustomers with(Nolock) where stop_shipping_code = '0' and pricing_code = '" + rbcheck_type.SelectedValue + "' {0} order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion
        Clear_customer();
    }

    protected void Clear_customer()
    {
        supplier_code.Text = "";
        send_contact.Text = "";   //名稱
        //send_tel.Text = "";           // 電話
        send_city.SelectedValue = "";   //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";   //出貨地址-鄉鎮市區
        send_address.Text = "";        //出貨地址-路街巷弄號
    }

    protected void Clear()
    {
        dlcustomer_code.SelectedIndex = 0;
        supplier_code.Text = "";
        check_type.SelectedIndex = 0;
        subpoena_category.SelectedIndex = 0;
        receive_contact.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        area_arrive_code.Items.Clear();
        pieces.Text = "";
        plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        arrive_to_pay_freight.Text = "";
        arrive_to_pay_append.Text = "";
        send_contact.Text = "";
        send_city.SelectedIndex = 0;
        send_area.Items.Clear();
        send_address.Text = "";
        invoice_desc.Text = "";
        receipt_flag.Checked = false;
        pallet_recycling_flag.Checked = false;
        Shipments_date.Text = "";

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 到著站簡碼
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"Select A.* , A.station_code  + ' '+  B.station_name as showname    
                                                   from ttArriveSitesScattered A
                                                   left join tbStation B on A.station_code  = B.station_code 
                                                   where A.station_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        area_arrive_code.SelectedValue = dt.Rows[0]["station_code"].ToString();
                    }
                    catch { }
                }
            }
        }
        #endregion
    }

    protected Boolean Chk_check_number2()
    {
        Boolean IsOK = true;
        //託運單號
        //暫時僅用10碼，且貨號前方不補零。新竹規則MOD(num, 7) 前9碼 + 0~6檢碼
        lbErr_chknum.Text = "";
        if (ddl_check_number2.SelectedValue.Length != 10)
        {
            lbErr_chknum.Text = "託運單號錯誤，長度應為10。";
            IsOK = false;
        }

        if (IsOK)
        {
            int num = Convert.ToInt32(ddl_check_number2.SelectedValue.Substring(0, 9));
            //竹運
            if (supplier_code.Text != "" && (supplier_code.Text.Substring(0, 3) == "001" || supplier_code.Text.Substring(0, 3) == "002"))
            {
                //not 8158486615 and 8158586601
                //not 5920000000 and 5929999999
                //not 8347120000 and 8347129999
                //not 8819346760 and 8819406750
                //not 9000200010 and 9000229999
                //not 8283490025 and 8286490015


            }
            else
            {
                //  686355000 ~ 686404999  或
                //  828349002 ~ 828649001
                if (num >= start_number_type1 && num <= end_number_type1)
                {
                    int chk = num % 7;
                    int lastnum = Convert.ToInt32(ddl_check_number2.SelectedValue.Substring(9, 1));
                    if (lastnum != chk)
                    {
                        lbErr_chknum.Text = "非有效的託運單號，請檢查編號是否正確";
                        IsOK = false;
                    }
                }
                else if (num >= start_number_type2 && num <= end_number_type2)
                {
                    int chk = num % 7;
                    int lastnum = Convert.ToInt32(ddl_check_number2.SelectedValue.Substring(9, 1));
                    if (lastnum != chk)
                    {
                        lbErr_chknum.Text = "非有效的託運單號，請檢查編號是否正確";
                        IsOK = false;
                    }
                }
                else
                {
                    lbErr_chknum.Text = "非有效的託運單號,應落在 " + start_number_type1.ToString() + " ~ " + end_number_type1.ToString() + " <br>或 " + start_number_type2.ToString() + " ~ " + end_number_type2.ToString() + "之間，請檢查編號是否正確";
                    IsOK = false;
                }
            }
        }

        if (IsOK)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = @"SELECT request_id FROM tcDeliveryRequests With(Nolock) WHERE check_number =N'" + ddl_check_number2.SelectedValue + "'";
                cmd.CommandText = strSQL;

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        lbErr_chknum.Text = "此託運單號已設定，請重新確認!";
                        IsOK = false;
                    }
                }
            }
        }


        lbErr_chknum.Visible = !IsOK;
        return IsOK;
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("託運單");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 30;
            sheet1.Column(8).Width = 15;
            sheet1.Column(9).Width = 30;


            sheet1.Cells[1, 1, 1, 9].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 9].Value = "託運清單";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 9].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 9].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 9].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 9].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中


            sheet1.Cells[2, 1, 2, 9].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 9].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 9].Merge = true;
            DateTime dt_S, dt_E;
            if (rb1.Checked)
            {
                if (DateTime.TryParse(tb_dateS1.Text, out dt_S) && DateTime.TryParse(tb_dateE1.Text, out dt_E))
                {
                    sheet1.Cells[2, 1, 2, 1].Value = "建單區間：" + dt_S.ToString("yyyy/MM/dd") + "~" + dt_E.ToString("yyyy/MM/dd");
                }
            }
            else if (rb2.Checked)
            {
                if (DateTime.TryParse(tb_dateS2.Text, out dt_S) && DateTime.TryParse(tb_dateE2.Text, out dt_E))
                {
                    sheet1.Cells[2, 1, 2, 1].Value = "發送區間：" + dt_S.ToString("yyyy/MM/dd") + "~" + dt_E.ToString("yyyy/MM/dd");
                }
            }
            //else if (!string.IsNullOrEmpty(check_number.Text.Trim()))//rb3.Checked
            //{
            //    sheet1.Cells[2, 1, 2, 1].Value = "託運單號：" + check_number.Text;
            //}

            if (!string.IsNullOrEmpty(check_number.Text.Trim()))//rb3.Checked
            {
                sheet1.Cells[2, 1, 2, 1].Value = "託運單號：" + check_number.Text;
            }

            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;



            sheet1.Cells[3, 1, 3, 9].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1, 3, 9].Style.Font.Size = 12;
            sheet1.Cells[3, 1, 3, 9].Style.Font.Bold = true;
            sheet1.Cells[3, 1, 3, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 9].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 9].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "貨號";
            sheet1.Cells[3, 3].Value = "配送日";
            sheet1.Cells[3, 4].Value = "發送站";
            sheet1.Cells[3, 5].Value = "寄件人";
            sheet1.Cells[3, 6].Value = "收件人";
            sheet1.Cells[3, 7].Value = "地址";
            sheet1.Cells[3, 8].Value = "區配商代碼";
            sheet1.Cells[3, 9].Value = "類型";

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["check_number"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["supplier_date"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["sendstation2"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["send_contact"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["receive_contact"].ToString();
                sheet1.Cells[i + 4, 7].Value = DT.Rows[i]["receive_city"].ToString() + DT.Rows[i]["receive_area"].ToString() + DT.Rows[i]["address"].ToString();
                sheet1.Cells[i + 4, 8].Value = DT.Rows[i]["customer_code"].ToString();
                sheet1.Cells[i + 4, 9].Value = DT.Rows[i]["DeliveryType"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("託運清單.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string request_id = e.CommandArgument.ToString();
        string errMsg = string.Empty;

        switch (e.CommandName)
        {
            case "cmdUnCancel":
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@cancel_date", DBNull.Value);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                    cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);

                    try
                    {
                        dbAdapter.execNonQuery(cmd);
                    }
                    catch (Exception ex)
                    {
                        errMsg = ex.Message;
                    }
                }

                if (errMsg == "")
                {
                    #region  Log
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "usp_DeliveryRequesRecord";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@request_id", request_id);
                        cmd.Parameters.AddWithValue("@action", "D");
                        cmd.Parameters.AddWithValue("@memo", "取消銷單");
                        try
                        {
                            dbAdapter.execNonQuery(cmd);
                        }
                        catch (Exception ex)
                        {

                            if (Session["account_code"] != null) errMsg = "【" + Session["account_code"] + "】";
                            errMsg = "銷單:" + Request.RawUrl + errMsg + ": " + ex.ToString();

                            //錯誤寫至Log
                            PublicFunction _fun = new PublicFunction();
                            _fun.Log(errMsg, "S");
                        }
                    }
                    #endregion

                    paramlocation();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('取消完畢。');</script>", false);
                }

                break;
        }
    }
}