﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LT_intrustReport : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            Send_date.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別

            switch (manager_type)
            {
                case "0":
                case "1":
                    supplier_code = "";
                    break;
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }

            string wherestr = "";

            #region 配送站站所
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

            ddlSendStation.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
            ddlSendStation.DataValueField = "station_code";
            ddlSendStation.DataTextField = "showname";
            ddlSendStation.DataBind();
            if (manager_type == "0" || manager_type == "1") ddlSendStation.Items.Insert(0, new ListItem("全部", ""));
            if (ddlSendStation.Items.Count > 0) ddlSendStation.SelectedIndex = 0;
            #endregion



            if (Request.QueryString["Send_date"] != null)
            {
                if (Request.QueryString["Send_date"] != "")
                {
                    Send_date.Text = Request.QueryString["Send_date"];
                }
            }
            if (Request.QueryString["SendStation"] != null)
            {
                if (Request.QueryString["SendStation"] != "")
                {
                    ddlSendStation.SelectedValue = Request.QueryString["SendStation"];
                }
            }
            #region 作業人員
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@station", ddlSendStation.SelectedValue);
                cmd.CommandText = string.Format(@"
                                                    select  driver_code,driver_code+' '+driver_name as driver_name from tbDrivers A
                                                    left join tbStation A1 on A.station = A1.station_scode
                                                    where supplier_code = 'Z01' and A.active_flag = 1
                                                    AND  (@station ='' or A1.station_code = @station) ");

                ddlscanname.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
                ddlscanname.DataValueField = "driver_code";
                ddlscanname.DataTextField = "driver_name";
                ddlscanname.DataBind();
                ddlscanname.Items.Insert(0, new ListItem("全部", ""));
            }
            #endregion
            if (Request.QueryString["scanname"] != null)
            {
                ddlscanname.SelectedValue = Request.QueryString["scanname"];
                lbdriver.Text = ddlscanname.SelectedValue;
            }
            if (Request.QueryString["Station"] != null)
            {
                Station.SelectedValue = Request.QueryString["Station"];
            }
            if (Request.QueryString["Send_date"] != null)
            {
                lbprintdate.Text = Request.QueryString["Send_date"];
                readdata();
            }
        }
    }

    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            string money = "";
            cmd.Parameters.Clear();
            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@sdate", Send_date.Text);
            cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));//Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd")

            cmd.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue);
            cmd.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue);
            cmd.Parameters.AddWithValue("@Station", Station.SelectedValue);

            if (Send_date.Text.ToString() != "")
            {
                querystring += "&Send_date=" + Send_date.Text.ToString();
            }

            querystring += "&SendStation=" + ddlSendStation.SelectedValue.ToString();       //配送站
            querystring += "&scanname=" + ddlscanname.SelectedValue.ToString();             //掃描工號
            querystring += "&Station=" + Station.SelectedValue.ToString();                  //區域碼


            #endregion
            cmd.CommandText = string.Format(@"
declare @temp table(
	check_number varchar(20),
	code_name nvarchar(10),
	collection_money int,
	pieces int,
	origin_station nvarchar(20),
	work_station nvarchar(20),
	delivery_station nvarchar(20),
	newest_scan_item nvarchar(10),
	arrive_option nvarchar(10),
	work_time nvarchar(10),
	driver nvarchar(30),
	nothing varchar(10),
	customer_code varchar(20)
)

insert into @temp
select 
A.check_number,B.code_name,ISNULL(A.collection_money,0), A.pieces,
	D.station_scode+' '+D.station_name,
	E.station_scode+' '+E.station_name,
	C.station_scode+' '+C.station_name,
 _arr.arrive_state,--WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'   END  AS
 _arr.arrive_item,
 RIGHT(REPLICATE('0', 2) + CAST(CONVERT(NVARCHAR(2),DATEPART(HOUR,_arr.newest_scandate)) as NVARCHAR), 2)  
 + ':' +
 RIGHT(REPLICATE('0', 2) + CAST(CONVERT(NVARCHAR(2),DATEPART(minute,_arr.newest_scandate)) as NVARCHAR), 2),
case when _arr.driver_code is not null then  _arr.driver_code +' '+ F.driver_name  else _arr.newest_driver_code +' '+ F.driver_name   end ,
 ''  , A.customer_code
from tcDeliveryRequests A With(Nolock)
CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr 
LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
LEFT JOIN tbStation C With(Nolock) on A.area_arrive_code = C.station_scode
LEFT JOIN tbStation D With(Nolock) on A.supplier_code = D.station_code
LEFT JOIN tbStation E With(Nolock) ON _arr.newest_station = E.station_scode
LEFT JOIN tbDrivers F With(Nolock) ON F.driver_code = _arr.driver_code or _arr.newest_driver_code = F.driver_code
WHERE 1 = 1 
--and A.print_date between @sdate and @edate
And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = @Less_than_truckload
AND A.DeliveryType  = 'D'
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND A.pricing_type in ('02')
AND (_arr.arrive_state in ('配送','配達','到著'));

with cte as (
	select *, ROW_NUMBER() over(
		partition by customer_code, check_number
		order by customer_code, check_number
	) row_num
	from @temp
)

select check_number as '託運單號',
	code_name as '傳票類別',
	collection_money as '代收金額',
	pieces as '件數',
	origin_station as '發送站',
	work_station as '作業站',
	delivery_station as '配送站',
	newest_scan_item as '最新作業別',
	arrive_option as '配異說明',
	work_time as '作業時間',
	driver as '司機',
	nothing as '盤點在庫' ,
	customer_code,
	row_num
	from cte 
where customer_code <> 'F3500010002' or
(customer_code = 'F3500010002' and row_num = 1)
                                              
                                                ", strWhereCmd);
            cmd.CommandTimeout = 600;
            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {
                SqlCommand cmds = new SqlCommand();
                string wherestr = "";
                cmds.CommandTimeout = 600;
                cmds.CommandText = string.Format(@"  
WITH TOTAL AS
(
            SELECT  pieces, ISNULL(A.collection_money,0)  money
            FROM tcDeliveryRequests A With(Nolock) 
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
--and A.print_date between @sdate and @edate

And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = @Less_than_truckload
AND A.DeliveryType  = 'D'
AND A.pricing_type in ('02')
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND (_arr.arrive_state in ('配送','配達','到著'))    
     )
SELECT  ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(money),0) '元' FROM TOTAL
", wherestr);

                cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                cmds.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue);
                cmds.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue);
                cmds.Parameters.AddWithValue("@Station", Station.SelectedValue);
                cmds.Parameters.AddWithValue("@sdate", Send_date.Text);
                cmds.Parameters.AddWithValue("@edate", Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));

                DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    row["託運單號"] = dt.Rows.Count + "筆";
                    row["代收金額"] = dtsum.Rows[0]["元"].ToString();
                    row["件數"] = dtsum.Rows[0]["件數"].ToString();

                    dt.Rows.Add(row);
                }

                New_List.DataSource = dt;
                New_List.DataBind();

                ltotalpages.Text = (dt.Rows.Count-1).ToString();
            }
        }
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";

        if (Send_date.Text != "")
        {
            querystring += "&Send_date=" + Send_date.Text;
        }


        querystring += "&SendStation=" + ddlSendStation.SelectedValue.ToString();
        querystring += "&scanname=" + ddlscanname.SelectedValue;
        querystring += "&Station=" + Station.SelectedValue.ToString();


        Response.Redirect(ResolveUrl("~/LT_intrustReport.aspx?search=yes" + querystring));
    }
    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "當日配送、配達明細表";
        string sqlwhere = "";
        string file_name = "當日配送、配達明細表" + DateTime.Now.ToString("yyyyMMdd");

        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();

        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            string money = "";
            cmd.Parameters.Clear();
            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", 1);
            cmd.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@Station", Station.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@sdate", Send_date.Text);
            cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));
            #endregion
            cmd.CommandText = string.Format(@"
declare @temp table(
	check_number varchar(20),
	code_name nvarchar(10),
	collection_money int,
	pieces int,
	origin_station nvarchar(20),
	work_station nvarchar(20),
	delivery_station nvarchar(20),
	newest_scan_item nvarchar(10),
	arrive_option nvarchar(10),
	work_time nvarchar(10),
	driver nvarchar(30),
	nothing varchar(10),
	customer_code varchar(20)
)

insert into @temp
select 
A.check_number,B.code_name,ISNULL(A.collection_money,0), A.pieces,
	D.station_scode+' '+D.station_name,
	E.station_scode+' '+E.station_name,
	C.station_scode+' '+C.station_name,
 _arr.arrive_state,--WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'   END  AS
 _arr.arrive_item,
 RIGHT(REPLICATE('0', 2) + CAST(CONVERT(NVARCHAR(2),DATEPART(HOUR,_arr.newest_scandate)) as NVARCHAR), 2)  
 + ':' +
 RIGHT(REPLICATE('0', 2) + CAST(CONVERT(NVARCHAR(2),DATEPART(minute,_arr.newest_scandate)) as NVARCHAR), 2),
case when _arr.driver_code is not null then  _arr.driver_code +' '+ F.driver_name  else _arr.newest_driver_code +' '+ F.driver_name   end ,
 ''  , A.customer_code
from tcDeliveryRequests A With(Nolock)
CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr 
LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
LEFT JOIN tbStation C With(Nolock) on A.area_arrive_code = C.station_scode
LEFT JOIN tbStation D With(Nolock) on A.supplier_code = D.station_code
LEFT JOIN tbStation E With(Nolock) ON _arr.newest_station = E.station_scode
LEFT JOIN tbDrivers F With(Nolock) ON F.driver_code = _arr.driver_code or _arr.newest_driver_code = F.driver_code
WHERE 1 = 1 
--and A.print_date between @sdate and @edate
And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = @Less_than_truckload
AND A.DeliveryType  = 'D'
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND A.pricing_type in ('02')
AND (_arr.arrive_state in ('配送','配達','到著'));

with cte as (
	select *, ROW_NUMBER() over(
		partition by customer_code, check_number
		order by customer_code, check_number
	) row_num
	from @temp
)

select check_number as '託運單號',
	code_name as '傳票類別',
	collection_money as '代收金額',
	pieces as '件數',
	origin_station as '發送站',
	work_station as '作業站',
	delivery_station as '配送站',
	newest_scan_item as '最新作業別',
	arrive_option as '配異說明',
	work_time as '作業時間',
	driver as '司機',
	nothing as '盤點在庫' ,
	customer_code,
	row_num
	from cte 
where customer_code <> 'F3500010002' or
(customer_code = 'F3500010002' and row_num = 1)
", strWhereCmd);
            cmd.CommandTimeout = 600;
            dt1 = dbReadOnlyAdapter.getDataTable(cmd);
            SqlCommand cmds = new SqlCommand();
            string wherestr = "";
            cmds.CommandTimeout = 600;
            wherestr += dbReadOnlyAdapter.genWhereCommIn(ref cmds, "pricing_type", "02".Split(','));
            cmds.CommandText = string.Format(@"  
WITH TOTAL AS
(
            SELECT  pieces, ISNULL(A.collection_money,0)  money
            FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
--and A.print_date between @sdate and @edate

And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = 1
AND A.DeliveryType  = 'D'
AND A.pricing_type in ('02')
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND (_arr.arrive_state in ('配送','配達','到著'))  
     )
SELECT  ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(money),0) '元' FROM TOTAL
", wherestr);

            cmds.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue.ToString());
            cmds.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue.ToString());
            cmds.Parameters.AddWithValue("@Station", Station.SelectedValue.ToString());
            //cmds.Parameters.AddWithValue("@print_dates", Send_date.Text);

            cmds.Parameters.AddWithValue("@sdate", Send_date.Text);
            cmds.Parameters.AddWithValue("@edate", Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));

            DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
            if (dt1 != null && dt1.Rows.Count > 0)
            {
                DataRow row = dt1.NewRow();
                row["代收金額"] = dtsum.Rows[0]["元"].ToString();
                row["件數"] = dtsum.Rows[0]["件數"].ToString();
                dt1.Rows.Add(row);
            }
        }

        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@scanname", ddlscanname.SelectedValue.ToString());
            cmd.Parameters.AddWithValue("@Station", Station.SelectedValue.ToString());

            cmd.Parameters.AddWithValue("@sdate", Send_date.Text);
            cmd.Parameters.AddWithValue("@edate", Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));

            //cmd.Parameters.AddWithValue("@print_dates", Send_date.Text);

            cmd.CommandText = string.Format(@" 
DECLARE @TD1 Table
 (
                                        arrive nvarchar(10),
                                        general nvarchar(10),
		                                [Collection] nvarchar(10),
                                        Receivable nvarchar(10),
                                        Uncollected nvarchar(10)
)
 DECLARE @TD2 Table
 (
                                        arrive nvarchar(10),
                                        general nvarchar(10),
		                                [Collection] nvarchar(10),
                                        Receivable nvarchar(10),
                                        Uncollected nvarchar(10)
)
insert into @TD1(arrive,general,[Collection],Receivable,Uncollected)

select arrive,'','',lmoney,'' from 
(
select 
case when arrive is null then '配送' else arrive end arrive,
sum(lmoney) lmoney
 from(
 select 
                                                case B.code_name when '元付' then '一般' else '代收' end codename,
                                                ISNULL(A.collection_money,0)  lmoney, 
                                                A.pieces as pieces,
                                                case when _arr.arrive_item is null then _arr.arrive_state
												else _arr.arrive_state +'-'+ _arr.arrive_item end arrive --WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'   END  AS
                                                -- '配異說明'
                                                from tcDeliveryRequests A with(nolock)
                                                LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
                                                CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
                                                where 1 = 1
--and A.print_date between @sdate and @edate

And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = 1
AND A.DeliveryType  = 'D'
AND A.pricing_type in ('02')
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND (_arr.arrive_state in ('配送','配達','到著'))  
												) A group by arrive
)A

insert into @TD2(arrive,general,[Collection],Receivable,Uncollected)
select PT.arrive,PT.一般,PT.代收,'','' from (
select
case when arrive is null then '配送' else arrive end arrive,
codename,
count(pieces)pieces
from(
select 
                                                case B.code_name when '元付' then '一般' else '代收' end codename,
                                                ISNULL(A.collection_money,0)  lmoney, 
                                                A.pieces as pieces,
                                                case when _arr.arrive_item is null then _arr.arrive_state
												else _arr.arrive_state +'-'+ _arr.arrive_item end arrive --WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'   END  AS
                                                -- '配異說明'
                                                from tcDeliveryRequests A with(nolock)
                                                LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
                                                CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
                                                where 1 = 1
--and A.print_date between @sdate and @edate

And (@station_code = '' or A.area_arrive_code = Replace (@station_code, 'F', ''))
AND (
( _arr.send_date between @sdate and @edate )
or 
( _arr.arrive_date between @sdate and @edate )
)
and A.cancel_date IS NULL 
AND A.Less_than_truckload  = 1
AND A.DeliveryType  = 'D'
AND A.pricing_type in ('02')
AND (@scanname = '' or _arr.driver_code = @scanname or _arr.newest_driver_code = @scanname)
AND (@Station = '' or A.supplier_code = @Station )
AND (_arr.arrive_state in ('配送','配達','到著'))  
												) A group by arrive,codename ) Mytable
Pivot(
	sum(pieces) 
	for Mytable.codename in ( [一般], [代收])
) PT

select
A.arrive '配達區分統計',
'' 'A',
B.general '一般',
B.[Collection] '代收',
case when A.arrive <> '配達-拒收' then A.Receivable end '代收應收',
case when A.arrive = '配達-拒收' then A.Receivable end '代收未收' ,
'' '收款簽字'
from @TD1 A inner join @TD2 B on A.arrive =B.arrive
");
            cmd.CommandTimeout = 600;
            dt2 = dbReadOnlyAdapter.getDataTable(cmd);


            if (dt1 != null && dt1.Rows.Count > 0 || dt2 != null && dt2.Rows.Count > 0)
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    p.Workbook.Properties.Title = "當日配送、配達明細表";
                    p.Workbook.Worksheets.Add("當日配送配達明細表");
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];

                    int colIndex = 1;
                    int rowIndex = 2;
                    ws.Cells[1, 1].Value = "當日配送、配達明細表";
                    ws.Cells[1, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;

                    ws.Cells[1, 1].Style.Font.Name = "標楷體";
                    ws.Cells[1, 1].Style.Font.Size = 12;
                    ws.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[1, 1].Style.Fill.BackgroundColor.SetColor(Color.White);
                    ws.Cells[1, 1, 1, 12].Merge = true;
                    ws.Cells[1, 1, 1, 12].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[1, 1, 1, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[1, 1, 1, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[1, 1, 1, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    ws.Cells[2, 1].Value = string.Format(@"配送日期：{0} 配送站：{1} 掃讀工號：{2} 區域碼：{3}", Send_date.Text, ddlSendStation.SelectedValue.ToString(), ddlscanname.SelectedValue.ToString(), Station.SelectedValue.ToString());
                    ws.Cells[2, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[2, 1].Style.Font.Name = "標楷體";
                    ws.Cells[2, 1].Style.Font.Size = 12;
                    ws.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ws.Cells[2, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[2, 1].Style.Fill.BackgroundColor.SetColor(Color.White);
                    ws.Cells[2, 1, 2, 12].Merge = true;
                    ws.Cells[2, 1, 2, 12].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[2, 1, 2, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[2, 1, 2, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[2, 1, 2, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;
                    foreach (DataColumn dc in dt1.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(Color.LightGray);

                        //Setting Top/left,right/bottom borders.
                        var border = cell.Style.Border;
                        border.Bottom.Style = ExcelBorderStyle.Thin;
                        border.Top.Style = ExcelBorderStyle.Thin;
                        border.Left.Style = ExcelBorderStyle.Thin;
                        border.Right.Style = ExcelBorderStyle.Thin;

                        //Setting Value in cell
                        cell.Value = dc.ColumnName;
                        colIndex++;
                    }
                    rowIndex++;

                    foreach (DataRow dr in dt1.Rows)
                    {
                        colIndex = 1;
                        foreach (DataColumn dc in dt1.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            cell.Value = dr[dc.ColumnName];

                            //Setting borders of cell
                            var border = cell.Style.Border;
                            border.Bottom.Style = ExcelBorderStyle.Thin;
                            border.Top.Style = ExcelBorderStyle.Thin;
                            border.Left.Style = ExcelBorderStyle.Thin;
                            border.Right.Style = ExcelBorderStyle.Thin;
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    rowIndex++;

                    ws.Cells[rowIndex, 1].Value = "配達區分統計";
                    ws.Cells[rowIndex, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 1].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 1].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 1].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[rowIndex, 1, rowIndex, 2].Merge = true;
                    ws.Cells[rowIndex, 1, rowIndex, 2].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 1, rowIndex, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 1, rowIndex, 2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 1, rowIndex, 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    ws.Cells[rowIndex, 3].Value = "一般";
                    ws.Cells[rowIndex, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 3].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 3].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 3].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 3].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 3].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[rowIndex, 4].Value = "代收";
                    ws.Cells[rowIndex, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 4].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 4].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 4].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 4].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 4].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[rowIndex, 5].Value = "代收應收";
                    ws.Cells[rowIndex, 5].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 5].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 5].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 5].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[rowIndex, 6].Value = "代收未收";
                    ws.Cells[rowIndex, 6].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 6].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 6].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 6].Style.Fill.BackgroundColor.SetColor(Color.LightGray);

                    ws.Cells[rowIndex, 7].Value = "收款簽字";
                    ws.Cells[rowIndex, 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[rowIndex, 7].Style.Font.Name = "標楷體";
                    ws.Cells[rowIndex, 7].Style.Font.Size = 12;
                    ws.Cells[rowIndex, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[rowIndex, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    ws.Cells[rowIndex, 7].Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    ws.Cells[rowIndex, 7, rowIndex, 12].Merge = true;
                    ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    foreach (DataRow dr in dt2.Rows)
                    {
                        colIndex = 1;
                        foreach (DataColumn dc in dt2.Columns)
                        {
                            if (colIndex == 1)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Top.Style = ExcelBorderStyle.Thin;
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Right.Style = ExcelBorderStyle.Thin;

                                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                ws.Cells[rowIndex, 1, rowIndex, 2].Merge = true;
                            }
                            else if (colIndex == 7)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Top.Style = ExcelBorderStyle.Thin;
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Right.Style = ExcelBorderStyle.Thin;
                                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                ws.Cells[rowIndex, 7, rowIndex, 12].Merge = true;
                                ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                ws.Cells[rowIndex, 7, rowIndex, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            }
                            else {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];
                                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Bottom.Style = ExcelBorderStyle.Thin;
                                border.Top.Style = ExcelBorderStyle.Thin;
                                border.Left.Style = ExcelBorderStyle.Thin;
                                border.Right.Style = ExcelBorderStyle.Thin;
                            }
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    ws.Cells.AutoFitColumns();
                    ws.View.FreezePanes(2, 1);
                    ws.Cells.Style.Font.Size = 12;
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(p.GetAsByteArray());
                    Response.End();
                }

                //string strLink = "";
                //strLink = string.Format(
                //                  "downloadReportExcel.aspx?station_code={0}&scanname={1}&Station={2}&sdate={3}&edate={4}"
                //                 , ddlSendStation.SelectedValue.ToString()
                //                 , ddlscanname.SelectedValue.ToString()
                //                 , Station.SelectedValue.ToString()
                //                 , Send_date.Text
                //                 , Convert.ToDateTime(Send_date.Text).AddDays(1).ToString("yyyy/MM/dd"));//"2019-05-30"

                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message",
                //        string.Format(
                //            @"<script>{0}</script>"
                //            , "$('#_dw').attr('href','" + strLink + "');$('#_dw')[0].click(); $('#_dw').attr('href','#');"
                //            )
                //        , false);

            }
            else
            {

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "alert('無配達資料!!'); ", true);
                return;
            }
        }
    }


    protected void ddlSendStation_SelectedIndexChanged(object sender, EventArgs e)
    {

        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@station", ddlSendStation.SelectedValue);


            cmd.CommandText = string.Format(@"
select  driver_code,driver_code+' '+driver_name as driver_name from tbDrivers A
left join tbStation A1 on A.station = A1.station_scode
where supplier_code = 'Z01' and A.active_flag = 1
AND  (@station ='' or A1.station_code = @station) 
order by driver_code
");//WHERE rn = 1


            ddlscanname.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
            ddlscanname.DataValueField = "driver_code";
            ddlscanname.DataTextField = "driver_name";
            ddlscanname.DataBind();
            ddlscanname.Items.Insert(0, new ListItem("全部", ""));
        }
        #endregion

    }
}