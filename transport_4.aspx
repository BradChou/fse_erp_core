﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="transport_4.aspx.cs" Inherits="transport_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                init();
            });
        });

        function init() {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                defaultDate: (new Date())  //預設當日
            });

             $("#<%= option.ClientID%>").change(function () {
              var type = $("#<%= option.ClientID%> option:selected").val()
                 if (type == '0') {
                     $('#divStartfrom').show(); 
                 }
                 else {
                     $('#divStartfrom').hide();
                     $("#<%= StartFrom.ClientID%>").val('1');
                 }
            });
            
        }
    </script>

    <style>
        input[type="radio"] {
          
            display: inherit; 
            
        }
    
        label{
            text-align:left;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        
        .auto-style1 {
            width: 206px;
        }

        
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hid_sel" runat="server" />
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">標籤列印</h2>


            <div class="templatemo-login-form">
                <!---->
                <div class="form-group form-inline">
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged" AutoPostBack ="true" ></asp:DropDownList>
                    <asp:DropDownList ID="Customer" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                    <asp:DropDownList ID="rb_pricing_type" runat="server" CssClass="form-control">
                        <asp:ListItem>論板</asp:ListItem>
                        <asp:ListItem>專車</asp:ListItem>                        
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem >未列印</asp:ListItem>
                        <asp:ListItem >已列印</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lbDate" runat="server" Text="發送日期"></asp:Label>
                    <asp:TextBox ID="date1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>~
                    <asp:TextBox ID="date2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <%--<asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>                    --%>
                    <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20" ></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                </div>
            </div>
            <hr>
            <%--<p>※筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>--%>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lbErr" runat="server" ForeColor="Red" Text="※請勾選要列印的託運單"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode ="Conditional">
                <ContentTemplate>
                    <div class="inner-wrap" style="overflow: auto; height: calc(20vh); width: 100%">
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="auto-style1">
                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                </th>
                                <th>建檔日期</th>
                                <th>發送日期</th>
                                <th>配送日期</th>
                                <th>貨號</th>
                                <th>收貨人</th>
                                <th>電話</th>
                                <th>地址</th>
                                <th>件數</th>
                                <th>板數</th>
                                <th>才數</th>
                                <th>傳票類別</th>
                                <th>付款</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="列印">
                                            <asp:CheckBox ID="chkRow" runat="server" />
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                            <asp:HiddenField ID="Hid_print_flag" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_flag").ToString())%>' />
                                            <asp:HiddenField ID="Hid_arrive_assign_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date","{0:yyyy/MM/dd}").ToString())%>' />                                            
                                        </td>
                                        <td data-th="建檔日期">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate","{0:yyyy/MM/dd}").ToString())%>
                                        </td>
                                        <td data-th="發送日期">
                                            <asp:Label ID="lbprint_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="配送日期">
                                            <asp:Label ID="lbSupplier_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="貨號">
                                            <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%> </td>
                                        <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1").ToString())%>#<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1_ext").ToString())%></td>
                                        <td data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address").ToString())%></td>
                                        <td data-th="件數">
                                            <asp:Label ID="lbpieces" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%>'></asp:Label> 
                                        </td>
                                        <td data-th="板數">
                                            <asp:Label ID="lbplates" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbm").ToString())%> </td>
                                        <td data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                        <td data-th="付款"></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="13" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                    <p>列印勾選筆數：<span class="chkcount" ></span></p>                    
                </ContentTemplate>
              <%--  <Triggers>
                    <asp:PostBackTrigger ControlID="search" />
                    <asp:PostBackTrigger ControlID="btnPirntLabel" />
                </Triggers>--%>
            </asp:UpdatePanel>



            <%-- <div class="pagination-wrap">
             <ul class="pagination">
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li class="active"><a href="#">3 <span class="sr-only">(current)</span></a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                  <span aria-hidden="true"><i class="fa fa-play"></i></span>
                </a>
              </li>
            </ul>
          </div>--%>

            <hr>

           <div class="templatemo-login-form">
                <!---->
                <div class="form-style-10">
                    <div class="section"><span>1</span>進行列印</div>
                    <div class="inner-wrap">
                        <%--<button type="submit" class="templatemo-blue-button">進行列印</button>--%>

                        <div class="form-group form-inline">
                            <asp:Button ID="btnPirntLabel" CssClass="btn btn-primary" runat="server" Text="進行列印" OnClick="btnPirntLabel_Click1" />
                            <asp:DropDownList ID="option" runat="server" class="form-control">
                                <asp:ListItem Value="0">A4 (一式6筆託運標籤)</asp:ListItem>
                                <asp:ListItem Value="2">A4 (一式1筆託運標籤)</asp:ListItem>
                                <asp:ListItem Value="1">捲筒列印</asp:ListItem>
                            </asp:DropDownList>
                            <div id="divStartfrom" class="form-group "  >
                                自第<asp:TextBox ID="StartFrom" runat="server"  class="form-control" MaxLength="1" Width="50">1</asp:TextBox>格續印
                            </div>
                            <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="StartFrom" ErrorMessage="需介於 1~6" ForeColor="Red" MaximumValue="6" MinimumValue="1" Type="Integer" ></asp:RangeValidator>
                            <br >
                           
                            <div class="templatemo-inline-block">
                              <ContentTemplate>
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="※請勾選列印張數依照棧數或件數"></asp:Label>
                              </ContentTemplate>
                            <br >
                                <asp:RadioButtonList ID="print_number" Style="" RepeatDirection="Horizontal" RepeatColumns="0" RepeatLayout="Flow" runat="server" CssClass="radio radio-success" >
                                    <asp:ListItem Value="1" Text=" 板數"></asp:ListItem>
                                    <asp:ListItem Value="0" Text=" 件數"></asp:ListItem>
                                </asp:RadioButtonList>
                
                      
                        
                        
                    


                           <%-- <select name="select" class="form-control">
                                <option>A4 (一式6筆託運標籤)</option>
                                <option></option>
                            </select>--%>
                        </div>
                        <%--<p class="text-primary">貨號(條碼)產生中……</p>--%>
                    </div>
                    <div class="section"><span>2</span>列印託運總表</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btPrint" class="templatemo-blue-button" runat="server" Text="列印託運總表" OnClick="btPrint_Click" />
                        <asp:ListBox ID="lbMsg" runat="server" Width="100%" CssClass="lb_set" Visible="false"></asp:ListBox>
                    </div>
                    <%--<div class="section"><span>3</span>列印選擇</div>
               <div class="inner-wrap">
                 <div class="form-group form-inline">
                   <button type="submit" class="templatemo-blue-button">列印</button>
                   <select name="select" class="form-control">
                     <option>A4 (一式6筆託運標籤)</option>
                     <option></option>
                   </select>
                 </div>
               </div>--%>
                </div>
            </div>
        
    </div>
</asp:Content>

