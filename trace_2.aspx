﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTrace.master" AutoEventWireup="true" CodeFile="trace_2.aspx.cs" Inherits="trace_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">配送查詢</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">配送站所</label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                        <asp:DropDownList ID="Customer" runat="server" CssClass="form-control" ></asp:DropDownList>
                        <%--<select class="form-control">
                            <option>全選</option>
                        </select>--%>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">員工代號</label>
                        <asp:DropDownList ID="Driver" runat="server" CssClass="form-control"></asp:DropDownList>
                        <%--<input type="date" class="form-control">--%>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">發送日期</label>
                        <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                        <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <%-- <select class="form-control">
                            <option>00</option>
                        </select>--%>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: -1px; top: 3px; margin-top: 0px">
                        <label for="inputLastName">掃讀時間(起~迄)</label>
                        <asp:DropDownList ID="stime" runat="server" CssClass="form-control">
                            <asp:ListItem Value="00:00:00.000">00</asp:ListItem>
                            <asp:ListItem Value="01:00:00.000">01</asp:ListItem>
                            <asp:ListItem Value="02:00:00.000">02</asp:ListItem>
                            <asp:ListItem Value="03:00:00.000">03</asp:ListItem>
                            <asp:ListItem Value="04:00:00.000">04</asp:ListItem>
                            <asp:ListItem Value="05:00:00.000">05</asp:ListItem>
                            <asp:ListItem Value="06:00:00.000">06</asp:ListItem>
                            <asp:ListItem Value="07:00:00.000">07</asp:ListItem>
                            <asp:ListItem Value="08:00:00.000">08</asp:ListItem>
                            <asp:ListItem Value="09:00:00.000">09</asp:ListItem>
                            <asp:ListItem Value="10:00:00.000">10</asp:ListItem>
                            <asp:ListItem Value="11:00:00.000">11</asp:ListItem>
                            <asp:ListItem Value="12:00:00.000">12</asp:ListItem>
                            <asp:ListItem Value="13:00:00.000">13</asp:ListItem>
                            <asp:ListItem Value="14:00:00.000">14</asp:ListItem>
                            <asp:ListItem Value="15:00:00.000">15</asp:ListItem>
                            <asp:ListItem Value="16:00:00.000">16</asp:ListItem>
                            <asp:ListItem Value="17:00:00.000">17</asp:ListItem>
                            <asp:ListItem Value="18:00:00.000">18</asp:ListItem>
                            <asp:ListItem Value="19:00:00.000">19</asp:ListItem>
                            <asp:ListItem Value="20:00:00.000">20</asp:ListItem>
                            <asp:ListItem Value="21:00:00.000">21</asp:ListItem>
                            <asp:ListItem Value="22:00:00.000">22</asp:ListItem>
                            <asp:ListItem Value="23:00:00.000">23</asp:ListItem>
                        </asp:DropDownList>
                        <%-- <select class="form-control">
                                <option>23</option>
                            </select>--%>~ 
                        <asp:DropDownList ID="etime" runat="server" CssClass="form-control">
                            <asp:ListItem Value="00:59:59.997">00</asp:ListItem>
                            <asp:ListItem Value="01:59:59.997">01</asp:ListItem>
                            <asp:ListItem Value="02:59:59.997">02</asp:ListItem>
                            <asp:ListItem Value="03:59:59.997">03</asp:ListItem>
                            <asp:ListItem Value="04:59:59.997">04</asp:ListItem>
                            <asp:ListItem Value="05:59:59.997">05</asp:ListItem>
                            <asp:ListItem Value="06:59:59.997">06</asp:ListItem>
                            <asp:ListItem Value="07:59:59.997">07</asp:ListItem>
                            <asp:ListItem Value="08:59:59.997">08</asp:ListItem>
                            <asp:ListItem Value="09:59:59.997">09</asp:ListItem>
                            <asp:ListItem Value="10:59:59.997">10</asp:ListItem>
                            <asp:ListItem Value="11:59:59.997">11</asp:ListItem>
                            <asp:ListItem Value="12:59:59.997">12</asp:ListItem>
                            <asp:ListItem Value="13:59:59.997">13</asp:ListItem>
                            <asp:ListItem Value="14:59:59.997">14</asp:ListItem>
                            <asp:ListItem Value="15:59:59.997">15</asp:ListItem>
                            <asp:ListItem Value="16:59:59.997">16</asp:ListItem>
                            <asp:ListItem Value="17:59:59.997">17</asp:ListItem>
                            <asp:ListItem Value="18:59:59.997">18</asp:ListItem>
                            <asp:ListItem Value="19:59:59.997">19</asp:ListItem>
                            <asp:ListItem Value="20:59:59.997">20</asp:ListItem>
                            <asp:ListItem Value="21:59:59.997">21</asp:ListItem>
                            <asp:ListItem Value="22:59:59.997">22</asp:ListItem>
                            <asp:ListItem Selected="True" Value="23:59:59.997">23</asp:ListItem>
                        </asp:DropDownList>
                        <%--<div class="margin-right-15 templatemo-inline-block">
                            <input type="radio" name="radio" id="r1" value="" checked>
                            <label for="r1" class="font-weight-400"><span></span>全部明細</label>
                        </div>
                        <div class="margin-right-15 templatemo-inline-block">
                            <input type="radio" name="radio" id="r2" value="">
                            <label for="r2" class="font-weight-400"><span></span>未配達/異常配達明細</label>
                        </div>--%>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-12 form-group form-inline">
                        <label for="inputFirstName">顯示方式</label>
                        <asp:RadioButtonList ID="rbReportType" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" ToolTip="請選擇查詢類型!" RepeatLayout="Flow" style="left: 2px; top: -1px">
                            <asp:ListItem Value="0" Selected="True">全部明細&nbsp;&nbsp;&nbsp;</asp:ListItem>
                            <asp:ListItem Value="1">未配達/異常配達明細</asp:ListItem>
                        </asp:RadioButtonList>

                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_area").ToString())%>
                    </div>
                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btnQry" CssClass="btn btn-primary" runat="server" Text="查詢" OnClick="btnQry_Click" />
                </div>
            </div>
            <hr>
            <p class="text-primary">配送狀況查詢</p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>站所</th>
                    <th>作業人員</th>
                    <th>貨號</th>
                    <th>出貨人</th>
                    <th>收貨人</th>
                    <th>商品種類</th>
                    <th>發送件數</th>
                    <th>配送件數</th>
                    <th>配送時間</th>
                    <th>配達時間</th>
                    <th>配達區分</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                            <td data-th="作業人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                            <td data-th="貨號">
                                <a href="trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>&date=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%>" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></a>
                                </td>
                            <td data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="商品種類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.product_category_name").ToString())%></td>
                            <td data-th="發送件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                            <td data-th="配送件數">1</td>
                            <td data-th="配送時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                            <td data-th="配達時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_date").ToString())%></td>
                            <td data-th="配達區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_option").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="12" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                總筆數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
            </div>
        </div>
    </div>
</asp:Content>

