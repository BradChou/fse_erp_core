﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets2_1_edit.aspx.cs" Inherits="assets2_1_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet"></link>
    <script src="js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-sliderAccess.js" type="text/javascript"></script>
    <script src="js/timepicker/jquery-ui-timepicker-zh-TW.js" type="text/javascript"></script>

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            $('.datetimepicker').prop("readonly", true).timepicker({
                showSecond: true, //顯示秒  
                timeFormat: 'HH:mm:ss', //格式化時間  
                controlType: "select"
            });

            $("._feeitem").change(function() {
                CalAmount();
            });

            $('.fancybox').fancybox();

            $("#carsel").fancybox({
                'width': 1200,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

        });

        function ValidateFloat2(e, pnumber) {
            if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
                var newValue = /\d+[.]?[1-9]?/.exec(e.value);
                if (newValue != null) {
                    e.value = newValue;
                }
                else {
                    e.value = "";
                }
            }
            return false;
        }

        function num(obj) {
            obj.value = obj.value.replace(/[^\d.]/g, ""); //清除"數字"和"."以外的字元
            obj.value = obj.value.replace(/^\./g, ""); //驗證第一個字元是數字
            obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一個, 清除多餘的
            obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
            obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'); //只能輸入兩位小數
        }

        function CalAmount() {
            var ownership = $("#<%= Hid_ownership.ClientID%>").val();  //所有權
            var FeeItem = $('._feeitem').val();
            var quant = $("._quant").val();
            var litre = $("._litre").val();
            var price_notax = $("._price_notax").val();
            var buy_notax = $("._buy_notax").val();
            var total_buy = 0;
            var receipt_notax = 0;
            var company = "";
            if (FeeItem == "1") {
                if ($.isNumeric(litre) && $.isNumeric(price_notax)) {
                    $("._total_price").val(litre * price_notax);
                    
                }
            }
            else if (FeeItem == "2") { //ETC
                if ($.isNumeric(quant) && $.isNumeric(buy_notax)) {
                    total_buy = quant * buy_notax;
                    $("._total_buy").val(total_buy);
                    switch (ownership)
                    {
                        //case "1":  //自有車
                        //    break;
                        case "2":  //個人車主
                            receipt_notax = parseInt(parseInt(buy_notax) * 1.07);
                            break;
                        //case "3":  //區配
                        //    break;
                        //case "4":  //外車
                            //    break;
                        default:
                            receipt_notax = parseInt(parseInt(buy_notax) * 1.1);
                            break;
                    }

                    
                    $("._receipt_notax").val(receipt_notax);
                    $("._total_receipt").val(parseInt(receipt_notax) * quant);
                }
            }
            else if (FeeItem == "3") { //GPS
                if ($.isNumeric(quant) && $.isNumeric(buy_notax)) {
                    total_buy = quant * buy_notax;
                    $("._total_buy").val(total_buy);
                    var company = $("._company").val();
                    if (company == "漢名") {
                        receipt_notax = parseInt(buy_notax) + 50;
                    }
                    else {
                        receipt_notax = parseInt(buy_notax);
                    }
                    $("._receipt_notax").val(receipt_notax);
                    $("._total_receipt").val(parseInt(receipt_notax) * quant);
                }
            }
            else if (FeeItem == "4") { //修繕費
                if ($.isNumeric(quant) && $.isNumeric(buy_notax)) {
                    total_buy = quant * buy_notax;
                    $("._total_buy").val(total_buy);
                    receipt_notax = parseInt(parseInt(buy_notax) * 1.05);
                    $("._receipt_notax").val(receipt_notax);
                    $("._total_receipt").val(parseInt(receipt_notax) * quant);
                }
            }
            else if (FeeItem == "7") { //罰單
                if ($.isNumeric(quant) && $.isNumeric(buy_notax)) {
                    total_buy = quant * buy_notax;
                    $("._total_buy").val(total_buy);
                    receipt_notax = parseInt(buy_notax) + 50;
                    $("._receipt_notax").val(receipt_notax);
                    $("._total_receipt").val(parseInt(receipt_notax) * quant);
                }
            }
            else {
                if ($.isNumeric(quant) && $.isNumeric(buy_notax)) {
                    total_buy = quant * buy_notax;
                    $("._total_buy").val(total_buy);
                    receipt_notax = parseInt(buy_notax);
                    $("._receipt_notax").val(receipt_notax);
                    $("._total_receipt").val(parseInt(receipt_notax) * quant);

                }
            }


        }


        $(document).ready(function () {
           $("#<%= ddl_fee.ClientID%>").change(function () {
                var value = $(this).val();
                switch (value) {
                    case "1":
                        $("#<%= lblitre.ClientID%>").attr("style", "display:inline-block;");
                        $("#<%= litre.ClientID%>").attr("style", "display:inline-block;");
                        $("#<%= lbquant.ClientID%>").attr("style", "display:none;");
                        $("#<%= quant.ClientID%>").attr("style", "display:none;");
                        break;
                    default:
                        $("#<%= lblitre.ClientID%>").attr("style", "display:none;");
                        $("#<%= litre.ClientID%>").attr("style", "display:none;");
                        $("#<%= lbquant.ClientID%>").attr("style", "display:inline-block;");
                        $("#<%= quant.ClientID%>").attr("style", "display:inline-block;");
                        break;
                }

            });
        });

    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form-group" style="float: right;">
        <%--<a href="assets2_1_edit.aspx" ><span class="btn btn-danger glyphicon glyphicon-plus">刪除</span></a>--%>
    </div>
    <h2 class="margin-bottom-10">廠商請款資料維護-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>

    <div class="templatemo-login-form">
        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">基本資料</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>費用類別</label>
                            <asp:DropDownList ID="ddl_fee" runat="server" class="form-control _feeitem" onselectedindexchanged="fee_SelIndexChanged" AutoPostBack="True" ></asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_fee" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇費用類別</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>車牌</label>
                            <asp:TextBox ID="car_license" runat="server" class="form-control" Enable="false"></asp:TextBox>
                            <a id="carsel"  href="assets1_9_carsel.aspx?car_license_no=<%=car_license_no.ClientID %>&car_license=<%= car_license.ClientID %>&Hid_assets_id=<%=Hid_assets_id.ClientID%>&Hid_ownership=<%=Hid_ownership.ClientID%>&Button1=<%=Button1.ClientID%>" class="fancybox fancybox.iframe "  ><span class="btn btn-link small  ">選取
                            </span></a>
                            <asp:HiddenField ID="car_license_no" runat="server" /> 
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" style="display:none" />
                                <asp:HiddenField ID="Hid_assets_id" runat="server" />
                                <asp:HiddenField ID="Hid_ownership" runat="server" />
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入車牌</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="tonnes">　</label>
                            <asp:CheckBox ID="chkfee" CssClass=" checkbox checkbox-success"  Text="攤提"  Enabled="false" runat="server" />
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="month"><span class="REDWD_b">*</span>交易時間</label>
                            <asp:TextBox ID="idate" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="idate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入交易日期</asp:RequiredFieldValidator>
                            <asp:TextBox ID="itime" runat="server" class="datetimepicker form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="re3" runat="server" ControlToValidate="itime" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入交易時間</asp:RequiredFieldValidator>--%>                            
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="year">費用期間</label>
                            <asp:TextBox ID="fee_sdate" runat="server" class="form-control date_picker"  autocomplete="off" OnTextChanged="sdate_TextChanged" AutoPostBack="true"></asp:TextBox>~
                            <asp:TextBox ID="fee_edate" runat="server" class="form-control date_picker"  autocomplete="off" OnTextChanged="edate_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="oil">油品公司</label>
                            <asp:DropDownList ID="oil" runat="server" class="form-control"></asp:DropDownList>
                            <%--<asp:TextBox ID="oil" runat="server" class="form-control" MaxLength="10"></asp:TextBox>--%>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="company">公司名稱</label>
                            <asp:TextBox ID="company" runat="server" class="form-control _company" MaxLength="20"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="items">項目</label>
                            <asp:TextBox ID="items" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="detail">明細</label>
                            <asp:TextBox ID="detail" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline form-group ">
                            <label for="quant" id="lbquant" runat="server">數量</label>
                            <asp:TextBox ID="quant" runat="server" class="form-control _quant" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');CalAmount();"></asp:TextBox>
                            <label for="litre" id="lblitre" runat="server" >公升數</label>
                            <asp:TextBox ID="litre" runat="server" class="form-control _litre" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');CalAmount();" ></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="age">牌價(未稅)</label>
                            <asp:TextBox ID="price_notax" runat="server" class="form-control _price_notax" onkeyup="num(this);CalAmount();"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="total_price">牌價總額(未稅)</label>
                            <asp:TextBox ID="total_price" runat="server" class="form-control _total_price" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="engine_number">里程數</label>
                            <asp:TextBox ID="milage" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand">購入單價(未稅)</label>
                            <asp:TextBox ID="buy_price_notax" runat="server" class="form-control _buy_notax" onkeyup="num(this);CalAmount();"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="total_buy">購入總額(未稅)</label>
                            <asp:TextBox ID="total_buy" runat="server" class="form-control _total_buy" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="memo">備註</label>
                            <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="brand">收款單價(未稅)</label>
                            <asp:TextBox ID="receipt_price_notax" runat="server" class="form-control _receipt_notax" onkeyup="num(this);"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="total_receipt">收款總額(未稅)</label>
                            <asp:TextBox ID="total_receipt" runat="server" class="form-control _total_receipt" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-center" style="margin-top: 5px;">
                <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/assets2_1.aspx">取消</asp:LinkButton>
                <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
            </div>

        </div>
    </div>
</asp:Content>

