﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class member_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            MultiView1.ActiveViewIndex = 0;


            //#region  權限
            //string wherestr = "";
            //Boolean view_auth = false;
            //Boolean insert_auth = false;
            //Boolean modify_auth = false;
            //Boolean delete_auth = false;
            //Boolean etc_auth = false;
            //using (SqlCommand cmdauth = new SqlCommand())
            //{
            //    cmdauth.Parameters.AddWithValue("@manager_type", manager_type);
            //    cmdauth.Parameters.AddWithValue("@account_code", account_code);
            //    cmdauth.CommandText = string.Format(@"select view_auth , insert_auth , modify_auth , delete_auth , etc_auth
            //                                         from tbFunctionsAuth 
            //                                         where ((account_code  = @account_code) or (manager_type = @manager_type)) and func_code = 'C1-1'", wherestr);

            //    using (DataTable dt = dbAdapter.getDataTable(cmdauth))
            //    {
            //        if (dt.Rows.Count > 0)
            //        {
            //            view_auth = dt.Rows[0]["view_auth"] != null ? Convert.ToBoolean(dt.Rows[0]["view_auth"]) : false;
            //            insert_auth = dt.Rows[0]["insert_auth"] != null ? Convert.ToBoolean(dt.Rows[0]["insert_auth"]) : false;
            //            modify_auth = dt.Rows[0]["modify_auth"] != null ? Convert.ToBoolean(dt.Rows[0]["modify_auth"]) : false;
            //            delete_auth = dt.Rows[0]["delete_auth"] != null ? Convert.ToBoolean(dt.Rows[0]["delete_auth"]) : false;
            //            etc_auth = dt.Rows[0]["etc_auth"] != null ? Convert.ToBoolean(dt.Rows[0]["etc_auth"]) : false;
            //        }
            //    }
            //}
            //#endregion


            #region  區配商
            string manager_type = Session["manager_type"].ToString(); //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            Suppliers.DataSource = Utility.getSupplierDT( supplier_code , manager_type );
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            //Suppliers.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            #region 發送站-區配商
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select supplier_code, supplier_code + ' ' + supplier_name as showname from tbSuppliers where LEN(supplier_code) > 0 and supplier_code != '000' and active_flag = 1";
                supplier.DataSource = dbAdapter.getDataTable(cmd);
                supplier.DataValueField = "supplier_code";
                supplier.DataTextField = "showname";
                supplier.DataBind();
                supplier.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion


            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            string wherestr = string.Empty;
            if (Less_than_truckload == "1") wherestr = " and code_id  = '02'";
            cmd2.CommandText = string.Format(" select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM' {0} order by code_id asc", wherestr);
            PM_code.DataSource = dbAdapter.getDataTable(cmd2);
            PM_code.DataValueField = "code_id";
            PM_code.DataTextField = "showname";
            PM_code.DataBind();
            PM_code.Items.Insert(0, new ListItem("請選擇", ""));

            SPM_code.DataSource = PM_code.DataSource;
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion


            //readdata();
            
        }
    }

    private void Clear()
    {
        PM_code.SelectedValue = "";
        SPM_code.Items.Clear();
        City.Items.Clear();
        CityArea.Items.Clear();
        master_code.Text = "";
        second_code.Text = "";
        customer_name.Text = "";
        shortname.Text = "";
        uni_number.Text = "";
        principal.Text = "";
        tel.Text = "";
        fax.Text = "";
        business_people.Text = "";
        address.Text = "";
        ticket_period.SelectedValue = "";
        ticket.Text = "";
        email.Text = "";
        contract_sdate.Text = "";
        contract_edate.Text = "";
        filename.Text = "";
        close_date.SelectedValue = "";
        tbclose.Text = "";
        billing_special_needs.Text = "";
        MultiView1.ActiveViewIndex = 0;
    }

    private void initForm()
    {

        master_code.Text  = Suppliers.SelectedValue + "-";
        second_code.Text = "0" + PM_code.SelectedValue;
        SPM_code.SelectedValue = PM_code.SelectedValue;
        supplier_name.Text = Suppliers.SelectedItem.Text;
        #region 郵政縣市
        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        City.DataSource = dbAdapter.getDataTable(cmd1);
        City.DataValueField = "city";
        City.DataTextField = "city";
        City.DataBind();
        City.Items.Insert(0, new ListItem("請選擇", ""));
        InvoiceCity.DataSource = dbAdapter.getDataTable(cmd1);
        InvoiceCity.DataValueField = "city";
        InvoiceCity.DataTextField = "city";
        InvoiceCity.DataBind();
        InvoiceCity.Items.Insert(0, new ListItem("請選擇縣市別", ""));
        #endregion

        #region 停運原因
        SqlCommand cmd2 = new SqlCommand();
        cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'TS' and active_flag = 1 order by seq asc ";
        stop_code.DataSource = dbAdapter.getDataTable(cmd2);
        stop_code.DataValueField = "code_id";
        stop_code.DataTextField = "code_name";
        stop_code.DataBind();       
        #endregion

        //City_SelectedIndexChanged(null, null);


    }


    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
           
        }

    }

    protected void InvoiceCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (InvoiceCity.SelectedValue != "")
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", InvoiceCity.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    InvoiceArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void PM_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        initForm();
        //應該要判斷該計價模式是否已新增主客代編號
        //如果就不允許新增，只能修改
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#customer_body').removeClass('disabledinput');$('#customer_body').addClass('enabledinput');</script>", false);
        
    }

    protected void insertSameAddress(object sender, EventArgs e)
    {
        InvoiceCity.SelectedIndex = City.SelectedIndex;
        InvoiceCity_SelectedIndexChanged(sender, e);
        InvoiceArea.SelectedIndex = CityArea.SelectedIndex;
        InvoiceAddress.Text = address.Text;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        DateTime dt_temp = new DateTime();
        int i_tmp;
        int customer_type = 0;
        int customer_id = 0;
        #region 序號
        string serialnum = "";
        SqlCommand cmdt = new SqlCommand();
        DataTable dtt;
        cmdt.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue.ToString());
        cmdt.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
        cmdt.CommandText = " select max(supplier_id) as serial from tbCustomers where supplier_code=@supplier_code  ";   //and pricing_code=@pricing_code
        dtt = dbAdapter.getDataTable(cmdt);
        if (dtt.Rows.Count > 0)
        {
            if (dtt.Rows[0]["serial"] != DBNull.Value)
            {
                serialnum = (Convert.ToInt32(dtt.Rows[0]["serial"].ToString()) + 1).ToString();
                while (serialnum.Length < 4)
                {
                    serialnum = "0" + serialnum;
                }

            }
            else
            {
                serialnum = "0000";
            }
        }
        else
        {
            serialnum = "0000";
        }
        #endregion


        #region 新增        
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.AddWithValue("@supplier_id", serialnum);                                       //主客代序號
        cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue.ToString());            //區配商代碼
        cmd.Parameters.AddWithValue("@master_code", Suppliers.SelectedValue.ToString() + serialnum);   //主客代完整代碼
        master_code.Text = Suppliers.SelectedValue.ToString() + serialnum;
        cmd.Parameters.AddWithValue("@second_id", "0");                                               //次客代流水號
        cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());               //計價模式
        cmd.Parameters.AddWithValue("@second_code", "0" + PM_code.SelectedValue.ToString());           //次客代完整代碼
        string customer_code = Suppliers.SelectedValue.ToString() + serialnum + "0" + PM_code.SelectedValue.ToString();
        cmd.Parameters.AddWithValue("@customer_code", customer_code);                                 //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
        cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
        cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());                //客戶簡稱
        cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
        cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間
        if (exclusiveSendStation.Checked == true && supplier.SelectedValue.ToString().Length > 0)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", supplier.SelectedValue.ToString()); //棧板綁定專屬發送站
        if (shippingAddress.Checked == true)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", "0"); //棧板發送站由出貨地址判斷

        //customer_type (1:區配 2:竹運 3:自營)
        //if (customer_hcode.Text != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別(保留不帶值)   
        //}

        if (serialnum == "0000")
        {
            cmd.Parameters.AddWithValue("@customer_type", "1");
            customer_type = 1;
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer_type", "3");
            customer_type = 3;
        }

        cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
        cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
        cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
        cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
        cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
        cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
        cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
        cmd.Parameters.AddWithValue("@invoice_city", InvoiceCity.SelectedValue.ToString());       //發票地址-縣市
        cmd.Parameters.AddWithValue("@invoice_area", InvoiceArea.SelectedValue.ToString());   //發票地址-鄉鎮市區
        cmd.Parameters.AddWithValue("@invoice_road", InvoiceAddress.Text.ToString());             //發票地址-路街巷弄號
        cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
        cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
        cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
        //合約生效日
        if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
        }

        //合約到期日
        if (DateTime.TryParse(contract_edate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
        }

        //票期
        if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(ticket.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
            }
            
        }
        else
        {
            cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
        }
        cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
        //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日
        
        //結帳日
        if (int.TryParse(close_date.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(tbclose.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
        }

        //cmd.Parameters.AddWithValue("@tariffs_table", feename.Text.ToString());            //運價表
        cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
        cmd.Parameters.AddWithValue("@customer_hcode", "");                                  //竹運客代
        cmd.Parameters.AddWithValue("@contact_1", "");                                       //竹運-聯絡人1  
        cmd.Parameters.AddWithValue("@email_1", "");                                         //竹運-聯絡人1 email
        cmd.Parameters.AddWithValue("@contact_2", "");                                       //竹運-聯絡人2
        cmd.Parameters.AddWithValue("@email_2", "");                                         //竹運-聯絡人2 email
        cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
        cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
        cmd.Parameters.AddWithValue("@type", Less_than_truckload );                          //棧板/零擔
        //cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
        //cmd.Parameters.AddWithValue("@update_date", DateTime.Now);
        //cmd.CommandText = dbAdapter.SQLdosomething("tbCustomers", cmd, "insert");
        //dbAdapter.execNonQuery(cmd);
        cmd.CommandText = dbAdapter.genInsertComm("tbCustomers", true, cmd);        //新增
        int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out customer_id);
        #endregion



        //#region 使用者操作log
        //SqlCommand cmd5 = new SqlCommand();
        //cmd5.Parameters.AddWithValue("@p_id", pid);
        //cmd5.Parameters.AddWithValue("@sdate", DateTime.Now);
        //cmd5.Parameters.AddWithValue("@user_id", Session["user_id"]);
        //cmd5.Parameters.AddWithValue("@action", "儲存");
        //cmd5.Parameters.AddWithValue("@ip", Utility.GetIPAddress());
        //cmd5.CommandText = dbAdapter.SQLdosomething("vi_project_log", cmd5, "insert");
        //dbAdapter.execNonQuery(cmd5);

        //#endregion

        //MultiView1.ActiveViewIndex = 1;
        SetDefaultView2(customer_code, customer_type, customer_id, PM_code.SelectedValue.ToString());

        /*string sScript = " $('#li_01').removeClass('current');" +
                         "    $('#li_02').addClass('current');";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);*/

        setFee.Visible = true;
        FinalCheck.Visible = true;
        btnsave.Visible = false;
        btncancel.Visible = false;
        Suppliers.Enabled = false;
        PM_code.Enabled = false;
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('客代編號" + customer_code + "新增完成！');</script>", false);
        return;
    }

    /// <summary>
    /// 運價初始資料設定
    /// </summary>
    protected void SetDefaultView2(string customer_code, int customer_type, int customer_id, string pricing_code)
    {
        //運價生效日
        DateTime dt_temp = new DateTime();
        tbmaster_code.Text = customer_code.Substring(0, 7);
        tbsecond_code.Text = customer_code.Substring(7, 3);
        lbcustomer_type.Text = customer_type.ToString();
        lb_cusid.Text = customer_id.ToString();
        lb_pricing_code.Text = pricing_code;
        lbl_PriceTip.Text = "";
        fee_sdate.Text = "※目前採用公版運價";
        if (customer_code.Length > 0)
        {
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.CommandText = "usp_GetTodayPriceBusDefLog";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@customer_code", customer_code);
                cmd2.Parameters.AddWithValue("@return_type", "1");
                using (DataTable the_dt = dbAdapter.getDataTable(cmd2))
                {
                    if (the_dt.Rows.Count > 0)
                    {
                        if (DateTime.TryParse(the_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                        {
                            fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                        }

                        int i_temp;
                        if (!int.TryParse(the_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                        lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                    }
                }
            }
        }


        //自營才開放價格設定
        if (customer_type == 3)
        {
            pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(customer_code, customer_id.ToString())));
            pan_price_list.Visible = true;
        }
        else
        {
            pan_price_list.Visible = false;
        }
    }

    protected void btnQry_view2_Click(object sender, EventArgs e)
    {
        SetDefaultView2(tbmaster_code.Text + tbsecond_code.Text, Convert.ToInt32 (lbcustomer_type.Text), Convert.ToInt32 ( lb_cusid.Text), lb_pricing_code.Text);
    }

    protected void Button1_Click(object sender, EventArgs e) 
    {
        DateTime dt_temp = new DateTime();
        int i_tmp;
        string sScript = "";

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
            cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());                //客戶簡稱
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間
            cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
            cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
            cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
            cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
            cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
            cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
            cmd.Parameters.AddWithValue("@invoice_city", InvoiceCity.SelectedValue.ToString());       //發票地址-縣市
            cmd.Parameters.AddWithValue("@invoice_area", InvoiceArea.SelectedValue.ToString());   //發票地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@invoice_road", InvoiceAddress.Text.ToString());             //發票地址-路街巷弄號
            cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
            cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
            cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
                                                                                                 //合約生效日
            if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
            {
                cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
            }

            //合約到期日
            if (DateTime.TryParse(contract_edate.Text, out dt_temp))
            {
                cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
            }

            //票期
            if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
            {
                if (i_tmp == -1)
                {
                    if (int.TryParse(ticket.Text, out i_tmp))
                    {
                        cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }

            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
                                                                                                 //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日

            //結帳日
            if (int.TryParse(close_date.SelectedValue, out i_tmp))
            {
                if (i_tmp == -1)
                {
                    if (int.TryParse(tbclose.Text, out i_tmp))
                    {
                        cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
            }

            cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
            cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
            cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
            cmd.Parameters.AddWithValue("@individual_fee", individual_fee.Checked);                //是否個別計價  
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", lb_cusid.Text);
            cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "新增主客戶" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                sScript ="alert('儲存失敗：" + ex.Message.ToString() +"')";
            }
        }   
        //MultiView1.ActiveViewIndex = 2;
        
        using (SqlCommand cmd2 = new SqlCommand())
        {
            cmd2.Parameters.AddWithValue("@customer_code", tbmaster_code.Text  + tbsecond_code.Text);
            cmd2.CommandText = " select top 1 tbCustomers.* , B.code_name as pricingname, C.code_name as stopname  from tbCustomers "+
                              " left join  tbItemCodes B on B.code_id = tbCustomers.pricing_code and B.code_sclass = 'PM'" +
                              " left join  tbItemCodes C on C.code_id = tbCustomers.stop_shipping_code and C.code_sclass = 'TS'" +
                              " where customer_code = @customer_code ";
            using (DataTable dt = dbAdapter.getDataTable(cmd2))
            {
                lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                if (dt.Rows[0]["supplier_code"].ToString().Trim() != "")
                {
                    SqlCommand cmds = new SqlCommand();
                    DataTable dts;
                    cmds.Parameters.AddWithValue("@supplier_code", dt.Rows[0]["supplier_code"].ToString().Trim());
                    cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
                    dts = dbAdapter.getDataTable(cmds);
                    if (dts.Rows.Count > 0)
                    {
                        lsupplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
                    }
                }
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();
                lbshortname.Text = dt.Rows[0]["customer_shortname"].ToString();
                lbuni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();
                lbprincipal.Text = dt.Rows[0]["shipments_principal"].ToString();
                lbtel.Text = dt.Rows[0]["telephone"].ToString();
                lbfax.Text = dt.Rows[0]["fax"].ToString();
                lbCity.Text = dt.Rows[0]["shipments_city"].ToString();                
                lbCityArea.Text = dt.Rows[0]["shipments_area"].ToString();
                lbaddress.Text = dt.Rows[0]["shipments_road"].ToString();
                lbbusiness_people.Text = dt.Rows[0]["business_people"].ToString();
                lbticket_period.Text = dt.Rows[0]["ticket_period"].ToString();
                inputInvoiceCity.Text = dt.Rows[0]["invoice_city"].ToString();
                inputInvoiceArea.Text = dt.Rows[0]["invoice_area"].ToString();
                inputInvoiceAddress.Text = dt.Rows[0]["invoice_road"].ToString();
                lbemial.Text = dt.Rows[0]["shipments_email"].ToString().Trim();
                lbSPM_code.Text = dt.Rows[0]["pricingname"].ToString().Trim();
                lbstop_code.Text = dt.Rows[0]["stopname"].ToString().Trim();
                lbcontract_sdate.Text = dt.Rows[0]["contract_effect_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd") :"";
                lbcontract_edate.Text = dt.Rows[0]["contract_expired_date"] != DBNull.Value ?  ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd") :"";
                sScript += "$('#hlFileView2').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename.Text + "');";
                lbclose_date.Text  = dt.Rows[0]["checkout_date"].ToString();
                lbbilling_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString();
                lbFee.Text = Convert.ToBoolean(dt.Rows[0]["individual_fee"]) == true ? "獨立計價" : "採運價表計價";

            }
        }
          

        // sScript += " $('#li_01').removeClass('current');$('#li_02').removeClass('current');" +
        //                 "    $('#li_03').addClass('current');";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_1.aspx';alert('運價新增完成');</script>", false);
        return;
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        Clear();
    }
}