﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_1.aspx.cs" Inherits="money_1"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        
        $(document).ready(function () {
            
            close_typeChange();

            $("#<%= Suppliers.ClientID%>").change(function () {
                SuppliersChange();
            });

            
        });
       
        function SuppliersChange()
        {
            var type = $("#<%= dlclose_type.ClientID%> option:selected").val()
            if (type == '1') {
                ChangeCloseDay();
            }
        }

        function close_typeChange()
        {
            $("#<%= dlclose_type.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '1':
                        $("._close").show();
                        $("._daterange").html('請款期間');
                        ChangeCloseDay();
                        break;
                    case '2':
                        $("._close").hide();
                        $("._daterange").html('發送期間');
                        break;
                }
            });
        }

        function SetClosedayDDLEmpty() {
            $("#<%= dlCloseDay.ClientID%>").empty();
            $("#<%= dlCloseDay.ClientID%>").append($('<option></option>').val('').text('請選擇'));
        }
        function ChangeCloseDay() {
            var sel_supplier_code = $.trim($("#<%= Suppliers.ClientID%> option:selected").val());
            var sel_dates = $.trim($("#<%= dates.ClientID%>").val());
            var sel_datee = $.trim($("#<%= datee.ClientID%>").val());
            if (sel_supplier_code.length == 0 ) {
                SetClosedayDDLEmpty();
            }
            else {
                $.ajax(
                {
                    url: "money_1_3.aspx/GetCloseDayDDLHtml",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ dates: sel_dates, datee : sel_datee, supplier_code: sel_supplier_code, second_code: null }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d.length > 0) {
                        $("#<%= dlCloseDay.ClientID%>").html(data.d);
                    }
                    else {
                        SetClosedayDDLEmpty();
                    }
                });
            }
        }
    
    </script>
    <style type="text/css">
        .auto-style1 {
            height: 43px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">竹運應收</h2>
                       
            <div class="templatemo-login-form" >
            	<div class="form-group form-inline">                  
                    <label for="inputLastName">請款期間</label>
                    <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate"></asp:TextBox> ~ 
                    <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                    <label for="inputFirstName">客戶代號</label>
                    <%--<asp:TextBox ID="Suppliers" runat="server" class="form-control" OnTextChanged="Suppliers_TextChanged"></asp:TextBox>              --%>     
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" ></asp:DropDownList>
                    <asp:DropDownList ID="dlclose_type" CssClass="form-control" runat="server" OnSelectedIndexChanged="dlclose_type_SelectedIndexChanged">
                        <asp:ListItem Value="1">1. 已出帳</asp:ListItem>
                        <asp:ListItem Value="2">2. 未出帳</asp:ListItem>
                    </asp:DropDownList>
                    <%--<select class="form-control">
                      <option>1. 己出帳</option>
                    </select>--%>
                    <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="帳單查詢" OnClick="btQry_Click" /> 
                    <asp:Button ID="btClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');"  />
                    <asp:Button ID="btExport" runat="server" class="templatemo-white-button " Text="匯出" OnClick="btExport_Click"/> 
                </div>
                <div class="form-group form-inline">
                    <label class="_close" for="inputFirstName">結帳日</label>
                    <asp:DropDownList ID="dlCloseDay" runat="server" CssClass="form-control _close"></asp:DropDownList>
                	<%--<button type="submit" class="btn btn-primary _close">重寄帳單</button>--%>
                </div>
              <hr>
              <p class="text-primary  ">
                  <asp:Label ID="lbSuppliers" runat="server" ></asp:Label><br>請款期間：<asp:Label ID="lbdate" runat="server" ></asp:Label></p>
               <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide" >
                        <th class="auto-style1">序號</th>
                        <th class="auto-style1">特殊狀況說明</th>
                        <th class="auto-style1">發送日期</th>
                        <th class="auto-style1">明細貨號</th>
                        <th class="auto-style1">發送站客代</th>
                        <th class="auto-style1">發送站</th>
                        <th class="auto-style1">寄件人</th>
                        <th class="auto-style1">收件人</th>
                        <th class="auto-style1">配送縣市</th>
                        <th class="auto-style1">配送區域</th>
                        <th class="auto-style1">件數</th>
                        <th class="auto-style1">板數</th>
                        <th class="auto-style1">收貨人住址</th>
                        <th class="auto-style1">區配商代碼</th>
                        <th class="auto-style1">區配廠商</th>
                        <th class="auto-style1">特殊配送說明</th>
                        <th class="auto-style1">B段總計</th>
                        <th class="auto-style1">偏遠區費用</th>
                        <th class="auto-style1">JUNFU費用總計(不打折)</th>
                    </tr>
                    <asp:Repeater ID="New_List_01" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="序號">
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                </td>
                                <td data-th="特殊狀況說明"></td>
                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%></td> 
                                <td data-th="明細貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                <td data-th="發送站客代"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                                <td data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                                <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                <td data-th="配送縣市"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%></td>
                                <td data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%></td>
                                <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                                <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                <td data-th="收貨人住址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_address").ToString())%></td>
                                <td data-th="區配商代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%></td>
                                <td data-th="區配廠商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.區配廠商").ToString())%></td>
                                <td data-th="特殊配送說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                <td data-th="B段總計"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_fee","{0:N0}").ToString())%></td>
                                <td data-th="偏遠區費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.remote_fee","{0:N0}").ToString())%></td>
                                <td data-th="JUNFU費用總計(不打折)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>

                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List_01.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="19" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <table class="paytable">
                    <tr>
                        <th>小計：</th>
                        <td ><nobr>NT$
                            <asp:Label ID="subtotal_01" runat="server"></asp:Label>
                            元</nobr></td>
                    </tr>
                    <tr>
                        <th>5%營業稅：</th>
                        <td><nobr>NT$  
                            <asp:Label ID="tax_01" runat="server"></asp:Label>
                            元</nobr></td>
                    </tr>
                    <tr>
                        <th>應收帳款：</th>
                        <td><nobr>NT$   
                            <asp:Label ID="total_01" runat="server"></asp:Label>
                            元</nobr></td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered templatemo-user-table" style="width: 50%">
                    <tr class="tr-only-hide">
                        <th>計價模式</th>
                        <th>實收運費</th>
                        <th>稅額</th>
                        <th>應收帳款</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                <td data-th="實收運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subtotal","{0:N0}").ToString())%></td>
                                <td data-th="稅額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax","{0:N0}").ToString())%></td>
                                <td data-th="應收帳款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total","{0:N0}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="4" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
              <%--<table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                  <tr>
                    <td>特殊狀況說明</td>
                    <td>發送日期</td>
                    <td>明細貨號</td>
                    <td>發送站客代</td>
                    <td>發送站</td>
                    <td>寄件人</td>
                    <td>收件人</td>
                    <td>配送縣市</td>
                    <td>配送區域</td>
                    <td>件數</td>
                    <td>板數</td>
                    <td>收貨人住址</td>
                    <td>區配商代碼</td>
                    <td>區配廠商</td>
                    <td>特殊配送說明</td>
                    <td>B段總計</td>
                    <td>偏遠區費用</td>
                    <td>JUNFU費用總計(不打折)</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>&nbsp;</td>
                    <td>2016/12/13</td>
                    <td>123456789</td>
                    <td>0010001-001</td>
                    <td>台北</td>
                    <td>AA</td>
                    <td>aa</td>
                    <td>新竹市</td>
                    <td>東區</td>
                    <td>25</td>
                    <td>1</td>
                    <td>新竹市....</td>
                    <td>B03</td>
                    <td>瑞高</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>2016/12/13</td>
                    <td>123456789</td>
                    <td>0010001-001</td>
                    <td>台北</td>
                    <td>AA</td>
                    <td>aa</td>
                    <td>新竹市</td>
                    <td>東區</td>
                    <td>25</td>
                    <td>1</td>
                    <td>新竹市....</td>
                    <td>B03</td>
                    <td>瑞高</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>2016/12/13</td>
                    <td>123456789</td>
                    <td>0010001-001</td>
                    <td>台北</td>
                    <td>AA</td>
                    <td>aa</td>
                    <td>新竹市</td>
                    <td>東區</td>
                    <td>25</td>
                    <td>1</td>
                    <td>新竹市....</td>
                    <td>B03</td>
                    <td>瑞高</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>2016/12/13</td>
                    <td>123456789</td>
                    <td>0010001-001</td>
                    <td>台北</td>
                    <td>AA</td>
                    <td>aa</td>
                    <td>新竹市</td>
                    <td>東區</td>
                    <td>25</td>
                    <td>1</td>
                    <td>新竹市....</td>
                    <td>B03</td>
                    <td>瑞高</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td>2016/12/13</td>
                    <td>123456789</td>
                    <td>0010001-001</td>
                    <td>台北</td>
                    <td>AA</td>
                    <td>aa</td>
                    <td>新竹市</td>
                    <td>東區</td>
                    <td>25</td>
                    <td>1</td>
                    <td>新竹市....</td>
                    <td>B03</td>
                    <td>瑞高</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>                    
                </tbody>
              </table>
              <table class="paytable">
              <tr>
                <th>小計：</th>
                <td>NT$ 95元</td>
              </tr>
              <tr>
              	<th>5%營業稅：</th>
                <td>NT$ -48元</td>
              </tr>
              <tr>
              	<th>應收帳款：</th>
                <td>NT$ 70元</td>
              </tr>
          	</table>--%>
              
        </div>
        </div>
    </div>
</asp:Content>

