﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-客戶登入</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">
    <!-- 
        Visual Admin Template
        http://www.templatemo.com/preview/templatemo_455_visual_admin
        -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <!-- jQuery文件 -->
	<script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->

    <style type="text/css">
        .table_qrcode{
            min-width:360px;
            text-align:center;
        }

        .auto-style1 {
            display: block;
            width: 100%;
            height: 28px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            left: 0px;
            top: 0px;
            border: 1px solid #ccc;
            padding: 3px 8px;
            background-color: #fff;
            background-image: none;
        }

        .auto-style2 {
            position: relative;
            display: table;
            border-collapse: separate;
            left: 0px;
            top: 0px;
        }

    </style>

    <script type="text/javascript">
        var i = 0;
        function changevcode() {
            i++;
            $("#chcode").html("<img id='Login1_Image2' src='CheckImageCode.aspx?a=" + i + "' alt='點選更換驗證碼' title='點選更換驗證碼' />");
        }6
    </script>
    <script type="text/javascript">
     $(document).ready(function () {
         $('.fancybox').fancybox();
         $("#forgetclick").fancybox({
             'width': 980,
             'height': 600,
             'autoScale': false,
             'transitionIn': 'none',
             'transitionOut': 'none',
             'type': 'iframe',
             'onClosed': function () {
                 parent.location.reload(true);
             }
         });


         //標準的寫法:
         $.ajax({
             type: "post",
             dataType: "json",
             contentType: "application/json", //注意:WebMethod()必須加這項,否則客戶端資料不會傳到服務端
             async: false,
             url: "login.aspx/imgUrl",//模擬web服務,提交到方法
             // 可選的 async:false,阻塞的非同步就是同步
             beforeSend: function () {
                 // do something.
                 // 一般是禁用按鈕等防止使用者重複提交
                 $("#btnClick").attr({ disabled: "disabled" });
                 // 或者是顯示loading圖片
             },
             success: function (data) {
                // alert("success: " + data.d);//注意這裡:必須通過data.d才能獲取到伺服器返回的值
                 // 服務端可以直接返回Model,也可以返回序列化之後的字串,如果需要反序列化:string json = JSON.parse(data.d);
                 // 有時候需要巢狀呼叫ajax請求,也是可以的
                 console.log(data.d);
                 document.getElementById("loginImage").src = data.d;
   
             },
             complete: function () {
                 //do something.
                 $("#btnClick").removeAttr("disabled");
                 // 隱藏loading圖片
             },
             error: function (data) {
                 alert("error: " + data.d);
             }
         });

     });
     $.fancybox.update();

    </script>
</head>
<body class="login-bg">
    <form id="form1" runat="server">
        <div class="templatemo-content-widget templatemo-login-widget white-bg">
            <header class="text-center">
                <img src="/" id="loginImage" alt="峻富雲端物流管理登入">
            </header>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
                    <asp:TextBox ID="account" runat="server" CssClass="auto-style1" placeholder="登入帳號"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="account" ForeColor="Red" ValidationGroup="validate">請輸入帳號</asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <div class="auto-style2">
                    <div class="input-group-addon"><i class="fa fa-key fa-fw"></i></div>
                    <asp:TextBox ID="password" TextMode="Password" runat="server" CssClass="form-control" placeholder="密碼"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="password" ForeColor="Red" ValidationGroup="validate">請輸入密碼</asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-group">
                <asp:Button ID="btn_login" CssClass="btn btn-primary" runat="server" Text="登入" OnClick="btn_login_Click" /><span></span>
            </div>
            <%-- <header class="text-left " style="line-height: 24px;font-size:14px"  >
                 <p style="font-weight:bold">
               2022中秋節連假期間，服務調整公告</p>
                 <br />
                <p>中秋期間包裹收寄件服務調整說明：</p>
                 <p>
1.因應中秋宅配高峰期，此期間暫停指定日期及時段服務，且不保證隔日配達。</p>
<p>  ●影響期間：09/05(一)~9/11(日)</p> 
 <p>  ●影響範圍：全省</p> 
<p>2.中秋連續假期09/09~09/11收送件服務調整：</p> 
 <p>●09/09 (五)中秋連假暫停收件，僅送件</p>
 <p>●09/10(六)中秋連假暫停收送件</p>
 <p>●09/11(日)中秋連假暫停收送件</p>
 <p>上述事項如造成貴公司不便之處，敬請見諒。</p>
                 <br />
<p>FSE全速配(股)有限公司 敬上</p>--%>
                <%--<div class="checkbox">
                    <label>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:CheckBox ID="remchk" runat="server" OnCheckedChanged="remchk_CheckedChanged" AutoPostBack="true" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        記住帳號
                    </label>
                </div>--%>
            
        </div>

        <div class="templatemo-content-widget templatemo-login-widget templatemo-register-widget white-bg">
            <p><strong>忘記密碼? <a href="forget.aspx" class="fancybox fancybox.iframe" id="forgetclick"><span class="blue-text">忘記密碼</span></a> </strong></p>
        </div>
    </form>
</body>
</html>
