﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class MasterWeb : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            UserName.Text = Session["user_name"].ToString();
            string type = Request.QueryString["type"] != null ? Request.QueryString["type"].ToString() : "";
            Session["type"] = type;

            #region  上方權限
            DataTable dttop = new DataTable();
            dttop = Utility.getTopMenu(Session["account_code"].ToString(), type );
            if (dttop.Rows.Count > 0)
            {
                string top_func_code = "";
                for (int i = 0; i < dttop.Rows.Count; i++)
                {
                    top_func_code = dttop.Rows[i]["top_func_code"].ToString();
                    switch (top_func_code)
                    {
                        case "D":
                            D.Visible = true;
                            Dhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "T":
                            T.Visible = true;
                            Thref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "R":
                            R.Visible = true;
                            Rhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "C":
                            C.Visible = true;
                            Chref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "F":
                            F.Visible = true;
                            Fhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "S":
                            S.Visible = true;
                            Shref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "A":
                            A.Visible = true;
                            Ahref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "B":
                            B.Visible = true;
                            Bhref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                        case "E":
                            E.Visible = true;
                            Ehref.HRef = dttop.Rows[i]["func_link"].ToString();
                            break;
                    }
                }
            }
            #endregion

            #region  Log
            try
            {
                string StrAccount = Session["account_code"].ToString();
                if (StrAccount.Length > 0)
                {
                    new PublicFunction().ExeOpLog(StrAccount, System.IO.Path.GetFileName(Request.PhysicalPath), "");
                }
            }
            catch (Exception ex)
            {

            }

            #endregion


        }
    }
}
