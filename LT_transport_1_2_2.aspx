﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_1_2_2.aspx.cs" Inherits="LT_transport_1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#Receiveclick").fancybox({
                'width': 980,
                'height': 600,
                'autoScale': false,
                'transitionIn': 'none',
                'type': 'iframe',
                'transitionOut': 'none',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

            $(".subpoena_category").change();

            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date())  //預設當日
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".date_picker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    defaultDate: (new Date())  //預設當日
                });
            }


        });
        $.fancybox.update();



        $(document).on("change", ".subpoena_category", function () {
            var _isOK = true;
            var _cateVal = $(this).children("option:selected").val();
            var _setItem;
            var _disItem;
            switch (_cateVal) {
                case '11'://元付
                    //_collection_money
                    _disItem = $("._collection_money");
                    _setItem = $("._arrive_to_pay_freight,._arrive_to_pay_append");
                    break;
                case '21'://到付
                    _disItem = $("._collection_money,._arrive_to_pay_append");
                    _setItem = $("._arrive_to_pay_freight");
                    break;
                case '25'://元付-到付追加
                    _disItem = $("._collection_money,._arrive_to_pay_freight");
                    _setItem = $("._arrive_to_pay_append");
                    break;
                case '41'://代收貨款
                    _disItem = $("._arrive_to_pay_append,._arrive_to_pay_freight");
                    _setItem = $("._collection_money");
                    break;
                default:
                    _isOK = false;
                    break;
            }
            if (_isOK) {

                _disItem.map(function () { $(this).prop('readonly', true); });
                _setItem.map(function () { $(this).prop('readonly', false); });

            }

        });
    </script>


    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        ._ddl {
            min-width: 85px;
        }

        ._addr {
            min-width: 35%;
        }

        .div_addr {
            width: 75%;
        }

        .ralign {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">拒收退貨託運單(站所用)</h2>
            <hr />
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>拒收正物流單號：</label>
                    <asp:TextBox ID="strkey" MaxLength="21" runat="server" class="form-control"></asp:TextBox>
                    <asp:Button ID="btnselect" CssClass="templatemo-blue-button" runat="server" Text="查 詢" OnClick="btnselect_click" />
                    <asp:HiddenField ID="hid_request_id" runat="server" />
                </div>
               <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <contenttemplate>
                        <div class="form-group form-inline">
                            <label>取貨站所：</label>
                            <asp:Label ID="lbstation_scode" runat="server"></asp:Label>
                        </div>
                        <div class="form-group form-inline">
                            <label>收貨人：</label>
                            <asp:Label ID="lbreceive_contact" runat="server">  </asp:Label>
                                <label>客代編號：</label>
                            <asp:Label ID="lbcustomer_code" runat="server">  </asp:Label>
                        </div>
                        <div class="form-group form-inline">
                            <label>收貨人電話：</label>
                            <asp:Label ID="lbreceive_tel1" runat="server">  </asp:Label>
                        </div>
                        <div class="form-group form-inline">
                            <label>收貨人地址：</label>
                            <asp:Label ID="lbreceive_address" runat="server">  </asp:Label>
                            <asp:HiddenField ID="hid_receive_city" runat="server" /> 
                            <asp:HiddenField ID="hid_receive_area" runat="server" />
                            <asp:HiddenField ID="hid_receive_address" runat="server" />
                        </div>                         
                    </contenttemplate>
                </asp:UpdatePanel>

                <div class="bs-callout bs-callout-info">
                    <h3>派遣內容</h3>
                    <div class="rowform">
                        <div class="row form-inline">
                            <div class="form-group  form-inline">
                                <asp:Label ID="send_station_scode" runat="server" hidden="true"></asp:Label>
                            </div>
                            <div class="form-group  form-inline">
                                <label>訂單編號(20碼)：</label>
                                <asp:TextBox ID="strorder_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                            </div>
                            <br>
                            <div class="form-group  form-inline">
                                <label>退(寄)貨人：</label>
                                <asp:Label ID="strsend_contact" runat="server"></asp:Label>
                            </div>
                            <br>
                            <div class="form-group  form-inline">
                                <label>退(寄)貨人電話：</label>
                                <asp:Label ID="strsend_tel" runat="server"></asp:Label>
                            </div>
                            <br>
                            <div class="form-group  form-inline">
                                <label>退(寄)貨人地址：</label>
                                <asp:Label id="ddlzip" runat="server"></asp:Label>
                                <asp:Label ID="ddlsend_city" runat="server"></asp:Label>
                                <asp:Label ID="ddlsend_area" runat="server"></asp:Label>
                                <asp:Label ID="strsend_address" runat="server"></asp:Label>
                            </div>
                            <br>
                            <div class="form-group  form-inline">
                                <label>收回品項：</label>
                                <textarea id="invoice_memo" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                            </div>
                            <br>
                            <div class="form-group  form-inline">
                                <label>備註(說明)：</label>
                                <textarea id="invoice_desc" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnsave_Click" />
                    <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="清 除" OnClick="btnRomv_Click" />
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
