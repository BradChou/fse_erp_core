﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_9_edit.aspx.cs" Inherits="assets1_9_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $('.fancybox').fancybox();

            $("#carsel").fancybox({
                'width': 980,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });
            init();
        });

        function init() {
            $("#tbrecord [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#tbrecord [id*=cbpay]").prop("checked", true);
                } else {
                    $("#tbrecord [id*=cbpay]").prop("checked", false);
                }

            });
            $("#tbrecord [id*=cbpay]").click(function () {
                if ($("#tbrecord [id*=cbpay]").length == $("#tbrecord [id*=cbpay]:checked").length) {
                    $("#tbrecord [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#tbrecord [id*=chkHeader]").prop("checked", false);
                }

            });


        }


        $(document).on("click", "#<%= btnavg.ClientID%>", function () {
            var _month = $("#<%= apportion.ClientID%>").val();
            var _price = $("#<%= price.ClientID%>").val();
            if (!$.isNumeric(_month))
            {
                alert('請先輸入攤提月份');
                return false;
            }
            if (!$.isNumeric(_price)) {
                alert('請先輸入保單金額');
                return false;
            }
            
            $("#<%= apportion_price.ClientID%>").val(Math.floor(_price / _month));
            
        });

    </script>

    <style>
        ._th {
            text-align: center;
        }
        .bottnmargin {
            margin-bottom: 7px;
        }
        .btn {
            font-size :12px;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }
        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <h2 class="margin-bottom-10">車輛保險維護-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
            <div class="templatemo-login-form">
                <!--基本設定-->
                <div class="bs-callout bs-callout-info">
                    <h3>1. 基本設定</h3>
                    <div class="rowform">
                        <div class="row ">
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="car_license"><span class="REDWD_b">*</span>車牌</label>
                                <asp:TextBox ID="car_license" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                <a id="carsel"  href="assets1_9_carsel.aspx?car_license=<%= car_license.ClientID %>&Hid_assets_id=<%=Hid_assets_id.ClientID%>" class="fancybox fancybox.iframe " ><span class="btn btn-link small  ">選取</span></a>
                                <asp:HiddenField ID="Hid_assets_id" runat="server" />
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入車輛代號</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="form_id">保單編號</label>
                                <asp:TextBox ID="form_id" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="form_id" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入保單編號</asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="Insurance_name">保單名稱</label>
                                <asp:TextBox ID="Insurance_name" runat="server" class="form-control" Width="70%"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="Insurance_name" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入保單名稱</asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="kind"><span class="REDWD_b">*</span>險種</label>
                                <asp:DropDownList ID="dlkind" runat="server" class="form-control"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="dlkind" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請指定險種</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label><span class="REDWD_b">*</span>費用期間</label>
                                <asp:TextBox ID="Sdate" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>～
                                <asp:TextBox ID="Edate" runat="server" class="form-control date_picker"  autocomplete="off"></asp:TextBox>
                            </div>
                            
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="price"><span class="REDWD_b">*</span>金額</label>
                                <asp:TextBox ID="price" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="price" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入金額</asp:RequiredFieldValidator>
                            </div>

                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="apportion"><span class="REDWD_b">*</span>攤提月份</label>
                                <asp:TextBox ID="apportion" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="apportion" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入攤提月份</asp:RequiredFieldValidator>
                            </div>

                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="apportion_price"><span class="REDWD_b">*</span>月攤提數</label>
                                <asp:TextBox ID="apportion_price" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                <a id="btnavg" runat="server"><span class="btn btn-success  ">均攤</span></a>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="apportion" ForeColor="Red" Font-Size="Small"  ValidationGroup="validate">請輸入月攤提數</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="amount_paid">累計攤提金額</label>
                                <asp:TextBox ID="amount_paid" runat="server" class="form-control" ReadOnly ="true"  ></asp:TextBox>
                            </div>
                            <div class="col-lg-4 bottnmargin form-inline">
                                <label for="memo">備註</label>
                                <asp:TextBox ID="memo" runat="server" class="form-control" ></asp:TextBox>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!--繳費記錄-->
                <div class="bs-callout bs-callout-info">
                    <h3>2. 繳費記錄</h3>
                    <div class="rowform">
                        <div class="row ">
                            <div class="col-lg-6" >
                                <table id="tbrecord" class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th"></th>
                                    <th class="_th">期間</th>
                                    <th class="_th">金額</th>
                                    <th class="_th"><asp:CheckBox ID="chkHeader" runat="server" />繳費</th>
                                    <th class="_th">備註</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <ItemTemplate>
                                        <tr class="paginate">
                                            <td>
                                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.row_id").ToString())%>
                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                            </td>
                                            <td data-th="期間(年/月)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.year").ToString())%>/<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.month").ToString())%></td>
                                            <td data-th="金額"><asp:Literal ID="Liprice" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price").ToString())%>' ></asp:Literal></td>
                                            <td data-th="繳費">
                                                <asp:CheckBox ID="cbpay" CssClass=" font-weight-400" runat="server" Checked='<%#  Convert.ToBoolean(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ispay").ToString())) %>' />
                                            </td>
                                            <td data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="5" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
               
                <div class="form-group text-center">
                    <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click"  />
                    <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl ="~/assets1_9.aspx">取消</asp:LinkButton>
                    <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
                </div>

            </div>
        
</asp:Content>

