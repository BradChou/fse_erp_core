﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class store1_3edi : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

            #region 基本資料
            using (SqlCommand cmd = new SqlCommand())
            {
                
                cmd.Parameters.AddWithValue("@account_code", a_id);
                cmd.CommandText = " select * from tbAccounts where account_code = @account_code  ";
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lbitem.Text = dt.Rows[0]["manager_unit"].ToString();         //管理單位
                        lbitem_employee.Text = dt.Rows[0]["emp_code"].ToString();    //人　　員
                        lbaccount_code.Text = dt.Rows[0]["account_code"].ToString(); //帳　　號
                        lbaccount_name.Text = dt.Rows[0]["user_name"].ToString();    //名　　稱
                    }
                    
                }
            }

            #endregion

            #region 場區
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", a_id);
            if (!string.IsNullOrEmpty(Str))
            {
                var StrAry = Str.Split(',');
                foreach (var item in StrAry)
                {
                    if(!string.IsNullOrEmpty(item))
                    {
                        objCommand.Parameters.AddWithValue("@code_id" + item, item);
                        wherestr += "  and seq<>@code_id" + item;
                    }
                }
                SqlCommand objCommand2 = new SqlCommand();
                string wherestr2 = " and (";
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i>0)
                        {
                            wherestr2 += " or ";
                        }
                        objCommand2.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr2 += "  seq=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr2 += " )";
                objCommand2.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr2);
                using (DataTable dt2 = dbAdapter.getDataTable(objCommand2))
                {
                    DeptListGet.DataSource = dt2;
                    DeptListGet.DataValueField = "seq";
                    DeptListGet.DataTextField = "code_name";
                    DeptListGet.DataBind();
                }
            }
            objCommand.CommandText = string.Format(@"select seq, code_name from ttItemCodesFieldArea with(nolock) where active_flag = 1 {0} order by seq", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                DeptList.DataSource = dt;
                DeptList.DataValueField = "seq";
                DeptList.DataTextField = "code_name";
                DeptList.DataBind();
            }
            #endregion

        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        var DeptListGetVal = DeptListGet_Val.Value.ToString().Trim();
        string JStr = "";
        if (string.IsNullOrEmpty(DeptListGetVal))
        {
            JStr = "alert('無選擇項目!');";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
            return;
        }
        else
        {
            try
            {
                var Account_Code = Func.GetRow("Account_Code", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=2", "Account_Code", lbaccount_code.Text.Trim());
                if (string.IsNullOrEmpty(Account_Code))
                {
                    using (SqlCommand cmdInsert = new SqlCommand())
                    {
                        cmdInsert.Parameters.AddWithValue("@Account_Code", lbaccount_code.Text.Trim());
                        cmdInsert.Parameters.AddWithValue("@Kinds", 2);
                        cmdInsert.Parameters.AddWithValue("@Str", DeptListGetVal);
                        cmdInsert.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                        cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                        cmdInsert.CommandText = dbAdapter.SQLdosomething("ttPersonCorrespond", cmdInsert, "insert");
                        dbAdapter.execNonQuery(cmdInsert);
                        JStr = "alert('新增成功！');";
                    }
                }
                else
                {
                    using (SqlCommand cmdupd = new SqlCommand())
                    {
                        cmdupd.Parameters.AddWithValue("@Str", DeptListGetVal);
                        cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                        cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "Kinds", 2);
                        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "Account_Code", Account_Code);
                        cmdupd.CommandText = dbAdapter.genUpdateComm("ttPersonCorrespond", cmdupd);
                        dbAdapter.execNonQuery(cmdupd);
                        JStr = "alert('修改成功！');";
                    }
                 }
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "location.href='store1_3.aspx';</script>", false);
            }
            catch (Exception ex)
            {
                JStr = "alert('執行異常!" + ex.ToString() + "');";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>" + JStr + "</script>", false);
                return;
            }
        }

        
    }




}
