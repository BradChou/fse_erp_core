﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using BarcodeLib;
using System.Drawing;

public partial class pallet_add : System.Web.UI.Page
{
    public string request_id
    {
        get { return ViewState["request_id"].ToString(); }
        set { ViewState["request_id"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region 客代編號(套權限)
            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = Session["master_code"].ToString();
            master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            using (SqlCommand cmd2 = new SqlCommand())
            {
                string wherestr = "";
                if (supplier_code != "")
                {
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                }
                if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                {
                    cmd2.Parameters.AddWithValue("@master_code", master_code);
                    wherestr += " and customer_code like @master_code+'%' ";
                }


                cmd2.CommandText = string.Format(@"Select customer_code , customer_code + '-' + customer_shortname as name  from tbCustomers with(Nolock) where 0=0 and stop_shipping_code = '0'  {0} order by customer_code asc ", wherestr);
                dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                dlcustomer_code.DataValueField = "customer_code";
                dlcustomer_code.DataTextField = "name";
                dlcustomer_code.DataBind();
                dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }

            Shipments_date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            #endregion

            #region 配送時段
            SqlCommand cmd6 = new SqlCommand();
            cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
            time_period.DataSource = dbAdapter.getDataTable(cmd6);
            time_period.DataValueField = "code_id";
            time_period.DataTextField = "code_name";
            time_period.DataBind();
            time_period.SelectedValue = "不指定";
            #endregion

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity with(nolock) order by seq asc ";
            receive_city.DataSource = dbAdapter.getDataTable(cmd1);
            receive_city.DataValueField = "city";
            receive_city.DataTextField = "city";
            receive_city.DataBind();
            receive_city.Items.Insert(0, new ListItem("請選擇", ""));

            send_city.DataSource = dbAdapter.getDataTable(cmd1);
            send_city.DataValueField = "city";
            send_city.DataTextField = "city";
            send_city.DataBind();
            send_city.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            readdata();
        }
    }


    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.Clear();
            cmd.CommandText = string.Format(@"Select A.* , B.customer_code , B.send_contact  , B.send_city , B.send_area, B.send_address ,  B.receive_contact , B.receive_city , B.receive_area , B.receive_address , B.area_arrive_code 
                                                , B.invoice_desc 
                                                From ttPlletReturn  A with(nolock)
                                                Left join tcDeliveryRequests  B with(nolock) on A.request_id  = B.request_id 
                                                Where ISNULL(A.return_request_id,'')   = ''
                                                Order by B.area_arrive_code , B.supplier_code ");

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                New_List.DataSource = dt;
                New_List.DataBind();
                if (upd_return.UpdateMode == UpdatePanelUpdateMode.Conditional)
                {
                    upd_return.Update();
                }
            }
        }
    }

    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBox1_CheckedChanged(null, null);

        #region 到著站簡碼
        SqlCommand cmd8 = new SqlCommand();
        cmd8.Parameters.AddWithValue("@supplier_code_sel", supplier_code.ToString());
        cmd8.Parameters.AddWithValue("@hctcode", "001");
        cmd8.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1 and supplier_code =@supplier_code_sel
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
                                                                                        order by supplier_code
                                            end");
        area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
        area_arrive_code.DataValueField = "supplier_code";
        area_arrive_code.DataTextField = "showname";
        area_arrive_code.DataBind();
        area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
        #endregion

    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        //顯示寄件人資料
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_shortname , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name,
                                  C.pricing_code
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = @customer_code ";
        cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
            city_SelectedIndexChanged(send_city, null);
            send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();  //出貨地址-鄉鎮市區
            send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
            Hid_pricing_type.Value = dt.Rows[0]["pricing_code"].ToString().Trim();
            //lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
            supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();      //區配code
            supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();      //區配name

        }

        if (UpdatePanel2.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel2.Update();
        }
    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        dlarea.Items.Clear();
        dlarea.Items.Add(new ListItem("請選擇", ""));

        if (dlcity.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }

    }


    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {

        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                                   left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                   where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        area_arrive_code.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
                    }
                    catch { }

                }
            }
        }
        cbarrive_CheckedChanged(null, null);
    }

    protected void cbarrive_CheckedChanged(object sender, EventArgs e)
    {
        if (cbarrive.Checked && area_arrive_code.SelectedValue != "")
        {
            receive_address.Text = area_arrive_code.SelectedItem.Text + "站址";
        }

    }

    protected void area_arrive_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbarrive_CheckedChanged(null, null);
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        string ErrStr = "";
        int i_tmp;
        if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
        if (i_tmp <= 0)
        {
            ErrStr = "請勾選要出單的項目";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
            return;
        }


        //lbErrQuant.Text = "";


        //bool individual = individual_fee.Style.Value.IndexOf("display:block") > -1;
        //int i_tmp;
        //switch (rbcheck_type.SelectedValue)
        //{
        //    case "01":
        //    case "04":
        //        if (string.IsNullOrWhiteSpace(plates.Text) || string.IsNullOrWhiteSpace(pieces.Text))
        //        {
        //            lbErrQuant.Text = "請輸入板數及件數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入板數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        break;
        //    case "02":
        //        if (string.IsNullOrWhiteSpace(pieces.Text))
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        break;
        //    case "03":
        //        if (string.IsNullOrWhiteSpace(cbm.Text)
        //            || string.IsNullOrWhiteSpace(plates.Text)
        //            || string.IsNullOrWhiteSpace(pieces.Text)
        //            )
        //        {
        //            lbErrQuant.Text = "請輸入板數、件數及才數";
        //            cbm.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入板數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入才數";
        //            cbm.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }
        //        break;
        //}

        //if (individual && i_supplier_fee.Text == "")
        //{
        //    lbErrQuant.Text = "請輸入貨件運費";
        //    i_supplier_fee.Focus();
        //    lbErrQuant.Visible = true;
        //    return;
        //}


        //if (lbErrQuant.Text == "" && lbErr.Text == "")
        //{
        try
        {
            int ttpieces = 0;
            int ttplates = 0;
            int ttcbm = 0;
            int ttcollection_money = 0;
            int ttarrive_to_pay = 0;
            int ttarrive_to_append = 0;
            int remote_fee = 0;
            int request_id = 0;
            //int supplier_fee = 0;  //配送費用
            //int cscetion_fee = 0;  //C配運價
            //int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

            #region 新增                
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@pricing_type", Hid_pricing_type.Value);                        //計價模式 (01:論板、02:論件、03論才、04論小板)
                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
                cmd.Parameters.AddWithValue("@check_number", "");                                             //託運單號(貨號) 列印標簽產生
                // cmd.Parameters.AddWithValue("@check_number", check_number.Text);                            //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
                cmd.Parameters.AddWithValue("@order_number", "");                                             //訂單號碼
                cmd.Parameters.AddWithValue("@check_type", "001");                                            //託運類別
                cmd.Parameters.AddWithValue("@receive_customer_code", "");                                    //收貨人編號
                cmd.Parameters.AddWithValue("@subpoena_category", "11");                                      //傳票類別
                cmd.Parameters.AddWithValue("@receive_tel1", receive_tel1.Text);                              //電話1
                cmd.Parameters.AddWithValue("@receive_tel1_ext", receive_tel1_ext.Text.ToString());           //電話分機
                cmd.Parameters.AddWithValue("@receive_tel2", "");                                             //電話2
                cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
                cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
                cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
                cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號                    
                cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
                cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code.SelectedValue);             //到著碼
                cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", Convert.ToInt16(cbarrive.Checked));  //到站領貨 0:否、1:是
                if (cbarrive.Checked)
                {
                    cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
                }
                if (!int.TryParse(pieces.Text, out ttpieces)) ttpieces = 0;
                if (!int.TryParse(plates.Text, out ttplates)) ttplates = 0;
                if (!int.TryParse(cbm.Text, out ttcbm)) ttcbm = 0;

                cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
                cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
                cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數
                cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
                cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加
                cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
                cmd.Parameters.AddWithValue("@send_contact", dlcustomer_code.SelectedItem.Text.Split('-')[1]); //寄件人
                cmd.Parameters.AddWithValue("@send_tel", "");                                                 //寄件人電話
                cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
                cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
                cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
                cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
                cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
                cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //說明
                cmd.Parameters.AddWithValue("@special_send", "0");                                            //特殊配送
                DateTime date;
                cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(arrive_assign_date.Text, out date) ? (object)date : DBNull.Value);     //指定日
                cmd.Parameters.AddWithValue("@time_period", time_period.SelectedValue.ToString());            //時段
                cmd.Parameters.AddWithValue("@receipt_flag", 0);                                             //是否回單
                cmd.Parameters.AddWithValue("@pallet_recycling_flag", 0);                                    //是否棧板回收
                cmd.Parameters.AddWithValue("@Pallet_type", "");
                cmd.Parameters.AddWithValue("@turn_board", 0);                                                 //翻版
                cmd.Parameters.AddWithValue("@upstairs", 0);                                                  //上樓
                cmd.Parameters.AddWithValue("@difficult_delivery", 0);                                        //困配
                cmd.Parameters.AddWithValue("@turn_board_fee", 0);
                cmd.Parameters.AddWithValue("@upstairs_fee", 0);
                cmd.Parameters.AddWithValue("@difficult_fee", 0);
                cmd.Parameters.AddWithValue("@add_transfer", 0);                                              //是否轉址
                cmd.Parameters.AddWithValue("@sub_check_number", "000");                                      //次貨號
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code_sel);                             //配送商代碼
                cmd.Parameters.AddWithValue("@supplier_name", supplier_name_sel);                             //配送商名稱
                cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間   
                cmd.Parameters.AddWithValue("@print_date", DateTime.TryParse(Shipments_date.Text, out date) ? (object)date : DBNull.Value);   //發送日期
                cmd.Parameters.AddWithValue("@print_flag", 0);
                cmd.Parameters.AddWithValue("@is_pallet", true);

                cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                {
                    string wherestr = "";
                    if (New_List.Items.Count > 0)
                    {
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@return_request_id", request_id);
                            for (int i = 0; i <= New_List.Items.Count - 1; i++)
                            {
                                CheckBox cbSel = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                                if (cbSel.Checked)
                                {
                                    string id = ((HiddenField)New_List.Items[i].FindControl("Hid_rid")).Value;
                                    cmd2.Parameters.AddWithValue("@id" + i.ToString(), id);
                                    if (wherestr == "")
                                    {
                                        wherestr += "@id" + i.ToString();
                                    }
                                    else
                                    {
                                        wherestr += ",@id" + i.ToString();
                                    }
                                }
                            }
                            cmd2.CommandText = string.Format("update ttPlletReturn set return_request_id=@return_request_id where id in({0})", wherestr);
                            dbAdapter.execNonQuery(cmd2);
                        }
                    }

                    #region
                    string ttErrStr = string.Empty;
                    //貨號區間
                    int start_number = 0;
                    int end_number = 999999999;
                    int current_number = 0;
                    int check_code = 0;        //檢查碼
                    Int64 check_number = 0;    //貨號(current_number + check_code)


                    #region 取貨號(網路託運單)
                    SqlCommand cmdt = new SqlCommand();
                    DataTable dtt;
                    cmdt.CommandText = " select * from tbCheckNumber where type = 2 ";
                    dtt = dbAdapter.getDataTable(cmdt);
                    if (dtt.Rows.Count > 0)
                    {
                        start_number = Convert.ToInt32(dtt.Rows[0]["start_number"]);
                        end_number = Convert.ToInt32(dtt.Rows[0]["end_number"]);
                        current_number = Convert.ToInt32(dtt.Rows[0]["current_number"].ToString());
                        if (current_number == 0 || current_number == end_number)
                        {
                            current_number = start_number;
                        }
                        else
                        {
                            current_number += 1;
                        }

                    }
                    else
                    {
                        ttErrStr = "查無網路託運單貨號區間，請冾系統管理員。";
                    }
                    #endregion


                    if (ttErrStr == "")
                    {

                        string _print_date = Shipments_date.Text.Trim();
                        string _Supplier_date = "";
                        string _arrive_assign_date = arrive_assign_date.Text.Trim();

                        #region 託運單號
                        using (SqlCommand cmd_update = new SqlCommand())
                        {
                            check_code = (int)current_number % 7;
                            check_number = Convert.ToInt64(current_number.ToString() + check_code.ToString());
                            cmd_update.Parameters.AddWithValue("@check_number", check_number.ToString());     //貨號

                            SqlCommand cmd3 = new SqlCommand();
                            cmd3.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                            cmd3.Parameters.AddWithValue("@check_code", check_code);                   //檢查碼
                            cmd3.Parameters.AddWithValue("@check_number", check_number);               //貨號
                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "type", "2");
                            cmd3.CommandText = dbAdapter.genUpdateComm("tbCheckNumber", cmd3);         //修改
                            dbAdapter.execNonQuery(cmd3);

                            if (current_number == end_number)
                            {
                                current_number = start_number;
                            }
                            else
                            {
                                current_number += 1;
                            }

                            #region 發送日期
                            if (_print_date == "")
                            {
                                cmd_update.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日
                                _print_date = DateTime.Today.ToString("yyyy/MM/dd");
                            }
                            #endregion
                            #region  配送日期
                            if (_Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(_print_date);

                                if (_arrive_assign_date != "")
                                {
                                    supplier_date = Convert.ToDateTime(_arrive_assign_date);
                                }
                                else
                                {
                                    //配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
                                    int Week = (int)Convert.ToDateTime(_print_date).DayOfWeek;

                                    if (Convert.ToDateTime(_print_date).ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

                                    supplier_date = Convert.ToDateTime(_print_date);
                                    switch (Week)
                                    {
                                        case 5:
                                            supplier_date = supplier_date.AddDays(3);
                                            break;
                                        case 6:
                                            supplier_date = supplier_date.AddDays(2);
                                            break;
                                        default:
                                            supplier_date = supplier_date.AddDays(1);
                                            break;
                                    }
                                    while (Utility.IsHolidays(supplier_date))
                                    {
                                        supplier_date = supplier_date.AddDays(1);         // temp
                                    }
                                }

                                cmd_update.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期                                
                            }
                            else
                            {

                                //1.配達，且配達區分 = 正常配交，就不可改指定日期
                                //2.指配日期不可以設定今天以前(含)的日期
                                if (_arrive_assign_date != "" && _arrive_assign_date != _Supplier_date)
                                {
                                    DateTime supplier_date = Convert.ToDateTime(_print_date);
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                    cmd_update.Parameters.AddWithValue("@supplier_date", supplier_date);          //配送日期                                    
                                }
                            }

                            cmd_update.Parameters.AddWithValue("@print_flag", "1");                              //是否列印                           

                            #endregion
                            cmd_update.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                            cmd_update.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd_update);      //修改
                            dbAdapter.execNonQuery(cmd_update);

                        }

                        #endregion




                        //if (option.SelectedValue == "1")
                        //{
                        //捲筒
                        PrintReelLabel(request_id.ToString());
                        //}
                        //else
                        //{
                        //    PrintLabel(print_requestid, option.SelectedValue);
                        //}

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
                        return;
                    }

                    #endregion

                    //if (!individual)
                    //{
                    //    SqlCommand cmd2 = new SqlCommand();
                    //    cmd2.CommandText = "usp_GetShipFeeByRequestId";
                    //    cmd2.CommandType = CommandType.StoredProcedure;
                    //    cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                    //    using (DataTable dt = dbAdapter.getDataTable(cmd2))
                    //    {
                    //        if (dt != null && dt.Rows.Count > 0)
                    //        {
                    //            status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                    //            if (status_code == 1)
                    //            {
                    //                supplier_fee = Convert.ToInt32(dt.Rows[0]["supplier_fee"]);       //配送費用
                    //                cscetion_fee = Convert.ToInt32(dt.Rows[0]["cscetion_fee"]);       //C配運價
                    //                                                                                  //unit_price = Convert.ToInt32(dt.Rows[0]["supplier_unit_fee"]);    //單位價格
                    //                                                                                  //unit_price_c = Convert.ToInt32(dt.Rows[0]["cscetion_unit_fee"]);  //單位價格-C配

                    //                //回寫到託運單的費用欄位
                    //                using (SqlCommand cmd3 = new SqlCommand())
                    //                {
                    //                    cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                    //                    cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                    //                    cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                    //                    cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id.ToString());
                    //                    cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                    //                    try
                    //                    {
                    //                        dbAdapter.execNonQuery(cmd3);
                    //                    }
                    //                    catch (Exception ex)
                    //                    {
                    //                        string strErr = string.Empty;
                    //                        if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                    //                        strErr = "網路託運單-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                    //                        //錯誤寫至Log
                    //                        PublicFunction _fun = new PublicFunction();
                    //                        _fun.Log(strErr, "S");
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                }

            }
            #endregion


        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            ErrStr += ex.ToString();
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "網路託運單" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }
        if (ErrStr == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');parent.$.fancybox.close();parent.location.reload(true);</script>", false);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
        }
        return;
        //}

    }


    /// <summary>
    /// 列印標籤(捲筒列印)(依計價模式為列印張數單位)
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReelLabel(string print_requestid)
    {
        if (print_requestid != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = " and request_id in(" + print_requestid + ")";
                string orderby = " order by  A.check_number";
                //if (Suppliers.SelectedValue + Customer.SelectedValue == "0000049") orderby = " order by  A.cdate , A.request_id"; //三陽工業，指定依打單的順序列印
                cmd.CommandText = string.Format(@"select isnull(D.code_name,'') + CASE A.receipt_flag WHEN 1 THEN '回單' ELSE '' END + CASE A.pallet_recycling_flag WHEN 1 THEN '棧板回收' ELSE '' END  AS title1,
                                                  A.print_date ,
                                                  A.supplier_date,
                                                  A.area_arrive_code,
                                                  B.code_name as subpoena_category_name,
                                                  CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
                                                  CASE A.pricing_type WHEN '03' THEN '才數' ELSE '總板數' END  as  title2, 
                                                  CASE A.pricing_type WHEN '03' THEN  A.cbm ELSE A.plates  END  as plates,
                                                  A.pieces, 
                                                  A.check_number , 
                                                  'a'+ A.check_number +'a' as barcode,
                                                  A.receive_contact,
                                                  CASE WHEN A.receive_tel1_ext <> '' THEN A.receive_tel1 + ' #' +  A.receive_tel1_ext  ELSE A.receive_tel1 END  +  ' ' + A.receive_tel2 AS receive_tel,
                                                  A.receive_city + A.receive_area + A.receive_address as receive_address,
                                                  A.invoice_desc,
                                                  A.send_contact,
                                                  A.send_tel ,
                                                  A.send_city + A.send_area + A.send_address as send_address,
	                                              CASE A.pricing_type WHEN '01' THEN A.plates WHEN  '02' THEN A.pieces WHEN '03' THEN A.cbm ELSE A.plates END as pages
                                                  from tcDeliveryRequests A WITH(Nolock)
                                                  left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                                  left join tbCustomers C on C.customer_code = A.customer_code
                                                  left join tbItemCodes D on D.code_id  = A.pricing_type  and d.code_sclass  = 'PM'
                                                  where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                DataTable DT = dbAdapter.getDataTable(cmd);
                if (DT != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = DT.Clone();  //複製DT的結構
                    for (int i = 0; i <= DT.Rows.Count - 1; i++)
                    {
                        DT.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["barcode"].ToString()));
                        int pages = DT.Rows[i]["pages"] == DBNull.Value ? 1 : Convert.ToInt32(DT.Rows[i]["pages"]);
                        for (int j = 1; j <= pages; j++)
                        {
                            dt2.ImportRow(DT.Rows[i]);
                        }
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];
                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "ReelLabel3", ParName, ParValue, "dsRPT_DeliveryRequests", dt2);
                }


            }

        }


    }

    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {

        using (var barcode = new Barcode()
        {

            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 250,
            Height = 50,
            LabelFont = new Font("verdana", 10f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            return barcode.Encode(TYPE.Codabar, content);
        }
    }


}