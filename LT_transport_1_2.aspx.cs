﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LT_transport_1_2 : System.Web.UI.Page
{
    string send_station_code = "";
    bool check_address = true;
    public int start_number
    {
        get { return Convert.ToInt32(ViewState["start_number"]); }
        set { ViewState["start_number"] = value; }
    }

    public int end_number
    {
        get { return Convert.ToInt32(ViewState["end_number"]); }
        set { ViewState["end_number"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //Clear_customer();
        if (!IsPostBack)
        {
            Session["MasterCustomerCode"] = "";
            Session["product_type"] = "";

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            manager_type = Session["manager_type"].ToString(); //管理單位類別   

            master_code = Session["master_code"].ToString();

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }
            #region 客代編號
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";
                if (supplier_code != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and customer_code like ''+@supplier_code+'%' ";
                }

                cmd.Parameters.AddWithValue("@type", "1");
                cmd.CommandText = string.Format(@"Select customer_code , customer_code+ '-' + customer_shortname as name  
                                           From tbCustomers with(Nolock) 
                                           Where stop_shipping_code = '0' and pricing_code = '02' and type =@type
                                            {0}
                                         order by customer_code asc", wherestr);
                ddlcustomer.DataSource = dbAdapter.getDataTable(cmd);
                ddlcustomer.DataValueField = "customer_code";
                ddlcustomer.DataTextField = "name";
                ddlcustomer.DataBind();
                ddlcustomer.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }
            #endregion

            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";
                if (supplier_code != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and customer_code like ''+@supplier_code+'%' ";
                }
                cmd.Parameters.AddWithValue("@type", "1");
                cmd.CommandText = string.Format(@"Select 
                                    customer_shortname,customer_code,telephone,shipments_city+shipments_area+shipments_road as adders
                                    ,shipments_city
                                    ,shipments_area
                                    ,shipments_road 
                                           From tbCustomers with(Nolock) 
                                           Where stop_shipping_code = '0' and pricing_code = '02' and type =@type
                                            {0}
                                         order by customer_code asc", wherestr);
                cmd.Parameters.AddWithValue(@"customer_code", supplier_code);

                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    lbreceive_contact.Text = dt.Rows[0]["customer_shortname"].ToString();

                    lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();

                    lbreceive_tel1.Text = dt.Rows[0]["telephone"].ToString();

                    lbreceive_address.Text = dt.Rows[0]["adders"].ToString();

                    hid_receive_city.Value = dt.Rows[0]["shipments_city"].ToString();
                    hid_receive_area.Value = dt.Rows[0]["shipments_area"].ToString();
                    hid_receive_address.Value = dt.Rows[0]["shipments_road"].ToString();
                }
            }

            #region 郵政縣市
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from tbPostCity order by seq asc ";
                ddlsend_city.DataSource = dbAdapter.getDataTable(cmd);
                ddlsend_city.DataValueField = "city";
                ddlsend_city.DataTextField = "city";
                ddlsend_city.DataBind();
                ddlsend_city.Items.Insert(0, new ListItem("請選擇", ""));

                ddlsend_area.Items.Insert(0, new ListItem("請選擇", ""));
                //ddlzip.Items.Insert(0, new ListItem("郵遞區號", ""));
            }
            #endregion

        }
    }

    protected void btnselect_click(object sender, EventArgs e)
    {
        
        ddlcustomer.Visible = false;
        if (strkey.Text == "")
        {
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            //Clear_customer();
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入託運單號!!');</script>", false);
            return;
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@check_number", strkey.Text);
                cmd.CommandText = @"
                                    select 
                                    tcd.customer_code,--客代編號
                                    receive_contact,--原收件人
                                    receive_tel1,   --原收件人電話
                                    receive_city,   --原收件人縣/市
                                    receive_area,   --原收件人鄉/鎮/區
                                    substring(receive_address,0,50) as receive_address,--原收件人地址
                                    send_contact,   --原寄件人
                                    case when tcd.send_city = '' then tbc.shipments_city else send_city end send_city ,      --原寄件人縣/市
                                    case when tcd.send_area = '' then tbc.shipments_area else send_area end send_area ,      --原寄件人鄉/鎮/區
                                    substring(send_address,0,50) as send_address,   --原寄件人地址
                                    send_tel,       --原寄件人電話
                                    tcd.supplier_code,   --客代簡碼
                                    B.station_scode,
                                    B.station_name,
                                    tbc.MasterCustomerCode,
                                    tbc.product_type,

                                    pieces
                                    from tcDeliveryRequests tcd  with(Nolock) 
									left join   tbCustomers tbc  with(Nolock)  on tcd.customer_code = tbc.customer_code
									left join    ttArriveSitesScattered A With(Nolock)  on A.post_city=tcd.receive_city and A.post_area = tcd.receive_area 
                                    left join   tbStation B With(Nolock)  on A.station_code = B.station_scode
                                    where tcd.check_number = @check_number
                                    ";
                dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    lbstation_scode.Text = dt.Rows[0]["station_scode"].ToString().Replace("F", "") + " " + dt.Rows[0]["station_name"].ToString();
                    lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                    lbreceive_contact.Text = dt.Rows[0]["send_contact"].ToString();
                    lbreceive_tel1.Text = dt.Rows[0]["send_tel"].ToString();
                    lbreceive_address.Text = dt.Rows[0]["send_city"].ToString() + dt.Rows[0]["send_area"].ToString() + dt.Rows[0]["send_address"].ToString();
                    Session["MasterCustomerCode"] = dt.Rows[0]["MasterCustomerCode"].ToString();
                    Session["product_type"] = dt.Rows[0]["product_type"].ToString();

                    strsend_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                    strsend_tel.Text = dt.Rows[0]["receive_tel1"].ToString();
                    //tbPieces.Text= dt.Rows[0]["pieces"].ToString();
                    DataTable Cdt = new DataTable();
                    DataTable Adt = new DataTable();
                    using (SqlCommand Ccmd = new SqlCommand())
                    {
                        Ccmd.Parameters.AddWithValue("@city", dt.Rows[0]["receive_city"].ToString());
                        Ccmd.CommandText = "select * from tbPostCity where city = @city order by seq asc ";
                        Cdt = dbAdapter.getDataTable(Ccmd);
                    }
                    string tab_cit = "";

                    if (Cdt.Rows.Count <= 0)
                    {
                        ddlsend_city.SelectedValue = "";
                    }
                    else
                    {
                        ddlsend_city.SelectedValue = Cdt.Rows[0]["city"].ToString();
                    }
                    city_SelectedIndexChanged(null, null);
                    if (ddlsend_city.SelectedValue != "")
                    {
                        using (SqlCommand Ccmd = new SqlCommand())
                        {
                            Ccmd.Parameters.AddWithValue("@city", ddlsend_city.SelectedValue);
                            Ccmd.Parameters.AddWithValue("@area", dt.Rows[0]["receive_area"].ToString());
                            Ccmd.CommandText = "select * from tbPostCityArea where city = @city and area=@area ";
                            Adt = dbAdapter.getDataTable(Ccmd);
                        }
                    }
                    if (Adt.Rows.Count <= 0)
                    {
                        ddlsend_area.SelectedValue = "";
                    }
                    else
                    {

                        ddlsend_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();
                    }
                    area_SelectedIndexChanged(null, null);
                    strsend_address.Text = dt.Rows[0]["receive_address"].ToString();


                    hid_receive_city.Value = dt.Rows[0]["send_city"].ToString();
                    hid_receive_area.Value = dt.Rows[0]["send_area"].ToString();
                    hid_receive_address.Value = dt.Rows[0]["send_address"].ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無原託運單號資訊，請選擇客代編號代入收貨人資訊');</script>", false);
                    ddlcustomer.Visible = true;
                    return;
                }
            }
            using (SqlCommand cmd = new SqlCommand())
            {
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@post_city", ddlsend_city.Text);
                cmd.Parameters.AddWithValue("@post_area", ddlsend_area.Text);
                cmd.CommandText = @"
Select zip, post_area ,A.station_code,B.station_name  from ttArriveSitesScattered A With(Nolock)  
inner join tbStation B With(Nolock)  on A.station_code = B.station_scode
where A.post_city=@post_city and A.post_area = @post_area 
order by seq asc";
                dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    send_station_scode.Text = dt.Rows[0]["station_name"].ToString();
                }
            }
        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        bool isMasterCustomerCode = false;
        int request_id = 0;
        string insert_check_number = "";
        string check_number = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        string ErrStr = "";

        string customer_code = "";
        if (ddlcustomer.SelectedValue != "")
        {
            customer_code = ddlcustomer.SelectedValue;
        }
        else
        {
            customer_code = lbcustomer_code.Text;
        }

        string supplier_code = "";
        string supplier_name = "";

        Regex reg = new Regex("^[A-Za-z0-9]+$");
        if (strorder_number.Text != "")
        {
            if (!reg.IsMatch(strorder_number.Text))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('訂單編號不可輸入英文、數字以外的值!!');</script>", false);
                return;
            }
        }
        //if (strkey.Text != "")
        //{
        if (strsend_contact.Text == "" || strsend_tel.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入退貨人、退貨人電話');</script>", false);
            return;
        }


        if (ddlsend_city.SelectedValue == "" || ddlsend_area.SelectedValue == "" )
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇退貨人地址縣市、區');</script>", false);
            return;
        }
       
        
        var product_type_list = new List<string> { "2", "3" };
        
        if (product_type_list.Contains(Session["product_type"].ToString()) && (Session["account_code"].ToString() != Session["MasterCustomerCode"].ToString()))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋/超值箱客代需用同一主客代的月結客代才能打退貨單');</script>", false);
            return;
        }
       
        if (tbPieces.Text == "" || dlProductName.SelectedValue == "" || dlProductSpec.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入退貨件數、名稱、規格');</script>", false);
            return;
        }
        //預購袋不能建退貨單
        //string prefix_check_number = strkey.Text.Substring(0,3);

        //if(prefix_check_number == "880")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋帳號無法建立退貨單號');</script>", false);
        //    return;
        //}
        //else if(prefix_check_number == "550")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('超值箱帳號無法建立退貨單號');</script>", false);
        //    return;
        //}

        DataTable dt = new DataTable();
        if (strkey.Text != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@check_number", strkey.Text);
                cmd.CommandText = @"
                                    select 
                                    *
                                    from tcDeliveryRequests with(nolock) where check_number = @check_number
                                    ";
                dt = dbAdapter.getDataTable(cmd);
            }
        }
        try
        {

            #region 新增    
            using (SqlCommand cmd = new SqlCommand())
            {//NEXT VALUE FOR DBO.MYSEQ


                cmd.CommandText = @"INSERT INTO tcDeliveryRequests
                            (
  Less_than_truckload,
  CbmSize,
  pricing_type,
  customer_code,
  check_type,
  receive_customer_code,
  subpoena_category,
  receive_tel1,
  receive_contact,
  receive_city,
  receive_area,
  receive_address,
  remote_fee,
  area_arrive_code,
  receive_by_arrive_site_flag,
  pieces,
  plates,
  cbm,
  arrive_to_pay_freight,
  arrive_to_pay_append,
  collection_money,
  send_contact,
  send_tel,
  send_city,
  send_area,
  send_address,
  donate_invoice_flag,
  electronic_invoice_flag,
  invoice_memo,
  invoice_desc,
  time_period,
  receipt_flag,
  pallet_recycling_flag,
  add_transfer,
  sub_check_number,
  supplier_code,
  supplier_name,
  cuser,
  cdate,
  uuser,
  udate,
  print_date,
  print_flag,
  DeliveryType,
check_number,
order_number,
return_check_number,
ProductId,
SpecCodeId,
SendCode,
SendSD,
SendMD,
ReceiveCode,
ReceiveSD,
ReceiveMD

) 
                            VALUES (
 @Less_than_truckload,
  @CbmSize,
  @pricing_type,
  @customer_code,
  @check_type,
  @receive_customer_code,
  @subpoena_category,
  @receive_tel1,
  @receive_contact,
  @receive_city,
  @receive_area,
  @receive_address,
  @remote_fee,
  @area_arrive_code,
  @receive_by_arrive_site_flag,
  @pieces,
  @plates,
  @cbm,
  @arrive_to_pay_freight,
  @arrive_to_pay_append,
  @collection_money,
  @send_contact,
  @send_tel,
  @send_city,
  @send_area,
  @send_address,
  @donate_invoice_flag,
  @electronic_invoice_flag,
  @invoice_memo,
  @invoice_desc,
  @time_period,
  @receipt_flag,
  @pallet_recycling_flag,
  @add_transfer,
  @sub_check_number,
  @supplier_code,
  @supplier_name,
  @cuser,
  @cdate,
  @uuser,
  @udate,
  @print_date,
  @print_flag,
  @DeliveryType,
   NEXT VALUE FOR ttAutoNumeral ,
  @order_number,
  @return_check_number,
  @ProductId,
  @SpecCodeId,
@SendCode,
@SendSD,
@SendMD,
@ReceiveCode,
@ReceiveSD,
@ReceiveMD
) ;
SELECT SCOPE_IDENTITY()
";

                var subpoena_code = "";

                if (Session["account_code"].ToString() != null && Session["account_code"].ToString().Length == 11 && Session["account_code"].ToString().StartsWith("F"))
                {
                    subpoena_code = "21"; //客戶自己建檔的話傳票類別為到付
                }
                else
                {
                    subpoena_code = "11"; //站所人員建檔的話傳票類別為元付
                }
                ////地址解析寄件地址
                var send_info = GetAddressParsing(ddlsend_city.Text.Trim() + ddlsend_area.Text.Trim() + strsend_address.Text.Trim(), customer_code.Trim(), "1", "1");
                if (send_info != null)
                {
                    //寫入集區SDMD
                    string SendCode = send_info.StationCode;
                    string SendSD = send_info.SendSalesDriverCode == null ? "" : send_info.SendSalesDriverCode;
                    string SendMD = send_info.SendMotorcycleDriverCode == null ? "" : send_info.SendMotorcycleDriverCode;
                    cmd.Parameters.AddWithValue("@SendCode", SendCode);
                    cmd.Parameters.AddWithValue("@SendSD", SendSD);
                    cmd.Parameters.AddWithValue("@SendMD", SendMD);
                }

                //地址解析收件地址
                var receive_info = GetAddressParsing(lbreceive_address.Text.Trim(), customer_code.Trim(), "1", "1");
                if (receive_info != null)
                {
                    //寫入配區SDMD
                    string ReceiveCode = receive_info.StationCode;
                    string ReceiveSD = receive_info.SendSalesDriverCode==null?"": receive_info.SendSalesDriverCode;
                    string ReceiveMD = receive_info.SendMotorcycleDriverCode == null ? "" : receive_info.SendMotorcycleDriverCode;
                    cmd.Parameters.AddWithValue("@ReceiveCode", ReceiveCode);
                    cmd.Parameters.AddWithValue("@ReceiveSD", ReceiveSD);
                    cmd.Parameters.AddWithValue("@ReceiveMD", ReceiveMD);
                }



                cmd.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);      //產品名
                cmd.Parameters.AddWithValue("@SpecCodeId", dlProductSpec.SelectedValue);  //規格

                //有對應單號
                if (dt.Rows.Count > 0)
                {
                    cmd.Parameters.AddWithValue("@CbmSize", dt.Rows[0]["CbmSize"].ToString());                    //材積大小
                   
                    cmd.Parameters.AddWithValue("@check_type", dt.Rows[0]["check_type"].ToString());              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", dt.Rows[0]["receive_customer_code"].ToString());            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_code);
                    cmd.Parameters.AddWithValue("@remote_fee", dt.Rows[0]["remote_fee"].ToString());              //偏遠區加價
                    cmd.Parameters.AddWithValue("@pieces", tbPieces.Text);                                             //件數
                    cmd.Parameters.AddWithValue("@plates", dt.Rows[0]["plates"].ToString());                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", dt.Rows[0]["cbm"].ToString());                                                   //才數
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", dt.Rows[0]["arrive_to_pay_freight"].ToString());                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", dt.Rows[0]["arrive_to_pay_append"].ToString());              //到付追加
                    cmd.Parameters.AddWithValue("@collection_money", 0);                  //代收金
                    cmd.Parameters.AddWithValue("@time_period", dt.Rows[0]["time_period"].ToString());            //時段

                    if (strkey.Text.StartsWith("880") || strkey.Text.StartsWith("500"))
                    {
                        //寫入productId
                        //string productId = checkMasterCustomerCode(Session["account_code"].ToString(), Convert.ToInt32(tbPieces.Text));
                        //cmd.Parameters.AddWithValue("@ProductId", productId);
                        cmd.Parameters.AddWithValue("@customer_code", Session["MasterCustomerCode"].ToString());

                        
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@customer_code", dt.Rows[0]["customer_code"].ToString());        //客代編號
                        
                    }                   

                    
                }

                else
                {
                    cmd.Parameters.AddWithValue("@CbmSize", 0);                    //材積大小
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);                     //客代編號
                    cmd.Parameters.AddWithValue("@check_type", "001");              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", "");            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_code);
                    cmd.Parameters.AddWithValue("@remote_fee", 0);              //偏遠區加價
                    cmd.Parameters.AddWithValue("@pieces", tbPieces.Text);                                             //件數
                    cmd.Parameters.AddWithValue("@plates", 0);                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", 0);                                                   //才數
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", 0);                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", 0);              //到付追加
                    cmd.Parameters.AddWithValue("@collection_money", 0);                  //代收金
                    cmd.Parameters.AddWithValue("@time_period", "午");            //時段
                    if (ddlcustomer.SelectedValue.ToString() == "")
                    {
                        cmd.Parameters.AddWithValue("@area_arrive_code", customer_code.Substring(0, 3).Replace("F", ""));
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@area_arrive_code", ddlcustomer.SelectedValue.ToString().Substring(0, 3).Replace("F", ""));             //到著碼
                    }

                }
                if (!string.IsNullOrEmpty(strkey.Text))
                {
                    using (SqlCommand cmd2 = new SqlCommand())
                    {
                        DataTable dt2 = new DataTable();

                        cmd2.Parameters.AddWithValue("@customer_code", lbcustomer_code.Text);
                        cmd2.Parameters.AddWithValue("@post_city", hid_receive_city.Value);
                        cmd2.Parameters.AddWithValue("@post_area", hid_receive_area.Value);
                        cmd2.Parameters.AddWithValue("@check_number", strkey.Text);

                        cmd2.CommandText = @"select return_address_flag,station_code,* from tcDeliveryRequests R1
left join ttArriveSitesScattered A1 on send_city=post_city and send_area=post_area
left join tbCustomers C1 on C1.customer_code=R1.customer_code
where check_number=@check_number";
                        dt2 = dbAdapter.getDataTable(cmd2);

                        //有對應單號
                        if (dt2.Rows.Count > 0)
                        {
                            string return_address_flag = dt2.Rows[0]["return_address_flag"].ToString();
                            string station_code = dt2.Rows[0]["station_code"].ToString();
                            //無勾選依照寄件地址退貨,依supplier_code作為到著
                            if (return_address_flag != "True")
                            {
                                var bb = dt.Rows[0]["supplier_code"].ToString();
                                var aa = dt.Rows[0]["supplier_code"].ToString().Replace("F", "");
                                cmd.Parameters.AddWithValue("@area_arrive_code", dt.Rows[0]["supplier_code"].ToString().Replace("F", "")); //到著碼
                            }
                            else
                            {
                                //依照寄件地址退貨
                                //地址解析收件地址
                                if (receive_info != null)
                                {
                                    //寫入到著碼
                                    string ReceiveCode = receive_info.StationCode;

                                    cmd.Parameters.AddWithValue("@area_arrive_code", ReceiveCode);

                                }
                                //找不到依二級地址
                                else
                                {
                                    cmd.Parameters.AddWithValue("@area_arrive_code", station_code);
                                }
                            }

                        }
                    }
                }


                cmd.Parameters.AddWithValue("@Less_than_truckload", 1);                                       //0：棧板運輸  1：零擔運輸
                cmd.Parameters.AddWithValue("@pricing_type", "02");          //計價模式 (01:論板、02:論件、03論才、04論小板)
                cmd.Parameters.AddWithValue("@receive_tel1", lbreceive_tel1.Text);                              //電話1
                cmd.Parameters.AddWithValue("@receive_contact", lbreceive_contact.Text);                        //收件人    
                cmd.Parameters.AddWithValue("@receive_city", hid_receive_city.Value);                           //收件地址-縣市
                cmd.Parameters.AddWithValue("@receive_area", hid_receive_area.Value);                           //收件地址-鄉鎮市區
                cmd.Parameters.AddWithValue("@receive_address", hid_receive_address.Value);                     //收件地址-路街巷弄號

                DataTable dts = new DataTable();
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.AddWithValue("@post_city", hid_receive_city.Value);
                    cmd1.Parameters.AddWithValue("@post_area", hid_receive_area.Value);
                    cmd1.CommandText = "select station_code from ttArriveSitesScattered where post_city= @post_city and post_area = @post_area";
                    dts = dbAdapter.getDataTable(cmd1);
                }


                cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", 0);                                       //到站領貨 0:否、1:是

                cmd.Parameters.AddWithValue("@arrive_address", hid_receive_address.Value);                            //到著碼地址


                cmd.Parameters.AddWithValue("@send_contact", strsend_contact.Text);                                       //寄件人
                cmd.Parameters.AddWithValue("@send_tel", strsend_tel.Text.ToString());                           //寄件人電話
                cmd.Parameters.AddWithValue("@send_city", ddlsend_city.SelectedValue.ToString());                //寄件人地址-縣市
                cmd.Parameters.AddWithValue("@send_area", ddlsend_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
                cmd.Parameters.AddWithValue("@send_address", strsend_address.Text);                              //寄件人地址-號街巷弄號

                cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.Value);                             //收回品項
                cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //(備註說明)


                cmd.Parameters.AddWithValue("@receipt_flag", 0);                           //是否回單
                cmd.Parameters.AddWithValue("@pallet_recycling_flag", 0);         //是否棧板回收

                cmd.Parameters.AddWithValue("@add_transfer", 0);                                              //是否轉址
                cmd.Parameters.AddWithValue("@sub_check_number", "000");                                      //次貨號
                using (SqlCommand cmdp = new SqlCommand())
                {
                    cmdp.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
                                      " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                                      " where c.customer_code = '" + customer_code + "' ";
                    using (DataTable dtss = dbAdapter.getDataTable(cmdp))
                    {
                        if (dtss.Rows.Count > 0)
                        {
                            supplier_code = dtss.Rows[0]["supplier_code"].ToString().Trim();        //區配
                            supplier_name = dtss.Rows[0]["supplier_name"].ToString().Trim();        // 區配名稱
                        }
                    }
                }


                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);                                 //配送商代碼
                cmd.Parameters.AddWithValue("@supplier_name", supplier_name);                                 //配送商名稱
                cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間
                cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                                     //出貨日期
                cmd.Parameters.AddWithValue("@print_flag", 0);                                                //是否列印
                cmd.Parameters.AddWithValue("@return_check_number", strkey.Text);

                check_number = strkey.Text;

                cmd.Parameters.AddWithValue("@DeliveryType", "R");



                //cmd.Parameters.AddWithValue("@check_number", strcheck_number.Text);                         //託運單號(貨號) 現階段長度10~20碼
                cmd.Parameters.AddWithValue("@order_number", strorder_number.Text);                           //訂單號碼(退貨單號)
                                                                                                              //cmd.Parameters.AddWithValue("@check_number", "NEXT VALUE FOR ttAutoNumeral");

                //cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                {
                    using (SqlCommand cmdch = new SqlCommand())
                    {
                        cmdch.Parameters.AddWithValue("@request_id", request_id);
                        cmdch.CommandText = @"select check_number from tcDeliveryRequests with(nolock) where request_id = @request_id";
                        using (DataTable dt3 = dbAdapter.getDataTable(cmdch))
                        {
                            insert_check_number = dt3.Rows[0][0].ToString();
                        }
                    }
                }

                //寫入CBMDetailLog
                using (SqlCommand cmdcbm = new SqlCommand())
                {
                    cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                    cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);

                    using (SqlCommand cmds = new SqlCommand())
                    {
                        cmds.CommandText = string.Format(@"Select check_number,SpecCodeId From tcDeliveryRequests with(nolock) where check_number ='" + insert_check_number + "'");
                        DataTable dtc = dbAdapter.getDataTable(cmds);
                        if (dtc != null && dtc.Rows.Count > 0)
                        {
                            string CBM = dtc.Rows[0]["SpecCodeId"].ToString();
                            if (CBM == "")
                            {
                                using (SqlCommand cmdc = new SqlCommand())
                                {
                                    cmdc.CommandText = "select product_type from tbCustomers where customer_code = '" + lbcustomer_code.Text.ToString().Trim() + "' ";
                                    using (DataTable dtp = dbAdapter.getDataTable(cmdc))
                                        if (dtp.Rows.Count > 0)
                                        {
                                            string product_type = dtp.Rows[0]["product_type"].ToString().Trim();
                                            if (product_type == "1")  //一般件 沒有的話選 S090
                                            {
                                                CBM = "S090";
                                            }
                                            else if (product_type == "2")  //預購袋 沒有的話選 B003
                                            {
                                                CBM = "B003";
                                            }
                                            else if (product_type == "3")  //超值箱 沒有的話選 X003
                                            {
                                                CBM = "X003";
                                            }
                                            else if (product_type == "4" || product_type == "5")  //商城、內部文件 沒有的話選 N001
                                            {
                                                CBM = "N001";
                                            }
                                            else { CBM = "S090"; }
                                        }
                                }
                            }
                            cmdcbm.Parameters.AddWithValue("@CBM", CBM);
                        }
                    }
                    cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                    cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                    cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                    dbAdapter.execNonQuery(cmdcbm);
                }

                //cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                //    using (SqlCommand cmdch = new SqlCommand()) {
                //        cmdch.Parameters.AddWithValue("@request_id", dt2.Rows[0][0].ToString());
                //        cmdch.CommandText = @"select check_number with(nolock) from tcDeliveryRequests where request_id = @request_id";
                //        using (DataTable dt3 = dbAdapter.getDataTable(cmdch))
                //        {
                //            insert_check_number = dt3.Rows[0][0].ToString();
                //        }
                //    }
                //}
            }

            #endregion
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            ErrStr += ex.ToString();
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "零擔託運單" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }
        if (ErrStr == "")
        {
            //DataTable seldt = new DataTable();
            //using (SqlCommand cmd = new SqlCommand())
            //{

            //    cmd.Parameters.AddWithValue("@check_number", strkey.Text);
            //    cmd.CommandText = @"
            //                        select 
            //                        *
            //                        from tcDeliveryRequests with(nolock) where send_contact = @check_number
            //                        ";
            //    seldt = dbAdapter.getDataTable(cmd);
            //}
            //ddlzip.Items.Clear();
            //ddlzip.Items.Add(new ListItem("郵遞區號", ""));
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            ddlcustomer.Items.Clear();
            ddlcustomer.Visible = false;
            ddlcustomer.Items.Add(new ListItem("請選擇", ""));
            //Clear_customer();
            //if (seldt.Rows.Count > 0)
            //{

            //strcheck_number.Text = seldt.Rows[0]["check_number"].ToString();

            if (strkey.Text != "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成 託運單號為：" + insert_check_number + "  !!');</script>", false);
                strsend_contact.Text = "";          // 名稱
                strsend_tel.Text = "";              // 電話
                strkey.Text = "";

                strcheck_number.Text = "";

                strorder_number.Text = "";
                invoice_memo.Value = "";
                invoice_desc.Value = "";
                //新增完成後不清空地址相關欄位
                //hid_receive_city.Value = "";
                //hid_receive_area.Value = "";
                //hid_receive_address.Value = "";

                //ddlzip.SelectedIndex = -1;          // 郵遞區號
                ddlzip.Text = "選擇地址後自動帶出";
                ddlsend_city.SelectedIndex = -1;    // 出貨地址-縣市
                ddlsend_area.SelectedIndex = -1;    // 出貨地址-鄉鎮市區
                strsend_address.Text = "";       //出貨地址-路街巷弄號
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成 託運單號為：" + insert_check_number + "  !!');</script>", false);
                strsend_contact.Text = "";          // 名稱
                strsend_tel.Text = "";              // 電話
                strkey.Text = "";

                strcheck_number.Text = "";

                strorder_number.Text = "";
                invoice_memo.Value = "";
                invoice_desc.Value = "";
                //新增完成後不清空地址相關欄位
                //hid_receive_city.Value = "";
                //    hid_receive_area.Value = "";
                //    hid_receive_address.Value = "";

                //ddlzip.SelectedIndex = -1;          // 郵遞區號
                ddlzip.Text = "選擇地址後自動帶出";
                ddlsend_city.SelectedIndex = -1;    // 出貨地址-縣市
                ddlsend_area.SelectedIndex = -1;    // 出貨地址-鄉鎮市區
                strsend_address.Text = "";       //出貨地址-路街巷弄號
            }
            //}
            //else
            //{
            //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
            //}
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
        }
        //}
        //else {
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入原託運單號，若無原託運單號，請輸入客代編號!!');</script>", false);
        //    return;
        //}

        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dtRequests = new DataTable();
            cmd.CommandText = @"update tcDeliveryRequests set return_check_number = @return_check_number where check_number = @check_number";
            cmd.Parameters.AddWithValue("@check_number", check_number);
            cmd.Parameters.AddWithValue("@return_check_number", insert_check_number);
            dtRequests = dbAdapter.getDataTable(cmd);
            if (dtRequests.Rows.Count > 0)
            {
                send_station_scode.Text = dtRequests.Rows[0]["station_name"].ToString();
            }
        }
    }
    protected void btnRomv_Click(object sender, EventArgs e)
    {
        Clear_customer();
    }

    protected void Clear_customer()
    {
        strsend_contact.Text = "";          // 名稱
        strsend_tel.Text = "";              // 電話
        strkey.Text = "";

        strcheck_number.Text = "";

        strorder_number.Text = "";
        invoice_memo.Value = "";
        invoice_desc.Value = "";

        hid_receive_city.Value = "";
        hid_receive_area.Value = "";
        hid_receive_address.Value = "";

        //ddlzip.SelectedIndex = -1;          // 郵遞區號
        ddlzip.Text = "選擇地址後自動帶出";
        ddlsend_city.SelectedIndex = -1;    // 出貨地址-縣市
        ddlsend_area.SelectedIndex = -1;    // 出貨地址-鄉鎮市區
        strsend_address.Text = "";       //出貨地址-路街巷弄號

        lbcustomer_code.Text = "";
        lbstation_scode.Text = "";    // 出貨地址-縣市
        lbreceive_contact.Text = "";          // 名稱
        lbreceive_tel1.Text = "";              // 電話
        lbreceive_address.Text = "";          // 郵遞區號
        //lbcustomer_name.Text = "";
    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlsend_city.SelectedValue != "")
        {
            ddlsend_area.Items.Clear();
            ddlsend_area.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", ddlsend_city.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    ddlsend_area.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            ddlsend_area.Items.Clear();
            ddlsend_area.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void area_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDownList dlzip = ddlzip;
        if (ddlsend_area.SelectedValue != "")
        {
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            //dlzip.Items.Clear();
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@post_city", ddlsend_city.SelectedValue.ToString());
            cmda.Parameters.AddWithValue("@post_area", ddlsend_area.SelectedValue.ToString());
            cmda.CommandText = @"
Select zip, post_area ,A.station_code,B.station_name  from ttArriveSitesScattered A With(Nolock)  
inner join tbStation B With(Nolock)  on A.station_code = B.station_scode
where A.post_city=@post_city and A.post_area = @post_area 
order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                //lbstation_scode.Text = dta.Rows[0]["station_code"].ToString().Trim() + " " + dta.Rows[0]["station_name"].ToString().Trim();
                send_station_scode.Text = dta.Rows[0]["station_code"].ToString().Trim() + " " + dta.Rows[0]["station_name"].ToString().Trim();
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    ddlzip.Text = dta.Rows[i]["zip"].ToString().Trim();
                    //dlzip.Items.Add(new ListItem(dta.Rows[i]["zip"].ToString().Trim(), dta.Rows[i]["zip"].ToString().Trim()));
                }
            }
        }
        else
        {
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            //dlzip.Items.Clear();
            //dlzip.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcustomer.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue(@"customer_code", ddlcustomer.SelectedValue);
                cmd.CommandText = @"select customer_shortname,customer_code,telephone,shipments_city+shipments_area+shipments_road as adders
                                    ,shipments_city
                                    ,shipments_area
                                    ,shipments_road
                                    from tbCustomers  
                                    where customer_code = @customer_code and
                                    stop_shipping_code = '0' and pricing_code = '02' order by customer_code asc ";
                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    lbreceive_contact.Text = dt.Rows[0]["customer_shortname"].ToString();

                    lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();

                    lbreceive_tel1.Text = dt.Rows[0]["telephone"].ToString();

                    lbreceive_address.Text = dt.Rows[0]["adders"].ToString();

                    hid_receive_city.Value = dt.Rows[0]["shipments_city"].ToString();
                    hid_receive_area.Value = dt.Rows[0]["shipments_area"].ToString();
                    hid_receive_address.Value = dt.Rows[0]["shipments_road"].ToString();
                }
            }
        }
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                    send_station_code = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }

    //public string checkMasterCustomerCode(string customerCode,int pieces)
    //{
    //    string product_type = string.Empty;
    //    bool is_new_customer = true;
    //    //string ProductId = string.Empty;

    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.Parameters.AddWithValue(@"customer_code", customerCode);
    //        cmd.CommandText = @"select product_type, is_new_customer from tbcustomers where customer_Code = @customer_code ";
    //        DataTable dt = dbAdapter.getDataTable(cmd);
    //        if (dt.Rows.Count > 0)
    //        {
    //            product_type = dt.Rows[0]["product_type"].ToString();
    //            is_new_customer = Boolean.Parse(dt.Rows[0]["is_new_customer"].ToString());
    //        }
    //    }

    //    if (product_type == "1")
    //    {
    //        if (pieces > 1)
    //        {
    //            if (is_new_customer == true)
    //            {
    //                ProductId = "CM000030";
    //            }
    //            else
    //            {
    //                ProductId = "CM000036";
    //            }
    //        }
    //        else
    //        {
    //            if (is_new_customer == true)
    //            {
    //                ProductId = "CS000029";
    //            }
    //            else
    //            {
    //                ProductId = "CS000035";
    //            }               
    //        }
    //    }
    //    else if (product_type == "2")
    //    {
    //        ProductId = "PS000031";
    //    }
    //    else if (product_type == "3")
    //    {
    //        ProductId = "PS000034";          

    //    }
    //    return ProductId;
    //}

    protected void dl_Pieces_SelectedIndexChanged(object sender, EventArgs e)
    {

        var product_type = "";
        bool is_new_customer = false;
      
            #region 產品
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            if (supplier_code == "" || supplier_code.Length>3)  //總部人員打單
            {
                cmd.Parameters.AddWithValue("@customer_code", lbcustomer_code.Text);
            }
            else 
            {
                cmd.Parameters.AddWithValue("@customer_code", supplier_code);
            }
                var table = dbAdapter.getDataTable(cmd);
                product_type = table.Rows[0]["product_type"].ToString().ToLower();
                is_new_customer = (bool)table.Rows[0]["is_new_customer"];
            }
        



        string sqlstr = "";
        if (tbPieces.Text != "")
        {
            if (Convert.ToInt32(tbPieces.Text) > 1)
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶多筆
                else if (product_type == "1") { sqlstr = "'CM000036'"; } //月結舊客戶多筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論件多筆
                else { sqlstr = "' '"; }
            }
            else
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶單筆
                else if (product_type == "1") { sqlstr = "'CS000035'"; } //月結舊客戶單筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論件單筆
                else { sqlstr = "' '"; }
            }
        }

        using (SqlCommand cmd8 = new SqlCommand())
        {
            //目前先寫死 (馬丁)
            cmd8.CommandText = @"select ProductId,
               case ProductId 
               when 'CM000030' then '論S'
               when 'CS000035' then '月結'
               when 'CM000036' then '月結'

               when 'CM000043' then '論件'
               else Name
              end as Name from productmanage 
              where ProductId in ( " + sqlstr + " ) and IsActive='1' order by id";

            dlProductName.DataSource = dbAdapter.getDataTableForFSE01(cmd8);
            dlProductName.DataValueField = "ProductId";
            dlProductName.DataTextField = "Name";
            dlProductName.DataBind();
            dlProductName.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion
    }
    protected void dl_ProductName_SelectedIndexChanged(object sender, EventArgs e)
    {


        #region 產品規格
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select  is_new_customer from tbCustomers where customer_code = @customer_code";
            if (supplier_code == "" || supplier_code.Length > 3)  //總部人員打單
            {
                cmd.Parameters.AddWithValue("@customer_code", lbcustomer_code.Text);
            }
            else 
            {
                cmd.Parameters.AddWithValue("@customer_code", supplier_code);
            }                        
            var table = dbAdapter.getDataTable(cmd);
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
        }

        using (SqlCommand cmd9 = new SqlCommand())
        {
            var strsql = "";



            //一般月結客代 - 多筆、單筆、月結多筆、月結單筆
            if (dlProductName.SelectedValue == "CM000030" || dlProductName.SelectedValue == "CS000029" || dlProductName.SelectedValue == "CM000036" || dlProductName.SelectedValue == "CS000035")
            {
                strsql = "'S060','S090','S110','S120','S150','B001','B002','B003'";
            }
            ////一般月結客代 - 月結袋
            //if (dlProductName.SelectedValue == "CS000035")
            //{
            //    strsql = "'B001','B002','B003'";
            //}

            //預購袋客代 - 預購袋 且為新客袋
            else if (dlProductName.SelectedValue == "PS000031" && is_new_customer == true)
            {
                strsql = "'B003'";
            }
            //預購袋客代 - 預購袋
            else if (dlProductName.SelectedValue == "PS000031")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 速配箱
            else if (dlProductName.SelectedValue == "PS000032")
            {
                strsql = "'X001','X002','X005'";
            }
            //預購袋客代 - 超值袋
            else if (dlProductName.SelectedValue == "PS000033")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 超值箱
            else if (dlProductName.SelectedValue == "PS000034")
            {
                strsql = "'X001','X002','X005'";
            }
            //內部文件
            else if (dlProductName.SelectedValue == "CS000037")
            {
                strsql = "'N001'";
            }
            //商城出貨 - 包材
            else if (dlProductName.SelectedValue == "CS000038")
            {
                strsql = "'N001'";
            }
            //一般件 - 論件
            else if (dlProductName.SelectedValue == "CM000043" )
            {
                strsql = "'C001','C002','C003','C004','C005'";
            }
            else { }

            if (dlProductName.SelectedValue == "CM000043")
            {
                cmd9.CommandText = "SELECT CodeId,CodeName FROM (select ROW_NUMBER()OVER (PARTITION BY SPEC ORDER BY P.ID DESC)SN,CodeId,CodeName,OrderBy  from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec' where ProductId = @ProductId and  CustomerCode = @customerCode and DeliveryType = 'D')T WHERE SN = '1' order by OrderBy asc";
                cmd9.Parameters.AddWithValue("@customerCode", supplier_code);
                cmd9.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
            else
            {
                cmd9.CommandText = "select CodeId,CodeName from ItemCodes with(nolock) where CodeType = 'ProductSpec' and CodeId in (" + strsql + ") order by id";
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
        }
        #endregion

    }


}
