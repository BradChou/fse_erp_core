﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_8.aspx.cs" Inherits="system_8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />  
     <script type="text/javascript">
        $(document).ready(function () {
            $(".monthpicker").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                defaultDate: 0,
                dateFormat: 'yy/mm',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                },
                onChangeMonthYear: function (iyy, imm, inst) {
                    $(this).val(iyy + "/" + imm);
                    //$('.ui-datepicker-close').click();
                }
            });
        });

        //check all from week day
        $(document).on("change", "._cko", function () {
            var _thisck = $(this).is(":checked");
            var _weekNum = $(this).attr("_week");
            $(".week" + _weekNum).prop("checked", _thisck);
        });

        $(document).on("click", "#btn_search", function () {
            var _this = $.trim($(".monthpicker").val());
            if (_this == "") {
                //錯誤訊息
                alert("請選取搜尋日期!");
                $(".monthpicker").focus();
            } else {
                //block ui + local url change
                //var _top = getUrlParameter('mm');
                var _url = location.href.split(/[?#]/)[0];
                location.replace(_url + "?mm=" + _this);
            }
        });

        
        function padLeft(str, lenght) {
            if (str.length >= lenght)
                return str;
            else
                return padLeft("0" + str, lenght);
        }

        $(document).on("click", "#btn_previous", function () {
            var _this = $.trim($(".monthpicker").val());
            if (_this == "") {
                //錯誤訊息
                alert("請選取搜尋日期!");
                $(".monthpicker").focus();
            } else {
                var _url = location.href.split(/[?#]/)[0];
                var date = new Date(_this + '/01');
                date.setMonth(date.getMonth() - 1);
                var y = padLeft(date.getFullYear().toString(), 2);
                var m = padLeft((date.getMonth()+1).toString(), 2);
                location.replace(_url + "?mm=" + y + "/" + m);
            }
        });

        $(document).on("click", "#btn_next", function () {
            var _this = $.trim($(".monthpicker").val());
            if (_this == "") {
                //錯誤訊息
                alert("請選取搜尋日期!");
                $(".monthpicker").focus();
            } else {
                var _url = location.href.split(/[?#]/)[0];
                var date = new Date(_this + '/01');
                date.setMonth(date.getMonth() + 1);
                var y = padLeft(date.getFullYear().toString(), 2);
                var m = padLeft((date.getMonth() + 1).toString(), 2);
                location.replace(_url + "?mm=" + y + "/" + m);
            }
        });


        $(document).on("click", "#btn_ok", function () {
            var _yesdates = $('input.ck_day[type=checkbox]:checked').map(function () { return $(this).prop('name'); }).get().join(',');
            var _nodates = $('input.ck_day[type=checkbox]:not(:checked)').map(function () { return $(this).prop('name'); }).get().join(',');
            var _sdate = $('input.ck_day[type=checkbox]').eq(0).prop('name');

            if (confirm("確認變更假日?")) {
                $.ajax({
                    url: 'system_8.aspx/DateSave',
                    type: 'POST',
                    async: false,//同步 
                    data: JSON.stringify({ sdate: _sdate, yesdates: _yesdates, nodates: _nodates  }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json"
                }).done(function (data, statusText, xhr) {

                    result = JSON.parse(data.d);

                    if (result.status >= 1) {
                        var _hday = parseInt(result.memo);
                        var _wday = parseInt($(".lbl_oday").html()) - _hday;
                        $(".lbl_wday").html(_wday);
                        $(".lbl_hday").html(_hday);
                        alert("設定成功");
                        //location.reload();
                        //location.replace(location.href);
                    } else {
                        alert(result.result);
                    }
                });
            }
        });


        $(document).on("click", "#btn_cancel", function () {
            var _this = $.trim($(".monthpicker").val());
            var _url = location.href.split(/[?#]/)[0];
            if (_this != "") {
                _url = _url + "?mm=" + _this;
            }
            location.replace(_url);
        });


    </script>

     <style type="text/css">
        .ui-datepicker-calendar {
            display: none;
        }

        .table_date_set > td label {
            font-weight: bold;
            font-size: larger;
        }

        .title2 {
            color: #5a5a5a;
            font-size: 13pt;
            padding-left: 5px;
        }

        .tip {
            padding: 3px 5px;
            line-height: 1.3;
            display: inline-block;
            font-weight: 400;
            vertical-align: middle;
        }


        .htip2 {
            color: #31708f;
            border-color: #31708f;
            background-color: #ffffff;
            border: solid 0.5px;
        }

        .htip3 {
            color: #ffffff;
            border-color: #31708f;
            background-color: #31708f;
            border: solid 0.5px;
        }

        .hoy_yy, .hoy_mm, .hoy_days, .hoy_hd, .hoy_wd {
            width: 8%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">行事曆</h2>

            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-12 col-md-12 form-group form-inline">
                        <label for="startMonth">年度/月份</label>
                        <asp:TextBox ID="startMonth" runat="server" CssClass="monthpicker form-control" placeholder="年/月" data-date-format="yyyy/mm"></asp:TextBox>
                        <div class="checkbox checkbox-danger" title="勾選表示「假日」" autocomplete="off">
                            <input id="ck_tip" class="styled" type="checkbox" disabled="disabled" checked="checked">
                            <label for="ck_tip">
                                已勾選表示「假日」
                            </label>
                        </div>
                        <input type="button" class="btn btn-primary" value="查詢" id="btn_search" />
                    </div>
                </div>
                <hr>
                <h1>
                    <asp:Literal ID="menuitem" runat="server"></asp:Literal>
                </h1>
                <p class="text-right">
                    <span class="tip htip1">總天數<asp:Label ID="lbl_ODay" runat="server" CssClass="lbl_oday"></asp:Label>日</span>
                    <span class="form-inline">
                        <span class="tip htip2">工作日<asp:Label ID="lbl_WDay" runat="server" CssClass="lbl_wday"></asp:Label>日</span>
                        <span class="tip htip3">非工作日<asp:Label ID="lbl_HDay" runat="server" CssClass="lbl_hday"></asp:Label>日</span>
                    </span>
                </p>
                <table class="table table-striped table-bordered templatemo-user-table table_date_set">
                    <thead>
                        <tr>
                            <td>
                                <div class="checkbox checkbox-danger" title="星期一" autocomplete="off">
                                    <input id="cko_1" class="styled _cko" _week="1" type="checkbox">
                                    <label for="cko_1">
                                        星期一
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="checkbox checkbox-danger" title="星期二" autocomplete="off">
                                    <input id="cko_2" class="styled _cko" _week="2" type="checkbox">
                                    <label for="cko_2">
                                        星期二
                                    </label>
                                </div>
                            </td>

                            <td>
                                <div class="checkbox checkbox-danger" title="星期三" autocomplete="off">
                                    <input id="cko_3" class="styled _cko" _week="3" type="checkbox">
                                    <label for="cko_3">
                                        星期三
                                    </label>
                                </div>
                            </td>

                            <td>
                                <div class="checkbox checkbox-danger" title="星期四" autocomplete="off">
                                    <input id="cko_4" class="styled _cko" _week="4" type="checkbox">
                                    <label for="cko_4">
                                        星期四
                                    </label>
                                </div>
                            </td>

                            <td>
                                <div class="checkbox checkbox-danger" title="星期五" autocomplete="off">
                                    <input id="cko_5" class="styled _cko" _week="5" type="checkbox">
                                    <label for="cko_5">
                                        星期五
                                    </label>
                                </div>
                            </td>

                            <td>
                                <div class="checkbox checkbox-danger" title="星期六" autocomplete="off">
                                    <input id="cko_6" class="styled _cko" _week="6" type="checkbox">
                                    <label for="cko_6">
                                        星期六
                                    </label>
                                </div>
                            </td>

                            <td>
                                <div class="checkbox checkbox-danger" title="星期日" autocomplete="off">
                                    <input id="cko_0" class="styled _cko" _week="0" type="checkbox">
                                    <label for="cko_0">
                                        星期日
                                    </label>
                                </div>
                            </td>

                        </tr>
                    </thead>

                    <asp:Panel ID="pan_Calendar" runat="server" CssClass="_calendar"></asp:Panel>

                    <!-- test end -->


                </table>

                <p class="text-center">
                    <input type="button" class="btn btn-success" id="btn_previous" value="上個月" />
                    <input type="button" class="btn btn-danger" id="btn_ok" value="確認" />
                    <input type="button" class="btn btn-default" id="btn_cancel" value="取消" />
                    <input type="button" class="btn btn-success" id="btn_next" value="下個月" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>

