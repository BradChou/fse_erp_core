﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="loan4_1_edit.aspx.cs" Inherits="loan4_1_edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        $(function () {
            $('.fancybox').fancybox();
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });
        });

        //function CalAmount(e, pnumber) {
        //    if (!/^\d+[.]?[1-9]?$/.test(pnumber)) {
        //        var newValue = /\d+[.]?[1-9]?/.exec(e.value);
        //        if (newValue != null) {
        //            e.value = newValue;
        //        }
        //        else {
        //            e.value = "";
        //        }
        //    }
        //    return false;
        //}

        function CalAmount() {
            var A = $("._A").val();
            var B = $("._B").val();
            if ($.isNumeric(A) && $.isNumeric(B)) {
                $("._C").val(A - B);                
            }
        }

        function CalAmountS() {
            var A = $("._SA").val();
            var B = $("._SB").val();
            if ($.isNumeric(A) && $.isNumeric(B)) {               
                $("._SC").val(A - B);
            }
        }


        function CalAmount2() {
            var Loan_Amount = $("._C").val();  //貸款金額
            var Interest_Rate = $("._R").val();
            var Loan_Years = $("._D").val();
            var Loan_Start = $("._Loan_Start").val();
            var Num_Pmt_Per_Year = $("._Num_Pmt_Per_Year").val();

            if (Loan_Amount * Interest_Rate * Loan_Years > 0) {
                //var Rate = Interest_Rate / Num_Pmt_Per_Year;  //為各期的利率
                if ($.isNumeric(Loan_Years) && $.isNumeric(Num_Pmt_Per_Year)) {
                    var Nper = Loan_Years * Num_Pmt_Per_Year;  //為貸款的總付款期數 
                    $("._G").val(Nper);
                }
                var i = Interest_Rate / 1200;
                var pmt = PMT(i, Nper, -Loan_Amount);
                jQuery('._F').val(pmt.toFixed(0));  //分期付款額 
            }
        }

        function CalAmount2S() {
            var Loan_Amount = $("._SC").val();  //貸款金額
            var Interest_Rate = $("._SR").val();
            var Loan_Years = $("._SD").val();
            var Loan_Start = $("._SLoan_Start").val();
            var Num_Pmt_Per_Year = $("._SNum_Pmt_Per_Year").val();

            if (Loan_Amount * Interest_Rate * Loan_Years > 0) {
                //var Rate = Interest_Rate / Num_Pmt_Per_Year;  //為各期的利率
                if ($.isNumeric(Loan_Years) && $.isNumeric(Num_Pmt_Per_Year)) {
                    var Nper = Loan_Years * Num_Pmt_Per_Year;  //為貸款的總付款期數 
                    $("._SG").val(Nper);
                }
                var i = Interest_Rate / 1200;
                var pmt = PMT(i, Nper, -Loan_Amount);
                jQuery('._SF').val(pmt.toFixed(0));  //分期付款額 
            }
        }

        //function CalAmount3() {
        //    var Loan_Amount = $("._SC").val();  //貸款金額
        //    var Interest_Rate = $("._SR").val();
        //    var Loan_Years = $("._D").val();
        //    var Loan_Start = $("._Loan_Start").val();
        //    var Num_Pmt_Per_Year = $("._Num_Pmt_Per_Year").val();

        //    if (Loan_Amount * Interest_Rate * Loan_Years > 0) {
              
        //        if ($.isNumeric(Loan_Years) && $.isNumeric(Num_Pmt_Per_Year)) {
        //            var Nper = Loan_Years * Num_Pmt_Per_Year;  //為貸款的總付款期數                     
        //        }
        //        var i = Interest_Rate / 1200;
        //        var pmt = PMT(i, Nper, -Loan_Amount);
        //        jQuery('._SF').val(pmt.toFixed(0));  //分期付款額 
        //    }
        //}

        function PMT(i, n, p) {
            return i * p * Math.pow((1 + i), n) / (1 - Math.pow((1 + i), n));
        }



    </script>

    <style>
        .bottnmargin {
            margin-bottom: 7px;
        }

        .row {
            line-height: 1.74;
        }

        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

        ._checkboxlist {
            display: inline;
        }

            ._checkboxlist label {
                min-width: 30px;
            }

        .unvisiable {
            display :none ;
        }

         ._th {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h2 class="margin-bottom-10">車輛貸款維護-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
    <div class="templatemo-login-form">
        <div class="panel-group">
            <div class="panel panel-info">
                <div class="panel-heading">貸款基本資料-公司</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>車牌</label>
                            <asp:TextBox ID="car_license" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="car_license" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請選擇車牌</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="car_price"><span class="REDWD_b">*</span>車價</label>
                            <asp:TextBox ID="car_price" runat="server" class="form-control _A" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="car_price" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入車價</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>頭款</label>
                            <asp:TextBox ID="first_payment" runat="server" class="form-control _B" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="first_payment" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入頭款</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="loan_price"><span class="REDWD_b">*</span>貸款金額</label>
                            <asp:TextBox ID="loan_price" runat="server" class="form-control _C" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>                            
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>年利率</label>
                            <asp:TextBox ID="annual_rate" runat="server" class="form-control _R" MaxLength="10" onkeyup="this.value=this.value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'')"></asp:TextBox>%                            
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="annual_rate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入年利率</asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>貸款起始日</label>
                            <asp:TextBox ID="loan_startdate" runat="server" class="form-control date_picker _Loan_Start"  autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="loan_startdate" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入貸款起始日</asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="loan_period"><span class="REDWD_b">*</span>貸款年限</label>
                            <asp:TextBox ID="loan_period" runat="server" class="form-control _D" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount2();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="loan_period" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入貸款年限</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="annual_pay_count"><span class="REDWD_b">*</span>每年付款次數</label>
                            <asp:TextBox ID="annual_pay_count" runat="server" class="form-control _Num_Pmt_Per_Year" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount2();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="annual_pay_count" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入每年付款次數</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="installment_price">分期付款額</label>
                            <%-- <asp:Label ID="installment_price" runat="server" CssClass ="form-control _F"  ></asp:Label>--%>
                            <asp:TextBox ID="installment_price" runat="server" CssClass="form-control _F"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="installment_count">分期付款次數</label>
                            <%--<asp:Label ID="installment_count" runat="server" CssClass ="form-control _G"  ></asp:Label>--%>
                            <asp:TextBox ID="installment_count" runat="server" CssClass="form-control _G"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="paid_count">實際付款次數</label>
                            <asp:Label ID="paid_count" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="interest">利息總額</label>
                            <asp:Label ID="interest" runat="server" CssClass="form-control"></asp:Label>
                            <asp:Button ID="cal" CssClass="templatemo-white-button" runat="server" Text="試算" OnClick="cal_Click" />
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="loan_company">貸款公司</label>
                            <asp:TextBox ID="loan_company" runat="server" CssClass="form-control "></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="memo">備註</label>
                            <asp:TextBox ID="memo" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-success ">
                <div class="panel-heading">區配&外車扣款</div>
                <div class="panel-body">
                    <div class="row ">
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="car_price"><span class="REDWD_b">*</span>車價</label>
                            <asp:TextBox ID="car_price_s" runat="server" class="form-control _SA" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmountS();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="car_price_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入車價</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>頭款</label>
                            <asp:TextBox ID="first_payment_s" runat="server" class="form-control _SB" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmountS();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="first_payment_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入頭款</asp:RequiredFieldValidator>
                        </div>
                         <div class="col-lg-4 bottnmargin form-inline">
                            <label for="deposit_s">訂金</label>
                            <asp:TextBox ID="deposit_s" runat="server" CssClass="form-control "  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="loan_price_s"><span class="REDWD_b">*</span>貸款金額</label>
                            <asp:TextBox ID="loan_price_s" runat="server" class="form-control _SC" MaxLength="10" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>                            
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>年利率</label>
                            <asp:TextBox ID="annual_rate_s" runat="server" class="form-control _SR" MaxLength="10" onkeyup="this.value=this.value.replace(/[^\d{1,}\.\d{1,}|\d{1,}]/g,'');"></asp:TextBox>%                            
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator4" runat="server" ControlToValidate="annual_rate_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入年利率</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label><span class="REDWD_b">*</span>貸款起始日</label>
                            <asp:TextBox ID="loan_startdate_s" runat="server" class="form-control date_picker _SLoan_Start"  autocomplete="off"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server" ControlToValidate="loan_startdate_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入貸款起始日</asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="loan_period_s"><span class="REDWD_b">*</span>貸款年限</label>
                            <asp:TextBox ID="loan_period_s" runat="server" class="form-control _SD" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount2S();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator5" runat="server" ControlToValidate="loan_period_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入貸款年限</asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="annual_pay_count"><span class="REDWD_b">*</span>每年付款次數</label>
                            <asp:TextBox ID="annual_pay_count_s" runat="server" class="form-control _SNum_Pmt_Per_Year" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'');CalAmount2S();"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator6" runat="server" ControlToValidate="annual_pay_count_s" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入每年付款次數</asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="installment_price_s">分期付款額</label>
                            <asp:TextBox ID="installment_price_s" runat="server" CssClass="form-control _SF"></asp:TextBox>
                        </div>
                       
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="installment_count_s">分期付款次數</label>                            
                            <asp:TextBox ID="installment_count_s" runat="server" CssClass="form-control _SG"></asp:TextBox>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="paid_count_s">實際付款次數</label>
                            <asp:Label ID="paid_count_s" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                        <div class="col-lg-4 bottnmargin form-inline">
                            <label for="interest_s">利息總額</label>
                            <asp:Label ID="interest_s" runat="server" CssClass="form-control"></asp:Label><asp:Button ID="calS" CssClass="templatemo-white-button" runat="server" Text="試算" OnClick="cal_Click" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_a1" data-toggle="tab">公司償還計畫表</a></li>
                        <li><a href="#tab_a2" data-toggle="tab">區配&外車償還計畫表</a></li>                     
                    </ul>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_a1">
                        <div class="row ">
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th"></th>
                                    <th class="_th">應付款日期</th>
                                    <th class="_th">期初金額</th>
                                    <th class="_th">分期付款額</th>
                                    <th class="_th">本金</th>
                                    <th class="_th">利息</th>
                                    <th class="_th">期末餘額</th>
                                    <th class="_th">累計利息</th>
                                    <th class="_th">實際付款日</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <ItemTemplate>
                                        <tr class="paginate">
                                            <td>
                                                <%# Container.ItemIndex + 1 %>
                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                            </td>
                                            <td data-th="日期">
                                                <asp:Label ID="lbdate" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.due_date","{0:yyyy-MM-dd}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="期初金額">
                                                <asp:Label ID="lbopening_balance" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.opening_balance" ,"{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="分期付款額">
                                                <asp:Label ID="lbinstallment_price" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.installment_price","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="本金">
                                                <asp:Label ID="lbprincipal" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.principal","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="利息">
                                                <asp:Label ID="lbinterest" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.interest","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="期末餘額">
                                                <asp:Label ID="lbend_balance" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.end_balance","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="累計利息">
                                                <asp:Label ID="lbtotal_interest" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_interest","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Literal ID="Lipaydate" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pay_date","{0:yyyy-MM-dd}").ToString())%>' Visible='<%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.paid")) == true  ) ? true : false   %>'></asp:Literal>
                                                <a href="loan4_1pay.aspx?id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>"
                                                    class='<%# (Convert.ToInt32(DataBinder.Eval(Container, "DataItem.id")) >0 && Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.paid")) == false ) ? "btn btn-info fancybox fancybox.iframe" : "btn btn-info fancybox fancybox.iframe unvisiable" %>'
                                                    id="payclick">付款
                                                </a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="9" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane " id="tab_a2">
                        <div class="row ">                           
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th"></th>
                                    <th class="_th">應付款日期</th>
                                    <th class="_th">期初金額</th>
                                    <th class="_th">分期付款額</th>
                                    <th class="_th">本金</th>
                                    <th class="_th">利息</th>
                                    <th class="_th">期末餘額</th>
                                    <th class="_th">累計利息</th>
                                    <th class="_th">實際付款日</th>
                                </tr>
                                <asp:Repeater ID="New_List_S" runat="server">
                                    <ItemTemplate>
                                        <tr class="paginate">
                                            <td>
                                                <%# Container.ItemIndex + 1 %>
                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                            </td>
                                            <td data-th="日期">
                                                <asp:Label ID="lbdate" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.due_date","{0:yyyy-MM-dd}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="期初金額">
                                                <asp:Label ID="lbopening_balance" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.opening_balance" ,"{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="分期付款額">
                                                <asp:Label ID="lbinstallment_price" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.installment_price","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="本金">
                                                <asp:Label ID="lbprincipal" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.principal","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="利息">
                                                <asp:Label ID="lbinterest" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.interest","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="期末餘額">
                                                <asp:Label ID="lbend_balance" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.end_balance","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td data-th="累計利息">
                                                <asp:Label ID="lbtotal_interest" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total_interest","{0:N0}").ToString())%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Literal ID="Lipaydate" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pay_date","{0:yyyy-MM-dd}").ToString())%>' Visible='<%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.paid")) == true  ) ? true : false   %>'></asp:Literal>
                                                <asp:Literal ID="Lipaid_price" runat="server" Text='<%# "已繳："+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.paid_price","{0:N0}").ToString()) + Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%>' Visible='<%# (Convert.ToInt32(DataBinder.Eval(Container, "DataItem.paid_price")) >0  ) ? true : false   %>'></asp:Literal>
                                                <a href="loan4_1pay.aspx?id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>"
                                                    class='<%# (Convert.ToInt32(DataBinder.Eval(Container, "DataItem.id")) >0 && Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.paid")) == false ) ? "btn btn-info fancybox fancybox.iframe" : "btn btn-info fancybox fancybox.iframe unvisiable" %>'
                                                    id="payclick">付款
                                                </a>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List_S.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="9" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                </div>
                


                <div class="form-group text-center">
                    <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                    <asp:LinkButton ID="btncancel" CssClass="templatemo-white-button" runat="server" PostBackUrl="~/loan4_1.aspx">取消</asp:LinkButton>
                    <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
                </div>

            </div>
        </div>
    </div>
</asp:Content>

