﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;

public partial class trace_3_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            if (Request.QueryString["supplier_code"] != null)
            {
                Suppliers.Text  = Request.QueryString["supplier_code"];
            } 
           
            if (Request.QueryString["dates"] != null)
            {
                dates.Text  = Request.QueryString["dates"];
            }

            if (Request.QueryString["datee"] != null)
            {
                datee.Text  = Request.QueryString["datee"];
            }
            
            if (Request.QueryString["kind"] != null)
            {
                kind.Text = Request.QueryString["kind"];
            }
            if (Request.QueryString["customer_code"] != null)
            {
                Hid_customer_code.Value = Request.QueryString["customer_code"];
            }
            if (Request.QueryString["type"] != null)
            {
                Hid_type.Value = Request.QueryString["type"];
            }
            

            readdata();
        }
    }

    private void readdata()
    {


        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            cmd.CommandTimeout = 99999;
            cmd.Parameters.Clear();

            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            if (dates.Text != "")
            {
                cmd.Parameters.AddWithValue("@dates", dates.Text);
                querystring += "&dates=" + dates.Text;
            }

            if (datee.Text != "")
            {
                cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));
                querystring += "&datee=" + datee.Text;
            }

            //if (Suppliers.Text  != "")
            //{
                cmd.Parameters.AddWithValue("@Suppliers", Suppliers.Text);
                querystring += "&supplier_code=" + Suppliers.Text;
            //}

            cmd.Parameters.AddWithValue("@customer_code", Hid_customer_code.Value);
            querystring += "&customer_code=" + Hid_customer_code.Value;
            
            cmd.Parameters.AddWithValue("@DeliveryType", Hid_type.Value);
            querystring += "&type=" + Hid_type.Value;


            #region old
            //switch (kind.Text)
            //{
            //    //case "1":  //應配筆數                    
            //    //    break;
            //    case "2":  //配送筆數
            //        strWhereCmd = " AND supplier_date IS NOT NULL";
            //        break;
            //    case "3":  //午前配達筆數
            //        strWhereCmd = " AND arrive_date IS NOT NULL AND supplier_date IS NOT NULL AND  (CONVERT(varchar ,arrive_date, 108)<=CONVERT(varchar ,'13:00:00', 108)) and (CONVERT(varchar ,arrive_date, 112)<=CONVERT(varchar ,supplier_date, 112))";
            //        break;
            //    case "4":  //當日配達筆數
            //        strWhereCmd = " AND arrive_date IS NOT NULL AND supplier_date IS NOT NULL AND (CONVERT(varchar ,arrive_date, 108)<=CONVERT(varchar ,'17:00:00', 108)) and (CONVERT(varchar ,arrive_date, 112)<=CONVERT(varchar ,supplier_date, 112))";
            //        break;
            //    case "5":  //已配達筆數
            //        strWhereCmd = " and arrive_date IS NOT NULL";
            //        break;
            //    case "6":  //未配達筆數
            //        strWhereCmd = " and arrive_date IS  NULL";
            //        break;
            //}
            #endregion

            #region new
            switch (kind.Text)
            {
                //case "1":  //應配筆數                    
                //    break;
                case "2":  //配送筆數
                    strWhereCmd = " AND info.send_date IS NOT NULL";
                    break;
                case "3":  //午前配達筆數
                    strWhereCmd = " AND info.arrive_date IS NOT NULL AND info.send_date IS NOT NULL AND  (CONVERT(varchar ,info.arrive_date, 108)<=CONVERT(varchar ,'13:00:00', 108)) and (CONVERT(varchar ,info.arrive_date, 112)<=CONVERT(varchar ,info.send_date, 112))";
                    break;
                case "4":  //當日配達筆數
                    strWhereCmd = " AND info.arrive_date IS NOT NULL AND info.send_date IS NOT NULL AND (CONVERT(varchar ,info.arrive_date, 108)<=CONVERT(varchar ,'17:00:00', 108)) and (CONVERT(varchar ,info.arrive_date, 112)<=CONVERT(varchar ,info.send_date, 112))";
                    break;
                case "5":  //已配達筆數
                    strWhereCmd = " and info.arrive_date IS NOT NULL";
                    break;
                case "6":  //未配達筆數
                    strWhereCmd = " and info.arrive_date IS  NULL";
                    break;
            }
            #endregion 

            querystring += "&kind=" + kind.Text;


            #endregion

            #region new 
            cmd.CommandText = string.Format(@"select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
    info.driver_code as driver_code ,
    info.driver_name as driver_name ,
    A.check_number,
    A.send_contact,
    A.receive_contact, 
    A.product_category ,
    D.code_name as product_category_name,
    CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
    info.supplier_time as supplier_date, 
    info.arrive_date as arrive_date,
    F.code_name as arrive_option,
    Convert(varchar,A.print_date,111) as print_date,
	CASE WHEN A.Less_than_truckload = '0' THEN 
		CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(B.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END 
	ELSE 
		sta2.station_name 
	END  as sendstation,

	CASE WHEN A.Less_than_truckload = '0' THEN 
		C.supplier_name
	ELSE 
		sta1.station_name 
	END  as arrivestation
	
    from tcDeliveryRequests  A with(nolock)
    LEFT JOIN tbCustomers B With(Nolock) on B.customer_code = A.customer_code
	LEFT JOIN tbSuppliers C With(Nolock) on C.supplier_code = A.area_arrive_code and ISNULL(A.area_arrive_code,'') <> ''
    LEFT JOIN tbItemCodes D with(nolock) on D.code_id = A.product_category and D.code_bclass = '2' and D.code_sclass = 'S3'
	LEFT JOIN tbStation sta1 with(nolock) on A.area_arrive_code = sta1.station_scode 
	LEFT JOIN ttArriveSitesScattered E with(nolock)  on A.send_city = E.post_city and A.send_area = E.post_area 
	LEFT JOIN tbStation sta2 with(nolock) on E.station_code = sta2.station_scode 
	CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id ) info 
    LEFT JOIN tbItemCodes F With(nolock) on info.arrive_option = F.code_id  and F.code_sclass = 'AO'
    where 1=1
	and A.print_date >= @dates and A.print_date < @datee
    AND ( A.area_arrive_code=@Suppliers or @Suppliers ='all' )
    and A.customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
    AND A.cancel_date IS NULL
    and A.Less_than_truckload =@Less_than_truckload
    and A.DeliveryType ='D'
	and A.check_number <> ''
    {0}
    order by A.check_number ", strWhereCmd);

            #endregion

            #region old
            //cmd.CommandText = string.Format(@"DECLARE  @NO NVARCHAR(5)
            //                                    DECLARE  @driver_code NVARCHAR(10)
            //                                    DECLARE  @driver_name NVARCHAR(10)
            //                                    DECLARE  @CHKNUM NVARCHAR(12)  --貨號
            //                                    DECLARE  @send_contact NVARCHAR(20) 
            //                                    DECLARE  @receive_contact NVARCHAR(20) 
            //                                    DECLARE  @product_category  NVARCHAR 
            //                                    DECLARE  @product_category_name  NVARCHAR 
            //                                    DECLARE  @quant  int
            //                                    DECLARE  @supplier_date  DATETIME
            //                                    DECLARE  @arrive_date  DATETIME
            //                                    DECLARE  @arrive_option NVARCHAR(10) 
            //                                    DECLARE  @print_date  NVARCHAR(10)
            //                                    DECLARE  @sendstation  NVARCHAR(10) 
            //DECLARE  @arrivestation  NVARCHAR(10) 

            //                                    select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
            //                                    '          ' as driver_code ,
            //                                    '                    ' as driver_name ,
            //                                    A.check_number,
            //                                    A.send_contact,
            //                                    A.receive_contact, 
            //                                    A.product_category ,
            //                                    D.code_name as product_category_name,
            //                                    CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
            //                                    @supplier_date as supplier_date, 
            //                                    @arrive_date as arrive_date,
            //                                    '          ' as arrive_option,
            //                                    Convert(varchar,A.print_date,111) as print_date,
            //                                    CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(B.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation,
            //C.supplier_name as arrivestation
            //                                    into #mylist	  
            //                                    from tcDeliveryRequests  A with(nolock)
            //                                    LEFT JOIN tbCustomers B With(Nolock) on B.customer_code = A.customer_code
            //LEFT JOIN tbSuppliers C With(Nolock) on C.supplier_code = A.area_arrive_code 
            //                                    LEFT JOIN tbItemCodes D with(nolock) on D.code_id = A.product_category and D.code_bclass = '2' and D.code_sclass = 'S3'
            //                                    where 
            //                                    --CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@dates,111) AND CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@datee,111)
            //                                    --AND A.supplier_code LIKE '%'+  ISNULL(@Suppliers,'') +'%'
            //                                    CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@dates,111) 
            //                                    AND CONVERT(VARCHAR,A.print_date,111) < CONVERT(VARCHAR,@datee,111)
            //                                    AND ( A.area_arrive_code=@Suppliers or @Suppliers ='all' )
            //                                    and A.customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
            //                                    AND A.cancel_date IS NULL
            //                                    and A.Less_than_truckload = @Less_than_truckload
            //                                    and A.DeliveryType =@DeliveryType
            //                                    and A.check_number <> ''
            //                                    order by A.check_number 

            //                                    DECLARE R_cursor CURSOR FOR
            //                                    select * FROM #mylist
            //                                    OPEN R_cursor
            //                                    FETCH NEXT FROM R_cursor INTO @NO,@driver_code, @driver_name, @CHKNUM,@send_contact ,@receive_contact ,@product_category , @product_category_name,@quant,@supplier_date,@arrive_date,@arrive_option,@print_date,@sendstation,@arrivestation
            //                                    WHILE (@@FETCH_STATUS = 0) 
            //                                     BEGIN
            //                                      select top 1 @driver_code=B.driver_code  , @driver_name = C.driver_name , @supplier_date = Convert(varchar,B.scan_date,120) from ttDeliveryScanLog B with (nolock)  
            //                                      left join tbDrivers C with(nolock) on B.driver_code = C.driver_code
            //                                      where check_number = @CHKNUM and scan_item = '2' AND B.cdate >=  DATEADD(MONTH,-6,getdate())  order by scan_date desc

            //                                            SET @arrive_date = NULL
            //                                      select top 1 @driver_code=B.driver_code  , @arrive_date =Convert(varchar,B.scan_date,120) , @arrive_option= isnull(AO.code_name,'') from ttDeliveryScanLog B with (nolock)  
            //                                      left join tbItemCodes AO with(nolock) on B.arrive_option = AO.code_id and AO.code_sclass = 'AO'
            //                                      where check_number = @CHKNUM and scan_item = '3' AND B.cdate >=  DATEADD(MONTH,-6,getdate())  order by scan_date desc



            //                                      UPDATE #mylist
            //                                        SET driver_code  =@driver_code , 
            //                                        driver_name =@driver_name, 
            //                                        supplier_date = @supplier_date, 
            //                                        arrive_date=@arrive_date ,
            //                                        arrive_option= @arrive_option
            //                                      WHERE check_number = @CHKNUM

            //                                     FETCH NEXT FROM R_cursor INTO @NO,@driver_code, @driver_name, @CHKNUM,@send_contact ,@receive_contact ,@product_category , @product_category_name,@quant,@supplier_date,@arrive_date,@arrive_option,@print_date,@sendstation,@arrivestation
            //                                     END
            //                                    CLOSE R_cursor
            //                                    DEALLOCATE R_cursor

            //                                    select * from #mylist WHERE 1=1 {0}
            //                                    IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
            //                                    DROP TABLE #mylist", strWhereCmd);
            #endregion


            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dt.DefaultView;
                objPds.AllowPaging = true;
                objPds.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                objPds.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!objPds.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!objPds.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (objPds.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
                }

                #endregion

                New_List.DataSource = objPds;
                New_List.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();
                DT = dt;
            }

        }

    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("配達明細");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 15;
            sheet1.Column(8).Width = 15;
            sheet1.Column(9).Width = 15;
            sheet1.Column(10).Width = 15;
            sheet1.Column(11).Width = 15;
            sheet1.Column(12).Width = 15;
            sheet1.Column(13).Width = 15;
            sheet1.Column(14).Width = 15;
            sheet1.Column(15).Width = 15;


            sheet1.Cells[1, 1, 1, 15].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 15].Value = "配達明細";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 15].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 15].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 15].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 15].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 15].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 15].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 15].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "發送日期：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            sheet1.Cells[3, 1, 3, 15].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1, 3, 15].Style.Font.Size = 12;
            sheet1.Cells[3, 1, 3, 15].Style.Font.Bold = true;
            sheet1.Cells[3, 1, 3, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 15].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 15].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 15].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "人員編號";
            sheet1.Cells[3, 3].Value = "作業人員";
            sheet1.Cells[3, 4].Value = "發送日";
            sheet1.Cells[3, 5].Value = "發送站";
            sheet1.Cells[3, 6].Value = "到著站";
            sheet1.Cells[3, 7].Value = "貨號";
            sheet1.Cells[3, 8].Value = "出貨人";
            sheet1.Cells[3, 9].Value = "收貨人";
            sheet1.Cells[3, 10].Value = "商品種類";
            sheet1.Cells[3, 11].Value = "發送件數";
            sheet1.Cells[3, 12].Value = "配送件數";
            sheet1.Cells[3, 13].Value = "配送時間";
            sheet1.Cells[3, 14].Value = "配達時間";
            sheet1.Cells[3, 15].Value = "配達區分";


            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["driver_code"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["driver_name"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["print_date"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["sendstation"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["arrivestation"].ToString();
                sheet1.Cells[i + 4, 7].Value = DT.Rows[i]["check_number"].ToString();
                sheet1.Cells[i + 4, 8].Value = DT.Rows[i]["send_contact"].ToString();
                sheet1.Cells[i + 4, 9].Value = DT.Rows[i]["receive_contact"].ToString();
                sheet1.Cells[i + 4, 10].Value = DT.Rows[i]["product_category_name"].ToString();
                sheet1.Cells[i + 4, 11].Value = DT.Rows[i]["quant"].ToString();
                sheet1.Cells[i + 4, 12].Value = DT.Rows[i]["quant"].ToString();
                sheet1.Cells[i + 4, 13].Value = DT.Rows[i]["supplier_date"].ToString();
                sheet1.Cells[i + 4, 14].Value = DT.Rows[i]["arrive_date"].ToString();
                sheet1.Cells[i + 4, 15].Value = DT.Rows[i]["arrive_option"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("配達明細.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}