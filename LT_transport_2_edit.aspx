﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LT_transport_2_edit.aspx.cs" Inherits="LT_transport_2_edit" EnableEventValidation="false" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-託運單修改</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>

    <link href="css/build.css" rel="stylesheet" />

    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            text-align: left;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .radio {
            padding-left: 5px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
            min-width: 0px;
        }

        .checkbox {
            padding-left: 15px;
        }

        ._shortwidth {
            width: 80px;
        }

        .ralign {
            text-align: right;
        }

        .address {
            width: 420px !important
        }

        .table-margin {
            margin-left: 115px;

        }

        .point{
            pointer-events : none;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('.fancybox').fancybox();

            //$("#btCancel").fancybox({
            //    'width': 300,
            //    'height': 300,
            //    'autoScale': false,
            //    'transitionIn': 'none',
            //    'transitionOut': 'none',
            //    'type': 'iframe',
            //    beforeClose: function () {
            //        //// working
            //        //var $iframe = $('.fancybox-iframe retval');
            //        //var retval = $iframe.val();
            //        //alert(retval);
            //        ////alert($('input', $iframe.contents()).val());
            //        ////alert('aaa');
            //    },


            //});


            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            $(".chosen-select").chosen();
            $(".subpoena_category").change();

            $(".subpoena_category").change();

<%--            $("#<%= turn_board.ClientID%>").change(function () {
                $("._turn_board_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= upstairs.ClientID%>").change(function () {
                $("._upstairs_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= difficult_delivery.ClientID%>").change(function () {
                $("._difficult_fee").prop('disabled', !$(this).is(":checked"));
            });--%>

            $("#<%= chkProductValue.ClientID%>").change(function () {
                $("._product_value").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= btnRecsel.ClientID%>").click(function () {
                var customercode = $("#<%= customer_code.ClientID%>").val();
                if (customercode == "") {
                    alert('請先選取客代');
                    return false;
                }
            });
        });
        $.fancybox.update();

        $(document).on("change", ".subpoena_category", function () {
            var _isOK = true;
            var _cateVal = $(this).children("option:selected").val();
            var _setItem;
            var _disItem;
            switch (_cateVal) {
                case '11'://元付
                    //_collection_money
                    _disItem = $("._collection_money");
                    _setItem = $("._arrive_to_pay_freight,._arrive_to_pay_append");
                    break;
                case '21'://到付
                    _disItem = $("._collection_money,._arrive_to_pay_append");
                    _setItem = $("._arrive_to_pay_freight");
                    break;
                case '25'://元付-到付追加
                    _disItem = $("._collection_money,._arrive_to_pay_freight");
                    _setItem = $("._arrive_to_pay_append");
                    break;
                case '41'://代收貨款
                    _disItem = $("._arrive_to_pay_append,._arrive_to_pay_freight");
                    _setItem = $("._collection_money");
                    break;
                default:
                    _isOK = false;
                    break;
            }
            if (_isOK) {

                _disItem.map(function () { $(this).prop('readonly', true); });
                _setItem.map(function () { $(this).prop('readonly', false); });

            }

        });

        function confirm_click() {
            return confirm("您確定要銷單嗎 ?");
            //window.event.returnValue = false;
        }

    </script>
    <style type="text/css">
        .ui-datepicker select.ui-datepicker-month,
        .ui-datepicker select.ui-datepicker-year {
            color: black;
        }

       

        .ui-datepicker a {
            color: #0275d8;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div style="width: 100%; position: fixed; top: 0px; left: 0px; height: 1000px; opacity: 0.1; z-index: 1000; background-color: white" runat="server" id="BK"></div>

        <div class="templatemo-content-widget white-bg">
            <div class="row">
                <div class="templatemo-login-form">
                    <%--<div class="form-group form-inline">
                        <label for="inputFirstName">託運類別：</label>
                        <div class="margin-right-15 templatemo-inline-block ">
                            <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged" Visible ="false"  >
                                <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                <asp:ListItem Value="02">02 論件</asp:ListItem>
                                <asp:ListItem Value="03">03 論才</asp:ListItem>
                                <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                <asp:ListItem Value="05">05 專車</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>--%>
                    <div class="form-group form-inline">
                        <label>
                            <asp:Label ID="lbDeliveryType" runat="server" Text="出貨"></asp:Label>日期：</label><asp:TextBox ID="Shipments_date" runat="server" CssClass="form-control date_picker " autocomplete="off"></asp:TextBox>
                        <asp:DropDownList ID="dlDeliveryType" runat="server" CssClass="form-control ">
                            <asp:ListItem Selected="True" Value="D">出貨</asp:ListItem>
                            <asp:ListItem Value="R">回收</asp:ListItem>
                        </asp:DropDownList>
                        <div id="plCancel" runat="server" class="form-group">
                            <span runat="server" class=" btn btn-warning "><a id="btCancel">銷單</a></span>
                        </div>
                        <div id="plNotCancel" runat="server" class="form-group">
                            <span runat="server" class=" btn btn-warning "><a id="btNotCancel">不可銷單</a></span>
                        </div>
                        <label class="" style="color: red;">※有掃讀歷程單號，不允許銷單</label>
                    </div>
                    <div class="form-group form-inline">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <label>客代編號：</label>
                                <asp:DropDownList ID="dlcustomer_code" runat="server" AutoPostBack="True" CssClass="form-control chosen-select" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:Label ID="lbcustomer_name" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代編號</asp:RequiredFieldValidator>
                                <div id="arrive_option" runat="server" style="color: red; display: none">貨件配達完成，不得再更改託運資料</div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                    <!--基本設定-->
                    <div class="bs-callout bs-callout-info">
                        <h3>1. 基本設定</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label for="lbcheck_number"><span class="REDWD_b">*</span>託運單號</label>
                                    <asp:Label ID="lbcheck_number" runat="server" BackColor="#FFFF66" CssClass="form-control "></asp:Label>
                                    <asp:HiddenField ID="hid_id" runat="server" />
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">訂單號碼</label>
                                    <asp:TextBox ID="order_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="inputLastName"><span class="REDWD_b">*</span>託運類別</label>
                                    <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">收貨人編號</label>
                                    <asp:TextBox ID="receive_customer_code" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="inputLastName"><span class="REDWD_b">*</span>傳票類別</label>
                                    <asp:DropDownList ID="subpoena_category" runat="server" class="form-control subpoena_category">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>2. 收件人</h3>
                        <div class="rowform">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="row form-inline">
                                        <div class="form-group">
                                            <label>收件人編號</label>
                                            <asp:TextBox ID="receiver_code" runat="server" class="form-control" AutoPostBack="true" OnTextChanged="receiver_code_TextChanged" Width="80px"></asp:TextBox>
                                            <asp:Button ID="btnRecsel" CssClass="templatemo-white-button" runat="server" Text="通訊錄" Font-Size="X-Small" OnClick="btnRecsel_Click" />
                                            <label><span class="REDWD_b">*</span>收件人</label>
                                            <asp:TextBox ID="receive_contact" runat="server" class="form-control" AutoPostBack="True" OnTextChanged="receive_contact_TextChanged" MaxLength="20"></asp:TextBox>
                                            <asp:Button Style="display: none" ID="btnqry" runat="server" Text="" />
                                            <div style="display: none">
                                                <asp:TextBox ID="receiver_id" runat="server" />
                                            </div>
                                            &nbsp;
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="cbSaveReceive" runat="server" Text="設為常用收件人" Font-Size="Smaller" />
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label><span class="REDWD_b">*</span>電話1</label>
                                            <asp:TextBox ID="receive_tel1" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                            分機
                                        <asp:TextBox ID="receive_tel1_ext" runat="server" class="form-control" MaxLength="5" Width="80"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>電話2</label>
                                            <asp:TextBox ID="receive_tel2" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <div id="divReceiver" runat="server" visible="false" class="templatemo-login-form panel panel-default" style="overflow: auto; height: 300px; background-color: aliceblue">
                                            <br />
                                            <div>
                                                <div class="form-group form-inline">
                                                    <label>關鍵字：</label>
                                                    <asp:TextBox ID="keyword" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                                    <asp:Button ID="cancel" CssClass="btn btn-default " runat="server" Text="關 閉" OnClick="cancel_Click" />
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered templatemo-user-table">
                                                <tr class="tr-only-hide">
                                                    <th nowrap class="text-center ">選取</th>
                                                    <th nowrap class="text-center ">客戶代碼</th>
                                                    <th nowrap class="text-center ">客戶名稱</th>
                                                    <th nowrap class="text-center ">地址</th>
                                                </tr>
                                                <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-th="選取">
                                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                                                <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                                            </td>
                                                            <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                                            <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                                            <td data-th="地址" class="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_road").ToString())%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <% if (New_List.Items.Count == 0)
                                                    {%>
                                                <tr>
                                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                                </tr>
                                                <% } %>
                                            </table>
                                        </div>

                                    </div>
                                    <div class="row form-inline">
                                        <div class="form-group">
                                            <label><span class="REDWD_b">*</span>地&nbsp; 址</label>
                                            <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control chosen-select _shortwidth" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:TextBox ID="receive_address" CssClass="form-control address" runat="server" MaxLength="70" placeholder="請輸入地址" OnTextChanged="CheckAreaIncorrectly" AutoPostBack="true"></asp:TextBox>
                                            &nbsp;                                
                                            
                                        </div>
                                    </div>
                                    <div class="row form-inline">
                                        <div class="form-group">
                                            <label><span class="REDWD_b">*</span>到著碼</label>
                                            <asp:DropDownList ID="lb_area_arrive_code" runat="server" CssClass="form-control _shortwidth point" OnSelectedIndexChanged="area_arrive_code_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="cbarrive" runat="server" Text="站址自領" AutoPostBack="True" OnCheckedChanged="cbarrive_CheckedChanged" />
                                                <asp:Label ID="is_special_area" runat="server" CssClass="ralign" ForeColor="Red"></asp:Label>
                                            </span>

                                            <%--                                            <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="area_arrive_code" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇到著碼</asp:RequiredFieldValidator>--%>

  
                                            <span id="showalert" name ="showalert" style="color:red;"></span>
                                            <asp:Label ID="Label12" runat="server" Visible="True" ForeColor="red" Text="請勿自行選擇到著碼，系統會自動判讀顯示"></asp:Label>
                                            <asp:Label ID="lbShuttleStationCode" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSpecialAreaId" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSpecialAreaFee" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSendCode" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSendSD" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSendMD" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbReceiveCode" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbReceiveSD" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbReceiveMD" runat="server"  Visible="false" ></asp:Label>

                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="receive_tel1" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="receive_contact" ForeColor="Red" ValidationGroup="validate">請輸入收件人</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="receive_city" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="receive_area" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="receive_address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>3. 才件代收</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="件數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="pieces" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0" OnTextChanged="dl_Pieces_SelectedIndexChanged" AutoPostBack="true"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label4" runat="server" Text="板數" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="plates" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label5" runat="server" Text="才數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="cbm" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>
                                </div>
                                <asp:Label ID="lbErrQuant" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    &nbsp;&nbsp;&nbsp;<span class="REDWD_b">*</span><asp:Label ID="lbProductName" runat="server" Text="產品名稱" CssClass="ralign"></asp:Label><asp:DropDownList ID="dlProductName" runat="server" CssClass="form-control " Width="100px" OnSelectedIndexChanged="dl_ProductName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="reg_size" runat="server" ControlToValidate="dlProductName" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇產品</asp:RequiredFieldValidator>
                                &nbsp;&nbsp;&nbsp;<span class="REDWD_b">*</span><asp:Label ID="lbProductSpec" runat="server" Text="產品規格" CssClass="ralign"></asp:Label><asp:DropDownList ID="dlProductSpec" runat="server" CssClass="form-control " Width="100px"></asp:DropDownList>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="dlProductSpec" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇產品規格</asp:RequiredFieldValidator>
                                    <asp:Label ID="Label13" runat="server" Text="請依據產品名稱 檢查件數是否有誤" Visible="false" style="color:red"></asp:Label>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="＄代收金額" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="collection_money" CssClass="form-control  _collection_money" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6" AutoPostBack=true OnTextChanged="CheckMoneyLimit"></asp:TextBox>&nbsp;&nbsp;
                                <%--<asp:Label ID="Label7" runat="server" Text="＄到付運費(含稅)" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control  _arrive_to_pay_freight" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label8" runat="server" Text="＄到付追加" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="arrive_to_pay_append" CssClass="form-control  _arrive_to_pay_append" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>--%>
                                    <asp:Label ID="Label7" runat="server" Text="＄報值金額" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="Report_fee" CssClass="form-control"  runat="server" onkeyup="this.value = minmax(this.value, 0, 50000)" Width="100px" placeholder="0" MaxLength="5" AutoPostBack=true OnTextChanged="CheckReportLimit"  ></asp:TextBox>
                                    <span class="checkbox checkbox-success">  
                                    <asp:CheckBox ID="chkProductValue" runat="server" Text="保值費" OnCheckedChanged="CheckBox2_CheckedChanged" AutoPostBack=true />
                                </span>
                                ＄<asp:TextBox ID="tbProductValue" CssClass="form-control _product_value" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group" id="individual_fee" runat="server" style="display: none;">
                                            <asp:Label ID="Label9" runat="server" Text="＄貨件運費" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_supplier_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label10" runat="server" Text="＄配送費用" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="i_csection_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label11" runat="server" Text="＄偏遠加價" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_remote_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>*<br>
                            特殊設定</h3>
                        <div class="rowform">
                            <%-- <div class="row form-inline">
                            <div class="form-group">
                                <asp:CheckBox ID="donate_invoice_flag" class="font-weight-400" runat="server" Text="發票捐贈" />
                                &nbsp;&nbsp;
                                <asp:CheckBox ID="electronic_invoice_flag" class="font-weight-400" runat="server" Text="電子發票" />
                                <asp:TextBox ID="uniform_numbers" CssClass="form-control" runat="server" placeholder="統一編號"></asp:TextBox>
                            </div>
                        </div>--%>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label>產品編號</label>
                                    <asp:TextBox ID="ArticleNumber" runat="server" class="form-control"></asp:TextBox>
                                    <label>出貨平台</label>
                                    <asp:TextBox ID="SendPlatform" runat="server" class="form-control"></asp:TextBox>
                                    <label>袋號</label>
                                    <asp:TextBox ID="bagno" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="form-group form-inline  ">
                                    <label>品名</label>
                                    <asp:TextBox ID="ArticleName" runat="server" class="form-control " Width="70%"></asp:TextBox>
                                </div>



                            </div>
                            <div class="row form-inline">
                                <%--<div class="form-group">
                                    <label>商品種類</label>
                                    <asp:DropDownList ID="product_category" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">手機</label>
                                    <asp:TextBox ID="arrive_mobile" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                </div>--%>
                                <%--<div class="form-group">
                                    <label for="inputLastName">特殊配送</label>
                                    <asp:DropDownList ID="special_send" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>--%>
                                <%--<div class="form-group">
                                <label for="inputFirstName">E-mail</label>
                                <asp:TextBox ID="arrive_email" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>--%>

                                <div class="form-group">
                                    <label>指定日</label>
                                    <asp:TextBox ID="arrive_assign_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="holidaydelivery" runat="server" Text="是否為假日配送" /></span>
                                    <asp:Label ID="Label1" runat="server" Text="時段"></asp:Label>
                                    <asp:DropDownList ID="time_period" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <label for="dltemp">配送溫層</label>
                                    <asp:DropDownList ID="dltemp" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label for="inputLastName">備註</label>
                                    <%--<asp:DropDownList ID="invoice_memo" runat="server" CssClass="form-control">                                    
                                </asp:DropDownList>--%>
                                    <%--<asp:TextBox ID="invoice_desc" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                    <textarea id="invoice_desc" runat="server" cols="20" rows="3" maxlength="50" class="form-control"></textarea>
                                </div>
                                <div class="form-group ">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="receipt_flag" runat="server" Text="回單" /></span>
                                    <br />
<%--                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="pallet_recycling_flag" runat="server" Text="棧板回收" onclick="pallet_recycling_flagClick(this)" /></span>
                                    <a id="carsel" href="transport_1_carsel.aspx?Pallet_type=<%=Pallet_type.ClientID %>&Pallet_type_text=<%=Pallet_type_text.ClientID %>" class="fancybox fancybox.iframe "></a>
                                    <div id="Pallet_type_div" runat="server" style="display: none;">
                                        棧板種類<asp:TextBox ID="Pallet_type_text" CssClass="form-control" runat="server" Width="80px" ReadOnly></asp:TextBox>
                                        <asp:HiddenField ID="Pallet_type" runat="server" />


                                    </div>--%>
                                    
                                    <span class="checkbox checkbox-success ">
                                        <asp:CheckBox ID="receipt_round_trip" runat="server" Text="來回件" /></span>
                                    <br /> 
                                    <span class="checkbox checkbox-success form-inline">

                                        <asp:CheckBox ID="WareHouse" runat="server" Text="統倉" /></span>
                                </div>
                                <%--<div class="form-group">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="turn_board" runat="server" Text="翻板" /></span>
                                    ＄<asp:TextBox ID="turn_board_fee" CssClass="form-control _turn_board_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                    <br />
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="upstairs" runat="server" Text="上樓" /></span>
                                    ＄<asp:TextBox ID="upstairs_fee" CssClass="form-control _upstairs_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                    <br />
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="difficult_delivery" runat="server" Text="困配" /></span>
                                    ＄<asp:TextBox ID="difficult_fee" CssClass="form-control _difficult_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>4. 寄件人</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label>&nbsp;客戶代號</label>
                                            <asp:TextBox ID="customer_code" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            <asp:Label ID="Label2" runat="server" Text="寄件人"></asp:Label>
                                            <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="chkshowsend" runat="server" Text="顯示寄件人資料" AutoPostBack="True" Checked="True" OnCheckedChanged="CheckBox1_CheckedChanged" /></span>
                                        </div>
                                        <label>電話/手機</label>
                                        <asp:TextBox ID="send_tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="row form-inline">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label>&nbsp;寄件地址</label>
                                            <asp:DropDownList ID="send_city" runat="server" CssClass="form-control chosen-select _shortwidth" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:DropDownList ID="send_area" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="send_address" CssClass="form-control address" runat="server"></asp:TextBox>
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="chksend_address" class="font-weight-400" runat="server" Text="無地址" /></span>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info" style="height: 100%">

                        <h3>5. 修改紀錄</h3>
                        <div style="overflow: auto; height: 100%;">
                            <asp:TextBox ID="original_collection_money" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_receive_address" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_area_arrive_code" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_pieces" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_receive_contact" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_receive_tel1" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_order_number" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_arrive_assign_date" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_send_contact" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_send_tel" Style="display: none" runat="server"> </asp:TextBox>
                            <asp:TextBox ID="original_send_address" Style="display: none" runat="server"> </asp:TextBox>
                            <table class="table table-striped table-bordered templatemo-user-table  table-margin" style="width: 900px">
                                <thead>
                                    <tr class="tr-only-hide">
                                        <td>修改人</td>
                                        <td>修改欄位</td>
                                        <td>原內容</td>
                                        <td>修改後內容</td>
                                        <td>修改日期</td>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="Delivery_Info_Repeater" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="修改人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.uuser").ToString())%></td>
                                            <td data-th="修改欄位"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.revise_item").ToString())%></td>
                                            <td data-th="原內容"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.original_data").ToString())%></td>
                                            <td data-th="修改後內容"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.new_data").ToString())%></td>
                                            <td data-th="修改日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (Delivery_Info_Repeater.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="12" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </div>

                        <div class="bs-callout bs-callout-info" id="div_cancel" runat="server" visible="false">
                            <h3>銷單資訊</h3>
                            <div class="rowform">
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <label>銷單日期：</label><asp:Literal ID="liCancel_date" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <label>銷單人員：</label><asp:Literal ID="liCancel_psn" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <div class="row form-inline">
                                    <div class="form-group">
                                        <label>銷單原因：</label><asp:Literal ID="liCancel_memo" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                            <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                            <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
                        </div>

                    </div>
                </div>
            </div>
    </form>
    <script>
<%--        function pallet_recycling_flagClick(e) {
            if (e.checked == true) {
                $('a#carsel').trigger('click');
                $("#<%=Pallet_type_div.ClientID%>").attr("style", "display:block;");
            } else {
                $("#<%=Pallet_type_div.ClientID%>").attr("style", "display:none;");
                $("#<%=Pallet_type.ClientID%>").val();
                $("#<%=Pallet_type_text.ClientID%>").val();
            }
        }--%>
    </script>
</body>
</html>
