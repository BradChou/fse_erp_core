﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="assets1_2log.aspx.cs" Inherits="assets1_2log" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">使用單位異動紀錄</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">                                
                                <label>車牌：</label>
                                <asp:Label ID="lbcheck_number" runat="server"  class="form-control"></asp:Label>
                                <label>車行：</label>
                                <asp:Label ID="car_retailer" runat="server"  class="form-control"></asp:Label>
                                <label>車主：</label>
                                <asp:Label ID="owner" runat="server"  class="form-control"></asp:Label>
                                <label>司機：</label>
                                <asp:Label ID="driver" runat="server"  class="form-control"></asp:Label>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>NO</th>
                                <th>時間</th>
                                <th>使用單位</th>
                                <th>原使用單位</th>
                                <th>備註</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" >
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy-MM-dd}").ToString())%></td>
                                        <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NewDept").ToString())%></td>
                                        <td ><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1","code_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dept").ToString()))%></td>
                                        <td ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="5" style="text-align: center">查無使用單位異動紀錄</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
