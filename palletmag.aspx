﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="palletmag.aspx.cs" Inherits="palletmag" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .tb_title, ._d_function {
            text-align: center;
        }

        .tb_detail {
            text-align: left;
        }

        .btn {
            margin: 3px;
        }

        ._h_function {
            width: 10%;
        }

        ._h_addr {
            width: 30%;
        }

        ._h_auto {
            min-width: 5px;
        }

        .div_sh, ._top_left {
            width: 70%;
            float: left;
        }

        .div_btn {
            width: 30%;
            float: right;
            right: 3%;
        }

        .tb_top {
            width: 100%;
        }

        ._top_right {           
            float: right;
            right: 5px;
        }

        ._hr {
            display: inline-table;
        }

        .detail_btn_area {
            text-align: center;
        }
        ._detail{
            width:68%;
            text-align:left;
            margin:10px;
           
        }
        ._detail_title{
            width:10%;   
        }
        ._detail_data{
            width:40%;    
            padding:3px !important;
        }
        ._ext{width:70px !important;}
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
         .table_list tr:hover {
            background-color: lightyellow;
        }
         textarea{
             margin:5px 0px;
         }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
        });
        function Receiver_dw() {
            var rec_cuscode = $(".rec_cuscode").val();
            var rec_code = $(".rec_code").val();
            var rec_name = $(".rec_name").val();
            var the_dw = $("input[name=dw");
            var url_dw = "";
            url_dw = "GetReport.aspx?type=receiver&rec_code=" + rec_code + "&rec_name=" + rec_name + "&rec_cuscode=" + rec_cuscode;

            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            setTimeout("parent.$.fancybox.close()", 2000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">棧板管理</h2>

            <asp:Panel ID="pan_list" runat="server">
                <div class="col-lg-12 form-inline form-group">
                    <table class="tb_top">
                        <tr>
                            <td class="_top_left">查詢條件：
                            <span class="section">1</span>
                                 <label>客　　代：</label>
                                <asp:DropDownList ID="second_code" runat="server" CssClass="form-control rec_cuscode" ></asp:DropDownList>                                
                            </td>
                            <td class="_top_right">
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                <%--<asp:Button ID="btn_New" runat="server" class="btn btn-warning" Text="" />--%>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <hr />
                </div>

                <div><asp:Button ID="Allbtselect" CssClass="btn btn-primary" runat="server" Text="整批歸還" OnClick="Allbtselect_Click" /></div>
                <table id="custom_table" class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr class="tr-only-hide">
                        <th class="tb_title _h_function"><asp:CheckBox ID="chkHeader" runat="server" /> 功　　能</th>
                        <th class="tb_title _h_auto">貨　　號</th>
                        <th class="tb_title _h_auto">到 著 碼</th>
                        <th class="tb_title _h_auto">配送日</th>
                        <th class="tb_title _h_auto">收件人</th>
                        <th class="tb_title _h_auto">區配商代碼</th>
                        <th class="tb_title _h_auto">棧板類型</th>
                        <th class="tb_title _h_auto">板數</th>
                        <th class="tb_title _h_auto">未歸還板數</th>
                        <th class="tb_title _h_auto">歸還記錄</th>                       
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="_d_function" data-th="功能"><asp:CheckBox ID="chkRow" runat="server"  Visible ='<%# Convert.ToInt32(Eval("unreturn_cnt"))>0?true:false %>' />
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                    <asp:Button ID="btselect" CssClass="btn btn-primary _d_btn " CommandName="cmdReturn" CommandArgument='<%# Server.HtmlEncode(Eval("request_id").ToString()) %>' runat="server" Text="歸還"  Visible ='<%# Convert.ToInt32(Eval("unreturn_cnt"))>0?true:false %>' />
                                    <asp:TextBox ID="tbReturnCnt" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%>' Width ="50px"  onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" Visible ='<%# Convert.ToInt32(Eval("unreturn_cnt"))>0?true:false %>'></asp:TextBox>
                                </td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.showname").ToString())%></td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                <td class="tb_detail " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Pallet_type_text").ToString())%></td>
                                <td class="tb_detail " ><asp:Literal ID="Liplates" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%>' ></asp:Literal> </td>
                                <td class="tb_detail " ><asp:Literal ID="Lireturncnt" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.unreturn_cnt").ToString())%>' ></asp:Literal></td>
                                <td><a href="palletlog.aspx?request_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>&check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>" class="fancybox fancybox.iframe " id="logclick">
                                    異動記錄
                                    </a></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="8" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總筆數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
            </asp:Panel>

        </div>
    </div>
</asp:Content>

