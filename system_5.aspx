﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_5.aspx.cs" Inherits="system_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link href="css/build.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('.fancybox').fancybox();
            <%-- $("#<%= rbPsn.ClientID%> ").change(function () {
                $("#<%= rbRole.ClientID%> ").prop('checked',false);
            });

             $("#<%= rbRole.ClientID%> ").change(function () {
                $("#<%= rbPsn.ClientID%> ").prop('checked',false);
            });--%>
        });
        $.fancybox.update();
        $(document).on("click", "._edit", function () {
            $('._search').css('display','none'); 
            $("._save").removeClass('display');
            $(':checkbox').each(function () {
                $(this).prop('disabled', false);
            });
           

            //$("#custom_table").find("tbody tr").each(function () {
            //    //alert('aaa');
            //    var _chkview = $(this).find("._chkview[type='checkbox']");
            //    var _chkadd = $(this).find("._chkadd[type='checkbox']");
            //    var _chkmodify = $(this).find("._chkmodify[type='checkbox']");
            //    var _chkdel = $(this).find("._chkdel[type='checkbox']");

            //    _chkview.disabled = true;
            //    _chkadd.disabled = false;
            //    _chkmodify.removeAttr("disabled");
            //    _chkdel.removeAttr("disabled");
            //});

        });

        $(document).on("click", "._cancel", function () {
            $('._search').css('display', 'block');
            $("._save").addClass('display');
            $(':checkbox').each(function () {
                $(this).prop('disabled', true);
            });
        });
            
        function DisabledChk()
        {
            $(':checkbox').each(function () {
                $(this).prop('disabled', true);
            });
        }

    </script>
    <style>
        input[type="radio"] {
            display: inline-block;
                    }

        input[type="checkbox"] {
            display:inline-block ;
            
        }

        .radio label {
            margin-right: 15px;
            text-align :left ;
        }

        .leftalgin {
            text-align: left ;
        }

        .disabled {
             display: none;
        }

        ._setadd {
            right: 10px;
            text-align: right;
        }
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">權限設定</h2>
            <div class="templatemo-login-form" >
            	<div class="form-group form-inline">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-6 col-md-6 ">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rbPsn" runat="server" Text="個人權限" Checked="true" OnCheckedChanged="radio_CheckedChanged" AutoPostBack="true" CssClass="radio radio-success" style="height: 21px" /></td>
                                            <td>
                                                <label for="account">帳號</label>
                                                <asp:TextBox ID="account" runat="server" class="form-control"></asp:TextBox>
                                                <%-- <asp:Button ID="btnSel" CssClass="templatemo-white-button" runat="server" Text="選擇人員" Font-Size="Small" />--%>
                                                <span id="btnSel" runat="server" class="templatemo-white-button"><a id="accountclick" class="fancybox fancybox.iframe" style="color: #39ADB4; font-weight: 200; font-size: x-small"
                                                    href="AccountSel.aspx?account=<%= account.ClientID%>&user_name=<%=user_name.ClientID %>&job_title=<%=job_title.ClientID%>&customer_shortname=<%=customer_shortname.ClientID%>&manager_type=<%=Hid_manager_type.ClientID%>">選擇人員</a></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <div id="divpsn" runat="server" visible="true" class="templatemo-content-widget">
                                                            <div class="row">
                                                                <label for="inputFirstName">人員：</label>
                                                                <asp:TextBox ID="user_name" runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <div class="row">
                                                                <label for="inputFirstName">職稱：</label>
                                                                <asp:TextBox ID="job_title" runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <div class="row">
                                                                <label for="inputFirstName">公司別：</label>
                                                                <asp:TextBox ID="customer_shortname" runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <asp:HiddenField ID="Hid_manager_type" runat="server" />
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="col-lg-6 col-md-6 ">
                                    <asp:RadioButton ID="rbRole" runat="server" Text="角色權限" AutoPostBack="true" OnCheckedChanged="radio_CheckedChanged" CssClass="radio radio-success" />
                                    <asp:DropDownList ID="dlRole" runat="server" class="form-control">
                                        <asp:ListItem Value="1">峻富管理者</asp:ListItem>
                                        <asp:ListItem Value="2">峻富一般員工</asp:ListItem>
                                        <asp:ListItem Value="3">峻富自營</asp:ListItem>
                                        <asp:ListItem Value="4">區配商</asp:ListItem>
                                        <asp:ListItem Value="5">區配商自營</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Button ID="btn_Search" class="btn btn-primary _search" runat="server" Text="查　　詢" ValidationGroup="validate" OnClick="btn_Search_Click" />
                                    <asp:Button ID="btn_Modify" class="btn btn-warning  _edit" runat="server" Text="編　　輯" OnClick="btn_Modify_Click1" />
                                    <asp:Button ID="btn_Save" class="btn btn-warning _save" runat="server" Text="儲　　存" Visible="false" OnClick="btn_Save_Click" />
                                    <asp:Button ID="btn_Cancel" class=" btn btn-default _cancel" runat="server" Text="取　　消" Visible="false" OnClick="btn_Cancel_Click"   />
                                </div>

                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="account" ForeColor="Red" ValidationGroup="validate">請輸入查詢帳號</asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                </div>
                
                <hr />
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode ="Conditional" >
                    <ContentTemplate>
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>功能代碼</th>
                                <th>功能名稱</th>
                                <th>瀏覽權限</th>
                                <th>新增權限</th>
                                <th>修改權限</th>
                                <th>刪除權限</th>
                              <%--  <th>設定人員</th>
                                <th>設定日期</th>--%>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="功能代碼">
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.auth_id").ToString())%>' />                                            
                                            <asp:HiddenField ID="hid_link" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_link").ToString())%>' />
                                            <asp:HiddenField ID="hid_account_code" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_code").ToString())%>' />
                                            <asp:Literal ID="lifunc" runat="server" Text ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_code").ToString())%>' ></asp:Literal> 
                                        </td>
                                        <td data-th="功能名稱" class="leftalgin">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_name").ToString())%>
                                        </td>
                                        <td data-th="瀏覽權限">
                                            <asp:CheckBox ID="chkView" Checked='<%# Convert.ToBoolean( Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.view_auth").ToString()))%>'
                                                CssClass='<%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_link").ToString()) == "" ? " disabled _chkview" : "_chkview" %>' runat="server" />
                                        </td>
                                        <td data-th="新增權限">
                                            <asp:CheckBox ID="chkAdd" Checked='<%# Convert.ToBoolean( Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.insert_auth").ToString()))%>'
                                                CssClass='<%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_link").ToString()) == "" ? "disabled _chkadd" : "_chkadd" %>' runat="server" />
                                        </td>
                                        <td data-th="修改權限">
                                            <asp:CheckBox ID="chkModify" Checked='<%# Convert.ToBoolean( Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.modify_auth").ToString()))%>'
                                                CssClass='<%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_link").ToString()) == "" ? "disabled _chkmodify" : "_chkmodify" %>' runat="server" />
                                        </td>
                                        <td data-th="刪除權限">
                                            <asp:CheckBox ID="chkDel" Checked='<%# Convert.ToBoolean( Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.delete_auth").ToString()))%>'
                                                CssClass='<%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.func_link").ToString()) == "" ? "disabled _chkdel" : "_chkdel" %>' runat="server" />
                                        </td>
                                       <%-- <td data-th="設定人員">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.uuser").ToString())%>
                                        </td>
                                        <td data-th="設定日期">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy/MM/dd}").ToString())%> 
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="8" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
             

           <%-- <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr>
                        <td>權限功能</td>
                        <td>最高管理權限</td>
                        <td>一般峻富員工</td>
                        <td>區配商</td>
                        <td>客戶</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>客戶新增</td>
                        <td>可新增全公司(不輪區配商)客戶</td>
                        <td>可新增峻富總公司客戶</td>
                        <td>可新增該區配商客戶</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>客戶基本資料查詢</td>
                        <td>可查詢全公司客戶</td>
                        <td>可查詢全公司客戶</td>
                        <td>可查詢該區配商客戶</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>客戶帳務資料查詢</td>
                        <td>可查詢全公司客戶</td>
                        <td>可查詢峻富總公司客戶</td>
                        <td>可查詢該區配商客戶</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>客戶基本資料修改</td>
                        <td>可修改全公司客戶</td>
                        <td>可修改峻富總公司客戶</td>
                        <td>可查詢該區配商客戶</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>客戶分公司新增</td>
                        <td>可新增全公司客戶</td>
                        <td>可新增峻富總公司客戶</td>
                        <td>可新增該區配商客戶</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>客戶計價模式新增</td>
                        <td>可新增全公司客戶</td>
                        <td>可新增峻富總公司客戶</td>
                        <td>可新增該區配商客戶</td>
                        <td>X</td>
                    </tr>
                </tbody>
            </table>
            <h2 class="margin-bottom-10">帳務資料管理權限</h2>
            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr>
                        <td>權限功能</td>
                        <td>最高管理權限</td>
                        <td>一般峻富員工</td>
                        <td>區配商</td>
                        <td>峻富自營客</td>
                        <td>區配自營客</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>HCT系統運費資料</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>區配運費資料</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>可查詢</td>
                        <td>X</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>峻富自營客運費資料</td>
                        <td>可查詢/修改</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>可查詢</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>區配自營客運費資料</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>可查詢</td>
                    </tr>
                    <tr>
                        <td>C段運費資料</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>可查詢</td>
                        <td>X</td>
                        <td>X</td>
                    </tr>
                    <tr>
                        <td>HCT系統運費資料</td>
                        <td>可查詢/修改</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                        <td>X</td>
                    </tr>
                </tbody>
            </table>--%>
            </div>
        </div>
    </div>
</asp:Content>

