﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        account.Text = Application["Account"].ToString();
    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        cmd.Parameters.AddWithValue("@user_email", email.Text.ToString());
        cmd.CommandText = @"select a.account_code, a.customer_code, a.user_name, a.user_email, c.shipments_email from tbAccounts a left join tbCustomers c on a.customer_code = c.customer_code
                    where a.account_code = @account_code and (user_email = @user_email or shipments_email = @user_email)";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string randPwd = "";
                do
                {
                    randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                } while (!System.Text.RegularExpressions.Regex.IsMatch(randPwd,
                                                                   "\\d+[A-F]+\\d+[A-F]"));
                //具有兩個以上的英文字母，並且中間夾數字才算合格
                //將第二個英文字母變小寫
                int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
                randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

                MD5 md5 = MD5.Create();
                string result = Utility.GetMd5Hash(md5, randPwd);
                
                var httpClient = new HttpClient() { BaseAddress = new Uri(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"]) };
                var parameters = new Dictionary<string, string> { { "UsernameAccountCode", account.Text.ToString().Trim() }, { "Password", randPwd } };
                string jsonStr = JsonConvert.SerializeObject(parameters);
                HttpContent contentPost = new StringContent(jsonStr, Encoding.UTF8, "application/json");
                try
                {
                    //Post http callas.  
                    HttpResponseMessage response = httpClient.PostAsync(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"], contentPost).Result;
                    //nesekmes atveju error..
                    response.EnsureSuccessStatusCode();
                    //responsas to string
                    string responseBody = response.Content.ReadAsStringAsync().Result;

                    if (responseBody != "null")
                    {
                        Console.WriteLine("True");
                    }
                    else
                    {
                        Console.WriteLine("False");
                    }
                }
                catch (HttpRequestException a)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message :{0} ", a.Message);

                }


                // 判斷是全速配還是俊富客戶
                if (dt.Rows[0]["customer_code"].ToString().StartsWith("F"))
                {
                    string body = dt.Rows[0]["user_name"] + @" 您好，<br>請使用此登入連結 http://supererp.fsexpress.com.tw/  與新密碼 " + randPwd +
                                        @"<br>登入全速配雲端物流系統 我們將帶給您更穩定可靠的物流管理品質
                                        <br>新密碼： " + randPwd + " <br>" +
                                        "登入連結 http://supererp.fsexpress.com.tw/" +
                                        "<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";
                    Utility.Mail_Send(ConfigurationManager.AppSettings["FSEMailSender"], new[] { email.Text.ToString() }, null, "全速配雲端物流管理系統密碼通知信", body, true, null);
                }
                else
                {
                    string body = dt.Rows[0]["user_name"] + @" 您好，<br>請使用此登入連結 http://supererp.junfu.asia/  與新密碼 " + randPwd +
                                        @" 登入峻富物流<br> 我們將帶給您更穩定可靠的物流管理品質
                                        <br>新密碼： " + randPwd + " <br>" +
                                        "登入連結 http://supererp.junfu.asia/" +
                                        "<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";
                    Utility.Mail_Send(ConfigurationManager.AppSettings["MailSender"], new[] { email.Text.ToString() }, null, "峻富雲端物流管理系統密碼通知信", body, true, null);
                }

                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@password", result);
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_code", account.Text.ToString().Trim());
                cmdupd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmdupd);
                dbAdapter.execNonQuery(cmdupd);

                SqlCommand cmdPWChanged = new SqlCommand();
                cmdPWChanged.Parameters.AddWithValue("@account", account.Text.ToString().Trim());
                cmdPWChanged.CommandText = "update tbAccounts set password_changed = 1 where account_code = @account";
                dbAdapter.execNonQuery(cmdPWChanged);

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "message", "<script>location.href='login.aspx';alert('密碼通知信已寄至信箱，請前往查收');</script>", false);
                return;

            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('您的帳號和信箱不符，請重新輸入，如有問題，請聯繫系統管理員');</script>", false);
            return;
        }
    }
}