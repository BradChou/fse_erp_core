﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class CancelDialog : System.Web.UI.Page
{
    public string request_id
    {
        get { return ViewState["request_id"].ToString(); }
        set { ViewState["request_id"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((Request.QueryString["id"] != null) && (Request.QueryString["id"] != ""))
            {
                request_id = Request.QueryString["id"].ToString();
            }

        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        //判斷是否可以銷單：
        SqlCommand cmd3 = new SqlCommand();
        cmd3.CommandText = "select check_number,return_check_number, latest_scan_item,cancel_date from tcDeliveryRequests where request_id = @request_id";
        cmd3.Parameters.AddWithValue("@request_id", request_id);
        DataTable dt;
        dt = dbAdapter.getDataTable(cmd3);

        if (dt.Rows[0]["latest_scan_item"].ToString() == "7" || dt.Rows[0]["latest_scan_item"].ToString() == "1" || dt.Rows[0]["latest_scan_item"].ToString() == "2" || dt.Rows[0]["latest_scan_item"].ToString() == "3" || dt.Rows[0]["latest_scan_item"].ToString() == "4")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此筆已有貨態無法銷單');</script>", false);
            return;
        }

        if (!string.IsNullOrEmpty(dt.Rows[0]["cancel_date"].ToString()))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此筆已經銷單');</script>", false);
            return;
        }

        //逆物流抓原貨號及司機
        string origin_check_number = dt.Rows[0]["return_check_number"].ToString();
        string refused_check_number= dt.Rows[0]["check_number"].ToString();

        SqlCommand cmd5 = new SqlCommand();
        cmd5.CommandText = "select check_number,latest_scan_driver_code, latest_scan_item,cancel_date from tcDeliveryRequests where check_number = @check_number";
        cmd5.Parameters.AddWithValue("@check_number", origin_check_number);
        DataTable dt2;
        dt2 = dbAdapter.getDataTable(cmd5);

              
        string origin_driver_code = dt2.Rows[0]["latest_scan_driver_code"].ToString();

        string strErr = string.Empty;
        #region 
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "usp_DeliveryRequesRecord";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@request_id", request_id);
        cmd.Parameters.AddWithValue("@action", "D");
        cmd.Parameters.AddWithValue("@delete_date", DateTime.Today);
        cmd.Parameters.AddWithValue("@delete_user", Session["account_code"].ToString());
        cmd.Parameters.AddWithValue("@memo", delete_memo.Text);
        try
        {
            dbAdapter.execNonQuery(cmd);
        }
        catch (Exception ex)
        {

            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "銷單:" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }


        #region 回寫到託運單的欄位
        if (request_id != "")
        {
            SqlCommand cmd2 = new SqlCommand();
            cmd2.Parameters.AddWithValue("@cancel_date", DateTime.Today);
            cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
            cmd2.Parameters.AddWithValue("@return_check_number", DBNull.Value);
            cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd2);
            }
            catch (Exception ex)
            {

                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "銷單:" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");
            }

            SqlCommand cmdd = new SqlCommand();
            DataTable dtRequests = new DataTable();
            cmdd.CommandText = @"insert into ttDeliveryScanLog
(driver_code,
check_number,
scan_item,
scan_date,
pieces,
arrive_option,
cdate,
car_number
)
values
(@driver_code,
@check_number,
@scan_item,
@scan_date,
@pieces,
@arrive_option,
@cdate,
@car_number)";
            cmdd.Parameters.AddWithValue("@driver_code", origin_driver_code);
            cmdd.Parameters.AddWithValue("@check_number", origin_check_number);
            cmdd.Parameters.AddWithValue("@scan_item", "3");
            cmdd.Parameters.AddWithValue("@scan_date", DateTime.Now);
            cmdd.Parameters.AddWithValue("@pieces", "1");
            cmdd.Parameters.AddWithValue("@arrive_option", "50");
            cmdd.Parameters.AddWithValue("@cdate", DateTime.Now);
            cmdd.Parameters.AddWithValue("@car_number", refused_check_number);     //紀錄當下取消的拒收單號
            cmdd.CommandText = dbAdapter.genInsertComm("ttDeliveryScanLog", false, cmdd);
            dbAdapter.execNonQuery(cmdd);



            #region 預購袋超值箱逆物流銷單不減數量

            using (SqlCommand cmda = new SqlCommand())
            {
                DataTable dta = new DataTable();
                cmda.Parameters.AddWithValue("@request_id", request_id);
                cmda.CommandText = @"select C.product_type , D.check_number from tbCustomers  C left join tcDeliveryRequests D with(nolock) on C.customer_code = D.customer_code 
                where request_id = @request_id";
                dta = dbAdapter.getDataTable(cmda);

                string prefix_check_number = dta.Rows[0]["check_number"].ToString().Substring(0, 3);

                if (prefix_check_number == "990" & (dta.Rows[0]["product_type"].ToString() == "2" || dta.Rows[0]["product_type"].ToString() == "3"))
                {

                }
                else if (prefix_check_number == "980" & (dta.Rows[0]["product_type"].ToString() == "2" || dta.Rows[0]["product_type"].ToString() == "3"))
                {

                }
                else
                {

                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Parameters.AddWithValue("@request_id", request_id);
                    sqlCommand.CommandText = @"update Customer_checknumber_setting set used_count = used_count-1 where customer_code in (select b.customer_code 
                                        from tcDeliveryRequests b with(nolock) 
                                        where b.request_id = @request_id)";

                    dbAdapter.execNonQuery(sqlCommand);
                }
                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandText = "select product_type,r1.customer_code,check_number from tcDeliveryRequests r1 left join tbcustomers c1 on  c1.customer_code=r1.customer_code where request_id = @request_id";
                cmd4.Parameters.AddWithValue("@request_id", request_id);

                DataTable dt4;
                dt4 = dbAdapter.getDataTable(cmd4);

                string customer_code = dt4.Rows[0]["customer_code"].ToString();
                string check_number = dt4.Rows[0]["check_number"].ToString();

                using (SqlCommand sql = new SqlCommand())
                {
                    DataTable dataTable = new DataTable();
                    sql.Parameters.AddWithValue("@customer_code", customer_code);
                    sql.CommandText = @"
                       declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock) group by customer_code
                       
                       select A.is_active, A.total_count - A.used_count as rest_count from Customer_checknumber_setting A With(Nolock)
                       join @newest t on A.customer_code = t.customer_code and A.update_date = t.update_date
                       where A.customer_code = @customer_code";

                    dataTable = dbAdapter.getDataTable(sql);
                    if (dataTable.Rows.Count > 0)
                    {
                        var active = dataTable.Rows[0]["is_active"].ToString();
                        long remain_count;
                        if (active == "False")
                        { remain_count = 0; }
                        else
                        { remain_count = Convert.ToInt64(dataTable.Rows[0]["rest_count"].ToString()); }


                        if (dt4.Rows[0]["product_type"].ToString() == "2" || dt4.Rows[0]["product_type"].ToString() == "3")
                        {

                            using (SqlCommand cmd1 = new SqlCommand())
                            {
                                cmd1.Parameters.AddWithValue("@customer_code", customer_code);
                                cmd1.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                cmd1.Parameters.AddWithValue("@pieces_count", 1);
                                cmd1.Parameters.AddWithValue("@function_flag", "D2");
                                cmd1.Parameters.AddWithValue("@adjustment_reason", "單筆銷單");
                                cmd1.Parameters.AddWithValue("@check_number", check_number);
                                cmd1.Parameters.AddWithValue("@remain_count", remain_count);
                                cmd1.CommandText = dbAdapter.genInsertComm("checknumber_record_for_bags_and_boxes", false, cmd1);

                                dbAdapter.execNonQuery(cmd1);
                            }
                        }
                        #endregion
                    }
                }
            }
        }
        #endregion

        if (strErr == "")
        {
            retval.Value = "OK";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('銷單成功'); parent.$.fancybox.close();parent.location.reload(true);</script>", false);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('銷單失敗:" + strErr + "');</script>", false);
        }
        return;
    }
    #endregion

}