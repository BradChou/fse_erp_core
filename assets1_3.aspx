﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_3.aspx.cs" Inherits="assets1_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {      
            $('.fancybox').fancybox();
            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
            
            $('.share').on('keyup', function () {
                var sum = 0;
                var _parent = $(this).parents("tr");
                _parent.find('td input.share').each(function (i) {
                    var share = $(this).val();
                    //alert(share);
                    if ($.isNumeric(share)) {
                        sum += parseInt(share);
                    }
                });
                var tb_tot = _parent.find(".tot");
                tb_tot.val(sum);
                if (sum != 100) {
                    tb_tot.css("color", "red");
                    $("#<%=btnSave.ClientID%>").prop('disabled', true);
                }
                else {
                    tb_tot.css("color", "black");
                    $("#<%=btnSave.ClientID%>").prop('disabled', false);
                }          
            });
                        

            $(document).on("click", "#<%=btnEdit.ClientID%>", function () {
                $("#<%= btnsearch.ClientID%>").css('display', 'none');
                //$("#<%//= btnLog.ClientID%>").css('display', 'none');                
                $("#<%=btnEdit.ClientID%>").css('display', 'none');
                $("#<%= btnSave.ClientID%>").css('display', 'inline-block');
                $("#btn_Cancel").css('display', 'inline-block');
                $("#<%=Panel1.ClientID %> input.share").prop('disabled', false);
                $("#<%=ddl_dept.ClientID %>").prop('disabled', true);  
                $("#<%=car_license.ClientID %>").prop('disabled', true); 
                $('.share').keyup(); 
            });

            $(document).on("click", "#btn_Cancel", function () {
                $("#<%= btnsearch.ClientID%>").css('display', 'inline-block');
                //$("#<%//= btnLog.ClientID%>").css('display', 'unset');                
                $("#<%=btnEdit.ClientID%>").css('display', 'inline-block');
                $("#<%= btnSave.ClientID%>").css('display', 'none');
                $("#btn_Cancel").css('display', 'none');
                $("#<%=Panel1.ClientID %> input.share").prop('disabled', true);
                $("#<%=ddl_dept.ClientID %>").prop('disabled', false); 
                $("#<%=car_license.ClientID %>").prop('disabled', false); 
                
            });



        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

    </script>

    <style>
        ._th {
            text-align: center;           
        }
        ._textsmall {
             font-size :small ;
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">每月帳工維護</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline list-inline ">
                    <label>車輛使用單位：</label>
                    <asp:DropDownList ID="ddl_dept" runat="server" class="form-control "></asp:DropDownList>
                    <label>車牌：</label>
                    <asp:TextBox ID="car_license" runat="server" class="form-control " ></asp:TextBox>
                    <asp:Button ID="btnsearch" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <span><a id="btnEdit" runat="server" class="btn btn-primary" style="display:none;">每月修改</a></span>
                    <asp:Button ID="btnSave" CssClass="btn btn-warning" runat="server" Text="儲存" Style="display: none" OnClick="btnSave_Click" Enabled="False" />
                    <span><a id="btn_Cancel" class="btn btn-warning " style="display: none">取消</a></span>
                    <%--<asp:Button ID="btnLog" CssClass="btn btn-primary" runat="server" Text="各部門修改紀錄" />--%>
                </div>
                <p class=" text-danger">※備註：每月開放修改時間：21日0時0分起，至25日23時59分</p>
                <p class="text-primary">※點選異動日期可查詢異動紀錄</p>
                <asp:Panel ID="Panel1" runat="server"  >
                    <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide" >
                        <th class="_th" rowspan="2">部門</th>
                        <th class="_th" rowspan="2">車牌</th>
                        <th class="_th" rowspan="2">噸數</th>
                        <th class="_th" rowspan="2">使用司機</th>
                        <th class="_th" colspan="15">帳務部門拆分</th>
                        <th class="_th" rowspan="2">前次異動日期</th>
                        <th class="_th" rowspan="2">前次異動人員</th>
                    </tr>
                    <tr>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept1" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept2" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept3" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept4" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept5" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept6" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept7" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept8" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept9" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept10" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept11" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept12" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept13" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall"><asp:Label ID="lbaccdept14" runat="server" ></asp:Label></th>
                        <th class="_th _textsmall">總計</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server"   >
                        <ItemTemplate>
                            <tr class ="paginate">                               
                                <td data-th="部門"><%#Func.GetRow("code_name","tbItemCodes","","code_id = @code_id and code_sclass='dept' and code_bclass='6'","code_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dept").ToString()))%></td> 
                                <td data-th="車牌"><asp:Literal ID="Licar_license" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%>'></asp:Literal></td>
                                <td data-th="噸數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                <td data-th="使用司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td ><asp:TextBox ID="tb_1" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=1 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_2" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=2 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_3" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=3 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_4" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=4 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_5" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=5 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_6" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=6 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_7" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=7 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_8" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=8 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_9" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=9 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_10" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=10 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_11" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=11 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_12" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=12 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_13" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=13 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox ID="tb_14" class="share" disabled runat="server" Width="70px" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Text='<%#Func.GetRow("share","tbAssetsAccountShare",""," account_dept=14 and a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox></td>
                                <td ><asp:TextBox disabled ID="tb_tot" class="tot" runat="server" Width="70px"  Text='<%#Func.GetRowSum("sum(share)","tbAssetsAccountShare","","a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>'></asp:TextBox>
                                </td>
                                <td data-th="前次異動日期">
                                    <a href="assets1_3log.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>&check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%>" class="fancybox fancybox.iframe " id="logclick">
                                    <%#Func.GetRow("udate","tbAssetsAccountShare","","a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>
                                    </a>
                                </td>                                
                                <td data-th="前次異動人員"><%#Func.GetRow("uuser","tbAssetsAccountShare","","a_id = @a_id","a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString()))%>
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString())%>' />
                                    <asp:HiddenField ID="hid_a_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="21" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                </asp:Panel>
                    
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
                   
                
            </div>
        </div>
    </div>
</asp:Content>

