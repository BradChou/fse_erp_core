﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.ServiceModel.Channels;
using System.Windows.Forms;

public partial class LT_trace_1_3 : System.Web.UI.Page
{

    public int id
    {
        get { return (int)ViewState["id"]; }
        set { ViewState["id"] = value; }
    }





    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            Less.Text = Less_than_truckload;
           

            

               

                

          
            readdata();
        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();


        cmd.Parameters.Clear();

        

        if (Less_than_truckload == "0"){}
        

        cmd.CommandText = string.Format(@"select * from condition_category");

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }


    //確認
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        Boolean IsMod = id > 0 ? true : false;


        if (string.IsNullOrEmpty(category.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入類別名稱!');</script>", false);
            return;
        }


        using (SqlCommand cmd2 = new SqlCommand())
        {
            string strSQL = @"select * from condition_category where level= @level and category= @category and attribution= @attribution ";
            if (IsMod) strSQL += "AND id NOT IN('" + id.ToString() + "')";
            cmd2.CommandText = strSQL;
            cmd2.Parameters.AddWithValue("@level", dllevel.SelectedValue);
            cmd2.Parameters.AddWithValue("@category", category.Text.Trim());
            cmd2.Parameters.AddWithValue("@attribution", dlattribution.SelectedValue);

            using (DataTable dt = dbAdapter.getDataTable(cmd2))
            {
                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此資料已存在，請重新確認!');</script>", false);
                    return;
                }
            }
        }

        pan_detail.Visible = false;
        if (IsMod)
        {
            #region 修改        
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@level", dllevel.SelectedItem.Value);  //層級
            cmd.Parameters.AddWithValue("@category", category.Text.Trim()); //類別
            cmd.Parameters.AddWithValue("@attribution", dlattribution.SelectedItem.Value); //歸屬

            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間             
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
            cmd.CommandText = dbAdapter.genUpdateComm("condition_category", cmd);   //修改
            dbAdapter.execNonQuery(cmd);
            #endregion

            pan_list.Visible = true;

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='LT_trace_1_3.aspx';alert('修改完成');</script>", false);
            return;
        }
        else
        {





            #region 新增        
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@level", dllevel.SelectedValue);         //層級
            cmd.Parameters.AddWithValue("@category", category.Text.Trim());        //類別名稱
            cmd.Parameters.AddWithValue("@attribution", dlattribution.SelectedValue);        //歸屬
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                             //更新時間

            cmd.CommandText = dbAdapter.SQLdosomething("condition_category", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            #endregion


            pan_list.Visible = true;



            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='LT_trace_1_3.aspx';alert('新增完成');</script>", false);
            return;
        }
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "cmdSelect":
                dllevel.Enabled = true;
                category.Enabled = true;
                dlattribution.Enabled = true;
                pan_list.Visible = false;
                if (dllevel.SelectedValue != "2") 
                {
                    dlattribution.Visible = false;
                    lbattribution.Visible = false;
                }
                else
                {
                    dlattribution.Visible = true;
                    lbattribution.Visible = true;
                }
                string wherestr = "";
                SqlCommand cmd = new SqlCommand();

                cmd.Parameters.AddWithValue("@id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                wherestr += " AND id=@id";

                cmd.CommandText = string.Format(@"select * from condition_category                                                
                                              WHERE 0=0  {0} ", wherestr);

                using (SqlCommand cmd3 = new SqlCommand())
                {
                    cmd3.CommandText = @"select category from condition_category where level = '1' ";
                    dlattribution.DataSource = dbAdapter.getDataTable(cmd3);
                    dlattribution.DataValueField = "category";
                    dlattribution.DataTextField = "category";
                    dlattribution.DataBind();
                    dlattribution.Items.Insert(0, new ListItem("請選擇", ""));
                }

                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT.Rows.Count > 0)
                    {
                        id = Convert.ToInt32(DT.Rows[0]["id"]);

                        //btnAdd.Text = "修改";
                        if (dllevel.Items.FindByValue(DT.Rows[0]["level"].ToString().Trim()) == null)
                        {
                            dllevel.Items.Insert(0, new ListItem(DT.Rows[0]["level"].ToString().Trim(), DT.Rows[0]["level"].ToString().Trim()));
                        }
                        else
                        {
                            dllevel.SelectedValue = DT.Rows[0]["level"].ToString().Trim();
                        }
                        category.Text = DT.Rows[0]["category"].ToString().Trim();
                        
                       
                        if (dlattribution.Items.FindByValue(DT.Rows[0]["attribution"].ToString().Trim()) == null)
                        {
                            dlattribution.Items.Insert(0, new ListItem(DT.Rows[0]["attribution"].ToString().Trim(), DT.Rows[0]["attribution"].ToString().Trim()));
                        }
                        else
                        {
                            dlattribution.SelectedValue = DT.Rows[0]["attribution"].ToString().Trim();
                        }


                    }
                }
                pan_detail.Visible = true;
                
                break;
            case "cmddelete":

                using (SqlCommand cmddel = new SqlCommand())
                {
                    cmddel.Parameters.Add("@where_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                    cmddel.CommandText = dbAdapter.genDeleteComm("condition_category", cmddel);
                    dbAdapter.execNonQuery(cmddel);
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='LT_trace_1_3.aspx';alert('刪除完成');</script>", false);
                return;
                break;

        }
    }


    protected void search_Click(object sender, EventArgs e)
    {
  
    }

    

   

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void level_SelectedIndexChanged(object sender, EventArgs e)
    {

        //if (dllevel.SelectedValue == "1" || dllevel.SelectedValue == "3")
        //{
        //    dlattribution.Enabled = true;
        //}

        if (dllevel.SelectedItem.Text.ToString() != "2")
        {
            lbattribution.Visible = false;
            dlattribution.Visible = false;

        }
        else {
            lbattribution.Visible = true;
            dlattribution.Visible = true; 
        }

        }


    //新增
    protected void btn_New_Click(object sender, EventArgs e)
    {
        dllevel.Enabled = true;
        pan_list.Visible = false;
        id = -1;
        category.Enabled = true;
        dllevel.SelectedValue = "";        
        pan_detail.Visible = true;

        category.Text = "";


        using (SqlCommand cmd3 = new SqlCommand())
        {
            cmd3.CommandText = @"select category from condition_category where level = '1' ";
            dlattribution.DataSource = dbAdapter.getDataTable(cmd3);
            dlattribution.DataValueField = "category";
            dlattribution.DataTextField = "category";
            dlattribution.DataBind();
            dlattribution.Items.Insert(0, new ListItem("請選擇", ""));
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        pan_list.Visible = true;
        id = -1;
        pan_detail.Visible = false;
    }
}