﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="assets1_3log.aspx.cs" Inherits="assets1_3log" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">每月帳工維護異動紀錄</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">                                
                                <label>車牌：</label>
                                <asp:Label ID="lbcheck_number" runat="server"  class="form-control"></asp:Label>
                                <label>部門：</label>
                                <asp:Label ID="DeptName" runat="server"  class="form-control"></asp:Label>
                                <label>噸數：</label>
                                <asp:Label ID="Tonnes" runat="server"  class="form-control"></asp:Label>
                                <label>司機：</label>
                                <asp:Label ID="driver" runat="server"  class="form-control"></asp:Label>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>NO</th>
                                <th>棧板</th>
                                <th>專車</th>
                                <th>全拖</th>
                                <th>超商_不分區</th>
                                <th>超商_日配</th>
                                <th>超商_夜配</th>
                                <th>超商_北區</th>
                                <th>超商_桃竹區</th>
                                <th>異動日期</th>
                                <th>異動人員</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" >
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=1 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=2 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=3 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=4 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=5 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=6 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=7 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                        <td ><%#Func.GetRow("share","tbAssetsAccountShareChange",""," account_dept=8 and CONVERT(varchar(100),udate,120) = @udate and a_id=@a_id","udate|a_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString()+"|"+Request.QueryString["a_id"].ToString()))%></td>
                                    <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.uuser").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="9" style="text-align: center">查無每月帳工維護異動紀錄</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
