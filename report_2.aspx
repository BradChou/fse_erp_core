﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_2.aspx.cs" Inherits="report_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">C段配送明細表</h2>
            <div class="templatemo-login-form" >
                <!---->
                <div class="form-group form-inline">

                    <%--<label>配送期間：</label>--%>
                    <label>發送期間：</label>
                    <asp:TextBox ID="dates" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    ~
                    <asp:TextBox ID="datee" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    <label>配送廠商：</label>
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control"></asp:DropDownList>
                    <asp:DropDownList ID="rb_pricing_type" runat="server" CssClass="form-control">
                        <asp:ListItem>論板</asp:ListItem>
                        <asp:ListItem>專車</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnQry" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="btnQry_Click" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="匯　出" OnClick="btExport_Click" />

                </div>
            </div>
             <div style="overflow: auto; height: 550px; width: 100%">
                <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">發送日期</th>
                    <th style="white-space: nowrap;">配送日期</th>
                    <th style="white-space: nowrap;">明細貨號</th>
                    <th style="white-space: nowrap;">訂單號碼</th>
                    <th style="white-space: nowrap;">發送站</th>
                    <th style="white-space: nowrap;">寄件人</th>
                    <th style="white-space: nowrap;">收件人</th>
                    <th style="white-space: nowrap;">收貨人電話號碼</th>
                    <th style="white-space: nowrap;">配送縣市別</th>
                    <th style="white-space: nowrap;">配送區域</th>
                    <th style="white-space: nowrap;">件數</th>
                    <th style="white-space: nowrap;">棧板數</th>    
                    <th style="white-space: nowrap;">收件人地址</th>
                    <th style="white-space: nowrap;">配送商代碼</th>
                    <th style="white-space: nowrap;">配送廠商</th>
                    <th style="white-space: nowrap;">特殊配送說明</th>
                    <th style="white-space: nowrap;">回單張數</th> 
                    <th style="white-space: nowrap;">棧板回收</th> 
                    <th style="white-space: nowrap;">翻板</th>   
                    <th style="white-space: nowrap;">上樓</th>   
                    <th style="white-space: nowrap;">困配</th>   
                    <th style="white-space: nowrap;">配送費用</th>
                    <th style="white-space: nowrap;">計價模式</th>
                    <th style="white-space: nowrap;">偏遠區</th>
                    <th style="white-space: nowrap;">到付追加</th>
                    <th style="white-space: nowrap;">到付運費</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">總費用</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server"  >
                    <ItemTemplate>
                        <tr class ="paginate">
                            <td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;"  data-th="發送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;"  data-th="配送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="明細貨號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="訂單號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收貨人電話號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收貨人電話號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送縣市別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送縣市別").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送區域").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="棧板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.棧板數").ToString())%></td>
                            <td class="_left" style="min-width :200px"" data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人地址").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送商代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送商代碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送廠商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送廠商").ToString())%></td>
                            <td class="_left" style="min-width :200px" data-th="特殊配送說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.特殊配送說明").ToString())%></td>                            
                            <td style="white-space: nowrap;" data-th="回單張數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.是否回單").ToString())%></td>                            
                            <td style="white-space: nowrap;" data-th="棧板回收"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.棧板回收").ToString())%></td>    
                            <td style="white-space: nowrap;" data-th="翻板"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.翻板").ToString())%></td>    
                            <td style="white-space: nowrap;" data-th="上樓"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.上樓").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="困配"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.困配").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送費用").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.計價模式").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="偏遠區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.偏遠區").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付追加"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付追加").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付運費").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="總費用"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.總費用").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="37" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            </div>
            
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
            <div id="page-nav" class="page"></div>
        </div>
    </div>
</asp:Content>

