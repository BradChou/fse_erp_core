﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="loan4_1.aspx.cs" Inherits="loan4_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

            $(".F_MENU ul").hide();
            $(".F_MENU>.F_BTN").click(function () {
                $(".F_MENU ul").hide();
                $(this).parent().find("ul").show();
                return false;
            });

            $(".close_BTN").click(function () {
                $(".F_MENU ul").hide();
                return false;
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
    </script>

    <style>
        ._th {
            text-align: center;
        }
        .auto-style1 {
            text-align: center;
            width: 149px;
        }
        .auto-style2 {
            width: 149px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">貸款管理</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label>車牌：</label>
                    <asp:TextBox ID="car_license" runat="server" class="form-control " ></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="車貸報表" OnClick="btExport_Click"   />
                </div>
                <div class="form-group" style="float: right;">
                    <a href="loan4_1_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>
                
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th"></th>
                        <th class="_th">車牌</th>
                        <th class="_th">車價</th>
                        <th class="_th">貸款金額</th>
                        <th class="_th">年利率</th>
                        <th class="_th">頭款</th>
                        <th class="_th">貸款年限</th>
                        <th class="_th">貸款起始日</th>
                        <th class="auto-style1">分期付款金額</th>
                        <th class="_th">分期付款次數</th>
                        <th class="_th">實際付款次數</th>
                        <th class="_th">利息總額</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                        <th></th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand" >
                        <ItemTemplate>
                            <tr class ="paginate">
                                <td>
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                    <%--<asp:Button ID="btn_print" CssClass="btn btn-info" CommandName="Print" CommandArgument='<%# Server.HtmlEncode(Eval("id").ToString()) %>' runat="server" Text="車款分期償還計畫表" />--%>
                                    <a href="loan4_1_edit.aspx?id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>" class="btn btn-success   " id="updclick">修改</a>
                                </td>
                                <td data-th="車牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                <td data-th="車價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_price").ToString())%></td>
                                <td data-th="貸款金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.loan_price").ToString())%></td>
                                <td data-th="年利率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.annual_rate").ToString())%> </td>
                                <td data-th="頭款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.first_payment").ToString())%></td>
                                <td data-th="貸款年限"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.loan_period").ToString())%></td>
                                <td data-th="貸款起始日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.loan_startdate","{0:yyyy-MM-dd}").ToString())%></td>
                                <td data-th="分期付款金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.installment_price").ToString())%></td>
                                <td data-th="分期付款次數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.installment_count").ToString())%></td>
                                <td data-th="實際付款次數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.paid_count").ToString())%></td>
                                <td data-th="利息總額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.interest").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td>
                                    <asp:UpdatePanel ID="upF_MENU" runat="server" UpdateMode="Conditional" class="F_MENU">
                                        <ContentTemplate>
                                            <span class="F_BTN"><a class="btn btn-primary F_BTN_a">車款分期償還計畫表</a>
                                                <ul class="menu_list2">
                                                    <li>
                                                        <asp:Button ID="btnPrint1" runat="server" Text="公司" CommandArgument='<%# Server.HtmlEncode(Eval("id").ToString()) %>'
                                                            CommandName="Print1" UseSubmitBehavior="false" CssClass="btn btn-block" />
                                                    </li>
                                                    <li>
                                                        <asp:Button ID="btnPrint2" runat="server" Text="區配&外車" CommandArgument='<%# Server.HtmlEncode(Eval("id").ToString()) %>'
                                                            UseSubmitBehavior="false" CssClass="btn btn-block" CommandName="Print2" /></li>
                                                    <%--<li>
                                                        <asp:Button ID="btnPrint3" runat="server" Text="總表" CommandArgument='<%# Server.HtmlEncode(Eval("id").ToString()) %>'
                                                            UseSubmitBehavior="false" CssClass="btn btn-block" CommandName="Print3" /></li>--%>
                                                    <a class="btn btn-block btn-default close_BTN"><i class="fa fa-close"></i></a>
                                                </ul>
                                            </span>
                                            
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnPrint1" />
                                            <asp:PostBackTrigger ControlID="btnPrint2" />
                                            <%--<asp:PostBackTrigger ControlID="btnPrint3" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="12" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

