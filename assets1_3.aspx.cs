﻿using ClosedXML.Excel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_3 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }
    }

    public SortedList AccDepts
    {
        get { return (SortedList)ViewState["AccDepts"]; }
        set { ViewState["AccDepts"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 使用單位
            SqlCommand objCommand = new SqlCommand();
            string wherestr = "";
            var Str = Func.GetRow("Str", "ttPersonCorrespond", "", "Account_Code=@Account_Code and Kinds=1", "Account_Code", Session["account_code"].ToString());
            if (!string.IsNullOrEmpty(Str))
            {
                wherestr += " and (";
                var StrAry = Str.Split(',');
                var i = 0;
                foreach (var item2 in StrAry)
                {
                    if (!string.IsNullOrEmpty(item2))
                    {
                        if (i > 0)
                        {
                            wherestr += " or ";
                        }
                        objCommand.Parameters.AddWithValue("@code_id" + item2, item2);
                        wherestr += "  code_id=@code_id" + item2;
                        i += 1;
                    }
                }
                wherestr += " )";
            }
            objCommand.CommandText = string.Format(@"select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' AND code_sclass = 'dept' and active_flag = 1 {0} order by code_id", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                ddl_dept.DataSource = dt;
                ddl_dept.DataValueField = "code_id";
                ddl_dept.DataTextField = "code_name";
                ddl_dept.DataBind();
                ddl_dept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            //帳務部門

            Label[] lbaccdeptS = { lbaccdept1, lbaccdept2, lbaccdept3, lbaccdept4, lbaccdept5, lbaccdept6, lbaccdept7, lbaccdept8, lbaccdept9, lbaccdept10, lbaccdept11, lbaccdept12, lbaccdept13, lbaccdept14 };
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id , code_name  from tbItemCodes  with(nolock)   WHERE code_bclass= '6' and code_sclass= 'account_dept'";
                DataTable dt = dbAdapter.getDataTable(cmd);
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    ((Label)lbaccdeptS[i]).Text = dt.Rows[i]["code_name"].ToString();
                }
            }
        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();

        //objCommand.CommandText = string.Format(@"Declare @pvtheaders nvarchar(max) ,@sql nvarchar(max) --@sumsql     動態組出樞紐資料行相加
        //                                            SELECT   @pvtheaders=ISNULL(@pvtheaders,'')+'['+code_name +'],' 
        //                                            FROM tbItemCodes  with(nolock)   WHERE code_bclass= '6' and code_sclass= 'account_dept' 
        //                                            GROUP BY code_name

        //                                            set @pvtheaders = left(@pvtheaders, len(@pvtheaders) - 1)
        //                                            set @sql = ' SELECT * FROM  (
        //                                               select ISNULL(B.id,0) as id, A.dept, A.tonnes, A.driver ,dept.code_name as deptname,   acdept.code_name , A.car_license , isNULL(B.share,0) as share
        //                                                        , CONVERT(varchar(100), B.udate, 120) as udate  , D.user_name
        //                                               from ttAssets A with(nolock)
        //                                               left join tbAssetsAccountShare B with(nolock) on A.car_license  = B.car_license
        //                                               left join tbItemCodes dept with(nolock) on A.dept  = dept.code_id and dept.code_bclass = ''6'' and dept.code_sclass= ''dept''
        //                                               left join tbItemCodes acdept with(nolock) on B.account_dept  = acdept.code_id and acdept.code_bclass = ''6'' and acdept.code_sclass= ''account_dept'' 
        //                                               left join tbAccounts  D With(Nolock) on D.account_code = B.uuser
        //                                                        where 1=1 {0}
        //                                                ) p
        //                                                pivot
        //                                                (SUM(share)
        //                                            exec sp_executesql @sql", wherestr);

        //帳務部門

        AccDepts = new SortedList();
        string pvtheaders = "";
        Label[] lbaccdeptS = { lbaccdept1, lbaccdept2, lbaccdept3, lbaccdept4, lbaccdept5, lbaccdept6, lbaccdept7, lbaccdept8, lbaccdept9, lbaccdept10, lbaccdept11, lbaccdept12, lbaccdept13, lbaccdept14 };
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select code_id , code_name  from tbItemCodes  with(nolock)   WHERE code_bclass= '6' and code_sclass= 'account_dept'";
            DataTable dt = dbAdapter.getDataTable(cmd);
            //foreach (DataRow dr in dt.Rows)
            //{
            //    AccDepts.Add(dr["code_id"], dr["code_name"]);
            //    pvtheaders += "[" + dr["code_name"].ToString()+ "],";

            //}
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                AccDepts.Add(Convert.ToInt32( dt.Rows[i]["code_id"]), dt.Rows[i]["code_name"]);
                pvtheaders += "[" + dt.Rows[i]["code_name"].ToString() + "],";
                //((Label)lbaccdeptS[i]).Text = dt.Rows[i]["code_name"].ToString();
            }
        }
        objCommand.Parameters.AddWithValue("@pvtheaders", pvtheaders);
        //objCommand.CommandText = string.Format(@"Declare @sql nvarchar(max)
        //set @pvtheaders = left(@pvtheaders, len(@pvtheaders) - 1)
        //set @sql = ' SELECT * FROM  (
        //   select ISNULL(B.id,0) as id, A.dept, A.tonnes, A.driver ,dept.code_name as deptname,acdept.code_name , A.car_license, A.a_id , ISNULL(B.share,0) AS share
        //            , CONVERT(varchar(100), B.udate, 120) as udate  , D.user_name
        //   from ttAssets A with(nolock)
        //   left join tbAssetsAccountShare B with(nolock) on A.car_license  = B.car_license
        //   left join tbItemCodes dept with(nolock) on A.dept  = dept.code_id and dept.code_bclass = ''6'' and dept.code_sclass= ''dept''
        //   left join tbItemCodes acdept with(nolock) on B.account_dept  = acdept.code_id and acdept.code_bclass = ''6'' and acdept.code_sclass= ''account_dept'' 
        //   left join tbAccounts  D With(Nolock) on D.account_code = B.uuser
        //            where 1=1 {0} 
        //    ) p
        //    pivot
        //    (SUM(share)
        //    for code_name in ('+ @pvtheaders +')) AS pvt'
        //exec sp_executesql @sql", wherestr);
        string wherestr = "";
        btnEdit.Style.Add("display", "none");
        if (ddl_dept.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@dept", ddl_dept.SelectedValue.ToString());
            wherestr += "  and dept=@dept";
        }

        if (car_license.Text != "")
        {
            objCommand.Parameters.AddWithValue("@car_license", car_license.Text);
            wherestr += "  and A.car_license=@car_license";
        }
        //distinct 年月
        objCommand.CommandText = string.Format(@"Select dept, tonnes, driver, A.car_license as car_license, a_id,B.udate as udate 
                                                 From ttAssets A 
                                                 Left join (select distinct left(CONVERT(varchar(100),udate,23),7) as udate,car_license ,ROW_NUMBER() OVER (PARTITION BY car_license ORDER BY udate DESC) AS rn from tbAssetsAccountShare with(nolock)) B on A.car_license  = B.car_license  and B.rn = 1 
                                                 Where 1=1 and A.stop_type is NULL {0} 
                                                 Order by dept", wherestr);
        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
            if ((ddl_dept.SelectedValue.ToString() != "" || car_license.Text != "") && dt.Rows.Count > 0)
            {
                btnEdit.Style.Add("display", "unset");
            }
        }

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        ////帳務部門
        //SortedList AccDepts = new SortedList();  
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = "select code_id , code_name  from tbItemCodes  with(nolock)   WHERE code_bclass= '6' and code_sclass= 'account_dept'";
        //    DataTable dt = dbAdapter.getDataTable(cmd);
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        AccDepts.Add(dr["code_id"], dr["code_name"]);
        //    }
        //}

        #region  先刪除該使用單位的所有分攤資料
        string dept = ddl_dept.SelectedValue;
        if (dept != "")
        {
            string strWhere = string.Empty;
            using (SqlCommand cmd_del = new SqlCommand())
            {
                cmd_del.Parameters.AddWithValue("@dept", dept);
                if (car_license.Text != "")
                {
                    cmd_del.Parameters.AddWithValue("@car_license", car_license.Text.Trim());
                    strWhere = " and car_license=@car_license";
                }
                cmd_del.CommandText = string.Format (@"delete from  tbAssetsAccountShare where a_id in( select a_id  from ttAssets  where stop_type is NULL and dept = @dept {0})", strWhere);
                try
                {
                    dbAdapter.execNonQuery(cmd_del);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

            }
        }
        #endregion

        for (int i = 0; i <= New_List.Items.Count - 1; i++)
        {
            HiddenField Hid_id = ((HiddenField)New_List.Items[i].FindControl("hid_id"));
            HiddenField Hid_a_id = ((HiddenField)New_List.Items[i].FindControl("hid_a_id"));
            Literal Licar_license = ((Literal)New_List.Items[i].FindControl("Licar_license"));


            for (int j = 0; j <= AccDepts.Count - 1; j++)
            {
                TextBox tbshare = (TextBox)New_List.Items[i].FindControl("tb_" + (j + 1).ToString());
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@a_id", Hid_a_id.Value);
                    cmd.Parameters.AddWithValue("@car_license", Licar_license.Text);
                    cmd.Parameters.AddWithValue("@account_dept", AccDepts.GetKey(j));
                    cmd.Parameters.AddWithValue("@share", tbshare.Text);
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                    cmd.CommandText = dbAdapter.SQLdosomething("tbAssetsAccountShare", cmd, "insert");
                    dbAdapter.execNonQuery(cmd);
                }

                //if (Hid_id.Value == "")
                //{
                //    using (SqlCommand cmd = new SqlCommand())
                //    {
                //        cmd.Parameters.AddWithValue("@a_id", Hid_a_id.Value);
                //        cmd.Parameters.AddWithValue("@car_license", Licar_license.Text);
                //        cmd.Parameters.AddWithValue("@account_dept", AccDepts.GetKey(j));
                //        cmd.Parameters.AddWithValue("@share", tbshare.Text);
                //        cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                //        cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                //        cmd.CommandText = dbAdapter.SQLdosomething("tbAssetsAccountShare", cmd, "insert");
                //        dbAdapter.execNonQuery(cmd);
                //    }
                //}
                //else
                //{
                //    using (SqlCommand cmd = new SqlCommand())
                //    {
                //        cmd.Parameters.AddWithValue("@share", tbshare.Text);
                //        cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                //        cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                //        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "a_id", Hid_a_id.Value);
                //        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_dept", j + 1);
                //        cmd.CommandText = dbAdapter.genUpdateComm("tbAssetsAccountShare", cmd);
                //        dbAdapter.execNonQuery(cmd);
                //    }
                //}
                //異動紀錄
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@a_id", Hid_a_id.Value);
                    cmd.Parameters.AddWithValue("@car_license", Licar_license.Text);
                    cmd.Parameters.AddWithValue("@account_dept", AccDepts.GetKey(j));
                    cmd.Parameters.AddWithValue("@share", tbshare.Text);
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                    cmd.CommandText = dbAdapter.SQLdosomething("tbAssetsAccountShareChange", cmd, "insert");
                    dbAdapter.execNonQuery(cmd);
                }
            }
        }

        //switch (ApStatus)
        //{
        //    case "Add":
        //        #region 判斷重覆
        //        SqlCommand cmda = new SqlCommand();
        //        DataTable dta = new DataTable();
        //        cmda.Parameters.AddWithValue("@form_id", form_id.Text);
        //        cmda.Parameters.AddWithValue("@assets_id", Hid_assets_id.Value);
        //        cmda.CommandText = "Select id from ttInsurance with(nolock) where form_id=@form_id and assets_id=@assets_id";
        //        dta = dbAdapter.getDataTable(cmda);
        //        if (dta.Rows.Count > 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
        //            return;
        //        }
        //        #endregion

        //        SqlCommand cmdadd = new SqlCommand();
        //        cmdadd.Parameters.AddWithValue("@serial", form_id.Text);
        //        cmdadd.Parameters.AddWithValue("@assets_id", Hid_assets_id.Value);
        //        cmdadd.Parameters.AddWithValue("@Insurance_name", Insurance_name.Text);
        //        cmdadd.Parameters.AddWithValue("@kind", dlkind.SelectedValue);
        //        cmdadd.Parameters.AddWithValue("@sdtate", Sdate.Text);
        //        cmdadd.Parameters.AddWithValue("@edate", Edate.Text);
        //        cmdadd.Parameters.AddWithValue("@price", price.Text);
        //        cmdadd.Parameters.AddWithValue("@apportion_price", apportion_price.Text);
        //        cmdadd.Parameters.AddWithValue("@apportion", apportion.Text);
        //        cmdadd.Parameters.AddWithValue("@memo", memo.Text);
        //        cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
        //        cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
        //        cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
        //        cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
        //        cmdadd.CommandText = dbAdapter.SQLdosomething("ttInsurance", cmdadd, "insert");
        //        try
        //        {
        //            dbAdapter.execNonQuery(cmdadd);
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrStr = ex.Message;
        //        }

        //        break;
        //    case "Modity":
        //        int Totpay = 0;  //累計攤提金額
        //        #region 繳費記錄
        //        if (New_List.Items.Count > 0)
        //        {

        //        }

        //        #endregion

        //        SqlCommand cmdupd = new SqlCommand();
        //        cmdupd.Parameters.AddWithValue("@Insurance_name", Insurance_name.Text);
        //        cmdupd.Parameters.AddWithValue("@kind", dlkind.SelectedValue);
        //        cmdupd.Parameters.AddWithValue("@sdtate", Sdate.Text);
        //        cmdupd.Parameters.AddWithValue("@edate", Edate.Text);
        //        cmdupd.Parameters.AddWithValue("@price", price.Text);
        //        cmdupd.Parameters.AddWithValue("@amount_paid", Totpay);
        //        cmdupd.Parameters.AddWithValue("@apportion", apportion.Text);
        //        cmdupd.Parameters.AddWithValue("@memo", memo.Text);
        //        cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
        //        cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
        //        cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
        //        cmdupd.CommandText = dbAdapter.genUpdateComm("ttInsurance", cmdupd);
        //        try
        //        {
        //            dbAdapter.execNonQuery(cmdupd);
        //        }
        //        catch (Exception ex)
        //        {
        //            ErrStr = ex.Message;
        //        }

        //        break;

        //}



        if (ErrStr != "")
        {
            Panel1.Enabled = false;
            btnSave.Visible = false;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');</script>", false);
        }
        return;





    }


}