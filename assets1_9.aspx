﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_9.aspx.cs" Inherits="assets1_9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });


            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }
        .auto-style1 {
            height: 21px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">車輛保險查詢</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <asp:TextBox ID="keyword" class="form-control " runat="server" placeholder="輸入車輛代號 or 車輛名稱" Width ="200px"></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載"   />
                </div>
                <div class="form-group" style="float: right;">
                    <a href="assets1_9_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>
                <p class="text-primary">※點選任一筆保單可查詢及修改</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th"></th>
                        <th class="_th">表單編號</th>
                        <th class="_th">費用名稱</th>
                        <th class="_th">車牌</th>
                        <th class="_th">費用期間</th>
                        <th class="_th">金額</th>
                        <th class="_th">攤提月份</th>
                        <th class="_th">月攤提數</th>
                        <th class="_th">累計攤提金額</th>
                        <th class="_th">未攤提金額</th>
                        <th class="_th">車主</th>
                        <th class="_th">營業模式</th>
                        <th class="_th">使用者</th>
                        <th class="_th">分攤部門</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">
                                <td>
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                    <a href="assets1_9_edit.aspx?id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>" class="btn btn-info  " id="updclick">修改</a>
                                </td>
                                <td data-th="表單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.form_id").ToString())%></td>
                                <td data-th="費用名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Insurance_name").ToString())%></td>
                                <td data-th="車牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                <td data-th="費用期間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sdtate","{0:yyyy/MM/dd}").ToString())%>~<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.edate","{0:yyyy/MM/dd}").ToString())%></td>
                                <td data-th="金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>
                                <td data-th="攤提月份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.apportion").ToString())%></td>
                                <td data-th="月攤提數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.apportion_price","{0:N0}").ToString())%></td>
                                <td data-th="累計攤提金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.amount_paid","{0:N0}").ToString())%></td>
                                <td data-th="未攤提金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>
                                <td data-th="車主"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.owner").ToString())%></td>
                                <td data-th="營業模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.business_model").ToString())%></td>
                                <td data-th="使用者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user").ToString())%></td>
                                <td data-th="分攤部門"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dept").ToString())%></td>
                                <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="16" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

