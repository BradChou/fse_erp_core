﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class trace_4 : System.Web.UI.Page
{
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            if (dateStart.Text.Length == 0)
            {
                dateStart.Text =
                dateEnd.Text = DateTime.Now.ToString("yyyy/MM/dd");
            }

            DefaultData();
        }
    }

    protected void DefaultData()
    {
        DateTime dt_tmp = new DateTime();
        String str_DateStart, str_DateEnd;
        int i_type = 0;

        if (!int.TryParse(rbl_type.SelectedValue.ToString(), out i_type)) i_type = 0;

        if (!DateTime.TryParse(dateStart.Text, out dt_tmp)) dt_tmp = DateTime.Now;

        str_DateStart = dt_tmp.ToString("yyyy/MM/dd");

        if (!DateTime.TryParse(dateEnd.Text, out dt_tmp))
        {
            str_DateEnd = str_DateStart;
        }
        else
        {
            if (dt_tmp.Date < Convert.ToDateTime(str_DateStart)) str_DateEnd = str_DateStart;
            else str_DateEnd = dt_tmp.ToString("yyyy/MM/dd");
        }

        lbDate.Text = str_DateStart + "~" + str_DateEnd;
        lbType.Text = rbl_type.SelectedItem.Text;

        using (SqlCommand cmd = new SqlCommand())
        {
            #region 組SQL
            String strSQL = @" DECLARE @tmp_all table (supplier_code nvarchar(3), check_number nvarchar(20)) 

                               --(in date range)
                               IF @type = 0
                                  BEGIN
                                     --簽單(預設)
                               	  INSERT INTO @tmp_all
                                     SELECT supplier_code,check_number        
                                       FROM tcDeliveryRequests drs With(Nolock)
                                      WHERE cancel_date IS NULL and CONVERT(CHAR(10),print_date,111) BETWEEN @dateS AND　@dateE AND check_number IS NOT NULL  AND print_date IS NOT NULL 
                                      AND customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
                                  END
                               ELSE IF @type = 1
                                  BEGIN
                                     --回單
                               	  INSERT INTO @tmp_all
                               	  SELECT supplier_code,check_number       
                                       FROM tcDeliveryRequests drs With(Nolock)
                                      WHERE cancel_date IS NULL and CONVERT(CHAR(10),print_date,111) BETWEEN @dateS AND　@dateE AND drs.receipt_flag = 1 AND check_number IS NOT NULL  AND print_date IS NOT NULL 
                                      AND customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
                                  END
                               ELSE 
                                  BEGIN
                                     --棧板回收
                               	  INSERT INTO @tmp_all
                               	  SELECT supplier_code,check_number       
                                       FROM tcDeliveryRequests drs With(Nolock)
                                      WHERE cancel_date IS NULL and CONVERT(CHAR(10),print_date,111) BETWEEN @dateS AND　@dateE AND drs.pallet_recycling_flag = 1 AND check_number IS NOT NULL  AND print_date IS NOT NULL 
                                      AND customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
                                  END
                               BEGIN
                                   --應掃筆數
                                   SELECT supplier_code,COUNT(1) num 
                                       INTO #tmp_1
                                       FROM @tmp_all
                                   GROUP BY supplier_code
                                      
                                   --未掃筆數
                                   SELECT supplier_code,COUNT(1) num
                                       INTO #tmp_2
                                       FROM @tmp_all 
                                   WHERE check_number NOT IN(SELECT dlog.check_number FROM ttDeliveryScanLog dlog With(Nolock) WHERE  scan_date>=DATEADD(MONTH,-3,GETDATE()) )
                                   GROUP BY supplier_code

                                   --已掃筆數
                                   SELECT supplier_code,COUNT(1) num
                                       INTO #tmp_3
                                       FROM @tmp_all 
                                   WHERE check_number  IN(SELECT dlog.check_number FROM ttDeliveryScanLog dlog With(Nolock) WHERE  scan_date>=DATEADD(MONTH,-3,GETDATE()))
                                   GROUP BY supplier_code
                                   
  
                                   --回傳TABLE(應掃+未掃筆數+已掃筆數)
                                   SELECT  ROW_NUMBER() OVER(ORDER BY n1.supplier_code) AS 'no'
                                          ,n1.supplier_code,sup.supplier_name+'('+n1.supplier_code+')' supplier_name 
	                                      ,n1.num 'num_all',ISNULL(n2.num,0) 'num_no' ,ISNULL(n3.num,0) 'num_yes'
                                          ,CONVERT(VARCHAR(5),ROUND(CONVERT(FLOAT,(n1.num-ISNULL(n2.num,0)))  /CONVERT(FLOAT,n1.num),2) * 100)+'%' AS 'rate' 
                                     FROM #tmp_1 n1
                                     LEFT JOIN #tmp_2 n2 ON n1.supplier_code=n2.supplier_code
                                     LEFT JOIN #tmp_3 n3 ON n1.supplier_code=n3.supplier_code
	                                 LEFT JOIN tbSuppliers sup With(Nolock) ON n1.supplier_code = sup.supplier_code AND sup.active_flag =1 
                               END
                               
                               IF object_id('tempdb..#tmp_1') is not null
                                   DROP TABLE #tmp_1
                               
                               IF object_id('tempdb..#tmp_2') is not null
                                   DROP TABLE #tmp_2   

                               IF object_id('tempdb..#tmp_3') is not null
                                   DROP TABLE #tmp_3   ";
            #endregion

            cmd.Parameters.AddWithValue("@type", i_type.ToString());
            cmd.Parameters.AddWithValue("@dateS", str_DateStart);
            cmd.Parameters.AddWithValue("@dateE", str_DateEnd);
            string manager_type = Session["manager_type"].ToString(); //管理單位類別  
            string customer_code = Session["master_code"].ToString();

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    customer_code = "";
                    break;
              
            }
            cmd.Parameters.AddWithValue("@customer_code", customer_code);
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    int[] total = new int[3] ;
                    for (int i = 0; i <= dt.Rows.Count - 1 ; i++)
                    {
                        total[0] += Convert.ToInt32(dt.Rows[i]["num_all"]);
                        total[1] += Convert.ToInt32(dt.Rows[i]["num_yes"]);
                        total[2] += Convert.ToInt32(dt.Rows[i]["num_no"]);
                    }
                    row["supplier_name"] = "總計";
                    row["num_all"] = total[0];
                    row["num_yes"] = total[1];
                    row["num_no"] = total[2];
                    row["rate"] = ((Convert.ToDouble(total[1]) / Convert.ToDouble(total[0])) * 100).ToString("N0") + "%" ;
                    dt.Rows.Add(row);
                }

                list_scan.DataSource = dt;
                list_scan.DataBind();
                DT = dt;

                //PagedDataSource pagedData = new PagedDataSource();
                //pagedData.DataSource = dt.DefaultView;
                //pagedData.AllowPaging = true;
                //pagedData.PageSize = 10;

                //#region 下方分頁顯示
                ////一頁幾筆
                //int CurPage = 0;
                //if ((Request.QueryString["Page"] != null))
                //{
                //    //控制接收的分頁並檢查是否在範圍
                //    if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                //    {
                //        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                //        {
                //            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                //        }
                //        else
                //        {
                //            CurPage = 1;
                //        }
                //    }
                //    else
                //    {
                //        CurPage = 1;
                //    }
                //}
                //else
                //{
                //    CurPage = 1;
                //}
                //pagedData.CurrentPageIndex = (CurPage - 1);
                //lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1"));
                //lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString()));
                ////第一頁控制
                //if (!pagedData.IsFirstPage)
                //{
                //    lnkfirst.Enabled = true;
                //    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1)));
                //}
                //else
                //{
                //    lnkfirst.Enabled = false;
                //}
                ////最後一頁控制
                //if (!pagedData.IsLastPage)
                //{
                //    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1)));
                //    lnklast.Enabled = true;
                //}
                //else
                //{
                //    lnklast.Enabled = false;
                //}

                ////上一頁控制
                //if (CurPage - 1 == 0)
                //{
                //    lnkPrev.Enabled = false;
                //}
                //else
                //{
                //    lnkPrev.Enabled = true;
                //}

                ////下一頁控制
                //if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                //{
                //    lnkNext.Enabled = false;
                //}
                //else
                //{
                //    lnkNext.Enabled = true;
                //}

                //if (pagedData.PageSize > 0)
                //{
                //    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                //}

                //#endregion

                //list_scan.DataSource = pagedData;
                //list_scan.DataBind();
                //ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }

    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DateTime dt_tmp = new DateTime();


        if (!DateTime.TryParse(dateStart.Text, out dt_tmp))
        {
            RetuenMsg("請選擇發送日期!");
        }
        else
        {
            DefaultData();
        }
    }

    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void btnSearch_Click1(object sender, EventArgs e)
    {

    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("配達統計");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;

            sheet1.Cells[1, 1, 1, 6].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 6].Value = rbl_type.SelectedItem.Text + "查詢";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 6].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 6].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 6].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 6].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "發送日期：" + dateStart.Text + "~" + dateEnd.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            sheet1.Cells[3, 1, 3, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1, 3, 6].Style.Font.Size = 12;
            sheet1.Cells[3, 1, 3, 6].Style.Font.Bold = true;
            sheet1.Cells[3, 1, 3, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                       
            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "區配商";
            sheet1.Cells[3, 3].Value = "應掃筆數";
            sheet1.Cells[3, 4].Value = "已掃筆數";
            sheet1.Cells[3, 5].Value = "未掃筆數";
            sheet1.Cells[3, 6].Value = "掃描率";

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["supplier_name"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["num_all"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["num_yes"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["num_no"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["rate"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=report.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}