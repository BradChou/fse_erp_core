﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Text;

public partial class LTreport_1_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;
            date2.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}  and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

            Suppliers.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "station_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (manager_type == "0" || manager_type == "1")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && manager_type == "2")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);
            #endregion

            issendcontact.Items.Insert(0, new ListItem("全部", ""));
            issendcontact.Items.Insert(1, new ListItem("未銷單", "1"));
            issendcontact.Items.Insert(2, new ListItem("已銷單", "2"));

            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["date2"] != null)
            {
                if (Request.QueryString["date2"] != "")
                {
                    date2.Text = Request.QueryString["date2"];
                }
            }

            if (Request.QueryString["Suppliers"] != null)
            {

                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }

            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            if (Request.QueryString["issendcontact"] != null)
            {
                if (Request.QueryString["issendcontact"] != "")
                {
                    issendcontact.SelectedValue = Request.QueryString["issendcontact"];
                }
            }

            if (Request.QueryString["RType"] != null)
            {
                dlRType.SelectedValue = Request.QueryString["RType"];
            }
            if (Request.QueryString["date1"] != null)
            {
                readdata();
            }

        }
    }

    private void readdata()
    {
        string sqlwhere = "";
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            cmd.Parameters.AddWithValue("@DeliveryType", "R");

            sqlwhere += @"AND A.DeliveryType =  @DeliveryType";

            if (issendcontact.SelectedValue != "")
            {
                if (issendcontact.SelectedValue == "1")
                    sqlwhere += @" AND A.cancel_date is null ";
                else
                    sqlwhere += @" AND A.cancel_date is not null  ";
            }

            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND A.supplier_code  =   @supplier_id ";
            }
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                sqlwhere += @" AND A.customer_code =  @customer_code  ";
            }
            querystring += "&second_code=" + second_code.SelectedValue.ToString();
            querystring += "&RType=" + dlRType.SelectedValue;
            

            if (date1.Text.ToString() != "")
            {
                querystring += "&date1=" + date1.Text.ToString();
            }

            if (date2.Text.ToString() != "")
            {
                querystring += "&date2=" + date2.Text.ToString();
            }
            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";

            //類型可以選全選
            if (dlRType.SelectedValue != "")
            {
                if (dlRType.SelectedValue == "R1")
                    sqlwhere += @"and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
                else if (dlRType.SelectedValue == "R2")
                    sqlwhere += @"and (A.invoice_desc not like '%來回件%' and A.supplier_code ='F35' or A.supplier_code <>'F35')";
            }


            querystring += "&rb_pricing_type=" + pricing_type;
            querystring += "&issendcontact=" + issendcontact.SelectedValue;


            cmd.CommandText = string.Format(@"
            SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
			A.print_date '發送日期', 
            A.ship_date '取件日期',
			CONVERT(CHAR(10),DATEADD(day,1,A.ship_date),111) '配送日期',  
			A.check_number '明細貨號' , 
			A.order_number '訂單編號' 
			,sta1.station_name  '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數',
			CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收件人地址'
			,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end '寄件件人地址'
			,case when A.supplier_code = 'F35' then 
(select station_scode from tbStation tb 
inner join ttArriveSitesScattered Q on Q.station_code = tb.station_scode where Q.post_city=A.Receive_city and Q.post_area = A.receive_area )
when A.supplier_code <> 'F35' then Replace(A.supplier_code, 'F', '') end   '站所代碼',


case when A.supplier_code = 'F35' then (select D1.station_name from 
												  ttArriveSitesScattered C1 With(Nolock) 
												  LEFT JOIN tbStation D1 with(nolock) on  D1.station_scode =  C1.station_code
												  where C1.post_city = A.receive_city and C1.post_area= A.receive_area ) else
G.station_name end '站所名稱',
			B.code_name '傳票區分',
case  when A.check_number like '990%' then '0'		
else
ISNULL(A.collection_money,0) end  '代收金額',
			A.invoice_desc '備註'
			,_arr.arrive_state '貨態狀況'
			,_arr.arrive_date '配達時間'
			,_arr.arrive_item '配送異常說明'
            ,_arr.newest_station+' '+H.station_name '作業站',
		      case when _arr.newest_driver_code is null then '' 
                 when _arr.newest_driver_code = '' then '' 
                 when  _arr.newest_driver_code <> '' then (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end '作業人員',
			--case T.receive_option when TB.code_id then TB.code_name end '集貨異常情形',
            --_arr.arrive_state '貨態情形',
            case A.latest_scan_item  when '5' then isnull(TB.code_name,'') 
                       when '3' then isnull(_arr.arrive_item,'')
					   when '4' then isnull(_arr.arrive_item,'')
            end  '貨態情形', 
			_arr.newest_scandate '掃讀日期',
            case when A.invoice_desc like '%來回件%' AND A.supplier_code ='F35' then '來回件'
            else '退貨'
            end '類型區分'
			FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code
            LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_scode  = ta.station_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
            LEFT JOIN tbStation H With(Nolock) ON _arr.newest_station = H.station_scode
            left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and  t.scan_item = '5' and A.latest_scan_date = T.scan_date
			left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			  AND A.print_date IS NOT NULL
			  AND A.Less_than_truckload= 1 
            {0}
            ", sqlwhere);
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {

                SqlCommand cmds = new SqlCommand();
                string wherestr = "";
                cmds.CommandTimeout = 600;

                //類型可選全選
                if (dlRType.SelectedValue != "")
                {
                    if (dlRType.SelectedValue == "R1")
                        wherestr += @"and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
                    else if (dlRType.SelectedValue == "R2")
                        wherestr += @"and (A.invoice_desc not like '%來回件%' and A.supplier_code ='F35' or A.supplier_code <>'F35')";
                }

                wherestr += dbReadOnlyAdapter.genWhereCommIn(ref cmds, "pricing_type", "02".Split(','));
                cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbCustomers C With(Nolock) ON A.customer_code = C.customer_code
			LEFT JOIN tbItemCodes D With(Nolock) on D.code_id =A.pricing_type and D.code_sclass = 'PM'
			LEFT JOIN tbItemCodes _special With(Nolock) ON A.special_send = _special.code_id AND _special.code_sclass ='S4'	
			LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code
			LEFT JOIN ttArriveSitesScattered sta With(nolock) on A.send_city = sta.post_city and A.send_area = sta.post_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code = @supplier_id OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            {0}
)
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                cmds.Parameters.AddWithValue("@DeliveryType", "R");
                cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                if (Suppliers.SelectedValue != "")
                {
                    cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                }
                else
                {
                    cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                }

                if (second_code.SelectedValue != "")
                {
                    string customer_code = second_code.SelectedValue.Trim();
                    cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }
                else
                {
                    cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                }

                DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
                ltotalpages.Text = dt.Rows.Count.ToString();
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    row["配送區域"] = "總計";
                    row["件數"] = dtsum.Rows[0]["件數"].ToString();

                    dt.Rows.Add(row);
                }
                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }


    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }

        if (date2.Text.ToString() != "")
        {
            querystring += "&date2=" + date2.Text.ToString();
        }

        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&second_code=" + second_code.SelectedValue.ToString();
        querystring += "&RType=" + dlRType.SelectedValue.ToString();
        querystring += "&issendcontact=" + issendcontact.SelectedValue;

        Response.Redirect(ResolveUrl("~/LTreport_1_2.aspx?search=yes" + querystring));
    }

        
    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "逆物流總表";
        string sqlwhere = "";
        string file_name = "逆物流總表" + DateTime.Now.ToString("yyyyMMdd");
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = "usp_GetLTDistributionReportR";
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
        //    cmd.Parameters.AddWithValue("@print_dates", date1.Text);
        //    cmd.Parameters.AddWithValue("@print_datee", date2.Text);
        //    cmd.Parameters.AddWithValue("@DeliveryType", "R");
        //    cmd.Parameters.AddWithValue("@Rtype", dlRType.SelectedValue);

        //    if (issendcontact.SelectedValue != "")
        //    {
        //        cmd.Parameters.AddWithValue("@issendcontact", issendcontact.SelectedValue);
        //    }

        //    if (Suppliers.SelectedValue != "")
        //    {
        //        cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
        //    }

        //    if (second_code.SelectedValue != "")
        //    {
        //        string customer_code = second_code.SelectedValue.Trim();
        //        cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
        //    }


        //    string pricing_type = "'02'";
        //    cmd.Parameters.AddWithValue("@pricing_type", pricing_type);


        //    using (DataTable dt = dbAdapter.getDataTable(cmd))
        //    {
        //        if (dt != null && dt.Rows.Count > 0)
        //        {


        //            using (ExcelPackage p = new ExcelPackage())
        //            {
        //                p.Workbook.Properties.Title = sheet_title;
        //                p.Workbook.Worksheets.Add(sheet_title);
        //                ExcelWorksheet ws = p.Workbook.Worksheets[1];
        //                int colIndex = 1;
        //                int rowIndex = 1;
        //                foreach (DataColumn dc in dt.Columns)
        //                {
        //                    var cell = ws.Cells[rowIndex, colIndex];
        //                    var fill = cell.Style.Fill;
        //                    fill.PatternType = ExcelFillStyle.Solid;
        //                    fill.BackgroundColor.SetColor(Color.LightGray);

        //                    //Setting Top/left,right/bottom borders.
        //                    var border = cell.Style.Border;
        //                    border.Bottom.Style =
        //                        border.Top.Style =
        //                        border.Left.Style =
        //                        border.Right.Style = ExcelBorderStyle.Thin;

        //                    //Setting Value in cell
        //                    cell.Value = dc.ColumnName;
        //                    colIndex++;
        //                }
        //                rowIndex++;

        //                using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
        //                {
        //                    col.Style.Numberformat.Format = "yyyy/MM/dd";
        //                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
        //                }


        //                foreach (DataRow dr in dt.Rows)
        //                {
        //                    colIndex = 1;
        //                    foreach (DataColumn dc in dt.Columns)
        //                    {
        //                        var cell = ws.Cells[rowIndex, colIndex];
        //                        cell.Value = dr[dc.ColumnName];

        //                        //Setting borders of cell
        //                        var border = cell.Style.Border;
        //                        border.Left.Style =
        //                            border.Right.Style = ExcelBorderStyle.Thin;
        //                        colIndex++;
        //                    }

        //                    rowIndex++;
        //                }
        //                ws.Cells.AutoFitColumns();
        //                ws.View.FreezePanes(2, 1);
        //                ws.Cells.Style.Font.Size = 12;
        //                Response.Clear();
        //                Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
        //                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //                Response.BinaryWrite(p.GetAsByteArray());
        //                Response.End();

        //                //str_retuenMsg += "下載完成!";
        //            }
        //        }
        //        //else
        //        //{
        //        //    str_retuenMsg += "查無此資訊，請重新確認!";
        //        //}
        //    }

        //}

        using (SqlCommand cmd = new SqlCommand())
        {

            if (issendcontact.SelectedValue != "")
            {
                if (issendcontact.SelectedValue == "1")
                    sqlwhere += @" AND A.cancel_date is null ";
                else
                    sqlwhere += @" AND A.cancel_date is not null  ";
            }

            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND A.supplier_code  =   @supplier_id ";
            }

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code + "%");  //客代
                sqlwhere += @" AND A.customer_code like  @customer_code  ";
            }

            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";

            //類型可選全選
            if (dlRType.SelectedValue != "")
            {
                if (dlRType.SelectedValue == "R1")
                    sqlwhere += @"and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
                else if (dlRType.SelectedValue == "R2")
                    sqlwhere += @"and (A.invoice_desc not like '%來回件%' and A.supplier_code ='F35' or A.supplier_code <>'F35')";
            }


            //cmd.CommandText = "usp_GetLTDistributionReportD";
            cmd.CommandText = string.Format(@"SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
			A.print_date '發送日期', 
            A.ship_date '取件日期',
			CONVERT(CHAR(10),DATEADD(day,1,A.ship_date),111) '配送日期',  
			A.check_number '明細貨號' , 
			A.order_number '訂單編號' 
			,sta1.station_name  '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數',
			receive_address '收件人地址',
            A.send_city '發送縣市別', 
            A.send_area '發送縣區域'
			, send_address  '寄件件人地址'
			,case when A.supplier_code = 'F35' then 
(select station_scode from tbStation tb 
inner join ttArriveSitesScattered Q on Q.station_code = tb.station_scode where Q.post_city=A.Receive_city and Q.post_area = A.receive_area )
when A.supplier_code <> 'F35' then Replace(A.supplier_code, 'F', '') end   '站所代碼',


case when A.supplier_code = 'F35' then (select D1.station_name from 
												  ttArriveSitesScattered C1 With(Nolock) 
												  LEFT JOIN tbStation D1 with(nolock) on  D1.station_scode =  C1.station_code
												  where C1.post_city = A.receive_city and C1.post_area= A.receive_area ) else
G.station_name end '站所名稱',
			B.code_name '傳票區分',
case  when A.check_number like '990%' then '0'		
else
ISNULL(A.collection_money,0) end  '代收金額',
			A.invoice_desc '備註'
			
			,convert(nvarchar,_arr.arrive_date,120) '配達時間'
			--_arr.arrive_item '配送異常說明' ,
            ,_arr.newest_station+' '+H.station_name '作業站',
		      case when _arr.newest_driver_code is null then '' 
                 when _arr.newest_driver_code = '' then '' 
                 when  _arr.newest_driver_code <> '' then (select driver_code +' '+driver_name from tbDrivers where driver_code = _arr.newest_driver_code) end '作業人員',
			--_arr.arrive_state '貨態情形',
            --case T.receive_option when TB.code_id then TB.code_name end '集貨異常情形',
            _arr.arrive_state '貨態狀況',
			case latest_scan_item  when '5' then isnull(TB.code_name,'') 
                       when '3' then isnull(_arr.arrive_item,'')
					   when '4' then isnull(_arr.arrive_item,'')
            end  '貨態情形', 
            convert(nvarchar(19), _arr.newest_scandate,120) '掃讀日期',
            case when A.cancel_date is null then ''
            else 'V'    end '銷單',
            case when A.invoice_desc like '%來回件%' AND A.supplier_code ='F35' then '來回件'
            else '退貨'
            end '類型區分'
            FROM tcDeliveryRequests A With(Nolock) 
		    LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code
            LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_scode  = ta.station_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
            LEFT JOIN tbStation H With(Nolock) ON _arr.newest_station = H.station_scode
            left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and t.scan_item = '5' and A.latest_scan_date = T.scan_date
			left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= 1 
            AND A.DeliveryType= @DeliveryType
            {0} 
",
            sqlwhere);

            //cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            cmd.Parameters.AddWithValue("@DeliveryType", "R");

            //if (issendcontact.SelectedValue != "")
            //{
            //    cmd.Parameters.AddWithValue("@issendcontact", issendcontact.SelectedValue);
            //}

            //if (Suppliers.SelectedValue != "")
            //{
            //    cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            //}

            //if (second_code.SelectedValue != "")
            //{
            //    string customer_code = second_code.SelectedValue.Trim();
            //    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            //}

            //string pricing_type = "02";

            //cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {


                    SqlCommand cmds = new SqlCommand();
                    string wherestr = "";
                    cmds.CommandTimeout = 600;
                    
                    //類型可選全選
                    if (dlRType.SelectedValue != "")
                    {
                        if (dlRType.SelectedValue == "R1")
                            wherestr += @"and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
                        else if (dlRType.SelectedValue == "R2")
                            wherestr += @"and (A.invoice_desc not like '%來回件%' and A.supplier_code ='F35' or A.supplier_code <>'F35')";
                    }

                    wherestr += dbReadOnlyAdapter.genWhereCommIn(ref cmds, "pricing_type", "02".Split(','));
                    cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbCustomers C With(Nolock) ON A.customer_code = C.customer_code
			LEFT JOIN tbItemCodes D With(Nolock) on D.code_id =A.pricing_type and D.code_sclass = 'PM'
			LEFT JOIN tbItemCodes _special With(Nolock) ON A.special_send = _special.code_id AND _special.code_sclass ='S4'	
			LEFT JOIN tbStation G With(Nolock) ON A.supplier_code = G.station_code
			LEFT JOIN ttArriveSitesScattered sta With(nolock) on A.send_city = sta.post_city and A.send_area = sta.post_area
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code = @supplier_id OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            {0}
)
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                    cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                    cmds.Parameters.AddWithValue("@DeliveryType", "R");
                    cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                    cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                    if (Suppliers.SelectedValue != "")
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                    }

                    if (second_code.SelectedValue != "")
                    {
                        string customer_code = second_code.SelectedValue.Trim();
                        cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                    }

                    DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
                    ltotalpages.Text = dt.Rows.Count.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.NewRow();
                        row["配送區域"] = "總計";
                        row["件數"] = dtsum.Rows[0]["件數"].ToString();

                        dt.Rows.Add(row);
                    }

                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        //Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}.xlsx", HttpUtility.UrlEncode(file_name, Encoding.UTF8), Encoding.UTF8));
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }
                }
            }
        }
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@type", "1");
            if (manager_type == "5")
            {
                cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock) where A.customer_code = @customer_code");
            }
            else
            {
                if (Suppliers.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    wherestr = " and A.supplier_code = @supplier_code ";
                }
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    Left join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 
                                                    and A.stop_shipping_code = '0' and A.type =@type {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
            }

            second_code.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
            second_code.DataValueField = "customer_code";
            second_code.DataTextField = "name";
            second_code.DataBind();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2") second_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            //second_code.Items.Insert(0, new ListItem("", ""));
        }
        #endregion
    }
}