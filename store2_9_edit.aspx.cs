﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class store2_9_edit : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";
           

            fee_date.Attributes.Add("readonly", "true");
            iuser.Attributes.Add("readonly", "true");
            idate.Attributes.Add("readonly", "true");
            pay_date.Attributes.Add("readonly", "true");            
            tax.Attributes.Add("readonly", "true");
            total.Attributes.Add("readonly", "true");
            iuser.Text = Session["user_name"].ToString();

            readdata(a_id);

        }
    }

    private void readdata(string a_id)
    {  
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);
            
            cmd.CommandText = @"Select * from ttAssetsExpenditure A with(nolock) 
                                Where id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                
                fee_date.Text = dt.Rows[0]["fee_date"].ToString().Trim();
                iuser.Text = dt.Rows[0]["iuser"].ToString().Trim();
                idate.Text = Convert.ToDateTime(dt.Rows[0]["idate"]).ToString("yyyy/MM/dd");
                company.Text = dt.Rows[0]["company"].ToString().Trim();
                pay_kind.SelectedValue = dt.Rows[0]["pay_kind"].ToString().Trim();

                foreach (ListItem item in paper.Items)
                {
                    if (dt.Rows[0]["paper"].ToString().Trim().IndexOf(item.Value) > 0)
                    {
                        item.Selected = true;
                    }
                }

                pay_date.Text = Convert.ToDateTime(dt.Rows[0]["pay_date"]).ToString("yyyy/MM/dd");
                pay_account.Text = dt.Rows[0]["pay_account"].ToString().Trim();
                pay_num.Text = dt.Rows[0]["pay_num"].ToString().Trim();
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();                
                price.Text = dt.Rows[0]["price"].ToString().Trim();
                tax.Text = dt.Rows[0]["tax"].ToString().Trim();
                total.Text = dt.Rows[0]["money"].ToString().Trim();
                btnsave.CssClass= btnsave.CssClass.Replace("hidden", "");
                printpaper.CssClass = printpaper.CssClass.Replace("hidden", "");
            }

        }
        else
        {
            ApStatus = "Add";
        }

       
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                //car_license.ReadOnly  = true ;
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                idate.Text = DateTime.Now.ToString("yyyy/MM/dd");
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
       
        string paperselectedValue = "";
        foreach (ListItem item in paper.Items)
        {
            if (item.Selected)
            {
                paperselectedValue += ","+item.Value;
            }
        }

        //#region 判斷重覆
        //string wherestr = "";
        //SqlCommand cmda = new SqlCommand();
        //DataTable dta = new DataTable();
        //string oil = "";
        /////if (ddl_fee.SelectedValue=="1")
        /////{
        /////    oil = ddl_oil.SelectedValue;
        /////}
        /////cmda.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue);
        //cmda.Parameters.AddWithValue("@fee_date", fee_date.Text.ToString().Trim());
        /////if (!string.IsNullOrEmpty(oil))
        /////{
        /////    cmda.Parameters.AddWithValue("@oil", oil);
        /////    wherestr += "  and oil =@oil";
        /////}
        //if (ApStatus== "Modity")
        //{
        //    cmda.Parameters.AddWithValue("@id", a_id);
        //    wherestr += "  and id <>@id";
        //}
        //cmda.CommandText = string.Format(@"Select id from ttAssetsExpenditure with(nolock) where fee_type=@fee_type and fee_date=@fee_date {0}", wherestr);
        //dta = dbAdapter.getDataTable(cmda);
        //if (dta.Rows.Count > 0)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
        //    return;
        //}
        //#endregion

        switch (ApStatus)
        {
            case "Add":

                SqlCommand cmdadd = new SqlCommand();
                ///cmdadd.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue);
                //if (ddl_fee.SelectedValue=="1" && !string.IsNullOrEmpty(oil))
                //{
                     cmdadd.Parameters.AddWithValue("@oil", "");
                //}
                cmdadd.Parameters.AddWithValue("@fee_type", "");
                cmdadd.Parameters.AddWithValue("@fee_type_name", "");
                cmdadd.Parameters.AddWithValue("@fee_date", fee_date.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@iuser", iuser.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@idate", idate.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@company", company.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@pay_kind", pay_kind.SelectedValue);
                cmdadd.Parameters.AddWithValue("@paper", paperselectedValue);
                cmdadd.Parameters.AddWithValue("@pay_date", pay_date.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@pay_account", pay_account.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@pay_num", pay_num.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@money", total.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@price", price.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@tax", tax.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdadd.Parameters.AddWithValue("@type", 1);                                               //1:超商
                cmdadd.CommandText = dbAdapter.SQLdosomething("ttAssetsExpenditure", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                ///cmdupd.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue);
                ///cmdupd.Parameters.AddWithValue("@fee_type_name", ddl_fee.SelectedItem.Text);
                //if (ddl_fee.SelectedValue == "1" && !string.IsNullOrEmpty(oil))
                //{
                    cmdupd.Parameters.AddWithValue("@oil", "");
                //}
                cmdupd.Parameters.AddWithValue("@fee_type", "");
                cmdupd.Parameters.AddWithValue("@fee_type_name", "");
                cmdupd.Parameters.AddWithValue("@fee_date", fee_date.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@iuser", iuser.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@idate", idate.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@company", company.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@pay_kind", pay_kind.SelectedValue);
                cmdupd.Parameters.AddWithValue("@paper", paperselectedValue);
                cmdupd.Parameters.AddWithValue("@pay_date", pay_date.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@pay_account", pay_account.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@pay_num", pay_num.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@money", total.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@price", price.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@tax", tax.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", a_id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttAssetsExpenditure", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='store2_9.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }

    protected void Print_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.Parameters.AddWithValue("@a_id", a_id);
        cmd.CommandText = @"Select * from ttAssetsExpenditure A with(nolock) 
                                Where id = @a_id ";
        dt = dbAdapter.getDataTable(cmd);

        string[] ParName = new string[0];
        string[] ParValue = new string[0];
        PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "Reportassets3_2", ParName, ParValue, "dsAssets3_2", dt);
    }
    

    ///    if (Upd_title.UpdateMode == UpdatePanelUpdateMode.Conditional)
    ///    {
    ///        Upd_title.Update();
    ///    }
    ///}

    [System.Web.Services.WebMethod]
    public static APICode CallAPI(string feetype, string feedate, string oil)
    {
        APICode Result = new APICode();
        bool States = false;
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";
        if (feetype != "")
        {
            objCommand.Parameters.AddWithValue("@fee_type", feetype);
            wherestr += "  and a.fee_type =@fee_type";
        }
        if (oil != "")
        {
            objCommand.Parameters.AddWithValue("@oil", oil);
            wherestr += "  and a.oil =@oil";
        }
        if (feedate != "")
        {
            DateTime sdate = Convert.ToDateTime(feedate + "/01");
            DateTime edate = sdate.AddMonths(1).AddDays(-sdate.AddMonths(1).Day);
            objCommand.Parameters.AddWithValue("@sdate", sdate);
            wherestr += "  and a.date >=@sdate";
            objCommand.Parameters.AddWithValue("@edate", edate.AddDays(1));
            wherestr += "  and a.date <=@edate";
        }

        objCommand.CommandText = string.Format(@"select A.* ,
                                                    ROUND(A.litre * A.receipt_price_notax,2) as 'receiptamt_notax'
                                                    from ttAssetsFee A with(nolock)
                                                where 1=1  {0} 
                                                order by a.date asc", wherestr);
        int MoneyTotal = 0;
        int PriceTotal = 0;
        int TaxTotal = 0;
        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            foreach(DataRow od in dt.Rows)
            {
                States = true;
                PriceTotal += int.Parse(Convert.ToInt32(od["receiptamt_notax"]).ToString());
            }
            TaxTotal = Convert.ToInt32(PriceTotal * 0.5);
            MoneyTotal = PriceTotal + TaxTotal;
        }

        Result = new APICode
        {
            State = States,
            Money = MoneyTotal.ToString(),
            Price = PriceTotal.ToString(),
            Tax = TaxTotal.ToString()
        };
        return Result;
    }
    public class APICode
    {
        public bool State { get; set; }
        public string Money { get; set; }
        public string Price { get; set; }
        public string Tax { get; set; }
    }

}