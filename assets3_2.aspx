﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets3_2.aspx.cs" Inherits="assets3_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>

    </style>
    
    <script type="text/javascript">
        $(document).ready(function () {    
            
            $(".btn-primary").click(function () {
                showBlockUI();
            });
            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });


        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

    </script>

    <style>
        ._th {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">支出證明單/請款單</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <%--<asp:UpdatePanel ID="Upd_title" runat="server" UpdateMode ="Conditional">
                        <ContentTemplate>--%>
                            <label>費用類別：</label>
                            <asp:DropDownList ID="ddl_fee" runat="server" class="form-control " AutoPostBack ="true" OnSelectedIndexChanged="ddl_fee_SelectedIndexChanged"></asp:DropDownList>
                            <asp:Label ID="lb_oil" runat="server" Text="油品公司" Visible="false"></asp:Label>
                            <asp:DropDownList ID="ddl_oil" runat="server" class="form-control " Visible="false">
                                <asp:ListItem Value="">請選擇</asp:ListItem>
                                <asp:ListItem>全國</asp:ListItem>
                                <asp:ListItem>台塑</asp:ListItem>
                            </asp:DropDownList>
                            <label>費用年月：</label>
                            <asp:TextBox ID="fee_date2" runat="server" class="form-control date_pickerYearMonth" MaxLength="7" placeholder="YYYY/MM" autocomplete="off"></asp:TextBox>
                            <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                        <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                <div class="form-group" style="float: right;">
                    <a href="assets3_2_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>
                
                <p class="text-primary">※點選任一資料可查詢及修改</p>
               <%-- <asp:UpdatePanel ID="Upd_data" runat="server">
                    <ContentTemplate>--%>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="_th"></th>
                                <th class="_th">費用類別</th>
                                <th class="_th">費用年月</th>
                                <th class="_th">申請年月</th>
                                <th class="_th">支付對象</th>
                                <th class="_th">預計付款日</th>
                                <th class="_th">支付方式</th>
                                <th class="_th">請款金額(元)</th>
                                <th class="_th">銷售額(元)</th>
                                <th class="_th">稅額(元)</th>
                                <th class="_th">用途說明</th>
                                <th class="_th">更新人員</th>
                                <th class="_th">更新日期</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr class="paginate">
                                        <td>
                                            <asp:Button ID="btn_Edit" CssClass="btn btn-success" CommandName="cmdPrint" runat="server" Text="列 印"  CommandArgument ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                            <a href="assets3_2_edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>" class="btn btn-info  " id="updclick">修改</a>
                                        </td>
                                        <td><%#DataBinder.Eval(Container, "DataItem.fee_type").ToString()=="1"?Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.feetypeoil").ToString()):Func.GetRow("code_name","tbItemCodes","","code_id = @fee_type and code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1","fee_type",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.fee_type").ToString()))%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.fee_date").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.idate","{0:yyyy-MM-dd}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.company").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pay_date","{0:yyyy-MM-dd}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pay_kindStr").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.money").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="12" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>

                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
                <div id="page-nav" class="page"></div>
                    <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                
            </div>
        </div>
    </div>
</asp:Content>

