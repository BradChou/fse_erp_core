﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GetReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (!IsPostBack)

        if (!loginchk.IsLogin())
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        else
        {
            String type = string.Empty;        //報表種類
            if (Request.QueryString["type"] != null) type = Request.QueryString["type"].ToString();
            switch (type)
            {
                case "receiver":    //收貨人資料
                    String str_rec_cuscode = string.Empty;   //客代編號
                    String str_rec_code = string.Empty;//收貨人編號
                    String str_rec_name = string.Empty;//收貨人姓名

                    if (Request.QueryString["rec_cuscode"] != null) str_rec_cuscode = Request.QueryString["rec_cuscode"].ToString();
                    if (Request.QueryString["rec_code"] != null) str_rec_code = Request.QueryString["rec_code"].ToString();
                    if (Request.QueryString["rec_name"] != null) str_rec_name = Request.QueryString["rec_name"].ToString();

                    DBToExcel_receiver(str_rec_cuscode,str_rec_code, str_rec_name);
                    break;
                default:
                    break;
            }

        }

    }

    /// <summary>
    /// 匯出Excel檔FromDB
    /// </summary>
    /// <param name="rec_cuscode">客代編號</param>
    /// <param name="rec_code">收貨人編號</param>
    /// <param name="rec_name">收貨人姓名</param>
    protected void DBToExcel_receiver(string str_rec_cuscode, string  rec_code, string rec_name)
    {      
        String str_retuenMsg = string.Empty;
        try
        {
           
            lbMsg.Items.Clear();
            #region 撈DB寫入EXCEL
            string sheet_title = "收貨人";
            string file_name = "收貨人" + DateTime.Now.ToString("yyyyMMdd");

            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();
                #region 關鍵字

                if (str_rec_cuscode != "")
                {
                    cmd.Parameters.AddWithValue("@customer_code", str_rec_cuscode);
                    strWhereCmd += " and customer_code like '%'+@customer_code+'%' ";
                }

                if (rec_code != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_code", rec_code);
                    strWhereCmd += " and receiver_code like '%'+@receiver_code+'%' ";                   
                }

                if (rec_name != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_name", rec_name);
                    strWhereCmd += " and receiver_name like '%'+@receiver_name+'%' ";                    
                }
                #endregion

                cmd.CommandText = string.Format(@"SELECT customer_code '客戶代碼', receiver_code '收貨人代碼',receiver_name '收貨人', tel '電話1', tel_ext '分機1',tel2 '電話2'
                                                         ,address_city '地址-縣市' ,address_area  '地址-鄉鎮區', address_road '地址-內容'
                                                         ,memo  '備註'
                                                    FROM tbReceiver With(Nolock) 
                                                   WHERE 1 = 1  {0} order by receiver_code", strWhereCmd);

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        using (ExcelPackage p = new ExcelPackage())
                        {
                            p.Workbook.Properties.Title = sheet_title;
                            p.Workbook.Worksheets.Add(sheet_title);
                            ExcelWorksheet ws = p.Workbook.Worksheets[1];
                            int colIndex = 1;
                            int rowIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                var fill = cell.Style.Fill;
                                fill.PatternType = ExcelFillStyle.Solid;
                                fill.BackgroundColor.SetColor(Color.LightGray);                             

                                //Setting Top/left,right/bottom borders.
                                var border = cell.Style.Border;
                                border.Bottom.Style =
                                    border.Top.Style =
                                    border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;

                                //Setting Value in cell
                                cell.Value = dc.ColumnName;
                                colIndex++;
                            }
                            rowIndex++;

                            foreach (DataRow dr in dt.Rows)
                            {
                                colIndex = 1;
                                foreach (DataColumn dc in dt.Columns)
                                {
                                    var cell = ws.Cells[rowIndex, colIndex];
                                    cell.Value = dr[dc.ColumnName];                                    
                                    //Setting borders of cell
                                    var border = cell.Style.Border;
                                    border.Left.Style =
                                        border.Right.Style = ExcelBorderStyle.Thin;
                                    colIndex++;
                                }

                                rowIndex++;
                            }

                            ws.View.FreezePanes(2, 1);
                            ws.Cells.Style.Font.Size = 12;
                            ws.Cells.AutoFitColumns();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.BinaryWrite(p.GetAsByteArray());
                            Response.End();

                            str_retuenMsg += "下載完成!";
                        }
                    }
                    else
                    {
                        str_retuenMsg += "查無此資訊，請重新確認!";
                    }

                    //lbMsg.
                }
            }
            #endregion

        }
        catch (Exception ex)
        {
            str_retuenMsg += "系統異常，請洽相關人員!";
            str_retuenMsg += " (" + ex.Message.ToString() + ")";
        }
        finally
        {
            lbMsg.Items.Add(str_retuenMsg);
        }

    }



}