﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/LTMasterSystem.master" CodeFile="LT_RoadSection.aspx.cs" Inherits="LT_RoadSection" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <link href="css/templatemo-styleA.css" rel="stylesheet">
    <style>
        input[type="radio"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                text-align: left;
            }

        ._add_table {
            width: 90%;
        }

        ._add_title {
            width: 9%;
            padding: 5px;
        }

        ._add_data {
            width: 41%;
        }

        ._add_btn {
            text-align: center;
        }

        ._foradd {
            margin: 5px 20px;
        }

        ._setadd {
            right: 10px;
            text-align: right;
        }

        ._hr {
            width: 100%;
            display: inline-table;
        }

        ._btnsh {
            margin: 10px auto;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        td {
            padding: 8px
        }

        table {
            width: 100%
        }

        ._title {
            width: 150px;
            text-align: right;
        }

        .div_title {
            width: 165px;
            text-align: right;
        }

        .form_zip {
            width: 80px !important
        }

        .form_sec {
            width: 40px !important
        }

        .form_num {
            width: 60px !important
        }

        .form_rad {
            width: 60px !important
        }

        .fl {
            float: left
        }

        .pad8 {
            padding: 8px
        }
        .wi100{width:100% }
        .tc{text-align:center}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
           <h2 class="margin-bottom-10"><asp:Label ID="lbl_title" Text="路段維護" runat="server"></asp:Label></h2>
            <asp:Panel ID="pan_list" runat="server" CssClass="col-lg-12 form-inline form-group">
                <div class="row col-lg-6">
                    查詢條件：
                      <asp:TextBox ID="tb_searchzip" runat="server" class="form-control form_zip" placeholder="郵遞區號"></asp:TextBox>
                    <asp:TextBox ID="tb_searchaddress" runat="server" class="form-control" placeholder="地址關鍵字"></asp:TextBox>
                    <asp:TextBox ID="tb_searchpeo" runat="server" class="form-control" placeholder="工號、司機"></asp:TextBox>
                     <asp:DropDownList ID="type" runat="server" CssClass=" form-control" >
                                    <asp:ListItem Value="">號碼類別</asp:ListItem>
                                    <asp:ListItem Value="0">全</asp:ListItem>
                                    <asp:ListItem Value="1">單</asp:ListItem>
                                    <asp:ListItem Value="2">雙</asp:ListItem>
                                </asp:DropDownList>
                    <asp:Button ID="btn_Search" class="btn btn-primary _btnsh" runat="server" Text="查 詢" OnClick="btn_search_Click" />
                </div>
                <div class="row col-lg-6 _setadd">
                    <asp:Button ID="btn_Add" class="btn btn-warning" runat="server" Text="新 增" OnClick="btn_Add_Click"  />
                </div>
                <hr class="_hr" />
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr>
                        <th rowspan="2" class="th_gn">No</th>
                        <th rowspan="2" class="th_gn">郵遞區號</th>
                        <th rowspan="2" class="th_gn">縣市</th>
                        <th class="th_gn">鄉鎮</th>
                        <th class="th_gn">村里厝</th>
                        <th class="th_gn">鄰</th>
                        <th class="th_gn">鄰</th>
                        <th class="th_gn">路街道</th>
                        <th rowspan="2" class="th_gn">段</th>
                        <th class="th_gn">巷</th>
                        <th class="th_gn">巷</th>
                        <th rowspan="2" class="th_gn">全/單/雙(號)</th>
                        <th class="th_gn">號碼</th>
                        <th class="th_gn">號碼</th>
                        <th rowspan="2" class="th_br">所別</th>
                        <th class="th_br">正常</th>
                        <th class="th_br">替代</th>
                        <th class="th_br">到著</th>
                        <th class="th_br">到著</th>
                        <th rowspan="2" class="th_br">工號</th>
                        <th rowspan="2" class="th_br">司機</th>
                        <th class="th_br">MD</th>
                        <th class="th_br">MD</th>
                        <th class="th_br">DD</th>
                        <th class="th_br">DD</th>
                        <th rowspan="2" class="th_br">聯運</th>
                        <th class="th_br">假日</th>
                        <th class="th_br">假日</th>
                        <th rowspan="2" class="th_br">更新人員</th>
                        <th rowspan="2" class="th_br">更新日期</th>
                        <th rowspan="2" class="th_br">功能</th>
                    </tr>
                    <tr>
                        <th class="th_gn">市區</th>
                        <th class="th_gn">工業區</th>
                        <th class="th_gn">起始</th>
                        <th class="th_gn">結束</th>
                        <th class="th_gn">其他</th>
                        <th class="th_gn">起始</th>
                        <th class="th_gn">結束</th>
                        <th class="th_gn">起始</th>
                        <th class="th_gn">結束</th>
                        <th class="th_br">集區</th>
                        <th class="th_br">集區</th>
                        <th class="th_br">配區</th>
                        <th class="th_br">疊貨區</th>
                        <th class="th_br">配區</th>
                        <th class="th_br">疊貨區</th>
                        <th class="th_br">配區</th>
                        <th class="th_br">疊貨區</th>
                        <th class="th_br">配區</th>
                        <th class="th_br">疊貨區</th>
                    </tr>
                  

                    <asp:Repeater ID="list_customer" runat="server"  OnItemCommand="list_customer_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                                <td data-th="郵遞區號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.zip").ToString())%></td>
                                <td data-th="縣市"><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.city").ToString())%></td>
                                <td data-th="鄉鎮"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area").ToString())%></td>
                                   <td data-th="村里厝(工業區)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.village").ToString())%></td>
                                <td data-th="鄰(起)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.neighborS").ToString())%></td>
                                <td data-th="鄰(迄)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.neighborE").ToString())%></td>
                                <td data-th="路街道"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.road").ToString())%></td>
                                <td data-th="段"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.section").ToString())%></td>
                                <td data-th="巷(起)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.laneS").ToString())%></td>
                                <td data-th="巷(迄)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.laneE").ToString())%></td>
                                <td data-th="全/單/雙(號)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.numbertype").ToString())%></td>
                                <td data-th="號碼(起)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NumberS").ToString())%></td>
                                <td data-th="號碼(迄)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NumberE").ToString())%></td>
                                <td data-th="所別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                                <td data-th="正常集區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collecting_area").ToString())%></td>
                                <td data-th="替代集區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collecting_areaR").ToString())%></td>
                                <td data-th="到著配區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_distribution").ToString())%></td>
                                <td data-th="到著集貨區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_stack").ToString())%></td>
                                <td data-th="工號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.emp_code").ToString())%></td>
                                <td data-th="司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dri_name").ToString())%></td>
                                <td data-th="MD配區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MDarrive_distribution").ToString())%></td>
                                <td data-th="MD集貨區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MDarrive_stack").ToString())%></td>
                                <td data-th="DD配區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DDarrive_distribution").ToString())%></td>
                                <td data-th="DD集貨區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DDarrive_stack").ToString())%></td>
                                <td data-th="聯運"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.LanYun").ToString())%></td>
                                <td data-th="假日集貨區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.HDarrive_distribution").ToString())%></td>
                                <td data-th="假日配區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.HDarrive_stack").ToString())%></td>
                                <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.uuser").ToString())%></td>
                                <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td class="td_data td_edit" data-th="編輯">
                                    <asp:HiddenField ID="id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>' />
                                    <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_customer.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="30" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span> 筆
                </div>
            </asp:Panel>

            <asp:Panel ID="pan_form" Visible="false"  runat="server" CssClass="col-lg-12 form-inline form-group">
                <hr />

                <div class="bs-callout bs-callout-info">
                    <h3>1. 路段設定</h3>
                    <table class="tb_edit">
                        <tr class="">
                            <td class="_title">
                                <label class="_tip_important">縣市區域路段</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="Zip" runat="server" CssClass="form-control form_zip" Enabled="false" placeholder="郵遞區號"></asp:TextBox>
                                <asp:DropDownList ID="ddl_City" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="縣市" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="reg_City" runat="server" ControlToValidate="ddl_City" ForeColor="Red" ValidationGroup="validate">請選擇縣市</asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddl_area" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="區域" OnSelectedIndexChanged="Area_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="reg_area" runat="server" ControlToValidate="ddl_area" ForeColor="Red" ValidationGroup="validate">請選擇鄉鎮區</asp:RequiredFieldValidator>


                                <asp:TextBox ID="village" runat="server" CssClass="form-control " placeholder="村里厝/工業區"></asp:TextBox>
                             
                                <asp:TextBox ID="road" runat="server" CssClass="form-control " placeholder="路"></asp:TextBox>
                                <asp:TextBox ID="section" runat="server" CssClass="form-control form_sec" MaxLength="1" placeholder=""></asp:TextBox>&nbsp;段
                               



                            </td>



                        </tr>
                        <tr class="">
                            <td class="_title">
                                <label class="">鄰(區間)</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="neighborS" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                                ～
                             <asp:TextBox ID="neighborE" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="">
                            <td class="_title">
                                <label class="">巷(區間)</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="laneS" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                                ～
                              <asp:TextBox ID="laneE" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="">
                            <td class="_title">
                                <label class="">號碼(區間)</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="NumberS" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                                ～
                              <asp:TextBox ID="NumberE" runat="server" class="form-control form_num" onkeyup="value=value.replace(/[^\d]/g,'')" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>

                        <tr class="">
                            <td class="_title">
                                <label class="_tip_important">單/雙 號</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:RadioButtonList ID="numbertype" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px">
                                    <asp:ListItem Selected="True" Value="0">全</asp:ListItem>
                                    <asp:ListItem Value="1">單</asp:ListItem>
                                    <asp:ListItem Value="2">雙</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>


                    </table>
                </div>

                <div class="bs-callout bs-callout-info">
                    <h3>2. 所別人員</h3>
                    <table class="tb_edit">

                        <tr class="">
                            <td class="_title">
                                <label class="_tip_important">所別</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:DropDownList ID="sta" runat="server" CssClass="form-control" AutoPostBack="true" ToolTip="所別"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="sta" ForeColor="Red" ValidationGroup="validate">請選擇所別</asp:RequiredFieldValidator>

                            </td>
                        </tr>


                        <tr class="">
                            <td class="_title">
                                <label class="_tip_important">工號</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="Emp_code" runat="server" class="form-control "></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="Emp_code" ForeColor="Red" ValidationGroup="validate">請填寫工號</asp:RequiredFieldValidator>
                            </td>
                        </tr>


                        <tr class="">
                            <td class="_title">
                                <label class="_tip_important">司機</label>
                            </td>
                            <td class="_edit_data form-inline ">
                                <asp:TextBox ID="Driver" runat="server" class="form-control "></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server" ControlToValidate="Driver" ForeColor="Red" ValidationGroup="validate">請填寫司機</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="bs-callout bs-callout-info">
                    <h3>3. 其他設定</h3>
                    <div class="row">
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">正常集區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="collecting_area" runat="server" class="form-control"></asp:TextBox>
                              
                              
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">替代集區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="collecting_areaR" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">到著配區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="arrive_distribution" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">到著集貨區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="arrive_stack" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                    </div>


                   <div class="row">
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">MD配區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="MDarrive_distribution" runat="server" class="form-control"></asp:TextBox>
                              
                              
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">MD疊貨區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="MDarrive_stack" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">DD配區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="DDarrive_distribution" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">DD疊貨區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="DDarrive_stack" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                    </div>



                        <div class="row">
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">假日配區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="HDarrive_distribution" runat="server" class="form-control"></asp:TextBox>
                              
                              
                            </div>
                        </div>
                        <div class="fl">
                            <div class="div_title fl pad8">
                                <label class="">假日疊貨區</label>
                            </div>
                            <div class="_edit_data form-inline fl pad8">
                                <asp:TextBox ID="HDarrive_stack" runat="server" class="form-control"></asp:TextBox>
                                               
                            </div>
                        </div>
                     
                    </div>

                </div>
                <div class="wi100 fl">
                   <center>
                   <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick ="btn_OK_Click"   ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                   <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                   </center>
                </div>
            </asp:Panel>
            <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
        </div>
    </div>
</asp:Content>

