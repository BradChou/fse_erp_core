﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMember.master" AutoEventWireup="true" CodeFile="member_3.aspx.cs" Inherits="member_3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 21px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">客戶資料查詢</h2>

            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <asp:TextBox ID="txcustomer_code" class="form-control" runat="server" placeholder="輸入客戶代號"></asp:TextBox>
                    或 
                    <asp:TextBox ID="txcustomer_shortname" class="form-control" runat="server" placeholder="輸入客戶簡稱"></asp:TextBox>
                    <asp:DropDownList ID="PM_code" runat="server" CssClass="form-control" ></asp:DropDownList>
                    <asp:DropDownList ID="Status" runat="server" CssClass="form-control" >
                        <asp:ListItem Value="" >全部</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">營運中</asp:ListItem>
                        <asp:ListItem Value="1">停運</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click"  />

                </div>
                <hr>
                <p class="text-primary">※點選任一客代可查詢及修改</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th>客戶代號</th>
                        <th>負責區配商</th>
                        <th>客戶名稱</th>
                        <th>出貨地址</th>
                        <th>合約到期日</th>
                        <th>更新人員</th>
                        <th>更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-th="客戶代號">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_id").ToString())%>' />
                                    <a href="member_3-edit.aspx?src=member_3.aspx&customer_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_id").ToString())%>" class="fancybox fancybox.iframe btn btn-link " id="updclick"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString()), Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.type").ToString()))%></a>                                    
                                    <%#Server.HtmlDecode((DataBinder.Eval(Container, "DataItem.stop_shipping_code").ToString()) != "0"  ? "<a class='text-danger'>(停運)</a>" : "")%>
                                </td>
                                <td data-th="負責區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_code").ToString())%> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_shortname").ToString())%></td>
                                <td data-th="出貨地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_road").ToString())%></td>
                                <td data-th="合約到期日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.contract_expired_date","{0:yyyy/MM/dd}").ToString())%></td>
                                <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="5" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              
                        <nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>
            </div>
        </div>
    </div>

</asp:Content>

