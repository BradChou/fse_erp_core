﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_2_4.aspx.cs" Inherits="money_2_4" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        ._th {
            text-align: center;
        }
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">B段應付</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-group form-inline">
                                <label id="lbdaterange" runat="server" class="_daterange">請款期間</label>
                                <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate"></asp:TextBox>
                                ~ 
                                <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate"></asp:TextBox>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <label for="Suppliers">供應商</label>
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>

                    <asp:DropDownList ID="dlclose_type" CssClass="form-control" runat="server" OnSelectedIndexChanged="dlclose_type_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="1">1. 已出帳</asp:ListItem>
                        <asp:ListItem Value="2">2. 未出帳</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="帳單查詢" OnClick="btQry_Click" />
                    <asp:Button ID="btClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');" />
                    <asp:Button ID="btExport" runat="server" class="templatemo-white-button " Text="匯出" OnClick="btExport_Click" />
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label id="lbclose" runat="server" class="_close">結帳日</label>
                            <asp:DropDownList ID="dlCloseDay" runat="server" CssClass="form-control _close"></asp:DropDownList>
                            <%--<button type="submit" class="btn btn-primary _close">重寄帳單</button>--%>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlclose_type" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <hr>
                <asp:Label ID="lbSuppliers" runat="server"></asp:Label><br>
                請款期間：<asp:Label ID="lbdate" runat="server"></asp:Label>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="_th">序號</th>
                                <th class="_th">派遣日期</th>
                                <th class="_th">任務編號</th>
                                <th class="_th">供應商</th>
                                <th class="_th">板數</th>
                                <th class="_th">發包價</th>
                            </tr>
                            <asp:Repeater ID="New_List_01" runat="server">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="序號">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                        </td>
                                        <td data-th="派遣日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_date","{0:yyyy/MM/dd}").ToString())%></td>
                                        <td data-th="任務編號"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_id").ToString()))%></td>
                                        <td data-th="供應商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                        <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates","{0:N0}").ToString())%></td>
                                        <td data-th="發包價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>

                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List_01.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="6" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                        <table class="paytable">
                            <tr>
                                <th>小計：</th>
                                <td>NT$ 
                            <asp:Label ID="subtotal_01" runat="server"></asp:Label>
                                    元</td>
                            </tr>
                            <tr>
                                <th>5%營業稅：</th>
                                <td>NT$  
                            <asp:Label ID="tax_01" runat="server"></asp:Label>
                                    元</td>
                            </tr>
                            <tr>
                                <th>應付帳款：</th>
                                <td>NT$   
                            <asp:Label ID="total_01" runat="server"></asp:Label>
                                    元</td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </div>
        </div>
    </div>
</asp:Content>

