﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_1_4.aspx.cs" Inherits="LT_transport_1_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
            });

            init();
            
        });

        function init() {
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: -7, temp
                defaultDate: (new Date())  //預設當日
            });
        }

    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">整批退貨匯入</h2>
            <div class="templatemo-login-form" >
                <div class="form-style-10">
                    <div class="section"><span>1</span>下載空白託運單</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btndownload" class="templatemo-white-button" runat="server" Text="空白託運單" OnClick="btndownload_Click" />
                        <p class="text-primary">※第一次使用請下載空白託運表單標準格式,最新更新日期為2022/10/25</p>
                        <%--<p class="text-danger">※版本更新日期：2018/10/26，若您匯入版本相較於本日期為舊，請先下載空白託運單</p>--%>
                    </div>

                    <div class="section"><span>2</span>上傳檔案</div>
                    <div class="inner-wrap ">
                        <table>
                            <tr>
                               <asp:Label ID="Label2" runat="server" Text="1.請選擇要匯入的客代"></asp:Label><asp:DropDownList ID="dlcustomer_code" runat="server"  CssClass="chosen-select" ></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇要匯入的客代</asp:RequiredFieldValidator>
                                <br />
                                <asp:Label ID="Label4" runat="server" Text="2.請選擇發送日期　　"></asp:Label><asp:TextBox ID="date" runat="server"  CssClass="datepicker"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="date" ForeColor="Red" ValidationGroup="validate">請選擇發送日期</asp:RequiredFieldValidator>
                                <td><asp:Label ID="Label3" runat="server" Text="3.請選擇要匯入的檔案"></asp:Label></td>
                                <td><asp:FileUpload ID="file01" runat="server" Width="300px" /></td>
                                <td><asp:Button ID="btImport" CssClass="templatemo-blue-button" runat="server" Text="確認上傳" ValidationGroup="validate"  OnClick="btImport_Click" /></td>
                            </tr>
                        </table>
                        <div>
                            <div style="overflow: auto; height: 200px; vertical-align: top;">
                                <asp:ListBox ID="lbMsg" runat="server" Height="200px" Width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="section"><span>3</span></div>
                    <div class="inner-wrap" style="overflow: auto; height: calc(40vh); width: 100%" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <p>※今日總筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>
                        <table id="custom_table"  class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>
                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                </th>
                                <th>傳票類別</th> 
                                <th>貨號</th>
                                <th>收件人</th>
                                <th>收件人電話</th>
                                <th>收件人地址</th>
                                <th>板數</th>
                                <th>件數</th>
                                <th>才數</th>
                                <th>指配日期</th>
                                <th>到付運費(含稅)</th>
                                <th>代收貨款</th>
                                <th>到付追加(元付)</th>
                                <th>備註</th>
                            </tr>
                            <asp:repeater id="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="列印">
                                        <asp:CheckBox ID="chkRow" runat="server" />
                                        <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                        <asp:HiddenField ID="hid_print_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%>' />
                                        <asp:HiddenField ID="Hid_print_flag" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_flag").ToString())%>' />
                                        <asp:HiddenField ID="Hid_Supplier_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Supplier_date","{0:yyyy/MM/dd}").ToString())%>' />
                                        </td>
                                        <td data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                        <td data-th="貨號"><asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label></td>
                                        <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                        <td data-th="收件人電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1").ToString())%></td>
                                        <td data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_address").ToString())%></td>
                                        <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                        <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                                        <td data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbm").ToString())%></td>
                                        <td data-th="指配日期"><asp:Label ID="lbarrive_assign_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%>'></asp:Label></td>
                                        <td data-th="到付運費(含稅)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_to_pay_freight").ToString())%></td>
                                        <td data-th="代收貨款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money").ToString())%></td>
                                        <td data-th="到付追加(元付)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_to_pay_append").ToString())%></td>                                      
                                        <td data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.invoice_desc").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="14" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="section"><span>4</span>列印</div>
                     <div class="inner-wrap">
                        <div class="form-group form-inline">
                            <asp:Label ID="Label1" runat="server" Text="紙張格式"></asp:Label>
                            <asp:DropDownList ID="option" runat="server" class="form-control">
                              <asp:ListItem Value="1">退貨-一式二筆黏貼</asp:ListItem>
                               <%-- <asp:ListItem Value="2">A4 (一式1筆託運標籤)</asp:ListItem>
                                <asp:ListItem Value="1">捲筒列印</asp:ListItem>--%>
                            </asp:DropDownList>
                         <%--   <div id="divStartfrom" class="form-group "  >
                                自第<asp:TextBox ID="StartFrom" runat="server"  class="form-control" MaxLength="1" Width="50">1</asp:TextBox>格續印
                            </div>--%>
                            <%--<asp:RangeValidator ID="rv1" runat="server" ControlToValidate="StartFrom" ErrorMessage="需介於 1~6" ForeColor="Red" MaximumValue="6" MinimumValue="1" Type="Integer" ></asp:RangeValidator>--%>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnPirntLabel" CssClass="btn btn-primary" runat="server" Text="列印標籤" OnClick="btnPirntLabel_Click" />
                            <p class="text-primary">※列印標籤時產生貨號</p>
                        </div>
                    </div>
                </div>
            </div>
           <%-- <hr>--%>
        </div>
    </div>
</asp:Content>

