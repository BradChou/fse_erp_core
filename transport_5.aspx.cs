﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using BObject.Bobjects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using BarcodeLib;
using System.Collections.Specialized;
using System.Text;
//using NPOI.HSSF.UserModel;  //-- HSSF 用來產生Excel 2003檔案（.xls）
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;   //-- XSSF 用來產生Excel 2007檔案（.xlsx）
using RestSharp;
using System.Net;

public partial class transport_5 : System.Web.UI.Page
{

    string send_station_scode = "";
    bool check_address = true;
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            #region 
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            manager_type = Session["manager_type"].ToString(); //管理單位類別
            string account_code = Session["account_code"].ToString(); //使用者帳號
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            if (station_level.Equals("1"))
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' and management=@management order by station_scode";
                    cmd.Parameters.AddWithValue("@management", station);
                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }
            else if (station_level.Equals("2"))
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' and station_scode=@station_scode order by station_scode";
                    cmd.Parameters.AddWithValue("@station_scode", station_scode);
                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }
            else if (station_level.Equals("4"))
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' and  station_area=@station_area order by station_scode";
                    cmd.Parameters.AddWithValue("@station_area", station_area);

                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }
            else if (station_level.Equals("5"))
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' order by station_scode";
                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }
            else if (station_level.Equals(""))
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' order by station_scode";
                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }
            else
            {
                //站所別
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = @"select station_scode,'('+station_scode+')' + station_name as station_name from tbStation where station_code <>'F53' and station_code <>'F71' and station_scode=@station order by station_scode";
                    cmd.Parameters.AddWithValue("@station", station);
                    ddlStation.DataSource = dbAdapter.getDataTable(cmd);
                    ddlStation.DataValueField = "station_scode";
                    ddlStation.DataTextField = "station_name";
                    ddlStation.DataBind();
                }
            }



            if (manager_type == "0" || manager_type == "1" || manager_type == "2")
            {
                ddlStation.Items.Insert(0, new ListItem("全部", ""));

            }

            #endregion

            date1.Text = DateTime.Today.ToString("yyyy/MM/dd");
            date2.Text = DateTime.Today.ToString("yyyy/MM/dd");
            cdate1.Text = DateTime.Today.ToString("yyyy/MM/dd");
            cdate2.Text = DateTime.Today.ToString("yyyy/MM/dd");
            lbDate.Text = "建檔日期";
            cdate1.Visible = true;
            cdate2.Visible = true;
            date1.Visible = false;
            date2.Visible = false;



            //readdata();
            //string sScript = "$('.date_picker').datepicker({ " +
            //                 "  dateFormat: 'yy/mm/dd', " +
            //                 "  changeMonth: true, " +

            //                 " });  ";
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        }



    }


    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string request_id = ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim();

        string script = string.Empty;
        switch (e.CommandName)
        {
            case "cmdEdit":
                FillData(request_id);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_01').modal();" + script + "</script>", false);
                break;

            case "cmdCancel":
                Hid_Cancel_request_id.Value = e.CommandArgument.ToString();
                delete_memo.Text = "";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_02').modal();</script>", false);
                break;
        }
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        var ErrStr = string.Empty;
        if (Func.IsNormalDelivery(strkey.Text))
        {
            ErrStr = "此筆已正常配達，無法進行修改";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('編輯失敗:" + ErrStr + "');</script>", false);
            return;
        }

        if (Func.IsCancelled(strkey.Text))
        {
            ErrStr = "此筆已經銷單，無法進行修改";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('編輯失敗:" + ErrStr + "');</script>", false);
            return;
        }
        //檢查地址
        GetAddressParsing(ddlsend_city.SelectedValue.ToString().Trim() + ddlsend_area.SelectedValue.ToString().Trim() + strsend_address.Text.Trim(), lbcustomer_code.Text.Trim(), "1", "1");
        if (check_address == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查地址是否正確');</script>", false);
            return;
        }
        #region 更新    
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@Less_than_truckload", 1);                                       //0：棧板運輸  1：零擔運輸

            //DataTable dts = new DataTable();
            //using (SqlCommand cmd1 = new SqlCommand())
            //{
            //    cmd1.Parameters.AddWithValue("@post_city", hid_receive_city.Value);
            //    cmd1.Parameters.AddWithValue("@post_area", hid_receive_area.Value);
            //    cmd1.CommandText = "select station_code from ttArriveSitesScattered where post_city= @post_city and post_area = @post_area";
            //    dts = dbAdapter.getDataTable(cmd1);
            //}


            //cmd.Parameters.AddWithValue("@area_arrive_code", dts.Rows[0]["station_code"].ToString());             //到著碼
            cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", 0);                               //到站領貨 0:否、1:是


            cmd.Parameters.AddWithValue("@pieces", 1);                                                                          //件數


            cmd.Parameters.AddWithValue("@send_contact", strsend_contact.Text);                              //寄件人
            cmd.Parameters.AddWithValue("@send_tel", strsend_tel.Text.ToString());                           //寄件人電話
            cmd.Parameters.AddWithValue("@send_city", ddlsend_city.SelectedValue.ToString());                //寄件人地址-縣市
            cmd.Parameters.AddWithValue("@send_area", ddlsend_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@send_address", strsend_address.Text);                              //寄件人地址-號街巷弄號
            cmd.Parameters.AddWithValue("@send_station_scode", send_station_scode);                        //發送站         
            cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
            cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
            cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
            cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
            cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.Value);                             //收回品項
            cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //(備註說明)


            cmd.Parameters.AddWithValue("@DeliveryType", "R");

            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", hid_request_id.Value);
            cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);
            dbAdapter.execNonQuery(cmd);
        }
        #endregion

        #region 編輯完刷新
        string wherestr = "";
        string orderby = " order by A.cdate desc";

        using (SqlCommand cmd = new SqlCommand())
        {
            if (ddlType.SelectedValue == "0")
            {
                // wherestr += "and A.invoice_desc not like '%來回件%'"; 退貨
                wherestr += "and  ((A.round_trip = '0' ) or (A.round_trip is null ))";
            }
            else if (ddlType.SelectedValue == "1")
            {
                // wherestr += "and A.invoice_desc like '%來回件%'"; 來回件
                wherestr += "and (A.round_trip = '1' )";
            }


            //如果有貨號忽略站所條件
            if (ddlStation.SelectedValue != "")
            {
                if (check_number.Text == "")
                {
                    cmd.Parameters.AddWithValue("@station_scode", ddlStation.SelectedValue.ToString());
                    wherestr += " and  ts.station_scode = @station_scode";
                }

            }


            if (DropDownList1.SelectedValue == "0")
            {
                wherestr += " and A.print_flag =0";

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and A.check_number = @check_number";
                }
                else if (cdate1.Text != "" && cdate2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                    cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                    wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
                }
            }
            else if (DropDownList1.SelectedValue == "1")
            {
                wherestr += " and A.print_flag =1";
                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and (A.check_number = @check_number or A.send_contact = @check_number)";
                }
                else if (date1.Text != "" && date2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@print_date1", date1.Text);
                    cmd.Parameters.AddWithValue("@print_date2", date2.Text);
                    wherestr += " and ((CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@print_date2,111) ) " +
                                " or (A.print_date IS NULL and CONVERT(VARCHAR,A.cdate,111)  > CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@print_date2,111) )) ";
                }
            }
            else
            {
                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and A.check_number = @check_number";
                }

                else if (cdate1.Text != "" && cdate2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                    cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                    wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
                }
            }


            cmd.Parameters.AddWithValue("@DeliveryType", "R");
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            cmd.CommandText = string.Format(@"select 
A.request_id,
A.cdate, 
ts.station_scode arrive_assign_date, 
A.supplier_date,
A.print_date,
A.print_flag, 
A.check_number 
,A.order_number																								
,case isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') when '' then A.send_contact
	else
 isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') end send_contact   --退貨人
 , A.send_city		--退貨人(縣/市)
 , A.send_area		--退貨人(區)
,  A.send_address	--退貨人(地址)
, send_tel		--退貨人電話
,A.receive_contact																						--收貨人
,A.receive_tel1																						    --收貨人電話
,ISNULL(A.receive_city,'') + ISNULL(A.receive_area,'') + ISNULL(A.receive_address,'')receive_address	--收貨人地址
,_Arr.arrive_state
,_Arr.newest_driver_code +' '+ td.driver_name newest_driver_code -- 取貨人
,_arr.arrive_item  arrive_item -- '配送異常說明
,A.invoice_desc
--,case when A.invoice_desc  not like '%來回件%' then '退貨' else '來回件' end returntype
,case when A.round_trip  = '1' then '來回件' else '退貨'end returntype
,A.supplier_date
from tcDeliveryRequests A With(Nolock)
                                    LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
								    left join tbStation ts With(Nolock)  on  ta.station_code = ts.station_scode 
                                    
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
left join tbdrivers td with(nolock)  on  td.driver_code = _arr.newest_driver_code
left join tbItemCodes B on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
where 1= 1
and A.Less_than_truckload = @Less_than_truckload
--and A.invoice_desc not like '%來回件%' --濾掉momo來回件逆物流
and (((A.receive_contact like '%富邦%') and (A.invoice_desc  not like '%來回件%') )or (A.receive_contact not like '%富邦%'))  --濾掉momo來回件逆物流
and A.DeliveryType =@DeliveryType
AND (A.cancel_date is null)
{0} {1}
", wherestr, orderby);
            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
        }
        #endregion
        UpdatePanel2.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('編輯完成!!');$('#modal-default_01').modal('hide');</script>", false);

    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string strErr = string.Empty;

        //有貨態不能銷單
        SqlCommand cmd3 = new SqlCommand();
        DataTable dt3;
        cmd3.CommandText = string.Format("select latest_scan_item , S.receive_option from tcDeliveryRequests R left join ttDeliveryScanLog S with(nolock) on R.check_number = S.check_number and r.latest_pick_up_scan_log_id = s.log_id where request_id = " + Hid_Cancel_request_id.Value + " ");
        dt3 = dbAdapter.getDataTable(cmd3);

        if (dt3.Rows[0]["latest_scan_item"].ToString() != "")
        {
            if (dt3.Rows[0]["latest_scan_item"].ToString() == "5" && dt3.Rows[0]["receive_option"].ToString() == "20")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此單已經有掃讀貨態，無法進行銷單');</script>", false);
                return;
            }
            else if (dt3.Rows[0]["latest_scan_item"].ToString() != "5")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此單已經有掃讀貨態，無法進行銷單');</script>", false);
                return;
            }

        }

        //更新 tcDeliveryRequests 銷單日期
        SqlCommand cmd2 = new SqlCommand();
        cmd2.Parameters.AddWithValue("@cancel_date", DateTime.Today);
        cmd2.Parameters.AddWithValue("@return_check_number", DBNull.Value);
        cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", Hid_Cancel_request_id.Value);
        cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);   //修改
        try
        {
            dbAdapter.execNonQuery(cmd2);
        }
        catch (Exception ex)
        {

            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "銷單:" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }
        ////寫入銷單資料表
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_DeliveryRequesRecord";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@request_id", Hid_Cancel_request_id.Value);
            cmd.Parameters.AddWithValue("@action", "R");
            cmd.Parameters.AddWithValue("@delete_date", DateTime.Today);
            cmd.Parameters.AddWithValue("@delete_user", Session["account_code"].ToString());
            cmd.Parameters.AddWithValue("@memo", delete_memo.Text);
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {

                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "銷單:" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");
            }
        }

        #region 編輯完刷新
        string wherestr = "";
        string orderby = " order by A.cdate desc";

        using (SqlCommand cmd = new SqlCommand())
        {
            if (ddlType.SelectedValue == "0")
            {
                //wherestr += "and A.invoice_desc not like '%來回件%'"; 退貨
                wherestr += "and  ((A.round_trip = '0' ) or (A.round_trip is null ))";
            }
            else if (ddlType.SelectedValue == "1")
            {
                //wherestr += "and A.invoice_desc like '%來回件%'"; 來回件
                wherestr += "and (A.round_trip = '1' )";
            }

            if (ddlStation.SelectedValue != "")
            {
                if (check_number.Text == "")
                {
                    cmd.Parameters.AddWithValue("@station_scode", ddlStation.SelectedValue.ToString());
                    wherestr += " and  ts.station_scode = @station_scode";
                }

            }


            if (DropDownList1.SelectedValue == "0")
            {
                wherestr += " and A.print_flag =0";

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and A.check_number = @check_number";
                }
                else if (cdate1.Text != "" && cdate2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                    cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                    wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
                }
            }
            else if (DropDownList1.SelectedValue == "1")
            {
                wherestr += " and A.print_flag =1";
                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and (A.check_number = @check_number or A.send_contact = @check_number)";
                }
                else if (date1.Text != "" && date2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@print_date1", date1.Text);
                    cmd.Parameters.AddWithValue("@print_date2", date2.Text);
                    wherestr += " and ((CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@print_date2,111) ) " +
                                " or (A.print_date IS NULL and CONVERT(VARCHAR,A.cdate,111)  > CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@print_date2,111) )) ";
                }
            }
            else
            {
                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    wherestr += " and A.check_number = @check_number";
                }

                else if (cdate1.Text != "" && cdate2.Text != "")
                {
                    cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                    cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                    wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
                }
            }


            cmd.Parameters.AddWithValue("@DeliveryType", "R");
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            cmd.CommandText = string.Format(@"select 
A.request_id,
A.cdate, 
ts.station_scode arrive_assign_date, 
A.supplier_date,
A.print_date,
A.print_flag, 
A.check_number 
,A.order_number																								
,case isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') when '' then A.send_contact
	else
 isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') end send_contact   --退貨人
 , A.send_city		--退貨人(縣/市)
 , A.send_area		--退貨人(區)
,  A.send_address	--退貨人(地址)
, send_tel		--退貨人電話
,A.receive_contact																						--收貨人
,A.receive_tel1																						    --收貨人電話
,ISNULL(A.receive_city,'') + ISNULL(A.receive_area,'') + ISNULL(A.receive_address,'')receive_address	--收貨人地址
,_Arr.arrive_state
,_Arr.newest_driver_code
,_arr.arrive_item  arrive_item -- '配送異常說明
,A.invoice_desc
--,case when A.invoice_desc  not like '%來回件%' then '退貨' else '來回件' end returntype
,case when A.round_trip  = '1' then '來回件' else '退貨'end returntype
,A.supplier_date
from tcDeliveryRequests A With(Nolock)
                                    LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
								    left join tbStation ts With(Nolock)  on  ta.station_code = ts.station_scode 
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
left join tbItemCodes B on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
where 1= 1
and A.Less_than_truckload = @Less_than_truckload
--and A.invoice_desc not like '%來回件%' --濾掉momo來回件逆物流
and (((A.receive_contact like '%富邦%') and (A.invoice_desc  not like '%來回件%') )or (A.receive_contact not like '%富邦%'))  --濾掉momo來回件逆物流
and A.DeliveryType =@DeliveryType
AND (A.cancel_date is null)
{0} {1}
", wherestr, orderby);
            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
        }
        #endregion

        UpdatePanel2.Update();
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('銷單完成!!');$('#modal-default_02').modal('hide');</script>", false);
    }
    protected void Check_Clicked(object sender, EventArgs e)
    {

        for (int i = 0; i <= New_List.Items.Count - 1; i++)
        {
            CheckBox cbSel = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
            cbSel.Checked = cbAll.Checked;
        }
    }

    protected void FillData(string request_id)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        #region 郵政縣市
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select * from tbPostCity order by seq asc ";
            ddlsend_city.DataSource = dbAdapter.getDataTable(cmd);
            ddlsend_city.DataValueField = "city";
            ddlsend_city.DataTextField = "city";
            ddlsend_city.DataBind();
            ddlsend_city.Items.Insert(0, new ListItem("請選擇", ""));

            ddlsend_area.Items.Insert(0, new ListItem("請選擇", ""));
            ddlzip.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion

        try
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@request_id", request_id);

                cmd.CommandText = string.Format(@"
        select S.*,A2.zip from (
        select A.request_id 
        , A.customer_code							      
        ,isnull((select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = A.send_contact),A.check_number) oldcheck_number	        --原貨號
        ,ts.station_scode	
        ,A.supplier_code 	      
        ,case isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') when '' then A.send_contact
        	else
         isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact  order by cdate desc),'') end send_contact   --退貨人
         , A.send_city		--退貨人(縣/市)
         , A.send_area		--退貨人(區)
        ,  A.send_address	--退貨人(地址)
        , send_tel		--退貨人電話
        ,A.receive_contact																						--收貨人
        ,A.receive_tel1																						    --收貨人電話
        ,ISNULL(A.receive_city,'') + ISNULL(A.receive_area,'') + ISNULL(A.receive_address,'') receive_address	--收貨人地址
        ,A.invoice_desc							                                                                                            --備註
        ,A.invoice_memo							                                                                                            --備註
        from tcDeliveryRequests A With(Nolock)
        LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
        left join tbStation ts With(Nolock)  on  ta.station_code = ts.station_scode 
        where A.request_id = @request_id
        ) S left join [ttArriveSites] A2 With(Nolock) on A2.post_city = S.send_city and A2.post_area = S.send_area");
                dt = dbAdapter.getDataTable(cmd);
            }

            if (dt.Rows.Count > 0)
            {
                lbstation_scode.Text = dt.Rows[0]["station_scode"].ToString().Replace("F", "");       //流水號
                hid_request_id.Value = dt.Rows[0]["request_id"].ToString();             //流水號
                lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                strkey.Text = dt.Rows[0]["oldcheck_number"].ToString();
                lbreceive_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                lbreceive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();
                lbreceive_address.Text = dt.Rows[0]["receive_address"].ToString();
                invoice_desc.InnerText = dt.Rows[0]["invoice_desc"].ToString();
                invoice_memo.InnerText = dt.Rows[0]["invoice_memo"].ToString();
                strsend_contact.Text = dt.Rows[0]["send_contact"].ToString();
                strsend_tel.Text = dt.Rows[0]["send_tel"].ToString();
                ddlsend_city.SelectedValue = dt.Rows[0]["send_city"].ToString();
                city_SelectedIndexChanged(null, null);
                ddlsend_area.SelectedValue = dt.Rows[0]["send_area"].ToString();
                area_SelectedIndexChanged(null, null);
                strsend_address.Text = dt.Rows[0]["send_address"].ToString();

                hid_receive_city.Value = dt.Rows[0]["send_city"].ToString();
                hid_receive_area.Value = dt.Rows[0]["send_area"].ToString();
                hid_receive_address.Value = dt.Rows[0]["send_address"].ToString();

            }
        }
        catch (Exception ex)
        {
            //_fun.Log(string.Format("【{0} Error】DataBind:{1}", "乘客帳號管理", ex.Message), "W");
        }
    }


    protected void search_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string wherestr = "";
        string orderby = " order by A.cdate desc";

        SqlCommand cmd = new SqlCommand();


        if (ddlType.SelectedValue == "0")
        {
            // wherestr += "and (A.invoice_desc not like '%來回件%' and A.supplier_code ='F35' or A.supplier_code <>'F35')";
            wherestr += "and  ((A.round_trip = '0' ) or (A.round_trip is null ))";
            wherestr += " and (A.check_number not like '98%')";
            wherestr += " and (A.check_number not like '202%' and send_contact not like '201%') ";

        }
        else if (ddlType.SelectedValue == "1")
        {
            //wherestr += "and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
            wherestr += "and (A.round_trip = '1' )";
        }
        else if (ddlType.SelectedValue == "2")
        {
            //wherestr += "and (A.invoice_desc like '%來回件%' AND A.supplier_code ='F35')";
            wherestr += "and  ((A.round_trip = '0' ) or (A.round_trip is null )) ";
            wherestr += " and ((A.check_number like '98%') or (A.check_number like '202%'  and send_contact like '201%')) ";
        }
        string management = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();
        if (ddlStation.SelectedValue != "")
        {
            if (check_number.Text == "")
            {
                cmd.Parameters.AddWithValue("@station_scode", ddlStation.SelectedValue.ToString());
                wherestr += " and   F.station_scode = @station_scode";
            }

        }
        else
        {
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";

            if (check_number.Text == "")
            {
                if (station_level.Equals("1"))
                {
                    cmd.Parameters.AddWithValue("@management", management);

                    cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            managementList += "'";
                            managementList += dt1.Rows[i]["station_scode"].ToString();
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }

                    wherestr += " and F.management in (" + managementList + ")";
                }
                else if (station_level.Equals("2"))
                {
                    cmd.Parameters.AddWithValue("@station_scode", station_scode);
                    cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_scodeList += "'";
                            station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }

                    wherestr += " and   F.station_scode in (" + station_scodeList + ")";
                }
                else if (station_level.Equals("4"))
                {
                    cmd.Parameters.AddWithValue("@station_area", station_area);

                    cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_areaList += "'";
                            station_areaList += dt1.Rows[i]["station_scode"].ToString();
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }
                    wherestr += " and   F.station_area in (" + station_areaList + ")";
                }
                else if (station_level.Equals("") || station_level.Equals("5"))
                {

                }
                else
                {
                    cmd.Parameters.AddWithValue("@station_scode", station_scode);
                    wherestr += " and   F.station_scode = @station_scode";
                }
            }

        }

        if (DropDownList2.SelectedValue == "0")
        {
            wherestr += " and  A.ship_date is null";
        }
        else if (DropDownList2.SelectedValue == "1")
        {
            wherestr += " and  A.ship_date is not null";
        }


        if (DropDownList1.SelectedValue == "0")
        {
            wherestr += " and A.print_flag =0";

            if (check_number.Text != "")
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                wherestr += " and A.check_number = @check_number";
            }
            else if (cdate1.Text != "" && cdate2.Text != "")
            {
                cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
            }
        }
        else if (DropDownList1.SelectedValue == "1")
        {
            wherestr += " and A.print_flag =1";
            if (check_number.Text != "")
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                wherestr += " and (A.check_number = @check_number or A.send_contact = @check_number)";
            }
            else if (date1.Text != "" && date2.Text != "")
            {
                cmd.Parameters.AddWithValue("@print_date1", date1.Text);
                cmd.Parameters.AddWithValue("@print_date2", date2.Text);
                wherestr += " and ((CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@print_date2,111) ) " +
                            " or (A.print_date IS NULL and CONVERT(VARCHAR,A.cdate,111)  > CONVERT(VARCHAR,@print_date1,111) and CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@print_date2,111) )) ";
            }
        }
        else
        {
            if (check_number.Text != "")
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                wherestr += " and A.check_number = @check_number";
            }

            else if (cdate1.Text != "" && cdate2.Text != "")
            {
                cmd.Parameters.AddWithValue("@cdate1", cdate1.Text);
                cmd.Parameters.AddWithValue("@cdate2", cdate2.Text);
                wherestr += " and CONVERT(VARCHAR,A.cdate,111)  >= CONVERT(VARCHAR,@cdate1,111) and  CONVERT(VARCHAR,A.cdate,111)  <= CONVERT(VARCHAR,@cdate2,111)";
            }
        }


        cmd.Parameters.AddWithValue("@DeliveryType", "R");
        cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

        cmd.CommandText = string.Format(@"select 
A.request_id,
A.cdate, 
ts.station_scode arrive_assign_date, 
F.station_scode ,
F.station_name,
A.supplier_date,
A.print_date,
A.print_flag, 
A.check_number 
,A.order_number																								
,case isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact order by cdate desc),'') when '' then A.send_contact
	else
 isnull((select top 1 Receive_contact from tcDeliveryRequests with(nolock) where check_number = A.send_contact order by cdate desc),'') end send_contact   --退貨人
 , A.send_city		--退貨人(縣/市)
 , A.send_area		--退貨人(區)
,  A.send_address	--退貨人(地址)
, send_tel		--退貨人電話
,A.receive_contact																						--收貨人
,A.receive_tel1																						    --收貨人電話
,ISNULL(A.receive_city,'') + ISNULL(A.receive_area,'') + ISNULL(A.receive_address,'')receive_address	--收貨人地址
,_Arr.arrive_state
,_Arr.newest_driver_code + ' ' +td.driver_name newest_driver_code -- 取貨人
--,_arr.arrive_item  arrive_item -- '配送異常說明

,case latest_scan_item  when '5' then isnull(TB.code_name,'') 
                       when '3' then isnull(_arr.arrive_item,'')
					   when '4' then isnull(_arr.arrive_item,'')
end  arrive_item  -- 區分
,A.invoice_desc
--case when A.invoice_desc  not like '%來回件%' and A.supplier_code ='F35' then '退貨' 
--when A.invoice_desc like '%來回件%' and A.supplier_code ='F35' then '來回件'
--else '退貨' end returntype
,case when A.round_trip  = '1' then '來回件' else '退貨'end returntype
,A.supplier_date
,case T.receive_option when TB.code_id then TB.code_name end  receive_option  --'集貨異常情形'
from tcDeliveryRequests A With(Nolock)
                                    LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
								    left join tbStation F With(Nolock)  on  F.station_scode = ta.station_code 
								    left join tbStation ts With(Nolock)  on  ts.station_code = A.supplier_code 
                                    
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
left join tbdrivers td with(nolock)  on  td.driver_code = _arr.newest_driver_code
left join tbItemCodes B on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and t.scan_item = '5' and A.latest_scan_date = T.scan_date
left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
where 1= 1
and A.Less_than_truckload = @Less_than_truckload
--and A.invoice_desc not like '%來回件%' --濾掉momo來回件逆物流
and (((A.receive_contact like '%富邦%') and (A.invoice_desc  not like '%來回件%') )or (A.receive_contact not like '%富邦%')or ((A.receive_contact like '%富邦%') and (A.invoice_desc  like '%來回件%') and (A.check_number like '98%')))  --濾掉momo來回件逆物流,98單不濾掉
and A.DeliveryType =@DeliveryType

AND (A.cancel_date is null)
{0} {1}", wherestr, orderby);
        DataTable dt = dbAdapter.getDataTable(cmd);
        New_List.DataSource = dt;
        New_List.DataBind();
        //ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    protected void btnPirntLabel_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string check_number = string.Empty;
        string print_requestid = string.Empty;
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                Boolean Update = false;
                string print_date = ((Label)New_List.Items[i].FindControl("lbprint_date")).Text;
                string Supplier_date = ((Label)New_List.Items[i].FindControl("lbSupplier_date")).Text;
                string arrive_assign_date = ((HiddenField)New_List.Items[i].FindControl("Hid_arrive_assign_date")).Value;
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));

                if ((chkRow != null) && (chkRow.Checked) && (lbcheck_number.Text != ""))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    print_requestid += "'" + id + "',";
                    check_number += "'" + lbcheck_number.Text + "',";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        #region 發送日期
                        if (print_date == "")
                        {
                            cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日                                
                            Update = true;
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@print_date", print_date);                  //發送日                                
                            Update = true;
                        }
                        #endregion
                        cmd.Parameters.AddWithValue("@print_flag", "1");                           //是否列印
                        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                        cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                        dbAdapter.execNonQuery(cmd);
                    }
                }
            }
        }
        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);


        if (print_requestid == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的託運單');</script>", false);
            return;
        }
        else
        {
            Boolean IsChkOk = false;
            if (print_requestid.Length > 0) IsChkOk = true;
            string url = "";
            if (IsChkOk)
            {
                NameValueCollection nvcParamters = new NameValueCollection();
                nvcParamters["ids"] = print_requestid.Replace("'", "");

                //一筆6式
                if (option.SelectedValue == "1")
                {
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1";
                    //url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1_post";
                    nvcParamters["position"] = Position.SelectedItem.Value;
                }
                //捲筒
                else if (option.SelectedValue == "2")
                {
                    //捲筒
                    //PrintReelLabel(print_requestid);
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0";
                }
                //一筆兩式
                else if (option.SelectedValue == "0")
                {
                    url = "https://erp.fs-express.com.tw/DeliveryRequest/PrintReturnTagByIds2_0";
                }




                string strForm = PreparePOSTForm(url, nvcParamters);

                Response.Clear();

                Response.Write(strForm);
                Response.End();

                //                using (SqlCommand cmd = new SqlCommand())
                //                {
                //                    string wherestr = " and A1.request_id in(" + print_requestid + ")";
                //                    //cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                //                    //cmd.Parameters.AddWithValue("@starData", date1.Text);
                //                    //cmd.Parameters.AddWithValue("@endData", date2.Text);
                //                    cmd.CommandText = string.Format(@"
                //                                     select 
                //case when A1.supplier_code = 'F35' then 
                //(select station_scode from tbStation A inner join ttArriveSitesScattered B on B.station_code = A.station_scode where B.post_city=A1.Receive_city and B.post_area = A1.Receive_area )
                //when A1.supplier_code <> 'F35' then ts.station_scode end  
                //                                     area_arrive_code --A1.area_arrive_code	--零擔到著簡碼 
                //									,convert(nvarchar(4),A1.print_date,120) Print_year
                //									,SUBSTRING(convert(nvarchar(10),A1.print_date,120),6,2 ) Print_Mon
                //									,SUBSTRING(convert(nvarchar(10),A1.print_date,120),9,2 ) Print_Day
                //                                    --,print_date			--通知取件時間
                //                                    ,RTRIM(A1.Order_number)	Orderbarcode	--退貨單號
                //                                    , A1.Order_number
                //                                    ,A1.Receive_contact    --收件人
                //                                    ,A1.Receive_city + A1.Receive_area + A1.receive_address	as Reveiveaddress	--收件人地址 
                //                                    ,isnull(A2.Receive_contact,A1.Send_contact) Send_contact		--寄件人
                //                                    ,A1.Send_city + A1.Send_area+ A1.Send_address  as Sendaddress --寄件人地址
                //                                    ,RTRIM(A1.check_number)  as barcode   --條碼
                //                                    ,A1.Invoice_desc			--備註
                //                                    ,A1.Invoice_memo			--備註
                //                                    ,A1.Check_number			--查貨單號
                //                                    ,A1.Send_tel 			--寄件人電話 
                //                                    ,A1.receive_tel1 		--收件人電話 
                //                                    ,A1.pieces 			--件數
                //									,(select top 1 ts.station_scode  from ttArriveSitesScattered ta left join tbStation ts on ta.station_code = ts.station_scode
                //									where ta.post_city = A1.Send_city and ta.post_area =  A1.Send_area) send_arrive_code --發送站碼
                //                                    ,(select top 1 td.driver_name from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number = A1.check_number order by tds.cdate desc) driver_name
                //									,(select top 1 td.driver_mobile from ttDeliveryScanLog tds inner join tbDrivers td on tds.driver_code = td.driver_code  where check_number =  A1.check_number order by tds.cdate desc) driver_mobile
                //                                    from tcDeliveryRequests A1 with(nolock)
                //                                    left join  tbStation ts on ts.station_code = A1.supplier_code
                //                                    Left join tcDeliveryRequests A2 on A1.send_contact = A2.check_number
                //                                    AND ( A1.cancel_date is null)
                //                                    where 1=1  {0}", wherestr);

                //                    DataTable DT = dbAdapter.getDataTable(cmd);
                //                    if (DT != null)
                //                    {
                //                        DataTable dt2 = new DataTable();
                //                        dt2 = DT.Clone();  //複製DT的結構
                //                        for (int i = 0; i <= DT.Rows.Count - 1; i++)
                //                        {
                //                            DT.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["barcode"].ToString()));
                //                            if (DT.Rows[i]["Orderbarcode"].ToString() != "") { 
                //                            DT.Rows[i]["Orderbarcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["Orderbarcode"].ToString()));
                //                            }
                //                            dt2.ImportRow(DT.Rows[i]);
                //                        }
                //                        string[] ParName = new string[0];
                //                        string[] ParValue = new string[0];
                //                        if (option.SelectedValue == "1")
                //                        {
                //                            PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "Report2", ParName, ParValue, "ReturnsData", dt2);
                //                        }
                //                        else
                //                        {
                //                            PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "SpeedReturnReport", ParName, ParValue, "ReturnsData", dt2);
                //                        }
                //                    }
                //                }
            }
        }
    }
    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {
            oStream.Dispose();
        }
    }
    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {
        //Barcode test = new Barcode();
        //test. 
        using (var barcode = new Barcode()
        {

            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 530,
            Height = 100,
            LabelFont = new Font("verdana", 20f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            try
            {
                return barcode.Encode(TYPE.CODE128, content);
            }
            catch (Exception ex)
            {
                string errstr = ex.Message;
                return null;
            }

        }
    }


    protected void btPrint_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string check_number = string.Empty;
        string print_requestid = "";
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                Boolean Update = false;
                string print_date = ((Label)New_List.Items[i].FindControl("lbprint_date")).Text;
                string Supplier_date = ((Label)New_List.Items[i].FindControl("lbSupplier_date")).Text;
                //string arrive_assign_date = ((HiddenField)New_List.Items[i].FindControl("Hid_arrive_assign_date")).Value;
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));

                if ((chkRow != null) && (chkRow.Checked) && (lbcheck_number.Text != ""))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    Label lbprint_date = ((Label)New_List.Items[i].FindControl("lbprint_date"));

                    if (lbprint_date.Text == "")
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            #region 發送日期
                            if (print_date == "")
                            {
                                cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日                                
                                Update = true;
                            }
                            #endregion

                            #region  配送日期
                            if (Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(print_date);

                                //if (arrive_assign_date != "")
                                //{
                                //    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                //}
                                //else
                                //{
                                //配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
                                int Week = (int)Convert.ToDateTime(print_date).DayOfWeek;

                                if (Convert.ToDateTime(print_date).ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

                                supplier_date = Convert.ToDateTime(print_date);
                                switch (Week)
                                {
                                    case 5:
                                        supplier_date = supplier_date.AddDays(3);
                                        break;
                                    case 6:
                                        supplier_date = supplier_date.AddDays(2);
                                        break;
                                    default:
                                        supplier_date = supplier_date.AddDays(1);
                                        break;
                                }
                                while (Utility.IsHolidays(supplier_date))
                                {
                                    supplier_date = supplier_date.AddDays(1);         // temp
                                }

                                //if (supplier_date == Convert.ToDateTime("2017/10/04"))
                                //{
                                //    supplier_date = supplier_date.AddDays(1);         // temp
                                //}
                                //}


                                cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            #endregion
                            if (Update == true)
                            {
                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                                dbAdapter.execNonQuery(cmd);
                            }
                        }
                    }
                    else
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            #region 發送日期
                            if (print_date == "")
                            {
                                cmd.Parameters.AddWithValue("@print_date", lbprint_date);                  //發送日                                
                                Update = true;
                            }
                            #endregion

                            #region  配送日期
                            if (Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(print_date);

                                //if (arrive_assign_date != "")
                                //{
                                //    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                //}
                                //else
                                //{
                                //配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
                                int Week = (int)Convert.ToDateTime(print_date).DayOfWeek;

                                if (Convert.ToDateTime(print_date).ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

                                supplier_date = Convert.ToDateTime(print_date);
                                switch (Week)
                                {
                                    case 5:
                                        supplier_date = supplier_date.AddDays(3);
                                        break;
                                    case 6:
                                        supplier_date = supplier_date.AddDays(2);
                                        break;
                                    default:
                                        supplier_date = supplier_date.AddDays(1);
                                        break;
                                }
                                while (Utility.IsHolidays(supplier_date))
                                {
                                    supplier_date = supplier_date.AddDays(1);         // temp
                                }

                                //if (supplier_date == Convert.ToDateTime("2017/10/04"))
                                //{
                                //    supplier_date = supplier_date.AddDays(1);         // temp
                                //}
                                //}


                                cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            #endregion
                            if (Update == true)
                            {
                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                                dbAdapter.execNonQuery(cmd);
                            }
                        }

                    }


                    check_number += "'" + lbcheck_number.Text + "',";
                    print_requestid += id.ToString() + ",";
                }

            }
        }
        if (check_number != "") check_number = check_number.Substring(0, check_number.Length - 1);
        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);
        if (check_number == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的託運單');</script>", false);
            return;
        }
        else
        {
            if (ExcelFile.Checked == true)
            {
                DBToExcel(check_number);
            }
            else
            {
                NameValueCollection nvcParamters = new NameValueCollection();
                nvcParamters["ids"] = print_requestid;
                string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintConsignmentSummaryPDFByIds";

                string strForm = PreparePrintDeliverySummaryCommitForm(url, nvcParamters);

                //Page.Controls.Add(new LiteralControl(strForm));
                Response.Clear();

                Response.Write(strForm);
                Response.End();
            }

        }

    }
    private static String PreparePrintDeliverySummaryCommitForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    protected void DBToExcel(string check_number)
    {

        String str_retuenMsg = string.Empty;
        try
        {
            Boolean IsChkOk = false;
            if (check_number.Length > 0) IsChkOk = true;
            #region 撈DB寫入EXCEL
            if (IsChkOk)
            {
                if (Less_than_truckload == "0")
                {
                    string sheet_title = "託運總表";
                    string file_name = "DeliveryReport" + DateTime.Now.ToString("yyyyMMdd");

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        if (check_number != "")
                        {
                            // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                            string wherestr = " and check_number in(" + check_number + ")";
                            string orderby = " order by  A.check_number";
                            //if (Suppliers.SelectedValue + Customer.SelectedValue == "0000049") orderby = " order by  A.cdate"; //三陽工業，指定依打單的順序列印

                            cmd.CommandText = string.Format(@"Select Convert(varchar,A.print_date,111)  '發送日期', Convert(varchar,A.supplier_date,111)   '配送日期' , A.check_number '貨號' , 
                                                          C.code_name  '計價模式' ,A.pieces '件數' , A.plates '板數', A.cbm '才數',
                                                          A.send_contact '寄件人',  
                                                          A.receive_contact '收件人',  
										                  A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
                                                          A.receive_tel1 '收件人電話',  
										                  A.collection_money '代收金',A.arrive_to_pay_freight '到付運費',A.arrive_to_pay_append'到付追加', 
                                                          B.code_name '付款別',
                                                          A.invoice_desc '備註'  
                                                          from tcDeliveryRequests A with(nolock)
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                            using (DataTable dt = dbAdapter.getDataTable(cmd))
                            {
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    MemoryStream ms = new MemoryStream();
                                    //HSSFWorkbook workbook = new HSSFWorkbook(); // .xls
                                    IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）
                                    ISheet sheet = null;
                                    IRow headerRow = null;
                                    int rowIndex = 0;
                                    if (string.IsNullOrEmpty(sheet_title))
                                    {
                                        //沒有Table Name以預設方式命名Sheet
                                        sheet = workbook.CreateSheet();
                                    }
                                    else
                                    {
                                        //有Table Name以Table Name命名Sheet
                                        sheet = workbook.CreateSheet(sheet_title);
                                    }
                                    headerRow = sheet.CreateRow(rowIndex++);

                                    //自動換行
                                    //csHeader.WrapText = true;

                                    #region 處理標題列
                                    foreach (DataColumn column in dt.Columns)
                                    {
                                        headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                                    }
                                    #endregion

                                    #region 處理資料列
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        IRow dataRow = sheet.CreateRow(rowIndex++);

                                        foreach (DataColumn column in dt.Columns)
                                        {
                                            dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));
                                        }
                                    }
                                    #endregion

                                    workbook.Write(ms);
                                    Response.Clear();
                                    //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx", file_name));
                                    Response.BinaryWrite(ms.ToArray());
                                    Response.Flush();
                                    Response.End();
                                    ms.Close();
                                    ms.Dispose();
                                }
                                else
                                {
                                    str_retuenMsg += "查無此資訊，請重新確認!";
                                }

                                //lbMsg.
                            }
                        }



                    }
                }
                else
                {
                    string sheet_title = "FSE託運總表";
                    string file_name = "DeliveryReport" + DateTime.Now.ToString("yyyyMMdd");
                    int totalcount = 0;
                    int totalpieces = 0;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        if (check_number != "")
                        {
                            // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                            string wherestr = " and A.check_number in(" + check_number + ")";
                            string orderby = " order by  A.check_number";
                            //if (Suppliers.SelectedValue + Customer.SelectedValue == "0000049") orderby = " order by  A.cdate"; //三陽工業，指定依打單的順序列印

                            cmd.CommandText = string.Format(@"Select
                                                          ROW_NUMBER() OVER(ORDER BY A.check_number) '序號',
                                                          A.check_number '貨號' , 
                                                          A.receive_contact '收件人',  
                                                          ISNULL(A.pieces,0) '件數',
										                  A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
                                                          --D.supplier_name '到著站',
                                                          D.station_name '到著站',
                                                          A.receive_tel1 '收件人電話',  
										                  A.collection_money '代收金額',
                                                          B.code_name '付款別',
                                                          --case T.receive_option when TB.code_id then TB.code_name end '集貨異常情形',
                                                          _Arr.arrive_state '回覆狀態',
                                                          case latest_scan_item  when '5' then isnull(TB.code_name,'') 
                                                          when '3' then isnull(_arr.arrive_item,'')
					                                      when '4' then isnull(_arr.arrive_item,'')
                                                          end  '貨態情形', 
                                                          A.invoice_desc '備註'  
                                                          from tcDeliveryRequests A with(nolock)
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          --Left join tbSuppliers D with(nolock) on A.supplier_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                          CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
                                                          Left join tbStation D with(nolock) on A.supplier_code = D.station_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                          left join ttDeliveryScanLog T with(nolock) on t.check_number = A.check_number and t.scan_item = '5' and A.latest_scan_date = T.scan_date
                                                          left join tbItemCodes TB with(nolock) on TB.code_sclass = 'RO' and TB.active_flag = '1' and TB.code_id = T.receive_option
                                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                            using (DataTable dt = dbAdapter.getDataTable(cmd))
                            {
                                if (dt != null && dt.Rows.Count > 0)
                                {




                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {

                                        totalpieces = totalpieces + Convert.ToInt32(dt.Rows[i]["件數"].ToString());
                                    }
                                    int rowIndex = 0;
                                    ISheet sheet = null;
                                    totalcount = dt.Rows.Count;
                                    MemoryStream ms = new MemoryStream();
                                    //HSSFWorkbook workbook = new HSSFWorkbook(); // .xls
                                    IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）

                                    IRow headerRow = null;

                                    if (string.IsNullOrEmpty(sheet_title))
                                    {
                                        //沒有Table Name以預設方式命名Sheet
                                        sheet = workbook.CreateSheet();
                                    }
                                    else
                                    {
                                        //有Table Name以Table Name命名Sheet
                                        sheet = workbook.CreateSheet(sheet_title);
                                    }

                                    ICellStyle style = workbook.CreateCellStyle();
                                    //style.Alignment = HorizontalAlignment.Center;

                                    var font1 = workbook.CreateFont();
                                    font1.Boldweight = 30;
                                    font1.FontHeightInPoints = 24;
                                    style.SetFont(font1);

                                    IRow titletop = sheet.CreateRow(rowIndex++);
                                    ICell cell = sheet.CreateRow(0).CreateCell(0);
                                    cell.SetCellValue("FSE託運總表");
                                    cell.CellStyle = style;

                                    IRow dataRowDate = sheet.CreateRow(rowIndex++);
                                    dataRowDate.CreateCell(0).SetCellValue("發送日期");
                                    dataRowDate.CreateCell(1).SetCellValue(DateTime.Now.ToString("yyyy-MM-dd"));




                                    //if (supplier_code != "")
                                    //{
                                    //    using (SqlCommand cmda = new SqlCommand())
                                    //    {
                                    //        DataTable dta = new DataTable();
                                    //        cmda.Parameters.AddWithValue("@supplier_code", supplier_code);
                                    //        cmda.CommandText = @" WITH cus AS(
                                    //                    SELECT *,
                                    //                    ROW_NUMBER() OVER(PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                    //                    FROM tbCustomers with(nolock) where 1 = 1 and supplier_code <> '001' and stop_shipping_code = '0' and supplier_code=@supplier_code
                                    //                    )
                                    //                    SELECT customer_name
                                    //                    FROM cus
                                    //                    WHERE rn = 1";

                                    //        dta = dbAdapter.getDataTable(cmda);
                                    //        if (dta.Rows.Count > 0)
                                    //        {
                                    //            IRow dataRowCus = sheet.CreateRow(rowIndex++);
                                    //            dataRowCus.CreateCell(0).SetCellValue("寄件人");
                                    //            dataRowCus.CreateCell(1).SetCellValue(dta.Rows[0]["customer_name"].ToString());
                                    //        }
                                    //    }
                                    //}



                                    rowIndex++;




                                    headerRow = sheet.CreateRow(rowIndex++);

                                    //自動換行
                                    //csHeader.WrapText = true;

                                    #region 處理標題列
                                    foreach (DataColumn column in dt.Columns)
                                    {
                                        headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                                    }
                                    #endregion

                                    #region 處理資料列
                                    foreach (DataRow row in dt.Rows)
                                    {
                                        IRow dataRow = sheet.CreateRow(rowIndex++);

                                        foreach (DataColumn column in dt.Columns)
                                        {


                                            dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));
                                            ICellStyle styleA = workbook.CreateCellStyle();


                                        }
                                    }
                                    IRow dataRowA = sheet.CreateRow(rowIndex + 1);



                                    dataRowA.CreateCell(1).SetCellValue(Convert.ToString("總計"));
                                    dataRowA.CreateCell(2).SetCellValue(Convert.ToString("共" + totalcount + "筆"));
                                    dataRowA.CreateCell(3).SetCellValue(Convert.ToString("共" + totalpieces + "件"));


                                    sheet.SetColumnWidth(0, 15 * 256);//序號寬度
                                    sheet.SetColumnWidth(1, 30 * 256);//貨號寬度
                                    sheet.SetColumnWidth(2, 30 * 256);//收件人寬度
                                    sheet.SetColumnWidth(3, 10 * 256);//件數寬度
                                    sheet.SetColumnWidth(4, 100 * 256);//地址寬度
                                    sheet.SetColumnWidth(5, 25 * 256);//到著站寬度
                                    sheet.SetColumnWidth(6, 30 * 256);//收件人電話寬度
                                    sheet.SetColumnWidth(7, 10 * 256);//代收金額寬度
                                    sheet.SetColumnWidth(8, 10 * 256);//付款別寬度
                                    sheet.SetColumnWidth(9, 60 * 256);//備註寬度


                                    #endregion

                                    workbook.Write(ms);
                                    Response.Clear();
                                    //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    //Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx", file_name));
                                    Response.AddHeader("content-disposition", string.Format("attachment;  filename={0}.xlsx", HttpUtility.UrlEncode(file_name, Encoding.UTF8), Encoding.UTF8));
                                    Response.BinaryWrite(ms.ToArray());
                                    Response.Flush();
                                    Response.End();
                                    ms.Close();
                                    ms.Dispose();
                                }
                                else
                                {
                                    str_retuenMsg += "查無此資訊，請重新確認!";
                                }

                                //lbMsg.
                            }
                        }



                    }
                }


            }
            #endregion

        }
        catch (Exception ex)
        {
            str_retuenMsg += "系統異常，請洽相關人員!";
            str_retuenMsg += " (" + ex.Message.ToString() + ")";
        }
        finally
        {
            lbMsg.Items.Add(str_retuenMsg);

        }

    }

    protected void DBToExcel_bak(string check_number)
    {
        //DateTime dt_active_date = new DateTime();
        //DateTime.TryParse(Request.QueryString["atdate"].ToString()
        String str_retuenMsg = string.Empty;
        //DateTime dt_active_date = new DateTime();
        try
        {
            Boolean IsChkOk = false;

            //lbMsg.Items.Clear();
            //if (!IsPublic)
            //{
            //    if (DateTime.TryParse(active_date, out dt_active_date)
            //        && customer_id > 0
            //        && customer_code.Length > 0) IsChkOk = true;
            //}
            //else
            //{
            //    if (customer_code.Length > 0) IsChkOk = true;
            //}

            if (check_number.Length > 0) IsChkOk = true;

            #region 撈DB寫入EXCEL
            if (IsChkOk)
            {
                string sheet_title = "託運總表";
                string file_name = "託運總表" + DateTime.Now.ToString("yyyyMMdd");

                using (SqlCommand cmd = new SqlCommand())
                {
                    if (check_number != "")
                    {
                        // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                        string wherestr = " and check_number in(" + check_number + ")";
                        cmd.CommandText = string.Format(@"select Convert(varchar,A.print_date,111)  '發送日期', Convert(varchar,A.supplier_date,111)   '配送日期' , A.check_number '貨號' , 
                                                          C.code_name  '計價模式' ,A.pieces '件數' , A.plates '板數', A.cbm '才數',
                                                          A.receive_contact '收件人',  
										                  A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
										                  A.collection_money '代收金',A.arrive_to_pay_freight '到付運費',A.arrive_to_pay_append'到付追加', 
                                                          B.code_name '付款別'  
                                                          from tcDeliveryRequests A with(nolock)
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on B.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          where 1= 1  {0} order by  A.check_number", wherestr);
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                using (ExcelPackage p = new ExcelPackage())
                                {
                                    p.Workbook.Properties.Title = sheet_title;
                                    p.Workbook.Worksheets.Add(sheet_title);
                                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                                    int colIndex = 1;
                                    int rowIndex = 1;
                                    foreach (DataColumn dc in dt.Columns)
                                    {
                                        var cell = ws.Cells[rowIndex, colIndex];
                                        var fill = cell.Style.Fill;
                                        fill.PatternType = ExcelFillStyle.Solid;
                                        fill.BackgroundColor.SetColor(Color.LightGray);

                                        //Setting Top/left,right/bottom borders.
                                        var border = cell.Style.Border;
                                        border.Bottom.Style =
                                            border.Top.Style =
                                            border.Left.Style =
                                            border.Right.Style = ExcelBorderStyle.Thin;

                                        //Setting Value in cell
                                        cell.Value = dc.ColumnName;
                                        colIndex++;
                                    }
                                    rowIndex++;

                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        colIndex = 1;
                                        foreach (DataColumn dc in dt.Columns)
                                        {
                                            var cell = ws.Cells[rowIndex, colIndex];
                                            cell.Value = dr[dc.ColumnName];

                                            //Setting borders of cell
                                            var border = cell.Style.Border;
                                            border.Left.Style =
                                                border.Right.Style = ExcelBorderStyle.Thin;
                                            colIndex++;
                                        }

                                        rowIndex++;
                                    }
                                    ws.View.FreezePanes(2, 1);
                                    ws.Cells.Style.Font.Size = 12;
                                    //ws.Column(dt.Columns.Count).Hidden = true;//最後一欄為table name需隱藏→匯入DB比對時使用
                                    Response.Clear();
                                    Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                    Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    //Response.ContentType = "application/octet-stream";
                                    Response.ContentType = "application/download";
                                    Response.BinaryWrite(p.GetAsByteArray());
                                    Response.End();

                                    str_retuenMsg += "下載完成!";
                                }
                            }
                            else
                            {
                                str_retuenMsg += "查無此資訊，請重新確認!";
                            }

                            //lbMsg.
                        }
                    }



                }

            }
            #endregion

        }
        catch (Exception ex)
        {
            str_retuenMsg += "系統異常，請洽相關人員!";
            str_retuenMsg += " (" + ex.Message.ToString() + ")";
        }
        finally
        {
            lbMsg.Items.Add(str_retuenMsg);

        }

    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue == "0")
        {
            lbDate.Text = "建檔日期";
            cdate1.Visible = true;
            cdate2.Visible = true;
            date1.Visible = false;
            date2.Visible = false;
        }
        else
        {
            lbDate.Text = "發送日期";
            cdate1.Visible = false;
            cdate2.Visible = false;
            date1.Visible = true;
            date2.Visible = true;
        }
        // string sScript = "$('.date_picker').datepicker();  ";
        // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
    }

    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlsend_city.SelectedValue != "")
        {
            ddlsend_area.Items.Clear();
            ddlsend_area.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", ddlsend_city.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    ddlsend_area.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            ddlsend_area.Items.Clear();
            ddlsend_area.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void area_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlzip = ddlzip;
        if (ddlsend_area.SelectedValue != "")
        {
            dlzip.Items.Clear();
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@post_city", ddlsend_city.SelectedValue.ToString());
            cmda.Parameters.AddWithValue("@post_area", ddlsend_area.SelectedValue.ToString());
            cmda.CommandText = @"
Select zip, post_area ,A.station_code,B.station_name  from ttArriveSitesScattered A With(Nolock)  
inner join tbStation B With(Nolock)  on A.station_code = B.station_scode
where A.post_city=@post_city and A.post_area = @post_area 
order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                //lbstation_scode.Text = dta.Rows[0]["station_code"].ToString() +" " + dta.Rows[0]["station_name"].ToString();
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlzip.Items.Add(new ListItem(dta.Rows[i]["zip"].ToString().Trim(), dta.Rows[i]["zip"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlzip.Items.Clear();
            dlzip.Items.Add(new ListItem("請選擇", ""));
        }
    }

    private static String PreparePOSTForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                    send_station_scode = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }
}