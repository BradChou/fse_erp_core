﻿using ClosedXML.Excel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets2_2 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            readdata();
        }
    }

    private void readdata()
    {
        string wherestr = "";
        SqlCommand cmd = new SqlCommand();
        DateTime ChkDate = DateTime.Today;
        cmd.Parameters.AddWithValue("@starttime", ChkDate.ToString("yyyy/MM/dd"));
        wherestr += " and CONVERT(VARCHAR,A.starttime,111)  = @starttime";
        cmd.CommandText = string.Format(@"Select * from ttAssetsFee A where 1= 1 {0} order by A.id", wherestr);
        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
        }
    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "空白費用總表標準格式.xls";
        Func.DownloadExcel(ttFilePath, ttFileName);
    }

    protected void btImport_Click(object sender, EventArgs e)
    {
        lbMsg.Items.Clear();
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (file01.HasFile)
        {
            #region 抓車牌
            DataTable dt_car = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select car_license, REPLACE(car_license,'-','') as car_license_nodash, ISNULL(oil_discount,0) as oil_discount, ownership from ttAssets with(nolock) ";
                dt_car = dbAdapter.getDataTable(cmd);
            }
            #endregion

            string ttErrStr = "";
            DateTime startTime = DateTime.Now;//開始匯入時間
            DateTime? endTime;//匯入結束時間
            int successNum = 0;
            int failNum = 0;
            int totalNum = 0;
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            //string ttFileName = ttPath + file02.PostedFile.FileName;
            string ttFileName = ttPath + file01.FileName;

            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                HSSFWorkbook workbook = null;
                HSSFSheet u_sheet = null;
                string ttSelectSheetName = "";
                workbook = new HSSFWorkbook(file01.FileContent);
                Boolean IsSheetOk = true;//工作表驗證
                try
                {
                    //for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                    //{
                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);

                    if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                    {
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + startTime.ToString("yyyy-MM-dd HH:mm:ss")));

                        //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                        string strRow, strInsCol, strInsVal;
                        List<StringBuilder> sb_del_list = new List<StringBuilder>();
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_del = new StringBuilder();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        HSSFRow Row_title = new HSSFRow();

                        string ChkError01 = "";
                        string ChkError02 = "";
                        Boolean ChkFirstError = true;

                        if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);
                        for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                        {
                            #region 初始化

                            HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列  
                            strRow = (i + 1).ToString();
                            #endregion

                            #region 必填欄位篩選
                            for (var CellnNum = 0; CellnNum < 4; CellnNum++)
                            {
                                if (Row.GetCell(CellnNum) == null || Row.GetCell(CellnNum).ToString() == "")
                                {
                                    ChkFirstError = false;
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                    IsSheetOk = false;
                                    ChkError01 = "工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!";
                                    ChkError02 += "第" + strRow + "列： 請確認【" + Row_title.GetCell(CellnNum).StringCellValue + "】 是否正確!|";
                                    break;
                                }
                            }
                            #endregion

                            if (ChkFirstError)
                            {
                                totalNum += 1;
                                successNum += 1;

                                #region 驗證&取值 IsSheetOk

                                string fee_type = "";
                                string a_id = "";
                                string datestr = "";
                                string timestr = "";
                                string oil = "";
                                string company = "";
                                string items = "";
                                string detail = "";
                                float quant = 0;
                                float milage = 0;
                                float litre = 0;
                                float price_notax = 0;       //牌價單價(未稅)
                                float buy_price_notax = 0;   //購入單價(未稅)
                                float receipt_price_notax = 0; //收款單價(未稅)
                                float price = 0;             //牌價單價(含稅)
                                float buy_price = 0;         //購入單價(含稅)
                                float receipt_price = 0;     //收款單價(含稅)
                                float total_price = 0;
                                string memo = "";
                                double oil_discount = 0;     //油價折扣
                                string ownership = "";



                                for (int j = 0; j < Row_title.Cells.Count; j++)
                                {
                                    Boolean IsChkOk = true;

                                    #region 欄位篩選
                                    switch (j)
                                    {
                                        case 0:  //費用類別
                                            if (Row.GetCell(j) != null) fee_type = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(fee_type))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                switch (fee_type)
                                                {
                                                    case "油料":
                                                        fee_type = "1";
                                                        break;
                                                    case "ETC":
                                                        fee_type = "2";
                                                        break;
                                                    case "GPS":
                                                        fee_type = "3";
                                                        break;
                                                    case "修繕費":
                                                        fee_type = "4";
                                                        break;
                                                    case "保險費":
                                                        fee_type = "5";
                                                        break;
                                                    case "租金":
                                                        fee_type = "6";
                                                        break;
                                                    case "罰單":
                                                        fee_type = "7";
                                                        break;
                                                    case "輪胎":
                                                        fee_type = "8";
                                                        break;
                                                    case "牌照稅":
                                                        fee_type = "9";
                                                        break;
                                                    case "燃料稅":
                                                        fee_type = "10";
                                                        break;
                                                    case "其他":
                                                        fee_type = "99";
                                                        break;
                                                    default:
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 1:  //車牌號碼
                                            if (Row.GetCell(j) != null) a_id = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(a_id))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 2:  //交易日期
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        datestr = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        datestr = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(datestr))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            datestr = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                datestr = "NULL";
                                            }
                                            if (datestr == "NULL")
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 3:  //交易時間
                                            if (Row.GetCell(j) != null)
                                            {
                                                timestr = Row.GetCell(j).ToString().Trim();
                                                if (String.IsNullOrEmpty(timestr))
                                                {
                                                    IsChkOk =
                                                    IsSheetOk = false;
                                                }
                                                if (datestr != "NULL")
                                                {
                                                    datestr += " " + timestr;
                                                }
                                            }
                                            break;
                                        case 4:  //油品公司
                                            if (Row.GetCell(j) != null)
                                            {
                                                oil = Row.GetCell(j).ToString().Trim();
                                                switch (oil)
                                                {
                                                    case "台塑":
                                                        oil = "台塑";

                                                        break;
                                                    case "全國":
                                                        oil = "全國";
                                                        break;
                                                }
                                            }
                                            break;
                                        case 5:  //公司名稱
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    company = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                        case 6:  //項目
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    items = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                        case 7:  //明細
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    detail = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                        case 8:  //數量
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    quant = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 9:  //里程數
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    milage = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 10:  //公升數
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    litre = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 11:  //(含稅)牌價單價(折扣前)
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    price = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 12:
                                            //油價：(含稅)購入單價(折扣後)(油價-1.4元)
                                            //非油價：(未稅)購入單價
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (fee_type == "1")  //油卡
                                                {
                                                    if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                    {
                                                        buy_price = float.Parse(Row.GetCell(j).ToString());       //含稅
                                                    }
                                                }
                                                else
                                                {
                                                    //非油卡
                                                    if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                    {
                                                        buy_price_notax = float.Parse(Row.GetCell(j).ToString());  //未稅
                                                        buy_price = (float)(buy_price * 1.05);
                                                        //switch (fee_type)
                                                        //{
                                                        //    case "2": //ETC
                                                        //        receipt_price_notax = (float)(buy_price_notax * 1.1); // 收款單價(未稅) 
                                                        //        receipt_price = (float)(receipt_price_notax * 1.05);  // 收款單價(含稅) 

                                                        //        break;
                                                        //    case "3": //GPS
                                                        //        if (company.Contains("漢名"))
                                                        //        {
                                                        //            receipt_price_notax = buy_price_notax + 50;
                                                        //            receipt_price = (float)(receipt_price_notax * 1.05);
                                                        //        }
                                                        //        else
                                                        //        {
                                                        //            receipt_price_notax = buy_price_notax;
                                                        //            receipt_price = (float)(receipt_price_notax * 1.05);
                                                        //        }
                                                        //        break;
                                                        //    case "4": //修繕費
                                                        //        receipt_price_notax = (float)(buy_price_notax * 1.05); // 收款單價(未稅) 
                                                        //        receipt_price = (float)(receipt_price_notax * 1.05);   // 收款單價(含稅) 
                                                        //        break;
                                                        //    case "7": //罰單
                                                        //        receipt_price_notax = (float)(buy_price_notax + 50);   // 收款單價(未稅) 
                                                        //        receipt_price = (float)(receipt_price_notax * 1.05);   // 收款單價(含稅) 
                                                        //        break;
                                                        //    default:
                                                        //        receipt_price_notax = buy_price_notax;
                                                        //        receipt_price = (float)(receipt_price_notax * 1.05);
                                                        //        break;
                                                        //}
                                                    }
                                                }
                                            }
                                            break;
                                        //case 13:   //總金額=牌價(含稅) * quant
                                        //    if (Row.GetCell(j) != null)
                                        //    {
                                        //        if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                        //        {
                                        //            total_price = float.Parse(Row.GetCell(j).ToString());
                                        //        }
                                        //    }
                                        //    break;
                                        case 13:  //備註
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    memo = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                    }
                                    #endregion

                                    if (!IsChkOk)
                                    {
                                        if (string.IsNullOrEmpty(ChkError01))
                                        {
                                            ChkError01 = "工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!";
                                        }
                                        ChkError02 += "第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!|";
                                    }
                                }

                                #endregion

                                #region 組新增

                                //檢查車號格式
                                //統一秀有"-"的車牌格式
                                DataRow[] rows = dt_car.Select("car_license='" + a_id + "'");
                                if (rows.Length == 0)
                                {
                                    DataRow[] nodash = dt_car.Select("car_license_nodash='" + a_id + "'");
                                    if (nodash.Length > 0)
                                    {
                                        a_id = nodash[0]["car_license"].ToString();
                                        oil_discount = Convert.ToDouble(nodash[0]["oil_discount"]);
                                        ownership = nodash[0]["ownership"].ToString();
                                    }
                                }
                                else
                                {
                                    oil_discount = Convert.ToDouble(rows[0]["oil_discount"]);
                                    ownership = rows[0]["ownership"].ToString();
                                }


                                switch (fee_type)
                                {
                                    case "1": //油卡
                                        price_notax = (float)(price / 1.05);   //牌價單價(未稅)

                                        if (buy_price == 0) buy_price = (float)(price - 1.4);   //購入單價(含稅)
                                        buy_price_notax = (float)(buy_price / 1.05);            //購入單價(未稅)

                                        if (ownership == "1")  //自有車
                                        {
                                            receipt_price_notax = buy_price_notax; // 收款單價(未稅) 
                                        }
                                        else
                                        {
                                            //收款單價要以未稅牌價-折扣
                                            if ((price_notax + oil_discount) < buy_price_notax)                 // 收款單價(未稅) 
                                            {
                                                receipt_price_notax = buy_price;
                                            }
                                            else
                                            {
                                                receipt_price_notax = (float)(price_notax + oil_discount);
                                            }
                                        }
                                        
                                        receipt_price = (float)(receipt_price_notax * 1.05);     //收款單價(含稅)

                                        break;
                                    case "2": //ETC

                                        if (ownership == "1")  //自有車
                                        {
                                            receipt_price_notax = buy_price_notax; // 收款單價(未稅) 
                                        }
                                        else if (ownership == "2")  //個人車主
                                        {
                                            receipt_price_notax = (float)(buy_price_notax * 1.07); // 收款單價(未稅) 
                                        }
                                        else
                                        {
                                            receipt_price_notax = (float)(buy_price_notax * 1.1); // 收款單價(未稅) 
                                        }

                                        receipt_price = (float)(receipt_price_notax * 1.05);  // 收款單價(含稅) 

                                        break;
                                    case "3": //GPS
                                        if (company.Contains("漢名"))
                                        {
                                            receipt_price_notax = buy_price_notax + 50;
                                            receipt_price = (float)(receipt_price_notax * 1.05);
                                        }
                                        else
                                        {
                                            receipt_price_notax = buy_price_notax;
                                            receipt_price = (float)(receipt_price_notax * 1.05);
                                        }

                                        break;
                                    case "4": //修繕費
                                        if (ownership == "1")  //自有車
                                        {
                                            receipt_price_notax = buy_price_notax; // 收款單價(未稅) 
                                        }
                                        else {
                                            receipt_price_notax = (float)(buy_price_notax * 1.05); // 收款單價(未稅) 
                                        }
                                        
                                        receipt_price = (float)(receipt_price_notax * 1.05);   // 收款單價(含稅) 
                                        break;
                                    case "7": //罰單
                                        if (ownership == "1")  //自有車
                                        {
                                            receipt_price_notax = buy_price_notax; // 收款單價(未稅) 
                                        }
                                        else {
                                            receipt_price_notax = (float)(buy_price_notax + 50);   // 收款單價(未稅) 
                                        }
                                        
                                        receipt_price = (float)(receipt_price_notax * 1.05);   // 收款單價(含稅) 
                                        break;
                                    default:
                                        receipt_price_notax = buy_price_notax;
                                        receipt_price = (float)(receipt_price_notax * 1.05);
                                        break;
                                }



                                //if (!DateTime.TryParse(date, out date)) date = DateTime.Now;


                                strInsCol = "fee_type, a_id, date, oil, company, items, detail, milage, litre, price, buy_price,receipt_price,  price_notax, buy_price_notax, receipt_price_notax ,quant,total_price,memo,starttime,cdate, cuser,udate, uuser";
                                strInsVal = "N\'" + fee_type + "\', " +
                                            "N\'" + a_id + "\', ";
                                if (datestr == "NULL")
                                {
                                    strInsVal += datestr + ", ";
                                }
                                else
                                {
                                    strInsVal += "N\'" + datestr + "\', ";
                                }
                                strInsVal += "N\'" + oil + "\', " +
                                            "N\'" + company + "\', " +
                                            "N\'" + items + "\', " +
                                            "N\'" + detail + "\', " +
                                            "N\'" + milage + "\', " +
                                            "N\'" + litre + "\', " +
                                            "N\'" + price.ToString("N2") + "\', " +
                                            "N\'" + buy_price.ToString("N2") + "\', " +
                                            "N\'" + receipt_price.ToString("N2") + "\', " +
                                            "N\'" + price_notax.ToString("N2") + "\', " +
                                            "N\'" + buy_price_notax.ToString("N2") + "\', " +
                                            "N\'" + receipt_price_notax.ToString("N2") + "\', " +
                                            "N\'" + quant + "\', " +
                                            "N\'" + total_price + "\', " +
                                            "N\'" + memo + "\', " +
                                            "N\'" + startTime.ToString("yyyy-MM-dd HH:mm:ss") + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\', "+
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\' ";

                                string insertstr = "INSERT INTO ttAssetsFee (" + strInsCol + ") VALUES(" + strInsVal + ")";
                                sb_temp_ins.Append(insertstr);

                                //每100筆組成一字串
                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }

                                #endregion

                            }
                        }

                        //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                        if (IsSheetOk)
                        {
                            #region 執行SQL

                            //startTime = DateTime.Now;
                            //新增
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    String strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }

                                    }
                                }
                            }
                            endTime = DateTime.Now;
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  結束匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            #endregion

                            #region 匯入記錄
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                cmd.Parameters.AddWithValue("@changeTable", "ttAssetsFee");
                                cmd.Parameters.AddWithValue("@fromWhere", "每月費用資料維護-匯入");
                                cmd.Parameters.AddWithValue("@memo", "");
                                cmd.Parameters.AddWithValue("@successNum", "1");
                                cmd.Parameters.AddWithValue("@failNum", "1");
                                cmd.Parameters.AddWithValue("@totalNum", "1");
                                cmd.Parameters.AddWithValue("@startTime", startTime.ToString("yyyy-MM-dd HH:mm:ss"));
                                cmd.Parameters.AddWithValue("@endTime", endTime);
                                cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);//Session["account_code"] != null ? Session["account_code"].ToString() : null
                                cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                            #endregion

                            readdata();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ChkError02))
                            {
                                lbMsg.Items.Add(ChkError01.ToString());
                                string[] ChkError02Str = ChkError02.Split('|');
                                foreach (string k in ChkError02Str)
                                {
                                    if (!string.IsNullOrEmpty(k.ToString()))
                                    {
                                        lbMsg.Items.Add(k.ToString());
                                    }
                                }
                            }
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");
                    }

                    //}
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    if (IsSheetOk)
                    {
                        lbMsg.Items.Add("匯入完畢");
                    }
                }
            }
        }
    }

    public bool IsValidCheckNumber(string check_number)
    {
        bool IsVaild = true;
        if (check_number.Length != 10)
        {
            IsVaild = false;
        }
        else
        {
            int num = Convert.ToInt32(check_number.Substring(0, 9));

            int chk = num % 7;
            int lastnum = Convert.ToInt32(check_number.Substring(9, 1));
            if (lastnum != chk)
            {
                IsVaild = false;
            }
        }
        return IsVaild;
    }


}