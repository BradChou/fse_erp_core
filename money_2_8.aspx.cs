﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using Ionic.Zip;
using System.Collections;

public partial class money_2_8 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = DateTime.Today.ToString("yyyy/MM/") + "01"; //yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                        //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            
            #region 
            Suppliers.DataSource = Utility.getSupplierDT2(supplier_code, manager_type);
            Suppliers.DataValueField = "supplier_no";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (manager_type == "0" || manager_type == "1") Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion

        }

    }

           
   
    protected void btExport_Click(object sender, EventArgs e)
    {
        string supplier_no = string.Empty;
        string path = Request.PhysicalApplicationPath + @"files\money\應付帳款(" + Convert.ToDateTime(dates.Text).ToString("yyyyMMdd") + "-" + Convert.ToDateTime(datee.Text).ToString("yyyyMMdd") + ")";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        if (Suppliers.SelectedValue == "" && Suppliers.Items.Count > 1)
        {
            for (int i = 1; i <= Suppliers.Items.Count - 1; i++)
            {
                supplier_no = Suppliers.Items[i].Value;
                tbSuppliers _tbSuppliers = getSuppliers(supplier_no);

                #region 匯出報表
                export_ABC(supplier_no, _tbSuppliers, path);
                export_D(supplier_no, _tbSuppliers, path);
                export_E(supplier_no, _tbSuppliers, path);
                #endregion

            }
        }
        else if (Suppliers.SelectedValue != "")
        {
            supplier_no = Suppliers.SelectedValue.ToString();
            tbSuppliers _tbSuppliers = getSuppliers(supplier_no);

            #region 匯出報表
            export_ABC(supplier_no, _tbSuppliers, path);
            export_D(supplier_no, _tbSuppliers, path);
            export_E(supplier_no, _tbSuppliers, path);
            #endregion
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可出帳供應商');</script>", false);
            return;
        }

        #region 壓縮檔案
        ZipFiles(path, string.Empty, string.Empty);
        #endregion

        


    }

    private tbSuppliers getSuppliers(string supplier_no)
    {
        tbSuppliers _tbSuppliers = null;
        #region 供應商資訊
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"select supplier_code , supplier_no ,  supplier_name, uniform_numbers , principal , contact ,  account from tbSuppliers with(nolock) where supplier_no = @supplier_no";
            cmd.Parameters.AddWithValue("@supplier_no", supplier_no);
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                _tbSuppliers = DataTableExtensions.ToList<tbSuppliers>(_dt).ToList().FirstOrDefault();
            }
        }
        #endregion

        return _tbSuppliers;
    }

    protected string  export_ABC(string supplier_no, tbSuppliers _tbSuppliers, string path)
    {
        string filename = "";
        DataTable dt_a = null;
        DataTable dt_b = null;
        DataTable dt_c = null;
        Utility.AccountsReceivableInfo _info_A = new Utility.AccountsReceivableInfo();
        Utility.AccountsReceivableInfo _info_B = new Utility.AccountsReceivableInfo();
        Utility.AccountsReceivableInfo _info_C = new Utility.AccountsReceivableInfo();
        string Title = "峻富物流股份有限公司";
        string supplier_name = Suppliers.SelectedItem.Text;
        
        #region
        using (SqlCommand cmd = new SqlCommand())
        {
            string randomCode = "";
            cmd.CommandText = @"select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
                                where  type = 1  and supplier_code = @supplier_code and close_enddate >= @dates and close_enddate < @datee
                                and src = @src  FOR XML PATH('')),1,1,'') 'randomCode'";
            cmd.Parameters.AddWithValue("@src", "A");
            cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
            cmd.Parameters.AddWithValue("@dates", dates.Text);                                                          //日期起
            cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));  //日期迄
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                randomCode = _dt.Rows[0]["randomCode"].ToString();
                if (randomCode != "")
                    using (SqlCommand cmd_a = new SqlCommand())
                    {
                        cmd_a.CommandText = "usp_GetAccountsPayable_A";
                        cmd_a.CommandType = CommandType.StoredProcedure;
                        cmd_a.Parameters.AddWithValue("@supplier", supplier_no);
                        cmd_a.Parameters.AddWithValue("@randomCode", randomCode);
                        dt_a = dbAdapter.getDataTable(cmd_a);
                        _info_A = Utility.GetSumAccountsPayable("A", dates.Text, datee.Text, null, Suppliers.SelectedValue, randomCode);
                    }
            }
        }
        #endregion

        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            string randomCode = "";
            cmd.CommandText = @"Select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
                                Where  type = 1  and supplier_code = @supplier_code and close_enddate >= @dates and close_enddate < @datee
                                And src = @src  FOR XML PATH('')),1,1,'') 'randomCode'";
            cmd.Parameters.AddWithValue("@src", "B");
            cmd.Parameters.AddWithValue("@supplier_code", supplier_no);
            cmd.Parameters.AddWithValue("@dates", dates.Text);                                                          //日期起
            cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));    //日期迄
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                randomCode = _dt.Rows[0]["randomCode"].ToString();
                if (randomCode != "")
                    using (SqlCommand cmd_b = new SqlCommand())
                    {
                        cmd_b.CommandText = "usp_GetAccountsPayable_B";
                        cmd_b.CommandType = CommandType.StoredProcedure;
                        cmd_b.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);
                        cmd_b.Parameters.AddWithValue("@randomCode", randomCode);
                        dt_b = dbAdapter.getDataTable(cmd_b);
                        _info_B = Utility.GetSumAccountsPayable("B", dates.Text, datee.Text, null, Suppliers.SelectedValue, randomCode);
                    }
            }
        }
        #endregion

        #region

        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    string randomCode = "";
        //    cmd.CommandText = @"select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
        //                        where  type = 1  and supplier_code = @supplier_code and close_enddate >= @dates and close_enddate < @datee
        //                        and src = @src  FOR XML PATH('')),1,1,'') 'randomCode'";
        //    cmd.Parameters.AddWithValue("@src", "C");
        //    cmd.Parameters.AddWithValue("@supplier_code", supplier_no);
        //    cmd.Parameters.AddWithValue("@dates", dates.Text);                                                          //日期起
        //    cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));  //日期迄
        //    DataTable _dt = dbAdapter.getDataTable(cmd);
        //    if (_dt.Rows.Count > 0)
        //    {
        //        randomCode = _dt.Rows[0]["randomCode"].ToString();
        //        using (SqlCommand cmd_c = new SqlCommand())
        //        {
        //            cmd_c.CommandText = "usp_GetAccountsPayable";
        //            cmd_c.CommandType = CommandType.StoredProcedure;
        //            cmd_c.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);
        //            cmd_c.Parameters.AddWithValue("@randomCode", randomCode);
        //            dt_c = dbAdapter.getDataTable(cmd_c);
        //            //_info = Utility.GetSumAccountsPayable_C(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.SelectedValue);
        //        }
        //    }
        //}
        #endregion

        if ((dt_a != null && dt_a.Rows.Count > 0) || (dt_b != null && dt_b.Rows.Count > 0) || (dt_c != null && dt_c.Rows.Count > 0))
        {
            string fullPath = Request.PhysicalApplicationPath + @"report\支出證明單.xlsx";
            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (ExcelPackage pck = new ExcelPackage(fs))
                {
                    //ExcelPackage pck = new ExcelPackage();
                    #region 請款單
                    var sheet_pay = pck.Workbook.Worksheets[1];
                    sheet_pay.Cells[4, 4].Value = _tbSuppliers.supplier_name;
                    sheet_pay.Cells[5, 4].Value = _info_A.total + _info_B.total;
                    sheet_pay.Cells[7, 13].Value = _info_A.subtotal + _info_B.subtotal;
                    sheet_pay.Cells[8, 13].Value = _info_A.tax + _info_B.tax;
                    sheet_pay.Cells[9, 13].Value = _info_A.total + _info_B.total;
                    sheet_pay.Cells[10, 7].Value = _tbSuppliers.account;
                    #endregion

                    #region sheet1-A段
                    var sheet1 = pck.Workbook.Worksheets.Add("A段");

                    //檔案邊界
                    sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet1.PrinterSettings.HorizontalCentered = true;

                    sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                                                                                   // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                                                                                   //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

                    //欄寬
                    sheet1.Column(1).Width = 6;
                    sheet1.Column(2).Width = 20;
                    sheet1.Column(3).Width = 15;
                    sheet1.Column(4).Width = 15;
                    sheet1.Column(5).Width = 30;
                    sheet1.Column(6).Width = 20;
                    sheet1.Column(7).Width = 10;
                    sheet1.Column(8).Width = 10;
                    sheet1.Column(9).Width = 10;
                    sheet1.Column(10).Width = 10;
                    sheet1.Column(11).Width = 10;



                    sheet1.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
                    sheet1.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
                    sheet1.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
                    sheet1.Cells[1, 1, 1, 11].Style.Font.Size = 18;
                    sheet1.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                    sheet1.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

                    sheet1.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
                    sheet1.Cells[2, 1, 2, 11].Style.Font.Size = 12;
                    sheet1.Cells[2, 1, 2, 11].Merge = true;
                    sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet1.Cells[3, 2, 3, 11].Merge = true;     //合併儲存格
                    sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
                    sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
                    sheet1.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
                    sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    sheet1.Cells[4, 1, 4, 11].Style.Font.Name = "微軟正黑體";
                    sheet1.Cells[4, 1, 4, 11].Style.Font.Size = 12;
                    sheet1.Cells[4, 1, 4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet1.Cells[4, 1, 4, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[4, 1, 4, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[4, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[4, 1].Value = "序號";
                    sheet1.Cells[4, 2].Value = "發送日期";
                    sheet1.Cells[4, 3].Value = "配送日期";
                    sheet1.Cells[4, 4].Value = "客戶代碼";
                    sheet1.Cells[4, 5].Value = "客戶名稱";
                    sheet1.Cells[4, 6].Value = "貨號";
                    sheet1.Cells[4, 7].Value = "發送區";
                    sheet1.Cells[4, 8].Value = "到著區";
                    sheet1.Cells[4, 9].Value = "件數";
                    sheet1.Cells[4, 10].Value = "板數";
                    sheet1.Cells[4, 11].Value = "發包價";

                    if (dt_a != null && dt_a.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_a.Rows.Count; i++)
                        {
                            sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                            sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet1.Cells[i + 5, 2].Value = dt_a.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt_a.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                            sheet1.Cells[i + 5, 3].Value = dt_a.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt_a.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                            sheet1.Cells[i + 5, 4].Value = dt_a.Rows[i]["客戶代碼"].ToString();
                            sheet1.Cells[i + 5, 5].Value = dt_a.Rows[i]["客戶名稱"].ToString();
                            sheet1.Cells[i + 5, 6].Value = dt_a.Rows[i]["貨號"].ToString();
                            sheet1.Cells[i + 5, 7].Value = dt_a.Rows[i]["發送站"].ToString();
                            sheet1.Cells[i + 5, 8].Value = dt_a.Rows[i]["到著站"].ToString();
                            sheet1.Cells[i + 5, 9].Value = Convert.ToInt32(dt_a.Rows[i]["板數"]);
                            sheet1.Cells[i + 5, 9].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[i + 5, 10].Value = Convert.ToInt32(dt_a.Rows[i]["件數"]);
                            sheet1.Cells[i + 5, 10].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[i + 5, 11].Value = Convert.ToInt32(dt_a.Rows[i]["發包價"]);

                            sheet1.Cells[i + 5, 11].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[i + 5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            if (i == dt_a.Rows.Count - 1 && _info_A != null)
                            {
                                sheet1.Cells[i + 6, 8].Value = "小計";
                                sheet1.Cells[i + 6, 9].Value = _info_A.plates;
                                sheet1.Cells[i + 6, 10].Value = _info_A.pieces;
                                sheet1.Cells[i + 6, 11].Value = _info_A.subtotal;
                                sheet1.Cells[i + 6, 11].Style.Numberformat.Format = "#,##0";
                                sheet1.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet1.Cells[i + 6, 1, i + 6, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet1.Cells[i + 6, 1, i + 6, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 6, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                                sheet1.Cells[i + 7, 8].Value = "5%營業稅";
                                sheet1.Cells[i + 7, 11].Value = _info_A.tax;
                                sheet1.Cells[i + 7, 11].Style.Numberformat.Format = "#,##0";
                                sheet1.Cells[i + 7, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet1.Cells[i + 7, 1, i + 7, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet1.Cells[i + 7, 1, i + 7, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 7, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                sheet1.Cells[i + 8, 8].Value = "應付帳款";
                                sheet1.Cells[i + 8, 11].Value = _info_A.total;
                                sheet1.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                                sheet1.Cells[i + 8, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet1.Cells[i + 8, 1, i + 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet1.Cells[i + 8, 1, i + 8, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet1.Cells[i + 8, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                            }
                        }
                    }


                    //頁尾加入頁次
                    sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                    sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");

                    #endregion
                    #region sheet2-B段
                    var sheet2 = pck.Workbook.Worksheets.Add("B段");
                    //檔案邊界
                    sheet2.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet2.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet2.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet2.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet2.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet2.PrinterSettings.HorizontalCentered = true;

                    sheet2.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                                                                                   // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                                                                                   //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

                    //欄寬
                    sheet2.Column(1).Width = 6;
                    sheet2.Column(2).Width = 20;
                    sheet2.Column(3).Width = 15;
                    sheet2.Column(4).Width = 15;
                    sheet2.Column(5).Width = 30;
                    sheet2.Column(6).Width = 20;




                    sheet2.Cells[1, 1, 1, 6].Merge = true;     //合併儲存格
                    sheet2.Cells[1, 1, 1, 6].Value = Title;    //Set the value of cell A1 to 1
                    sheet2.Cells[1, 1, 1, 6].Style.Font.Name = "微軟正黑體";
                    sheet2.Cells[1, 1, 1, 6].Style.Font.Size = 18;
                    sheet2.Cells[1, 1, 1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                    sheet2.Cells[1, 1, 1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

                    sheet2.Cells[2, 1, 2, 6].Style.Font.Name = "微軟正黑體";
                    sheet2.Cells[2, 1, 2, 6].Style.Font.Size = 12;
                    sheet2.Cells[2, 1, 2, 6].Merge = true;
                    sheet2.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    sheet2.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet2.Cells[3, 2, 3, 6].Merge = true;     //合併儲存格
                    sheet2.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
                    sheet2.Cells[3, 2, 3, 2].Style.Font.Size = 12;
                    sheet2.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
                    sheet2.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    sheet2.Cells[4, 1, 4, 6].Style.Font.Name = "微軟正黑體";
                    sheet2.Cells[4, 1, 4, 6].Style.Font.Size = 12;
                    sheet2.Cells[4, 1, 4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet2.Cells[4, 1, 4, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet2.Cells[4, 1, 4, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet2.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet2.Cells[4, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet2.Cells[4, 1].Value = "序號";
                    sheet2.Cells[4, 2].Value = "派遣日期";
                    sheet2.Cells[4, 3].Value = "任務編號";
                    sheet2.Cells[4, 4].Value = "供應商";
                    sheet2.Cells[4, 5].Value = "板數";
                    sheet2.Cells[4, 6].Value = "發包價";

                    if (dt_b != null && dt_b.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_b.Rows.Count; i++)
                        {
                            sheet2.Cells[i + 5, 1].Value = (i + 1).ToString();
                            sheet2.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet2.Cells[i + 5, 2].Value = dt_b.Rows[i]["task_date"] != DBNull.Value ? Convert.ToDateTime(dt_b.Rows[i]["task_date"]).ToString("yyyy/MM/dd") : "";
                            sheet2.Cells[i + 5, 3].Value = dt_b.Rows[i]["task_id"].ToString();
                            sheet2.Cells[i + 5, 4].Value = dt_b.Rows[i]["supplier_name"].ToString();
                            sheet2.Cells[i + 5, 5].Value = Convert.ToInt32(dt_b.Rows[i]["plates"]);
                            sheet2.Cells[i + 5, 5].Style.Numberformat.Format = "#,##0";
                            sheet2.Cells[i + 5, 6].Value = Convert.ToInt32(dt_b.Rows[i]["price"]);
                            sheet2.Cells[i + 5, 6].Style.Numberformat.Format = "#,##0";
                            sheet2.Cells[i + 5, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            if (i == dt_b.Rows.Count - 1 && _info_B != null)
                            {
                                sheet2.Cells[i + 6, 4].Value = "小計";
                                sheet2.Cells[i + 6, 5].Value = _info_B.plates;
                                sheet2.Cells[i + 6, 6].Value = _info_B.subtotal;
                                sheet2.Cells[i + 6, 6].Style.Numberformat.Format = "#,##0";
                                sheet2.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet2.Cells[i + 6, 1, i + 6, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet2.Cells[i + 6, 1, i + 6, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 6, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                                sheet2.Cells[i + 7, 4].Value = "5%營業稅";
                                sheet2.Cells[i + 7, 6].Value = _info_B.tax;
                                sheet2.Cells[i + 7, 6].Style.Numberformat.Format = "#,##0";
                                sheet2.Cells[i + 7, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet2.Cells[i + 7, 1, i + 7, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet2.Cells[i + 7, 1, i + 7, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 7, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                sheet2.Cells[i + 8, 4].Value = "應付帳款";
                                sheet2.Cells[i + 8, 6].Value = _info_B.total;
                                sheet2.Cells[i + 8, 6].Style.Numberformat.Format = "#,##0";
                                sheet2.Cells[i + 8, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet2.Cells[i + 8, 1, i + 8, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet2.Cells[i + 8, 1, i + 8, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet2.Cells[i + 8, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                            }
                        }
                    }


                    //頁尾加入頁次
                    sheet2.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                    sheet2.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");

                    #endregion
                    #region sheet3-C段
                    var sheet3 = pck.Workbook.Worksheets.Add("C段");

                    //檔案邊界
                    sheet3.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet3.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet3.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet3.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet3.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet3.PrinterSettings.HorizontalCentered = true;

                    sheet3.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                                                                                   // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                                                                                   //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

                    //欄寬
                    sheet3.Column(1).Width = 6;
                    sheet3.Column(2).Width = 20;
                    sheet3.Column(3).Width = 15;
                    sheet3.Column(4).Width = 15;
                    sheet3.Column(5).Width = 30;
                    sheet3.Column(6).Width = 20;
                    sheet3.Column(7).Width = 10;
                    sheet3.Column(8).Width = 10;
                    sheet3.Column(9).Width = 10;
                    sheet3.Column(10).Width = 10;
                    sheet3.Column(11).Width = 10;



                    sheet3.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
                    sheet3.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
                    sheet3.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
                    sheet3.Cells[1, 1, 1, 11].Style.Font.Size = 18;
                    sheet3.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                    sheet3.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

                    sheet3.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
                    sheet3.Cells[2, 1, 2, 11].Style.Font.Size = 12;
                    sheet3.Cells[2, 1, 2, 11].Merge = true;
                    sheet3.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    sheet3.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet3.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
                    sheet3.Cells[3, 2, 3, 2].Style.Font.Size = 12;
                    sheet3.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
                    sheet3.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    sheet3.Cells[4, 1, 4, 11].Style.Font.Name = "微軟正黑體";
                    sheet3.Cells[4, 1, 4, 11].Style.Font.Size = 12;
                    sheet3.Cells[4, 1, 4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet3.Cells[4, 1, 4, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet3.Cells[4, 1, 4, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet3.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet3.Cells[4, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    //sheet3.Cells[4, 1].Value = "序號";
                    //sheet3.Cells[4, 2].Value = "發送日期";
                    //sheet3.Cells[4, 3].Value = "配送日期";
                    //sheet3.Cells[4, 4].Value = "客戶代碼";
                    //sheet3.Cells[4, 5].Value = "客戶名稱";
                    //sheet3.Cells[4, 6].Value = "貨號";
                    //sheet3.Cells[4, 7].Value = "發送區";
                    //sheet3.Cells[4, 8].Value = "到著區";
                    //sheet3.Cells[4, 9].Value = "件數";
                    //sheet3.Cells[4, 10].Value = "板數";
                    //sheet3.Cells[4, 11].Value = "發包價";

                    if (dt_c != null && dt_c.Rows.Count > 0)
                    {

                    }


                    //頁尾加入頁次
                    sheet3.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                    sheet3.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");

                    #endregion

                    filename = @"應付帳款" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    //string path = Request.PhysicalApplicationPath + @"files\money\" + filename;
                    Stream stream = File.Create(path + @"\" + filename);
                    pck.SaveAs(stream);
                    stream.Close();

                    //Response.Clear();
                    //Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(supplier_name + "應付帳款" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ".xlsx", Encoding.UTF8));
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.BinaryWrite(pck.GetAsByteArray());
                    //Response.End();
                }
            }
        }
        return filename;
    }

    protected string export_D(string supplier, tbSuppliers _tbSuppliers, string path)
    {
        string filename= string.Empty ;
        DataTable dt_d = null;
        Utility.AccountsReceivableInfo _info_D = new Utility.AccountsReceivableInfo() ;
        string Title = "峻富物流股份有限公司";
        string supplier_name = Suppliers.SelectedItem.Text;

        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            string randomCode = "";
            cmd.CommandText = @"select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
                                where  type = 1  and supplier_code = @supplier_code and close_enddate >= @dates and close_enddate < @datee
                                and src = @src  FOR XML PATH('')),1,1,'') 'randomCode'";
            cmd.Parameters.AddWithValue("@src", "D");
            cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
            cmd.Parameters.AddWithValue("@dates", dates.Text);                                                        //日期起
            cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));  //日期迄
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                randomCode = _dt.Rows[0]["randomCode"].ToString();
                if (randomCode != "")
                    using (SqlCommand cmd_d = new SqlCommand())
                    {
                        cmd_d.CommandText = "usp_GetAccountsPayable_D";
                        cmd_d.CommandType = CommandType.StoredProcedure;
                        cmd_d.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);
                        cmd_d.Parameters.AddWithValue("@randomCode", randomCode);
                        dt_d = dbAdapter.getDataTable(cmd_d);
                        _info_D = Utility.GetSumAccountsPayable("D", dates.Text, datee.Text, null, Suppliers.SelectedValue, randomCode);
                    }
            }
        }
        #endregion

        if (dt_d != null && dt_d.Rows.Count > 0)
        {
            string fullPath = Request.PhysicalApplicationPath + @"report\支出證明單.xlsx";
            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (ExcelPackage pck = new ExcelPackage(fs))
                {
                    #region 請款單
                    var sheet_pay = pck.Workbook.Worksheets[1];
                    sheet_pay.Cells[4, 4].Value = _tbSuppliers.supplier_name;
                    sheet_pay.Cells[5, 4].Value = _info_D.total;
                    sheet_pay.Cells[7, 13].Value = _info_D.subtotal;
                    sheet_pay.Cells[8, 13].Value = _info_D.tax;
                    sheet_pay.Cells[9, 13].Value = _info_D.total;
                    sheet_pay.Cells[10, 7].Value = _tbSuppliers.account;
                    #endregion

                    #region sheet4-專車
                    var sheet4 = pck.Workbook.Worksheets.Add("專車");

                    sheet4.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet4.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet4.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet4.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet4.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet4.PrinterSettings.HorizontalCentered = true;

                    sheet4.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                                                                                   // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                                                                                   //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

                    //欄寬
                    sheet4.Column(1).Width = 6;
                    sheet4.Column(2).Width = 20;
                    sheet4.Column(3).Width = 15;
                    sheet4.Column(4).Width = 15;
                    sheet4.Column(5).Width = 30;
                    sheet4.Column(6).Width = 20;
                    sheet4.Column(7).Width = 10;
                    sheet4.Column(8).Width = 10;
                    sheet4.Column(9).Width = 10;
                    sheet4.Column(10).Width = 10;
                    sheet4.Column(11).Width = 10;



                    sheet4.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
                    sheet4.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
                    sheet4.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
                    sheet4.Cells[1, 1, 1, 11].Style.Font.Size = 18;
                    sheet4.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                    sheet4.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

                    sheet4.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
                    sheet4.Cells[2, 1, 2, 11].Style.Font.Size = 12;
                    sheet4.Cells[2, 1, 2, 11].Merge = true;
                    sheet4.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    sheet4.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet4.Cells[3, 2, 3, 11].Merge = true;     //合併儲存格
                    sheet4.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
                    sheet4.Cells[3, 2, 3, 2].Style.Font.Size = 12;
                    sheet4.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
                    sheet4.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    sheet4.Cells[4, 1, 4, 11].Style.Font.Name = "微軟正黑體";
                    sheet4.Cells[4, 1, 4, 11].Style.Font.Size = 12;
                    sheet4.Cells[4, 1, 4, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet4.Cells[4, 1, 4, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet4.Cells[4, 1, 4, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet4.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet4.Cells[4, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet4.Cells[4, 1].Value = "序號";
                    sheet4.Cells[4, 2].Value = "發送日期";
                    sheet4.Cells[4, 3].Value = "配送日期";
                    sheet4.Cells[4, 4].Value = "客戶代碼";
                    sheet4.Cells[4, 5].Value = "客戶名稱";
                    sheet4.Cells[4, 6].Value = "貨號";
                    sheet4.Cells[4, 7].Value = "發送區";
                    sheet4.Cells[4, 8].Value = "到著區";
                    sheet4.Cells[4, 9].Value = "件數";
                    sheet4.Cells[4, 10].Value = "板數";
                    sheet4.Cells[4, 11].Value = "發包價";

                    if (dt_d != null && dt_d.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt_d.Rows.Count; i++)
                        {
                            sheet4.Cells[i + 5, 1].Value = (i + 1).ToString();
                            sheet4.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet4.Cells[i + 5, 2].Value = dt_d.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt_d.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                            sheet4.Cells[i + 5, 3].Value = dt_d.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt_d.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                            sheet4.Cells[i + 5, 4].Value = dt_d.Rows[i]["客戶代碼"].ToString();
                            sheet4.Cells[i + 5, 5].Value = dt_d.Rows[i]["客戶名稱"].ToString();
                            sheet4.Cells[i + 5, 6].Value = dt_d.Rows[i]["貨號"].ToString();
                            sheet4.Cells[i + 5, 7].Value = dt_d.Rows[i]["發送站"].ToString();
                            sheet4.Cells[i + 5, 8].Value = dt_d.Rows[i]["到著站"].ToString();
                            sheet4.Cells[i + 5, 9].Value = Convert.ToInt32(dt_d.Rows[i]["板數"]);
                            sheet4.Cells[i + 5, 9].Style.Numberformat.Format = "#,##0";
                            sheet4.Cells[i + 5, 10].Value = Convert.ToInt32(dt_d.Rows[i]["件數"]);
                            sheet4.Cells[i + 5, 10].Style.Numberformat.Format = "#,##0";
                            sheet4.Cells[i + 5, 11].Value = Convert.ToInt32(dt_d.Rows[i]["發包價"]);

                            sheet4.Cells[i + 5, 11].Style.Numberformat.Format = "#,##0";
                            sheet4.Cells[i + 5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            if (i == dt_d.Rows.Count - 1 && _info_D != null)
                            {
                                sheet4.Cells[i + 6, 8].Value = "小計";
                                sheet4.Cells[i + 6, 9].Value = _info_D.plates;
                                sheet4.Cells[i + 6, 10].Value = _info_D.pieces;
                                sheet4.Cells[i + 6, 11].Value = _info_D.subtotal;
                                sheet4.Cells[i + 6, 11].Style.Numberformat.Format = "#,##0";
                                sheet4.Cells[i + 6, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet4.Cells[i + 6, 1, i + 6, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet4.Cells[i + 6, 1, i + 6, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 6, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                                sheet4.Cells[i + 7, 8].Value = "5%營業稅";
                                sheet4.Cells[i + 7, 11].Value = _info_D.tax;
                                sheet4.Cells[i + 7, 11].Style.Numberformat.Format = "#,##0";
                                sheet4.Cells[i + 7, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet4.Cells[i + 7, 1, i + 7, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet4.Cells[i + 7, 1, i + 7, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 7, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                sheet4.Cells[i + 8, 8].Value = "應付帳款";
                                sheet4.Cells[i + 8, 11].Value = _info_D.total;
                                sheet4.Cells[i + 8, 11].Style.Numberformat.Format = "#,##0";
                                sheet4.Cells[i + 8, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet4.Cells[i + 8, 1, i + 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet4.Cells[i + 8, 1, i + 8, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet4.Cells[i + 8, 11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                            }
                        }
                    }


                    //頁尾加入頁次
                    sheet4.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                    sheet4.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");

                    #endregion

                    filename = @"應付帳款-專車" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    Stream stream = File.Create(path + @"\" + filename);
                    pck.SaveAs(stream);
                    stream.Close();

                    //Response.Clear();
                    //Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(supplier_name + "應付帳款-專車" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ".xlsx", Encoding.UTF8));
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.BinaryWrite(pck.GetAsByteArray());
                    //Response.End();
                }
            }
        }
            
        return filename;
    }

    protected string  export_E(string supplier, tbSuppliers _tbSuppliers, string path)
    {
        string filename = string.Empty ;
        DataTable dt_e = null;
        Utility.AccountsReceivableInfo _info_E = new Utility.AccountsReceivableInfo();
        string Title = "峻富物流股份有限公司";
        string supplier_name = Suppliers.SelectedItem.Text;

        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            string randomCode = "";
            cmd.CommandText = @"select STUFF((select ',''' + Convert(varchar,randomCode) + ''''  from ttCheckoutCloseLog 
                                where  type = 1  and supplier_code = @supplier_code and close_enddate >= @dates and close_enddate < @datee
                                and src = @src  FOR XML PATH('')),1,1,'') 'randomCode'";
            cmd.Parameters.AddWithValue("@src", "E");
            cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
            cmd.Parameters.AddWithValue("@dates", dates.Text);                                                        //日期起
            cmd.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));  //日期迄
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                randomCode = _dt.Rows[0]["randomCode"].ToString();
                if (randomCode != "")
                    using (SqlCommand cmd_e = new SqlCommand())
                    {
                        cmd_e.CommandText = "usp_GetAccountsPayable_S";
                        cmd_e.CommandType = CommandType.StoredProcedure;
                        cmd_e.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);
                        cmd_e.Parameters.AddWithValue("@randomCode", randomCode);
                        dt_e = dbAdapter.getDataTable(cmd_e);
                        _info_E = Utility.GetSumAccountsPayable("E", dates.Text, datee.Text, null, Suppliers.SelectedValue, randomCode);
                    }
            }
        }
        #endregion

        if (dt_e != null && dt_e.Rows.Count > 0)
        {
            string fullPath = Request.PhysicalApplicationPath + @"report\支出證明單.xlsx";
            using (FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (ExcelPackage pck = new ExcelPackage(fs))
                {
                    //ExcelPackage pck = new ExcelPackage();
                    #region 請款單
                    var sheet_pay = pck.Workbook.Worksheets[1];
                    sheet_pay.Cells[4, 4].Value = _tbSuppliers.supplier_name;
                    sheet_pay.Cells[5, 4].Value = _info_E.total;
                    sheet_pay.Cells[7, 13].Value = _info_E.subtotal;
                    sheet_pay.Cells[8, 13].Value = _info_E.tax;
                    sheet_pay.Cells[9, 13].Value = _info_E.total;
                    sheet_pay.Cells[10, 7].Value = _tbSuppliers.account;
                    #endregion

                    #region sheet5-超商
                    var sheet5 = pck.Workbook.Worksheets.Add("超商");

                    //檔案邊界
                    sheet5.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet5.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet5.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet5.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet5.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet5.PrinterSettings.HorizontalCentered = true;

                    sheet5.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                                                                                   // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                                                                                   //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

                    //欄寬
                    sheet5.Column(1).Width = 6;
                    sheet5.Column(2).Width = 20;
                    sheet5.Column(3).Width = 15;
                    sheet5.Column(4).Width = 15;
                    sheet5.Column(5).Width = 30;
                    sheet5.Column(6).Width = 20;
                    sheet5.Column(7).Width = 10;
                    sheet5.Column(8).Width = 10;
                    sheet5.Column(9).Width = 10;
                    sheet5.Column(10).Width = 10;
                    sheet5.Column(11).Width = 10;
                    sheet5.Column(12).Width = 10;
                    sheet5.Column(13).Width = 10;



                    sheet5.Cells[1, 1, 1, 13].Merge = true;     //合併儲存格
                    sheet5.Cells[1, 1, 1, 13].Value = Title;    //Set the value of cell A1 to 1
                    sheet5.Cells[1, 1, 1, 13].Style.Font.Name = "微軟正黑體";
                    sheet5.Cells[1, 1, 1, 13].Style.Font.Size = 18;
                    sheet5.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                    sheet5.Cells[1, 1, 1, 13].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

                    sheet5.Cells[2, 1, 2, 13].Style.Font.Name = "微軟正黑體";
                    sheet5.Cells[2, 1, 2, 13].Style.Font.Size = 12;
                    sheet5.Cells[2, 1, 2, 13].Merge = true;
                    sheet5.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
                    sheet5.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    sheet5.Cells[3, 2, 3, 13].Merge = true;     //合併儲存格
                    sheet5.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
                    sheet5.Cells[3, 2, 3, 2].Style.Font.Size = 12;
                    sheet5.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
                    sheet5.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


                    sheet5.Cells[4, 1, 4, 13].Style.Font.Name = "微軟正黑體";
                    sheet5.Cells[4, 1, 4, 13].Style.Font.Size = 12;
                    sheet5.Cells[4, 1, 4, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet5.Cells[4, 1, 4, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet5.Cells[4, 1, 4, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet5.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet5.Cells[4, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet5.Cells[4, 1].Value = "序號";
                    sheet5.Cells[4, 2].Value = "日期";
                    sheet5.Cells[4, 3].Value = "日/夜";
                    sheet5.Cells[4, 4].Value = "溫層";
                    sheet5.Cells[4, 5].Value = "車老闆";
                    sheet5.Cells[4, 6].Value = "運務士";
                    sheet5.Cells[4, 7].Value = "車號";
                    sheet5.Cells[4, 8].Value = "車型";
                    sheet5.Cells[4, 9].Value = "主線";
                    sheet5.Cells[4, 10].Value = "爆量";
                    sheet5.Cells[4, 11].Value = "店到店";
                    sheet5.Cells[4, 12].Value = "回頭車";
                    sheet5.Cells[4, 13].Value = "加派車";
                    if (dt_e != null && dt_e.Rows.Count > 0)
                    {

                        for (int i = 0; i < dt_e.Rows.Count; i++)
                        {
                            sheet5.Cells[i + 5, 1].Value = (i + 1).ToString();
                            sheet5.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            sheet5.Cells[i + 5, 2].Value = dt_e.Rows[i]["InDate"] != DBNull.Value ? Convert.ToDateTime(dt_e.Rows[i]["InDate"]).ToString("yyyy/MM/dd") : "";
                            sheet5.Cells[i + 5, 3].Value = dt_e.Rows[i]["DayNight"].ToString();
                            sheet5.Cells[i + 5, 4].Value = dt_e.Rows[i]["Temperature_Text"].ToString();
                            sheet5.Cells[i + 5, 5].Value = dt_e.Rows[i]["PaymentObject"].ToString();
                            sheet5.Cells[i + 5, 6].Value = dt_e.Rows[i]["driver"].ToString();
                            sheet5.Cells[i + 5, 7].Value = dt_e.Rows[i]["car_license"].ToString();
                            sheet5.Cells[i + 5, 8].Value = dt_e.Rows[i]["cabin_type_text"].ToString();
                            sheet5.Cells[i + 5, 9].Value = Convert.ToInt32(dt_e.Rows[i]["expenses_01"]);
                            sheet5.Cells[i + 5, 9].Style.Numberformat.Format = "#,##0";
                            sheet5.Cells[i + 5, 10].Value = Convert.ToInt32(dt_e.Rows[i]["expenses_02"]);
                            sheet5.Cells[i + 5, 10].Style.Numberformat.Format = "#,##0";
                            sheet5.Cells[i + 5, 11].Value = Convert.ToInt32(dt_e.Rows[i]["expenses_03"]);
                            sheet5.Cells[i + 5, 11].Style.Numberformat.Format = "#,##0";
                            sheet5.Cells[i + 5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet5.Cells[i + 5, 12].Value = Convert.ToInt32(dt_e.Rows[i]["expenses_04"]);
                            sheet5.Cells[i + 5, 12].Style.Numberformat.Format = "#,##0";
                            sheet5.Cells[i + 5, 13].Value = Convert.ToInt32(dt_e.Rows[i]["expenses_05"]);
                            sheet5.Cells[i + 5, 13].Style.Numberformat.Format = "#,##0";

                            if (i == dt_e.Rows.Count - 1 && _info_E != null)
                            {
                                sheet5.Cells[i + 6, 12].Value = "小計";
                                sheet5.Cells[i + 6, 13].Value = _info_E.subtotal;
                                sheet5.Cells[i + 6, 13].Style.Numberformat.Format = "#,##0";
                                sheet5.Cells[i + 6, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet5.Cells[i + 6, 1, i + 6, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet5.Cells[i + 6, 1, i + 6, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 6, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                                sheet5.Cells[i + 7, 12].Value = "5%營業稅";
                                sheet5.Cells[i + 7, 13].Value = _info_E.tax;
                                sheet5.Cells[i + 7, 13].Style.Numberformat.Format = "#,##0";
                                sheet5.Cells[i + 7, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet5.Cells[i + 7, 1, i + 7, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet5.Cells[i + 7, 1, i + 7, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 7, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                                sheet5.Cells[i + 8, 12].Value = "應收帳款";
                                sheet5.Cells[i + 8, 13].Value = _info_E.total;
                                sheet5.Cells[i + 8, 13].Style.Numberformat.Format = "#,##0";
                                sheet5.Cells[i + 8, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                sheet5.Cells[i + 8, 1, i + 8, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                sheet5.Cells[i + 8, 1, i + 8, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                sheet5.Cells[i + 8, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                            }
                        }


                        //頁尾加入頁次
                        sheet5.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
                        sheet5.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                    }
                    #endregion

                    filename = @"應付帳款-超商" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
                    Stream stream = File.Create(path + @"\" + filename);
                    pck.SaveAs(stream);

                    stream.Close();
                    //Response.Clear();
                    //Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode(supplier_name + "應付帳款-超商" + DateTime.Now.ToString("yyyy/MM/dd HH:mm") + ".xlsx", Encoding.UTF8));
                    //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    //Response.BinaryWrite(pck.GetAsByteArray());
                    //Response.End();
                }
            }
        }
        return filename;
    }

    private void ZipFiles(string path, string password, string comment)
    {
        string zipPath = path + ".zip";
        //ZipFile zip = new ZipFile();
        //if (password != null && password != string.Empty) zip.Password = password;
        //if (comment != null && comment != "") zip.Comment = comment;
        //ArrayList files = GetFiles(path);
        //foreach (string f in files)
        //{
        //    zip.AddFile(f, string.Empty);//第二個參數設為空值表示壓縮檔案時不將檔案路徑加入
        //}
        //zip.Save(zipPath);

        //System.Text.Encoding.Default解决中文文件夹名称乱码
        using (ZipFile zf = new ZipFile(System.Text.Encoding.Default))
        {
            if (password != null && password != string.Empty) zf.Password = password;
            if (comment != null && comment != "") zf.Comment = comment;
            zf.AddDirectory(path);
            //压缩之后保存路径及压缩文件名
            zf.Save(zipPath);
        }

        #region 刪除暫存資料夾
        if (Directory.Exists(path))
        {
            ArrayList files = GetFiles(path);//找出目錄底下檔案
                                             //逐筆刪除
            foreach (string file in files)
            {
                File.Delete(file);
            }
            Directory.Delete(path);
        }
        #endregion


        string filename = System.IO.Path.GetFileName(zipPath);
        Response.AddHeader("Content-Disposition",string.Format("attachment; filename="+ filename));
        //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
        Response.WriteFile(zipPath);
        Response.End();
    }

    //讀取目錄下所有檔案
    private static ArrayList GetFiles(string path)
    {
        ArrayList files = new ArrayList();

        if (Directory.Exists(path))
        {
            files.AddRange(Directory.GetFiles(path));
        }

        return files;
    }

    public class tbSuppliers
    {
        public string supplier_code { get; set; }
        public string supplier_no { get; set; }
        public string supplier_name { get; set; }
        public string uniform_numbers { get; set; }
        public string principal { get; set; }
        public string contact { get; set; }
        public string account { get; set; }

    }

}