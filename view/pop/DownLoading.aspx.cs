﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Threading;

public partial class view_pop_DownLoading : BasePage		//透過繼承BasePage卡登入，避免外人try路徑，抓取有個資的檔案
{
	protected void Page_Load(object sender, EventArgs e)
	{
		string filename = HttpUtility.UrlDecode(checkGetString(Request["file"]));
		string path = HttpUtility.UrlDecode(checkGetString(Request["path"]));

		//path沒有"\\"結尾時，自動加上
		if (path.EndsWith("\\") == false)
		{
			path += "\\";
		}

		fileDownload(filename, path + filename);
	}

	private void fileDownload(string fileName, string fileUrl)
	{
		Page.Response.Clear();
		bool success = ResponseFile(Page.Request, Page.Response, fileName, fileUrl, 1024000);
		if (!success) Response.Write("找不到檔案！");
		Page.Response.End();
	}

	public static bool ResponseFile(HttpRequest _Request, HttpResponse _Response, string _fileName, string _fullPath, long _speed)
	{
		try
		{
			FileStream myFile = new FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
			BinaryReader br = new BinaryReader(myFile);
			try
			{
				_Response.AddHeader("Accept-Ranges", "bytes");
				_Response.Buffer = false;
				long fileLength = myFile.Length;
				long startBytes = 0;

				int pack = 10240; //10K bytes
				int sleep = (int)Math.Floor((double)(1000 * pack / _speed)) + 1;
				if (_Request.Headers["Range"] != null)
				{
					_Response.StatusCode = 206;
					string[] range = _Request.Headers["Range"].Split(new char[] { '=', '-' });
					startBytes = Convert.ToInt64(range[1]);
				}
				_Response.AddHeader("Content-Length", (fileLength - startBytes).ToString());
				if (startBytes != 0)
				{
					_Response.AddHeader("Content-Range", string.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength));
				}
				_Response.AddHeader("Connection", "Keep-Alive");
				_Response.ContentType = "application/octet-stream";
				_Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8));

				br.BaseStream.Seek(startBytes, SeekOrigin.Begin);
				int maxCount = (int)Math.Floor((double)((fileLength - startBytes) / pack)) + 1;

				for (int i = 0; i < maxCount; i++)
				{
					if (_Response.IsClientConnected)
					{
						_Response.BinaryWrite(br.ReadBytes(pack));
						Thread.Sleep(sleep);
					}
					else
					{
						i = maxCount;
					}
				}
			}
			catch (Exception ex)
			{
				ExceptionAdapter.logSysException(ex);
				return false;
			}
			finally
			{
				br.Close();
				myFile.Close();
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			return false;
		}
		return true;
	}
}