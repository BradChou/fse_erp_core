﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class fileupload_receipt : System.Web.UI.Page
{

    public string check_number
    {   
        get { return ViewState["check_number"].ToString(); }
        set { ViewState["check_number"] = value; }
    }

    public string request_id
    {   
        get { return ViewState["request_id"].ToString(); }
        set { ViewState["request_id"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            check_number = Request.QueryString["check_number"].ToString(); //貨號            
            request_id = Request.QueryString["request_id"].ToString(); //貨號

            if (string.IsNullOrEmpty(check_number) || string.IsNullOrEmpty(request_id))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請先指定貨號');parent.$.fancybox.close();</script>", false);
                return;
            }

        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {   
        if (!Page.IsValid)
            return;

        string fileurl = "";
        string imagess = "";

        int R_count = 1;
        string fileExtension = "";
        bool fileok = false;
        string tErrStr = "";


        string strFileName = "";
        string strimagess = "";

        if (file01.HasFile)
        {
            foreach (HttpPostedFile file in file01.PostedFiles)
            {
                Int32 fileSize = file.ContentLength;
                if (fileSize > 20971520) // 20* 1024 * 1024
                {
                    tErrStr += "檔案 [ " + file.FileName + " ] 限傳5MB   請重新選擇檔案";
                }
                fileExtension = System.IO.Path.GetExtension(file01.FileName).ToLower().ToString();
                //取得檔案格式
                string[] allowedExtensions = { ".gif", ".jpg", ".jpe*", ".png", ".tif", ".bmp", ".pdf" };
                //定義允許的檔案格式
                //逐一檢查允許的格式中是否有上傳的格式
                for (int i = 0; i <= allowedExtensions.Length - 1; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileok = true;
                    }
                }

                if (!fileok)
                {
                    tErrStr = "上傳檔案格式錯誤，請上傳gif、jpg、jpe*、png、tif、bmp、pdf格式";
                }
            }
        }
        else
        {
            if (hidf_id.Value == "")
            {
                tErrStr = "請選擇上傳檔案";
            }
        }

        if (tErrStr == "")
        {
            try
            {
                if (file01.HasFile)
                {
                    foreach (HttpPostedFile file in file01.PostedFiles) {
                        fileurl = "http://220.128.210.180:10080/files/receipt/";
                        string filepath = "~/files/receipt/";
                        string guid = Guid.NewGuid().ToString();
                        imagess = check_number + "_" + guid + fileExtension;
                        if (fileExtension == ".pdf")
                        {
                            file.SaveAs(Server.MapPath(filepath + imagess));
                        }
                        else
                        {
                            byte[] FileBytes;
                            MemoryStream ms = new MemoryStream();
                            file.InputStream.CopyTo(ms);
                            FileBytes = ms.GetBuffer();
                            System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(FileBytes));

                            if (!makethumbnail(image, Server.MapPath(filepath + imagess), 500, 500, "w", "jpg"))
                            {
                                tErrStr = "上傳失敗：";
                            }
                        }
                        strFileName += file.FileName + ";";
                        strimagess += imagess + ";";
                    }
                                        
                    if (tErrStr=="")
                    {
                        #region 
                        SqlCommand cmda = new SqlCommand();
                        DataTable dta = new DataTable();
                        cmda.Parameters.AddWithValue("@check_number", check_number);
                        cmda.Parameters.AddWithValue("@request_id", request_id);
                        cmda.CommandText = "Select log_id from ttDeliveryFileN where check_number=@check_number and request_id=@request_id";
                        dta = dbAdapter.getDataTable(cmda);
                        if (dta.Rows.Count > 0)
                        {
                            using (SqlCommand cmdupd = new SqlCommand())
                            {
                                int log_id = Convert.ToInt32(dta.Rows[0]["log_id"]);
                                cmdupd.Parameters.AddWithValue("@check_number", check_number);               //貨運單號
                                cmdupd.Parameters.AddWithValue("@request_id", request_id);
                                cmdupd.Parameters.AddWithValue("@file_type", "1");                            //1:回單
                                cmdupd.Parameters.AddWithValue("@file_path", fileurl);
                                cmdupd.Parameters.AddWithValue("@file_name", strimagess);
                                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);           //建立人員
                                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                      //建立時間            
                                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "log_id", log_id);
                                cmdupd.CommandText = dbAdapter.genUpdateComm("ttDeliveryFileN", cmdupd);
                                dbAdapter.execNonQuery(cmdupd);
                            }
                        }
                        else
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@check_number", check_number);               //貨運單號
                                cmd.Parameters.AddWithValue("@request_id", request_id);
                                cmd.Parameters.AddWithValue("@file_type", "1");                            //1:回單
                                cmd.Parameters.AddWithValue("@file_path", fileurl);
                                cmd.Parameters.AddWithValue("@file_name", strimagess);
                                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);           //建立人員
                                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                      //建立時間            
                                cmd.CommandText = dbAdapter.SQLdosomething("ttDeliveryFileN", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                        }
                        #endregion

                        
                    }
                    //getpicthumbnail(image, Server.MapPath(filepath + imagess), 400, 400, 50);
                    
                }

            }
            catch (Exception ex)
            {
                tErrStr = ex.Message;
            }
        }


        if (tErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + tErrStr + "');", true);
        }
        else
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('上傳成功');"+
                                                 "self.parent.$('#" + Request.QueryString["RtnImglink"] + "').attr('href','" + fileurl + imagess + "');" +
                                                 "parent.$.fancybox.close();</script>", false);
        }
    }


    /// <summary>
    /// 压缩图片并存储
    /// </summary>
    /// <param name="originalimagepath">源图路径（物理路径）</param>
    /// <param name="thumbnailpath">压缩面存储路径（物理路径）</param>
    /// <param name="width">压缩图宽度</param>
    /// <param name="height">压缩图高度</param>
    /// <param name="mode">生成压缩图的方式</param> 
    /// <param name="type">保存压缩图类型</param> 
    public  bool makethumbnail(System.Drawing.Image originalimagepath, string thumbnailpath, int width, int height, string mode, string type)
    {

        System.Drawing.Image originalimage = originalimagepath;
        bool isture = true; ;
        int towidth = width;
        int toheight = height;
        int x = 0;
        int y = 0;
        int ow = originalimage.Width;
        int oh = originalimage.Height;
        switch (mode)
        {
            case "hw"://指定高宽缩放（可能变形） 
                break;
            case "w"://指定宽，高按比例 
                toheight = originalimage.Height * width / originalimage.Width;
                break;
            case "h"://指定高，宽按比例
                towidth = originalimage.Width * height / originalimage.Height;
                break;
            case "cut"://指定高宽裁减（不变形） 
                if ((double)originalimage.Width / (double)originalimage.Height > (double)towidth / (double)toheight)
                {
                    oh = originalimage.Height;
                    ow = originalimage.Height * towidth / toheight;
                    y = 0;
                    x = (originalimage.Width - ow) / 2;
                }
                else
                {
                    ow = originalimage.Width;
                    oh = originalimage.Width * height / towidth;
                    x = 0;
                    y = (originalimage.Height - oh) / 2;
                }
                break;
            case "db"://等比缩放（不变形，如果高大按高，宽大按宽缩放） 
                if ((double)originalimage.Width / (double)towidth < (double)originalimage.Height / (double)toheight)
                {
                    toheight = height;
                    towidth = originalimage.Width * height / originalimage.Height;
                }
                else
                {
                    towidth = width;
                    toheight = originalimage.Height * width / originalimage.Width;
                }
                break;
            default:
                break;
        }
        //新建一个bmp图片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);
        //新建一个画板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
        //设置高质量插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
        //设置高质量,低速度呈现平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        //清空画布并以透明背景色填充
        g.Clear(System.Drawing.Color.Transparent);
        //在指定位置并且按指定大小绘制原图片的指定部分
        g.DrawImage(originalimage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
        new System.Drawing.Rectangle(x, y, ow, oh),
        System.Drawing.GraphicsUnit.Pixel);
        try
        {
            //保存缩略图
            if (type == "jpg")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            if (type == "bmp")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Bmp);
            }
            if (type == "gif")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Gif);
            }
            if (type == "png")
            {
                bitmap.Save(thumbnailpath, System.Drawing.Imaging.ImageFormat.Png);
            }

            //using (System.IO.MemoryStream oMS = new System.IO.MemoryStream())
            //{
            //    //將oTarImg儲存（指定）到記憶體串流中
            //    bitmap.Save(oMS, System.Drawing.Imaging.ImageFormat.Jpeg);
            //    //將串流整個讀到陣列中，寫入某個路徑中的某個檔案裡
            //    using (System.IO.FileStream oFS = System.IO.File.Open(thumbnailpath, System.IO.FileMode.OpenOrCreate))
            //    { oFS.Write(oMS.ToArray(), 0, oMS.ToArray().Length); }
            //}

        }
        catch (System.Exception e)
        {
            isture = false;
        }
        finally
        {
            originalimage.Dispose();
            bitmap.Dispose();
            g.Dispose();
        }
        return isture;
    }


    /// <summary>
    /// 无损压缩图片
    /// </summary>
    /// <param name="image">原图片</param>
    /// <param name="dfile">压缩后保存位置</param>
    /// <param name="dheight">高度</param>
    /// <param name="dwidth"></param>
    /// <param name="flag">压缩质量 1-100</param>
    /// <returns></returns>
    public bool getpicthumbnail(System.Drawing.Image  image, string dfile, int dheight, int dwidth, int flag)
    {
        System.Drawing.Image originalimage = image;
        System.Drawing.Imaging.ImageFormat  tformat = originalimage.RawFormat;
        int sw = 0, sh = 0;
        //按比例缩放

        System.Drawing.Size tem_size = new System.Drawing.Size(originalimage.Width, originalimage.Height);
        if (tem_size.Width > dheight || tem_size.Width > dwidth) //将**改成c#中的或者操作符号
        {
            if ((tem_size.Width * dheight) > (tem_size.Height * dwidth))
            {
                sw = dwidth;
                sh = (dwidth * tem_size.Height) / tem_size.Width;
            }
            else
            {
                sh = dheight;
                sw = (tem_size.Width * dheight) / tem_size.Height;
            }
        }
        else
        {
            sw = tem_size.Width;
            sh = tem_size.Height;
        }
        System.Drawing.Bitmap  ob = new Bitmap(dwidth, dheight);
        Graphics g = Graphics.FromImage(ob);
        g.Clear(Color.WhiteSmoke );
        g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic ;
        g.DrawImage(originalimage, new System.Drawing.Rectangle((dwidth - sw) / 2, (dheight - sh) / 2, sw, sh), 0, 0, originalimage.Width, originalimage.Height, GraphicsUnit.Pixel);
        g.Dispose();
        //以下代码为保存图片时，设置压缩质量
        EncoderParameters  ep = new EncoderParameters();
        long[] qy = new long[1];
        qy[0] = flag;//设置压缩的比例1-100
        EncoderParameter   eparam = new EncoderParameter( System.Drawing.Imaging.Encoder.Quality , qy);
        ep.Param[0]= eparam;
        try
        {
            ImageCodecInfo[] arrayici = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo jpegiciinfo = null;
            for (int x = 0; x < arrayici.Length; x++)
            {
                if (arrayici[x].FormatDescription.Equals("jpeg"))
                {
                    jpegiciinfo = arrayici[x];
                    break;
                }
            }
            if (jpegiciinfo != null)
            {
                ob.Save(dfile, jpegiciinfo, ep);//dfile是压缩后的新路径
            }
            else
            {
                ob.Save(dfile, tformat);
            }
            return true;
        }
        catch ( Exception ex)
        {
            return false;
        }
        finally
        {
            originalimage.Dispose();
            ob.Dispose();
        }
    }



}