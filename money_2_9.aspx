﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_2_9.aspx.cs" Inherits="money_2_9" EnableEventValidation="false"
      %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">結帳記綠</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label id="lbdaterange" runat="server" class="_daterange" >請款期間</label>
                            <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate" autocomplete="off"></asp:TextBox>
                            ~ 
                            <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate" autocomplete="off"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label for="Suppliers">供應商</label>
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged"></asp:DropDownList>                            
                            <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="查詢" OnClick="btQry_Click" />                            
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Suppliers" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label >結帳類型</label>
                            <asp:DropDownList ID="dlCloseSrc" runat="server" CssClass="form-control ">
                                <asp:ListItem Value="A">A段</asp:ListItem>
                                <asp:ListItem Value="B">B段</asp:ListItem>
                                <asp:ListItem Value="C">C段</asp:ListItem>
                                <asp:ListItem Value="D">專車</asp:ListItem>
                                <asp:ListItem Value="E">超商</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <hr>
                

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <p class="text-primary">
                            供應商：<asp:Label ID="lbSuppliers" runat="server"></asp:Label><br>
                            請款期間：<asp:Label ID="lbdate" runat="server"></asp:Label>
                        </p>
                        <asp:Panel ID="Panel1" runat="server">
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th>序號</th>
                                    <th>供應商</th>
                                    <th>結帳日期</th>
                                    <th>結帳區間</th>
                                    <th>結帳金額</th>
                                    <th></th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="序號">
                                                <%# (((RepeaterItem)Container).ItemIndex+1).ToString() %>                                               
                                            </td>
                                            <td data-th="供應商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                            <td data-th="結帳日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.close_begdate","{0:yyyy/MM/dd}").ToString())%>~<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.close_enddate","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="結帳區間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.close_enddate","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="結帳金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>
                                            <td data-th="">
                                                <asp:HiddenField ID="Hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.log_id").ToString())%>' />
                                                <asp:Button ID="btnDel" runat="server" Text="還原"  CommandName  ="cmdDel" CommandArgument ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.randomCode").ToString())%>' CssClass =" brn btn-danger"  OnClientClick ="return confirm('您確定要還還結帳嗎?');"
                                                    Visible ='<%# (Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.latest")) ==true ) ?true :false  %>' /></td>

                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="6" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

