﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class money_2_9 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = DateTime.Today.AddMonths(-1).ToString("yyyy/MM/") +"01"; //yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }
            cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where ISNULL(supplier_code ,'') <> '' {0} order by supplier_code", wherestr);
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            //if (Request.QueryString["Suppliers"] != null)
            //{
            //    Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            //}
            Suppliers_SelectedIndexChanged(null, null);
            //Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion
            
        }
            
    }

    private void readdata()
    {
        lbSuppliers.Text = Suppliers.SelectedItem.Text;
        lbdate.Text = dates.Text + "~" + datee.Text;

        using (SqlCommand cmd = new SqlCommand())
        {
            string wherestr = "";
            if (!String.IsNullOrEmpty(dates.Text) && !String.IsNullOrEmpty(datee.Text))
            {
                cmd.Parameters.AddWithValue("@dateS", dates.Text);
                cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(datee.Text).AddDays(1).ToString("yyyy/MM/dd"));
                wherestr = " and ((close_begdate >= @dateS and close_begdate < @dateE) or (close_enddate >= @dateS and close_enddate < @dateE))";
            }
            cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);  //供應商            
            cmd.Parameters.AddWithValue("@src", dlCloseSrc.SelectedValue);      //結帳類型            

            cmd.CommandText = string.Format(@"SELECT A.log_id , A.supplier_code , A.close_begdate , A.close_enddate , A.close_enddate , A.price , A.randomCode, A.latest, B.supplier_name
                                              From ttCheckoutCloseLog A with(nolock) 
                                              LEFT JOIN tbSuppliers B WITH(NOLOCK) ON A.supplier_code = B.supplier_code
                                              WHERE type = 1 and A.supplier_code=@supplier and A.src =  @src
                                              {0}", wherestr);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                
                New_List.DataSource = dt;
                New_List.DataBind();
               
            }
        }
    }
    

    protected void btQry_Click(object sender, EventArgs e)
    {
        readdata();
    }

   
    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        if (Suppliers.SelectedValue == "" )
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇供應商');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post_pay";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue );   //供應商
                cmd.Parameters.AddWithValue("@close_type", "2");                      //結帳程序類別(1:自動 2:手動)
                cmd.Parameters.AddWithValue("@close_src", "D");                       //結帳內容(A：A段派遣  B:B段派遣 C:專車 D:超商)
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean (dt.Rows[0]["result"].ToString()) == false )
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            strErr = "acc_post" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            readdata();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }

        return;
    }
    
    [WebMethod]
    public static string GetCloseDayDDLHtml(string dates, string datee,  string supplier_code )
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "" )
            {
                cmd.Parameters.AddWithValue("@supplier", supplier_code);
                wherestr = " and supplier like  @supplier+'%' ";
                cmd.Parameters.AddWithValue("@dates", dates  + " 00:00:00.000");  //日期起
                cmd.Parameters.AddWithValue("@datee", datee  + " 23:59:59.997");  //日期迄
                cmd.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from ttAssetsSB with(nolock)   
                                                   where 0=0 AND  checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    //sb_html.Append(@"<option  value=''>請選擇</option>");

                    foreach (DataRow row in dt.Rows)
                    {
                        sb_html.Append(@"<option value='" + row["checkout_close_date"].ToString() + "'>" + row["checkout_close_date"].ToString() + "</option>");
                    }

                }
            }
            
        }

        return sb_html.ToString();
    }
    

    protected void close_date_SelectedIndexChanged(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }
    
    
    protected void close_date_SelectedIndexChanged1(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string  strErr="";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (e.CommandName == "cmdDel")
        {
            //還原結帳
            string randomCode = e.CommandArgument.ToString();
            int log_id = 0;
            int.TryParse (((HiddenField)e.Item.FindControl("Hid_id")).Value.ToString().Trim(), out log_id);
            if (log_id > 0)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "acc_post_del";
                    cmd.CommandType = CommandType.StoredProcedure;
                    string customer_code = Suppliers.SelectedValue;
                    cmd.Parameters.AddWithValue("@log_id", log_id);  //結帳檔編號                    
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                            {
                                strErr = dt.Rows[0]["ErrMsg"].ToString();
                            }

                        }

                    }
                }
            }
            else
            {
                strErr = "結帳編號有誤";
                
            }

            if (strErr == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('還原成功');</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('還原失敗:" + strErr + "');</script>", false);
            }
            
            return;
        }


    }
}