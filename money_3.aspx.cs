﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class money_3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Boolean IsManager = false;
            if (Session["account_code"] != null & Session["account_code"].ToString() == "skyeyes") IsManager = true;

            div_oil.Visible =
            btnsave.Visible = false;
            if (DateTime.Now.Day >= 20 && DateTime.Now.Day <= 25
                || IsManager)
            {
                div_oil.Visible =
                btnsave.Visible = true;
                SetOilData();
            }

            btLog_Click(null, null);


        }

    }

    /// <summary>設定油價相關項目</summary>
    protected void SetOilData()
    {
        int i_week = DateTime.Now.GetWeekNumOfMonth();
        lbl_maxWeek.Text = i_week.ToString();//本月總週數

        switch (i_week)
        {
            case 5:
                new_Oil6.Visible =
                req_w6.Visible = false;
                break;
            case 6:
                break;
            default:
                new_Oil5.Visible =
                new_Oil6.Visible =
                req_w5.Visible =
                req_w6.Visible = false;
                break;
        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (class_level.Text != "")
        {
            Double d_tmp = 0;
            #region 新增                
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@class_level", class_level.Text);                  //級距
            cmd.Parameters.AddWithValue("@active_date", active_date.Text);                  //生效日

            //油價 1~6週
            cmd.Parameters.AddWithValue("@week1", new_Oil1.Text);
            cmd.Parameters.AddWithValue("@week2", new_Oil2.Text);
            cmd.Parameters.AddWithValue("@week3", new_Oil3.Text);
            cmd.Parameters.AddWithValue("@week4", new_Oil4.Text);

            if (!Double.TryParse(new_Oil5.Text, out d_tmp)) d_tmp = 0;
            cmd.Parameters.AddWithValue("@week5", d_tmp.ToString());

            if (!Double.TryParse(new_Oil6.Text, out d_tmp)) d_tmp = 0;
            cmd.Parameters.AddWithValue("@week6", d_tmp.ToString());

            cmd.Parameters.AddWithValue("@arrive", txt_avgoil.Text);                     //平均油價
            cmd.Parameters.AddWithValue("@notify_flag", "0");                            //通知與否
            //cmd.Parameters.AddWithValue("@notify_time", "");                           //通知時間
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);              //建立人員
            //cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                         //建檔日
            cmd.CommandText = dbAdapter.SQLdosomething("ttPriceClassLog", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            #endregion

            btLog_Click(null, null);
            new_Oil1.Text =
            new_Oil2.Text =
            new_Oil3.Text =
            new_Oil4.Text =
            new_Oil5.Text =
            new_Oil6.Text =
            new_avgOil.Text = "";

            active_date.Text = "";
            class_level.Text = "";
            lbl_level.Text = "";
            lbl_maxWeek.Text = "";

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);

            return;
        }
        else
        {
            class_level.Focus();

        }

    }

    /// <summary>取得級距</summary>
    /// <param name="oil">平均油價</param>
    /// <returns>級距</returns>
    public static int OilLevel(Double oil)
    {
        int level = 0;
        if (oil > 0)
        {
            SqlCommand cmdt = new SqlCommand();
            cmdt.CommandText = " select class_level  from tbOilClass With(Nolock) where min_price <='" + oil.ToString() + "' and max_price >='" + oil.ToString() + "'" +
                               " and class_level in(select class_level from tbPriceSupplier With(Nolock) group by class_level )";
            using (DataTable dtt = dbAdapter.getDataTable(cmdt))
            {
                if (dtt.Rows.Count > 0)
                {
                    level = (int)dtt.Rows[0]["class_level"];
                }
            }
            cmdt.Dispose();
        }
        return level;
    }

    protected void btLog_Click(object sender, EventArgs e)
    {
        string wherestr = "";
        SqlCommand cmd = new SqlCommand();

        //cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where 0=0 {0} order by supplier_code", wherestr);
        cmd.CommandText = string.Format(@"select A.seq,A.class_level,A.active_date,A.week1,A.week2,A.week3,A.week4,A.week5,A.week6,A.arrive,A.cdate 
                                                ,CASE WHEN (Len(B.user_name)>=3) THEN SUBSTRING( B.user_name,1,1 )+'*'+ SUBSTRING( B.user_name,3,Len(B.user_name)) 
                                                      WHEN (Len(B.user_name)=2) THEN SUBSTRING( B.user_name,1,1 )+'*'
                                                      ELSE B.user_name END 'user_name' 
                                                ,CASE WHEN A.notify_flag = '1' then '是' else '否' end as notify
                                          from ttPriceClassLog A WIth(Nolock) left join tbAccounts B With(Nolock)on A.cuser=B.account_code   where 0=0 {0} order by active_date desc", wherestr);
        New_List.DataSource = dbAdapter.getDataTable(cmd);
        New_List.DataBind();
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdmail")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@seq", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.seq=@seq";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM ttPriceClassLog A  
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                #region 寄發通知信
                string mailbody = "您好：<br/>新油價級距變更為：" + DT.Rows[0]["class_level"] + "<br/>將於" + Convert.ToDateTime(DT.Rows[0]["active_date"].ToString()).ToString("yyyy/MM/dd") + "啟用" +
                                  "<br/><br/><br/>******本郵件為系統自動發送，請勿直接回覆******";

                string ttErrStr = "";
                try
                {
                    MailSend.sendMail(System.Web.Configuration.WebConfigurationManager.AppSettings["Question_sendmail"], "karen@skyeyes.tw", "【系統測試】峻富物流管理油價級距變更通知", "karen" + mailbody.ToString(), "", "", "", "", "");
                    MailSend.sendMail(System.Web.Configuration.WebConfigurationManager.AppSettings["Question_sendmail"], "Selina@skyeyes.tw", "【系統測試】峻富物流管理油價級距變更通知", "何 怡真" + mailbody.ToString(), "", "", "", "", "");
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }


                SqlCommand cmd2 = new SqlCommand();
                cmd2.Parameters.AddWithValue("@notify_flag", "1");                            //通知與否
                cmd2.Parameters.AddWithValue("@notify_time", DateTime.Now);                   //通知時間
                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                cmd2.CommandText = dbAdapter.genUpdateComm("ttPriceClassLog", cmd2);   //修改
                dbAdapter.execNonQuery(cmd2);


                #endregion

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='money_3.aspx';alert('寄送完畢');</script>", false);

                return;

            }



        }
    }

    /// <summary>動態計算油價資訊</summary>
    /// <param name="oil"></param>
    [WebMethod]
    public static String GetOilAvg(string[] oil)
    {
        OilResult this_oil = new OilResult();
        try
        {
            DateTime dt_now = DateTime.Now;
            this_oil.weeknum = dt_now.GetWeekNumOfMonth();//本月總週數
            int i_DayMax = dt_now.GetDaysOfMonth();//本月總天數
            if (oil.Length == this_oil.weeknum)
            {
                int[] daylist = dt_now.GetWeekDaysInMonth();//每週天數
                Double[] _oilPrice = Array.ConvertAll(oil, s => Double.Parse(s));//每週油價

                for (int i = 0; i < this_oil.weeknum; i++)
                {
                    this_oil.avgoil += (daylist[i] * _oilPrice[i]);
                }
                this_oil.avgoil = this_oil.avgoil / i_DayMax;

                //小數第2位4捨5入
                this_oil.avgoil = Math.Round(this_oil.avgoil, 2, MidpointRounding.AwayFromZero);
                this_oil.level = OilLevel(this_oil.avgoil);
                if (this_oil.level > 0)
                {
                    this_oil.state = 1;
                    this_oil.reason = "ok";
                }
                else
                {
                    this_oil.reason = "此平均油價不在可用級距中，請重新設定油價!";
                }
            }
        }
        catch (Exception ex)
        {
            this_oil.reason = "資訊異常，請重新確認 ..." + ex.ToString();
            this_oil.state = 0;
            this_oil.avgoil = 0;
        }


        return new JavaScriptSerializer().Serialize(this_oil);
    }



    public class OilResult
    {
        /// <summary>平均油價</summary>
        public double avgoil { get; set; }
        /// <summary>狀態1:成功 0:失敗 </summary>
        public int state { get; set; }
        /// <summary>原因:成功→OK ; 失敗→失敗原因</summary>
        public string reason { get; set; }

        /// <summary>本月總週數</summary>
        public int weeknum { get; set; }

        /// <summary>級距</summary>
        public int level { get; set; }

        public OilResult()
        {
            avgoil = 0;
            state = 0;
            reason = "油價資訊不齊全";
            weeknum = 0;
            level = 0;
        }
    }
}