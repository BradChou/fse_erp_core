﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class report_3_7 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }
    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = Session["master_code"].ToString();
            master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員


            ddlRtype.Items.Insert(0, new ListItem("全部", ""));
            ddlRtype.Items.Insert(1, new ListItem("退貨", "1"));
            ddlRtype.Items.Insert(2, new ListItem("來回件", "2"));


            if (Less_than_truckload == "0")
            {
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000";
                            break;
                    }
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    string wherestr = "";
                    if (supplier_code != "")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and supplier_code = @supplier_code";
                    }
                    if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                    {
                        cmd2.Parameters.AddWithValue("@master_code", master_code);
                        wherestr += " and customer_code like @master_code+'%' ";
                    }


                    cmd2.CommandText = string.Format(@"Select customer_code , customer_code + '-' + customer_shortname as name  from tbCustomers with(Nolock) where 0=0 and stop_shipping_code = '0'  {0} order by customer_code asc ", wherestr);
                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "name";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
                }

            }
            else
            {
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                        break;
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    string wherestr = "";
                    cmd2.Parameters.AddWithValue("@type", "1");
                    if (supplier_code != "")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and supplier_code = @supplier_code";
                    }
                    if (manager_type == "5")
                    {
                        cmd2.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                        wherestr += "  and customer_code like ''+ @customer_code + '%'";
                    }
                    cmd2.CommandText = string.Format(@"
WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers with(nolock) where 1=1 and supplier_code <> '001' and stop_shipping_code = '0'
                                               and type = @type
{0}
                                            )
 SELECT supplier_id, master_code, customer_name , master_code+ second_code+ '-' +  customer_name as showname,
master_code+ second_code as customer_code
                                            FROM cus
                                            order by  master_code", wherestr);
                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "showname";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("全選", ""));
                }

            }


            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.Now.Day.ToString();  //DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            mm = mm.Length < 2 ? "0" + mm : mm;
            dates.Text = yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;



            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if (Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }

            //readdata();

        }
    }

    private void readdata()
    {
        string manager_type = Session["manager_type"].ToString(); //管理單位類別  
        string customer_code = Session["master_code"].ToString();

        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                customer_code = "";
                break;
            default:
                customer_code = customer_code.Substring(0, 3);
                break;
        }
        uc_PeidaReport1.readdata(dates.Text , datee.Text, Less_than_truckload, customer_code , dlcustomer_code.Text ,"R");
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
        //paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/trace_3.aspx?search=yes" + querystring));
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("配達統計");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 15;
            sheet1.Column(8).Width = 15;
            sheet1.Column(9).Width = 15;
            sheet1.Column(10).Width = 15;
            sheet1.Column(11).Width = 15;
            sheet1.Column(12).Width = 15;
            sheet1.Column(13).Width = 15;
            sheet1.Column(14).Width = 15;


            sheet1.Cells[1, 1, 1, 14].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 14].Value = "配達統計";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 14].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 14].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 14].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 14].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 14].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "發送日期：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            sheet1.Cells[3, 1,3, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1,3, 14].Style.Font.Size = 12;
            sheet1.Cells[3, 1,3, 14].Style.Font.Bold = true;
            sheet1.Cells[3, 1,3, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 14].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                   
            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "配送站所";
            sheet1.Cells[3, 3].Value = "應配筆數";
            sheet1.Cells[3, 4].Value = "配送筆數";
            sheet1.Cells[3, 5].Value = "午前配達筆數";
            sheet1.Cells[3, 6].Value = "午前配達率";
            sheet1.Cells[3, 7].Value = "當日配達筆數";
            sheet1.Cells[3, 8].Value = "當日配達率";
            sheet1.Cells[3, 9].Value = "已配達筆數";
            sheet1.Cells[3, 10].Value = "配達率";
            sheet1.Cells[3, 11].Value = "未配達筆數";
            sheet1.Cells[3, 12].Value = "異常率";
            sheet1.Cells[3, 13].Value = "首筆配達";
            sheet1.Cells[3, 14].Value = "末筆配達";
           

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["area_arrive_name"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["應配筆數"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["配送筆數"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["午前配達筆數"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["noonRate"].ToString();
                sheet1.Cells[i + 4, 7].Value = DT.Rows[i]["當日配達筆數"].ToString();
                sheet1.Cells[i + 4, 8].Value = DT.Rows[i]["dayRate"].ToString();
                sheet1.Cells[i + 4, 9].Value = DT.Rows[i]["已配達筆數"].ToString();
                sheet1.Cells[i + 4, 10].Value = DT.Rows[i]["yesRate"].ToString();
                sheet1.Cells[i + 4, 11].Value = DT.Rows[i]["noArrive"].ToString();
                sheet1.Cells[i + 4, 12].Value = DT.Rows[i]["noRate"].ToString();
                sheet1.Cells[i + 4, 13].Value = DT.Rows[i]["MIN"].ToString();
                sheet1.Cells[i + 4, 14].Value = DT.Rows[i]["MAX"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=report.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}