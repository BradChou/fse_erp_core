﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using RestSharp;
using System.Net;

public partial class LT_transport_2_edit : System.Web.UI.Page
{
    bool check_address = true;
    string area_arrive_code = "";
    string send_station_scode = "";
    string station_code = "";
    string product_type = "";
    bool bool_is_new_customer = false;
    string strcustomer_code = "";
    string default_pieces = "";

    public string request_id
    {
        get { return ViewState["request_id"].ToString(); }
        set { ViewState["request_id"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            int i_type = 0;
            int i_id = 0;
            string sScript = string.Empty;
            if (Request.QueryString["request_id"] != null) request_id = Request.QueryString["request_id"].ToString();
            if (Request.QueryString["r_type"] != null)
            {
                if (!int.TryParse(Request.QueryString["r_type"].ToString(), out i_type)) i_type = 0;
            }

            if (Request.QueryString["BK"] != null)
            {
                BK.Visible = true;
                btncancel.Visible = false;
                btnsave.Visible = false;
            }
            else
            {
                BK.Visible = false;
            }



            if (!int.TryParse(Request.QueryString["request_id"].ToString(), out i_id) || i_id <= 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('參數異常，請重新確認!');parent.$.fancybox.close();</script>", false);
                return;
            }
            else
            {
                DefaultSet();
                readdata(i_type, request_id);



                sScript += "$('#btCancel').click(function(){" +
                                 " if(confirm('您確定要銷單嗎 ?'))" +
                                 " {$('#btCancel').attr('href', 'CancelDialog.aspx?id=" + hid_id.Value + "');}" +
                                 " }); ";
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript  + "</script>", false);
                //return;
            }


            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.CommandText = string.Format("select latest_scan_item, s.receive_option as 'receive_option' from tcDeliveryRequests R with(nolock) left join ttDeliveryScanLog S with(nolock) on s.check_number = r.check_number and(r.latest_scan_date = s.scan_date) where  r.check_number in (select check_number from tcDeliveryRequests where request_id = " + request_id + ") ");
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows[0]["latest_scan_item"].ToString() == "5" & dt.Rows[0]["receive_option"].ToString() == "20")
            {
                plCancel.Style.Add("display", "none");
            }
            else if (dt.Rows[0]["latest_scan_item"].ToString() == "6")
            {
                plCancel.Style.Add("display", "none");
            }
            else if (dt.Rows[0]["latest_scan_item"].ToString() == "7")
            {
                plCancel.Style.Add("display", "none");
            }
            else if (dt.Rows[0]["latest_scan_item"].ToString() == "1")
            {
                plCancel.Style.Add("display", "none");
            }
            else if (dt.Rows[0]["latest_scan_item"].ToString() == "2")
            {
                plCancel.Style.Add("display", "none");
            }
            else if (dt.Rows[0]["latest_scan_item"].ToString() == "3")
            {
                plCancel.Style.Add("display", "none");
            }

            else
            {
                plNotCancel.Style.Add("display", "none");

            }

            //客戶帳號到著站選擇隱藏

            string[] serivce_code = { "9990193", "9990078", "9990172", "9990223", "9990224", "9990301", "9990331", "9990250", "skyeyes" };

            foreach (string d in serivce_code)
            {
                if (Session["account_code"].ToString().Contains(d))
                {
                    lb_area_arrive_code.Style.Add("pointer-events", "auto");
                }

            }



            //1.配達，且配達區分 = 正常配交，就不可改指定日期
            //2.指配日期不可以設定今天以前(含)的日期
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.Parameters.AddWithValue("@request_id", request_id);
                cmd2.CommandText = " select log_id from ttDeliveryScanLog With(Nolock) where check_number = (select check_number from  tcDeliveryRequests With(Nolock) WHERE  request_id=@request_id ) and scan_item = '3' and arrive_option = '3' ";

                DataTable dtb;
                dtb = dbAdapter.getDataTable(cmd2);
                if (dtb != null && dtb.Rows.Count > 0)
                {
                    arrive_assign_date.Enabled = false;
                }
            }

            holidaydelivery.Enabled = true;

            //
            using (SqlCommand cmd3 = new SqlCommand())
            {
                cmd3.Parameters.AddWithValue("@request_id", request_id);
                cmd3.CommandText = " select latest_scan_item, latest_arrive_option from tcDeliveryRequests A left join ttDeliveryScanLog B with(nolock) on B.check_number = A.check_number where request_id = @request_id ";

                DataTable dtb;
                dtb = dbAdapter.getDataTable(cmd3);
                if (dtb.Rows[0]["latest_scan_item"].ToString() == "3" & dtb.Rows[0]["latest_arrive_option"].ToString() == "3")
                {
                    btnsave.Style.Add("display","none");
                    arrive_option.Style.Add("display", "inline-block");
                }
            }

            sScript += "$('.date_picker').datepicker({ dateFormat: 'yy/mm/dd', minDate: 0});";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
            return;

        }
    }


    /// <summary>資料載入</summary>
    /// <param name="i_type">0:託運單修改 1:託運轉址 2: 託運單瀏覽</param>
    /// <param name="str_id">託運單id</param>
    private void readdata(int i_type = 0, string str_id = null)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@request_id", request_id);
                cmd.CommandText = " SELECT * FROM tcDeliveryRequests With(Nolock) WHERE request_id = @request_id ";

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    int i_tmp = 0;
                    DateTime dt_tmp = new DateTime();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string DeliveryType = dt.Rows[0]["DeliveryType"] != DBNull.Value ? dt.Rows[0]["DeliveryType"].ToString() : "";
                        dlDeliveryType.SelectedValue = DeliveryType;
                        switch (DeliveryType)
                        {
                            case "D":
                                lbDeliveryType.Text = "出貨";
                                break;
                            case "R":
                                lbDeliveryType.Text = "回收";
                                dlDeliveryType.ForeColor = System.Drawing.Color.Red;
                                break;
                        }
                        //rbcheck_type.SelectedValue = dt.Rows[0]["pricing_type"].ToString();              //託運類別 (計價模式 01:論板、02:論件、03論才、04論小板)
                        rbcheck_type_SelectedIndexChanged(null, null);
                        if (!DateTime.TryParse(dt.Rows[0]["print_date"].ToString(), out dt_tmp)) Shipments_date.Text = ""; //出貨日期
                        else Shipments_date.Text = dt_tmp.ToString("yyyy/MM/dd");
                        if (Convert.ToBoolean(dt.Rows[0]["print_flag"]) == true)
                        {
                            Shipments_date.Enabled = false;
                        }
                        dlcustomer_code.SelectedValue = dt.Rows[0]["customer_code"].ToString();          //客代編號
                        customer_code.Text = dt.Rows[0]["customer_code"].ToString();

                        //顯示寄件人資料
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            DataTable dta;
                            cmda.CommandText = @"select C.individual_fee from tbCustomers C With(Nolock) 
                                                where customer_code = '" + customer_code.Text + "' ";
                            dta = dbAdapter.getDataTable(cmda);
                            if (dta.Rows.Count > 0)
                            {
                                bool individualfee = dta.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dta.Rows[0]["individual_fee"]) : false;   //是否個別計價
                                //bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
                                //if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                                if (individualfee)
                                {
                                    individual_fee.Style.Add("display", "block");
                                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                                }
                                else
                                {
                                    individual_fee.Style.Add("display", "none");
                                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                                }
                            }
                        }




                        #region 1. 基本設定
                        hid_id.Value = dt.Rows[0]["request_id"].ToString();
                        lbcheck_number.Text = dt.Rows[0]["check_number"].ToString();                     //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
                        order_number.Text = dt.Rows[0]["order_number"].ToString();                       //訂單號碼
                        check_type.SelectedValue = dt.Rows[0]["check_type"].ToString();                  //託運類別
                        receive_customer_code.Text = dt.Rows[0]["receive_customer_code"].ToString();     //收貨人編號
                        subpoena_category.SelectedValue = dt.Rows[0]["subpoena_category"].ToString();    //傳票類別
                        #endregion                        

                        #region 3.才件代收
                        if (!String.IsNullOrEmpty(dt.Rows[0]["pieces"].ToString()) || !IsNumeric(dt.Rows[0]["pieces"].ToString())) //件數
                        {
                            pieces.Text = dt.Rows[0]["pieces"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["plates"].ToString()) || !IsNumeric(dt.Rows[0]["plates"].ToString())) //板數
                        {
                            plates.Text = dt.Rows[0]["plates"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["cbm"].ToString()) || !IsNumeric(dt.Rows[0]["cbm"].ToString())) //才數
                        {
                            cbm.Text = dt.Rows[0]["cbm"].ToString();
                        }

                        dlProductName.SelectedValue = dt.Rows[0]["ProductId"].ToString();
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            DataTable dta;
                            cmda.CommandText = @"select Name,ProductId from ProductManage
                                                where ProductId = '" + dt.Rows[0]["ProductId"].ToString() + "' ";
                            dta = dbAdapter.getDataTableForFSE01(cmda);
                            if (dta.Rows.Count > 0)
                            {
                                dlProductName.SelectedValue = dta.Rows[0]["ProductId"].ToString();          //產品名稱
                            }
                        }

                        using (SqlCommand cmdb = new SqlCommand())
                        {
                            DataTable dtb;
                            cmdb.CommandText = @"select CodeId from ItemCodes
                                                where CodeId = '" + dt.Rows[0]["SpecCodeId"].ToString() + "' ";
                            dtb = dbAdapter.getDataTableForFSE01(cmdb);
                            if (dtb.Rows.Count > 0)
                            {
                                dlProductSpec.SelectedValue = dtb.Rows[0]["CodeId"].ToString();         //規格
                            }
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["collection_money"].ToString()) || !IsNumeric(dt.Rows[0]["collection_money"].ToString())) //$代收金
                        {
                            collection_money.Text = dt.Rows[0]["collection_money"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["ReportFee"].ToString()) || !IsNumeric(dt.Rows[0]["ReportFee"].ToString())) //報值金額
                        {
                            Report_fee.Text = dt.Rows[0]["ReportFee"].ToString();
                        }

                        //if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_freight"].ToString()) || !IsNumeric(dt.Rows[0]["arrive_to_pay_freight"].ToString())) //到付運費
                        //{
                        //    arrive_to_pay_freight.Text = dt.Rows[0]["arrive_to_pay_freight"].ToString();
                        //}


                        //if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_append"].ToString()) || !IsNumeric(dt.Rows[0]["arrive_to_pay_append"].ToString())) //到付追加
                        //{
                        //    arrive_to_pay_append.Text = dt.Rows[0]["arrive_to_pay_append"].ToString();
                        //}
                        #endregion

                        #region 特殊設定
                        //product_category.SelectedValue = dt.Rows[0]["product_category"].ToString();   //商品種類
                        //arrive_mobile.Text = dt.Rows[0]["arrive_mobile"].ToString();                  //收件人-手機1 
                        //special_send.SelectedValue = dt.Rows[0]["special_send"].ToString();             //特殊配送                       

                        ArticleNumber.Text = dt.Rows[0]["ArticleNumber"].ToString();  //產品編號
                        SendPlatform.Text = dt.Rows[0]["SendPlatform"].ToString();    //出貨平台
                        ArticleName.Text = dt.Rows[0]["ArticleName"].ToString();      //品名
                        bagno.Text = dt.Rows[0]["bagno"].ToString();  //代號
                                                                      //指定日
                        if (!DateTime.TryParse(dt.Rows[0]["arrive_assign_date"].ToString(), out dt_tmp)) arrive_assign_date.Text = "";
                        else arrive_assign_date.Text = dt_tmp.ToString("yyyy/MM/dd");

                        holidaydelivery.Checked = dt.Rows[0]["holiday_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["holiday_delivery"]) : false; //是否假日配

                        time_period.SelectedValue = dt.Rows[0]["time_period"].ToString();              //時段
                        invoice_desc.Value = dt.Rows[0]["invoice_desc"].ToString();                    //說明    

                        //if (!int.TryParse(dt.Rows[0]["receipt_flag"].ToString(), out i_tmp)) i_tmp = 0;
                        //receipt_flag.Checked = i_tmp == 1 ? true : false; //是否回單

                        receipt_round_trip.Checked = dt.Rows[0]["round_trip"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["round_trip"]) : false;        //是否來回件
                        receipt_flag.Checked = dt.Rows[0]["receipt_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["receipt_flag"]) : false;        //是否回單
                        WareHouse.Checked = dt.Rows[0]["WareHouse"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["WareHouse"]) : false;
                        if (!string.IsNullOrEmpty(dt.Rows[0]["ProductValue"].ToString()) && dt.Rows[0]["ProductValue"].ToString() != "0")   //保值費欄位不為null、空值或0時
                        {
                            chkProductValue.Checked = true;        //是否有保值費
                            tbProductValue.Text = dt.Rows[0]["ProductValue"].ToString();   //保值費
                        }
                        Report_fee.Text= dt.Rows[0]["ReportFee"].ToString();   //報值金額
                        //pallet_recycling_flag.Checked = dt.Rows[0]["pallet_recycling_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["pallet_recycling_flag"]) : false;        //是否棧板回收
                        //if (pallet_recycling_flag.Checked)
                        //{
                        //    Pallet_type_div.Attributes.Add("style", "display:block");
                        //    Pallet_type_text.ReadOnly = true;
                        //    Pallet_type_text.Text = Func.GetRow("code_name", "tbItemCodes", "", " code_bclass = '2' and code_sclass = 'S8' and code_id=@code_id", "code_id", dt.Rows[0]["Pallet_type"].ToString());
                        //    Pallet_type.Value = dt.Rows[0]["Pallet_type"].ToString();
                        //}
                        //turn_board.Checked = dt.Rows[0]["turn_board"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["turn_board"]) : false;                        //翻版
                        //upstairs.Checked = dt.Rows[0]["upstairs"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["upstairs"]) : false;                             //上樓
                        //difficult_delivery.Checked = dt.Rows[0]["difficult_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["difficult_delivery"]) : false;//困配
                        //turn_board_fee.Text = turn_board.Checked ? dt.Rows[0]["turn_board_fee"].ToString() : "";                        //翻版
                        //upstairs_fee.Text = upstairs.Checked ? dt.Rows[0]["upstairs_fee"].ToString() : "";                            //上樓
                        //difficult_fee.Text = difficult_delivery.Checked ? dt.Rows[0]["difficult_fee"].ToString() : "";                 //困配
                        //turn_board_fee.Enabled = turn_board.Checked;
                        //upstairs_fee.Enabled = upstairs.Checked;
                        //difficult_fee.Enabled = difficult_delivery.Checked;
                        #endregion


                        #region 修改紀錄


                        SqlCommand cmd4 = new SqlCommand();
                        cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                        cmd4.CommandText = "SELECT c.uuser + ' ' + t.user_name as uuser, * FROM revise_data_log C With(Nolock) left join tbaccounts t with(nolock) on t.account_code = c.uuser WHERE c.check_number = @check_number order by c.udate desc";

                        using (DataTable dt4 = dbAdapter.getDataTable(cmd4))
                        {
                            Delivery_Info_Repeater.DataSource = dt4;
                            Delivery_Info_Repeater.DataBind();

                        }

                        #endregion

                        /*新託運單:寄件框=原收件人框
                          收件框全清空
                          sub_check_number = "001"
                          唯讀→ 託運類別、 基本設定、才件代收、特殊設定
                          其餘可改
                         */


                        switch (i_type)
                        {
                            case 1:

                                #region 新增轉址
                                plCancel.Visible = false;
                                //sub_check_number = 001、002 ...
                                if (!int.TryParse(dt.Rows[0]["sub_check_number"].ToString(), out i_tmp)) i_tmp = 0;
                                lbl_sub_check_number.Text = String.Format("{0:000}", (i_tmp + 1));


                                //收件人-清空
                                receive_tel1.Text = "";               //電話1
                                receive_tel1_ext.Text = "";           //電話分機
                                receive_tel2.Text = "";               //電話2
                                receive_contact.Text = "";            //收件人
                                receive_city.SelectedValue = "";      //收件地址-縣市     
                                receive_area.SelectedValue = "";      //收件地址-鄉鎮市區  
                                area_arrive_code = "";  //到著碼                                
                                cbarrive.Checked = false;             //到站領貨 
                                receive_address.Text = "";

                                //寄件框 填 原收件人框
                                send_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                                send_tel.Text = dt.Rows[0]["receive_tel1"].ToString();
                                send_city.SelectedValue = dt.Rows[0]["receive_city"].ToString();
                                city_SelectedIndexChanged(send_city, null);
                                send_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();
                                send_address.Text = dt.Rows[0]["receive_address"].ToString();

                                ReadOnlySet(false);

                                #endregion

                                break;
                            default:
                                #region 託運單修改、瀏覽

                                #region  銷單資訊
                                if (dt.Rows[0]["cancel_date"] == DBNull.Value || (dt.Rows[0]["cancel_date"].ToString() == ""))
                                {
                                    plCancel.Visible = true;
                                    btnsave.Visible = true;
                                    if (Request.QueryString["BK"] != null)
                                    {
                                        btnsave.Visible = false;
                                        plCancel.Visible = false;
                                    }


                                }
                                else
                                {
                                    plCancel.Visible = false;
                                    btnsave.Visible = false;
                                    div_cancel.Visible = true;
                                    liCancel_date.Text = Convert.ToDateTime(dt.Rows[0]["cancel_date"]).ToString("yyyy/MM/dd");

                                    using (SqlCommand cmdcancel = new SqlCommand())
                                    {

                                        cmdcancel.CommandText = @"Select delete_user,record_memo, b.user_name from ttDeliveryRequestsRecord a With(Nolock) 
                                                                  left join tbAccounts b on a.delete_user  = b.account_code 
                                                                  where record_action = 'D' and request_id  = @request_id
                                                                  and create_date >= dateadd( MONTH , -6, @cdate)";
                                        cmdcancel.Parameters.Add("@request_id", System.Data.SqlDbType.Int).Value = request_id;
                                        cmdcancel.Parameters.Add("@cdate", System.Data.SqlDbType.DateTime).Value = dt.Rows[0]["cdate"];

                                        using (DataTable dtcancel = dbAdapter.getDataTable(cmdcancel))
                                        {
                                            if (dtcancel.Rows.Count > 0)
                                            {
                                                liCancel_psn.Text = dtcancel.Rows[0]["delete_user"].ToString() + dtcancel.Rows[0]["user_name"].ToString();
                                                liCancel_memo.Text = dtcancel.Rows[0]["record_memo"].ToString();
                                            }
                                        }
                                    }

                                }
                                #endregion

                                lbl_sub_check_number.Text = "000";

                                #region 2. 收件人
                                receive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();                       //電話1
                                receive_tel1_ext.Text = dt.Rows[0]["receive_tel1_ext"].ToString();               //電話分機
                                receive_tel2.Text = dt.Rows[0]["receive_tel2"].ToString();                       //電話2
                                receive_contact.Text = dt.Rows[0]["receive_contact"].ToString();                 //收件人
                                receive_city.SelectedValue = dt.Rows[0]["receive_city"].ToString();              //收件地址-縣市     
                                city_SelectedIndexChanged(receive_city, null);
                                receive_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();              //收件地址-鄉鎮市區  
                                                                                                                 //receive_area_SelectedIndexChanged(null, null);



                                //到站領貨 0:否、1:是
                                if (!int.TryParse(dt.Rows[0]["receive_by_arrive_site_flag"].ToString(), out i_tmp)) i_tmp = 0;
                                cbarrive.Checked = i_tmp == 1 ? true : false;

                                if (cbarrive.Checked)
                                {
                                    receive_address.Text = dt.Rows[0]["arrive_address"].ToString();                  //到著站址
                                }
                                else
                                {
                                    receive_address.Text = dt.Rows[0]["receive_address"].ToString();                 //收件地址-路街巷弄號
                                }

                                #endregion

                                #region 4.寄件人
                                send_contact.Text = dt.Rows[0]["send_contact"].ToString();                     //寄件人
                                send_tel.Text = dt.Rows[0]["send_tel"].ToString();                             //寄件人電話
                                send_city.SelectedValue = dt.Rows[0]["send_city"].ToString();                  //寄件人地址-縣市
                                city_SelectedIndexChanged(send_city, null);
                                send_area.SelectedValue = dt.Rows[0]["send_area"].ToString();                  //寄件人地址-鄉鎮市區
                                send_address.Text = dt.Rows[0]["send_address"].ToString();                     // 寄件人地址 - 號街巷弄號
                                supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();                    //配送商代碼
                                supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();                    //配送商名稱
                                #endregion

                                #region 到著站簡碼
                                lb_area_arrive_code.DataSource = Utility.getArea_Arrive_Code(true);
                                lb_area_arrive_code.DataValueField = "station_scode";
                                lb_area_arrive_code.DataTextField = "showsname";
                                lb_area_arrive_code.DataBind();
                                lb_area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
                                #endregion

                                #region 修改紀錄

                                original_collection_money.Text = dt.Rows[0]["collection_money"].ToString();
                                original_receive_address.Text = dt.Rows[0]["receive_city"].ToString() + dt.Rows[0]["receive_area"].ToString() + dt.Rows[0]["receive_address"].ToString();
                                original_pieces.Text = dt.Rows[0]["pieces"].ToString();
                                original_receive_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                                original_receive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();
                                original_order_number.Text = dt.Rows[0]["order_number"].ToString();
                                original_arrive_assign_date.Text = dt.Rows[0]["arrive_assign_date"].ToString();
                                original_send_contact.Text = dt.Rows[0]["send_contact"].ToString();
                                original_send_tel.Text = dt.Rows[0]["send_tel"].ToString();
                                original_send_address.Text = dt.Rows[0]["send_city"].ToString() + dt.Rows[0]["send_area"].ToString() + dt.Rows[0]["send_address"].ToString();

                                SqlCommand cmd6 = new SqlCommand();
                                DataTable dt6 = new DataTable();
                                cmd6.CommandText = string.Format("select * from tcDeliveryRequests where request_id = @request_id");
                                cmd6.Parameters.AddWithValue("@request_id", request_id);
                                dt6 = dbAdapter.getDataTable(cmd6);
                                original_area_arrive_code.Text = dt6.Rows[0]["area_arrive_code"].ToString();

                                #endregion

                                lb_area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();        //到著碼
                                if (lb_area_arrive_code.SelectedValue == "" && dt.Rows[0]["area_arrive_code"].ToString() != "")
                                {
                                    lb_area_arrive_code.Items.Add(new ListItem(dt.Rows[0]["area_arrive_code"].ToString(), dt.Rows[0]["area_arrive_code"].ToString()));
                                    lb_area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();      //到著碼
                                }

                                //SqlCommand cmd7 = new SqlCommand();
                                //DataTable dt7 = new DataTable();
                                //cmd7.CommandText = string.Format("select * from tbStation  where station_scode = @station_scode");
                                //cmd7.Parameters.AddWithValue("@station_scode", dt.Rows[0]["area_arrive_code"].ToString());
                                //dt7 = dbAdapter.getDataTable(cmd7);
                                //if (!string.IsNullOrEmpty(dt.Rows[0]["area_arrive_code"].ToString()))
                                //{
                                //    lb_area_arrive_code.Text = dt.Rows[0]["area_arrive_code"].ToString() + dt7.Rows[0]["station_name"];        //到著碼
                                //}
                                //else
                                //{
                                //    lb_area_arrive_code.Text = "";
                                //}

                                if (!String.IsNullOrEmpty(dt.Rows[0]["supplier_fee"].ToString()) || !IsNumeric(dt.Rows[0]["supplier_fee"].ToString())) //貨件費用
                                {
                                    i_supplier_fee.Text = dt.Rows[0]["supplier_fee"].ToString();
                                }

                                if (!String.IsNullOrEmpty(dt.Rows[0]["csection_fee"].ToString()) || !IsNumeric(dt.Rows[0]["csection_fee"].ToString())) //C配運費
                                {
                                    i_csection_fee.Text = dt.Rows[0]["csection_fee"].ToString();
                                }

                                if (!String.IsNullOrEmpty(dt.Rows[0]["remote_fee"].ToString()) || !IsNumeric(dt.Rows[0]["remote_fee"].ToString()))     //偏遠區加價
                                {
                                    i_remote_fee.Text = dt.Rows[0]["remote_fee"].ToString();
                                }
                                //dlDistributor.SelectedValue = dt.Rows[0]["Distributor"].ToString();  //配送廠商

                                ReadOnlySet(true);

                                #endregion
                                break;
                        }

                        #region mark session
                        //arrive_email.Text = dt.Rows[0]["arrive_email"].ToString();                    //收件人-電子郵件
                        //invoice_memo.SelectedValue = dt.Rows[0]["invoice_memo"].ToString();           //備註
                        //donate_invoice_flag.Checked = Convert.ToBoolean(dt.Rows[0]["donate_invoice_flag"]);              //是否發票捐贈 
                        //electronic_invoice_flag.Checked  = Convert.ToBoolean(dt.Rows[0]["electronic_invoice_flag"]);     //是否為電子發票
                        //uniform_numbers.Text = dt.Rows[0]["uniform_numbers"].ToString();              //統一編號
                        #endregion



                    }

                }
            }

        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "查詢及補單作業-" + Request.RawUrl + strErr + ": " + ex.ToString();


            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

            //導至錯誤頁面
            String _theUrl = string.Format("Oops.aspx?err_title={0}&err_msg={1}", "系統發生錯誤!", "系統資訊異常，請通知系統管理人員。");
            Server.Transfer(_theUrl, true);
        }
    }

    /// <summary>頁面項目開放編輯與否-針對託運單修改 vs 託運轉址</summary>
    /// <param name="status"></param>
    protected void ReadOnlySet(Boolean status)
    {
        //託運類別 
        //rbcheck_type.Enabled = status;

        //基本設定
        lbcheck_number.Enabled = status;
        order_number.Enabled = status;
        check_type.Enabled = status;
        receive_customer_code.Enabled = status;
        subpoena_category.Enabled = status;

        //才件代收
        pieces.Enabled = status;
        plates.Enabled = status;
        cbm.Enabled = status;
        collection_money.Enabled = status;
        //arrive_to_pay_freight.Enabled = status;
        //arrive_to_pay_append.Enabled = status;

        //特殊設定
        //product_category.Enabled = status;
        //arrive_mobile.Enabled = status;
        //special_send.Enabled = status;
        arrive_assign_date.Enabled = status;
        time_period.Enabled = status;
        invoice_desc.Disabled = !status;
        receipt_flag.Enabled = status;
        WareHouse.Enabled = status;
        //pallet_recycling_flag.Enabled = status;
        receipt_round_trip.Enabled = !status;

        //turn_board.Enabled = status;
        //upstairs.Enabled = status;
        //difficult_delivery.Enabled = status;
        bagno.Enabled = status;
        ArticleNumber.Enabled = status;
        SendPlatform.Enabled = status;
        ArticleName.Enabled = status;


    }

    /// <summary>初始資料設定</summary>
    protected void DefaultSet()
    {
        #region 權限
        manager_type = Session["manager_type"].ToString(); //管理單位類別   
        supplier_code = Session["master_code"].ToString();
        //if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
        //{
        //    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
        //    if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
        //    {
        //        switch (manager_type)
        //        {
        //            case "1":
        //            case "2":
        //                supplier_code = "";
        //                break;
        //            default:
        //                supplier_code = "000";
        //                break;
        //        }
        //    }
        //}

        if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
        {
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
        }

        // 只有管理者可以進行修改  //客戶僅會從edi訂單管理進入故不擋
        if (manager_type == "2" || manager_type == "3" || manager_type == "4" )
        {
            btnsave.Style.Add("display", "none");
        }

        #endregion

        #region 郵政縣市
        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        receive_city.DataSource = dbAdapter.getDataTable(cmd1);
        receive_city.DataValueField = "city";
        receive_city.DataTextField = "city";
        receive_city.DataBind();
        receive_city.Items.Insert(0, new ListItem("請選擇", ""));

        send_city.DataSource = dbAdapter.getDataTable(cmd1);
        send_city.DataValueField = "city";
        send_city.DataTextField = "city";
        send_city.DataBind();
        send_city.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

        #region 客代編號
        rbcheck_type_SelectedIndexChanged(null, null);
        #endregion

        #region 託運類別
        SqlCommand cmd2 = new SqlCommand();
        cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
        check_type.DataSource = dbAdapter.getDataTable(cmd2);
        check_type.DataValueField = "code_id";
        check_type.DataTextField = "code_name";
        check_type.DataBind();
        #endregion

        #region 傳票類別
        SqlCommand cmd3 = new SqlCommand();
        cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 and code_id <>'31' and code_id <> '51' order by code_id ";
        subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
        subpoena_category.DataValueField = "code_id";
        subpoena_category.DataTextField = "code_name";
        subpoena_category.DataBind();
        #endregion

        #region 商品類別
        //SqlCommand cmd4 = new SqlCommand();
        //cmd4.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S3' and active_flag = 1 ";
        //product_category.DataSource = dbAdapter.getDataTable(cmd4);
        //product_category.DataValueField = "code_id";
        //product_category.DataTextField = "code_name";
        //product_category.DataBind();
        #endregion

        //#region 特殊配送
        //SqlCommand cmd5 = new SqlCommand();
        //cmd5.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S4' and active_flag = 1 ";
        //special_send.DataSource = dbAdapter.getDataTable(cmd5);
        //special_send.DataValueField = "code_id";
        //special_send.DataTextField = "code_name";
        //special_send.DataBind();
        //#endregion

        #region 配送時段
        SqlCommand cmd6 = new SqlCommand();
        cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
        time_period.DataSource = dbAdapter.getDataTable(cmd6);
        time_period.DataValueField = "code_id";
        time_period.DataTextField = "code_name";
        time_period.DataBind();
        time_period.Items.Insert(2, new ListItem("晚", "晚"));
        time_period.SelectedValue = "不指定";
        #endregion

        #region 配送溫層
        using (SqlCommand cmd10 = new SqlCommand())
        {
            cmd10.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S9' and active_flag = 1 ";
            dltemp.DataSource = dbAdapter.getDataTable(cmd10);
            dltemp.DataValueField = "code_id";
            dltemp.DataTextField = "code_name";
            dltemp.DataBind();
        }
        #endregion


        #region 零擔運輸商
        //using (SqlCommand cmd9 = new SqlCommand())
        //{
        //    cmd9.CommandText = "select * from tbItemCodes  where code_bclass = '7' and code_sclass = 'distributor' and active_flag=1 ";
        //    dlDistributor.DataSource = dbAdapter.getDataTable(cmd9);
        //    dlDistributor.DataValueField = "code_id";
        //    dlDistributor.DataTextField = "code_name";
        //    dlDistributor.DataBind();
        //    dlDistributor.Items.Insert(0, new ListItem("配送廠商", ""));
        //}
        #endregion

        //#region 到著站簡碼
        //SqlCommand cmd8 = new SqlCommand();
        ////// cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 ");
        ////cmd8.CommandText = string.Format(@"select distinct arr.supplier_code,  sup.supplier_code + ' '+  sup.supplier_name as showname 
        ////                                        from ttArriveSites arr with(nolock)
        ////                                        left join   tbSuppliers sup with(nolock) on arr.supplier_code =  sup.supplier_code and sup.active_flag = 1 
        ////                                        where arr.supplier_code <> '' and arr.supplier_code is not null
        ////                                        ");
        //cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 and show_trans = 1 order by supplier_code");
        //area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
        //area_arrive_code.DataValueField = "supplier_code";
        //area_arrive_code.DataTextField = "showname";
        //area_arrive_code.DataBind();
        //area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
        //#endregion

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@request_id", request_id);
            cmd.CommandText = " SELECT dr.pieces,dr.customer_code, c.product_type, c.is_new_customer FROM tcDeliveryRequests dr left join tbCustomers c on c.customer_code = dr.customer_code WHERE request_id = @request_id ";
            var table = dbAdapter.getDataTable(cmd);
            if (table.Rows.Count > 0)
            {
                default_pieces = table.Rows[0]["pieces"].ToString();
                strcustomer_code = table.Rows[0]["customer_code"].ToString();
                product_type = table.Rows[0]["product_type"].ToString().ToLower();
                bool_is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
            }
        }

        #region 產品
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", strcustomer_code);
            var table = dbAdapter.getDataTable(cmd);
        }
        string sqlstr = "";
        if (default_pieces != "")
        {
            if (Convert.ToInt32(default_pieces) > 1)
            {
                if (product_type == "1" && bool_is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶多筆
                else if (product_type == "1") { sqlstr = "'CM000036'"; } //月結舊客戶多筆
                else if (product_type == "2" && bool_is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論才多筆
                else { sqlstr = "' '"; }
            }
            else
            {
                if (product_type == "1" && bool_is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶單筆
                else if (product_type == "1") { sqlstr = "'CS000035'"; } //月結舊客戶單筆
                else if (product_type == "2" && bool_is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論才單筆
                else { sqlstr = "' '"; }
            }
        }


        using (SqlCommand cmd8 = new SqlCommand())
        {

               cmd8.CommandText = @"select ProductId,
               case ProductId when 'CS000029' then '論S'
               when 'CM000030' then '論S'
               when 'CS000035' then '月結'
               when 'CM000036' then '月結'

               when 'CM000043' then '論才'
               else Name
               end as Name from productmanage 
               where ProductId in ( " + sqlstr + " ) and IsActive='1' order by id"; ;

            dlProductName.DataSource = dbAdapter.getDataTableForFSE01(cmd8);
            dlProductName.DataValueField = "ProductId";
            dlProductName.DataTextField = "Name";
            dlProductName.DataBind();
            dlProductName.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion

        #region 產品規格
        //顯示寄件人資料
        using (SqlCommand cmda = new SqlCommand())
        {
            string ProductId = "";
            cmda.Parameters.AddWithValue("@request_id", request_id);
            cmda.CommandText = " SELECT ProductId FROM tcDeliveryRequests With(Nolock) WHERE request_id = @request_id ";
            DataTable dta;
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                ProductId = dta.Rows[0]["ProductId"].ToString();
            }
            using (SqlCommand cmd9 = new SqlCommand())
            {
                var strsql = "";
                //一般月結客代 - 多筆、單筆、月結多筆、月結單筆
                if (ProductId == "CM000030" || ProductId == "CS000029" || ProductId == "CM000036" || ProductId == "CS000035")
                {
                    strsql = "'S060','S090','S110','S120','S150','B001','B002','B003'";
                }

                //預購袋客代 - 預購袋 且為新客袋
                else if (ProductId == "PS000031" && bool_is_new_customer == true)
                {
                    strsql = "'B003'";
                }
                //預購袋客代 - 預購袋
                else if (ProductId == "PS000031")
                {
                    strsql = "'B001','B002','B003','B004'";
                }
                //預購袋客代 - 速配箱
                else if (ProductId == "PS000032")
                {
                    strsql = "'X001','X002','X005'";
                }
                //預購袋客代 - 超值袋
                else if (ProductId == "PS000033")
                {
                    strsql = "'B001','B002','B003','B004'";
                }
                //預購袋客代 - 超值箱
                else if (ProductId == "PS000034")
                {
                    strsql = "'X001','X002','X005'";
                }
                //內部文件
                else if (ProductId == "CS000037")
                {
                    strsql = "'N001'";
                }
                //商城出貨 - 包材
                else if (ProductId == "CS000038")
                {
                    strsql = "'N001'";
                }
                else if (ProductId == "CM000043" )
                {
                    strsql = "'C001','C002','C003','C004','C005'";
                }
                else { }
                if (ProductId == "CM000043" )
                {
                    cmd9.CommandText = "SELECT CodeId,CodeName FROM (select ROW_NUMBER()OVER (PARTITION BY SPEC ORDER BY P.ID DESC)SN,CodeId,CodeName,OrderBy  from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec' where ProductId = @ProductId and  CustomerCode = @customerCode )T WHERE SN = '1' order by OrderBy asc";
                    cmd9.Parameters.AddWithValue("@customerCode", strcustomer_code);
                    cmd9.Parameters.AddWithValue("@ProductId", ProductId);
                    dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                    dlProductSpec.DataValueField = "CodeId";
                    dlProductSpec.DataTextField = "CodeName";
                    dlProductSpec.DataBind();
                    dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
                }
                else if (strsql!="")
                {
                    cmd9.CommandText = "select CodeId,CodeName from ItemCodes with(nolock) where CodeType = 'ProductSpec' and CodeId in (" + strsql + ") order by id";
                    dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                    dlProductSpec.DataValueField = "CodeId";
                    dlProductSpec.DataTextField = "CodeName";
                    dlProductSpec.DataBind();
                    dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
                }
                else { }
            }
        }
        #endregion

        #region 到著站簡碼
        lb_area_arrive_code.DataSource = Utility.getArea_Arrive_Code(true);
        lb_area_arrive_code.DataValueField = "station_scode";
        lb_area_arrive_code.DataTextField = "showsname";
        lb_area_arrive_code.DataBind();
        lb_area_arrive_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

    }

    

    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (supplier_code != "")
        {
            switch (manager_type)
            {
                case "0":
                case "1":
                case "4":
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                    break;
                default:
                    cmd2.Parameters.AddWithValue("@master_code", supplier_code);
                    wherestr = " and master_code like ''+@master_code+'%' ";
                    break;
            }
        }
        cmd2.Parameters.AddWithValue("@pricing_code", "02");
        cmd2.Parameters.AddWithValue("@type", "1");
        cmd2.CommandText = string.Format(@"select customer_code , customer_code+ '-' + customer_shortname as name  from tbCustomers With(Nolock)
                                           where stop_shipping_code = '0' and pricing_code = @pricing_code and type =@type {0}  order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
        #endregion
        Clear();

    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkshowsend.Checked)
        {
            //顯示寄件人資料
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_shortname , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_name, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + strcustomer_code + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_shortname"].ToString();   //名稱
                send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();      //區配code
                supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();      //區配name

                bool individualfee = Convert.ToBoolean(dt.Rows[0]["individual_fee"]);   //是否個別計價
                                                                                        //bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
                                                                                        //if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                if (individualfee)
                {
                    individual_fee.Style.Add("display", "block");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
                else
                {
                    individual_fee.Style.Add("display", "none");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
            }
        }
    }

    protected void cbarrive_CheckedChanged(object sender, EventArgs e)
    {
        if (cbarrive.Checked && area_arrive_code != "")
        {
            receive_address.Text = area_arrive_code + "站址";
        }

        //area_arrive_code.Enabled = cbarrive.Checked;

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 到著站簡碼
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.station_code  + ' '+  B.station_name as showname    
                                                   from ttArriveSitesScattered A
                                                   left join tbStation B on A.station_code  = B.station_scode 
                                                   where A.station_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        area_arrive_code = dt.Rows[0]["station_code"].ToString();
                    }
                    catch { }
                }
            }
        }
        #endregion

        #region 取得零擔運輸配送商
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue);
            cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue);
            cmd.CommandText = "usp_GetDistributor";
            cmd.CommandType = CommandType.StoredProcedure;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        if (Convert.ToInt32(dt.Rows[0]["status_code"]) == 1)
                        {
                            //dlDistributor.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
                        }
                    }
                    catch { }
                }
            }

        }

        #endregion
        cbarrive_CheckedChanged(null, null);
    }

    protected void area_arrive_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbarrive_CheckedChanged(null, null);
    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    //dlDistributor.SelectedValue = "";
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        dlarea.Items.Clear();
        dlarea.ClearSelection();
        dlarea.Items.Add(new ListItem("請選擇", ""));

        if (dlcity.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                //dlarea.DataSource = dta;
                //dlarea.DataValueField = "area";
                //dlarea.DataTextField = "area";
                //dlarea.DataBind();
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
    }

    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        customer_code.Text = dlcustomer_code.SelectedValue;
        CheckBox1_CheckedChanged(null, null);
        divReceiver.Visible = false;
        New_List.DataSource = null;
        New_List.DataBind();

    }
    protected void dl_Pieces_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlcustomer_code.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請先選擇客戶代碼');</script>", false);
            return;
        }

        var product_type = "";
        bool is_new_customer = false;

        #region 產品
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type = table.Rows[0]["product_type"].ToString().ToLower();
            is_new_customer = (bool)table.Rows[0]["is_new_customer"];
        }

        string sqlstr = "";
        if (pieces.Text != "")
        {
            if (Convert.ToInt32(pieces.Text) > 1)
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶多筆
                else if (product_type == "1") { sqlstr = "'CM000036'"; } //月結舊客戶多筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論才多筆
                else { sqlstr = "' '"; }
            }
            else
            {
                if (product_type == "1" && is_new_customer == true) { sqlstr = "'CM000030'"; } //月結新客戶單筆
                else if (product_type == "1") { sqlstr = "'CS000035'"; } //月結舊客戶單筆
                else if (product_type == "2" && is_new_customer == true) { sqlstr = "'PS000031'"; } //預購袋新客袋只能選預購袋
                else if (product_type == "2") { sqlstr = "'PS000031','PS000032'"; } //預購袋
                else if (product_type == "3") { sqlstr = "'PS000033','PS000034'"; } //超值箱
                else if (product_type == "4") { sqlstr = "'CS000037'"; } //內部文件
                else if (product_type == "5") { sqlstr = "'CS000038'"; } //商城出貨
                else if (product_type == "6") { sqlstr = "'CM000043'"; } //論才單筆
                else { sqlstr = "' '"; }
            }
        }

        using (SqlCommand cmd8 = new SqlCommand())
        {
            //目前先寫死 (馬丁)
            cmd8.CommandText = cmd8.CommandText = @"select ProductId,
               case ProductId when 'CS000029' then '論S'
               when 'CM000030' then '論S'
               when 'CS000035' then '月結'
               when 'CM000036' then '月結'

               when 'CM000043' then '論才'
               else Name
               end as Name from productmanage 
               where ProductId in ( " + sqlstr + " ) and IsActive='1' order by id";

            dlProductName.DataSource = dbAdapter.getDataTableForFSE01(cmd8);
            dlProductName.DataValueField = "ProductId";
            dlProductName.DataTextField = "Name";
            dlProductName.DataBind();
            dlProductName.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion
    }

    protected void dl_ProductName_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 產品規格
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select  is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", customer_code.Text);
            var table = dbAdapter.getDataTable(cmd);
            bool_is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
        }

        using (SqlCommand cmd9 = new SqlCommand())
        {
            var strsql = "";
            //一般月結客代 - 多筆、單筆、月結多筆、月結單筆
            if (dlProductName.SelectedValue == "CM000030" || dlProductName.SelectedValue == "CS000029" || dlProductName.SelectedValue == "CM000036" || dlProductName.SelectedValue == "CS000035")
            {
                strsql = "'S060','S090','S110','S120','S150','B001','B002','B003'";
            }
            ////一般月結客代 - 月結袋
            //if (dlProductName.SelectedValue == "CS000035")
            //{
            //    strsql = "'B001','B002','B003'";
            //}

            //預購袋客代 - 預購袋 且為新客袋
            else if (dlProductName.SelectedValue == "PS000031" && bool_is_new_customer == true)
            {
                strsql = "'B003'";
            }
            //預購袋客代 - 預購袋
            else if (dlProductName.SelectedValue == "PS000031")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 速配箱
            else if (dlProductName.SelectedValue == "PS000032")
            {
                strsql = "'X001','X002','X005'";
            }
            //預購袋客代 - 超值袋
            else if (dlProductName.SelectedValue == "PS000033")
            {
                strsql = "'B001','B002','B003','B004'";
            }
            //預購袋客代 - 超值箱
            else if (dlProductName.SelectedValue == "PS000034")
            {
                strsql = "'X001','X002','X005'";
            }
            //內部文件
            else if (dlProductName.SelectedValue == "CS000037")
            {
                strsql = "'N001'";
            }
            //商城出貨 - 包材
            else if (dlProductName.SelectedValue == "CS000038")
            {
                strsql = "'N001'";
            }
            else if (dlProductName.SelectedValue == "CM000043" )
            {
                strsql = "'C001','C002','C003','C004','C005'";
            }
            else { }
            if (dlProductName.SelectedValue == "CM000043" )
            {
                cmd9.CommandText = "SELECT CodeId,CodeName FROM (select ROW_NUMBER()OVER (PARTITION BY SPEC ORDER BY P.ID DESC)SN,CodeId,CodeName,OrderBy  from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec' where ProductId = @ProductId and  CustomerCode = @customerCode )T WHERE SN = '1' order by OrderBy asc";
                cmd9.Parameters.AddWithValue("@customerCode", dlcustomer_code.SelectedValue);
                cmd9.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
            else
            {
                cmd9.CommandText = "select CodeId,CodeName from ItemCodes with(nolock) where CodeType = 'ProductSpec' and CodeId in (" + strsql + ") order by id";
                dlProductSpec.DataSource = dbAdapter.getDataTableForFSE01(cmd9);
                dlProductSpec.DataValueField = "CodeId";
                dlProductSpec.DataTextField = "CodeName";
                dlProductSpec.DataBind();
                dlProductSpec.Items.Insert(0, new ListItem("請選擇", ""));
            }
            
        }
        #endregion

    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void Clear()
    {
        customer_code.Text = "";
        send_contact.Text = "";   //名稱
        send_tel.Text = "";           // 電話
        send_city.SelectedValue = "";   //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";   //出貨地址-鄉鎮市區
        send_address.Text = "";        //出貨地址-路街巷弄號
        lbcustomer_name.Text = "";
       
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        lbErrQuant.Text = "";
        int ttpieces = 0;
        int ttplates = 0;
        int ttcbm = 0;
        int ttcollection_money = 0;
        int ttarrive_to_pay = 0;
        int ttarrive_to_append = 0;
        int remote_fee = 0;
        int supplier_fee = 0;  //配送費用
        int cscetion_fee = 0;  //C配運價
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)
        bool is_new_customer = false;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type = table.Rows[0]["product_type"].ToString().ToLower();
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
        }


        /* 20170727 託運類別 ... modify by lisa
        * 論板時、論小板時：【板數】與【件數】一定要提示填入數字
        * 論才時：【板數】與【件數】及【才數】皆要填入
        */

        if (!int.TryParse(pieces.Text, out ttpieces)) ttpieces = 0;
        if (!int.TryParse(plates.Text, out ttplates)) ttplates = 0;
        if (!int.TryParse(cbm.Text, out ttcbm)) ttcbm = 0;

        bool individual = individual_fee.Style.Value.IndexOf("display:block") > -1;
        int i_tmp;
        //switch (rbcheck_type.SelectedValue)
        //{
        //    case "01":
        //    case "04":
        //        if (string.IsNullOrWhiteSpace(plates.Text) || string.IsNullOrWhiteSpace(pieces.Text))
        //        {
        //            lbErrQuant.Text = "請輸入板數及件數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入板數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        break;
        //    case "02":
        //        if (string.IsNullOrWhiteSpace(pieces.Text))
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        break;
        //    case "03":
        //        if (string.IsNullOrWhiteSpace(cbm.Text)
        //            || string.IsNullOrWhiteSpace(plates.Text)
        //            || string.IsNullOrWhiteSpace(pieces.Text)
        //            )
        //        {
        //            lbErrQuant.Text = "請輸入板數、件數及才數";
        //            cbm.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入板數";
        //            plates.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入件數";
        //            pieces.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }

        //        if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        //        if (i_tmp <= 0)
        //        {
        //            lbErrQuant.Text = "請輸入才數";
        //            cbm.Focus();
        //            lbErrQuant.Visible = true;
        //            return;
        //        }
        //        break;
        //}

        if (Func.IsNormalDelivery(lbcheck_number.Text))
        {
            lbErrQuant.Text = "此筆已正常配達，無法再進行調整";
            lbErrQuant.Visible = true;
            return;
        }

        if (Func.IsCancelled(lbcheck_number.Text))
        {
            lbErrQuant.Text = "此筆已經銷單，無法再進行調整";
            lbErrQuant.Visible = true;
            return;
        }

        if (string.IsNullOrWhiteSpace(pieces.Text))
        {
            lbErrQuant.Text = "請輸入件數";
            pieces.Focus();
            lbErrQuant.Visible = true;
            return;
        }

        if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
        if (i_tmp <= 0)
        {
            lbErrQuant.Text = "請輸入件數";
            pieces.Focus();
            lbErrQuant.Visible = true;
            return;
        }


        if (individual && i_supplier_fee.Text == "")
        {
            lbErrQuant.Text = "請輸入貨件運費";
            i_supplier_fee.Focus();
            return;
        }

        //花蓮、台東地區 強制輸入才數
        if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        if (((receive_city.SelectedValue == "花蓮縣") || (receive_city.SelectedValue == "臺東縣")) && (i_tmp <= 0))
        {
            lbErrQuant.Text = "請輸入才數";
            cbm.Focus();
            lbErrQuant.Visible = true;
            return;
        }
        if (collection_money.Text != "" && (dlcustomer_code.ToString() != "F2900000402"))
        {         
          
            int NumA = 0;

            NumA = Convert.ToInt32(collection_money.Text);

            if (is_new_customer == true && NumA > 50000)  //新客代,上限5萬
            {
                Response.Write("<script language=JavaScript>alert('代收金額超過50,000上限，請重新輸入');</script>");
                //MessageBox.Show("代收金額超過20,000上限，請重新輸入");
                collection_money.Text = string.Empty;
                return;
            }
            else if (is_new_customer == false && NumA > 20000)
            {

                Response.Write("<script language=JavaScript>alert('代收金額超過20,000上限，請重新輸入');</script>");
                //MessageBox.Show("代收金額超過20,000上限，請重新輸入");
                collection_money.Text = string.Empty;
                return;
            }
        }
        //舊客戶不填寫 報值金額及保值費
        if (is_new_customer)
        {
        }
        else
        {
            if ((Report_fee.Text != "0" && Report_fee.Text != "") || (tbProductValue.Text != "" && tbProductValue.Text != "0"))
            {
                Label12.Visible = true;
                Label12.Text = "此客代無須填寫報值金額與保值費";
                return;
            }
            else
            {
            }
        }
        //一般件產品名稱需與件數符合
        var product_type_p = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            product_type_p = table.Rows[0]["product_type"].ToString().ToLower();            
        }
        if (product_type_p == "1")
        {
            if ((pieces.Text.Trim() == "1" && dlProductName.SelectedValue != "CS000035" && dlProductName.SelectedValue != "CM000030") || (pieces.Text.Trim() != "1" && dlProductName.SelectedValue != "CM000036" && dlProductName.SelectedValue != "CM000030"))
            {
                Label13.Visible = true;
                return;
            }
        }
        //前後收件地址是否相同,有差異進入地址檢查
        using (SqlCommand cmdra = new SqlCommand())
        {
            cmdra.Parameters.AddWithValue("@check_number", lbcheck_number.Text.Trim());
            cmdra.Parameters.AddWithValue("@receive_address", receive_city.SelectedValue + receive_area.SelectedValue + receive_address.Text.Trim());
            cmdra.CommandText = string.Format(@"select check_number  from tcDeliveryRequests 
where check_Number = @check_number and receive_city+receive_area+receive_address != @receive_address ");
            using (DataTable dt = dbAdapter.getDataTable(cmdra))
            {
                //檢查地址
                if (dt != null && dt.Rows.Count > 0)
                {
                    GetAddressParsing(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");
                    if (check_address == false)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查收件地址是否正確');</script>", false);
                        return;
                    }
                    else { area_arrive_code = station_code; }
                }

            }
        }

        //寄件地址是否有修改
        using (SqlCommand cmdsa = new SqlCommand())
        {
            cmdsa.Parameters.AddWithValue("@check_number", lbcheck_number.Text.Trim());
            cmdsa.Parameters.AddWithValue("@send_address", send_city.SelectedValue + send_area.SelectedValue + send_address.Text.Trim());
            cmdsa.CommandText = string.Format(@"select check_number  from tcDeliveryRequests 
where check_Number = @check_number and send_city+send_area+send_address != @send_address ");
            using (DataTable dt = dbAdapter.getDataTable(cmdsa))
            {
                //檢查地址
                if (dt != null && dt.Rows.Count > 0)
                {
                    GetAddressParsing(send_city.Text.Trim() + send_area.Text.Trim() + send_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");
                    if (check_address == false)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查寄件地址是否正確');</script>", false);
                        return;
                    }
                    else { send_station_scode = station_code; }
                }

            }
        }

        if (lbErrQuant.Text != "")
        {
            lbErrQuant.Visible = true;
        }
        else
        {
            int i_type = 0;
            if (Request.QueryString["r_type"] == null || !int.TryParse(Request.QueryString["r_type"].ToString(), out i_type)) i_type = 0;

            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@pricing_type", "02");                     //計價模式 (01:論板、02:論件、03論才、04論小板)
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
            cmd.Parameters.AddWithValue("@check_number", lbcheck_number.Text);                            //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
            cmd.Parameters.AddWithValue("@order_number", order_number.Text);                              //訂單號碼
            cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
            cmd.Parameters.AddWithValue("@receive_customer_code", receive_customer_code.Text);            //收貨人編號
            cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
            cmd.Parameters.AddWithValue("@receive_tel1", receive_tel1.Text);                              //電話1
            cmd.Parameters.AddWithValue("@receive_tel1_ext", receive_tel1_ext.Text.ToString());           //電話分機
            cmd.Parameters.AddWithValue("@receive_tel2", receive_tel2.Text);                              //電話2
            cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
            cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
            cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
            if (!individual)
            {
                remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
            }
            else
            {
                if (int.TryParse(i_remote_fee.Text, out i_tmp)) remote_fee = i_tmp;
            }
            cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價

            if (area_arrive_code != "")
            {
                cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code);             //到著碼
            }
            cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", Convert.ToInt16(cbarrive.Checked));                //到站領貨 0:否、1:是
            if (cbarrive.Checked)
            {
                cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
            }


            try
            {
                ttcollection_money = Convert.ToInt32(collection_money.Text);
            }
            catch { }

            //try
            //{
            //    ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
            //}
            //catch { }

            //try
            //{
            //    ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
            //}
            //catch { }

            cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
            cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
            cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數
            cmd.Parameters.AddWithValue("@ProductId", dlProductName.SelectedValue);                           //產品名稱
            cmd.Parameters.AddWithValue("@SpecCodeId", dlProductSpec.SelectedValue);                        //規格

            cmd.Parameters.AddWithValue("@SpecialAreaId", lbSpecialAreaId.Text);                           //特服id
            cmd.Parameters.AddWithValue("@SpecialAreaFee", lbSpecialAreaFee.Text);                        //特服費用        


            switch (subpoena_category.SelectedValue)
            {
                case "11"://元付
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);              //到付追加
                    break;
                case "21"://到付
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                 //到付運費
                    break;
                case "25"://元付-到付追加
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                //到付追加
                    break;
                case "41"://代收貨款
                    cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                     //代收金
                    break;
            }

            //cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
            //cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
            //cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加

            cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
            cmd.Parameters.AddWithValue("@send_tel", send_tel.Text.ToString());                           //寄件人電話
            cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
            cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
            if (send_station_scode != "")
            {
                cmd.Parameters.AddWithValue("@send_station_scode", send_station_scode);                   //發送站        
            }
            cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
            cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
            cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
                                                                                                          //cmd.Parameters.AddWithValue("@arrive_mobile", arrive_mobile.Text.ToString());               //收件人-手機1  
            cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
            cmd.Parameters.AddWithValue("@invoice_memo", "");                                             //備註
            cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value.Trim());                      //說明
                                                                                                          //cmd.Parameters.AddWithValue("@product_category", product_category.SelectedValue.ToString());//商品種類
                                                                                                          //cmd.Parameters.AddWithValue("@special_send", special_send.SelectedValue.ToString());          //特殊配送
            cmd.Parameters.AddWithValue("@ArticleNumber", ArticleNumber.Text);  //產品編號
            cmd.Parameters.AddWithValue("@SendPlatform", SendPlatform.Text);    //出貨平台
            cmd.Parameters.AddWithValue("@ArticleName", ArticleName.Text);      //品名
            cmd.Parameters.AddWithValue("@bagno", bagno.Text);  //袋號


            DateTime date;
            cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(arrive_assign_date.Text, out date) ? (object)date : DBNull.Value);                  //指定日
            cmd.Parameters.AddWithValue("@time_period", time_period.SelectedValue.ToString());            //時段
            cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
            //cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
            cmd.Parameters.AddWithValue("@round_trip", Convert.ToInt16(receipt_round_trip.Checked));         //是否來回件
            cmd.Parameters.AddWithValue("@WareHouse", Convert.ToInt16(WareHouse.Checked));         //是否來回件
            //if (pallet_recycling_flag.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@Pallet_type", Pallet_type.Value);
            //}
            //else
            //{
            //    cmd.Parameters.AddWithValue("@Pallet_type", "");
            //}
            //cmd.Parameters.AddWithValue("@turn_board", Convert.ToInt16(turn_board.Checked));                               //翻版
            //cmd.Parameters.AddWithValue("@upstairs", Convert.ToInt16(upstairs.Checked));                                   //上樓
            cmd.Parameters.AddWithValue("@holiday_delivery", Convert.ToInt16(holidaydelivery.Checked));                    //假日配
            //cmd.Parameters.AddWithValue("@difficult_delivery", Convert.ToInt16(difficult_delivery.Checked));               //困配
            cmd.Parameters.AddWithValue("@print_date", DateTime.TryParse(Shipments_date.Text, out date) ? (object)date : DBNull.Value);   //出貨日期
            cmd.Parameters.AddWithValue("@ProductValue", tbProductValue.Text);
            cmd.Parameters.AddWithValue("@ReportFee", Report_fee.Text);                     //報值金額
            //if (int.TryParse(turn_board_fee.Text, out i_tmp) && turn_board.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@turn_board_fee", i_tmp.ToString());
            //}
            //else
            //{
            //    cmd.Parameters.AddWithValue("@turn_board_fee", 0);
            //}

            //if (int.TryParse(upstairs_fee.Text, out i_tmp) && upstairs.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@upstairs_fee", i_tmp.ToString());
            //}
            //else
            //{
            //    cmd.Parameters.AddWithValue("@upstairs_fee", 0);
            //}

            //if (int.TryParse(difficult_fee.Text, out i_tmp) && difficult_delivery.Checked)
            //{
            //    cmd.Parameters.AddWithValue("@difficult_fee", i_tmp.ToString());
            //}
            //else
            //{
            //    cmd.Parameters.AddWithValue("@difficult_fee", 0);
            //}

            cmd.Parameters.AddWithValue("@sub_check_number", lbl_sub_check_number.Text);            //次貨號
            if (individual)
            {
                if (int.TryParse(i_supplier_fee.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@supplier_fee", i_tmp.ToString());
                    cmd.Parameters.AddWithValue("@total_fee", i_tmp.ToString());
                }
                if (int.TryParse(i_csection_fee.Text, out i_tmp))
                    cmd.Parameters.AddWithValue("@csection_fee", i_tmp.ToString());
            }

            if (i_type > 0)
            {
                cmd.Parameters.AddWithValue("@add_transfer", 1);                                              //是否轉址
                cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);

                if (!individual)
                {
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                    if (result > 0)
                    {
                        #region 回寫到託運單的費用欄位
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.CommandText = "usp_GetShipFeeByRequestId";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@request_id", result.ToString());
                        using (DataTable dt = dbAdapter.getDataTable(cmd2))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                if (status_code == 1)
                                {
                                    supplier_fee = Convert.ToInt32(dt.Rows[0]["supplier_fee"]);       //配送費用
                                    cscetion_fee = Convert.ToInt32(dt.Rows[0]["cscetion_fee"]);       //C配運價                                

                                    //回寫到託運單的費用欄位
                                    using (SqlCommand cmd3 = new SqlCommand())
                                    {
                                        cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                        cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                        cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                        cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", result.ToString());
                                        cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd3);
                                        }
                                        catch (Exception ex)
                                        {
                                            string strErr = string.Empty;
                                            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                            strErr = "一筆式託運單-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                            //錯誤寫至Log
                                            PublicFunction _fun = new PublicFunction();
                                            _fun.Log(strErr, "S");
                                        }
                                    }
                                }
                            }
                        }

                        #endregion
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增成功');parent.$.fancybox.close();parent.location.reload(true);</script>", false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資訊異常，請重新確認');parent.$.fancybox.close();</script>", false);
                    }
                }

            }
            else
            {
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code_sel);                             //配送商代碼
                cmd.Parameters.AddWithValue("@supplier_name", supplier_name_sel);                             //配送商名稱
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 


                #region 回寫到託運單的費用欄位
                if (!individual)
                {
                    if (request_id != "")
                    {
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.CommandText = "usp_GetShipFeeByRequestId";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                        using (DataTable dt = dbAdapter.getDataTable(cmd2))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                if (status_code == 1)
                                {
                                    supplier_fee = dt.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["supplier_fee"]) : 0;         //配送費用
                                    cscetion_fee = dt.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cscetion_fee"]) : 0;         //C配運價
                                                                                                                                                         //回寫到託運單的費用欄位
                                    using (SqlCommand cmd3 = new SqlCommand())
                                    {
                                        cmd.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                        cmd.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                        cmd.Parameters.AddWithValue("@total_fee", supplier_fee);
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion
                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);   //修改
                dbAdapter.execNonQuery(cmd);

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');parent.$.fancybox.close();parent.location.reload(true);</script>", false);
            }

            #region 常用收件人通訊錄
            if (cbSaveReceive.Checked && receiver_id.Text == "")
            {
                if (!string.IsNullOrEmpty(customer_code.Text.Trim()) && !string.IsNullOrEmpty(receive_contact.Text.Trim()))
                {
                    //先判斷，如果有姓名、編號、地址、已存在就不再新增
                    using (SqlCommand cmd5 = new SqlCommand())
                    {

                        cmd5.Parameters.AddWithValue("@customer_code", customer_code.Text.Trim());
                        cmd5.Parameters.AddWithValue("@receiver_code", receiver_code.Text.Trim());
                        cmd5.Parameters.AddWithValue("@receiver_name", receive_contact.Text.Trim());
                        cmd5.Parameters.AddWithValue("@address_city", receive_city.SelectedValue);
                        cmd5.Parameters.AddWithValue("@address_area", receive_area.SelectedValue);
                        cmd5.Parameters.AddWithValue("@address_road", receive_address.Text.Trim());


                        cmd5.CommandText = string.Format(@"Select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  and A.receiver_code =@receiver_code and  A.receiver_name=@receiver_name 
                                          and A.customer_code =@customer_code and  A.address_city =@address_city and  address_area = @address_area 
                                          and address_road = @address_road ");
                        using (DataTable dt = dbAdapter.getDataTable(cmd5))
                        {
                            if (dt == null || dt.Rows.Count == 0)
                            {
                                SqlCommand cmd4 = new SqlCommand();
                                cmd4.Parameters.AddWithValue("@customer_code", customer_code.Text);                     //客戶代碼
                                cmd4.Parameters.AddWithValue("@receiver_code", receiver_code.Text);                     //收貨人代碼
                                cmd4.Parameters.AddWithValue("@receiver_name", receive_contact.Text);                   //收貨人名稱
                                cmd4.Parameters.AddWithValue("@tel", receive_tel1.Text);                                //電話
                                cmd4.Parameters.AddWithValue("@tel_ext", receive_tel1_ext.Text);                        //分機
                                cmd4.Parameters.AddWithValue("@tel2", receive_tel2.Text);                               //電話2
                                cmd4.Parameters.AddWithValue("@address_city", receive_city.SelectedValue.ToString());   //地址-縣市
                                cmd4.Parameters.AddWithValue("@address_area", receive_area.SelectedValue.ToString());   //地址 - 區域
                                cmd4.Parameters.AddWithValue("@address_road", receive_address.Text);                    //地址-路段
                                cmd4.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                                cmd4.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                                cmd4.CommandText = dbAdapter.SQLdosomething("tbReceiver", cmd4, "insert");
                                try
                                {
                                    dbAdapter.execNonQuery(cmd4);
                                }
                                catch (Exception ex)
                                {
                                    string strErr = string.Empty;
                                    if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                    strErr = "一筆式託運單-儲存常用通訊人" + Request.RawUrl + strErr + ": " + ex.ToString();


                                    //錯誤寫至Log
                                    PublicFunction _fun = new PublicFunction();
                                    _fun.Log(strErr, "S");
                                }
                            }
                        }

                    }



                }
            }
            #endregion

            //寫入修改紀錄
            if (original_collection_money.Text.ToString() != collection_money.Text)
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_collection_money.Text);
                cmd4.Parameters.AddWithValue("@new_data", collection_money.Text);
                cmd4.Parameters.AddWithValue("@revise_item", "代收貨款");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            string new_receive_address = receive_city.SelectedValue.ToString() + receive_area.SelectedValue.ToString() + receive_address.Text.ToString();

            if (original_receive_address.Text.ToString() != new_receive_address)
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_receive_address.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", new_receive_address);
                cmd4.Parameters.AddWithValue("@revise_item", "收件人地址");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_area_arrive_code.Text.ToString() != area_arrive_code.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_area_arrive_code.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", area_arrive_code.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "到著站");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_pieces.Text.ToString() != ttpieces.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_pieces.Text);
                cmd4.Parameters.AddWithValue("@new_data", ttpieces);
                cmd4.Parameters.AddWithValue("@revise_item", "件數");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_receive_contact.Text.ToString() != receive_contact.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_receive_contact.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", receive_contact.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "收件人");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }


            if (original_receive_tel1.Text.ToString() != receive_tel1.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_receive_tel1.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", receive_tel1.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "收件人電話");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_order_number.Text.ToString() != order_number.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_order_number.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", order_number.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "訂單號碼");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            string new_send_address = send_city.SelectedValue.ToString() + send_area.SelectedValue.ToString() + send_address.Text.ToString();

            if (original_send_address.Text.ToString() != new_send_address)
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_send_address.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", new_send_address);
                cmd4.Parameters.AddWithValue("@revise_item", "寄件地址");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_arrive_assign_date.Text.ToString() != arrive_assign_date.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_arrive_assign_date.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", arrive_assign_date.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "指定日期");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_send_contact.Text.ToString() != send_contact.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_send_contact.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", send_contact.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "寄件人");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }

            if (original_send_tel.Text.ToString() != send_tel.Text.ToString())
            {
                SqlCommand cmd4 = new SqlCommand();

                cmd4.Parameters.AddWithValue("@check_number", lbcheck_number.Text);
                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);
                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);
                cmd4.Parameters.AddWithValue("@original_data", original_send_tel.Text.ToString());
                cmd4.Parameters.AddWithValue("@new_data", send_tel.Text.ToString());
                cmd4.Parameters.AddWithValue("@revise_item", "寄件人電話");
                cmd4.CommandText = dbAdapter.SQLdosomething("revise_data_log", cmd4, "insert");
                dbAdapter.execNonQuery(cmd4);
            }


            #region 95/99產生郵局流水號
            bool hasPostNum = false;
            DataTable tbPost = new DataTable();
            using (SqlCommand sql = new SqlCommand())
            {
                sql.CommandText = string.Format(@"SELECT * FROM [JunFuReal].[dbo].[Post_request] WHERE jf_check_number = '{0}' ORDER BY request_id desc", lbcheck_number.Text);
                tbPost = dbAdapter.getDataTable(sql);
                if (tbPost.Rows.Count > 0)
                {
                    hasPostNum = true;
                }
            }
            if ((area_arrive_code == "95" || area_arrive_code == "99") && !hasPostNum)
            {
                using (SqlCommand sql = new SqlCommand())
                {
                    sql.CommandText = string.Format(@"SELECT * FROM [JunFuReal].[dbo].tcDeliveryRequests WHERE check_number = '{0}' ORDER BY request_id desc", lbcheck_number.Text);
                    DataTable tbRequest = dbAdapter.getDataTable(sql);
                    if (tbRequest.Rows.Count > 0)
                    {
                        string requestId = tbRequest.Rows[0]["request_id"].ToString();
                        requestId = string.Format("{0,8:00000000}", int.Parse(requestId));
                        using (SqlCommand sql2 = new SqlCommand())
                        {
                            var udate = DateTime.Now;
                            var uuser = Session["account_code"];
                            var strpost_number = Func.GetPostNumber();
                            var addr = receive_city.SelectedValue.ToString() + receive_area.SelectedValue.ToString() + receive_address.Text;
                            var zipcode = Func.GetZipCode(addr);
                            var post_check_code = Func.GetPostNumberCheckCode(strpost_number, zipcode);

                            sql2.Parameters.AddWithValue("@jf_request_id", requestId);
                            sql2.Parameters.AddWithValue("@jf_check_number", tbRequest.Rows[0]["check_number"].ToString());
                            sql2.Parameters.AddWithValue("@post_number", strpost_number);
                            sql2.Parameters.AddWithValue("@zipcode5", zipcode);
                            sql2.Parameters.AddWithValue("@check_code", post_check_code);
                            sql2.Parameters.AddWithValue("@cdate", udate);
                            sql2.Parameters.AddWithValue("@udate", udate);
                            sql2.Parameters.AddWithValue("@cuser", uuser);
                            sql2.Parameters.AddWithValue("@uuser", uuser);
                            sql2.CommandText = dbAdapter.genInsertComm("Post_request", false, sql2);
                            dbAdapter.execNonQuery(sql2);
                        }
                    }
                }
            }
            else if ((area_arrive_code == "95" || area_arrive_code == "99") && hasPostNum)
            {
                var addr = receive_city.SelectedValue.ToString() + receive_area.SelectedValue.ToString() + receive_address.Text;
                var zipcode = Func.GetZipCode(addr);
                var post_check_code = Func.GetPostNumberCheckCode(tbPost.Rows[0]["post_number"].ToString(), zipcode);
                string requestId = tbPost.Rows[0]["request_id"].ToString();
                using (SqlCommand sqlCommand2 = new SqlCommand())
                {
                    sqlCommand2.Parameters.AddWithValue("@zipcode5", zipcode);
                    sqlCommand2.Parameters.AddWithValue("@check_code", post_check_code);
                    sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", requestId);
                    sqlCommand2.CommandText = dbAdapter.genUpdateComm("Post_request", sqlCommand2);
                    dbAdapter.execNonQuery(sqlCommand2);
                }
            }
            #endregion

            return;
        }

    }

    protected void search_Click(object sender, EventArgs e)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                if (keyword.Text != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_code", keyword.Text);
                    cmd.Parameters.AddWithValue("@receiver_name", keyword.Text);
                    strWhereCmd += " and (A.receiver_code like '%'+@receiver_code+'%' or A.receiver_name like '%'+@receiver_name+'%') ";
                }

                //if (customer_code.Text != "")
                //{
                //    cmd.Parameters.AddWithValue("@customer_code", customer_code.Text.Length >= 7 ? customer_code.Text.Substring(0, 7) : customer_code.Text);
                //    strWhereCmd += " and (A.customer_code like ''+@customer_code+'%') ";
                //}
                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                strWhereCmd += " and A.customer_code = @customer_code";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List.DataSource = dt;
                    New_List.DataBind();
                }
            }
        }

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.receiver_id=@receiver_id";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                // DT.Rows[0]["customer_code"].ToString().Trim();
                //DT.Rows[0]["receiver_code"].ToString().Trim();
                receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                city_SelectedIndexChanged(receive_city, null);
                receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                city_SelectedIndexChanged(send_city, null);
                receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                divReceiver.Visible = false;
            }
        }

    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = false;
    }

    protected void btnRecsel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = true;
        search_Click(null, null);
    }

    protected void receive_contact_TextChanged(object sender, EventArgs e)
    {
        if (receive_contact.Text != "")
        {
            entertry("receive_contact");
        }
    }




    protected void receiver_code_TextChanged(object sender, EventArgs e)
    {
        if (receiver_code.Text != "")
        {
            entertry("receiver_code");

        }
    }

    protected void entertry(string control)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                switch (control)
                {
                    case "receive_contact":
                        cmd.Parameters.AddWithValue("@receiver_name", receive_contact.Text);
                        strWhereCmd += " and  A.receiver_name like '%'+@receiver_name+'%' ";
                        break;
                    case "receiver_code":
                        cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
                        strWhereCmd += " and  A.receiver_code like '%'+@receiver_code+'%' ";
                        break;
                }
                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        if (DT.Rows.Count == 1)
                        {
                            receiver_id.Text = "";
                            receiver_code.Text = "";
                            receive_contact.Text = "";
                            receive_tel1.Text = "";
                            receive_tel1_ext.Text = "";
                            receive_tel2.Text = "";
                            receive_city.SelectedValue = "";
                            receive_area.SelectedValue = "";
                            receive_address.Text = "";

                            receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                            receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                            receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                            receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                            receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                            receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                            receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                            city_SelectedIndexChanged(receive_city, null);
                            receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                            receive_area_SelectedIndexChanged(null, null);
                            receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                            divReceiver.Visible = false;
                        }
                        else
                        {
                            New_List.DataSource = DT;
                            New_List.DataBind();
                            divReceiver.Visible = true;
                        }

                    }

                }
            }
        }
    }

    protected void CheckAreaIncorrectly(object sender, EventArgs e)
    {
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            string aaa = table.Rows[0]["is_new_customer"].ToString().ToLower();
            is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;

        }

        //if (receive_address.Text == "文化三路二段457號4樓" || receive_address.Text == "文化三路二段457號5樓" || receive_address.Text == "文化三路二段451號4樓" || receive_address.Text == "文化三路二段451號5樓")
        //{
        //    ddlarea_arrive_code.SelectedValue = "28";
        //}
        //判斷特服區
        if (GetSpecialAreaFee(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1").id.ToString() != "0")
        {

        }
        else
        {
            //重選填地址後清空特服費相關
            lbSpecialAreaId.Text = "";
            lbSpecialAreaFee.Text = "";
            CheckAreaIncorrectly_sub();
        }
    }

    void CheckAreaIncorrectly_sub()
    {
        var address = GetAddressParsing(receive_city.Text.Trim() + receive_area.Text.Trim() + receive_address.Text.Trim(), customer_code.Text.Trim(), "1", "1");

        if (address == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#showalert').text('');</script><script>alert('請檢查地址是否正確');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#showalert').text('');</script>", false);
            lb_area_arrive_code.SelectedValue = address.StationCode.ToString();
            lbShuttleStationCode.Text = address.ShuttleStationCode;
            lbReceiveCode.Text = address.StationCode;
            lbReceiveSD.Text = address.SalesDriverCode;
            lbReceiveMD.Text = address.MotorcycleDriverCode;
        }
    }
    protected void CheckMoneyLimit(object sender, EventArgs e)
    {
        bool is_new_customer = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, is_new_customer from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            is_new_customer = Convert.ToBoolean(table.Rows[0]["is_new_customer"]);
        }

        //新客戶且代收金額大於等於1萬 自動勾選保值費 
        if (string.IsNullOrEmpty(collection_money.Text)) { collection_money.Text = "0"; }
        if (string.IsNullOrEmpty(Report_fee.Text)) { Report_fee.Text = "0"; }
        if (is_new_customer == true && Convert.ToInt32(collection_money.Text) >= 10000)
        {
            chkProductValue.Checked = true;

            //取大者顯示
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }
        }
        //有勾選保值費，取大值顯示
        else if (chkProductValue.Checked == true)
        {
            //取大者顯示
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }
        }
    }

    //檢查報值金額  有勾選保值費取大值
    protected void CheckReportLimit(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(collection_money.Text)) { collection_money.Text = "0"; }
        if (string.IsNullOrEmpty(Report_fee.Text)) { Report_fee.Text = "0"; }

        if (chkProductValue.Checked == true)
        {
            if (Convert.ToInt32(Report_fee.Text) > Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(Report_fee.Text) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(Report_fee.Text) < Convert.ToInt32(collection_money.Text))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(collection_money.Text) * 0.01)).ToString();
            }
        }
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        //request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    check_address = true;
                    Info = response.Data.Data;
                    station_code = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }

    public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        SpecialAreaManage Info = new SpecialAreaManage();
        var AddressParsingURL = "http://map2.fs-express.com.tw/Address/GetSpecialAreaDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);
        request.Timeout = 5000;
        request.ReadWriteTimeout = 5000;
        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

        var response = client.Post<ResData<SpecialAreaManage>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                Info = response.Data.Data;
                if (Info.id > 0)
                {

                    //新客戶才顯示特服區提示
                    bool is_new_customer = false;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select is_new_customer from tbCustomers where customer_code = @customer_code";
                        cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                        var table = dbAdapter.getDataTable(cmd);
                        is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
                    }
                    if (is_new_customer == true) { is_special_area.Text = "此區為特服區，費用為: " + Info.Fee + " 元"; }
                    lb_area_arrive_code.SelectedValue = Info.StationCode;
                    area_arrive_code = lb_area_arrive_code.SelectedValue;
                    lbSpecialAreaId.Text = Info.id.ToString();
                    lbSpecialAreaFee.Text = Info.Fee.ToString();
                }
            }
            catch (Exception e)
            {
                throw e;

            }

        }
        else
        {
            throw new Exception("地址解析有誤");
        }

        return Info;
    }

    //保值費
    protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
    {
        //代收金額、報值金額 取大者*1%
        try
        {
            string strcollection_money = string.IsNullOrEmpty(collection_money.Text) ? "0" : collection_money.Text;
            string strReport_fee = string.IsNullOrEmpty(Report_fee.Text) ? "0" : Report_fee.Text;
            if (Convert.ToInt32(strcollection_money) >= Convert.ToInt32(strReport_fee))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strcollection_money) * 0.01)).ToString();
            }
            else if (Convert.ToInt32(strcollection_money) < Convert.ToInt32(strReport_fee))
            {
                tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strReport_fee) * 0.01)).ToString();
            }
            if (chkProductValue.Checked == false) { tbProductValue.Text = ""; }
            //若取消勾選,檢查代收金額欄位是否>1萬欄位
            if (chkProductValue.Checked == false && Convert.ToInt32(collection_money.Text.Trim()) >= 10000)
            {
                chkProductValue.Checked = true;
                if (Convert.ToInt32(strcollection_money) >= Convert.ToInt32(strReport_fee))
                {
                    tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strcollection_money) * 0.01)).ToString();
                }
                else if (Convert.ToInt32(strcollection_money) < Convert.ToInt32(strReport_fee))
                {
                    tbProductValue.Text = (Math.Ceiling(Convert.ToInt32(strReport_fee) * 0.01)).ToString();
                }
            }
        }
        catch { }
    }

}