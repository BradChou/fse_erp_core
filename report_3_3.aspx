﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_3_3.aspx.cs" Inherits="report_3_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">
                <asp:Label ID="lbTitle" runat="server" Text="已送送未配達"></asp:Label></h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                        <label for="inputLastName">配送站所</label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" ></asp:DropDownList>
                        <asp:TextBox ID="kind" runat="server"  style="display: none"></asp:TextBox>
                        <%--<select class="form-control">
                            <option>全選</option>
                        </select>--%>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">發送日期</label>
                        <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                        <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <asp:Button ID="btnQry" CssClass="btn btn-primary" runat="server" Text="查詢" OnClick="btnQry_Click" />
                       
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline text-right">
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                    </div>
                </div>
            </div>
            <hr>
            <p class="text-primary">
                <asp:Label ID="lbsubtitle" runat="server" ></asp:Label>查詢</p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>配送時間</th>
                    <th>配達時間</th>
                    <th>出貨人</th>
                    <th>出貨日期</th>
                    <th>指定日</th>
                    <th>貨號</th> 
                    <th>收件人編號</th>
                    <th>到著站所</th>
                    <th>收貨人</th>
                    <th>收貨地址</th>
                    <th>電話</th>
                    <th>異常原因</th>
                    <th>代收金額</th>
                    <th>到付運費</th>
                    <th>板數</th>
                    <th>才數</th>
                    <th>件數</th>
                    <th>小板數</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr class ="paginate">
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="配送時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date1").ToString())%></td>
                            <td data-th="配達時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date2").ToString())%></td>
                            <td data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td data-th="出貨日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td data-th="指定日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                            <td data-th="貨號">
                                <a href="trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>&date=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString()).Substring(0,7).Replace("/","-")%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></a>
                            </td>
                            <td data-th="收件人編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_customer_code").ToString())%></td>
                            <td data-th="到著站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_station").ToString())%></td>
                            <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="收貨地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_address").ToString())%></td>
                            <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel").ToString())%></td>
                            <td data-th="異常原因"></td>
                            <td data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money").ToString())%></td>
                            <td data-th="到付運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_to_pay_freight").ToString())%></td>
                            <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                            <td data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbm").ToString())%></td>
                            <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                            <td data-th="小板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.splates").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="17" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              <div id="page-nav" class="page"></div>
              
                        <%--<nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>--%>
        </div>
    </div>
</asp:Content>
