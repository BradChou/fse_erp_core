﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using BarcodeLib;
using System.Drawing;
using System.IO;
using BObject.Bobjects;

public partial class palletmag_3_detail : System.Web.UI.Page
{
    public string kind
    {
        get { return ViewState["kind"].ToString(); }
        set { ViewState["kind"] = value; }
    }

    public string customer_code
    {
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string supplier_code
    {
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string sdate
    {
        get { return ViewState["sdate"].ToString(); }
        set { ViewState["sdate"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            kind = Request.QueryString["kind"] != null ? Request.QueryString["kind"].ToString() : "";
            customer_code = Request.QueryString["customer_code"] != null ? Request.QueryString["customer_code"].ToString() : "";
            supplier_code = Request.QueryString["supplier_code"] != null ? Request.QueryString["supplier_code"].ToString() : "";
            sdate = Request.QueryString["sdate"] != null ? Request.QueryString["sdate"].ToString() : "";
            switch (kind)
            {
                case "rtn":
                    Literal1.Text = "實還";
                    break;
                case "owe":
                    Literal1.Text = "欠板";
                    break;
                default:
                    Literal1.Text = "應還";
                    break;
            }
            lbsdate.Text = sdate + "~" + DateTime.Today.ToString("yyyy/MM/dd");
            lbcustomer.Text = customer_code;
            lbsupplier.Text = supplier_code;
            readdata();
        }
    }

    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = "";
            string strFromCmd = "";
            cmd.Parameters.Clear();

            #region 關鍵字
            switch (kind)
            {
                case "all": //應還
                    strWhereCmd += " and A.pallet_recycling_flag = 1 and A.cancel_date  is NULL  and A.plates > 0";
                    break;
                case "rtn": //實還
                    strFromCmd += " inner join ttPlletLog P with(Nolock) on A.request_id = P.request_id";
                    break;
                case "owe": //尚欠
                    strWhereCmd += " and A.pallet_recycling_flag = 1 and A.cancel_date  is NULL  and A.plates > 0 and ISNULL(P.pallet_id,0)<=0";
                    strFromCmd += " left join ttPlletLog P with(Nolock) on A.request_id = P.pallet_id";
                    break;
            }

            if (customer_code != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", customer_code);
                strWhereCmd += " and A.customer_code like '%'+@customer_code+'%'";
            }

            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@area_arrive_code", supplier_code);
                strWhereCmd += " and A.area_arrive_code =@area_arrive_code ";
            }

            #endregion

            cmd.CommandText = string.Format(@"SELECT distinct A.pricing_type,A.request_id
                                                ,CONVERT(CHAR(10),A.supplier_date,111) supplier_date, A.check_number , LEFT( A.customer_code,3) as sendstation , A.receive_contact , 
                                                 CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(C.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation2,
                                                 A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area ,
                                                 CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                                 A.pieces , A.plates , A.cbm ,
                                                 B.code_name , A.customer_code , C.customer_shortname ,
                                                 A.send_contact,A.send_city , A.send_area , A.send_address , A.cancel_date,A.add_transfer
                                                ,(SELECT COUNT(1) FROM ttDeliveryScanLog With(Nolock) WHERE check_number = A.check_number) logNum 
                                                ,A.uuser, D.user_name ,A.print_date, A.area_arrive_code
                                          From tcDeliveryRequests A With(Nolock)
                                          {0}
                                          left join tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C With(Nolock) on C.customer_code = A.customer_code 
                                          left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                          where 1= 1 
                                          {1} order by A.print_date, A.check_number", strFromCmd, strWhereCmd);

            DataTable dt = dbAdapter.getDataTable(cmd);
            int Cnt = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    Cnt += Convert.ToInt32(dt.Rows[i]["plates"]);
                }
                DataRow row = dt.NewRow();
                row["user_name"] = "總計";
                row["plates"] = Cnt;
                dt.Rows.Add(row);
            }

            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
        }
    }



    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    

}