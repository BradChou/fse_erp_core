﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterReport.master" AutoEventWireup="true" CodeFile="report_3_4.aspx.cs" Inherits="report_3_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">簽單回傳</h2>
            <div class="templatemo-login-form" >
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label><asp:Label ID="lbSuppliers" runat="server" Text="配送站所"></asp:Label></label>
                        <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" ></asp:DropDownList>                       
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-6 col-md-6 form-group form-inline">
                        <label for="inputLastName">發送日期</label>
                        <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                        <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>  
                        <asp:Button ID="btnQry" CssClass="btn btn-primary" runat="server" Text="查詢" OnClick="btnQry_Click" />                     
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline text-right ">
                        <asp:Button ID="btPrint" CssClass="btn btn-warning " runat="server" Text="匯出" OnClick="btPrint_Click"  />
                    </div>
                </div>
            </div>
            <hr>
            <p class="text-primary">簽單回傳查詢</p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th>NO.</th>
                    <th>配達時間</th>
                    <th>出貨人</th>
                    <th>出貨日期</th>
                    <th>指定日</th>
                    <th>貨號</th> 
                    <th>收件人編號</th>
                    <th>到著站所</th>
                    <th>收貨人</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="配達時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                            <td data-th="出貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>                            
                            <td data-th="出貨日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td data-th="指定日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                            <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                            <td data-th="收件人編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_customer_code").ToString())%></td>
                            <td data-th="到著站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_station").ToString())%></td>
                            <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="9" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              
                        <nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>
        </div>
    </div>
</asp:Content>
