﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_sales.aspx.cs" Inherits="system_sales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script src="js/jquery-1.11.2.min.js"></script>
    <style type="text/css">
        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .td_th {
            text-align: center;
        }

        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="templatemo-content-widget white-bg">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="營業員主檔" runat="server"></asp:Label></h2>



        <asp:Panel ID="pan_view" runat="server">
            <div class="div_view">
                <!-- 查詢 -->
                <div class="form-group form-inline">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 70%;">
                                <label>查詢條件：</label>

                                <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 營業員姓名"></asp:TextBox>
                                <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control" ToolTip="是否生效"></asp:DropDownList>
                                <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                                <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1" />
                            </td>
                            <td style="width: 30%; float: right;">
                                <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- 查詢 end -->
                <!--內容-list -->

                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr>
                        <th class="td_th td_no">No</th>
                        <th class="td_th td_area">區域</th>
                        <th class="td_th td_type">職稱</th>
                        <th class="td_th td_name">姓名</th>
                        <th class="td_th td_tel">電話</th>
                        <th class="td_th td_email">Email</th>
                        <th class="td_th td_yn">是否生效</th>
                        <th class="td_th">建檔人</th>
                        <th class="td_th">建檔日期</th>
                        <th class="td_th td_edit">功　　能</th>
                    </tr>
                    <asp:Repeater ID="list_sales" runat="server" OnItemCommand="list_sales_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                                <td class="td_data td_area" data-th="區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area").ToString())%></td>
                                <td class="td_data td_type" data-th="職稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.type").ToString())%></td>
                                <td class="td_data td_name" data-th="姓名"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.name").ToString())%></td>
                                <td class="td_data td_tel" data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tel").ToString())%></td>
                                <td class="td_data td_email" data-th="Email"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.email").ToString())%></td>
                                <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.IsActive").ToString())%></td>
                                <td class="td_data " data-th="建檔人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cuser").ToString())%></td>
                                <td class="td_data " data-th="建檔日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td class="td_data td_edit" data-th="編輯">
                                    <asp:HiddenField ID="id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                    <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_sales.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="10" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總筆數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
                <!--內容-end -->
            </div>
        </asp:Panel>

        <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
            <hr />
            <table class="tb_edit">
                <tr>
                    <th>
                        <label class="_tip_important">區  域</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_area" runat="server" CssClass="form-control" AutoPostBack="true" >
                            <asp:ListItem Value="">請選擇</asp:ListItem>
                            <asp:ListItem Value="0">總部</asp:ListItem>
                            <asp:ListItem Value="1">北區</asp:ListItem>
                            <asp:ListItem Value="2">中區</asp:ListItem>
                            <asp:ListItem Value="3">南區</asp:ListItem>
                            <asp:ListItem Value="99">金來</asp:ListItem>
                            <asp:ListItem Value="-1">其他</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req11" runat="server" ControlToValidate="ddl_area" ForeColor="Red" ValidationGroup="validate" AutoPostBack="true">請選擇區域</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label class="_tip_important">職  稱</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_type" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_type" ForeColor="Red" ValidationGroup="validate">請選擇職稱</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th class="_edit_title">
                        <label class="_tip_important">帳號類型</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:DropDownList ID="ddl_account_type" runat="server" class="form-control _ddl chosen-select" AutoPostBack="true" OnSelectedIndexChanged="account_type_SelectedIndexChanged">
                            <asp:ListItem Value="">請選擇</asp:ListItem>
                            <asp:ListItem Value="BF">一般帳號</asp:ListItem>
                            <asp:ListItem Value="DRIVER">司機工號</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddl_account_type" ForeColor="Red" ValidationGroup="validate">請選擇類型</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th class="_edit_title">
                        <label class="_tip_important">營業員姓名</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:DropDownList ID="ddl_sales_name" runat="server" class="form-control _ddl chosen-select" AutoPostBack="true" OnSelectedIndexChanged="sales_name_SelectedIndexChanged"></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_sales_name" runat="server" ControlToValidate="ddl_sales_name" ForeColor="Red" ValidationGroup="validate">請選擇營業員</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>電  話</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="tel" runat="server" CssClass="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" OnSelectedIndexChanged="ddl_tel_edit_OnChanged" AutoPostBack="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>E-mail</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="email" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>銀  行</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_bank" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>銀行分行</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="bank_branch" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>銀行帳號</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="bank_account" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <%--<tr>
                    <th>
                        <label>推薦人</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_recommender" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>--%>
                <tr>
                    <th>
                        <label class="_tip_important">是否生效</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:RadioButtonList ID="rb_Active" runat="server" ToolTip="請設定是否生效!" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px">
                            <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                            <asp:ListItem Value="0">否</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>

                </tr>




                <tr>
                    <td colspan="4" style="text-align: center;">
                        <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                        <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>
</asp:Content>

