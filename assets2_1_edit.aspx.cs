﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets2_1_edit : System.Web.UI.Page
{
    public string a_id
    {
        get { return ViewState["a_id"].ToString(); }
        set { ViewState["a_id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            a_id = Request.QueryString["a_id"] != null ? Request.QueryString["a_id"].ToString() : "";

            #region 費用類別
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1 order by code_id ";
                ddl_fee.DataSource = dbAdapter.getDataTable(cmd);
                ddl_fee.DataValueField = "code_id";
                ddl_fee.DataTextField = "code_name";
                ddl_fee.DataBind();
                ddl_fee.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 油品公司
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes WHERE (code_bclass = '6') AND (code_sclass = 'oil') and active_flag = 1 order by code_id ";
                oil.DataSource = dbAdapter.getDataTable(cmd);
                oil.DataValueField = "code_name";
                oil.DataTextField = "code_name";
                oil.DataBind();
                oil.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            readdata(a_id);

        }
    }

    private void readdata(string a_id)
    {  
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", a_id);
            
            cmd.CommandText = @"Select * from ttAssetsFee A with(nolock) 
                                Where id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                ddl_fee.SelectedValue = dt.Rows[0]["fee_type"].ToString().Trim();
                car_license.Text = dt.Rows[0]["a_id"].ToString().Trim();
                idate.Text = Convert.ToDateTime(dt.Rows[0]["date"]).ToString("yyyy/MM/dd");
                itime.Text = Convert.ToDateTime(dt.Rows[0]["date"]).ToString("HH:mm:ss");
                fee_sdate.Text = dt.Rows[0]["fee_sdate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["fee_sdate"]).ToString("yyyy/MM/dd") : "";
                fee_edate.Text = dt.Rows[0]["fee_edate"] != DBNull.Value ?  Convert.ToDateTime(dt.Rows[0]["fee_edate"]).ToString("yyyy/MM/dd") :"";

                oil.SelectedValue = dt.Rows[0]["oil"].ToString().Trim();
                company.Text = dt.Rows[0]["company"].ToString().Trim();
                items.Text = dt.Rows[0]["items"].ToString().Trim();
                detail.Text = dt.Rows[0]["detail"].ToString().Trim();
                quant.Text = dt.Rows[0]["quant"].ToString().Trim();
                milage.Text = dt.Rows[0]["milage"].ToString().Trim();
                litre.Text = dt.Rows[0]["litre"].ToString().Trim();
                price_notax.Text = dt.Rows[0]["price_notax"].ToString().Trim();
                buy_price_notax.Text = dt.Rows[0]["buy_price_notax"].ToString().Trim();
                receipt_price_notax.Text = dt.Rows[0]["receipt_price_notax"].ToString().Trim();
                
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();

                if (ddl_fee.SelectedValue == "1")
                {
                    lblitre.Style.Add("display", "inline-block");
                    litre.Style.Add("display", "inline-block");
                    lbquant.Style.Add("display", "none");
                    quant.Style.Add("display", "none");
                    total_price.Text = (Convert.ToDouble (dt.Rows[0]["litre"]) * Convert.ToDouble(dt.Rows[0]["price_notax"])) .ToString("N0");
                    total_buy.Text = (Convert.ToDouble(dt.Rows[0]["litre"]) * Convert.ToDouble(dt.Rows[0]["buy_price_notax"])).ToString("N0");
                    total_receipt.Text = (Convert.ToDouble(dt.Rows[0]["litre"]) * Convert.ToDouble(dt.Rows[0]["receipt_price_notax"])).ToString("N0");

                }
                else
                {
                    lblitre.Style.Add("display", "none");
                    litre.Style.Add("display", "none");
                    lbquant.Style.Add("display", "inline-block");
                    quant.Style.Add("display", "inline-block");
                    total_price.Text = (Convert.ToDouble(dt.Rows[0]["quant"]) * Convert.ToDouble(dt.Rows[0]["price_notax"])).ToString("N0");
                    total_buy.Text = (Convert.ToDouble(dt.Rows[0]["quant"]) * Convert.ToDouble(dt.Rows[0]["buy_price_notax"])).ToString("N0");
                    total_receipt.Text = (Convert.ToDouble(dt.Rows[0]["quant"]) * Convert.ToDouble(dt.Rows[0]["receipt_price_notax"])).ToString("N0");
                }
            }
        }
        else
        {
            ApStatus = "Add";
        }

       
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                //car_license.ReadOnly  = true ;
                statustext.Text = "修改";
                break;
            case "Add":                
                statustext.Text = "新增";
                idate.Text = DateTime.Now.ToString("yyyy/MM/dd");
                itime.Text = DateTime.Now.ToString("HH:mm:ss");
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        DateTime _fee_sdate;
        DateTime _fee_edate;
        int _receipt_price_notax = 0;
        int _buy_price_notax = 0;
        int _price_notax = 0;

        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":

                #region 判斷重覆
                //SqlCommand cmda = new SqlCommand();
                //DataTable dta = new DataTable();
                //cmda.Parameters.AddWithValue("@car_license", car_license.Text);                
                //cmda.CommandText = "Select a_id from ttAssetsFee with(nolock) where car_license=@car_license ";
                //dta = dbAdapter.getDataTable(cmda);
                //if (dta.Rows.Count > 0)
                //{
                //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                //    return;
                //}
                #endregion

                SqlCommand cmdadd = new SqlCommand();
                cmdadd.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue);
                cmdadd.Parameters.AddWithValue("@a_id", car_license.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@date", idate.Text.ToString().Trim()+" "+ itime.Text.ToString().Trim());

                if (DateTime.TryParse(fee_sdate.Text, out _fee_sdate) && DateTime.TryParse(fee_edate.Text, out _fee_edate))
                {
                    cmdadd.Parameters.AddWithValue("@fee_sdate", _fee_sdate);
                    cmdadd.Parameters.AddWithValue("@fee_edate", _fee_edate);
                }

                cmdadd.Parameters.AddWithValue("@oil", ddl_fee.SelectedValue);                
                cmdadd.Parameters.AddWithValue("@company", company.Text.ToString().Trim());                
                cmdadd.Parameters.AddWithValue("@items", items.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@detail", detail.Text.ToString().Trim());

                switch (ddl_fee.SelectedValue)
                {
                    case "1":
                        cmdadd.Parameters.AddWithValue("@litre", litre.Text.ToString().Trim());
                        break;
                    default:
                        cmdadd.Parameters.AddWithValue("@quant", quant.Text.ToString().Trim());
                        break;
                }
                
                cmdadd.Parameters.AddWithValue("@milage", milage.Text.ToString().Trim());                
                cmdadd.Parameters.AddWithValue("@price_notax", price_notax.Text.ToString().Trim());
                if (int.TryParse(price_notax.Text.ToString().Trim(), out _price_notax))
                {
                    cmdadd.Parameters.AddWithValue("@price", Convert.ToInt32(_price_notax * 1.05));
                }

                cmdadd.Parameters.AddWithValue("@buy_price_notax", buy_price_notax.Text.ToString().Trim());
                if (int.TryParse(buy_price_notax.Text.ToString().Trim(), out _buy_price_notax))
                {
                    cmdadd.Parameters.AddWithValue("@buy_price", Convert.ToInt32(_buy_price_notax * 1.05));
                }

                cmdadd.Parameters.AddWithValue("@receipt_price_notax", receipt_price_notax.Text.ToString().Trim());
                if (int.TryParse(receipt_price_notax.Text.ToString().Trim(), out _receipt_price_notax))
                {
                    cmdadd.Parameters.AddWithValue("@receipt_price", Convert.ToInt32 (_receipt_price_notax *1.05));
                }
                cmdadd.Parameters.AddWithValue("@total_price", total_price.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdadd.CommandText = dbAdapter.SQLdosomething("ttAssetsFee", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;
            case "Modity":
                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue);
                cmdupd.Parameters.AddWithValue("@a_id", car_license.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@date", idate.Text.ToString().Trim() + " " + itime.Text.ToString().Trim());
                if (DateTime.TryParse(fee_sdate.Text, out _fee_sdate) && DateTime.TryParse(fee_edate.Text, out _fee_edate))
                {
                    cmdupd.Parameters.AddWithValue("@fee_sdate", _fee_sdate);
                    cmdupd.Parameters.AddWithValue("@fee_edate", _fee_edate);
                }
                cmdupd.Parameters.AddWithValue("@oil", ddl_fee.SelectedValue);
                cmdupd.Parameters.AddWithValue("@company", company.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@items", items.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@detail", detail.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@quant", quant.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@milage", milage.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@litre", litre.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@price_notax", price_notax.Text.ToString().Trim());
                if (int.TryParse(price_notax.Text.ToString().Trim(), out _price_notax))
                {
                    cmdupd.Parameters.AddWithValue("@price", Convert.ToInt32(_price_notax * 1.05));
                }
                cmdupd.Parameters.AddWithValue("@buy_price_notax", buy_price_notax.Text.ToString().Trim());
                if (int.TryParse(buy_price_notax.Text.ToString().Trim(), out _buy_price_notax))
                {
                    cmdupd.Parameters.AddWithValue("@buy_price", Convert.ToInt32(_buy_price_notax * 1.05));
                }
                cmdupd.Parameters.AddWithValue("@receipt_price_notax", receipt_price_notax.Text.ToString().Trim());
                if (int.TryParse(receipt_price_notax.Text.ToString().Trim(), out _receipt_price_notax))
                {
                    cmdupd.Parameters.AddWithValue("@receipt_price", Convert.ToInt32(_receipt_price_notax * 1.05));
                }
                cmdupd.Parameters.AddWithValue("@total_price", total_price.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@memo", memo.Text.ToString().Trim());
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", a_id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttAssetsFee", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='assets2_1.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }


    protected void fee_SelIndexChanged(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    protected void sdate_TextChanged(object sender, EventArgs e)
    {

    }

    protected void edate_TextChanged(object sender, EventArgs e)
    {

    }
}