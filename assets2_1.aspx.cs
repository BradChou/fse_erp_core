﻿using ClosedXML.Excel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class assets2_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 使用單位
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'dept' and active_flag = 1 order by code_id ";
                ddl_dept.DataSource = dbAdapter.getDataTable(cmd);
                ddl_dept.DataValueField = "code_id";
                ddl_dept.DataTextField = "code_name";
                ddl_dept.DataBind();
                ddl_dept.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 車行
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'CD' and active_flag = 1 order by code_id ";
                ddl_car_retailer.DataSource = dbAdapter.getDataTable(cmd);
                ddl_car_retailer.DataValueField = "code_id";
                ddl_car_retailer.DataTextField = "code_name";
                ddl_car_retailer.DataBind();
                ddl_car_retailer.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 費用類別
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1 order by code_id ";
                ddl_fee.DataSource = dbAdapter.getDataTable(cmd);
                ddl_fee.DataValueField = "code_id";
                ddl_fee.DataTextField = "code_name";
                ddl_fee.DataBind();
                ddl_fee.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion
            
            #region 初始化日期
            sdate.Text = DateTime.Now.ToString("yyy/MM/01");
            edate.Text = DateTime.Now.ToString("yyy/MM/dd");
            #endregion

            //readdata();
        }



    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (ddl_fee.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue.ToString());
            wherestr += "  and a.fee_type =@fee_type";
        }

        if (sdate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
            wherestr += "  and a.date >=@sdate";
        }
        if (edate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@edate",Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
            wherestr += "  and a.date <=@edate";
        }

        if (car_license.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@a_id", car_license.Text.Trim().ToString());
            wherestr += "  and a.a_id like '%'+@a_id+'%'";
        }

        if (ddl_dept.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@dept", ddl_dept.SelectedValue.ToString());
            wherestr += "  and c.dept =@dept";
        }

        if (ddl_fee.SelectedValue.ToString() == "1" && ddl_oil.SelectedValue != "")
        {
            objCommand.Parameters.AddWithValue("@oil", ddl_oil.SelectedValue.ToString());
            wherestr += "  and a.oil =@oil";
        }
        else if (ddl_fee.SelectedValue.ToString() != "1" && company.Text  != "")
        {
            objCommand.Parameters.AddWithValue("@company", company.Text);
            wherestr += "  and a.company like '%'+@company+'%'";
        }

        if (ddl_car_retailer.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_retailer", ddl_car_retailer.SelectedValue.ToString());
            wherestr += "  and c.car_retailer =@car_retailer";
        }

        if (owner.Text != "")
        {
            objCommand.Parameters.AddWithValue("@owner", owner.Text.Trim());
            wherestr += "  and c.owner like '%'+@owner+'%'";
        }

        //objCommand.CommandText = string.Format(@"select A.* 
        //                                            from ttAssetsFee A with(nolock)
        //                                        where 1=1  {0} 
        //                                        order by a.date asc", wherestr);

        objCommand.CommandText = string.Format(@"Select A.[id] ,A.[fee_type] ,A.[dept_id] ,A.[a_id] ,A.[date] ,A.[oil] ,A.[company] ,A.[items] ,A.[detail] ,A.[milage] ,A.[litre],
                                                 A.[price] ,A.[buy_price] ,A.[receipt_price] ,A.[price_notax] ,A.[buy_price_notax] ,A.[receipt_price_notax] ,A.[quant] ,A.[memo], 
                                                 case when A.fee_type = '1' then A.litre else A.quant end 'showquant',
                                                 case when A.fee_type = '1' then A.litre * price_notax  else A.quant * price_notax end 'total_price', 
                                                 case when A.fee_type = '1' then A.litre * buy_price_notax  else A.quant * buy_price_notax end 'total_buy',
                                                 case when A.fee_type = '1' then A.litre * receipt_price_notax  else A.quant * receipt_price_notax end 'total_receipt',
                                                 b.code_name 'feename', C.tonnes , D.code_name 'deptname', E.code_name 'car_retailer_name', C.owner , C.driver ,
                                                 F.user_name , A.udate
                                                 from ttAssetsFee A with(nolock)
	                                             LEFT JOIN tbItemCodes B WITH(NOLOCK) ON A.fee_type  = b.code_id  AND B.code_bclass = '6' AND B.code_sclass = 'fee_type' and B.active_flag = 1
	                                             LEFT JOIN ttAssets C with (nolock) on A.a_id = C.car_license  
	                                             LEFT JOIN tbItemCodes D with(nolock) on C.dept = D.code_id  AND D.code_bclass = '6' AND D.code_sclass = 'dept'  and D.active_flag = 1
	                                             LEFT JOIN tbItemCodes E with(nolock) on C.car_retailer = E.code_id AND E.code_bclass = '6' AND E.code_sclass = 'CD'  and E.active_flag = 1
                                                 LEFT JOIN tbAccounts  F With(Nolock) on F.account_code = A.uuser
                                                 where 1=1  {0} 
                                                 order by a.date asc", wherestr);


        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
            
        }

        if (Upd_title.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            Upd_title.Update();
        }
        if (Upd_data.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            Upd_data.Update();
        }
        
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "unblockUI", "$.unblockUI();", true); ;

    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = p.Field<double >("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    protected void ddl_fee_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_fee.SelectedValue == "1")
        {
            lb_oil.Visible = true;
            ddl_oil.Visible = true;
            lb_company.Visible = false;
            company.Visible = false;
        }
        else
        {
            lb_oil.Visible = false;
            ddl_oil.Visible = false;
            lb_company.Visible = true;
            company.Visible = true;
        }

        if (Upd_title.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            Upd_title.Update();
        }
    }

    protected void export_Click(object sender, EventArgs e)
    {
        string sheet_title = "每月費用";
        string file_name = "每月費用" + sdate.Text.ToString() + "~" + edate.Text.ToString();
        using (SqlCommand objCommand = new SqlCommand())
        {
            string wherestr = "";
            if (ddl_fee.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue.ToString());
                wherestr += "  and a.fee_type =@fee_type";
            }

            if (sdate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
                wherestr += "  and a.date >=@sdate";
            }
            if (edate.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
                wherestr += "  and a.date <=@edate";
            }

            if (car_license.Text.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@a_id", car_license.Text.Trim().ToString());
                wherestr += "  and a.a_id like '%'+@a_id+'%'";
            }

            if (ddl_dept.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@dept", ddl_dept.SelectedValue.ToString());
                wherestr += "  and c.dept =@dept";
            }

            if (ddl_fee.SelectedValue.ToString() == "1" && ddl_oil.SelectedValue != "")
            {
                objCommand.Parameters.AddWithValue("@oil", ddl_oil.SelectedValue.ToString());
                wherestr += "  and a.oil =@oil";
            }
            else if (ddl_fee.SelectedValue.ToString() != "1" && company.Text != "")
            {
                objCommand.Parameters.AddWithValue("@company", company.Text);
                wherestr += "  and a.company like '%'+@company+'%'";
            }

            if (ddl_car_retailer.SelectedValue.ToString() != "")
            {
                objCommand.Parameters.AddWithValue("@car_retailer", ddl_car_retailer.SelectedValue.ToString());
                wherestr += "  and c.car_retailer =@car_retailer";
            }

            if (owner.Text != "")
            {
                objCommand.Parameters.AddWithValue("@owner", owner.Text.Trim());
                wherestr += "  and c.owner like '%'+@owner+'%'";
            }


            objCommand.CommandText = string.Format(@"Select b.code_name '費用類別', A.[date] '交易日期',A.[a_id] '車牌',  C.tonnes '噸數', D.code_name '使用單位',  E.code_name '車行',C.owner '車主' ,
                                                     C.driver '司機', case when A.fee_type = '1' then A.litre else A.quant end '數量', A.[milage] '里程數', A.[price_notax] '牌價單價(未稅)' ,
                                                     A.[price] '牌價單價(含稅)' ,case when A.fee_type = '1' then A.litre * price_notax  else A.quant * price_notax end '牌價總額(未稅)', 
                                                     A.[buy_price_notax] '購入單價(未稅)', A.[buy_price] '購入單價(含稅)' , 
                                                     case when A.fee_type = '1' then A.litre * buy_price_notax  else A.quant * buy_price_notax end '購入總額(未稅)',
                                                     A.[receipt_price_notax] '收款單價(未稅)' , A.[receipt_price] '收款單價(含稅)',
                                                     case when A.fee_type = '1' then A.litre * receipt_price_notax  else A.quant * receipt_price_notax end '收款總額(未稅)',A.[memo] '備註'
                                                 from ttAssetsFee A with(nolock)
	                                             LEFT JOIN tbItemCodes B WITH(NOLOCK) ON A.fee_type  = b.code_id  AND B.code_bclass = '6' AND B.code_sclass = 'fee_type' and B.active_flag = 1
	                                             LEFT JOIN ttAssets C with (nolock) on A.a_id = C.car_license  
	                                             LEFT JOIN tbItemCodes D with(nolock) on C.dept = D.code_id  AND D.code_bclass = '6' AND D.code_sclass = 'dept'  and D.active_flag = 1
	                                             LEFT JOIN tbItemCodes E with(nolock) on C.car_retailer = E.code_id AND E.code_bclass = '6' AND E.code_sclass = 'CD'  and E.active_flag = 1
                                                 where 1=1  {0} 
                                                 order by a.date asc", wherestr);
            
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 2])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                        using (ExcelRange col = ws.Cells[2, 9, 2 + dt.Rows.Count, 19])
                        {
                            col.Style.Numberformat.Format = "#,##0";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;                        
                        Response.Clear();
                        Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "unblockUI", "$.unblockUI();", true); ;

    }
}