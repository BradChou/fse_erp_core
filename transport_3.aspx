﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="transport_3.aspx.cs" Inherits="transport_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .tb_title, ._d_function {
            text-align: center;
        }

        .tb_detail {
            text-align: left;
        }

        .btn {
            margin: 3px;
        }

        ._h_function {
            width: 10%;
        }

        ._h_addr {
            width: 30%;
        }

        ._h_auto {
            min-width: 5px;
        }

        .div_sh, ._top_left {
            width: 75%;
            float: left;
        }

        .div_btn {
            width: 30%;
            float: right;
            right: 3%;
        }

        .tb_top {
            width: 100%;
        }

        ._top_right {           
            float: right;
            right: 5px;
        }

        ._hr {
            display: inline-table;
        }

        .detail_btn_area {
            text-align: center;
        }
        ._detail{
            width:68%;
            text-align:left;
            margin:10px;
           
        }
        ._detail_title{
            width:10%;   
        }
        ._detail_data{
            width:40%;    
            padding:3px !important;
        }
        ._ext{width:70px !important;}
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
         .table_list tr:hover {
            background-color: lightyellow;
        }
         textarea{
             margin:5px 0px;
         }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
        });
        function Receiver_dw() {
            var rec_cuscode = $(".rec_cuscode").val();
            var rec_code = $(".rec_code").val();
            var rec_name = $(".rec_name").val();
            var rec_less = document.getElementById('<%=Less.ClientID %>').value;
         
         
            var the_dw = $("input[name=dw");
            var url_dw = "";
            url_dw = "GetReport.aspx?type=receiver&rec_code=" + rec_code + "&rec_name=" + rec_name + "&rec_cuscode=" + rec_cuscode + "&Lesstype=" + rec_less;
           


            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            setTimeout("parent.$.fancybox.close()", 20000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">收貨人管理</h2>

            <asp:Panel ID="pan_list" runat="server">

                 <div style="display:none"><asp:TextBox ID="Less" runat="server" CssClass="rec_less"></asp:TextBox></div>
                            
                <div class="col-lg-12 form-inline form-group">
                    <table class="tb_top">
                        <tr>
                            <td class="_top_left">查詢條件：
                            <span class="section">1</span>
                                 <label>客　　代：</label>
                                <asp:DropDownList ID="second_code" runat="server" CssClass="form-control rec_cuscode chosen-select" ></asp:DropDownList>
                            <span class="section">2</span>
                                <label>查詢代號：</label>
                                <asp:TextBox ID="receiver_code" runat="server" CssClass="rec_code"></asp:TextBox>
                            <span class="section">3</span>
                                <label>名稱：</label>
                                <asp:TextBox ID="receiver_name" runat="server" CssClass="rec_name"></asp:TextBox>
                            </td>
                           

                            <td class="_top_right">
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                <asp:Button ID="btn_New" runat="server" class="btn btn-warning" Text="新 增" OnClick="btn_New_Click" />
                                <input name="dw" type="button" class="btn btn-primary" value="下 載" onclick='Receiver_dw()' />
                                <a href="ReceiverUpload.aspx?type=receiver" class="fancybox fancybox.iframe" id="btnupload"><span class="btn btn-primary">上 傳</span></a>
                            </td>
                        </tr>
                    </table>
                    <hr />
                </div>
                <div class="wi-">

                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr class="tr-only-hide">
                        <th class="tb_title _h_function">功　　能</th>
                        <th class="tb_title _h_auto">客戶代碼</th>
                        <th class="tb_title _h_auto">收貨人代碼</th>
                        <th class="tb_title _h_auto">收貨人</th>
                        <th class="tb_title _h_auto">電話1</th>
                        <th class="tb_title _h_auto">電話2</th>
                        <th class="tb_title _h_addr">地址</th>
                        <th class="tb_title _h_auto">備註</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="_d_function" data-th="功能">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                    <asp:HiddenField ID="hid_customer_code" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%>' />
                                    
                                    <asp:Button ID="btselect" CssClass="btn btn-primary _d_btn " CommandName="cmdSelect" runat="server" Text="修 改" />
                                    <asp:Button ID="btn_del" CssClass="btn btn-danger _d_btn" CommandName="cmddelete" OnClientClick="return confirm('確定要刪除此筆資料嗎?');" runat="server" Text="刪 除" />
                                </td>
                                <td class="tb_detail " data-th="客戶"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                <td class="tb_detail _d_receiver" data-th="收貨人代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                <td class="tb_detail " data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                <td class="tb_detail _d_tel" data-th="電話1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tel").ToString())%></td>
                                <td class="tb_detail _d_tel2" data-th="電話2"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tel2").ToString())%></td>
                                <td class="tb_detail _d_function" data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.addr").ToString())%></td>
                                <td class="tb_detail " data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="8" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    客戶總數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
            </asp:Panel>



            <asp:Panel ID="pan_detail" runat="server"  Visible="false">

                <table class="_detail">
                    <tr class="form-group">
                        <th class="_detail_title">
                            <label class="_tip_important">客戶代碼</label></th>
                        <td class="_detail_data form-inline">
                             <asp:DropDownList ID="dlcustomer_code" runat="server" class="form-control chosen-select" >
                            </asp:DropDownList>
                           <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代編號</asp:RequiredFieldValidator>
                        </td>
                        <th class="_detail_title">
                            <label class="_tip_important">收貨人代碼</label></th>
                        <td class="_detail_data form-inline">
                            <asp:TextBox ID="receiver_code2" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="receiver_code2" ForeColor="Red" ValidationGroup="validate">請輸入收貨人代碼</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr class="form-group">
                        <th>
                            <label class="_tip_important">收貨人</label></th>
                        <td class="_detail_data form-inline">
                            <asp:TextBox ID="receiver_name2" runat="server" class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server" ControlToValidate="receiver_name2" ForeColor="Red" ValidationGroup="validate">請輸入收貨人</asp:RequiredFieldValidator>
                        </td>
                        <th>
                            <label class="_tip_important">地　　　址</label></th>
                        <td class="_detail_data form-inline">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="City" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="CityArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    <asp:TextBox ID="address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="City" ForeColor="Red" ValidationGroup="validate">請選擇縣市</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="CityArea" ForeColor="Red" ValidationGroup="validate">請選擇鄉鎮區</asp:RequiredFieldValidator>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr class="form-group">
                        <th>
                            <label class="_tip_important">電　話1(分機)</label></th>
                        <td class="_detail_data form-inline">
                            <asp:TextBox ID="tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                            (<asp:TextBox ID="tel_ext" CssClass="form-control _ext" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="4"></asp:TextBox>)
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req9" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">請輸入電話1</asp:RequiredFieldValidator>
                        </td>
                        <th><label class ="text-right ">電　　話2</label> </th>
                        <td class="_detail_data form-inline">
                            <asp:TextBox ID="tel2" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox></td>
                    </tr>
                    <tr class="form-group">
                        <th>
                            <label>備　　註</label></th>
                        <td class="_detail_data form-inline" colspan="3">                           
                            <textarea id="memo" runat="server" cols="30" rows="3" maxlength="20" class="form-control"></textarea>
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr class="form-group">
                        <td colspan="4" class="detail_btn_area">
                            <asp:Button ID="btnAdd" CssClass="btn btn-primary" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnCancel" CssClass="btn btn-default " runat="server" Text="取 消" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>

            </asp:Panel>
        </div>
    </div>
</asp:Content>

