﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class assets1_2_carsel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            readdata();
        }
    }


    private void readdata()
    {
       
        using (SqlCommand cmd = new SqlCommand())
        {
           
            cmd.Parameters.Clear();
            string wherestr = "";

            #region 關鍵字
            if (keyword.Text.ToString() != "")
            {
                cmd.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
                wherestr += "  and (a.car_license like '%'+@keyword+'%' or a.assets_name like '%'+@keyword+'%')";
            }
            #endregion

            cmd.CommandText = string.Format(@"select A.* , OS.code_name as ownership_text ,CD.code_name as car_retailer_text  , CT.code_name  as cabin_type_text
                                                    from ttAssets A with(nolock)
                                                    left join tbItemCodes OS with(nolock) on A.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                    left join tbItemCodes CD with(nolock) on A.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD'
                                                    left join tbItemCodes CT with(nolock) on A.cabin_type  = CT.code_id and CT.code_bclass = '6' and CT.code_sclass= 'cabintype'
                                                where 1=1  {0} 
                                                order by a.get_date asc", wherestr);


            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
            
        }           

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {
            
            string a_id = ((HiddenField)e.Item.FindControl("Hid_id")).Value.ToString().Trim();
            string car_license = ((Literal)e.Item.FindControl("Licar_license")).Text.Trim();
            //string manager_type = ((HiddenField)e.Item.FindControl("Hid_mangtype")).Value.ToString().Trim();
            //string customer_shortname = ((Literal)e.Item.FindControl("LiCus_name")).Text.Trim();
            //string job_title = ((Literal)e.Item.FindControl("Lijob_title")).Text.Trim();
            //string user_name = ((Literal)e.Item.FindControl("LiName")).Text.Trim();

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["Hid_assets_id"] + "').val('" + a_id + "');self.parent.$('#" + Request.QueryString["car_license"] + "').val('" + car_license + "');  parent.$.fancybox.close();</script>", false);

            return;

        }
       
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }


    
}