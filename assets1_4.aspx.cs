﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets1_4 : System.Web.UI.Page
{
    public string ssmanager_type
    {
        get { return ViewState["ssmanager_type"].ToString(); }
        set { ViewState["ssmanager_type"] = value; }
    }

    public string sssupplier_code
    {
        get { return ViewState["sssupplier_code"].ToString(); }
        set { ViewState["sssupplier_code"] = value; }
    }

    public int ssAccount_id
    {
        get { return (int)ViewState["ssAccount_id"]; }
        set { ViewState["ssAccount_id"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            ssmanager_type = Session["manager_type"].ToString(); //管理單位類別   
            sssupplier_code = Session["master_code"].ToString();
            sssupplier_code = (sssupplier_code.Length >= 3) ? sssupplier_code.Substring(0, 3) : "";
            if (sssupplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (ssmanager_type)
                {
                    case "1":
                        sssupplier_code = "";
                        break;
                    default:
                        sssupplier_code = "000";
                        break;
                }
            }


            SetDefaultMagType();
            if (Request.QueryString["manager_type"] != null)
            {
                rbl_typeForSh.SelectedValue = Request.QueryString["manager_type"];

            }
            readdata();

        }
    }

    protected void SetDefaultMagType()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            String wherestr = "";
            switch (ssmanager_type)
            {
                case "1":
                    wherestr = " and code_id in(1,2,3,4,5) ";
                    break;
                case "2":
                    wherestr = " and code_id in(-1) ";
                    break;
                case "3":
                    wherestr = " and code_id in(-1) ";
                    break;
                case "4":
                    wherestr = " and code_id in(5) ";
                    break;
                case "5":
                    wherestr = " and code_id in(-1) ";
                    break;
            }
            String strSQL = string.Format(@" SELECT code_id 'id',code_name 'name'
                                 FROM tbItemCodes With(Nolock)
                                WHERE code_sclass ='MT' AND active_flag=1 {0}", wherestr);
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                rbl_typeForSh.Items.Clear();
                rbl_typeForSh.DataSource = dt;
                rbl_typeForSh.DataValueField = "id";
                rbl_typeForSh.DataTextField = "name";
                rbl_typeForSh.DataBind();
                rbl_typeForSh.SelectedIndex = 0;
            }
        }

    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();
        #region 關鍵字
        if (rbl_typeForSh.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@manager_type", rbl_typeForSh.SelectedValue.ToString());
            strWhereCmd += " and manager_type = @manager_type";
            querystring += "&manager_type=" + rbl_typeForSh.SelectedValue;
        }
        if (sssupplier_code != "")
        {
            cmd.Parameters.AddWithValue("@head_code", sssupplier_code);
            strWhereCmd += " and head_code = @head_code";
        }


        #endregion

        //cmd.CommandText = string.Format(@"select A.* , B.customer_shortname, CASE A.active_flag WHEN 0 THEN '停用' ELSE '啟用' END AS statustext
        //                                  from tbAccounts A With(Nolock) 
        //                                  left join tbCustomers B With(Nolock) on A.master_code = B.master_code
        //                                  where 0 = 0   {0} order by account_code", strWhereCmd);

        cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                            select A.* , B.customer_shortname, CASE A.active_flag WHEN 0 THEN '停用' ELSE '啟用' END AS statustext,
                                            ROW_NUMBER() OVER (PARTITION BY A.master_code ORDER BY A.master_code DESC) AS rn
                                            from tbAccounts A With(Nolock) 
                                            left join tbCustomers B With(Nolock) on A.master_code = B.master_code
                                            where 0 = 0 and A.active_flag=1 {0} 
                                            )
                                            SELECT *
                                            FROM cus
                                            WHERE rn = 1 ", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }
    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }
}
