﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


public partial class LTsystem_4_4 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            //DDLSet();
            DefaultData();
        }
    }

    //protected void DDLSet()
    //{
    //    string manager_type = Session["manager_type"].ToString(); //管理單位類別
    //    string account_code = Session["account_code"].ToString(); //使用者帳號
    //    string supplier_code = Session["master_code"].ToString();

    //}


    protected void DefaultData()
    {

        String strSQL = string.Empty;
        string strWhere = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }


            #endregion


            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY A.station_code) AS num
                             ,id,station_code,station_scode,station_name, BusinessDistrict
                             ,D.user_name , A.udate,
							 CASE station_type 
WHEN 'A' THEN '直營' 
WHEN 'B' THEN '加盟' 
WHEN 'C' THEN '聯營' 
ELSE '' END station_type,
 CASE is_new_station 
WHEN '1' THEN '是' 
ELSE '否' END is_new_station,
tel
                        FROM tbStation A With(Nolock)
                        LEFT JOIN tbAccounts  D With(Nolock) on D.account_code = A.uuser
                        WHERE 1=1

";

            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND (A.station_code LIKE N'%'+@search+'%' OR A.station_name LIKE N'%'+@search+'%' )";
                cmd.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }


            #endregion

            cmd.CommandText = string.Format(strSQL + " {0} order by A.station_code", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_station.DataSource = pagedData;
                list_station.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "";

        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/LTsystem_4_4.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("Hid_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢示(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "站所資料-新增";
                pan_edit.Visible = true;
                pan_view.Visible = false;
                station_code.Enabled = true;
                station_scode.Enabled = true;
                lbl_id.Text = "0";

                station_code.Text =
                station_scode.Text =
                station_name.Text = "";
                BusinessDistrict.Text = "";
                lbl_app_login_date.Visible = false;
                station_type.SelectedValue = "";
             
                SDStart.Text = "";
                SDEnd.Text = "";
                MDStart.Text = "";
                MDEnd.Text = "";
                tel.Text = "";

                #endregion

                break;
            case 2:
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "站所資料-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @"SELECT id,station_code,station_scode,station_name ,BusinessDistrict, sd_range_start, sd_range_end, md_range_start,md_range_end,station_type,is_new_station,sd_range_start,sd_range_end,md_range_start,md_range_end,tel
                                          FROM tbStation  With(Nolock)
                                          WHERE id=@id";

                        cmd.Parameters.AddWithValue("@id", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                station_code.Text = row["station_code"].ToString();
                                station_scode.Text = row["station_scode"].ToString();
                                station_name.Text = row["station_name"].ToString();
                                BusinessDistrict.Text = row["BusinessDistrict"].ToString();
                                SDStart.Text = row["sd_range_start"].ToString();
                                SDEnd.Text = row["sd_range_end"].ToString();
                                MDStart.Text = row["md_range_start"].ToString();
                                MDEnd.Text = row["md_range_end"].ToString();
                                tel.Text = row["tel"].ToString();
                                lbl_id.Text = id.ToString();
                                station_type.SelectedValue = row["station_type"].ToString();
                              

                            }
                        }
                    }

                    station_code.Enabled = false;
                    station_scode.Enabled = false;
                    pan_edit.Visible = true;
                    pan_view.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "站所基本資料";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;
        Boolean IsOk = true;
        int i_id = 0;

        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;

        #region 驗證        

        //站所代碼不可重覆

        using (SqlCommand cmd = new SqlCommand())
        {
            string strSQL = string.Empty;
            strSQL = "SELECT COUNT(1) FROM  tbStation With(Nolock) WHERE  station_code =@station_code";
            if (IsEdit) strSQL += " AND id NOT IN('" + i_id.ToString() + "') ";
            cmd.Parameters.AddWithValue("@station_code", station_code.Text);
            cmd.CommandText = strSQL;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0)
                    {
                        IsOk = false;
                        RetuenMsg("站所代碼不可重覆,請重新確認!");
                    }
                }
            }
        }

        if (!IsOk) return;

        #endregion

        #region 存檔

        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_code"] != null) strUser = Session["account_code"].ToString();
            string str_table = "tbStation";
            using (SqlCommand cmd = new SqlCommand())
            {

                if (station_name.Text.Trim().Length > 0)
                {
                    cmd.Parameters.AddWithValue("@station_name", station_name.Text.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@station_name", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@BusinessDistrict", BusinessDistrict.Text.ToString());

              
                cmd.Parameters.AddWithValue("@station_type", station_type.SelectedValue.ToString());
               
                cmd.Parameters.AddWithValue("@tel", tel.Text.ToString());;

                if (SDStart.Text.ToString() == "" ^ SDEnd.Text.ToString() == "")
                {
                    RetuenMsg("請選擇完整SD區間");
                    return;
                }
                else if (MDStart.Text.ToString() == "" ^ MDEnd.Text.ToString() == "")
                {
                    RetuenMsg("請選擇完整MD區間");
                    return;
                }

                int test = 0;
                if (SDStart.Text.ToString() != "" & !int.TryParse(SDStart.Text.ToString(), out test))
                {
                    RetuenMsg("SD區間只可輸入數字");
                    return;
                }
                else if (SDEnd.Text.ToString() != "" & !int.TryParse(SDEnd.Text.ToString(), out test))
                {
                    RetuenMsg("SD區間只可輸入數字");
                    return;

                }
                else if (MDStart.Text.ToString() != "" & !int.TryParse(MDStart.Text.ToString(), out test))
                {
                    RetuenMsg("MD區間只可輸入數字");
                    return;

                }
                else if (MDEnd.Text.ToString() != "" & !int.TryParse(SDEnd.Text.ToString(), out test))
                {
                    RetuenMsg("MD區間只可輸入數字");
                    return;
                }
               
                int intSDStart = int.Parse(SDStart.Text.ToString());
                int intSDEnd = int.Parse(SDEnd.Text.ToString());
                int intMDStart = int.Parse(MDStart.Text.ToString());
                int intMDEnd = int.Parse(MDEnd.Text.ToString());

                if (intSDStart < 0 || intSDEnd < 0 || intMDStart < 0 || intMDEnd < 0)
                {
                    RetuenMsg("MD、SD區間禁止負數");
                    return;
                }
                else if (intSDEnd < intSDStart || intMDEnd < intMDStart)
                {
                    RetuenMsg("MD、SD區間錯誤");
                    return;
                }
                //else if (intSDStart >= intMDStart & intMDEnd >= intSDStart)
                //{
                //    RetuenMsg("MD、SD區間不可重疊");
                //    return;
                //}
                //else if (intMDStart >= intSDStart & intSDEnd >= intMDStart)
                //{
                //    RetuenMsg("MD、SD區間不可重疊");
                //    return;
                //}


                cmd.Parameters.AddWithValue("@sd_range_start", SDStart.Text.ToString());
                cmd.Parameters.AddWithValue("@sd_range_end", SDEnd.Text.ToString());
                cmd.Parameters.AddWithValue("@md_range_start", MDStart.Text.ToString());
                cmd.Parameters.AddWithValue("@md_range_end", MDEnd.Text.ToString());


                if (IsEdit)
                {
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm(str_table, cmd);
                    dbAdapter.execNonQuery(cmd);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@station_code", station_code.Text.ToString());
                    cmd.Parameters.AddWithValue("@station_scode", station_scode.Text.ToString());
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.CommandText = dbAdapter.genInsertComm(str_table, true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                }



                SetEditOrAdd(0);
                RetuenMsg(IsEdit ? "修改成功!" : "新增成功!");
            }
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }

        return result;
    }

    protected String GetValueStrFromSQL(string strSQL)
    {
        String result = "";
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0][0].ToString();
                    }
                }
            }

        }

        return result;
    }

    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }
    }


}
