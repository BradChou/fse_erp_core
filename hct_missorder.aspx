﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTrace.master" AutoEventWireup="true" CodeFile="hct_missorder.aspx.cs" Inherits="hct_missorder" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                defaultDate: (new Date())  //預設當日
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".datepicker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    defaultDate: (new Date())  //預設當日
                });

                jQuery(function ($) {
                    var pageParts = $(".paginate");
                    var numPages = pageParts.length;
                    var perPage = 10;
                    pageParts.slice(perPage).hide();
                    $("#page-nav").pagination({
                        items: numPages,
                        itemsOnPage: perPage,
                        cssStyle: "light-theme",
                        prevText: '上一頁',
                        nextText: '下一頁',
                        onPageClick: function (pageNum) {
                            var start = perPage * (pageNum - 1);
                            var end = start + perPage;

                            pageParts.hide()
                                     .slice(start, end).show();
                        }
                    });
                });
            }
        });
        $.fancybox.update();

        


    </script>
   
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">竹運單據補單作業</h2>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div id="div_search" runat="server">
                        <div class="templatemo-login-form">
                            <div class="form-group form-inline">
                                <label>掃單時間</label>
                                <asp:TextBox ID="tb_dateS" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD"></asp:TextBox>
                                ~
                                <asp:TextBox ID="tb_dateE" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD"></asp:TextBox>
                                <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" ValidationGroup="validate" />
                                <asp:Button ID="btnExport" CssClass="templatemo-blue-button" runat="server" Text="匯出" OnClick="btnExport_Click" />
                            </div>
                            <div class="form-group form-inline">
                                <label>貨號</label>
                                <asp:TextBox ID="check_number" CssClass="form-control" runat="server" MaxLength="10"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="divAdd" runat="server" visible="false" class="templatemo-login-form panel panel-default">
                        <div class="panel-heading">
                            <h2 class="text-uppercase">單筆補單</h2>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <div class="margin-right-15 templatemo-inline-block _div_type">
                                        <span class="_title _tip_important">託運類別</span>
                                        <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged" CssClass="radio radio-success">
                                            <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                            <asp:ListItem Value="02">02 論件</asp:ListItem>
                                            <asp:ListItem Value="03">03 論才</asp:ListItem>
                                            <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label>出貨日期：</label><asp:TextBox ID="Shipments_date" runat="server" class="form-control" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">貨　　號</span>
                                    <%--<asp:DropDownList runat="server" ID="ddl_check_number2" class="form-control ddl_check_number2"></asp:DropDownList>--%>
                                    <asp:TextBox ID="add_check_number" runat="server" Enabled ="false"  ></asp:TextBox>
                                    <asp:Label ID="lbErr_chknum" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">託運類別</span>
                                    <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                    </asp:DropDownList>
                                    <span class="_title _tip_important">傳票類別</span>
                                    <asp:DropDownList ID="subpoena_category" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">收件人</span>

                                    <asp:TextBox ID="receive_contact" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                    <span class="_title">客戶代號</span>
                                    <asp:DropDownList ID="dlcustomer_code" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">收件地址</span>
                                    <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:TextBox ID="receive_address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                    <asp:DropDownList ID="area_arrive_code" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">寄件人</span>
                                    <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">區配商代碼</span>
                                    <asp:TextBox ID="supplier_code" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">寄件地址</span>
                                    <asp:DropDownList ID="send_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="send_area" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:TextBox ID="send_address" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title"></span>
                                    <asp:CheckBox ID="receipt_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="回單" />
                                    <asp:CheckBox ID="pallet_recycling_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="棧板回收" />
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">貨物數量</span>
                                    <asp:TextBox ID="pieces" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    件數
                                <asp:TextBox ID="plates" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    板
                                <asp:TextBox ID="cbm" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    才
                                <asp:Label ID="lbErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">備　　註</span>
                                    <asp:TextBox ID="invoice_desc" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">費　　用</span>
                                    <label>代收金</label>
                                    <asp:TextBox ID="collection_money" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    &nbsp;
                                <label>到付運費</label>
                                    <asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    &nbsp;
                                <label>到付追加</label>
                                    <asp:TextBox ID="arrive_to_pay_append" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                </div>
                            </div>
                            <div class="btn_session">
                                <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="新增" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCanle" CssClass="btn templatemo-white-button" runat="server" Text="取消" OnClick="btnCanle_Click" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div id="div_list" runat="server">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>NO.</th>
                                <th>貨號</th>
                                <th>作業時間</th>
                                <th>作業</th>
                                <th>站所</th>
                                <th>到著/發送站</th>
                                <th>件數</th>
                                <th>班次代號</th>
                                <th>狀態</th>
                                <th>運輸方式</th>
                                <th>註區</th>
                                <th>員工</th>
                                <th></th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr class="paginate">
                                        <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                                        <td data-th="貨號">
                                            <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label></td>
                                        <td data-th="作業時間">
                                            <asp:Label ID="lbscan_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%>'></asp:Label></td>
                                        <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                                        <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                        <td data-th="到著/發送站"></td>
                                        <td data-th="件數">1</td>
                                        <td data-th="班次代號"></td>
                                        <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status").ToString())%></td>
                                        <td data-th="運輸方式"></td>
                                        <td data-th="註區"></td>
                                        <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>  <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                                        <td>
                                            <asp:HiddenField ID="Hid_logid" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.log_id").ToString())%>' />
                                            <asp:Button ID="btnAdd" CssClass="btn btn-warning " CommandName="cmdAdd" runat="server" Text="補單" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="11" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
                        <div id="page-nav" class="page"></div>


                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <span class="REDWD_b text-right ">*補單範圍只限半年內的單據</span>


        </div>
    </div>
</asp:Content>

