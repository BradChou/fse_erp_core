﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets2_2.aspx.cs" Inherits="assets2_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
            });

            init();
            
        });

        function init() {
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: -7, temp
                defaultDate: (new Date())  //預設當日11
            });
        }

    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">整批匯入</h2>
            <div class="templatemo-login-form" >
                <!---->
                <div class="form-style-10">
                    <div class="section"><span>1</span>下載空白費用總表</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btndownload" class="templatemo-white-button" runat="server" Text="空白費用總表" OnClick="btndownload_Click" />
                        <p class="text-primary">※第一次使用請下載空白費用總表標準格式</p>
                    </div>

                    <div class="section"><span>2</span>上傳檔案</div>
                    <div class="inner-wrap ">
                        <table style="margin-bottom: 5px;">
                            <tr>
                                <td><asp:Label ID="Label3" runat="server" Text="請選擇要匯入的檔案(.xls或.xlsx)"></asp:Label></td>
                                <td><asp:FileUpload ID="file01" runat="server" Width="300px" /></td>
                                <td><asp:Button ID="btImport" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate"  OnClick="btImport_Click" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="file01" ErrorMessage="請選擇檔案" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator1"
                                         ForeColor="Red"
                                        runat="server"
                                        ErrorMessage="格式錯誤!請選擇(.xls或.xlsx)檔案"
                                        ValidationExpression="^.+(.xls|.xlsx)$"
                                        ControlToValidate="file01" ValidationGroup="validate" Display="Dynamic"> </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <div style="overflow: auto; height: 200px; vertical-align: top;">
                                <asp:ListBox ID="lbMsg" runat="server" Height="200px" Width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="section"><span>3</span></div>
                    <div class="inner-wrap" style="overflow: auto; height: calc(40vh); width: 100%" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <p>※今日總筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>
                        <table id="custom_table"  class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>費用類別</th> 
                                <th>車牌號碼</th>
                                <th>交易日期</th>
                                <th>油品公司</th>
                                <th>公司名稱</th>
                                <th>項目</th>
                                <th>明細</th>
                                <th>數量</th>
                                <th>里程數</th>
                                <th>公升數</th>
                                <th>牌價</th>
                                <th>單價</th>                                
                                <th>備註</th>
                            </tr>
                            <asp:repeater id="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="費用類別"><%#Func.GetRow("code_name","tbItemCodes","","code_bclass = '6' and code_sclass = 'fee_type' and code_id = @code_id","code_id",Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.fee_type").ToString()))%></td>
                                        <td data-th="車牌號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%></td>
                                        <td data-th="交易日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.date","{0:yyyy-MM-dd HH:mm:ss}").ToString())%></td>
                                        <td data-th="油品公司"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.oil").ToString())%></td>
                                        <td data-th="公司名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.company").ToString())%></td>
                                        <td data-th="項目"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.items").ToString())%></td>
                                        <td data-th="明細"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.detail").ToString())%></td>
                                        <td data-th="數量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                                        <td data-th="里程數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.milage").ToString())%></td>
                                        <td data-th="公升數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.litre").ToString())%></td>
                                        <td data-th="牌價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price").ToString())%></td>
                                        <td data-th="單價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.buy_price").ToString())%></td>
                                        <td data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="13" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
           <%-- <hr>--%>
        </div>
    </div>
</asp:Content>

