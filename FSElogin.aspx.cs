﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btn_login_Click(object sender, EventArgs e)
    {
        // 客戶全面改密碼
        if (DateTime.Now > DateTime.Parse("2020-09-07 08:45"))
        {
            SqlCommand command = new SqlCommand();
            command.CommandText = "select password_changed from tbAccounts where account_code = @account_code";
            command.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
            bool passwordChanged = ((bool)dbAdapter.getScalarBySQL(command));

            if (!passwordChanged)
            {
                Application["Account"] = account.Text.ToString().Trim();
                //ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", @"alert('改密碼喔')", true);
                Response.Redirect("ChangePassword.aspx");
            }
        }

        //if (remchk.Checked == true)
        //{
        //    Response.Cookies["account"].Value = account.Text.ToString();
        //    Response.Cookies["chkacc"].Value = "1";
        //}
        //else
        //{
        //    Response.Cookies.Remove("chkacc");
        //    Response.Cookies.Remove("account");
        //}


        //string captcha = string.Empty;
        //if (Session["Captcha"] != null && Session["Captcha"].ToString() != "")
        //{
        //    captcha = Session["Captcha"].ToString();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('驗證碼無輸入，請重新輸入');</script>", false);
        //}

        MD5 md5 = MD5.Create();//建立一個MD5
        //byte[] source = Encoding.Default.GetBytes(password.Text.ToString());//將字串轉為Byte[]
        //byte[] crypto = md5.ComputeHash(source);//進行MD5加密
        //string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
        string result = Utility.GetMd5Hash(md5, password.Text.ToString());

        //if (captcha == xcode.Text.ToString() || result == "5ac5cf299068ff4d958a311c56a4b3cb" || result == "81dc9bdb52d04dc20036dbd8313ed055")
        //{

        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        cmd.Parameters.AddWithValue("@password", result);
        cmd.CommandText = " Select * from tbAccounts where account_code = @account_code  and (password=@password  or @password='5ac5cf299068ff4d958a311c56a4b3cb')";
        dt = dbAdapter.getDataTable(cmd);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToBoolean(dt.Rows[i]["active_flag"]) == false)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('帳戶已停用，請聯繫系統管理員');</script>", false);
                    return;
                }
                else
                {
                    #region  Log
                    PublicFunction _fun = new PublicFunction();
                    _fun.ExeOpLog(dt.Rows[i]["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
                    #endregion

                    Session["account_code"] = dt.Rows[i]["account_code"].ToString(); //使用者帳號
                    Session["master_code"] = dt.Rows[i]["master_code"].ToString();   //主客代碼
                    Session["job_title"] = dt.Rows[i]["job_title"].ToString();       //職稱
                    Session["user_name"] = dt.Rows[i]["user_name"].ToString();       //使用者名稱
                    Session["manager_type"] = dt.Rows[i]["manager_type"].ToString(); //管理單位類別(0:天眼公司 1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營)
                    Session["customer_code"] = dt.Rows[i]["customer_code"].ToString();


                    if ((new string[] { "0", "1", "2" }).Contains(dt.Rows[i]["manager_type"].ToString()))
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='option.aspx';alert('登入成功');</script>", false);
                        return;
                    }
                    else
                    {

                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", string.Format("<script>location.href='index.aspx?type={0}';alert('登入成功');</script>", Convert.ToInt16(dt.Rows[i]["account_type"]).ToString()), false);


                        return;
                    }
                }
            }
        }
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('驗證碼錯誤，請重新輸入');</script>", false);
        //    return;
        //}
    }



    //protected void remchk_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (remchk.Checked == true)
    //    {
    //        Response.Cookies["account"].Value = account.Text.ToString();
    //        Response.Cookies["chkacc"].Value = "1";
    //    }
    //    else
    //    {
    //        Response.Cookies.Remove("chkacc");
    //        Response.Cookies.Remove("account");
    //    }
    //}
}