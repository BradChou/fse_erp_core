﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class report_1 : System.Web.UI.Page
{
    public string  Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0" ; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //String yy = DateTime.Now.Year.ToString();
            //String mm = DateTime.Now.Month.ToString();
            //String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();


            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;
            date2.Text = Distribute_date;

            string manager_type = Session["manager_type"].ToString(); //管理單位類別
            string supplier_code = Session["master_code"].ToString();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
            {
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000";
                            break;
                    }
                }

            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers with(nolock) 
                                               where 0=0 and active_flag =1 and ISNULL(supplier_code ,'') <> '' {0} order by supplier_code", wherestr);
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);

            if (manager_type == "0" || manager_type == "1" || manager_type == "2") Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion

            if (Request.QueryString["date1"] != null)
            {
                date1.Text = Request.QueryString["date1"];
            }

            if (Request.QueryString["date2"] != null)
            {
                date2.Text = Request.QueryString["date2"];
            }

            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue  = Request.QueryString["Suppliers"];
            }

            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            //if (Request.QueryString["rb_pricing_type"] != null)
            //{
            //    rb_pricing_type.SelectedValue = Request.QueryString["rb_pricing_type"];
            //}
            
            if (Request.QueryString["rb_pricing_type"] != null)
            {
                for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
                {
                    if (Request.QueryString["rb_pricing_type"].ToString().Contains(cb_pricing_type.Items[i].Value))
                    {
                        cb_pricing_type.Items[i].Selected = true;
                    }
                    else
                    {
                        cb_pricing_type.Items[i].Selected = false;
                    }
                }
            }

            if (Request.QueryString["isClickSearch"] != null)
            {
                readdata();
            }
        }
    }

    private void readdata()
    {
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "supererp_pallet_GetDistributionReport";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text );
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                //cmd.Parameters.AddWithValue("@wherestr", " AND supplier_code LIKE '%" + Suppliers.SelectedValue.ToString() + "%'" );
            }
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            }
            querystring += "&second_code=" + second_code.SelectedValue.ToString();


            if (date1.Text.ToString() != "")
            {
                querystring += "&date1=" + date1.Text.ToString();
            }
            
            if (date2.Text.ToString() != "")
            {
                querystring += "&date2=" + date2.Text.ToString();
            }
           
            //if (rb_pricing_type.SelectedValue == "專車")  //專車只抓專車
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "05");
            //}
            //else if (rb_pricing_type.SelectedValue == "論板、件、才、小板")  //論板抓：板、件、才、小板
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "01");
            //}
            //else if (rb_pricing_type.SelectedValue == "論件") //論件只抓論件
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "02");
            //}

            string pricing_type = "";
            for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
            {
                if (cb_pricing_type.Items[i].Selected)
                {
                    pricing_type  += "'"+ cb_pricing_type.Items[i].Value +"',";
                }
            }
            if (pricing_type != "") pricing_type = pricing_type.Substring(0, pricing_type.Length - 1);
            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);            
            
            querystring += "&rb_pricing_type=" + pricing_type;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                
                    SqlCommand cmds = new SqlCommand();
                    string wherestr = "";
                //if (rb_pricing_type.SelectedValue == "專車")
                //{
                //    cmds.Parameters.AddWithValue("@pricing_type", "05");
                //    wherestr += " and pricing_type = @pricing_type";
                //}
                //else if (rb_pricing_type.SelectedValue == "論板、件、才、小板")
                //{
                //    cmds.Parameters.AddWithValue("@pricing_type", "05");
                //    wherestr += " and pricing_type <> @pricing_type";
                //}
                //else if (rb_pricing_type.SelectedValue == "論件")
                //{
                //    cmds.Parameters.AddWithValue("@pricing_type", "02");
                //    wherestr += " and pricing_type = @pricing_type";
                //}

                pricing_type = "";
                for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
                {
                    if (cb_pricing_type.Items[i].Selected)
                    {
                        pricing_type += cb_pricing_type.Items[i].Value + ",";
                    }
                }
                if (pricing_type != "") pricing_type = pricing_type.Substring(0, pricing_type.Length - 1);


                wherestr += dbAdapter.genWhereCommIn(ref cmds, "pricing_type", pricing_type.Split(','));
                //cmds.Parameters.AddWithValue("@pricing_type", pricing_type);
                //wherestr += " and pricing_type = @pricing_type";

                cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
                                                  SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' FROM tcDeliveryRequests WITH (NOLOCK)
                                                  WHERE CONVERT(VARCHAR, print_date, 111) >= CONVERT(VARCHAR, @print_dates, 111) and CONVERT(VARCHAR, print_date, 111) <= CONVERT(VARCHAR, @print_datee, 111)
                                                  AND(supplier_code = @supplier_id OR @supplier_id IS NULL)
                                                  AND(customer_code like @customer_code+'%' OR @customer_code IS NULL)
                                                  AND supplier_date IS NOT NULL
                                                  AND cancel_date IS NULL {0}
                                                 )
                                                 SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                    cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                    cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                    if (Suppliers.SelectedValue != "")
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                    }

                    if (second_code.SelectedValue != "")
                    {
                        string customer_code = second_code.SelectedValue.Trim();
                        cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                    }

                    DataTable dtsum = dbAdapter.getDataTable(cmds);
                    ltotalpages.Text = dt.Rows.Count.ToString();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow row = dt.NewRow();
                        row["配送區域"] = "總計";
                        row["棧板數"] = dtsum.Rows[0]["棧板數"].ToString();
                        row["件數"] = dtsum.Rows[0]["件數"].ToString();
                        row["才數"] = dtsum.Rows[0]["才數"].ToString();

                        dt.Rows.Add(row);
                    }
                
                New_List.DataSource = dt;
                New_List.DataBind();

            }
            
        }
            
    }
    

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1 .Text.ToString();
        }

        if (date2.Text.ToString() != "")
        {
            querystring += "&date2=" + date2.Text.ToString();
        }
                
        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&second_code=" + second_code.SelectedValue.ToString();

        //querystring += "&rb_pricing_type=" + rb_pricing_type.SelectedValue.ToString();
        string pricing_type = "";
        for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
        {
            if (cb_pricing_type.Items[i].Selected)
            {
                pricing_type += cb_pricing_type.Items[i].Value + ",";
            }
        }
        querystring += "&rb_pricing_type=" + pricing_type;
        querystring += "&isClickSearch=Search";

        Response.Redirect(ResolveUrl("~/report_1.aspx?search=yes" + querystring));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void New_List_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{

        //    string receive_city =(DataBinder.GetPropertyValue(e.Item.DataItem, "receive_city")).ToString();
        //    string send_city = (DataBinder.GetPropertyValue(e.Item.DataItem, "send_city")).ToString();
        //    int arrive_to_pay_freight = Convert.ToInt32 ( DataBinder.GetPropertyValue(e.Item.DataItem, "arrive_to_pay_freight"));
        //    int arrive_to_pay_append = Convert.ToInt32(DataBinder.GetPropertyValue(e.Item.DataItem, "arrive_to_pay_append"));
        //    int collection_money = Convert.ToInt32( DataBinder.GetPropertyValue(e.Item.DataItem, "collection_money"));
        //    int plates = Convert.ToInt32(DataBinder.GetPropertyValue(e.Item.DataItem, "plates"));
        //    string  platesstr = (plates >6) ? "plate6_price" : "plate" + plates.ToString() +  "_price";


        //    string customer_code = DataBinder.GetPropertyValue(e.Item.DataItem, "customer_code").ToString();
        //    string customer_type = "1";
        //    SqlCommand cmd = new SqlCommand();
        //    DataTable dtmerge = new DataTable();
        //    cmd.CommandText = "select customer_type  from tbCustomers  where customer_code  ='" + customer_code + "'";
        //    dtmerge = dbAdapter.getDataTable(cmd);
        //    if (dtmerge.Rows.Count > 0)
        //    {
        //        customer_type = dtmerge.Rows[0]["customer_type"].ToString();
        //    }

        //    SqlCommand cmdt = new SqlCommand();
        //    DataTable dt;
        //    switch (customer_type)
        //    {
        //        case "1":  //區配論板
        //            cmdt.CommandText = string.Format(@"select ISNULL(,0) from tbPriceSupplier where start_city = '"+ send_city + "  and receive_city = '"+ receive_city + "' and  class_level =(
        //                                                SELECT TOP 1 class_level FROM ttPriceClassLog With(Nolock)where active_date <= CONVERT(DATETIME, '2017/04/26', 102)  ORDER BY active_date DESC
        //                                                )""
        //            break;
        //        case "2":  //竹運論板
        //            cmd1.CommandText = "select A.*, B.code_name , '0' as class_level from tbPriceHCT A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "3":  //自營論板
        //            cmd1.CommandText = "select A.*, B.code_name , '0' as class_level from tbPriceBusiness A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "4":  //C配
        //            cmd1.CommandText = "select A.*, B.code_name from tbPriceCSection A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //        case "5":  //合發專用
        //            cmd1.CommandText = "select A.*, B.code_name from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
        //            break;
        //    }

           
        //    cmdt.CommandText = "SELECT TOP 1 class_level  FROM ttPriceClassLog With(Nolock) where active_date <= CONVERT (DATETIME, '2017/04/11', 102)  ORDER BY active_date DESC";
        //    dt = dbAdapter.getDataTable(cmdt);
        //    if (dt.Rows.Count > 0)
        //    {
        //        if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
        //    }
        //    ((Label)e.Item.FindControl("statustext")).Text = DataBinder.GetPropertyValue(e.Item.DataItem, "statustext").ToString();
        //    if (DataBinder.GetPropertyValue(e.Item.DataItem, "Status").ToString() == "20")
        //    {
        //        ((Label)e.Item.FindControl("statustext")).ForeColor = System.Drawing.Color.Red;
        //    }

        //    string ttTime = DataBinder.GetPropertyValue(e.Item.DataItem, "goTime").ToString();
        //    if (ttTime.Length < 8)
        //    { ttTime += ":00"; }
        //    ((Literal)e.Item.FindControl("goTime")).Text = DateTime.Parse(ttTime).ToString("HH:mm");

        //}
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "發送總表";
        string file_name = "發送總表" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 9999;
            cmd.CommandText = "supererp_pallet_GetDistributionReport";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
            }

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            }


            //if (rb_pricing_type.SelectedValue == "專車")  //專車只抓專車
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "05");
            //}
            //else if (rb_pricing_type.SelectedValue == "論板、件、才、小板")  //論板抓：板、件、才、小板
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "01");
            //}
            //else if (rb_pricing_type.SelectedValue == "論件") //論件只抓論件
            //{
            //    cmd.Parameters.AddWithValue("@pricing_type", "02");
            //}

            string pricing_type = "";
            for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
            {
                if (cb_pricing_type.Items[i].Selected)
                {
                    pricing_type += "'" + cb_pricing_type.Items[i].Value + "',";
                }
            }
            if (pricing_type != "") pricing_type = pricing_type.Substring(0, pricing_type.Length - 1);
            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);

            cmd.CommandType = CommandType.StoredProcedure;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    
                        SqlCommand cmds = new SqlCommand();
                        string wherestr = "";
                        //if (rb_pricing_type.SelectedValue == "專車")
                        //{
                        //    cmds.Parameters.AddWithValue("@pricing_type", "05");
                        //    wherestr += " and pricing_type = @pricing_type";
                        //}
                        //else if (rb_pricing_type.SelectedValue == "論板、件、才、小板")
                        //{
                        //    cmds.Parameters.AddWithValue("@pricing_type", "05");
                        //    wherestr += " and pricing_type <> @pricing_type";
                        //}
                        //else if (rb_pricing_type.SelectedValue == "論件")
                        //{
                        //    cmds.Parameters.AddWithValue("@pricing_type", "02");
                        //    wherestr += " and pricing_type = @pricing_type";
                        //}

                        pricing_type = "";
                        for (int i = 0; i <= cb_pricing_type.Items.Count - 1; i++)
                        {
                            if (cb_pricing_type.Items[i].Selected)
                            {
                                pricing_type += cb_pricing_type.Items[i].Value + ",";
                            }
                        }

                        if (pricing_type != "") pricing_type = pricing_type.Substring(0, pricing_type.Length - 1);
                    wherestr += dbAdapter.genWhereCommIn(ref cmds, "pricing_type", pricing_type.Split(','));

                    cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
                                                  SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' FROM tcDeliveryRequests WITH (NOLOCK)
                                                  WHERE CONVERT(VARCHAR, print_date, 111) >= CONVERT(VARCHAR, @print_dates, 111) and CONVERT(VARCHAR, print_date, 111) <= CONVERT(VARCHAR, @print_datee, 111)
                                                  AND(supplier_code = @supplier_id OR @supplier_id IS NULL)
                                                  AND(customer_code like @customer_code+'%' OR @customer_code IS NULL)
                                                  AND supplier_date IS NOT NULL
                                                  AND cancel_date IS NULL {0}
                                                 )
                                                 SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr);

                        cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                        cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                        if (Suppliers.SelectedValue != "")
                        {
                            cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());

                        }
                        else
                        {
                            cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value);
                        }

                        if (second_code.SelectedValue != "")
                        {
                            string customer_code = second_code.SelectedValue.Trim();
                            cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                        }
                        else
                        {
                            cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                        }


                        DataTable dts = dbAdapter.getDataTable(cmds);
                        if (dts != null && dts.Rows.Count > 0)
                        {
                            DataRow row = dt.NewRow();
                            row["配送區域"] = "總計";
                            row["棧板數"] = dts.Rows[0]["棧板數"].ToString();
                            row["件數"] = dts.Rows[0]["件數"].ToString();
                            row["才數"] = dts.Rows[0]["才數"].ToString();

                            dt.Rows.Add(row);
                        }
                    
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }
                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }
               
        }
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text != "")
            {
                cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                wherestr = "  and supplier_code=@Suppliers";
            }

            string Master_code = Session["master_code"].ToString();
            string manager_type = Session["manager_type"].ToString(); //管理單位類別
            
            if (Master_code != "")
            {
                cmd.Parameters.AddWithValue("@Master_code", (manager_type == "3" || manager_type == "5") ? Master_code : "");
                wherestr += "  and customer_code like ''+ @Master_code + '%'";
            }

            cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY /*master_code DESC*/ customer_id asc) AS rn
                                               FROM tbCustomers with(nolock) where 1=1 and supplier_code <> '001' and stop_shipping_code = '0'  {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 ", wherestr);
            second_code.DataSource = dbAdapter.getDataTable(cmd);
            second_code.DataValueField = "master_code";
            second_code.DataTextField = "showname";
            second_code.DataBind();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2")
            {
                second_code.Items.Insert(0, new ListItem("全部", ""));
            }
        }
        #endregion

        //if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        //{
        //    UpdatePanel1.Update();
        //}
    }
}