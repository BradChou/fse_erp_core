﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class money_2_6 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = DateTime.Today.AddMonths(-1).ToString("yyyy/MM/") +"01"; //yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            
            #region 
            Suppliers.DataSource = Utility.getSupplierDT2(supplier_code, manager_type);
            Suppliers.DataValueField = "supplier_no";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            
            //if (Request.QueryString["Suppliers"] != null)
            //{
            //    Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            //}
            Suppliers_SelectedIndexChanged(null, null);
            //Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion
            
        }
            
    }

    private void readdata()
    {
        lbSuppliers.Text = Suppliers.SelectedItem.Text;
        lbdate.Text = dates.Text + "~" + datee.Text;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable_S";
            cmd.CommandType = CommandType.StoredProcedure;
            string customer_code = Suppliers.SelectedValue ;
            cmd.Parameters.AddWithValue("@supplier", customer_code);  //供應商
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable("E", dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.SelectedValue);
                if (dt != null && dt.Rows.Count > 0 && _info != null)
                {
                    subtotal_01.Text = _info.subtotal.ToString("N0");
                    tax_01.Text = _info.tax.ToString("N0");
                    total_01.Text = _info.total.ToString("N0");
                }

                New_List_01.DataSource = dt;
                New_List_01.DataBind();

            }
        }


        //try
        //{
        //    using (SqlCommand cmd = new SqlCommand())
        //    {   
        //        cmd.CommandText = "usp_GetSumAccountsReceivable";
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue);  //客代
        //        cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
        //        cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
        //        cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
        //        using (DataTable dt = dbAdapter.getDataTable(cmd))
        //        {
        //            if (dt.Rows.Count > 0)
        //            {
        //                New_List.DataSource = dt;
        //                New_List.DataBind();
        //            }

        //        }
        //    }
        //}
        //catch (Exception ex)
        //{

        //    string strErr = string.Empty;
        //    strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

        //    PublicFunction _fun = new PublicFunction();
        //    _fun.Log(strErr, "S");
        //}
    }
    
    protected void dlclose_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        if (dlclose_type.SelectedValue == "1")
        {
            lbdaterange.InnerText = "請款期間";
            dlCloseDay.Visible = true;
            lbCloseDay.Visible = true;

            if (Suppliers.SelectedValue != "")
            {
                string wherestr = "";
                SqlCommand cmd2 = new SqlCommand();
                string supplier = Suppliers.SelectedValue;
                if (supplier != "")
                {
                    cmd2.Parameters.AddWithValue("@supplier", supplier);
                    wherestr += " and supplier_code = @supplier ";
                }

                cmd2.Parameters.AddWithValue("@dates", dates.Text + " 00:00:00.000");  //日期起
                cmd2.Parameters.AddWithValue("@datee", datee.Text + " 23:59:59.997");  //日期迄
                cmd2.CommandText = string.Format(@"select CONVERT(varchar(100),close_enddate, 111) as checkout_close_date  from ttCheckoutCloseLog with(nolock) where type = 1 and src = 'E'
                                                   AND  close_enddate  >= CONVERT (DATETIME, @dates, 102) AND close_enddate  <= CONVERT (DATETIME, @datee, 102)   
                                                   {0} order by checkout_close_date", wherestr);
                dlCloseDay.DataSource = dbAdapter.getDataTable(cmd2);
                dlCloseDay.DataValueField = "checkout_close_date";
                dlCloseDay.DataTextField = "checkout_close_date";
                dlCloseDay.DataBind();
                dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));
            }
            else
            {
                dlCloseDay.Items.Clear();
                dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));
            }
        }
        else
        {
            lbdaterange.InnerText = "發送期間";
            lbCloseDay.Visible = false;
            dlCloseDay.Visible = false;
        }
        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
        #endregion
    }

    protected void btQry_Click(object sender, EventArgs e)
    {
        readdata();
    }

   
    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {

        dlclose_type_SelectedIndexChanged(null, null);
        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        if (Suppliers.SelectedValue == "" )
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇供應商');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post_pay";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue );   //供應商
                cmd.Parameters.AddWithValue("@close_type", "2");                      //結帳程序類別(1:自動 2:手動)
                cmd.Parameters.AddWithValue("@close_src", "E");                       //結帳內容(A：A段派遣  B:B段派遣 C:C段 D:專車 E:超商)
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean (dt.Rows[0]["result"].ToString()) == false )
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            strErr = "acc_post" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            readdata();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }

        return;
    }
    
    [WebMethod]
    public static string GetCloseDayDDLHtml(string dates, string datee,  string supplier_code )
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "" )
            {
                cmd.Parameters.AddWithValue("@supplier", supplier_code);
                wherestr = " and supplier like  @supplier+'%' ";
                cmd.Parameters.AddWithValue("@dates", dates  + " 00:00:00.000");  //日期起
                cmd.Parameters.AddWithValue("@datee", datee  + " 23:59:59.997");  //日期迄
                cmd.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from ttAssetsSB with(nolock)   
                                                   where 0=0 AND  checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    //sb_html.Append(@"<option  value=''>請選擇</option>");

                    foreach (DataRow row in dt.Rows)
                    {
                        sb_html.Append(@"<option value='" + row["checkout_close_date"].ToString() + "'>" + row["checkout_close_date"].ToString() + "</option>");
                    }

                }
            }
            
        }

        return sb_html.ToString();
    }



    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = null;
        string Title = Suppliers.SelectedItem.Text;
        //string FeeColName = "";

        Title = "超商應付帳款明細表";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable_S";
            cmd.CommandType = CommandType.StoredProcedure;
            string customer_code = Suppliers.SelectedValue;
            cmd.Parameters.AddWithValue("@supplier", customer_code);  //供應商
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            dt = dbAdapter.getDataTable(cmd);
            _info = Utility.GetSumAccountsPayable("E",dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.SelectedValue);
           
        }

        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("超商應付");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 30;
            sheet1.Column(6).Width = 20;
            sheet1.Column(7).Width = 10;
            sheet1.Column(8).Width = 10;
            sheet1.Column(9).Width = 10;
            sheet1.Column(10).Width = 10;
            sheet1.Column(11).Width = 10;
            sheet1.Column(12).Width = 10;
            sheet1.Column(13).Width = 10;



            sheet1.Cells[1, 1, 1, 13].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 13].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 13].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 13].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 13].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 13].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 13].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 13].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 2, 3, 13].Merge = true;     //合併儲存格
            sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
            sheet1.Cells[3, 2, 3, 2].Value = "供應商：" + Suppliers.SelectedItem.Text;
            sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            sheet1.Cells[4, 1, 4, 13].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 13].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 1, 4, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 2].Value = "日期";
            sheet1.Cells[4, 3].Value = "日/夜";
            sheet1.Cells[4, 4].Value = "溫層";
            sheet1.Cells[4, 5].Value = "車老闆";
            sheet1.Cells[4, 6].Value = "運務士";
            sheet1.Cells[4, 7].Value = "車號";
            sheet1.Cells[4, 8].Value = "車型";
            sheet1.Cells[4, 9].Value = "主線";
            sheet1.Cells[4, 10].Value = "爆量";
            sheet1.Cells[4, 11].Value = "店到店";
            sheet1.Cells[4, 12].Value = "回頭車";
            sheet1.Cells[4, 13].Value = "加派車";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 5, 2].Value = dt.Rows[i]["InDate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["InDate"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 3].Value = dt.Rows[i]["DayNight"].ToString();
                sheet1.Cells[i + 5, 4].Value = dt.Rows[i]["Temperature_Text"].ToString();
                sheet1.Cells[i + 5, 5].Value = dt.Rows[i]["PaymentObject"].ToString();
                sheet1.Cells[i + 5, 6].Value = dt.Rows[i]["driver"].ToString();
                sheet1.Cells[i + 5, 7].Value = dt.Rows[i]["car_license"].ToString();
                sheet1.Cells[i + 5, 8].Value = dt.Rows[i]["cabin_type_text"].ToString();
                sheet1.Cells[i + 5, 9].Value = Convert.ToInt32(dt.Rows[i]["expenses_01"]);
                sheet1.Cells[i + 5, 9].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 10].Value = Convert.ToInt32(dt.Rows[i]["expenses_02"]);
                sheet1.Cells[i + 5, 10].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 11].Value = Convert.ToInt32(dt.Rows[i]["expenses_03"]);
                sheet1.Cells[i + 5, 11].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 12].Value = Convert.ToInt32(dt.Rows[i]["expenses_04"]);
                sheet1.Cells[i + 5, 12].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 13].Value = Convert.ToInt32(dt.Rows[i]["expenses_05"]);
                sheet1.Cells[i + 5, 13].Style.Numberformat.Format = "#,##0";

                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    sheet1.Cells[i + 6, 12].Value = "小計";
                    sheet1.Cells[i + 6, 13].Value = _info.subtotal;
                    sheet1.Cells[i + 6, 13].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 6, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 6, 1, i + 6, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 6, 1, i + 6, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                    sheet1.Cells[i + 7, 12].Value = "5%營業稅";
                    sheet1.Cells[i + 7, 13].Value = _info.tax;
                    sheet1.Cells[i + 7, 13].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 7, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 7, 1, i + 7, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 7, 1, i + 7, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[i + 8, 12].Value = "應收帳款";
                    sheet1.Cells[i + 8, 13].Value = _info.total;
                    sheet1.Cells[i + 8, 13].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 8, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 8, 1, i + 8, 13].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 8, 1, i + 8, 13].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 13].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                }
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("超商應付帳款明細表.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    protected void close_date_SelectedIndexChanged(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }
    
    
    protected void dl_pricing_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        dlclose_type_SelectedIndexChanged(null, null);
    }

    protected void close_date_SelectedIndexChanged1(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }
}