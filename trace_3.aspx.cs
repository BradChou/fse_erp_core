﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class trace_3 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }
    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = Session["master_code"].ToString();
            master_code = Session["master_code"].ToString();

            if (Less_than_truckload == "0")
            {
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000";
                            break;
                    }
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    string wherestr = "";
                    if (supplier_code != "")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and supplier_code = @supplier_code";
                    }
                    if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                    {
                        cmd2.Parameters.AddWithValue("@master_code", master_code);
                        wherestr += " and customer_code like @master_code+'%' ";
                    }


                    cmd2.CommandText = string.Format(@"Select customer_code , customer_code + '-' + customer_shortname as name  from tbCustomers with(Nolock) where 0=0 and stop_shipping_code = '0'  {0}  order by customer_code asc ", wherestr);
                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "name";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
                }

            }
            else
            {
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                        break;
                }

                using (SqlCommand cmd2 = new SqlCommand())
                {
                    string wherestr = "";
                    cmd2.Parameters.AddWithValue("@type", "1");
                    if (supplier_code != "")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and supplier_code = @supplier_code";
                    }
                    if (manager_type == "5")
                    {
                        cmd2.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                        wherestr += "  and customer_code like ''+ @customer_code + '%'";
                    }
                    cmd2.CommandText = string.Format(@"
WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers with(nolock) where 1=1 and supplier_code <> '001' and stop_shipping_code = '0'
and customer_code <> '0000207002'
                                               and type = @type
{0}
                                            )
 SELECT supplier_id, master_code, customer_name , master_code+ second_code+ '-' +  customer_name as showname,
master_code+ second_code as customer_code
                                            FROM cus
                                            order by  master_code", wherestr);
                    dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                    dlcustomer_code.DataValueField = "customer_code";
                    dlcustomer_code.DataTextField = "showname";
                    dlcustomer_code.DataBind();
                    dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
                }

            }
            

            


            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.Now.Day.ToString();  //DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            mm = mm.Length < 2 ? "0" + mm : mm;
            dates.Text = yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;



            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if (Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }

            //readdata();

        }
    }

    private void readdata()
    {
        string manager_type = Session["manager_type"].ToString(); //管理單位類別  
        string customer_code = Session["master_code"].ToString();

        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                customer_code = "";
                break;
            default:
                customer_code = customer_code.Substring(0, 3);
                break;
        }
        uc_PeidaReport1.readdata(dates.Text, datee.Text, Less_than_truckload, customer_code, dlcustomer_code.Text);
    }

    //private void readdata()
    //{
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.CommandTimeout = 9999;
    //        cmd.Parameters.Clear();
    //        #region 關鍵字
    //        cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

    //        if (dates.Text != "")
    //        {
    //            cmd.Parameters.AddWithValue("@dates", dates.Text);
    //        }

    //        if (datee.Text != "")
    //        {
    //            cmd.Parameters.AddWithValue("@datee", datee.Text);
    //        }
    //        string manager_type = Session["manager_type"].ToString(); //管理單位類別  
    //        string customer_code = Session["master_code"].ToString();

    //        switch (manager_type)
    //        {
    //            case "0":
    //            case "1":
    //            case "2":
    //                customer_code = "";
    //                break;
    //            default:
    //                customer_code = customer_code.Substring(0, 3);
    //                break;
    //        }
    //        cmd.Parameters.AddWithValue("@_supplier_code", customer_code);

    //        cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
    //        #endregion

    //        //cmd.CommandText = string.Format(@"DECLARE  @NO NVARCHAR(4) 
    //        //                            DECLARE  @CHKNUM NVARCHAR(10)  --貨號
    //        //                            DECLARE  @supplier_code NVARCHAR
    //        //                            DECLARE  @supplier_date DateTime 
    //        //                            DECLARE  @issupply INT 
    //        //                            DECLARE  @noon_arrive  INT
    //        //                            DECLARE  @day_arrive  INT
    //        //                            DECLARE  @first_time  NVARCHAR(10)
    //        //                            DECLARE  @last_time NVARCHAR(10) 
    //        //                            DECLARE  @OKsupply_date DateTime 
    //        //                            DECLARE  @OKarrive_date DateTime 
    //        //                            DECLARE  @should  INT

    //        //                            select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
    //        //                            A.check_number,
    //        //                            A.supplier_code ,
    //        //                            A.supplier_date ,				   --應配送日期
    //        //                            0 as issupply,					   --是否配送
    //        //                            0 as noon_arrive,				   --是否中午前配達(13:00前)
    //        //                            0 as day_arrive ,				   --當日配達(17:00前)
    //        //                            @OKarrive_date  as OKarrive_date,  --配達時間
    //        //                            1 as should
    //        //                            --'00:00:00' as first_time , --首筆配達時間
    //        //                            --'00:00:00' as last_time    --末筆配達時間
    //        //                            into #mylist	  
    //        //                            from tcDeliveryRequests  A with(nolock)
    //        //                            where  A.check_number <> ''
    //        //                            and CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@dates,111) AND CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@datee,111)
    //        //                            and A.customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
    //        //                            order by A.check_number 

    //        //                            DECLARE R_cursor CURSOR FOR
    //        //                            select * FROM #mylist
    //        //                            OPEN R_cursor
    //        //                            FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@supplier_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@OKarrive_date,@should
    //        //                            WHILE (@@FETCH_STATUS = 0) 
    //        //                             BEGIN	
    //        //                              select top 1  @OKsupply_date = scan_date from ttDeliveryScanLog  with (nolock)  
    //        //                              where check_number = @CHKNUM and scan_item = '2'  order by scan_date desc

    //        //                              IF @OKsupply_date IS NOT NULL 
    //        //                              BEGIN 
    //        //                               SET @issupply = 1 
    //        //                              END

    //        //                              select top 1 @OKarrive_date =scan_date from ttDeliveryScanLog  with (nolock) 
    //        //                              where check_number = @CHKNUM and scan_item = '3'  order by scan_date desc				

    //        //                              --如果配達時間小於13:00 且配達日期=應配送日期-->午前配達率=1		
    //        //                              IF @OKarrive_date IS NOT NULL AND  (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'13:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@supplier_date, 112))
    //        //                              BEGIN 
    //        //                               set  @noon_arrive = 1
    //        //                              END

    //        //                              --如果配達時間小於17:00 且配達日期=應配送日期-->午前配達率=1
    //        //                              IF @OKarrive_date IS NOT NULL AND (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'17:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@supplier_date, 112))
    //        //                              BEGIN 
    //        //                               set @day_arrive = 1
    //        //                              END

    //        //                              UPDATE #mylist
    //        //                                set issupply  =@issupply , 
    //        //                                noon_arrive = @noon_arrive, 
    //        //                                day_arrive=@day_arrive ,
    //        //                                OKarrive_date= @OKarrive_date
    //        //                              WHERE check_number = @CHKNUM

    //        //                             FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@supplier_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@OKarrive_date,@should
    //        //                             END
    //        //                            CLOSE R_cursor
    //        //                            DEALLOCATE R_cursor

    //        //                            SELECT a.supplier_code, a.MIN , b.MAX 
    //        //                            into #tb1
    //        //                            FROM   (SELECT supplier_code, MIN(OKarrive_date) AS MIN FROM #mylist group by supplier_code) a,  
    //        //                                   (SELECT supplier_code, MAX(OKarrive_date) AS MAX FROM #mylist group by supplier_code) b
    //        //                            where a.supplier_code = b.supplier_code;

    //        //                            select  ROW_NUMBER() OVER(ORDER BY tb1.supplier_code ) AS NO , tb1.*, CONVERT(varchar , #tb1.MIN, 108) as MIN , CONVERT(varchar , #tb1.MAX, 108) as MAX
    //        //                            from (select A.supplier_code,  CASE A.supplier_code WHEN  '001' THEN '零擔'  WHEN  '002' THEN '流通' ELSE B.supplier_name END as supplier_name  ,
    //        //                                  sum(A.should ) '應配筆數', sum(A.issupply ) '配送筆數', sum(A.noon_arrive) '午前配達筆數', sum(A.day_arrive)  '當日配達筆數'  
    //        //                                  from #mylist A
    //        //                                  left join tbSuppliers B on A.supplier_code = B.supplier_code 
    //        //                                  group by A.supplier_code,B.supplier_name) tb1, #tb1
    //        //                            where tb1.supplier_code = #tb1.supplier_code


    //        //                            IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
    //        //                            DROP TABLE #mylist

    //        //                            IF OBJECT_ID('tempdb..#tb1') IS NOT NULL
    //        //                            DROP TABLE #tb1");

    //        cmd.CommandText = string.Format(@"DECLARE  @NO NVARCHAR(5) 
    //                                            DECLARE  @CHKNUM NVARCHAR(20)  --貨號
    //                                            DECLARE  @supplier_code NVARCHAR
    //                                         DECLARE  @area_arrive_code NVARCHAR
    //                                            DECLARE  @supplier_date DateTime 
    //                                            DECLARE  @issupply INT 
    //                                            DECLARE  @noon_arrive  INT
    //                                            DECLARE  @day_arrive  INT
    //                                            DECLARE  @ok_arrive INT
    //                                            DECLARE  @first_time  NVARCHAR(10)
    //                                            DECLARE  @last_time NVARCHAR(10) 
    //                                            DECLARE  @OKsupply_date DateTime 
    //                                            DECLARE  @OKarrive_date DateTime 
    //                                            DECLARE  @OKsupplier_date DateTime 
    //                                            DECLARE  @should  INT
    //                                            DECLARE  @LTT BIT 

    //                                            Select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
    //                                            A.check_number,
    //                                            A.area_arrive_code ,
    //                                            A.supplier_date ,				   --應配送日期
    //                                            0 as issupply,					   --是否配送
    //                                            0 as noon_arrive,				   --是否中午前配達(13:00前)
    //                                            0 as day_arrive ,				   --當日配達(17:00前)
    //                                            0 as ok_arrive ,				   --已配達
    //                                            @OKarrive_date  as OKarrive_date,  --配達時間
    //                                            1 as should,
    //                                            --'00:00:00' as first_time , --首筆配達時間
    //                                            --'00:00:00' as last_time    --末筆配達時間
    //                                            Less_than_truckload
    //                                            into #mylist	  
    //                                            from tcDeliveryRequests  A with(nolock)
    //                                            where  A.check_number <> ''
    //                                            and CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@dates,111) AND CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@datee,111)                                                
    //                                         and A.area_arrive_code LIKE ''+  ISNULL(@_supplier_code,'') +'%'
    //                                            and A.customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
    //                                            and A.cancel_date  IS NULL and A.Less_than_truckload = @Less_than_truckload
    //                                            order by A.check_number 

    //                                            DECLARE R_cursor CURSOR FOR
    //                                            select * FROM #mylist
    //                                            OPEN R_cursor
    //                                            FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@area_arrive_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@ok_arrive,@OKarrive_date,@should, @LTT
    //                                            WHILE (@@FETCH_STATUS = 0) 
    //                                             BEGIN	
    //                                              select top 1  @OKsupply_date = scan_date from ttDeliveryScanLog  with (nolock)  
    //                                              where check_number = @CHKNUM and scan_item = '2' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc

    //                                              IF @OKsupply_date IS NOT NULL 
    //                                              BEGIN 
    //                                               SET @issupply = 1 
    //                                              END

    //                                                    SET @OKsupplier_date = NULL
    //										SET @OKarrive_date = NULL

    //                                                    Select top 1 @OKsupplier_date =scan_date  from ttDeliveryScanLog  with (nolock) 
    //                                              where check_number = @CHKNUM and scan_item = '2' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc	

    //                                              Select top 1 @OKarrive_date =scan_date , @ok_arrive= case when scan_date IS NOT NULL then 1 End from ttDeliveryScanLog  with (nolock) 
    //                                              where check_number = @CHKNUM and scan_item = '3' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc				

    //                                              --如果配達時間小於13:00 且配達日期=應配送日期-->午前配達率=1		
    //                                              IF @OKarrive_date IS NOT NULL AND @OKsupplier_date IS NOT NULL AND  (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'13:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@OKsupplier_date, 112))
    //                                              BEGIN 
    //                                               set  @noon_arrive = 1
    //                                              END

    //                                              --如果配達時間小於17:00 且配達日期=應配送日期-->午前配達率=1
    //                                              IF @OKarrive_date IS NOT NULL AND @OKsupplier_date IS NOT NULL AND (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'17:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@OKsupplier_date, 112))
    //                                              BEGIN 
    //                                               set @day_arrive = 1
    //                                              END

    //                                              UPDATE #mylist
    //                                                set issupply  =@issupply , 
    //                                                noon_arrive = @noon_arrive, 
    //                                                day_arrive=@day_arrive ,
    //                                                            ok_arrive=@ok_arrive ,
    //                                                OKarrive_date= @OKarrive_date
    //                                              WHERE check_number = @CHKNUM

    //                                             FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@area_arrive_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@ok_arrive,@OKarrive_date,@should, @LTT
    //                                             END
    //                                            CLOSE R_cursor
    //                                            DEALLOCATE R_cursor

    //                                            SELECT a.area_arrive_code, a.MIN , b.MAX 
    //                                            into #tb1
    //                                            FROM   (SELECT area_arrive_code, MIN(OKarrive_date) AS MIN FROM #mylist group by area_arrive_code) a,  
    //                                                    (SELECT area_arrive_code, MAX(OKarrive_date) AS MAX FROM #mylist group by area_arrive_code) b
    //                                            where a.area_arrive_code = b.area_arrive_code;

    //                                            select  ROW_NUMBER() OVER(ORDER BY tb1.area_arrive_code ) AS NO , tb1.*, CONVERT(varchar , #tb1.MIN, 108) as MIN , CONVERT(varchar , #tb1.MAX, 108) as MAX
    //                                            from (select A.area_arrive_code,  
    //                                                    CASE WHEN A.Less_than_truckload  = 1 THEN A.area_arrive_code ELSE (CASE A.area_arrive_code WHEN  '001' THEN '零擔'  WHEN  '002' THEN '流通' ELSE B.supplier_name END) END	as area_arrive_name  ,
    //                                                    sum(A.should ) '應配筆數', sum(A.issupply ) '配送筆數', sum(A.noon_arrive) '午前配達筆數', sum(A.day_arrive)  '當日配達筆數'  , sum(A.ok_arrive )  '已配達筆數' 
    //                                                    from #mylist A
    //                                                    left join tbSuppliers B on A.area_arrive_code = B.supplier_code and  ISNULL(B.supplier_code,'') <> ''
    //                                                    group by A.area_arrive_code, CASE WHEN A.Less_than_truckload  = 1 THEN A.area_arrive_code ELSE (CASE A.area_arrive_code WHEN  '001' THEN '零擔'  WHEN  '002' THEN '流通' ELSE B.supplier_name END) END ) tb1, #tb1
    //                                            where tb1.area_arrive_code = #tb1.area_arrive_code


    //                                            IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
    //                                            DROP TABLE #mylist

    //                                            IF OBJECT_ID('tempdb..#tb1') IS NOT NULL
    //                                            DROP TABLE #tb1");


    //        using (DataTable dt = dbAdapter.getDataTable(cmd))
    //        {

    //            if (dt.Rows.Count > 0)
    //            {
    //                double[] Tot = { 0,0,0,0,0,0};
    //                dt.Columns.Add(new DataColumn("noonRate", typeof(String)));   //午前配達率
    //                dt.Columns.Add(new DataColumn("dayRate", typeof(String)));    //當日配達率
    //                dt.Columns.Add(new DataColumn("noArrive", typeof(String)));   //未配達數
    //                dt.Columns.Add(new DataColumn("noRate", typeof(String)));     //異常率
    //                dt.Columns.Add(new DataColumn("yesRate", typeof(String)));    //配達率 

    //                for (int i = 0; i <= dt.Rows.Count-1; i++)
    //                {
    //                    string supplier_code = dt.Rows[i]["area_arrive_code"].ToString();
    //                    double should = dt.Rows[i]["應配筆數"] != DBNull.Value ? (int)dt.Rows[i]["應配筆數"] : 0;
    //                    Tot[0] += should;
    //                    double supply = dt.Rows[i]["配送筆數"] != DBNull.Value ? (int)dt.Rows[i]["配送筆數"] : 0;
    //                    Tot[1] += supply;
    //                    double noon = dt.Rows[i]["午前配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["午前配達筆數"] : 0;
    //                    Tot[2] += noon;
    //                    double day = dt.Rows[i]["當日配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["當日配達筆數"] : 0;
    //                    Tot[3] += day;
    //                    double okArrive = dt.Rows[i]["已配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["已配達筆數"] : 0;
    //                    Tot[4] += okArrive;
    //                    double noArrive = should - okArrive;
    //                    Tot[5] += noArrive;
    //                    dt.Rows[i]["noonRate"] = ((noon / should) * 100).ToString("N2");
    //                    dt.Rows[i]["dayRate"] = ((day / should) * 100).ToString("N2");
    //                    dt.Rows[i]["yesRate"] = ((okArrive / should) * 100).ToString("N2");
    //                    dt.Rows[i]["noArrive"] = noArrive.ToString();
    //                    dt.Rows[i]["noRate"] = ((noArrive / should) * 100).ToString("N2");
    //                    dt.Rows[i]["MIN"] = dt.Rows[i]["MIN"] !=DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["MIN"]).ToString("HH:mm") :"";
    //                    dt.Rows[i]["MAX"] = dt.Rows[i]["MAX"] !=DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["MAX"]).ToString("HH:mm") :"";

    //                    //string firstTime = string.Empty;
    //                    //string lastTime = string.Empty;


    //                    //send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
    //                    //string send_tel = dt.Rows[0]["send_tel"].ToString();
    //                    //send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
    //                    //string receive_contact = dt.Rows[0]["receive_contact"].ToString();
    //                    //receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
    //                    //string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
    //                    //receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;
    //                }

    //                DataRow row = dt.NewRow();
    //                row["area_arrive_code"] = "all";
    //                row["area_arrive_name"] = "總計";
    //                row["應配筆數"] = Tot[0];
    //                row["配送筆數"] = Tot[1];
    //                row["午前配達筆數"] = Tot[2];
    //                row["當日配達筆數"] = Tot[3];
    //                row["已配達筆數"] = Tot[4];
    //                row["noArrive"] = Tot[5];
    //                row["noonRate"] = ((Tot[2] / Tot[0]) * 100).ToString("N2");
    //                row["dayRate"] = ((Tot[3] / Tot[0]) * 100).ToString("N2");
    //                row["yesRate"] = ((Tot[4] / Tot[0]) * 100).ToString("N2");
    //                row["noRate"] = ((Tot[5] / Tot[0]) * 100).ToString("N2");



    //                dt.Rows.Add(row);

    //            }
    //            New_List.DataSource = dt;
    //            New_List.DataBind();
    //            DT = dt;
    //        }

    //    }
    //}

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
        //paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/trace_3.aspx?search=yes" + querystring));
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("配達統計");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 15;
            sheet1.Column(8).Width = 15;
            sheet1.Column(9).Width = 15;
            sheet1.Column(10).Width = 15;
            sheet1.Column(11).Width = 15;
            sheet1.Column(12).Width = 15;
            sheet1.Column(13).Width = 15;
            sheet1.Column(14).Width = 15;


            sheet1.Cells[1, 1, 1, 14].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 14].Value = "配達統計";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 14].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 14].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 14].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 14].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 14].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "發送日期：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            sheet1.Cells[3, 1,3, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1,3, 14].Style.Font.Size = 12;
            sheet1.Cells[3, 1,3, 14].Style.Font.Bold = true;
            sheet1.Cells[3, 1,3, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 14].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                   
            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "配送站所";
            sheet1.Cells[3, 3].Value = "應配筆數";
            sheet1.Cells[3, 4].Value = "配送筆數";
            sheet1.Cells[3, 5].Value = "午前配達筆數";
            sheet1.Cells[3, 6].Value = "午前配達率";
            sheet1.Cells[3, 7].Value = "當日配達筆數";
            sheet1.Cells[3, 8].Value = "當日配達率";
            sheet1.Cells[3, 9].Value = "已配達筆數";
            sheet1.Cells[3, 10].Value = "配達率";
            sheet1.Cells[3, 11].Value = "未配達筆數";
            sheet1.Cells[3, 12].Value = "異常率";
            sheet1.Cells[3, 13].Value = "首筆配達";
            sheet1.Cells[3, 14].Value = "末筆配達";
           

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["area_arrive_name"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["應配筆數"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["配送筆數"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["午前配達筆數"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["noonRate"].ToString();
                sheet1.Cells[i + 4, 7].Value = DT.Rows[i]["當日配達筆數"].ToString();
                sheet1.Cells[i + 4, 8].Value = DT.Rows[i]["dayRate"].ToString();
                sheet1.Cells[i + 4, 9].Value = DT.Rows[i]["已配達筆數"].ToString();
                sheet1.Cells[i + 4, 10].Value = DT.Rows[i]["yesRate"].ToString();
                sheet1.Cells[i + 4, 11].Value = DT.Rows[i]["noArrive"].ToString();
                sheet1.Cells[i + 4, 12].Value = DT.Rows[i]["noRate"].ToString();
                sheet1.Cells[i + 4, 13].Value = DT.Rows[i]["MIN"].ToString();
                sheet1.Cells[i + 4, 14].Value = DT.Rows[i]["MAX"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=report.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}