﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="Oops.aspx.cs" Inherits="Oops" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Error Page</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/templatemo-style.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/css.css" rel="stylesheet" />
    <style type="text/css">
        .error{
            width:95%;
            height:100%;
            background-color:#ffffff;
        }

    </style>
</head>
<body>
    <form runat="server">
        <div class="templatemo-content-widget error">
            <i class="fa fa-exclamation-triangle"></i>
            <p class="nu">
                <asp:Label ID="lbl_title" runat="server"></asp:Label></p>
            <hr />
            <p>
                <asp:Label ID="lbl_msg" runat="server"></asp:Label>
            </p>
            <div style="text-align: center;">
                <input type="button" class="btn btn-primary" onclick="window.history.go(-1); return false;" value="回上一頁" />
            </div>
        </div>
    </form>
</body>
</html>


