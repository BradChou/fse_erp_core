﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_1_3.aspx.cs" Inherits="LT_transport_1_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.fancybox').fancybox({
                'width': '100%',
            });
            $.fancybox.update();

            init();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                init();
            }

        });

        function init() {
            $("#<%= dltoday_customer_code.ClientID%>").change(function () {
                showBlockUI();
            });

            $("input[type=radio][name=check]").change(function () {
                if (this.value == "chkHeader") {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                    $("input[type=number][name=txtChkRange]").val(0);
                } else if (this.value == "chkRange") {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
            });

            $("#custom_table [id*=chkRow]").change(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("input[type=radio][name=check][value=chkHeader]").prop("checked", true);
                } else {
                    $("input[type=radio][name=check][value=chkRange]").prop("checked", true);
                }
            });

            $("input[type=number][name=txtChkRange]").change(function () {
                var start = $("#chkStart").val() - 1;
                if (start < 0)
                    start = 0;
                var end = $("#chkEnd").val();

                if (start <= end) {
                    $("#custom_table input[type='checkbox']").prop("checked", false);
                    $("#custom_table input[type='checkbox']").slice(start, end).prop("checked", true);
                }
            });

            $("input[type=number][name=txtChkRange]").focus(function () {
                $("input[type=radio][name=check][value=chkRange]").prop("checked", true);
            });

            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: -7, temp
                defaultDate: (new Date())  //預設當日
            });
        }


        document.onkeydown = function () {
            if (event.keyCode == 116) {
                event.keyCode = 0;
                event.returnValue = false;
            }
        }
        document.oncontextmenu = function () { event.returnValue = false; }

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

    </script>

    <style>
        input[type="radio"] {
            display: inline-block;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .modal-dialog {
            width: 1200px;
        }

        .ralign {
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="modal fade" id="modal-default_01">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <h2 class="margin-bottom-10">修改件數</h2>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div id="div_search" runat="server">
                                        <div class="templatemo-login-form">
                                            <div class="form-group form-inline">
                                                <label>託運單號：</label>
                                                <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20"></asp:TextBox>
                                                <asp:Button ID="Button1" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" CausesValidation="false" />
                                            </div>
                                            <%--<div class="form-group form-inline">
                                    <asp:RadioButton ID="rb2" runat="server" CssClass="radio radio-success" Text="發送區間" GroupName="dategroup" Checked="true"></asp:RadioButton>
                                    <asp:TextBox ID="tb_dateS2" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                            ~
                                    <asp:TextBox ID="tb_dateE2" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:RadioButton ID="rb1" runat="server" CssClass="radio radio-success" Text="建單區間" GroupName="dategroup"></asp:RadioButton>
                                    <asp:TextBox ID="tb_dateS1" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                            ~
                                    <asp:TextBox ID="tb_dateE1" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                </div>--%>

                                            <%--<div class="form-group form-inline">
                                    <label>匯入單號：</label>
                                    <asp:TextBox ID="Import_number" runat="server" class="form-control" placeholder="請輸入匯入單號"></asp:TextBox>
                                </div>--%>

                                            <div class="margin-right-15 templatemo-inline-block">
                                                <%--<label for="inputFirstName">託運類別：</label>--%>
                                                <asp:RadioButtonList ID="rb_pricing_type" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio radio-success" Visible="false">
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <asp:UpdatePanel ID="UpdatePane1" runat="server">
                            <ContentTemplate>
                                <div class="templatemo-content-widget white-bg">
                                    <div class="row">
                                        <div class="templatemo-login-form">
                                            <div class="form-group form-inline">
                                                <label>
                                                    <asp:Label ID="lbDeliveryType" runat="server" Text="出貨"></asp:Label>日期：</label><asp:TextBox ID="Shipments_date" runat="server" CssClass="form-control date_picker " autocomplete="off"></asp:TextBox>
                                                <asp:DropDownList ID="dlDeliveryType" runat="server" CssClass="form-control ">
                                                    <asp:ListItem Selected="True" Value="D">出貨</asp:ListItem>
                                                    <asp:ListItem Value="R">回收</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                            <div class="form-group form-inline">
                                                <label>客代編號：</label>
                                                <asp:Label ID="lbcustomer_code" runat="server"></asp:Label>

                                                <asp:Label ID="lbcustomer_name" runat="server"></asp:Label>

                                            </div>
                                            <!--基本設定-->
                                            <div class="bs-callout bs-callout-info">
                                                <h3>1. 基本設定</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label for="lbcheck_number"><span class="REDWD_b">*</span>託運單號</label>
                                                            <asp:Label ID="lbcheck_number" runat="server" BackColor="#FFFF66" CssClass="form-control "></asp:Label>
                                                            <asp:HiddenField ID="hid_id" runat="server" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputFirstName">訂單號碼</label>
                                                            <asp:TextBox ID="order_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="check_type"><span class="REDWD_b">*</span>託運類別</label>
                                                            <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputFirstName">收貨人編號</label>
                                                            <asp:TextBox ID="receive_customer_code" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputLastName"><span class="REDWD_b">*</span>傳票類別</label>
                                                            <asp:DropDownList ID="subpoena_category" runat="server" class="form-control subpoena_category">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>2. 收件人</h3>
                                                <div class="rowform">

                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label>收件人編號</label>
                                                            <asp:TextBox ID="receiver_code" runat="server" class="form-control" Width="80px"></asp:TextBox>
                                                            <%--<asp:Button ID="btnRecsel" CssClass="templatemo-white-button" runat="server" Text="通訊錄" Font-Size="X-Small" OnClick="btnRecsel_Click" />--%>
                                                            <label><span class="REDWD_b">*</span>收件人</label>
                                                            <asp:TextBox ID="receive_contact" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                                            <asp:Button Style="display: none" ID="btnqry" runat="server" Text="" />
                                                            <div style="display: none">
                                                                <asp:TextBox ID="receiver_id" runat="server" />
                                                            </div>
                                                            &nbsp;
                                                        </div>
                                                        <div class="form-group">
                                                            <label><span class="REDWD_b">*</span>電話1</label>
                                                            <asp:TextBox ID="receive_tel1" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^-0-9]/g,'')" MaxLength="20"></asp:TextBox>
                                                            分機
                                        <asp:TextBox ID="receive_tel1_ext" runat="server" class="form-control" MaxLength="5" Width="80"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>電話2</label>
                                                            <asp:TextBox ID="receive_tel2" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^-0-9]/g,'')" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                        <%--   <div id="divReceiver" runat="server" visible="false" class="templatemo-login-form panel panel-default" style="overflow: auto; height: 300px; background-color:aliceblue">
                                            <br />
                                            <div>
                                                <div class="form-group form-inline">
                                                    <label>關鍵字：</label>
                                                    <asp:TextBox ID="keyword" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                                    <asp:Button ID="cancel" CssClass="btn btn-default " runat="server" Text="關 閉" OnClick="cancel_Click" />
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered templatemo-user-table">
                                                <tr class="tr-only-hide">
                                                    <th nowrap class="text-center ">選取</th>
                                                    <th nowrap class="text-center ">客戶代碼</th>
                                                    <th nowrap class="text-center ">客戶名稱</th>
                                                    <th nowrap class="text-center ">地址</th>
                                                </tr>
                                                <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-th="選取">
                                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                                                <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                                            </td>
                                                            <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                                            <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                                            <td data-th="地址" class="text-left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_road").ToString())%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <% if (New_List.Items.Count == 0)
                                                    {%>
                                                <tr>
                                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                                </tr>
                                                <% } %>
                                            </table>
                                        </div>--%>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label><span class="REDWD_b">*</span>地&nbsp; 址</label>
                                                            <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control chosen-select _shortwidth"></asp:DropDownList>
                                                            <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control"></asp:DropDownList>
                                                            <asp:TextBox ID="receive_address" CssClass="form-control address" runat="server" MaxLength="70" placeholder="請輸入地址"></asp:TextBox>
                                                            &nbsp;                                
                                            
                                                        </div>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label><span class="REDWD_b">*</span>到著碼</label>
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="cbarrive" runat="server" Text="站址自領" /></span>
                                                            <%--                                                            <asp:DropDownList ID="area_arrive_code" runat="server" CssClass="form-control _shortwidth">
                                                            </asp:DropDownList>--%>
                                                        </div>
                                                    </div>
                                                    <%--                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="receive_tel1" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="receive_contact" ForeColor="Red" ValidationGroup="validate">請輸入收件人</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="receive_city" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="receive_area" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="receive_address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>3. 才件代收</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label5" runat="server" Text="件數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="pieces" CssClass="form-control " BackColor="#FFFF66" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label6" runat="server" Text="板數" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="plates" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label7" runat="server" Text="才數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="cbm" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>
                                                        </div>
                                                        <asp:Label ID="lbErrQuant" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            &nbsp;&nbsp;&nbsp;<span class="REDWD_b">*</span><asp:Label ID="lbCbmSize" runat="server" Text="材積大小" CssClass="ralign"></asp:Label><asp:DropDownList ID="dlCbmSize" runat="server" CssClass="form-control " Width="100px"></asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="reg_size" runat="server" ControlToValidate="dlCbmSize" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇材積大小</asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label8" runat="server" Text="＄代收金額" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="collection_money" CssClass="form-control  _collection_money" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label9" runat="server" Text="＄到付運費(含稅)" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control  _arrive_to_pay_freight" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label10" runat="server" Text="＄到付追加" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="arrive_to_pay_append" CssClass="form-control  _arrive_to_pay_append" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="form-group" id="individual_fee" runat="server" style="display: none;">
                                                                    <asp:Label ID="Label11" runat="server" Text="＄貨件運費" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_supplier_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label12" runat="server" Text="＄配送費用" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="i_csection_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label13" runat="server" Text="＄偏遠加價" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_remote_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>*<br>
                                                    特殊設定</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label>產品編號</label>
                                                            <asp:TextBox ID="ArticleNumber" runat="server" class="form-control"></asp:TextBox>
                                                            <label>出貨平台</label>
                                                            <asp:TextBox ID="SendPlatform" runat="server" class="form-control"></asp:TextBox>
                                                            <label>袋號</label>
                                                            <asp:TextBox ID="bagno" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row ">
                                                        <div class="form-group form-inline  ">
                                                            <label>品名</label>
                                                            <asp:TextBox ID="ArticleName" runat="server" class="form-control " Width="70%"></asp:TextBox>
                                                        </div>



                                                    </div>
                                                    <div class="row form-inline">


                                                        <div class="form-group">
                                                            <label>指定日</label>
                                                            <asp:TextBox ID="arrive_assign_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                                            <asp:Label ID="Label14" runat="server" Text="時段"></asp:Label>
                                                            <asp:DropDownList ID="time_period" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                            <label for="dltemp">配送溫層</label>
                                                            <asp:DropDownList ID="dltemp" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label for="inputLastName">備註</label>

                                                            <textarea id="invoice_desc" runat="server" cols="20" rows="3" maxlength="50" class="form-control"></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="receipt_flag" runat="server" Text="回單" /></span>
                                                            <br />
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="pallet_recycling_flag" runat="server" Text="棧板回收" onclick="pallet_recycling_flagClick(this)" /></span>
                                                            <a id="carsel" href="transport_1_carsel.aspx?Pallet_type=<%=Pallet_type.ClientID %>&Pallet_type_text=<%=Pallet_type_text.ClientID %>" class="fancybox fancybox.iframe "></a>
                                                            <div id="Pallet_type_div" runat="server" style="display: none;">
                                                                棧板種類<asp:TextBox ID="Pallet_type_text" CssClass="form-control" runat="server" Width="80px" ReadOnly></asp:TextBox>
                                                                <asp:HiddenField ID="Pallet_type" runat="server" />


                                                            </div>
                                                            <br />
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="receipt_round_trip" runat="server" Text="來回件" /></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="turn_board" runat="server" Text="翻板" /></span>
                                                            ＄<asp:TextBox ID="turn_board_fee" CssClass="form-control _turn_board_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                                            <br />
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="upstairs" runat="server" Text="上樓" /></span>
                                                            ＄<asp:TextBox ID="upstairs_fee" CssClass="form-control _upstairs_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                                            <br />
                                                            <span class="checkbox checkbox-success">
                                                                <asp:CheckBox ID="difficult_delivery" runat="server" Text="困配" /></span>
                                                            ＄<asp:TextBox ID="difficult_fee" CssClass="form-control _difficult_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>4. 寄件人</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group">
                                                                    <label>&nbsp;客戶代號</label>
                                                                    <asp:TextBox ID="customer_code" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                                                    <asp:Label ID="Label15" runat="server" Text="寄件人"></asp:Label>
                                                                    <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    <span class="checkbox checkbox-success">
                                                                        <asp:CheckBox ID="chkshowsend" runat="server" Text="顯示寄件人資料" /></span>
                                                                </div>
                                                                <label>電話/手機</label>
                                                                <asp:TextBox ID="send_tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>

                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                            <ContentTemplate>
                                                                <div class="form-group">
                                                                    <label>&nbsp;寄件地址</label>
                                                                    <asp:DropDownList ID="send_city" runat="server" CssClass="form-control chosen-select _shortwidth" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                                                    <asp:DropDownList ID="send_area" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                    <asp:TextBox ID="send_address" CssClass="form-control address" runat="server"></asp:TextBox>
                                                                    <span class="checkbox checkbox-success">
                                                                        <asp:CheckBox ID="chksend_address" class="font-weight-400" runat="server" Text="無地址" /></span>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info" id="div_cancel" runat="server" visible="false">
                                                <h3>銷單資訊</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label>銷單日期：</label><asp:Literal ID="liCancel_date" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label>銷單人員：</label><asp:Literal ID="liCancel_psn" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label>銷單原因：</label><asp:Literal ID="liCancel_memo" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group text-center">
                                                <%--<asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />--%>
                                                <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" OnClick="btnsave_Click" />
                                                <%--<asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />--%>
                                                <%--<asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="row">
            <h2 class="margin-bottom-10">整批匯入</h2>
            <div class="templatemo-login-form">
                <!---->
                <div class="form-style-10">

                    <div class="section"><span>1</span>下載空白託運單</div>
                    <div class="inner-wrap">
                        <%-- <a href="~/report/空白託運單標準格式.xlsx" id="hlFileView" class="templatemo-white-button" aria-hidden="true">空白託運單</a>--%>
                        <asp:Button ID="btndownload" class="templatemo-white-button" runat="server" Text="全速配空白託運單" OnClick="btndownload_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btndownloadbuy123" class="templatemo-white-button" runat="server" Text="生活市集空白託運單" OnClick="btndownload_Click_buy123" />
                        <p class="text-primary">※第一次使用請下載空白託運表單標準格式</p>
                        <p style="color: coral; font-size: large; font-weight: bold">※版本更新日期：2022/09/22，若您匯入版本相較於本日期為舊，請先下載空白託運單</p>
                    </div>

                    <div class="section"><span>2</span>上傳檔案</div>
                    <div class="inner-wrap ">
                        <table>
                            <tr>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="★.請選擇要匯入的客代"></asp:Label>
                                        <asp:DropDownList ID="dlcustomer_code" runat="server" CssClass="chosen-select" AutoPostBack="true" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:Label ID="LiRemainCnt" runat="server" CssClass="text-danger"></asp:Label>
                                        <asp:HiddenField ID="HiddenCount" runat="server" Value="99999" />
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇要匯入的客代</asp:RequiredFieldValidator>
                                        <label id="InnerInstruction" runat="server" style="color: red; display: none; margin-left: 10px">公司內部人員(包含站所)如需打單，請使用站所或部門客代帳號登入打單，目前不開放使用個人帳號打單。</label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </tr>
                            <caption>
                                <br />
                                <asp:Label ID="Label4" runat="server" Text="★.請選擇發送日期　　"></asp:Label>
                                <asp:TextBox ID="date" runat="server" CssClass="datepicker"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="req2" runat="server" ControlToValidate="date" Display="Dynamic" ForeColor="Red" ValidationGroup="validate">請選擇發送日期</asp:RequiredFieldValidator>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Text="★.請選擇要匯入的檔案項目"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="FileDropDownList" runat="server" CssClass="chosen-select"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="file01" runat="server" Width="300px" />
                                        <!-- 強制塞入Token，防止網頁重新整理-->
                                        <input id="hiddenTest" type="hidden" value="<%= GetToken() %>" name="hiddenTestN" />
                                    </td>
                                    <td>
                                        <asp:Button ID="btImport" runat="server" CssClass="templatemo-blue-button" OnClick="btImport_Click" Text="確認上傳" ValidationGroup="validate" />
                                        <%--btImport_Click有在使用--%>
                                        <asp:Button ID="btImport_SB" runat="server" CssClass=" templatemo-blue-button " OnClick="btImport_SB_Click" Text="確認上傳" />
                                    </td>
                                </tr>
                            </caption>
                        </table>
                        <div>
                            <div style="overflow: auto; height: 200px; vertical-align: top;">
                                <asp:ListBox ID="lbMsg" runat="server" Height="200px" Width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="section"><span>3</span></div>
                    <div class="inner-wrap" style="overflow: auto; height: calc(40vh); width: 100%">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <p>
                                    ※今日總筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal>&nbsp;&nbsp; </span>
                                    <asp:DropDownList ID="dltoday_customer_code" runat="server" CssClass="chosen-select" AutoPostBack="true" OnSelectedIndexChanged="dltoday_customer_code_SelectedIndexChanged" Visible="false"></asp:DropDownList>
                                    <asp:LinkButton ID="lbRefresh" runat="server" CssClass=" text-hide lbR" OnClick="lbRefresh_Click"></asp:LinkButton>
                                    <asp:HiddenField ID="hid_p" runat="server" />
                                    <asp:LinkButton ID="lbPrintLabel" runat="server" CssClass=" text-hide lbP" OnClick="lbPrintLabel_Click"></asp:LinkButton>
                                    <d>
                                        <asp:Label style="color: coral">一般配送:
                                            <asp:Label id="general_delivery" runat="server" text=""></asp:Label>
                                            假日配送:
                                            <asp:Label id="holiday_delivery" runat="server"></asp:Label>
                                            *請在週一至週五完成出貨</asp:Label></d>
                                </p>

                                <input type="radio" name="check" value="chkHeader">全選
                                <br>
                                <input type="radio" name="check" value="chkRange">從：
                                <input type="number" name="txtChkRange" id="chkStart" value="0" />至：<input type="number" name="txtChkRange" id="chkEnd" value="0" />

                                <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th></th>
                                        <th>No.</th>
                                        <th>傳票類別</th>
                                        <th>貨號</th>
                                        <th>收件人</th>
                                        <th>收件人電話</th>
                                        <th>收件人地址</th>
                                        <th>訂單編號</th>
                                        <th>指定配送</th>
                                        <th>件數</th>
                                        <th>指配日期</th>
                                        <th>代收貨款</th>
                                        <th>備註</th>
                                    </tr>
                                    <asp:Repeater ID="New_List" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="列印">
                                                    <asp:CheckBox ID="chkRow" runat="server" />
                                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                                    <asp:HiddenField ID="hid_print_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%>' />
                                                    <asp:HiddenField ID="Hid_print_flag" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_flag").ToString())%>' />
                                                    <asp:HiddenField ID="Hid_Supplier_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Supplier_date","{0:yyyy/MM/dd}").ToString())%>' />
                                                </td>
                                                <td data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ROWID").ToString())%></td>
                                                <td data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                                <td data-th="貨號">
                                                    <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label></td>
                                                <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                                <td data-th="收件人電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1").ToString())%></td>
                                                <td data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_address").ToString())%></td>
                                                <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                                                <td data-th="指定配送"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.holiday_delivery_chinese").ToString())%></td>
                                                <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%></td>
                                                <td data-th="指配日期">
                                                    <asp:Label ID="lbarrive_assign_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label></td>
                                                <td data-th="代收貨款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.collection_money").ToString())%></td>
                                                <td data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.invoice_desc").ToString())%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="11" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="section"><span>4</span>列印</div>
                    <div class="inner-wrap">
                        <div class="form-group form-inline">
                            <asp:Label ID="Label1" runat="server" Text="紙張格式"></asp:Label>
                            <asp:DropDownList ID="option" runat="server" class="form-control" OnSelectedIndexChanged="ShowPositionOrNot" AutoPostBack="true">
                                <asp:ListItem Value="0">A4 (一式6筆託運標籤)</asp:ListItem>
                                <asp:ListItem Value="1">捲筒列印</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CheckBox ID="cbShowSender" runat="server" Text="顯示寄件人資料" Checked="True" class="checkbox-inline" />
                        </div>
                        <div class="row form-inline">
                            <div class="form-group form-inline" id="posi" style="display: inline-block" runat="server">
                                選擇列印起始位置:
                                <asp:RadioButtonList ID="Position" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CssClass="radio radio-success" Style="left: 0px; top: 0px; border-style: solid; border-width: 1px;">
                                    <asp:ListItem Value="1" Text="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                    <asp:ListItem Value="3" Text=""></asp:ListItem>
                                    <asp:ListItem Value="4" Text=""></asp:ListItem>
                                    <asp:ListItem Value="5" Text=""></asp:ListItem>
                                    <asp:ListItem Value="6" Text=""></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="form-group">
                                <label class="form-group form-inline" id="printFileType" style="display: inline-block" runat="server">託運總表格式</label>
                            </div>
                            <div class="form-group">
                                <span>
                                    <input type="radio" id="ExcelFile" value="1" runat="server" checked>
                                    Excel (可編輯)
                                </span>
                                <br>
                                <span>
                                    <input type="radio" id="PDFFile" value="2" runat="server">
                                    PDF (不可編輯)
                                </span>
                                <br>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Button ID="btnPirntLabel" CssClass="btn btn-primary" runat="server" Text="列印標籤" OnClick="btnPirntLabel_Click" />
                            <asp:Button ID="btPrint" class="btn btn-primary" runat="server" Text="列印託運總表" OnClick="btPrint_Click" />
                            <a href="LT_transport_2_Search.aspx" class="fancybox fancybox.iframe btn  _d_btn " id="btCancel" runat="server"><span class="btn btn-warning">整批銷單</span></a>
                            <a href="LT_transport_2_Search_RedoCancel.aspx" class="fancybox fancybox.iframe btn  _d_btn " style="display: none" id="btRedoCancel"><span class="btn btn-warning">整批取消銷單</span></a>
                            <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-warning" OnClick="btnEdit_Click" Style="display: none">修改件數</asp:LinkButton>


                            <%--<a href="LT_transport_2_edit2.aspx" class="fancybox fancybox.iframe btn  _d_btn " id="btnEdit"><span class="btn btn-warning">修改件數</span></a>--%>
                        </div>
                    </div>
                    <div class="section"><span>5</span>出貨</div>
                    <p class="text-danger">依與站所約定時間前提出派員收件要求</p>
                    <asp:DropDownList ID="customerDropDown" runat="server" CssClass="chosen-select"></asp:DropDownList>
                    <asp:Button ID="Button" CssClass="btn btn-primary" runat="server" Text="派員收件" OnClick="btnAskForTakePackage_Click" />
                </div>
            </div>
            <%-- <hr>--%>
        </div>
    </div>
</asp:Content>

