﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using ClosedXML.Excel;
using System.Net;
using Ionic.Zip;
using System.Text.RegularExpressions;

public partial class trace_Rorderupload : System.Web.UI.Page
{
    
    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }


    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }


    }

    public string check_numbers
    {
        get { return (string)ViewState["check_numbers"]; }
        set { ViewState["check_numbers"] = value; }


    }

    public int addvalue
    {
        get
        {
            if (ViewState["addvalue"] == null)
            {
                ViewState["addvalue"] = 10; //預設新增10欄
            }
            return Convert.ToInt32(ViewState["addvalue"]);
        }
        set
        {
            ViewState["addvalue"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }

            BindTextBox();

        }

      


    }

    private void BindTextBox()
    {
        //PlaceHolder1.Controls.Clear(); //先清除所有子控制項
        DataTable dt = new DataTable();
        dt.Columns.Add("NO", typeof(int));
        dt.Columns.Add("check_number", typeof(string));
        dt.Columns.Add("filename", typeof(string));
        for (int i = 0; i < addvalue; i++)
        {
            DataRow row = dt.NewRow();
            row["NO"] = i+1;
            row["check_number"] = "";
            row["filename"] = "";
            dt.Rows.Add(row);
        }

        New_List.DataSource = dt;
        New_List.DataBind();
    }


    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e) {
        string _fileid = ((HiddenField)e.Item.FindControl("fileid")).Value.ToString().Trim();
        switch (e.CommandName) {
            case "cmdCheck":
                #region 回單影像
                if (_fileid != "")
                {
                    Regex reg = new Regex(";");
                    if (reg.IsMatch(_fileid))
                    {
                        string[] _rtn_filename = _fileid.Split(';');
                        int _count = 0;
                        foreach (string image in _rtn_filename)
                        {
                            if (image != "")
                            {
                                string iconpath = "http://220.128.210.180:10080/files/receipt/" + image;
                                string pdf = "http://220.128.210.180:10080/files/receipt/" + "pdf.png";
                                string fileExtension = System.IO.Path.GetExtension(image).ToLower().ToString();

                                if (fileExtension == ".pdf")
                                {

                                    Panel _myPanel = myPanel;
                                    LinkButton l = new LinkButton();
                                    l.ID = "Linkimage" + _count;
                                    l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + image);
                                    l.CssClass = "fancybox";
                                    l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", pdf, "imgSize");

                                    _myPanel.Controls.Add(l);
                                }
                                else
                                {

                                    Panel _myPanel = myPanel;
                                    LinkButton l = new LinkButton();
                                    l.ID = "Linkimage" + _count;
                                    l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + image);
                                    l.CssClass = "fancybox";
                                    l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", iconpath, "imgSize");
                                    
                                    _myPanel.Controls.Add(l);
                                }

                            }
                            _count++;
                        }
                    }
                    else
                    {

                        string fileExtension = System.IO.Path.GetExtension(_fileid).ToLower().ToString();

                        string iconpath = "http://220.128.210.180:10080/files/receipt/" + _fileid;
                        if (fileExtension == ".pdf")
                        {

                            string pdf = "http://220.128.210.180:10080/files/receipt/" + "pdf.png";
                            Panel _myPanel = myPanel;
                            LinkButton l = new LinkButton();
                            l.ID = "Linkimage" + 0;
                            l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + _fileid);
                            l.CssClass = "fancybox";
                            l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", pdf, "imgSize");

                            _myPanel.Controls.Add(l);
                        }
                        else {
                            Panel _myPanel = myPanel;
                            LinkButton l = new LinkButton();
                            l.ID = "Linkimage" + 0;
                            l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + _fileid);
                            l.CssClass = "fancybox";
                            l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", iconpath, "imgSize");

                            _myPanel.Controls.Add(l);
                        }
                    }
                }
                else
                {
                    //ImageButton2.Visible = false;
                }
                #endregion
                break;
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>checkSeatAdd();</script>", false);
        return;
    }

    protected void New_List_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {        
        HyperLink btn_upload = (HyperLink)e.Item.FindControl("btn_upload");
        TextBox check_number = (TextBox)e.Item.FindControl("check_number");
        TextBox filename = (TextBox)e.Item.FindControl("filename");
        HiddenField fileid = (HiddenField)e.Item.FindControl("fileid");
        if (fileid.Value != "") {
            fileid.Visible = true;
        }
        btn_upload.Attributes.Add("href", "fileupload_Rorder.aspx?filename=" + filename.ClientID + "&check_number="+ check_number.ClientID + "&fileid=" + fileid.ClientID );
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        int OK = 0;
        int NO = 0;
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.location.href='login.aspx';alert('登入時間逾時，請重新登入');parent.$.fancybox.close();</script>", false);
            return;
        }

        
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                TextBox check_number = ((TextBox)New_List.Items[i].FindControl("check_number"));
                HiddenField fileid = ((HiddenField)New_List.Items[i].FindControl("fileid"));

                if (check_number != null && check_number.Text != "")
                {
                    #region 
                    SqlCommand cmda = new SqlCommand();
                    DataTable dta = new DataTable();
                    cmda.Parameters.AddWithValue("@check_number", check_number.Text);
                    //cmda.Parameters.AddWithValue("@request_id", request_id);
                    cmda.CommandText = @"Select top 1* , isnull(B.log_id,'') as log_id
                                        from tcDeliveryRequests  A with(nolock) 
                                        left join  ttDeliveryFileN B with(nolock) on A.request_id = B.request_id 
                                        where A.check_number = @check_number and A.print_date  >= DATEADD(MONTH ,-6, getdate()) 
                                        order by A.cdate desc ";
                    dta = dbAdapter.getDataTable(cmda);
                    if (dta.Rows.Count > 0)
                    {
                        string request_id = dta.Rows[0]["request_id"].ToString();
                        if (dta.Rows[0]["log_id"].ToString() == "")
                        {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);               //貨運單號
                                    cmd.Parameters.AddWithValue("@request_id", request_id);
                                    cmd.Parameters.AddWithValue("@file_type", "1");                            //1:回單
                                    cmd.Parameters.AddWithValue("@file_path", "http://220.128.210.180:10080/files/receipt/");
                                    cmd.Parameters.AddWithValue("@file_name", fileid.Value);
                                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);           //建立人員
                                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);                      //建立時間            
                                    cmd.CommandText = dbAdapter.SQLdosomething("ttDeliveryFileN", cmd, "insert");
                                    dbAdapter.execNonQuery(cmd);
                                }
                            
                        }
                        else
                        {
                            using (SqlCommand cmdupd = new SqlCommand())
                            {
                                int log_id = Convert.ToInt32(dta.Rows[0]["log_id"]);
                                cmdupd.Parameters.AddWithValue("@check_number", check_number.Text);          //貨運單號
                                cmdupd.Parameters.AddWithValue("@request_id", request_id);
                                cmdupd.Parameters.AddWithValue("@file_type", "1");                           //1:回單
                                cmdupd.Parameters.AddWithValue("@file_path", "http://220.128.210.180:10080/files/receipt/");
                                cmdupd.Parameters.AddWithValue("@file_name", fileid.Value);
                                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);           //建立人員
                                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                      //建立時間
                                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "log_id", log_id);
                                cmdupd.CommandText = dbAdapter.genUpdateComm("ttDeliveryFileN", cmdupd);
                                dbAdapter.execNonQuery(cmdupd);
                            }
                        }

                        OK += 1;
                    }
                    else
                    {
                        ErrStr += "查無" + check_number.Text + "託運單\\r";
                        NO += 1;
                    }
                    

                    #endregion
                }


            }
        }

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完畢\\r成功："+ OK.ToString() + "\\r失敗："+ NO.ToString()+"\\r--------------------------------------------------\\r"+ ErrStr+"');</script>", false);

        return;
    }
   

    protected void check_number_TextChanged(object sender, EventArgs e)
    {
        TextBox check_number = (TextBox)sender;
        SqlCommand cmda = new SqlCommand();
        DataTable dta = new DataTable();
        cmda.Parameters.AddWithValue("@check_number", check_number.Text );
        cmda.CommandText = "select top 10*  from tcDeliveryRequests  where check_number = @check_number order by cdate desc ";
        dta = dbAdapter.getDataTable(cmda);
        if (dta.Rows.Count > 0)
        {

        }
        else {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無託運單');$(#"+ check_number.ClientID + ").val('');</script>", false);
            return;
        }
    }
}
    