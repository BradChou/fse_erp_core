﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="transport_5.aspx.cs" Inherits="transport_5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                init();
            });
        });

        function init() {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                defaultDate: (new Date())  //預設當日
            });

        }
    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hid_sel" runat="server" />
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">退貨單列印</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label for="rbcheck_type">取貨站所：</label>
                    <asp:DropDownList ID="ddlStation" runat="server" CssClass="form-control"></asp:DropDownList>
                    <%--<asp:DropDownList ID="Customer" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>--%>
                    <label for="rbcheck_type">司機帳號：</label>
                    <asp:TextBox ID="tb_search" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:DropDownList ID="ddlType" CssClass="form-control" runat="server">
                        <asp:ListItem Value="">全部</asp:ListItem>
                        <asp:ListItem Value="0">退貨</asp:ListItem>
                        <asp:ListItem Value="1">來回件</asp:ListItem>
                        <asp:ListItem Value="2">拒收</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem Value="">全部</asp:ListItem>
                        <asp:ListItem Value="0">未列印</asp:ListItem>
                        <asp:ListItem Value="1">已列印</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                        <asp:ListItem Value="0">集貨未成功</asp:ListItem>
                        <asp:ListItem Value="1">集貨已成功</asp:ListItem>
                        <asp:ListItem Value="">全部</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lbDate" runat="server" Text="發送日期"></asp:Label>
                    <asp:TextBox ID="date1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>~
                    <asp:TextBox ID="date2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20"></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                </div>
            </div>
            <hr>
            <%--<p>※筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>--%>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lbErr" runat="server" ForeColor="Red" Text="※請勾選要列印的退貨單"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="inner-wrap" style="overflow: auto; height: calc(50vh); width: 100%">
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>
                                    <asp:CheckBox ID="cbAll" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="true" />
                                    <%--<asp:CheckBox ID="chkHeader" runat="server" />--%>
                                </th>
                                <th class="tb_title _h_auto">建檔日</th>
                                <th class="tb_title _h_auto">取件日</th>
                                <th class="tb_title _h_auto">貨號</th>
                                <th class="tb_title _h_auto">訂單編號</th>
                                <th class="tb_title _h_auto">退貨人</th>
                                <th class="tb_title _h_auto">電話</th>
                                <th class="tb_title _h_addr">退貨人地址</th>
                                <th class="tb_title _h_auto">取貨人</th>
                                <th class="tb_title _h_auto">回覆狀態</th>
                                <th class="tb_title _h_auto">區分</th>
                                <th class="tb_title _h_auto">退貨屬性</th>
                                <th class="tb_title _h_auto">編輯</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="列印">
                                            <asp:CheckBox ID="chkRow" class="Method" runat="server" />
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                            <asp:HiddenField ID="Hid_print_flag" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_flag").ToString())%>' />
                                            <asp:HiddenField ID="Hid_arrive_assign_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date","{0:yyyy/MM/dd}").ToString())%>' />
                                        </td>
                                        <td data-th="建檔日期">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate","{0:yyyy/MM/dd}").ToString())%>
                                        </td>
                                        <td data-th="取件日">
                                            <asp:Label ID="lbprint_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="貨號">
                                            <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%> </td>
                                        <td data-th="退貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                                        <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_tel").ToString())%></td>
                                        <td data-th="退貨人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_address").ToString())%></td>
                                        <td data-th="取貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.newest_driver_code").ToString())%> </td>
                                        <td data-th="回覆狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_state").ToString())%> </td>
                                        <td data-th="區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item").ToString())%> </td>
                                        <td data-th="退貨屬性"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.returntype").ToString())%></td>
                                        <td data-th="編輯">
                                            <asp:Label ID="lbSupplier_date" runat="server" Visible="false" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                            <asp:Button ID="btn_Edit" CssClass="btn btn-success" CommandName="cmdEdit"
                                                runat="server" Text="編輯" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                            <asp:Button ID="Button2" CssClass="btn btn-warning" CommandName="cmdCancel"
                                                runat="server" Text="銷單" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="13" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                    <p>列印勾選筆數：<span class="chkcount"></span></p>
                </ContentTemplate>
            </asp:UpdatePanel>
            <hr>

            <div class="templatemo-login-form">
                <!---->
                <div class="form-style-10">
                    <div class="section"><span>1</span>進行列印</div>
                    <div class="inner-wrap">
                        <div class="form-group form-inline">
                            <asp:Button ID="btnPirntLabel" CssClass="btn btn-primary" runat="server" Text="進行列印" OnClick="btnPirntLabel_Click" />
                            <asp:DropDownList ID="option" runat="server" class="form-control">
                                <%--<asp:ListItem Value="0">退貨-直式</asp:ListItem>--%>
                                <asp:ListItem Value="0">退貨-一式二筆黏貼</asp:ListItem>
                                <asp:ListItem Value="1">拒收-一式6筆黏貼</asp:ListItem>
                                <asp:ListItem Value="2">拒收-捲筒</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group form-inline">
                            <div id="posi" style="display: inline-block" runat="server">
                                選擇列印起始位置:
                                <asp:RadioButtonList ID="Position" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CssClass="radio radio-success" Style="left: 0px; top: 0px; border-style: solid; border-width: 1px;">
                                    <asp:ListItem Value="1" Text="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                    <asp:ListItem Value="3" Text=""></asp:ListItem>
                                    <asp:ListItem Value="4" Text=""></asp:ListItem>
                                    <asp:ListItem Value="5" Text=""></asp:ListItem>
                                    <asp:ListItem Value="6" Text=""></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="form-group">
                                <label class="form-group form-inline" id="printFileType" style="display: inline-block" runat="server">託運總表格式</label>
                            </div>
                            <div class="form-group">
                                <span>
                                    <input type="radio" id="ExcelFile" value="1" runat="server" checked>
                                    Excel (可編輯)
                                </span>
                                <br>
                                <span>
                                    <input type="radio" id="PDFFile" value="2" runat="server">
                                    PDF (不可編輯)
                                </span>
                                <br>
                            </div>
                        </div>
                        <%--<p class="text-primary">貨號(條碼)產生中……</p>--%>
                    </div>
                    <div class="section"><span>2</span>列印託運總表</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btPrint" class="templatemo-blue-button" runat="server" Text="列印託運總表" OnClick="btPrint_Click" />
                        <asp:ListBox ID="lbMsg" runat="server" Width="100%" CssClass="lb_set" Visible="false"></asp:ListBox>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default_02" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <div class="templatemo-login-form">
                                <div class="form-group form-inline">
                                    <h4 class="modal-title" id="H1">請輸入銷單原因(限200字)</h4>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:TextBox ID="delete_memo" runat="server" class="form-control" TextMode="MultiLine" Width="100%" Height="200px" MaxLength="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="delete_memo" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入銷單原因</asp:RequiredFieldValidator>
                                    <br />
                                </div>
                                <div class="form-group text-center">
                                    <asp:HiddenField ID="Hid_Cancel_request_id" runat="server" Value="" />
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                                    <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="確認銷單" OnClientClick="return confirm('確定要銷單嗎?');" OnClick="btnCancel_Click" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default_01" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <div class="templatemo-login-form">
                                <div class="form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <label>取貨站所：</label>
                                            <asp:Label ID="lbstation_scode" runat="server">
                                            </asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <label>原貨號貨訂單號碼：</label>
                                            <asp:TextBox ID="strkey" MaxLength="21" runat="server" class="form-control"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <label>收貨人：</label>
                                            <asp:Label ID="lbreceive_contact" runat="server">  </asp:Label>
                                            <label>客代編號：</label>
                                            <asp:Label ID="lbcustomer_code" runat="server"> </asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <label>收貨人電話：</label>
                                            <asp:Label ID="lbreceive_tel1" runat="server">  </asp:Label>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="form-group form-inline">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                        <ContentTemplate>
                                            <label>收貨人地址：</label>
                                            <asp:Label ID="lbreceive_address" runat="server">  </asp:Label>
                                            <asp:HiddenField ID="hid_receive_city" runat="server" />
                                            <asp:HiddenField ID="hid_receive_area" runat="server" />
                                            <asp:HiddenField ID="hid_receive_address" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="bs-callout bs-callout-info">
                                    <h3>派遣內容</h3>
                                    <div class="rowform">
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <div class="row form-inline">
                                                    <div class="form-group  form-inline">
                                                        <label>退(寄)貨人</label>
                                                        <asp:TextBox ID="strsend_contact" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                                         <asp:label runat="server" ID="send_station_code"></asp:label>
                                                        <asp:Label runat="server" ID="Label1"></asp:Label>
                                                    </div>
                                                    <br>
                                                    <div class="form-group  form-inline">
                                                        <label>退(寄)貨人電話</label>
                                                        <asp:TextBox ID="strsend_tel" runat="server" class="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                    <br>
                                                    <div class="form-group  form-inline">
                                                        <label><span class="REDWD_b">*</span>退(寄)貨人地址</label>
                                                        <asp:DropDownList ID="ddlzip" runat="server" class="form-control  subpoena_category" Enabled="false">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlsend_city" runat="server" class="form-control  subpoena_category" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlsend_area" runat="server" class="form-control  subpoena_category" AutoPostBack="true" OnSelectedIndexChanged="area_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:TextBox ID="strsend_address" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <br>
                                                    <div class="form-group  form-inline">
                                                        <label>收回品項</label>
                                                        <textarea id="invoice_memo" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                                                    </div>
                                                    <br>
                                                    <div class="form-group  form-inline">
                                                        <label>備註(說明)</label>
                                                        <textarea id="invoice_desc" runat="server" cols="30" rows="3" maxlength="50" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                                    <asp:HiddenField ID="hid_request_id" runat="server" Value="" />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnClick="btn_OK_Click">儲存編輯</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

