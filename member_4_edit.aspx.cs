﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class member_4_edit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            
            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = " select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM'  order by code_id asc";
            SPM_code.DataSource = dbAdapter.getDataTable(cmd2);
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion


            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            City.DataSource = dbAdapter.getDataTable(cmd1);
            City.DataValueField = "city";
            City.DataTextField = "city";
            City.DataBind();
            City.Items.Insert(0, new ListItem("請選擇縣市別", ""));
            #endregion
            

        }
    }

    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        #region 欄位驗證
        if (supplier_id.Text.Length != 4)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入4碼代碼');</script>", false);
            return;
        }

        #endregion

        #region 判斷重覆

        string customer_code = dl_supplier_code.SelectedValue.ToString() + supplier_id.Text + "0" + SPM_code.SelectedValue.ToString();

        using (SqlCommand cmda = new SqlCommand())
        {
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@customer_code", customer_code);
            cmda.CommandText = "Select * from tbCustomers with(nolock) where customer_code=@customer_code ";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('客代已存在，請勿重覆建立');</script>", false);
                return;
            }
        }
            

        #endregion


        #region 新增        
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@supplier_id", supplier_id.Text );                                //主客代序號
            cmd.Parameters.AddWithValue("@supplier_code", dl_supplier_code.SelectedValue.ToString());      //區配商代碼
            cmd.Parameters.AddWithValue("@master_code", dl_supplier_code.SelectedValue.ToString() + supplier_id.Text);   //主客代完整代碼            
            cmd.Parameters.AddWithValue("@second_id", "0");                                               //次客代流水號
            cmd.Parameters.AddWithValue("@pricing_code", SPM_code.SelectedValue.ToString());               //計價模式
            cmd.Parameters.AddWithValue("@second_code", "0" + SPM_code.SelectedValue.ToString());           //次客代完整代碼
            
            cmd.Parameters.AddWithValue("@customer_code", customer_code);                                 //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
            cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
            cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());                //客戶簡稱
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間            
            cmd.Parameters.AddWithValue("@customer_type", "2");                                 //客戶類別(1:區配 2:竹運 3:自營)
            cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
            cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
            cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
            cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
            cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
            cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
            cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
            cmd.Parameters.AddWithValue("@customer_hcode", "");                                  //竹運客代
            cmd.Parameters.AddWithValue("@contact_1", "");                                       //竹運-聯絡人1  
            cmd.Parameters.AddWithValue("@email_1", "");                                         //竹運-聯絡人1 email
            cmd.Parameters.AddWithValue("@contact_2", "");                                       //竹運-聯絡人2
            cmd.Parameters.AddWithValue("@email_2", "");                                         //竹運-聯絡人2 email
            cmd.CommandText = dbAdapter.SQLdosomething("tbCustomers", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
            //cmd.CommandText = dbAdapter.genInsertComm("tbCustomers", true, cmd);        //新增
            //int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out customer_id);
        }

        #endregion



        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');parent.$.fancybox.close();parent.location.reload(true);</script>", false);
        return;
    }
}