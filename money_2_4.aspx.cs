﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;
using System.Web.Services;

public partial class money_2_4 : System.Web.UI.Page
{
    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 26).AddMonths(-1).ToString("yyyy/MM/dd") ;
            datee.Text = yy + "/" + mm + "/25";

            #region 
            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4" || ssManager_type == "5")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }

            Suppliers.DataSource = Utility.getSupplierDT2(ssSupplier_code, ssManager_type);
            Suppliers.DataValueField = "supplier_no";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));

            }
            #endregion



            dlclose_type_SelectedIndexChanged(null, null);

        }
    }

    private void readdata()
    {
        lbSuppliers.Text = Suppliers.SelectedItem.Text;
        lbdate.Text = dates.Text + "~" + datee.Text;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable_B";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue );  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable("B", dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.SelectedValue);
                if (dt != null && dt.Rows.Count > 0 && _info != null)
                {
                    DataRow row = dt.NewRow();
                    row["supplier_name"] = "總計";
                    row["plates"] = _info.plates;
                    row["price"] = _info.subtotal;                    
                    dt.Rows.Add(row);
                }

                New_List_01.DataSource = dt;
                New_List_01.DataBind();

                if (_info != null)
                {
                    subtotal_01.Text = _info.subtotal.ToString("N0");
                    tax_01.Text = _info.tax.ToString("N0");
                    total_01.Text = _info.total.ToString("N0");
                }


            }
        }
    }

    //protected void Suppliers_TextChanged(object sender, EventArgs e)
    //{
    //    string wherestr = "";
    //    #region 
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        if (Suppliers.Text != "")
    //        {
    //            cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);
    //            wherestr = " and customer_code = @customer_code";
    //        }
    //        cmd.CommandText = string.Format(@"select customer_code,customer_shortname  from tbCustomers where 0=0 {0} ", wherestr);
    //        using (DataTable dt = dbAdapter.getDataTable(cmd))
    //        {
    //            lbSuppliers.Text = dt.Rows[0]["customer_shortname"].ToString();
    //        }
    //    }
    //    #endregion
    //}

    //protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    lbSuppliers.Text = Suppliers.SelectedItem.Text;

    //}

    protected void btQry_Click(object sender, EventArgs e)
    {
       
        readdata();
    }

    protected void dlclose_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        if (dlclose_type.SelectedValue == "1")
        {
            lbdaterange.InnerText = "請款期間";
            lbclose.Visible = true ;
            dlCloseDay.Visible = true;

            string wherestr = "";
            SqlCommand cmd2 = new SqlCommand();
            if (Suppliers.SelectedValue != "")
            {
                cmd2.Parameters.AddWithValue("@supplier_no", Suppliers.SelectedValue);
                wherestr = " and supplier_no = @supplier_no";
            }
            cmd2.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd2.Parameters.AddWithValue("@datee", Convert.ToDateTime(datee.Text).AddDays(1));  //日期迄
            cmd2.CommandText = string.Format(@"Select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from ttDispatchTask with(nolock)   
                                                   Where 0=0 and task_type = 1 and checkout_close_date  >=@dates AND checkout_close_date  < @datee
                                                   {0} order by checkout_close_date", wherestr);
            dlCloseDay.DataSource = dbAdapter.getDataTable(cmd2);
            dlCloseDay.DataValueField = "checkout_close_date";
            dlCloseDay.DataTextField = "checkout_close_date";
            dlCloseDay.DataBind();
            dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));

        }
        else
        {
            lbdaterange.InnerText = "發送期間";
            lbclose.Visible = false;
            dlCloseDay.Visible = false;
        }

        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }

        if (UpdatePanel2.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel2.Update();
        }
        
        #endregion

    }

    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        if (Suppliers.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇供應商代號');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post_pay";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);        //供應商
                cmd.Parameters.AddWithValue("@close_type", "2");                          //結帳程序類別(1:自動 2:手動)
                cmd.Parameters.AddWithValue("@close_src", "B");                           //結帳內容(A：A段派遣  B:B段派遣 C:C段 D:專車 E:超商)
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {

            strErr = "acc_post_pay" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }
        return;
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = null;
        string Title = "";// Suppliers.SelectedItem.Text;     

        Title = "峻富物流股份有限公司";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable_B";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@supplier", Suppliers.SelectedValue);  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            dt = dbAdapter.getDataTable(cmd);
            _info = Utility.GetSumAccountsPayable("B", dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.SelectedValue );
        }


        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("B段應付帳款");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 30;
            sheet1.Column(6).Width = 20;




            sheet1.Cells[1, 1, 1, 6].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 6].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 6].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 6].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 6].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
            //sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
            //sheet1.Cells[3, 2, 3, 2].Value = "區配商：" + Suppliers.SelectedItem.Text;
            //sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            sheet1.Cells[4, 1, 4, 6].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 6].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 1, 4, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 2].Value = "派遣日期";
            sheet1.Cells[4, 3].Value = "任務編號";
            sheet1.Cells[4, 4].Value = "供應商";
            sheet1.Cells[4, 5].Value = "板數";
            sheet1.Cells[4, 6].Value = "發包價";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 5, 2].Value = dt.Rows[i]["task_date"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["task_date"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 3].Value = dt.Rows[i]["task_id"].ToString();
                sheet1.Cells[i + 5, 4].Value = dt.Rows[i]["supplier_name"].ToString();
                sheet1.Cells[i + 5, 5].Value = Convert.ToInt32(dt.Rows[i]["plates"]);
                sheet1.Cells[i + 5, 5].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 6].Value = Convert.ToInt32(dt.Rows[i]["price"]); 
                sheet1.Cells[i + 5, 6].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    sheet1.Cells[i + 6, 4].Value = "小計";
                    sheet1.Cells[i + 6, 5].Value = _info.plates;
                    sheet1.Cells[i + 6, 6].Value = _info.subtotal;
                    sheet1.Cells[i + 6, 6].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 6, 1, i + 6, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 6, 1, i + 6, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                    sheet1.Cells[i + 7, 4].Value = "5%營業稅";
                    sheet1.Cells[i + 7, 6].Value = _info.tax;
                    sheet1.Cells[i + 7, 6].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 7, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 7, 1, i + 7, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 7, 1, i + 7, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[i + 8, 4].Value = "應付帳款";
                    sheet1.Cells[i + 8, 6].Value = _info.total;
                    sheet1.Cells[i + 8, 6].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 8, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 8, 1, i + 8, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 8, 1, i + 8, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                }
            }



            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("B段應付帳款明細表.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    
    [WebMethod]
    public static string GetCloseDayDDLHtml(string dates, string datee, string supplier_code)
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@supplier", supplier_code);
                wherestr = " and supplier like  @supplier+'%' ";
                cmd.Parameters.AddWithValue("@dates", dates + " 00:00:00.000");  //日期起
                cmd.Parameters.AddWithValue("@datee", datee + " 23:59:59.997");  //日期迄
                cmd.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from ttDispatchTask with(nolock)   
                                                   where 0=0 AND  checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    sb_html.Append(@"<option  value=''>請選擇</option>");

                    foreach (DataRow row in dt.Rows)
                    {
                        sb_html.Append(@"<option value='" + row["checkout_close_date"].ToString() + "'>" + row["checkout_close_date"].ToString() + "</option>");
                    }

                }
            }

        }

        return sb_html.ToString();
    }
}