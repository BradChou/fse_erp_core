﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class money_2_2 : System.Web.UI.Page
{
    public DataTable  DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            string supplier_code = Session["master_code"].ToString();
            string manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            string wherestr = "";
            #region 
            using (SqlCommand cmd = new SqlCommand())
            {
                if (supplier_code != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                }
                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = 0 {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , master_code + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 order by master_code", wherestr);
                Suppliers.DataSource = dbAdapter.getDataTable(cmd);
                Suppliers.DataValueField = "master_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
            }
            #endregion


            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            }
            Suppliers_SelectedIndexChanged(null, null);

            if (Request.QueryString["second_code"] != null)
            {
                second_code.Text = Request.QueryString["second_code"];
            }

            if (Request.QueryString["dates"] != null)
            {
                dates.Text = Request.QueryString["dates"];
            }
            if (Request.QueryString["datee"] != null)
            {
                datee.Text = Request.QueryString["datee"];
            }
            lbdate.Text = dates.Text + "~" + datee.Text;

            if (Request.QueryString["business_people"] != null)
            {
                business_people.Text = Request.QueryString["business_people"];
            }

            readdata();
        }

    }

    private void readdata()
    {
        string wherestr = "";
        string qrycustomer_code = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            if (second_code.SelectedValue  != "")
            {
                qrycustomer_code = second_code.SelectedValue;
                cmd.Parameters.AddWithValue("@master_code", second_code.SelectedValue );
                wherestr = " and master_code=@master_code";
            }
            else
            {
                qrycustomer_code = Suppliers.SelectedValue;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue);
                wherestr = " and customer_code like ''+ @customer_code + '%'";
            }
            cmd.CommandText = string.Format(@"SELECT customer_name FROM tbCustomers WHERE 1 = 1 {0}", wherestr);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null &&  dt.Rows.Count >0)
                {
                    customer.Text = dt.Rows[0]["customer_name"].ToString();
                }
            }
        }
        business_people2.Text = business_people.Text;
        wherestr = "";
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@customer_code", qrycustomer_code);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                if (business_people.Text != "")
                {
                    cmd.Parameters.AddWithValue("@business_people", business_people.Text);
                    wherestr = " AND B.business_people = @business_people";
                }
                cmd.CommandText = string.Format(@"WITH business AS(
                                                  SELECT A.customer_code , B.customer_name , A.pricing_type , C.code_name ,
                                                  A.pieces , A.plates , A.cbm , B.business_people , 
                                                  Convert(varchar(100), A.checkout_close_date, 111) as checkout_close_date ,
                                                  ISNULL(_fee.supplier_fee + A.remote_fee, 0) as totalfee
                                                  from tcDeliveryRequests A With(Nolock)
                                                  LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                  LEFT JOIN tbItemCodes C With(Nolock) on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                  CROSS APPLY dbo.fu_GetShipFeeByCheckNumber(A.check_number) _fee
                                                  where  A.checkout_close_date IS NOT NULL AND A.customer_code like ''+@customer_code +'%'
                                                  AND A.checkout_close_date  >= CONVERT (DATETIME, CONVERT(varchar(100), @dates, 102), 102) AND A.checkout_close_date  <= CONVERT (DATETIME, CONVERT(varchar(100), @datee, 102) , 102)
                                                  {0})

                                                  select customer_code, customer_name, pricing_type, code_name, business_people, checkout_close_date,
                                                  CASE pricing_type WHEN '01' THEN sum(plates) WHEN '02' THEN sum(pieces) WHEN '03' THEN sum(cbm) ELSE sum(plates) END AS cnt,
                                                  sum(pieces) as pieces, sum(plates) as plates, sum(cbm) as cbm, sum(totalfee) as totalfee  from business
                                                  group by customer_code, customer_name, pricing_type, code_name, business_people, checkout_close_date", wherestr);
               
                
                
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        New_List.DataSource = dt;
                        New_List.DataBind();
                        DT = dt;
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }




    }

    protected void btQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {

        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        if (second_code.SelectedValue  != "")
        {
            querystring += "&second_code=" + second_code.SelectedValue;
        }

        if (business_people.Text != "")
        {
            querystring += "&business_people=" + business_people.Text;
        }

        
        Response.Redirect(ResolveUrl("~/money_2_2.aspx?search=yes" + querystring));
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text != "")
            {
                cmd.Parameters.AddWithValue("@master_code", Suppliers.SelectedValue);
                wherestr = " and master_code=@master_code";
            }
            cmd.CommandText = string.Format(@" SELECT second_code, customer_code, customer_shortname , second_code + '-' +  customer_shortname as showname
                                               FROM tbCustomers where 1=1 and stop_shipping_code = '0' {0} order by customer_code", wherestr);

            second_code.DataSource = dbAdapter.getDataTable(cmd);
            second_code.DataValueField = "customer_code";
            second_code.DataTextField = "showname";
            second_code.DataBind();
            second_code.Items.Insert(0, new ListItem("(全部)", ""));
        }
        #endregion
    }


    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    [WebMethod]
    public static string GetSecondCodeDDLHtml(string master_code)
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            if (master_code != "")
            {
                cmd.Parameters.AddWithValue("@master_code", master_code);
                wherestr = " and master_code=@master_code";
            }

            cmd.CommandText = string.Format(@" SELECT second_code, customer_code, customer_shortname , second_code + '-' +  customer_shortname as showname
                                               FROM tbCustomers where 1=1 {0} order by customer_code", wherestr);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                sb_html.Append(@"<option  value=''>(全部)</option>");

                foreach (DataRow row in dt.Rows)
                {
                    sb_html.Append(@"<option value='" + row["customer_code"].ToString() + "'>" + row["showname"].ToString() + "</option>");
                }

            }
        }

        return sb_html.ToString();
    }


    [WebMethod]
    public static string GetBusinessPeople(string customer_code)
    {

        string business_people = "";
        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            if (customer_code != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", customer_code);
                wherestr = " and customer_code=@customer_code";

                cmd.CommandText = string.Format(@"SELECT business_people
                                                  FROM tbCustomers
                                                  WHERE 0=0 {0}", wherestr);

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    business_people = dt.Rows[0]["business_people"].ToString();
                    
                }
            }
        }

        return business_people;
    }
    

    protected void btExport_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add( customer.Text + business_people2.Text + "出貨實績統計表");            

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 18;
            sheet1.Column(4).Width = 10;
            sheet1.Column(5).Width = 10;
            sheet1.Column(6).Width = 10;
            sheet1.Column(7).Width = 11;
            sheet1.Column(8).Width = 11;
            sheet1.Row(1).Height = 16;
            sheet1.Row(2).Height = 16;
            sheet1.Row(3).Height = 16;
            sheet1.Row(4).Height = 16;
            sheet1.Row(5).Height = 16;
            sheet1.Row(6).Height = 16;
            sheet1.Row(7).Height = 16;
            sheet1.Row(8).Height = 16;

            sheet1.Cells[1, 1, 2, 8].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 2, 8].Value = customer.Text + business_people2.Text + "出貨實績統計表";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 2, 8].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 2, 8].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 2, 8].Style.Font.Size = 20;
            sheet1.Cells[1, 1, 2, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 2, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[3, 1, 3, 8].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1, 3, 8].Style.Font.Size = 10;
            sheet1.Cells[3, 1, 3, 2].Merge = true;

            sheet1.Cells[3, 1, 3, 4].Value = business_people2.Text != "" ? "營業人員：" + business_people2.Text : "";
            sheet1.Cells[3, 1, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[3, 1, 3, 4].Merge = true;
            
            sheet1.Cells[3, 5, 3, 8].Merge = true;
            sheet1.Cells[3, 5, 3, 8].Value = "統計期間：" + lbdate.Text;
            sheet1.Cells[3, 5, 3, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            sheet1.Cells[4, 1, 7, 7].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 8].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 8].Style.Font.Bold = true;
            
            
            sheet1.Cells[4, 1, 4, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin  ; //框線
            sheet1.Cells[4, 1, 4, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;       
            sheet1.Cells[4, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;         

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 2].Value = "客戶名稱";
            sheet1.Cells[4, 3].Value = "客戶代號";
            sheet1.Cells[4, 4].Value = "計價方式";
            sheet1.Cells[4, 5].Value = "出貨板數";
            sheet1.Cells[4, 6].Value = "營業額";
            sheet1.Cells[4, 7].Value = "營業人員";
            sheet1.Cells[4, 8].Value = "結帳日";
            
           
            for (int i = 0; i < DT.Rows.Count; i++)
            {
            
                sheet1.Cells[i+ 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i+ 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i+ 5, 2].Value = DT.Rows[i]["customer_name"].ToString();
                sheet1.Cells[i+ 5, 3].Value = CustomerCodeDisplay(DT.Rows[i]["customer_code"].ToString());
                sheet1.Cells[i+ 5, 4].Value = DT.Rows[i]["code_name"].ToString();
                sheet1.Cells[i+ 5, 5].Value = Convert.ToInt32(DT.Rows[i]["cnt"]).ToString("N0");
                sheet1.Cells[i + 5, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right ;
                sheet1.Cells[i+ 5, 6].Value = Convert.ToInt32(DT.Rows[i]["totalfee"]).ToString("N0");
                sheet1.Cells[i + 5, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i+ 5, 7].Value = DT.Rows[i]["business_people"].ToString();
                sheet1.Cells[i+ 5, 8].Value =  Convert.ToDateTime(DT.Rows[i]["checkout_close_date"]).ToString("yyyy/MM/dd");
                
            }
            
            
            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText  = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=report.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}