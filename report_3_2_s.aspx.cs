﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class report_3_2_s : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            #region 配送站所
            if (Less_than_truckload == "1")
            {
                lbSuppliers.Text = "配送廠商";
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
                #region 零擔運輸商
                //using (SqlCommand cmd9 = new SqlCommand())
                //{
                //    cmd9.CommandText = "select * from tbItemCodes  where code_bclass = '7' and code_sclass = 'distributor' and active_flag=1 ";
                //    Suppliers.DataSource = dbAdapter.getDataTable(cmd9);
                //    Suppliers.DataValueField = "code_id";
                //    Suppliers.DataTextField = "code_name";
                //    Suppliers.DataBind();
                //    Suppliers.Items.Insert(0, new ListItem("全部", ""));
                //}
                #endregion
            }
            else
            {
                lbSuppliers.Text = "配送站所";
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    string manager_type = Session["manager_type"].ToString(); //管理單位類別
                    string account_code = Session["account_code"].ToString(); //使用者帳號
                    string supplier_code = Session["master_code"].ToString();
                    Suppliers.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
                    Suppliers.DataValueField = "supplier_code";
                    Suppliers.DataTextField = "showname";
                    Suppliers.DataBind();

                    if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
                    if (manager_type == "0" || manager_type == "1" || manager_type == "2") Suppliers.Items.Insert(0, new ListItem("全部", ""));

                }
            }

            #endregion

            //String yy = DateTime.Now.Year.ToString();
            //String mm = DateTime.Now.Month.ToString();
            //String days = DateTime.Now.Day.ToString();
            //mm = mm.Length < 2 ? "0" + mm : mm;
            //dates.Text = yy + "/" + mm + "/01";
            //datee.Text = yy + "/" + mm + "/" + days;

            //readdata();

        }
    }

    private void readdata()
    {

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 9999;
            cmd.Parameters.Clear();
            string wherestr1 = string.Empty;
            string wherestr2 = string.Empty;

            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            if (dates.Text != "")
            {
                cmd.Parameters.AddWithValue("@dates", dates.Text+ " 00:00:00.001");
            }

            if (datee.Text != "")
            {
                cmd.Parameters.AddWithValue("@datee", datee.Text+ " 23:59:59.999");
            }
           

            #endregion

            if (Less_than_truckload == "1")
            {
                if (Suppliers.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@Distributor", Suppliers.SelectedValue);
                    wherestr1 += " AND B.Distributor = @Distributor ";
                    wherestr2 += " and A.Distributor = @Distributor ";
                }

                cmd.CommandText = string.Format(@"DECLARE @tbl table
                                                (order_number nvarchar(50), ISscan bit )
                                                INSERT INTO @tbl
	                                            SELECT  distinct B.order_number , 1   
                                                from  ttDeliveryScanLog A With(Nolock) 
	                                            INNER JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number AND ISNULL(B.order_number,'') <> ''
	                                            WHERE  A.scan_item  IN('2','3') and B.print_date  >=@dates and  B.print_date <= @datee
                                                and B.Less_than_truckload =  1 
                                                {0}

                                                DECLARE @tb2 table 
	                                            (check_number nvarchar(20), scan_date datetime, rn int)
                                                INSERT INTO @tb2
	                                                SELECT distinct A.check_number, A.scan_date ,
	                                                ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number, A.scan_date DESC ) AS rn
	                                                from  ttDeliveryScanLog A With(Nolock) 
	                                                WHERE  A.scan_item IN('2','3') and  A.scan_date >=@dates

                                                Select area_arrive_code, area_arrive_station, COUNT(check_number ) as totcnt,  
                                                    Sum(CASE  WHEN scan_date IS NOT NULL THEN 1 ELSE ISNULL(ISscan,0) END) as yescnt,
	                                                Sum(CASE  WHEN scan_date IS NULL AND ISscan IS NULL   THEN 1 ELSE 0 END) as noncnt 
                                                    from  (select ROW_NUMBER() OVER (PARTITION BY A.order_number ORDER BY A.order_number, B.scan_date desc  ) AS rn,
																A.check_number,A.area_arrive_code, A.area_arrive_code  'area_arrive_station' ,F.scan_date,G.ISscan
																from tcDeliveryRequests A with(nolock)
																INNER JOIN ttDeliveryScanLog B with(nolock) on A.check_number = B.check_number and B.scan_Item= '1'                                                    
																LEFT JOIN @tb2 F on A.check_number  =  F.check_number and F.rn= 1
																LEFT JOIN @tbl G on A.order_number = G.order_number 
															where  A.cancel_date IS NULL
                                                            and A.print_date  >=@dates and  A.print_date <= @datee
                                                            and A.Less_than_truckload =@Less_than_truckload
                                                            {1}) a
													where rn = 1 
                                                group by  area_arrive_code, area_arrive_station
                                                order by  area_arrive_code, area_arrive_station", wherestr1, wherestr2);
            }
            else
            {
                //string manager_type = Session["manager_type"].ToString(); //管理單位類別  
                //string customer_code = Session["master_code"].ToString();

                //switch (manager_type)
                //{
                //    case "0":
                //    case "1":
                //    case "2":
                //        customer_code = "";
                //        break;
                //    default:
                //        customer_code = customer_code.Substring(0, 3);
                //        break;
                //}
                //cmd.Parameters.AddWithValue("@customer_code", customer_code);
                cmd.Parameters.AddWithValue("@Supplier_code", Suppliers.SelectedValue);
                cmd.CommandText = string.Format(@"DECLARE @tbl table
                                                (order_number nvarchar(50), ISscan bit )
                                                INSERT INTO @tbl
	                                            SELECT  distinct B.order_number , 1   
                                                from  ttDeliveryScanLog A With(Nolock) 
	                                            INNER JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number AND ISNULL(B.order_number,'') <> ''
	                                            WHERE  A.scan_item  IN('2','3') and B.print_date  >=@dates and  B.print_date <= @datee
                                                {0}

                                                DECLARE @tb2 table 
	                                            (check_number nvarchar(20), scan_date datetime, rn int)
                                                INSERT INTO @tb2
	                                                SELECT distinct A.check_number, A.scan_date ,
	                                                ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number, A.scan_date DESC ) AS rn
	                                                from  ttDeliveryScanLog A With(Nolock) 
	                                                WHERE  A.scan_item IN('2','3') and  A.scan_date >=@dates

                                                Select A.area_arrive_code, E.supplier_name 'area_arrive_station', COUNT(A.check_number ) as totcnt,  
                                                    Sum(CASE  WHEN F.scan_date IS NOT NULL THEN 1 ELSE ISNULL(G.ISscan,0) END) as yescnt,
	                                                Sum(CASE  WHEN F.scan_date IS NULL AND G.ISscan IS NULL   THEN 1 ELSE 0 END) as noncnt 
                                                    from  tcDeliveryRequests A with(nolock)
                                                    INNER JOIN ttDeliveryScanLog B with(nolock) on A.check_number = B.check_number and B.scan_Item= '1'
                                                    LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                    LEFT JOIN @tb2 F on A.check_number  =  F.check_number and F.rn= 1
	                                                LEFT JOIN @tbl G on A.order_number = G.order_number 
                                                where  A.cancel_date IS NULL 
                                                and A.print_date  >=@dates and  A.print_date <= @datee
                                                and A.Less_than_truckload =@Less_than_truckload
                                                {1}
                                                group by  A.area_arrive_code, E.supplier_name
                                                order by  A.area_arrive_code ", wherestr1, wherestr2);
            }
            

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                if (dt.Rows.Count > 0)
                {
                    double[] Tot = { 0, 0, 0  };
                    dt.Columns.Add(new DataColumn("noRate", typeof(String)));     //異常率               
                    dt.Columns.Add(new DataColumn("yesRate", typeof(String)));    //到著率 

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        string supplier_code = dt.Rows[i]["area_arrive_code"].ToString();
                        double should = dt.Rows[i]["totcnt"] != DBNull.Value ? (int)dt.Rows[i]["totcnt"] : 0;
                        Tot[0] += should;
                        double yes = dt.Rows[i]["yescnt"] != DBNull.Value ? (int)dt.Rows[i]["yescnt"] : 0;
                        Tot[1] += yes;
                        double noon = dt.Rows[i]["noncnt"] != DBNull.Value ? (int)dt.Rows[i]["noncnt"] : 0;
                        Tot[2] += noon;
                        dt.Rows[i]["yesRate"] = ((yes / should) * 100).ToString("N2");
                        dt.Rows[i]["noRate"] = ((noon / should) * 100).ToString("N2");
                    }

                    DataRow row = dt.NewRow();
                    if (Suppliers.SelectedValue != "")
                    {
                        row["area_arrive_code"] = Suppliers.SelectedValue;
                    }
                    else
                    {
                        row["area_arrive_code"] = "";
                    }
                    
                    row["area_arrive_station"] = "總計";
                    row["totcnt"] = Tot[0];
                    row["yescnt"] = Tot[1];
                    row["noncnt"] = Tot[2];
                    row["yesRate"] = ((Tot[1] / Tot[0]) * 100).ToString("N2");
                    row["noRate"] = ((Tot[2] / Tot[0]) * 100).ToString("N2");
                    dt.Rows.Add(row);

                }
                New_List.DataSource = dt;
                New_List.DataBind();
                DT = dt;
            }

        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        //paramlocation();
        readdata();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/report_3_1_s.aspx?search=yes" + querystring));
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("配達統計");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 15;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 15;
            sheet1.Column(6).Width = 15;
            sheet1.Column(7).Width = 15;
            sheet1.Column(8).Width = 15;
            sheet1.Column(9).Width = 15;
            sheet1.Column(10).Width = 15;
            sheet1.Column(11).Width = 15;
            sheet1.Column(12).Width = 15;
            sheet1.Column(13).Width = 15;
            sheet1.Column(14).Width = 15;


            sheet1.Cells[1, 1, 1, 14].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 14].Value = "配達統計";    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 14].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 14].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 14].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 14].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 14].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "發送日期：" + dates.Text + "~" + datee.Text;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

            sheet1.Cells[3, 1,3, 14].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1,3, 14].Style.Font.Size = 12;
            sheet1.Cells[3, 1,3, 14].Style.Font.Bold = true;
            sheet1.Cells[3, 1,3, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 14].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[3, 1, 3, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[3, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                   
            sheet1.Cells[3, 1].Value = "NO.";
            sheet1.Cells[3, 2].Value = "配送站所";
            sheet1.Cells[3, 3].Value = "應配筆數";
            sheet1.Cells[3, 4].Value = "配送筆數";
            sheet1.Cells[3, 5].Value = "午前配達筆數";
            sheet1.Cells[3, 6].Value = "午前配達率";
            sheet1.Cells[3, 7].Value = "當日配達筆數";
            sheet1.Cells[3, 8].Value = "當日配達率";
            sheet1.Cells[3, 9].Value = "已配達筆數";
            sheet1.Cells[3, 10].Value = "配達率";
            sheet1.Cells[3, 11].Value = "未配達筆數";
            sheet1.Cells[3, 12].Value = "異常率";
            sheet1.Cells[3, 13].Value = "首筆配達";
            sheet1.Cells[3, 14].Value = "末筆配達";
           

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 4, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 4, 2].Value = DT.Rows[i]["area_arrive_name"].ToString();
                sheet1.Cells[i + 4, 3].Value = DT.Rows[i]["應配筆數"].ToString();
                sheet1.Cells[i + 4, 4].Value = DT.Rows[i]["配送筆數"].ToString();
                sheet1.Cells[i + 4, 5].Value = DT.Rows[i]["午前配達筆數"].ToString();
                sheet1.Cells[i + 4, 6].Value = DT.Rows[i]["noonRate"].ToString();
                sheet1.Cells[i + 4, 7].Value = DT.Rows[i]["當日配達筆數"].ToString();
                sheet1.Cells[i + 4, 8].Value = DT.Rows[i]["dayRate"].ToString();
                sheet1.Cells[i + 4, 9].Value = DT.Rows[i]["已配達筆數"].ToString();
                sheet1.Cells[i + 4, 10].Value = DT.Rows[i]["yesRate"].ToString();
                sheet1.Cells[i + 4, 11].Value = DT.Rows[i]["noArrive"].ToString();
                sheet1.Cells[i + 4, 12].Value = DT.Rows[i]["noRate"].ToString();
                sheet1.Cells[i + 4, 13].Value = DT.Rows[i]["MIN"].ToString();
                sheet1.Cells[i + 4, 14].Value = DT.Rows[i]["MAX"].ToString();
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=report.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}