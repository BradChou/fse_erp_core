﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_2_8.aspx.cs" Inherits="money_2_8" EnableEventValidation="false"
      %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">報表匯出</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label id="lbdaterange" runat="server" class="_daterange" >請款期間</label>
                            <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate" autocomplete="off"></asp:TextBox>
                            ~ 
                            <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate" autocomplete="off"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label for="Suppliers">供應商</label>
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                            <asp:Button ID="btExport" runat="server" class="btn btn-primary" Text="匯出" OnClick="btExport_Click" />                            
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Suppliers" EventName="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btExport" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <hr>
                
            </div>
        </div>
    </div>
</asp:Content>

