﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// 通用Exception處理
/// </summary>
public class ExceptionAdapter
{
	/// <summary>
	/// 共用程式Exception的Log
	/// </summary>
	/// <param name="ex"></param>
	public static void logSysException(Exception e)
	{
		try
		{
			insExceptionToDB("SysEx", e.Message, "堆疊：" + e.StackTrace);
		}
		catch (Exception) { }
	}


    /// <summary>
    /// 儲存LOG至DB
    /// </summary>
    /// <param name="strLevel"></param>
    /// <param name="strMsg"></param>
    /// <param name="strException"></param>
    private static void insExceptionToDB(string strLevel, string strMsg, string strException)
    {
        try
        {
            string strLog = string.Empty;
            strLog += string.Format("{0}\t{1}\n", "時間", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"));
            strLog += string.Format("{0}\t{1}\n", "層級", strLevel);
            strLog += string.Format("{0}\t{1}\n", "訊息", strMsg);
            strLog += string.Format("{0}\t{1}\n", "例外", strException);
            strLog += string.Format("\n");

            TxtAdapter.writeToFile(strLog, BasePage.pathErrorLog + DateTime.Now.ToString("yyyyMMddHH"), true);
        }
        catch (Exception)
        {
            //不作處理，避免再發生錯誤會產生無限迴圈
        }
    }


    /// <summary>
    /// 回傳要Alert的錯誤訊息
    /// </summary>
    /// <param name="e"></param>
    public static string getAlertMsg(Exception e)
    {
        string strReturn = string.Empty;
        try
        {
            strReturn += e.Message + "\n";

            #region 替換錯誤顯示訊息
            if (strReturn.IndexOf("字串或二進位資料會被截斷") > -1) strReturn = "字串長度超過限制。\n";
            #endregion

        }
        catch (Exception ex)
        {
            throw ex;
        }

        return strReturn;
    }
}