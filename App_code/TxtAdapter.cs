﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;

/// <summary>
/// TxtAdapter 的摘要描述
/// </summary>
public class TxtAdapter
{
	/// <summary>
	/// 文字寫入文字檔
	/// </summary>
	/// <param name="strContain"></param>
	/// <param name="strFullFileName">TXT完整路徑，不指定時存在~/Temp/yyyyMMddHHmmssfff.txt</param>
	/// <param name="append"></param>
	public static void writeToFile(string strContain, string strFullFileName = "", bool append = false)
	{
		try
		{
			if (strFullFileName == "")
			{
				strFullFileName = BasePage.pathTempLog + DateTime.Now.ToString("yyyyMMddHHmmssfff");
			}
			if (strFullFileName.EndsWith(".txt") == false)
			{
				strFullFileName += ".txt";
			}

			using (StreamWriter swLog = new StreamWriter(strFullFileName, append))
			{
				swLog.WriteLine(strContain);
				swLog.Close();
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}
}