﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// uploadFile 的摘要描述
/// </summary>
public class uploadFile
{
    public uploadFile()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }
    public static void UploadFtpDirectory(string uploadurl, NetworkCredential credentials, string localPath)
    //recursiveDirectory(string dirPath, string uploadPath)
    {
        string[] files = System.IO.Directory.GetFiles(localPath, "*.*");

        foreach (string file in files)
        {
            //ftpClient.upload(uploadPath + "/" + Path.GetFileName(file), file);

            FtpWebRequest requestFTPUploader = (FtpWebRequest)WebRequest.Create(uploadurl + Path.GetFileName(file));
            requestFTPUploader.Credentials = credentials;
            requestFTPUploader.Method = WebRequestMethods.Ftp.UploadFile;

            FileInfo fileInfo = new FileInfo(file);
            FileStream fs = fileInfo.OpenRead();

            byte[] buffer = new byte[fs.Length];

            Stream ftpstream = requestFTPUploader.GetRequestStream();
            ftpstream.Write(buffer, 0, buffer.Length);
            ftpstream.Close();
            requestFTPUploader = null;
        }
    }

    /// <summary>
    /// zip檔案
    /// </summary>
    /// <param name="uploadurl"></param>
    /// <param name="credentials"></param>
    /// <param name="filename"></param>
    public static void UploadFtpFile(string uploadurl, NetworkCredential credentials, string filename)
    {
        FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uploadurl + Path.GetFileName(filename));
        request.Method = WebRequestMethods.Ftp.UploadFile;

        // This example assumes the FTP site uses anonymous logon.  
        request.Credentials = credentials;

        // Copy the contents of the file to the request stream.  
        byte[] fileContents = File.ReadAllBytes(filename);
        request.ContentLength = fileContents.Length;

        Stream requestStream = request.GetRequestStream();
        requestStream.Write(fileContents, 0, fileContents.Length);
        requestStream.Close();

        FtpWebResponse response = (FtpWebResponse)request.GetResponse();

        //Console.WriteLine("Upload File Complete, status {0}", response.StatusDescription);
        //using (System.IO.StreamWriter file =
        //new System.IO.StreamWriter(@"E:\WEB\mail\HCTftp\HCTftp_Photo\log\HCTftp_Photo_Log" + DateTime.Now.ToString("yyyy-MM-dd hhmmss") + ".TXT"))
        //{
        //    file.WriteLine("{0} Upload File Complete, status {1}", filename,response.StatusDescription);
        //}

        response.Close();
    }
}