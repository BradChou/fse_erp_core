﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Security.Cryptography;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;

/// <summary>
/// Utility 的摘要描述
/// </summary>
public class Utility
{
    public Utility()
    {
    }

    /// <summary>
    /// 將 dt 的資料代進去 ddlTmp
    /// </summary>
    /// <param name="ddlTmp"></param>
    /// <param name="dt">資料來源</param>
    /// <param name="DataValueField">資料實際值</param>
    /// <param name="DataTextField">資料顯示值</param>
    /// <param name="FirstItemEmpty">下拉第一個選項代入空的 ListItem</param>
    /// <param name="FirstItemStr">下拉第一個選項顯示字串</param>

    //西元轉民國年
    public static string getCDate(string strdate)
    {
        if (strdate != "")
        {
            string datenow = "";
            DateTime dt = Convert.ToDateTime(strdate);
            System.Globalization.TaiwanCalendar TC = new System.Globalization.TaiwanCalendar();
            System.Globalization.TaiwanLunisolarCalendar TA = new System.Globalization.TaiwanLunisolarCalendar();
            datenow = string.Format("{0}.{1}.{2}", TC.GetYear(dt), TC.GetMonth(dt).ToString().Length == 1 ? "0" + TC.GetMonth(dt).ToString() : TC.GetMonth(dt).ToString(), TC.GetDayOfMonth(dt).ToString().Length == 1 ? "0" + TC.GetDayOfMonth(dt).ToString() : TC.GetDayOfMonth(dt).ToString());
            return datenow;
        }
        else
        {
            return "";
        }
    }

    //民國轉西元年
    public static string getEDate(string strdate)
    {
        if (strdate != "")
        {
            string Y;
            string M;
            string D;
            string YMD;
            string[] datenowarray = strdate.Split('-');
            Y = (Convert.ToInt32(datenowarray[0]) + Convert.ToInt32(1911)).ToString();
            M = Convert.ToInt32(datenowarray[1]).ToString().Length == 1 ? "0" + Convert.ToInt32(datenowarray[1]).ToString() : Convert.ToInt32(datenowarray[1]).ToString();
            D = Convert.ToInt32(datenowarray[2]).ToString().Length == 1 ? "0" + Convert.ToInt32(datenowarray[2]).ToString() : Convert.ToInt32(datenowarray[2]).ToString();
            YMD = Y + "-" + M + "-" + D;
            return YMD;
        }
        else
        {
            return "";
        }
    }


    //判斷是否為假日
    public static bool IsHolidays(DateTime date)
    {
        #region 改抓行事曆，因為有可能星期六日要上班
        //// 週休二日
        //if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
        //{
        //    return true;
        //}  
        #endregion

        // 國定假日(國曆)
        if (date.ToString("MM/dd").Equals("01/01"))
        {
            return true;
        }
        if (date.ToString("MM/dd").Equals("02/28"))
        {
            return true;
        }
        if (date.ToString("MM/dd").Equals("04/04"))
        {
            return true;
        }
        if (date.ToString("MM/dd").Equals("04/05"))
        {
            return true;
        }
        if (date.ToString("MM/dd").Equals("05/01"))
        {
            return true;
        }
        if (date.ToString("MM/dd").Equals("10/10"))
        {
            return true;
        }

        // 國定假日(農曆)
        if (GetLeapDate(date, false).Equals("12/" + GetDaysInLeapMonth(date)))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("1/1"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("1/2"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("1/3"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("1/4"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("1/5"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("5/5"))
        {
            return true;
        }
        if (GetLeapDate(date, false).Equals("8/15"))
        {
            return true;
        }


        //// 公司假日
        ////日期是否在特定節日，資料庫有資料則為假日
        if (CheckHoliday(date))
        {
            return true;
        }
        ////日期是否在公司設定節日，資料庫有資料則為假日
        //if (CheckDeductionSh(date))
        //{
        //    return true;
        //}

        return false;
    }


    ///<summary>
    /// 取得農曆日期
    ///</summary>
    public static string GetLeapDate(DateTime Date, bool bShowYear = true, bool bShowMonth = true)
    {
        int L_ly, nLeapYear, nLeapMonth;

        ChineseLunisolarCalendar MyChineseLunisolarCalendar = new ChineseLunisolarCalendar();

        nLeapYear = MyChineseLunisolarCalendar.GetYear(Date);
        nLeapMonth = MyChineseLunisolarCalendar.GetMonth(Date);
        if (MyChineseLunisolarCalendar.IsLeapYear(nLeapYear)) //判斷此農曆年是否為閏年
        {
            L_ly = MyChineseLunisolarCalendar.GetLeapMonth(nLeapYear); //抓出此閏年閏何月

            if (nLeapMonth >= L_ly)
            {
                nLeapMonth--;
            }
        }
        else
        {
            nLeapMonth = MyChineseLunisolarCalendar.GetMonth(Date);
        }

        if (bShowYear)
        {
            return "" + MyChineseLunisolarCalendar.GetYear(Date) + "/" + nLeapMonth + "/" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
        }
        else if (bShowMonth)
        {
            return "" + nLeapMonth + "/" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
        }
        else
        {
            return "" + MyChineseLunisolarCalendar.GetDayOfMonth(Date);
        }
    }

    ///<summary>
    /// 取得某一日期的該農曆月份的總天數
    ///</summary>
    public static string GetDaysInLeapMonth(DateTime date)
    {
        ChineseLunisolarCalendar MyChineseLunisolarCalendar = new ChineseLunisolarCalendar();

        return "" + MyChineseLunisolarCalendar.GetDaysInMonth(MyChineseLunisolarCalendar.GetYear(date), date.Month);
    }


    /// <summary>
    /// 判斷日期是否為公司假日
    /// </summary>
    /// <param name="date"></param>
    /// <returns></returns>
    public static bool CheckHoliday(DateTime date)
    {
        bool RetVal = false;
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        cmd.CommandText = string.Format("SELECT CASE KindOfDay WHEN 'WORKDAY' THEN 0 ELSE 1 END  AS HOLIDAY  FROM tbCalendar where Date =  @Date");
        cmd.Parameters.AddWithValue("@Date", date);
        dt = dbAdapter.getDataTable(cmd);
        if (dt != null && dt.Rows.Count > 0)
        {
            RetVal = Convert.ToBoolean(dt.Rows[0]["HOLIDAY"]);
        }
        return RetVal;

    }

    public static DataTable getCntTwnDT(string strQryCntids)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        string strWhereCmd = "";
        if (strQryCntids != "")
        {
            string[] strArr = strQryCntids.Replace("'", "").Split(',');
            strQryCntids = "";
            for (int i = 0; i < strArr.Length; i++)
            {
                cmd.Parameters.AddWithValue("@CNTID" + i.ToString().Trim(), strArr[i]);
                if (strQryCntids != "")
                    strQryCntids += ",";
                strQryCntids += "@CNTID" + i.ToString().Trim();
            }

            strWhereCmd += string.Format(" and CNTID in ({0})", strQryCntids);
        }
        try
        {
            cmd.CommandText = string.Format(@"
                            select *
                            from TOWN
                            where 1=1 {0}
                            order by TWNID", strWhereCmd);

            dt = dbAdapter.getDataTable(cmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dt;
    }
    public static string GetIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string sIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (string.IsNullOrEmpty(sIPAddress))
        {
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        else
        {
            string[] ipArray = sIPAddress.Split(new Char[] { ',' });
            return ipArray[0];
        }
    }

    public static int getAsciiCode(string MyString)
    {
        if (MyString.Length == 0)
            return 0;
        else if (MyString.Length > 1)
            MyString = MyString[0].ToString();

        int AsciiCodeO = (int)System.Convert.ToChar(MyString);
        byte[] AsciiCodeB = System.Text.Encoding.ASCII.GetBytes(MyString);
        return AsciiCodeO;
    }

    public static int getremote_fee(string city, string area, string address)
    {
        int remote_fee = 0;
        string ttAddress = address;
        string ttcontent = "";   //村、里、地方
        string ttroad = "";      //路
        int ttno = 0;            //號
        //string ttnumber_type = "0";  //單雙(0:不分 1:單 2:雙)
        //int ttstart_no = 0;          //起始號碼
        //int ttend_no = 0;            //起始號碼
        int ttLen = 0;
        int ttIdx = -1;

        try
        {
            SqlCommand cmd = new SqlCommand();
            string wherestr = string.Empty;
            DataTable DT = null;
            cmd.Parameters.AddWithValue("@post_city", city);
            cmd.Parameters.AddWithValue("@post_area", area);
            cmd.Parameters.AddWithValue("@area_content", address);
            wherestr += " AND post_city=@post_city AND post_area=@post_area AND area_content =@area_content";
            cmd.CommandText = string.Format(@"Select * from  tbRemoteSections with(nolock) where 1=1 {0}", wherestr);
            DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {
                remote_fee = Convert.ToInt32(DT.Rows[0]["append_price"]);
            }
            else
            {
                wherestr = " AND post_city=@post_city AND post_area=@post_area";
                cmd.Parameters.Remove(cmd.Parameters["@area_content"]);
                //地址先拆村里 村里前為村里字串(視同地名);
                string[] village = { "村", "里" };
                for (int i = 0; i <= village.Length - 1; i++)
                {
                    ttIdx = address.IndexOf(village[i]);
                    if (ttIdx > -1)
                    {
                        ttLen = ttIdx + 1;
                        ttcontent = ttAddress.Substring(0, ttLen);
                        ttAddress = ttAddress.Length > ttLen ? ttAddress.Substring(ttIdx + 1) : "";
                        break;
                    }
                }
                ttIdx = -1;
                //地址拆路名，街、路、段遇到第1個數字之前的字串當路名，第一組數字當號碼，去查詢是否有相符的資料。

                ttIdx = -1;
                ttIdx = ttAddress.IndexOf("街");
                if (ttIdx > -1)
                {
                    ttLen = ttIdx + 1;
                    ttroad = ttAddress.Substring(0, ttLen);
                    ttAddress = ttAddress.Length > ttLen ? ttAddress.Substring(ttIdx + 1) : "";
                }
                else
                {
                    ttIdx = ttAddress.IndexOf("段");
                    if (ttIdx > -1)
                    {
                        ttLen = ttIdx + 1;
                        ttroad = ttAddress.Substring(0, ttLen);
                        ttAddress = ttAddress.Length > ttLen ? ttAddress.Substring(ttIdx + 1) : "";
                    }
                    else
                    {
                        ttIdx = ttAddress.IndexOf("路");
                        if (ttIdx > -1)
                        {
                            ttLen = ttIdx + 1;
                            ttroad = ttAddress.Substring(0, ttLen);
                            ttAddress = ttAddress.Length > ttLen ? ttAddress.Substring(ttIdx + 1) : "";
                        }
                    }
                }

                Regex regex = new Regex(@"(\d)");   //判斷是否為數字
                ttIdx = regex.Match(ttAddress).Index;
                bool success = regex.Match(ttAddress).Success;
                if (success && ttIdx > -1)
                {
                    int ttIdxe = ttIdx;
                    for (int i = ttIdx; i <= ttAddress.Length - 1; i++)
                    {
                        if (regex.IsMatch(ttAddress.Substring(i, 1)))
                        {
                            ttIdxe = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (IsNumeric(ttAddress.Substring(ttIdx, ttIdxe - ttIdx + 1)))
                    {
                        ttno = Convert.ToInt32(ttAddress.Substring(ttIdx, ttIdxe - ttIdx + 1));
                    }

                    //ttroad = ttAddress.Substring(0, ttIdx);
                    //ttAddress = ttAddress.Length > ttIdx ? ttAddress.Substring(ttIdx) : "";
                    //string[] no = ttAddress.Split('號');
                    //if (IsNumeric(no[0]))
                    //{
                    //    ttno = Convert.ToInt32(no[0]);
                    //}
                }
                else
                {
                    //查無幾號則視同地名
                    ttcontent = ttAddress;   //(視同地名)
                }

                if (ttroad != "")
                {
                    cmd.Parameters.AddWithValue("@road", ttroad);
                    wherestr += " AND road=@road";
                    if (ttno != 0)
                    {
                        cmd.Parameters.AddWithValue("@start_no", ttno);
                        cmd.Parameters.AddWithValue("@end_no", ttno);
                        wherestr += " AND @start_no>=start_no AND @end_no<=end_no";
                        switch (ttno % 2)
                        {
                            case 0:
                                cmd.Parameters.AddWithValue("@number_type", "2");  //0:不分 1:單 2:雙
                                break;
                            case 1:
                                cmd.Parameters.AddWithValue("@number_type", "1");
                                break;
                        }
                        wherestr += " AND number_type=@number_type";
                        cmd.CommandText = string.Format(@"Select * from  tbRemoteSections with(nolock) where 1=1 {0}", wherestr);
                        DT = dbAdapter.getDataTable(cmd);
                        if (DT.Rows.Count > 0)
                        {
                            remote_fee = Convert.ToInt32(DT.Rows[0]["append_price"]);
                        }
                        else
                        {
                            cmd.Parameters.Remove(cmd.Parameters["@number_type"]);
                            cmd.Parameters.AddWithValue("@number_type", "0");
                            cmd.CommandText = string.Format(@"Select * from  tbRemoteSections with(nolock) where 1=1 {0}", wherestr);
                            DT = dbAdapter.getDataTable(cmd);
                            if (DT.Rows.Count > 0)
                            {
                                remote_fee = Convert.ToInt32(DT.Rows[0]["append_price"]);
                            }
                        }
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@area_content", ttcontent);
                    DT = dbAdapter.getDataTable(cmd);
                    if (DT.Rows.Count > 0)
                    {
                        remote_fee = Convert.ToInt32(DT.Rows[0]["append_price"]);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            strErr = "Utility-getremote_fee-偏遠計價：" + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }



        return remote_fee;
    }


    /// <summary>
    /// 取得區配商(依客戶類型)
    /// </summary>
    /// <param name="supplier_code"></param>
    /// <param name="manager_type"></param>
    /// <returns></returns>
    public static DataTable getSupplierDT(string supplier_code, string manager_type)
    {
        string wherestr = "";
        DataTable dt = new DataTable();
        supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
        if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
        {
            switch (manager_type)
            {
                case "1":
                case "2":
                    supplier_code = "";
                    break;
                default:
                    supplier_code = "000";
                    break;
            }
        }


        #region  區配商
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }
            //2019/10/30 加入零擔區配商不顯示
            //cmd.CommandText = string.Format(@"Select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers with(nolock)  where active_flag = 1 and ISNULL(supplier_code ,'') <> ''  and [SupplierType]<> 1 {0} order by supplier_code", wherestr);
            cmd.CommandText = string.Format(@"Select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers with(nolock)  where active_flag = 1 and ISNULL(supplier_code ,'') <> ''  {0} order by supplier_code", wherestr);
            dt = dbAdapter.getDataTable(cmd);
        }
        #endregion
        return dt;
    }

    /// <summary>
    /// 供應商
    /// </summary>
    /// <param name="supplier_code"></param>
    /// <param name="manager_type"></param>
    /// <returns></returns>
    public static DataTable getSupplierDT2(string supplier_code, string manager_type)
    {
        string wherestr = "";
        DataTable dt = new DataTable();
        supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
        if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
        {
            switch (manager_type)
            {
                case "1":
                case "2":
                    supplier_code = "";
                    break;
                default:
                    supplier_code = "000";
                    break;
            }
        }


        #region  區配商
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }

            cmd.CommandText = string.Format(@"Select supplier_id, supplier_no,supplier_no + '-' +supplier_name as showname  
                                                from tbSuppliers with(nolock)  
                                                where active_flag = 1 and ISNULL(supplier_no ,'') <> ''  {0} 
                                                order by supplier_no", wherestr);
            dt = dbAdapter.getDataTable(cmd);
        }
        #endregion
        return dt;
    }

    /// <summary>
    /// 取得客戶
    /// </summary>
    /// <param name="supplier_code">區配商代碼</param>
    /// <param name="Master_code">客戶代碼前7碼</param>
    /// <param name="manager_type">客戶類型</param>
    /// <returns></returns>
    public static DataTable getCustomerDT(string supplier_code, string Master_code)
    {
        string wherestr = "";
        DataTable dt = new DataTable();


        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@Suppliers", supplier_code);
                wherestr = "  and supplier_code=@Suppliers";
            }
            if (Master_code != "")
            {
                cmd.Parameters.AddWithValue("@Master_code", Master_code);
                wherestr = "  and customer_code like ''+ @Master_code + '%'";
            }

            cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = '0'  {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname , master_code + '-' +  customer_name as showname2
                                            FROM cus
                                            WHERE rn = 1 order by master_code", wherestr);
            dt = dbAdapter.getDataTable(cmd);
        }


        #endregion
        return dt;
    }

    public static DataTable getAuthDT(string account_code, string funcId, string sirId = "", string type = "0")
    {
        string wherestr = string.Empty;
        string wherestr2 = string.Empty;
        DataTable dt = new DataTable();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@account_code", account_code);
            cmd.Parameters.AddWithValue("@type", type);
            if (funcId != "")
            {
                cmd.Parameters.AddWithValue("@funcId", funcId);
                wherestr += "and func_code like '' + @funcId + '%'";
                wherestr2 += "and func.func_code like '' + @funcId + '%'";
            }
            if (sirId != "")
            {
                cmd.Parameters.AddWithValue("@upper_level_code", sirId);
                //wherestr += "and upper_level_code =@upper_level_code";
                wherestr2 += "and func.upper_level_code =@upper_level_code and func.func_id not in (25,28)";
                //and (auth.view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 )
            }

            cmd.CommandText = string.Format(@"declare @@count int;
                                                  declare @@manager_type nvarchar(2);
                                                  select @@count =count(*) from tbFunctionsAuth where account_code  = @account_code {0};
                                                  IF (@@count =0) BEGIN
                                                  select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                  select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type and auth.func_type =@type
                                                   where 0=0 and func.type=@type  {1}
                                                   order by func.sort ,func.level
                                                  END ELSE BEGIN 
                                                   select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.account_code  = @account_code and auth.func_type =@type
                                                   where 0=0 and func.type=@type {2}
                                                   order by func.sort,func.level 
                                                  END; ", wherestr, wherestr2, wherestr2);

            dt = dbAdapter.getDataTable(cmd);
        }
        return dt;
    }

    public static DataTable getTopMenu(string account_code, string type)
    {
        string wherestr = string.Empty;
        string wherestr2 = string.Empty;
        DataTable dt = new DataTable();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@account_code", account_code);
            cmd.Parameters.AddWithValue("@type", type);

            //cmd.CommandText = string.Format(@"declare @@count int;
            //                                      declare @@manager_type nvarchar(2);
            //                                      select @@count =count(*) from tbFunctionsAuth where  (view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) 
            //                                      and account_code  = @account_code ;
            //                                      IF (@@count =0) BEGIN
            //                                          select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
            //                                          Select distinct (topfunc.top_func_code)
            //                                          from tbFunctions func
            //                                          left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type
            //                                          CROSS APPLY dbo.fu_GetTop_func_code(func.func_code) topfunc 
            //                                          where 0=0 and (auth.view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) 
            //                                      END ELSE BEGIN 
            //                                          select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
            //                                          Select distinct (topfunc.top_func_code)
            //                                          from tbFunctions func
            //                                          left join tbFunctionsAuth auth on auth.func_code = func.func_code and auth.account_code  = @account_code
            //                                          CROSS APPLY dbo.fu_GetTop_func_code(func.func_code) topfunc 
            //                                          where 0=0 and (auth.view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) 
            //                                      END; ");
            cmd.CommandText = string.Format(@"declare @@count int;
                                                  declare @@manager_type nvarchar(2);
                                                  select @@count =count(*) from tbFunctionsAuth where  (view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) 
                                                  and account_code  = @account_code ;
                                                  IF (@@count =0) BEGIN
                                                      select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                      Select * from (
                                                      Select topfunc.top_func_code, func.func_link ,  ROW_NUMBER() OVER (PARTITION BY topfunc.top_func_code ORDER BY func.sort ) as no
                                                      from tbFunctions func
                                                      left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type and auth.func_type =@type 
                                                      CROSS APPLY dbo.fu_GetTop_func_code(func.func_code) topfunc 
                                                      where type = @type and (auth.view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) )  topmenu where no = 1 
                                                  END ELSE BEGIN 
                                                      select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                      Select * from (
                                                      Select topfunc.top_func_code, func.func_link ,  ROW_NUMBER() OVER (PARTITION BY topfunc.top_func_code ORDER BY func.sort ) as no
                                                      from tbFunctions func
                                                      left join tbFunctionsAuth auth on auth.func_code = func.func_code and auth.account_code  = @account_code and auth.func_type =@type
                                                      CROSS APPLY dbo.fu_GetTop_func_code(func.func_code) topfunc 
                                                      where func_id not in(25,28) and type = @type and (auth.view_auth =1 or insert_auth= 1 or modify_auth = 1 or  delete_auth = 1 ) )  topmenu where no = 1 
                                                  END; ");
            dt = dbAdapter.getDataTable(cmd);
        }
        return dt;

    }




    //public static  SortedList getAuth(string account_code, string funcId)
    //{
    //    //SortedList Level_1s = new SortedList();
    //    //SortedList Level_2s = new SortedList();
    //    //SortedList Level_3s = new SortedList();
    //    int ttIdx = -1;
    //    SortedList List = new SortedList();
    //    string wherestr = string.Empty;
    //    string wherestr2 = string.Empty;
    //    DataTable dt = new DataTable();
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.Parameters.AddWithValue("@account_code", account_code);
    //        if (funcId != "")
    //        {
    //            cmd.Parameters.AddWithValue("@funcId", funcId);
    //            wherestr = "and func_code like '%' + @funcId + '%'";
    //            wherestr2 = "and func.func_code like '%' + @funcId + '%'";
    //        }

    //        cmd.CommandText = string.Format(@"declare @@count int;
    //                                              declare @@manager_type nvarchar(2);
    //                                              select @@count =count(*) from tbFunctionsAuth where account_code  = @account_code {0};
    //                                              IF (@@count =0) BEGIN
    //                                              select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
    //                                              select func.func_code , 
    //                                               func.func_name,
    //                                               func.upper_level_code , func.func_link, func.level,
    //                                               auth. auth_id , auth.manager_type , auth.account_code , 
    //                                               isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
    //                                               isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
    //                                               auth.uuser , auth.udate 
    //                                               from tbFunctions func
    //                                               left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type
    //                                                  where 0=0 {1}
    //                                               order by func.level,func.func_code  
    //                                              END ELSE BEGIN 
    //                                               select func.func_code , 
    //                                               func.func_name,
    //                                               func.upper_level_code , func.func_link, func.level,
    //                                               auth. auth_id , auth.manager_type , auth.account_code , 
    //                                               isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
    //                                               isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
    //                                               auth.uuser , auth.udate 
    //                                               from tbFunctions func
    //                                               left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.account_code  = @account_code
    //                                                  where 0=0 {2}
    //                                               order by func.level,func.func_code  
    //                                              END; ", wherestr, wherestr2, wherestr2);

    //        dt = dbAdapter.getDataTable(cmd);
    //        if (dt != null && dt.Rows.Count > 0)
    //        {
    //            Boolean view_auth = false;
    //            Boolean insert_auth = false;
    //            Boolean modify_auth = false;
    //            Boolean delete_auth = false;
    //            string funccode = "";
    //            string sirId = "";
    //            string href = "";
    //            int level = 0;

    //            for (int i = 0; i < dt.Rows.Count; i++)
    //            {
    //                level = Convert.ToInt32(dt.Rows[i]["level"].ToString());
    //                funccode = dt.Rows[i]["func_code"].ToString();
    //                sirId = dt.Rows[i]["upper_level_code"].ToString();
    //                href = dt.Rows[i]["func_link"].ToString();
    //                view_auth = Convert.ToBoolean(dt.Rows[i]["view_auth"]);
    //                insert_auth = Convert.ToBoolean(dt.Rows[i]["insert_auth"]);
    //                modify_auth = Convert.ToBoolean(dt.Rows[i]["modify_auth"]);
    //                delete_auth = Convert.ToBoolean(dt.Rows[i]["delete_auth"]);
    //                if (view_auth || insert_auth || modify_auth || delete_auth)
    //                {
    //                    string toTopLevelStr = "";
    //                    SqlCommand cmd2 = new SqlCommand();
    //                    cmd2.CommandText = "usp_GetShipFeeByCheckNumber";
    //                    cmd2.CommandType = CommandType.StoredProcedure;
    //                    cmd2.Parameters.AddWithValue("@func_code", funccode);
    //                    using (DataTable dtb = dbAdapter.getDataTable(cmd2))
    //                    {
    //                        if (dtb != null && dt.Rows.Count > 0)
    //                        {
    //                            toTopLevelStr = dt.Rows[0][0].ToString();
    //                            if (toTopLevelStr != "")
    //                            {
    //                                string[] Pstr = toTopLevelStr.Split(',');
    //                                for (int j = 0; j <= Pstr.Length - 1; j++)
    //                                {
    //                                    if (j == 0)
    //                                    {
    //                                        ttIdx = List.IndexOfKey(Pstr[j]);
    //                                        if (ttIdx == -1)
    //                                        {
    //                                            List.Add(Pstr[j], new SortedList());
    //                                        }
    //                                        else
    //                                        {
    //                                            SortedList subList = (SortedList)List.GetByIndex(ttIdx);
    //                                            List.SetByIndex(ttIdx, subList);
    //                                        }
    //                                    }
    //                                    else
    //                                    {
    //                                        ttIdx = List.IndexOfKey(Pstr[j-1]);
    //                                        if (ttIdx == -1)
    //                                        {
    //                                            List.Add(Pstr[j], new SortedList());
    //                                        }
    //                                        else
    //                                        {
    //                                            SortedList subList = (SortedList)List.GetByIndex(ttIdx);
    //                                            List.SetByIndex(ttIdx, subList);
    //                                        }
    //                                    }
    //                                }
    //                            }
    //                            else
    //                            {
    //                                // 沒有上層(第一層)
    //                                ttIdx = List.IndexOfKey(funccode);
    //                                if (ttIdx == -1)
    //                                {
    //                                    List.Add(funccode, new SortedList());
    //                                }
    //                            }
    //                        }
    //                    }


    //                    //if (sirId == "")
    //                    //{
    //                    //    ttIdx = List.IndexOfKey(funccode);
    //                    //    if (ttIdx == -1)
    //                    //    {
    //                    //        List.Add(funccode, new SortedList());
    //                    //    }
    //                    //}
    //                    //else
    //                    //{
    //                    //    //getSirfunc(sirId, ref List);
    //                    //    ttIdx = List.IndexOfKey(sirId);
    //                    //    if (ttIdx == -1)
    //                    //    {

    //                    //    }
    //                    //    else
    //                    //    {
    //                    //        SortedList subList = (SortedList)List.GetByIndex(ttIdx);
    //                    //        List.SetByIndex(ttIdx, subList);
    //                    //    }
    //                    //}
    //                }
    //            }
    //        }
    //    }
    //    //return dt;
    //    return List;
    //}


    //public void getSirfunc(string funcId, ref SortedList List)
    //{
    //    int ttIdx = -1;
    //    string wherestr = "";
    //    using (SqlCommand cmd = new SqlCommand())
    //    {   
    //        cmd.Parameters.AddWithValue("@funcId", funcId);
    //        wherestr = "and func_code = @funcId";
    //        cmd.CommandText = string.Format(@"select * from tbFunctions where 0=0 {0} ", wherestr);

    //        using (DataTable  dt = dbAdapter.getDataTable(cmd))
    //        {
    //            if (dt != null && dt.Rows.Count > 0)
    //            {
    //                string func = "";
    //                string sirId = "";
    //                string href = "";
    //                func = dt.Rows[0]["func_code"].ToString();
    //                sirId = dt.Rows[0]["upper_level_code"].ToString();
    //                href = dt.Rows[0]["func_link"].ToString();
    //                if (sirId == "")
    //                {
    //                    ttIdx = List.IndexOfKey(func);
    //                    if (ttIdx == -1)
    //                    {
    //                        List.Add(func, null);
    //                    }
    //                }
    //                else
    //                {
    //                    getSirfunc(sirId, ref List);
    //                    ttIdx = List.IndexOfKey(func);
    //                    if (ttIdx == -1)
    //                    {
    //                        List.Add(func, "");
    //                    }
    //                }

    //            }
    //        }
    //    }
    //}



    /// <summary> 回傳自營客戶自訂價格設定表HTML </summary>
    /// <param name="customer_code">客戶編號</param>
    /// <returns></returns>
    public static String GetBusDefLogOfPrice(string customer_code, string customer_id)
    {
        StringBuilder sb_html = new StringBuilder();
        try
        {
            int i_customer_id = 0;
            if (customer_id != null) int.TryParse(customer_id.ToString(), out i_customer_id);

            if (!String.IsNullOrEmpty(customer_code))
            {
                //客戶自營自訂運價記錄 ttPriceBusinessDefineLog
                using (SqlCommand cmd = new SqlCommand())
                {
                    //String strSQL = @"DECLARE @tariffs_effect_date_now DATETIME    

                    //                  DECLARE @t_temp TABLE (
                    //                  tariffs_effect_date_today Date )

                    //                  INSERT INTO @t_temp (tariffs_effect_date_today)
                    //                  EXEC usp_GetTodayPriceBusDefLog @customer_code = @customer_code_this;
                    //                  SELECT @tariffs_effect_date_now = tariffs_effect_date_today FROM @t_temp

                    //                  SELECT log_id,CONVERT(CHAR(10),tariffs_effect_date,111) tariffs_effect_date,tariffs_type
                    //                  ,CASE WHEN CONVERT(CHAR(10),tariffs_effect_date,111) = CONVERT(CHAR(10),@tariffs_effect_date_now,111) THEN '0'
                    //                        WHEN CONVERT(CHAR(10),tariffs_effect_date,111) < CONVERT(CHAR(10),@tariffs_effect_date_now,111) THEN '-1'
                    //                  	  ELSE  '1' END status 
                    //                    FROM ttPriceBusinessDefineLog With(Nolock)
                    //                   WHERE customer_code = @customer_code_this 
                    //                  ORDER BY tariffs_effect_date DESC ";

                    //開放結帳日後的運價可以刪除，但重新上傳要重新計算運價生效日後的運價
                    String strSQL = @"DECLARE @tariffs_effect_date_now DATETIME    
                                      DECLARE @checkout_close_date_now DATETIME     --最新結帳日

                                      DECLARE @t_temp TABLE (
                                      tariffs_effect_date_today Date )
                                      
                                      INSERT INTO @t_temp (tariffs_effect_date_today)
                                      EXEC usp_GetTodayPriceBusDefLog @customer_code = @customer_code_this;
                                      SELECT @tariffs_effect_date_now = tariffs_effect_date_today FROM @t_temp
                                      
                                      SELECT @checkout_close_date_now=CONVERT(varchar(100),max(checkout_close_date), 111)
                                      FROM tcDeliveryRequests With(Nolock) where customer_code=@customer_code_this

                                      SELECT P.log_id,CONVERT(CHAR(10),P.tariffs_effect_date,111) tariffs_effect_date, P.tariffs_type, P.cuser, Acc.user_name
                                      ,CASE WHEN CONVERT(CHAR(10),tariffs_effect_date,111) = CONVERT(CHAR(10),@tariffs_effect_date_now,111) AND (CONVERT(CHAR(10),tariffs_effect_date,111) > CONVERT(CHAR(10),@checkout_close_date_now,111)) THEN '2'
                                            WHEN CONVERT(CHAR(10),tariffs_effect_date,111) = CONVERT(CHAR(10),@tariffs_effect_date_now,111) THEN '0'   
                                            --WHEN (CONVERT(CHAR(10),tariffs_effect_date,111) < CONVERT(CHAR(10),@tariffs_effect_date_now,111)) AND (CONVERT(CHAR(10),tariffs_effect_date,111) > CONVERT(CHAR(10),@checkout_close_date_now,111)) THEN '3'
                                            WHEN CONVERT(CHAR(10),tariffs_effect_date,111) < CONVERT(CHAR(10),@tariffs_effect_date_now,111) THEN '-1'
                                      	  ELSE  '1' END status 
                                        FROM ttPriceBusinessDefineLog P With(Nolock) 
                                        LEFT JOIN tbAccounts  Acc with(nolock)  on P.cuser = Acc.account_code 
                                       WHERE P.customer_code = @customer_code_this 
                                      ORDER BY tariffs_effect_date DESC ";

                    cmd.CommandText = strSQL;
                    cmd.Parameters.AddWithValue("@customer_code_this", customer_code);
                    DateTime dt_tmp = new DateTime();
                    int i_id, i_type, i_status;
                    string str_date, str_type, str_id, str_class, str_alert, str_user;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        //table.head
                        sb_html.Append("<div class ='div_price_log scrollbar'><table class='tb_price_log'>");
                        sb_html.Append("<tr class='tb_price_th'><th class='_p_no'>No.</th><th class='p_date'>生效日</th><th class='p_type'>類　別</th><th class='p_type'>更新人員</th><th class='p_set' colspan='2'>功　　能</th></tr>");


                        #region table.body
                        if (dt.Rows.Count == 0)
                        {
                            sb_html.Append("<tr><td colspan='6' class='p_td_null'>尚未設定</td></tr>");
                        }

                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            str_class = i % 2 == 0 ? " bk_color1" : " bk_color2";
                            str_date = ""; str_type = ""; str_id = ""; str_alert = ""; str_user = "";
                            str_id = "";

                            if (int.TryParse(dt.Rows[i]["log_id"].ToString(), out i_id)) str_id = i_id.ToString();
                            if (DateTime.TryParse(dt.Rows[i]["tariffs_effect_date"].ToString(), out dt_tmp)) str_date = dt_tmp.ToString("yyyy/MM/dd");
                            if (int.TryParse(dt.Rows[i]["tariffs_type"].ToString(), out i_type)) { str_type = i_type == 2 ? "自　訂" : "公　版"; } //1:公版 2:自訂
                            if (!int.TryParse(dt.Rows[i]["status"].ToString(), out i_status)) i_status = -1; //status 1:未來 0:今日 -1:過去，不開放刪除                            
                            str_user = dt.Rows[i]["user_name"].ToString();

                            //0: 目前採用此運價 AND 已結帳
                            //2: 目前採用此運價 AND 未結帳
                            //-1: 舊運價 AND 已結帳
                            //1: 未來運價AND 未結帳
                            switch (i_status)
                            {
                                case 0:
                                case 2:
                                    //目前使用的規則
                                    str_class += " _price_today";
                                    str_alert += "目前採用此運價!";
                                    break;
                                case -1:
                                    str_alert += "此運價不開放刪除!";
                                    break;
                                default:
                                    str_alert += "此運價尚未開始啟用!";
                                    break;
                            }

                            sb_html.Append("<tr title='" + str_alert + "' class='tb_price_tr " + str_class + "'>");
                            sb_html.Append("<td>" + (i + 1).ToString() + "</td>");
                            sb_html.Append("<td class='p_date'>" + str_date + "</td>");
                            sb_html.Append("<td>" + str_type + "</td>");
                            sb_html.Append("<td>" + str_user + "</td>");

                            //尚未發生的設定才開放刪除
                            if (i_status == 1 || i_status == 2)
                            {
                                sb_html.Append("<td class='p_btn'><input name='del_" + str_id + "' type='button' class='btn _price_del btn-danger' value='刪 除' onclick='price_del(" + str_id + ")' btype='del_" + str_id + "' /></td>");
                            }
                            else
                            {
                                sb_html.Append("<td class='p_btn'></td>");
                            }

                            //自訂:開放下載; 公版:統一由上方的「下載公版」Btn下載
                            if (i_type == 2)
                            {
                                sb_html.Append("<td class='p_btn'><input name='dw_" + str_id + "' type = 'button' class='btn _price_dw btn-primary' value='下 載' onclick='price_dw(" + str_id + "," + i_customer_id.ToString() + ")' btype='dw_" + str_id + "' /></td>");
                            }
                            else
                            {
                                sb_html.Append("<td class='p_btn'></td>");
                            }
                            sb_html.Append("</tr>");
                        }
                        #endregion

                        //table.end
                        sb_html.Append("</table><div>");

                    }
                }

            }
        }
        catch (Exception ex)
        {

        }



        return sb_html.ToString();
    }


    public static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    public class AccountsReceivableInfo
    {
        /// <summary>
        /// 應收帳款
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 稅額
        /// </summary>
        public int tax { get; set; }

        /// <summary>
        /// 小計
        /// </summary>
        public int subtotal { get; set; }

        /// <summary>
        /// 板數
        /// </summary>
        public int plates { get; set; }

        /// <summary>
        /// 件數
        /// </summary>
        public int pieces { get; set; }

        /// <summary>
        /// 才數
        /// </summary>
        public int cbm { get; set; }

        /// <summary>
        /// 偏遠加價
        /// </summary>
        public int remote_fee { get; set; }

        /// <summary>
        /// 特殊費用
        /// </summary>
        public int special_fee { get; set; }


    }

    public static AccountsReceivableInfo GetSumAccountsReceivable(string dates, string datee, string closedate = null, string customer_code = null, string pricing_type = null,
                                                                   string groupby = null, string randomCode = null, string supplier_code = null)
    {
        AccountsReceivableInfo _info = new AccountsReceivableInfo();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsReceivable";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pricing_type", pricing_type);  //計價模式

                if (supplier_code != null && supplier_code.Length > 0)
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }

                cmd.Parameters.AddWithValue("@dates", dates);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
                cmd.Parameters.AddWithValue("@randomCode", randomCode);  //結帳碼
                if (groupby != null)
                {
                    cmd.Parameters.AddWithValue("@groupby", groupby);
                }
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
                        _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
                        _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
                        _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;
                        _info.pieces = dt.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["pieces"]) : 0;
                        _info.cbm = dt.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cbm"]) : 0;
                        _info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

        }

        return _info;
    }

    public static AccountsReceivableInfo GetSumReceivableByStore(string dates, string datee, string closedate = null, string customer_code = null, string groupby = null)
    {
        AccountsReceivableInfo _info = new AccountsReceivableInfo();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumReceivableByStore";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                cmd.Parameters.AddWithValue("@dates", dates);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
                //if (groupby != null)
                //{
                //    cmd.Parameters.AddWithValue("@groupby", groupby);
                //}
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
                        _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
                        _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
                    }
                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumReceivableByStore" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

        }

        return _info;
    }

    /// <summary>
    /// C段應付總計
    /// </summary>
    /// <param name="dates"></param>
    /// <param name="datee"></param>
    /// <param name="closedate"></param>
    /// <param name="customer_code"></param>
    /// <param name="pricing_type"></param>
    /// <param name="groupby"></param>
    /// <returns></returns>
    public static AccountsReceivableInfo GetSumAccountsPayable_C(string dates, string datee, string closedate = null, string customer_code = null, string pricing_type = null, string groupby = null)
    {
        AccountsReceivableInfo _info = new AccountsReceivableInfo();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsPayablebyJUNFU";  // "usp_GetSumAccountsPayable";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pricing_type", pricing_type);  //計價模式
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                cmd.Parameters.AddWithValue("@dates", dates);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
                if (groupby != null)
                {
                    cmd.Parameters.AddWithValue("@groupby", groupby);
                }
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
                        _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
                        _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
                        _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;
                        _info.pieces = dt.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["pieces"]) : 0;
                        _info.cbm = dt.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cbm"]) : 0;
                        _info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "GetSumAccountsPayable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

        }

        return _info;
    }

    /// <summary>
    /// 應付總計
    /// </summary>
    /// <param name="dates"></param>
    /// <param name="datee"></param>
    /// <returns></returns>
    public static AccountsReceivableInfo GetSumAccountsPayable(string src, string dates, string datee, string closedate = null, string supplier = null, string randomCode = null)
    {
        AccountsReceivableInfo _info = new AccountsReceivableInfo();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string sp_name = "";
                switch (src)
                {
                    case "A":
                        sp_name = "usp_GetSumAccountsPayable_A";   //A段應付總計
                        break;
                    case "B":
                        sp_name = "usp_GetSumAccountsPayable_B";   //B段應付總計
                        break;
                    case "D":
                        sp_name = "usp_GetSumAccountsPayable_D";   //專車應付總計
                        break;
                    case "E":
                        sp_name = "usp_GetSumAccountsPayable_S";   //超商應付總計
                        break;
                }
                cmd.CommandText = sp_name;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@supplier", supplier);  //供應商
                cmd.Parameters.AddWithValue("@dates", dates);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
                cmd.Parameters.AddWithValue("@randomCode", randomCode);  //結帳碼
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
                        _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
                        _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
                        _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;
                        _info.pieces = dt.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["pieces"]) : 0;
                        _info.cbm = dt.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cbm"]) : 0;
                        _info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "GetSumAccountsPayable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

        }

        return _info;
    }


    ///// <summary>
    ///// B段應付總計
    ///// </summary>
    ///// <param name="dates"></param>
    ///// <param name="datee"></param>
    ///// <returns></returns>
    //public static AccountsReceivableInfo GetSumAccountsPayable_B(string dates, string datee, string closedate = null, string supplier = null, string randomCode = null)
    //{
    //    AccountsReceivableInfo _info = new AccountsReceivableInfo();
    //    try
    //    {
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandText = "usp_GetSumAccountsPayable_B";
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.AddWithValue("@supplier", supplier);  //供應商
    //            cmd.Parameters.AddWithValue("@dates", dates);  //日期起
    //            cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
    //            cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
    //            using (DataTable dt = dbAdapter.getDataTable(cmd))
    //            {
    //                if (dt.Rows.Count > 0)
    //                {
    //                    _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
    //                    _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
    //                    _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
    //                    _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;                        
    //                    //_info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
    //                }

    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        string strErr = string.Empty;
    //        strErr = "GetSumAccountsPayable_B" + ": " + ex.ToString();

    //        PublicFunction _fun = new PublicFunction();
    //        _fun.Log(strErr, "S");

    //    }

    //    return _info;

    //}

    ///// <summary>
    ///// 專車應付總計
    ///// </summary>
    ///// <param name="dates"></param>
    ///// <param name="datee"></param>
    ///// <returns></returns>
    //public static AccountsReceivableInfo GetSumAccountsPayable_D(string dates, string datee, string closedate = null, string supplier = null)
    //{
    //    AccountsReceivableInfo _info = new AccountsReceivableInfo();
    //    try
    //    {
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandText = "usp_GetSumAccountsPayable_D";
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.AddWithValue("@supplier", supplier);  //供應商
    //            cmd.Parameters.AddWithValue("@dates", dates);  //日期起
    //            cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
    //            cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
    //            using (DataTable dt = dbAdapter.getDataTable(cmd))
    //            {
    //                if (dt.Rows.Count > 0)
    //                {
    //                    _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
    //                    _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
    //                    _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
    //                    _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;
    //                    //_info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
    //                }

    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        string strErr = string.Empty;
    //        strErr = "GetSumAccountsPayable_D" + ": " + ex.ToString();

    //        PublicFunction _fun = new PublicFunction();
    //        _fun.Log(strErr, "S");

    //    }

    //    return _info;

    //}

    ///// <summary>
    ///// 超商應付總計
    ///// </summary>
    ///// <param name="dates"></param>
    ///// <param name="datee"></param>
    ///// <returns></returns>
    //public static AccountsReceivableInfo GetSumAccountsPayable_S(string dates, string datee, string closedate = null, string supplier = null)
    //{
    //    AccountsReceivableInfo _info = new AccountsReceivableInfo();
    //    try
    //    {
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandText = "usp_GetSumAccountsPayable_S";
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.AddWithValue("@supplier", supplier);  //供應商
    //            cmd.Parameters.AddWithValue("@dates", dates);  //日期起
    //            cmd.Parameters.AddWithValue("@datee", datee);  //日期迄
    //            cmd.Parameters.AddWithValue("@closedate", closedate);  //結帳日
    //            using (DataTable dt = dbAdapter.getDataTable(cmd))
    //            {
    //                if (dt.Rows.Count > 0)
    //                {
    //                    _info.subtotal = dt.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["subtotal"]) : 0;
    //                    _info.tax = dt.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["tax"]) : 0;
    //                    _info.total = dt.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["total"]) : 0;
    //                    _info.plates = dt.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["plates"]) : 0;
    //                    //_info.special_fee = dt.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["special_fee"]) : 0;
    //                }

    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //        string strErr = string.Empty;
    //        strErr = "GetSumAccountsPayable_S" + ": " + ex.ToString();

    //        PublicFunction _fun = new PublicFunction();
    //        _fun.Log(strErr, "S");

    //    }

    //    return _info;

    //}


    /// <summary>
    /// 
    /// </summary>
    /// <param name="ComeFrom"></param>
    /// <param name="Mobile"></param>
    /// <param name="Title"></param>
    /// <param name="Content"></param>
    /// <param name="SendUser"></param>
    /// <param name="CheckNumber"></param>
    /// <returns></returns>
    public static bool SMS_Send(string ComeFrom, string Mobile, string Title, string Content, string SendUser, string CheckNumber)
    {
        var httpClient = new HttpClient() { BaseAddress = new Uri(@"http://map.fs-express.com.tw/Notification/SMSSend") };
        var parameters = new Dictionary<string, string> {
            { "ComeFrom", ComeFrom } ,
            { "Mobile", Mobile } ,
          { "Title", Title } ,
            { "Content", Content } ,
              { "SendUser", SendUser } ,
                { "CheckNumber", CheckNumber }
        };
        string jsonStr = JsonConvert.SerializeObject(parameters);
        HttpContent contentPost = new StringContent(jsonStr, Encoding.UTF8, "application/json");
        try
        {
            //Post http callas.  
            HttpResponseMessage response = httpClient.PostAsync(@"http://map.fs-express.com.tw/Notification/SMSSend", contentPost).Result;
            //nesekmes atveju error..
            response.EnsureSuccessStatusCode();
            //responsas to string
            string responseBody = response.Content.ReadAsStringAsync().Result;

            if (responseBody != "null")
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
        }
        catch (HttpRequestException a)
        {
            Console.WriteLine("\nException Caught!");
            Console.WriteLine("Message :{0} ", a.Message);

        }
        return true;
    }
    /// <summary>
    /// 完整的寄信函數
    /// </summary>
    /// <param name="MailFrom">寄信人Email Address</param>
    /// <param name="MailTos">收信人Email Address</param>
    /// <param name="Ccs">副本Email Address</param>
    /// <param name="MailSub">主旨</param>
    /// <param name="MailBody">內文</param>
    /// <param name="isBodyHtml">是否為Html格式</param>
    /// <param name="files">要夾帶的附檔</param>
    /// <returns>回傳寄信是否成功(true:成功,false:失敗)</returns>
    public static bool Mail_Send(string MailFrom, string[] MailTos, string[] Ccs, string MailSub, string MailBody, bool isBodyHtml, Dictionary<string, Stream> files)
    {

        try
        {

            //沒給寄信人mail address
            if (string.IsNullOrEmpty(MailFrom))
            {//※有些公司的Smtp Server會規定寄信人的Domain Name須是該Smtp Server的Domain Name，例如底下的 system.com.tw
                MailFrom = ConfigurationManager.AppSettings["MailSender"];
            }

            //命名空間： System.Web.Mail已過時，http://msdn.microsoft.com/zh-tw/library/system.web.mail.mailmessage(v=vs.80).aspx
            //建立MailMessage物件
            MailMessage mms = new MailMessage();
            //指定一位寄信人MailAddress
            mms.From = new MailAddress(MailFrom);
            //信件主旨
            mms.Subject = MailSub;
            //信件內容
            mms.Body = MailBody;
            //信件內容 是否採用Html格式
            mms.IsBodyHtml = isBodyHtml;

            if (MailTos != null)//防呆
            {
                for (int i = 0; i < MailTos.Length; i++)
                {
                    //加入信件的收信人(們)address
                    if (!string.IsNullOrEmpty(MailTos[i].Trim()))
                    {
                        mms.To.Add(new MailAddress(MailTos[i].Trim()));
                    }

                }
            }//End if (MailTos !=null)//防呆

            if (Ccs != null) //防呆
            {
                for (int i = 0; i < Ccs.Length; i++)
                {
                    if (!string.IsNullOrEmpty(Ccs[i].Trim()))
                    {
                        //加入信件的副本(們)address
                        mms.CC.Add(new MailAddress(Ccs[i].Trim()));
                    }

                }
            }//End if (Ccs!=null) //防呆


            //附件處理
            if (files != null && files.Count > 0)//寄信時有夾帶附檔
            {
                foreach (string fileName in files.Keys)
                {
                    Attachment attfile = new Attachment(files[fileName], fileName);
                    mms.Attachments.Add(attfile);
                }//end foreach
            }//end if 

            //using (SmtpClient client = new SmtpClient("smtp.gmail.com", 25))//或公司、客戶的smtp_server
            //{
            //    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LightYearMail"]) 
            //        && !string.IsNullOrEmpty(ConfigurationManager.AppSettings["LightYearMailPassword"]))//.config有帳密的話
            //    {//寄信要不要帳密？眾說紛紜Orz，分享一下經驗談....

            //        //網友阿尼尼:http://www.dotblogs.com.tw/kkc123/archive/2012/06/26/73076.aspx
            //        //※公司內部不用認證,寄到外部信箱要特別認證 Account & Password

            //        //自家公司MIS:
            //        //※要看smtp server的設定呀~

            //        //結論...
            //        //※程式在客戶那邊執行的話，問客戶，程式在自家公司執行的話，問自家公司MIS，最準確XD
            //        client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["LightYearMail"], ConfigurationManager.AppSettings["LightYearMailPassword"]);//寄信帳密
            //    }
            //    client.Send(mms);//寄出一封信
            //}//end using 

            using (SmtpClient client = new SmtpClient())//或公司、客戶的smtp_server
            {
                client.Host = ConfigurationManager.AppSettings["Support_mailsmtp"];
                client.Port = int.Parse(ConfigurationManager.AppSettings["Support_mailsmtpport"]);
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Support_sendmail"], ConfigurationManager.AppSettings["Support_mailpwd"]);

                client.Send(mms);//寄出一封信
            }//end using 



            //釋放每個附件，才不會Lock住
            if (mms.Attachments != null && mms.Attachments.Count > 0)
            {
                for (int i = 0; i < mms.Attachments.Count; i++)
                {
                    mms.Attachments[i].Dispose();

                }
            }

            return true;//成功
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
            string strErr = string.Empty;
            strErr = "寄信失敗-" + ": " + ex.ToString();
            System.Diagnostics.Debug.WriteLine(ex.ToString());


            //寄信失敗，寫Log文字檔
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
            return false;
        }
    }//End 寄信


    public static string GetMd5Hash(MD5 md5Hash, string input)
    {

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }

    public static string getUpper_Level_Code(string Path)
    {
        string Upper_Level_Code = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            String strSQL = @"select ISNULL(upper_level_code,'') as upper_level_code from  tbFunctions with (nolock) where func_link=@func_link ";

            cmd.CommandText = strSQL;
            cmd.Parameters.AddWithValue("@func_link", Path);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    Upper_Level_Code = dt.Rows[0]["upper_level_code"].ToString();
                }

            }
        }
        return Upper_Level_Code;
    }

    public static DataTable getArea_Arrive_Code(bool Less_than_truckload = false, string supplier_code = "")
    {
        DataTable dt = new DataTable();
        if (Less_than_truckload)
        {
            //零擔到著碼
            using (SqlCommand cmd = new SqlCommand())
            {
                //cmd.CommandText = string.Format(@" SELECT distinct area_arrive_code  from ttArriveSitesScattered with(nolock)
                //                            WHERE ISNULL(area_arrive_code,'') <> '' 
                //                            ORDER BY  area_arrive_code");

                //cmd.CommandText = string.Format(@" SELECT  station_code, station_code + ' ' +  station_name as showname   from tbStation with(nolock)
                //                            WHERE ISNULL(station_code,'') <> '' 
                //                            ORDER BY  station_code");

                cmd.CommandText = string.Format(@" SELECT  station_code, station_code + ' ' +  station_name as showname, station_scode, station_scode + ' ' +  station_name as showsname    from tbStation with(nolock)
                                            WHERE ISNULL(station_scode,'') <> '' and station_code <>'F53' and station_code <>'F71' 
                                            ORDER BY  station_scode");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                }
                catch (Exception ex)
                {
                    string strErr = string.Empty;
                    strErr = "getArea_Arrive_Code-" + ": " + ex.ToString();
                    PublicFunction _fun = new PublicFunction();
                    _fun.Log(strErr, "S");
                }
            }
        }
        else
        {
            //棧板到著碼
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@supplier_code_sel", supplier_code);
                cmd.Parameters.AddWithValue("@hctcode", "001");
                cmd.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1 and supplier_code =@supplier_code_sel
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
                                                                                        order by supplier_code
                                            end");

                dt = dbAdapter.getDataTable(cmd);
            }

        }
        return dt;
    }

    public static DataTable getArea_Arrive_Code_For_Member(bool Less_than_truckload = false, string supplier_code = "")
    {
        DataTable dt = new DataTable();
        if (Less_than_truckload)
        {
            //零擔到著碼
            using (SqlCommand cmd = new SqlCommand())
            {
                //cmd.CommandText = string.Format(@" SELECT distinct area_arrive_code  from ttArriveSitesScattered with(nolock)
                //                            WHERE ISNULL(area_arrive_code,'') <> '' 
                //                            ORDER BY  area_arrive_code");

                //cmd.CommandText = string.Format(@" SELECT  station_code, station_code + ' ' +  station_name as showname   from tbStation with(nolock)
                //                            WHERE ISNULL(station_code,'') <> '' 
                //                            ORDER BY  station_code");

                cmd.CommandText = string.Format(@" SELECT  id,station_code, station_code + ' ' +  station_name as showname, station_scode, station_scode + ' ' +  station_name as showsname    from tbStation with(nolock)
                                            WHERE ISNULL(station_scode,'') <> '' and station_code <>'F53' and station_code <>'F71' 
                                            ORDER BY  station_scode");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                }
                catch (Exception ex)
                {
                    string strErr = string.Empty;
                    strErr = "getArea_Arrive_Code-" + ": " + ex.ToString();
                    PublicFunction _fun = new PublicFunction();
                    _fun.Log(strErr, "S");
                }
            }
        }
        else
        {
            //棧板到著碼
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@supplier_code_sel", supplier_code);
                cmd.Parameters.AddWithValue("@hctcode", "001");
                cmd.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1 and supplier_code =@supplier_code_sel
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
                                                                                        order by supplier_code
                                            end");

                dt = dbAdapter.getDataTable(cmd);
            }

        }
        return dt;
    }

    /// <summary>
    /// 取得零擔客戶
    /// </summary>
    /// <returns></returns>
    public static DataTable GetCommonShippingFee()
    {
        DataTable dt;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"select * from CustomerShippingFee where customer_code = '0'";
            dt = dbAdapter.getDataTable(cmd);
        }

        return dt;
    }

    public static bool IsHolidayDelivery(string customerCode, string areaArriveCode, DateTime date)
    {
        bool isHoliday;
        // 指配日期是假日
        if (date == DateTime.MinValue)
            return false;

        isHoliday = Utility.CheckHoliday(date);

        // 客戶有啟用假日配
        int row;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"select ROW_NUMBER() over(order by customer_code) from tbCustomers 
where is_weekend_delivered = 1 and customer_code = @cusotmer_code";
            cmd.Parameters.AddWithValue("@cusotmer_code", customerCode);

            var result = dbAdapter.getScalarBySQL(cmd);
            if (result == null)
                return false;

            int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out row);
        }

        return isHoliday && (!areaArriveCode.StartsWith("9")) && (row > 0);
    }
}