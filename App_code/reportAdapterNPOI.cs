﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.Hosting;
using System.IO;
using System.Net;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using System.Collections;

using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System.Text.RegularExpressions;

/// <summary>
/// reportAdapterNPOI 的摘要描述
/// </summary>
public partial class reportAdapter : Page
{
    #region Class:TotalParam

    /// <summary>
    /// 小計/合計參數
    /// </summary>
    public class TotalParam
    {
        public enum TotalType
        {
            /// <summary>
            /// 小計
            /// </summary>
            SubTotal = 0,
            /// <summary>
            /// 合計
            /// </summary>
            Total = 1,
        }

        /// <summary>
        /// 小計/合計類別
        /// </summary>
        public TotalType totalType;
        /// <summary>
        /// 資料表名稱
        /// </summary>
        public string tableName;
        /// <summary>
        /// 合併儲存格欄位數
        /// </summary>
        public int mergedCnt;
        /// <summary>
        /// 群組欄位名稱
        /// </summary>
        public string groupColNames;
        /// <summary>
        /// 小計/合計參數
        /// </summary>
        /// <param name="totalType">小計/合計類別</param>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="mergedCnt">多合併儲存格的欄位數</param>
        /// <param name="groupColNames">群組欄位名稱，多欄位時以","隔開</param>
        public TotalParam(TotalType ttTotalType, string strTableName, int intMergedCnt = 0, string strGroupColNames = null)
        {
            totalType = ttTotalType;
            tableName = strTableName;
            mergedCnt = intMergedCnt;
            groupColNames = strGroupColNames;
        }
    }

    #endregion

    #region Class:LocateTable

    /// <summary>
    /// Excel範本資料定位
    /// </summary>
    protected class LocateTable
    {
        /// <summary>
        /// 定位資料名稱
        /// </summary>
        public string Name;
        /// <summary>
        /// 定位的行數
        /// </summary>
        public int RowIndex;
        /// <summary>
        /// 定位的欄數
        /// </summary>
        public int CelIndex;

        public LocateTable(string strName, int intRowIndex, int intCelIndex)
        {
            Name = strName;
            RowIndex = intRowIndex;
            CelIndex = intCelIndex;
        }
    }

    #endregion

    #region Class:LocateTable

    /// <summary>
    /// Excel範本資料定位
    /// </summary>
    public class SampleExcelParam
    {
        #region autoHeight
        private bool _autoHeight = true;
        /// <summary>
        /// 自動依換行符號(\n)個數調整高度
        /// </summary>
        public bool autoHeight
        {
            get { return _autoHeight; }
            set { _autoHeight = value; }
        }
        #endregion

        #region copyFormula
        private bool _copyFormula = true;
        /// <summary>
        /// 範本列如果有功能，是否複製到Table的其它列
        /// </summary>
        public bool copyFormula
        {
            get { return _copyFormula; }
            set { _copyFormula = value; }
        }
        #endregion

        #region paramTotal
        public TotalParam[] paramTotal { get; set; }
        #endregion
    }

    #endregion

    /// <summary>
    /// 參數的起始符號
    /// </summary>
    public const string paraSymbol = "##para";
    /// <summary>
    /// 表格的起始位置符號
    /// </summary>
    public const string locateSymbol = "##iStart";
    /// <summary>
    /// 輸出附檔名
    /// </summary>
    public const string strExcelExtension = ".xls";
    /// <summary>
    /// 報表名稱(sheet)和表格名稱(table)之間的分隔符號
    /// </summary>
    public const string splitSymbol = "#";

    /// <summary>
    /// DataTable匯出至Excel(NPOI)
    /// </summary>
    /// <param name="dtSource">資料表來源</param>
    /// <param name="expFileName">匯出的檔案名稱</param>
    /// <param name="strHeader">頁首顯示文字</param>
    /// <param name="boolPageNumber">是否顯示頁碼</param>
    /// <param name="boolBorder">是否畫框線</param>
    /// <param name="isResponse">是否直接Response到Client</param>
    /// <returns>輸出檔案位置</returns>
    public static string npoiDataTableToExcel(DataTable dtSource, string expFileName, string strHeader, bool boolPageNumber = true, bool boolBorder = true, bool isResponse = true, string filetype = strExcelExtension)
    {
        IWorkbook workbook = null;
        ISheet sheet = null;
        IRow headerRow = null;
        int rowIndex = 0;
        string outputFolder = ConfigurationManager.AppSettings["uploadpath"];
        string outputFileName = expFileName + (expFileName.EndsWith(strExcelExtension) ? "" : strExcelExtension);

        try
        {
            workbook = new HSSFWorkbook();
            if (string.IsNullOrEmpty(dtSource.TableName))
            {
                //沒有Table Name以預設方式命名Sheet
                sheet = workbook.CreateSheet();
            }
            else
            {
                //有Table Name以Table Name命名Sheet
                sheet = workbook.CreateSheet(dtSource.TableName);
            }
            headerRow = sheet.CreateRow(rowIndex++);

            #region Cell樣式
            //標題列樣式
            ICellStyle csHeader = getHeaderStyle(ref workbook);

            //資料列樣式
            ICellStyle csContent = getContainStyle(ref workbook);
            #endregion

            //頁首
            if (string.IsNullOrEmpty(strHeader) == false)
            {
                sheet.Header.Center = string.Format("&B &22 &\"Arial\" {0}", strHeader);
            }

            //頁尾(頁碼)
            if (boolPageNumber)
            {
                sheet.Footer.Center = "第&P頁，共&N頁";  //頁尾
            }

            //自動換行
            csHeader.WrapText = true;

            #region 處理標題列
            foreach (DataColumn column in dtSource.Columns)
            {
                headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

                if (boolBorder) headerRow.GetCell(column.Ordinal).CellStyle = csHeader;
            }
            #endregion

            #region 處理資料列
            foreach (DataRow row in dtSource.Rows)
            {
                IRow dataRow = sheet.CreateRow(rowIndex++);

                foreach (DataColumn column in dtSource.Columns)
                {
                    dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));

                    if (boolBorder) dataRow.GetCell(column.Ordinal).CellStyle = csContent;
                }
            }
            #endregion

            #region 寫入檔案
            var file = new FileStream(outputFolder + outputFileName, FileMode.Create);
            workbook.Write(file);
            file.Close();
            #endregion

            if (isResponse)
            {
                //另開視窗匯出至Client
                BasePage.popDownload(outputFolder, outputFileName);
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }

        return outputFolder + outputFileName;
    }

    /// <summary>
    /// DataTable匯出至Excel(NPOI)
    /// </summary>
    /// <param name="dtSource">資料集</param>
    /// <param name="expFileName">匯出的檔案名稱</param>
    /// <param name="strHeader">頁首顯示文字</param>
    /// <param name="boolPageNumber">是否顯示頁碼</param>
    /// <param name="boolBorder">是否畫框線</param>
    /// <param name="isResponse">是否直接Response到Client</param>
    /// <returns>輸出檔案位置</returns>
    public static string npoiDataTableToExcel(DataSet dsSource, string expFileName, string strHeader, bool boolPageNumber, bool boolBorder, bool isResponse = true)
    {
        IWorkbook workbook = null;
        string outputFolder = ConfigurationManager.AppSettings["uploadpath"];
        string outputFileName = expFileName + (expFileName.EndsWith(strExcelExtension) ? "" : strExcelExtension);

        try
        {
            workbook = new HSSFWorkbook();

            foreach (DataTable dtSource in dsSource.Tables)
            {
                ISheet sheet = null;
                IRow headerRow = null;
                int rowIndex = 0;

                if (string.IsNullOrEmpty(dtSource.TableName))
                {
                    //沒有Table Name以預設方式命名Sheet
                    sheet = workbook.CreateSheet();
                }
                else
                {
                    //有Table Name以Table Name命名Sheet
                    sheet = workbook.CreateSheet(dtSource.TableName);
                }
                headerRow = sheet.CreateRow(rowIndex++);

                #region Cell樣式
                //標題列樣式
                ICellStyle csHeader = getHeaderStyle(ref workbook);

                //資料列樣式
                ICellStyle csContent = getContainStyle(ref workbook);
                #endregion

                //頁首
                if (string.IsNullOrEmpty(strHeader) == false)
                {
                    sheet.Header.Center = string.Format("&B &22 &\"Arial\" {0}", strHeader);
                }

                //頁尾(頁碼)
                if (boolPageNumber)
                {
                    sheet.Footer.Center = "第&P頁，共&N頁";  //頁尾
                }

                //自動換行
                csHeader.WrapText = true;

                #region 處理標題列
                foreach (DataColumn column in dtSource.Columns)
                {
                    headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);

                    if (boolBorder) headerRow.GetCell(column.Ordinal).CellStyle = csHeader;
                }
                #endregion

                #region 處理資料列
                foreach (DataRow row in dtSource.Rows)
                {
                    IRow dataRow = sheet.CreateRow(rowIndex++);

                    foreach (DataColumn column in dtSource.Columns)
                    {
                        dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));

                        if (boolBorder) dataRow.GetCell(column.Ordinal).CellStyle = csContent;
                    }
                }
                #endregion
            }

            #region 寫入檔案
            var file = new FileStream(outputFolder + outputFileName, FileMode.Create);
            workbook.Write(file);
            file.Close();
            #endregion

            if (isResponse)
            {
                //另開視窗匯出至Client
                BasePage.popDownload(outputFolder, outputFileName);
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }

        return outputFolder + outputFileName;
    }

    /// <summary>
    /// Excel檔轉成DataTable
    /// </summary>
    /// <param name="streamExcel"></param>
    /// <param name="indexSheet"></param>
    /// <param name="indexHeaderRowS"></param>
    /// <param name="indexHeaderRowE"></param>
    /// <returns></returns>
    public static DataTable npoiExcelToDataTable(Stream streamExcel, int indexSheet = 0, int? indexHeaderRowS = null, int? indexHeaderRowE = null)
    {
        DataTable dtReturn = null;

        try
        {
            IWorkbook workbook = new HSSFWorkbook(streamExcel);
            ISheet sheet = workbook.GetSheetAt(indexSheet);

            dtReturn = npoiExcelToDataTable(sheet, indexHeaderRowS, indexHeaderRowE);

            streamExcel.Close();
            sheet = null;
            workbook = null;
        }
        catch (Exception)
        {
            throw;
        }

        return dtReturn;
    }

    /// <summary>
    /// Excel檔轉成DataTable，多行標題列會以"-"串接標題，如「行為人-種類」
    /// </summary>
    /// <param name="streamExcel"></param>
    /// <param name="strSheetName"></param>
    /// <param name="indexHeaderRowS"></param>
    /// <param name="indexHeaderRowE"></param>
    /// <returns></returns>
    public static DataTable npoiExcelToDataTable(Stream streamExcel, string strSheetName, int? indexHeaderRowS = null, int? indexHeaderRowE = null)
    {
        DataTable dtReturn = new DataTable();

        try
        {
            IWorkbook workbook = new HSSFWorkbook(streamExcel);
            ISheet sheet = workbook.GetSheet(strSheetName);
            dtReturn = npoiExcelToDataTable(sheet, indexHeaderRowS, indexHeaderRowE);
            streamExcel.Close();
            sheet = null;
            workbook = null;
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw;
        }

        return dtReturn;
    }

    /// <summary>
    /// Excel檔轉成DataTable，多行標題列會以"-"串接標題，如「行為人-種類」
    /// </summary>
    /// <param name="streamExcel"></param>
    /// <param name="strSheetName"></param>
    /// <param name="indexHeaderRowS"></param>
    /// <param name="indexHeaderRowE"></param>
    /// <returns></returns>
    public static DataTable npoiExcelToDataTable(ISheet sheet, int? indexHeaderRowS = null, int? indexHeaderRowE = null)
    {
        DataTable dtReturn = new DataTable();

        try
        {
            if (indexHeaderRowS.HasValue == false)
            {
                indexHeaderRowS = 0;
            }
            if (indexHeaderRowE.HasValue == false)
            {
                indexHeaderRowE = indexHeaderRowS;
            }

            int cellCount = 0;      //欄位數
            int rowCount = sheet.LastRowNum;        //列數

            #region 讀取標題列，標題名稱作為Table Column Name
            int indexHeaderCellS = 0;
            do
            {
                //欄位名稱
                string strColName = string.Empty;

                #region 標題列範圍內，讀取標列列的欄位名稱
                int iShiftRow = 0;
                do
                {
                    ICell cellTmp;
                    IRow rowTmp = sheet.GetRow(indexHeaderRowS.Value + iShiftRow);
                    if (rowTmp != null)
                    {
                        #region 取得標題列的欄位數
                        cellCount = Math.Max(cellCount, rowTmp.LastCellNum);
                        #endregion

                        #region 讀取標題欄的欄位名稱
                        int iShiftCell = 0;
                        do
                        {
                            cellTmp = rowTmp.GetCell(indexHeaderCellS + iShiftCell);
                        } while (cellTmp != null
                            && (indexHeaderCellS + (iShiftCell - 1) > 0 && cellTmp.IsMergedCell && chkSameMerged(rowTmp.GetCell(indexHeaderCellS + iShiftCell), rowTmp.GetCell(indexHeaderCellS + (iShiftCell - 1))))       //是合併儲存格時，判斷該Cell和前一個Cell是不是同一個合併儲存格
                            && indexHeaderCellS + (--iShiftCell) >= rowTmp.FirstCellNum     //讀取的Cell未超出Sheet範圍
                            && BasePage.checkGetString(getCellValue(cellTmp)) == "");       //尚未讀到標題
                        strColName = BasePage.formatNoteBySymbol(strColName, BasePage.checkGetString(getCellValue(cellTmp)), "-", true);
                        #endregion
                    }
                } while (indexHeaderRowS + (++iShiftRow) <= indexHeaderRowE);
                #endregion

                DataColumn column = new DataColumn(strColName);
                dtReturn.Columns.Add(column);
            } while ((++indexHeaderCellS) < cellCount);
            #endregion

            #region 讀取資料列
            for (int i = (indexHeaderRowE.Value + 1); i <= sheet.LastRowNum; i++)
            {
                DataRow rowReturn = dtReturn.NewRow();
                IRow row = sheet.GetRow(i);
                bool boolHasValue = false;      //避免空白行
                if (row != null)
                {
                    for (int j = 0; j < row.Cells.Count; j++)
                    {
                        if (getCellValue(row.GetCell(j)) != null)
                        {
                            boolHasValue = true;
                        }
                    }
                }
                if (boolHasValue)
                {
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        rowReturn[j] = getCellValue(row.GetCell(j));
                    }
                    dtReturn.Rows.Add(rowReturn);
                }
            }
            #endregion

            sheet = null;
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw;
        }

        return dtReturn;
    }

    /// <summary>
    /// 範本Excel填入值後，匯出Excel
    /// </summary>
    /// <param name="dsSource">資料集(TableName要和SheetName相同，Table開始填入位置設##iStart)</param>
    /// <param name="hashPara">替換參數(參數開頭為##para)</param>
    /// <param name="sampleFileName">範本檔檔名(存放在report路徑下)</param>
    /// <param name="expFileName">匯出的檔案名稱</param>
    /// <param name="isResponse">是否直接Response到Client</param>
    /// <returns></returns>
    public static string npoiSampleToExcel(string uploadpath, DataSet dsSource, Hashtable hashPara, string sampleFileName, string expFileName = null, bool isResponse = true, SampleExcelParam param = null
        , Boolean only_index_sheet = false
        , Boolean ToDownloadSession = false)
    {
        string strSampleFileFullName = string.Empty;        //範本檔路徑
        string outputFolder = uploadpath;       //匯出檔路徑
        string outputFileName = expFileName;
        IWorkbook workbook = null;
        ISheet npoiSheet = null;
        IRow npoiRow = null;
        ICell npoiCell = null;


        try
        {
            #region SampleExcelParam為null時，初始SampleExcelParam物件，以取得初始值
            if (param == null)
            {
                param = new SampleExcelParam();
            }
            #endregion

            #region 設定範本檔路徑
            strSampleFileFullName = BasePage.pathRPT + sampleFileName;
            if (strSampleFileFullName.EndsWith(strExcelExtension) == false) strSampleFileFullName += strExcelExtension;
            #endregion



            #region 讀取範本檔到workbook
            using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(strSampleFileFullName), FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);
            }
            #endregion            

            #region 處理資料列
            for (int iSheet = 0; iSheet < workbook.NumberOfSheets; iSheet++)
            {
                npoiSheet = workbook.GetSheetAt(iSheet);
                int intShiftRowCount = 0;       //列的位移列數(因插入Table造成的位移)
                int intSumRowCount = 0;     //小計/合計位移列數(因插入小計/合計列造成的位移)
                List<LocateTable> listLocate = new List<LocateTable>();     //Table放置的啟始位置列表
                KeyValuePair<List<string>, int?> kvSubTotal = new KeyValuePair<List<string>, int?>();       //小計/總計參數列表

                #region Sheet屬性設定
                npoiSheet.ForceFormulaRecalculation = true;     //自動更新公式計算結果
                #endregion

                #region 處理參數替換，並取得資料表放置的位置
                for (int iRow = npoiSheet.FirstRowNum; iRow <= npoiSheet.LastRowNum; iRow++)
                {
                    #region 列為空的話直接跳過
                    npoiRow = npoiSheet.GetRow(iRow);
                    if (npoiRow == null) continue;
                    #endregion

                    for (int iCell = npoiRow.FirstCellNum; iCell <= npoiRow.LastCellNum; iCell++)
                    {
                        #region 欄為空的話直接跳過
                        npoiCell = npoiRow.GetCell(iCell);
                        if (npoiCell == null) continue;
                        #endregion

                        #region 取得Table啟始位置
                        if (BasePage.checkGetString(getCellValue(npoiCell)).StartsWith(locateSymbol))
                        {
                            string strLocateName = BasePage.checkGetString(getCellValue(npoiCell)).Replace(locateSymbol, "");
                            LocateTable locate = new LocateTable(strLocateName, iRow, iCell);
                            listLocate.Add(locate);

                            //清除該欄位的值，避免沒有對應的DataSource，而遺留參數字串
                            setCellValue(npoiCell, "");
                        }
                        #endregion

                        #region 替換##para參數成指定的值
                        if (BasePage.checkGetString(getCellValue(npoiCell)).IndexOf(paraSymbol) > -1)
                        {
                            setCellValue(npoiCell, chkParaValue(hashPara, getCellValue(npoiCell)));
                        }
                        #endregion
                    }
                }
                #endregion

                #region 依找到的Table啟始位置，塞入Table
                foreach (LocateTable locate in listLocate)
                {
                    DataTable sTable = dsSource.Tables[BasePage.formatNoteBySymbol(npoiSheet.SheetName, locate.Name, splitSymbol, true)];

                    #region 整理群組小計/合計的參數
                    int? intMergedCnt = null;
                    kvSubTotal = new KeyValuePair<List<string>, int?>(new List<string>(), null);
                    if (param.paramTotal != null)
                    {
                        foreach (TotalParam totalParam in param.paramTotal)
                        {
                            if (totalParam.tableName == locate.Name)
                            {
                                if (totalParam.totalType == TotalParam.TotalType.Total)
                                {
                                    intMergedCnt = totalParam.mergedCnt;
                                }
                                else if (totalParam.totalType == TotalParam.TotalType.SubTotal)
                                {
                                    List<string> listSubColName = new List<string>();       //要群組的欄位名稱
                                    int intSubMergedCnt = 0;            //要合併儲存格的欄位數

                                    listSubColName = new List<string>(totalParam.groupColNames.Split(BasePage.separatorComma, StringSplitOptions.None));
                                    intSubMergedCnt = totalParam.mergedCnt;

                                    kvSubTotal = new KeyValuePair<List<string>, int?>(listSubColName, intSubMergedCnt);
                                }
                            }
                        }
                    }
                    #endregion

                    if (sTable != null && sTable.Rows.Count > 0)
                    {
                        string strGroupKey = string.Empty;                  //小計的GroupKey名稱
                        DataRow rowSubTotal = sTable.NewRow();              //小計列
                        DataRow rowTotal = sTable.NewRow();                 //總計列

                        #region 取得Table啟始列的CellStyle
                        npoiRow = npoiSheet.GetRow(locate.RowIndex + intShiftRowCount + intSumRowCount);

                        #region 一般資料列的CellStyle
                        ICellStyle[] csSample = new ICellStyle[npoiRow.Cells.Count];
                        for (int i = 0; i < npoiRow.Cells.Count; i++)
                        {
                            csSample[i] = workbook.CreateCellStyle();
                            if (npoiRow.GetCell(i + locate.CelIndex) != null)
                            {
                                csSample[i].CloneStyleFrom(npoiRow.GetCell(i + locate.CelIndex).CellStyle);
                            }
                        }
                        #endregion

                        #region 小計/合計資料列的CellStyle
                        IFont fontTotal = workbook.CreateFont();
                        fontTotal.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;

                        ICellStyle[] csSampleTotal = new ICellStyle[npoiRow.Cells.Count];
                        for (int i = 0; i < npoiRow.Cells.Count; i++)
                        {
                            csSampleTotal[i] = workbook.CreateCellStyle();
                            if (npoiRow.GetCell(i + locate.CelIndex) != null)
                            {
                                csSampleTotal[i].CloneStyleFrom(npoiRow.GetCell(i + locate.CelIndex).CellStyle);
                            }
                            csSampleTotal[i].SetFont(fontTotal);
                        }
                        #endregion
                        #endregion

                        #region 插入sTable行數的資料列
                        int intDataRowCnt = sTable.Rows.Count;      //資料列數
                        int intSubTotalRowCnt = kvSubTotal.Value.HasValue ? sTable.DefaultView.ToTable(true, kvSubTotal.Key.ToArray()).Rows.Count : 0;      //小計列數
                        int intTotalRowCnt = intMergedCnt == null ? 0 : 1;      //合計列數
                        InsertRows(ref npoiSheet, locate.RowIndex + intShiftRowCount + intSumRowCount, intDataRowCnt + intSubTotalRowCnt + intTotalRowCnt - 1, param.copyFormula);      //要插入的列數=資料列數+小計列數+合計列數-範本列
                        #endregion

                        #region 塞DataTable
                        for (int iDataRow = 0; iDataRow < sTable.Rows.Count; iDataRow++)
                        {
                            int npoiRowHeight = npoiSheet.DefaultRowHeight; //資料列的高度

                            #region 取得資料列
                            npoiRow = npoiSheet.GetRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                            if (npoiRow == null) npoiRow = npoiSheet.CreateRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                            #endregion

                            #region 填入資料列
                            for (int iDataCol = 0; iDataCol < sTable.Columns.Count; iDataCol++)
                            {
                                #region 取得儲存格
                                npoiCell = npoiRow.GetCell(locate.CelIndex + iDataCol);
                                if (npoiCell == null) npoiCell = npoiRow.CreateCell(locate.CelIndex + iDataCol);
                                #endregion

                                #region 填入儲存格
                                //setCellValue(npoiCell, sTable.Rows[iDataRow][iDataCol]);
                                setCellValue(npoiCell, sTable.Rows[iDataRow][iDataCol].ToString());
                                #endregion

                                #region 依Table啟始列的CellStyle設定CellStyle
                                if (param.autoHeight)
                                {
                                    if (iDataCol < csSample.Length)
                                    {
                                        npoiRow.GetCell(locate.CelIndex + iDataCol).CellStyle = csSample[iDataCol];

                                        #region 取得該列最高的高度
                                        if (BasePage.checkGetString(getCellValue(npoiCell)).IndexOf("\n") > -1)
                                        {
                                            int intRowCount = Regex.Matches(BasePage.checkGetString(getCellValue(npoiCell)), @"[\n]").Count + 1;        //換行列數
                                            int intRowHeight = intRowCount * npoiSheet.DefaultRowHeight;        //基本列高*列數
                                            npoiRowHeight = Math.Max(npoiRowHeight, intRowHeight);
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                #region 計算/整理小計列的資料
                                if (kvSubTotal.Key.Contains(sTable.Columns[iDataCol].ColumnName) == false
                                    && (sTable.Rows[iDataRow][iDataCol].GetType().Name == "Decimal"
                                        || sTable.Rows[iDataRow][iDataCol].GetType().Name == "Double"
                                        || sTable.Rows[iDataRow][iDataCol].GetType().Name == "Int32"
                                    ))
                                {
                                    rowSubTotal[iDataCol] = BasePage.checkGetDouble(rowSubTotal[iDataCol]) + BasePage.checkGetDouble(sTable.Rows[iDataRow][iDataCol]);
                                    rowTotal[iDataCol] = BasePage.checkGetDouble(rowTotal[iDataCol]) + BasePage.checkGetDouble(sTable.Rows[iDataRow][iDataCol]);
                                }
                                #endregion
                            }

                            #region 設定資料列高度
                            if (param.autoHeight)
                            {
                                npoiRow.HeightInPoints = npoiRowHeight / 20;        //HeightInPoints單位為"點"，Height單位為1/20點
                            }
                            #endregion
                            #endregion

                            #region 塞入小計列
                            if (kvSubTotal.Value.HasValue)
                            {
                                if (getGroupKey(kvSubTotal.Key, sTable, iDataRow) != getGroupKey(kvSubTotal.Key, sTable, iDataRow + 1) ||       //當前列的主鍵與下一列的主鍵不同
                                    iDataRow == sTable.Rows.Count - 1)      //當前列是資料表列後一列
                                {
                                    intSumRowCount++;

                                    #region 取得小計列
                                    npoiRow = npoiSheet.GetRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                                    if (npoiRow == null) npoiRow = npoiSheet.CreateRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                                    #endregion

                                    #region 合併儲存格
                                    npoiSheet.AddMergedRegion(new CellRangeAddress(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow,
                                        locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow,
                                        locate.CelIndex,
                                        locate.CelIndex + kvSubTotal.Value.Value));
                                    #endregion

                                    #region 填入小計列
                                    for (int iDataCol = 0; iDataCol < rowSubTotal.ItemArray.Length; iDataCol++)
                                    {
                                        #region 取得小計儲存格
                                        npoiCell = npoiRow.GetCell(locate.CelIndex + iDataCol);
                                        if (npoiCell == null) npoiCell = npoiRow.CreateCell(locate.CelIndex + iDataCol);
                                        #endregion

                                        #region 填入小計儲存格
                                        if (kvSubTotal.Key.Contains(rowSubTotal.Table.Columns[iDataCol].ColumnName))        //欄位名稱包含在群組欄位名稱中
                                        {
                                            setCellValue(npoiCell, getGroupKey(kvSubTotal.Key, sTable, iDataRow) + "小計");
                                        }
                                        else    //小計的欄位，若為數值但又不小計(如年度)，以SQL將資料型態轉成字串
                                        {
                                            setCellValue(npoiCell, rowSubTotal[iDataCol]);
                                        }
                                        #endregion

                                        #region 依Table啟始列的CellStyle設定CellStyle
                                        if (iDataCol < csSampleTotal.Length)
                                        {
                                            npoiRow.GetCell(locate.CelIndex + iDataCol).CellStyle = csSampleTotal[iDataCol];
                                        }
                                        #endregion
                                    }
                                    #endregion

                                    rowSubTotal = sTable.NewRow();
                                }
                            }
                            #endregion

                            #region 塞入合計列
                            if (intMergedCnt.HasValue)
                            {
                                if (iDataRow == sTable.Rows.Count - 1)      //當前列是資料表列後一列
                                {
                                    intSumRowCount++;

                                    #region 取得合計列
                                    npoiRow = npoiSheet.GetRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                                    if (npoiRow == null) npoiRow = npoiSheet.CreateRow(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow);
                                    #endregion

                                    #region 合併儲存格
                                    npoiSheet.AddMergedRegion(new CellRangeAddress(locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow,
                                        locate.RowIndex + intShiftRowCount + intSumRowCount + iDataRow,
                                        locate.CelIndex,
                                        locate.CelIndex + intMergedCnt.Value));
                                    #endregion

                                    #region 填入合計列
                                    for (int iDataCol = 0; iDataCol < rowTotal.ItemArray.Length; iDataCol++)
                                    {
                                        #region 取得合計儲存格
                                        npoiCell = npoiRow.GetCell(locate.CelIndex + iDataCol);
                                        if (npoiCell == null) npoiCell = npoiRow.CreateCell(locate.CelIndex + iDataCol);
                                        #endregion

                                        #region 填入合計儲存格
                                        if (iDataCol == 0)      //第一欄
                                        {
                                            setCellValue(npoiCell, "合計");
                                        }
                                        else    //小計的欄位，若為數值但又不小計(如年度)，以SQL將資料型態轉成字串
                                        {
                                            setCellValue(npoiCell, rowTotal[iDataCol]);
                                        }
                                        #endregion

                                        #region 依Table啟始列的CellStyle設定CellStyle
                                        if (iDataCol < csSampleTotal.Length)
                                        {
                                            npoiRow.GetCell(locate.CelIndex + iDataCol).CellStyle = csSampleTotal[iDataCol];
                                        }
                                        #endregion
                                    }
                                    #endregion

                                    rowTotal = sTable.NewRow();
                                }
                            }
                            #endregion
                        }
                        #endregion

                        //調整列數位移量
                        intShiftRowCount += (sTable.Rows.Count - 1);
                    }
                }
                #endregion

                if (only_index_sheet) break;
            }
            #endregion

            #region 寫入檔案
            #region 沒有設定匯出檔名的話，依：「範本檔名-yyyMMddHHmmss.xls」格式輸出
            if (string.IsNullOrEmpty(outputFileName))
            {
                FileInfo fi = new FileInfo(HttpContext.Current.Server.MapPath(strSampleFileFullName));
                outputFileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);
            }
            #region 設定匯出檔副檔名
            if (outputFileName.EndsWith(strExcelExtension) == false) outputFileName += strExcelExtension;
            #endregion

            #endregion

            using (FileStream fs = new FileStream(outputFolder + outputFileName, FileMode.Create))
            {
                workbook.Write(fs);
                fs.Close();
            }
            #endregion

            if (isResponse)
            {
                //另開視窗匯出至Client
                BasePage.popDownload(outputFolder, outputFileName, ToDownloadSession);
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw;
        }

        return outputFolder + outputFileName;
    }

    /// <summary>
    /// 取得資料表某一列的GroupKey名稱
    /// </summary>
    /// <param name="listColName"></param>
    /// <param name="dt"></param>
    /// <param name="indexRow"></param>
    /// <returns></returns>
    private static string getGroupKey(List<string> listColName, DataTable dt, int indexRow)
    {
        string strReturn = string.Empty;
        try
        {
            foreach (string strColName in listColName)
            {
                if (dt.Columns.Contains(strColName) && indexRow <= dt.Rows.Count - 1)
                {
                    strReturn = BasePage.formatNoteBySymbol(strReturn, dt.Rows[indexRow][strColName], "-", true);
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }

        return strReturn;
    }

    /// <summary>
    /// Excel插入列
    /// </summary>
    /// <param name="sheet"></param>
    /// <param name="fromRowIndex"></param>
    /// <param name="rowCount"></param>
    /// <param name="copyFormula"></param>
    private static void InsertRows(ref ISheet sheet, int fromRowIndex, int rowCount, bool copyFormula = false)
    {
        try
        {
            if (rowCount == 0) return;

            sheet.ShiftRows(fromRowIndex, sheet.LastRowNum, rowCount, true, false);

            IRow rowSource = sheet.GetRow(fromRowIndex + rowCount);
            if (rowSource != null)
            {
                for (int rowIndex = fromRowIndex; rowIndex < fromRowIndex + rowCount; rowIndex++)
                {
                    IRow rowInsert = sheet.CreateRow(rowIndex);
                    int intRelative = rowInsert.RowNum - rowSource.RowNum;      //公式計算，對應的相對列數
                    rowInsert.Height = rowSource.Height;        //設定列高
                    for (int colIndex = 0; colIndex < rowSource.LastCellNum; colIndex++)
                    {
                        ICell cellSource = rowSource.GetCell(colIndex);
                        ICell cellInsert = rowInsert.CreateCell(colIndex);
                        if (cellSource != null)
                        {
                            cellInsert.CellStyle = cellSource.CellStyle;

                            if (cellSource.CellType == CellType.Formula)
                            {
                                cellInsert.SetCellType(cellSource.CellType);

                                if (copyFormula)
                                {
                                    #region 更正公式，改成對應的列數
                                    string strFormula = cellSource.CellFormula;
                                    Match m = Regex.Match(strFormula, @"[\d]+");
                                    strFormula = strFormula.Replace(m.Value, Convert.ToString(Convert.ToInt32(m.Value) + intRelative));

                                    cellInsert.SetCellFormula(strFormula);
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }
    }

    /// <summary>
    /// 塞入Excel的Cell(未處理到的型別直接先以string處理)
    /// </summary>
    /// <param name="cell"></param>
    /// <param name="obj"></param>
    private static void setCellValue(ICell cell, object obj)
    {
        try
        {
            if (obj == null) return;

            switch (obj.GetType().Name)
            {
                case "String":
                    cell.SetCellValue(Convert.ToString(obj));
                    break;
                case "DateTime":
                    cell.SetCellValue(Convert.ToDateTime(obj));
                    break;
                case "Int32":
                    cell.SetCellValue(Convert.ToInt32(obj));
                    break;
                case "Decimal":
                case "Double":
                    cell.SetCellValue(Convert.ToDouble(obj));
                    break;
                default:
                    cell.SetCellValue(Convert.ToString(obj));
                    break;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// 取得Excel的Cell的值
    /// </summary>
    /// <param name="cell"></param>
    /// <returns></returns>
    private static object getCellValue(ICell cell)
    {
        object objReturn = null;
        try
        {
            if (cell != null)
            {
                switch (cell.CellType)
                {
                    case NPOI.SS.UserModel.CellType.Blank:
                        break;
                    case NPOI.SS.UserModel.CellType.Boolean:
                        objReturn = cell.BooleanCellValue;
                        break;
                    case NPOI.SS.UserModel.CellType.Error:
                        objReturn = cell.ErrorCellValue;
                        break;
                    case NPOI.SS.UserModel.CellType.Formula:
                        objReturn = cell.CellFormula;
                        break;
                    case NPOI.SS.UserModel.CellType.Numeric:
                        //NPOI中数字和日期都是NUMERIC类型的，这里对其进行判断是否是日期类型
                        if (DateUtil.IsCellDateFormatted(cell))//日期类型
                        {
                            objReturn = cell.DateCellValue;
                        }
                        else//其他数字类型
                        {
                            objReturn = cell.NumericCellValue;
                        }
                        break;
                    case NPOI.SS.UserModel.CellType.String:
                        objReturn = cell.RichStringCellValue;
                        break;
                    case NPOI.SS.UserModel.CellType.Unknown:
                        objReturn = cell.StringCellValue;
                        break;
                    default:
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return objReturn;
    }

    /// <summary>
    /// 替換##para參數成指令的值
    /// </summary>
    /// <param name="hashPara"></param>
    /// <param name="objOri"></param>
    /// <returns></returns>
    private static string chkParaValue(Hashtable hashPara, object objOri)
    {
        string strReturn = BasePage.checkGetString(objOri);
        try
        {
            foreach (DictionaryEntry entry in hashPara)
            {
                if (strReturn.IndexOf(BasePage.checkGetString(entry.Key)) > -1)
                {
                    strReturn = strReturn.Replace(BasePage.checkGetString(entry.Key), BasePage.checkGetString(entry.Value));
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return strReturn;
    }

    /// <summary>
    /// IRow Copy Command
    ///
    /// Description:  Inserts a existing row into a new row, will automatically push down
    ///               any existing rows.  Copy is done cell by cell and supports, and the
    ///               command tries to copy all properties available (style, merged cells, values, etc...)
    /// </summary>
    public static void CopyRow(IWorkbook workbook, ISheet worksheet, int sourceRowNum, int destinationRowNum)
    {
        // Get the source / new row
        IRow newRow = worksheet.GetRow(destinationRowNum);
        IRow sourceRow = worksheet.GetRow(sourceRowNum);

        // If the row exist in destination, push down all rows by 1 else create a new row
        if (newRow != null)
        {
            worksheet.ShiftRows(destinationRowNum, worksheet.LastRowNum, 1);
        }
        else
        {
            newRow = worksheet.CreateRow(destinationRowNum);
        }

        // Loop through source columns to add to new row
        ICellStyle newCellStyle = workbook.CreateCellStyle();
        for (int i = 0; i < sourceRow.LastCellNum; i++)
        {
            // Grab a copy of the old/new cell
            ICell oldCell = sourceRow.GetCell(i);
            ICell newCell = newRow.CreateCell(i);

            // If the old cell is null jump to next cell
            if (oldCell == null)
            {
                newCell = null;
                continue;
            }

            // Copy style from old cell and apply to new cell  
            newCellStyle.CloneStyleFrom(oldCell.CellStyle); ;
            newCell.CellStyle = newCellStyle;

            // If there is a cell comment, copy
            if (newCell.CellComment != null) newCell.CellComment = oldCell.CellComment;

            // If there is a cell hyperlink, copy
            if (oldCell.Hyperlink != null) newCell.Hyperlink = oldCell.Hyperlink;

            // Set the cell data type
            newCell.SetCellType(oldCell.CellType);

            // Set the cell data value
            switch (oldCell.CellType)
            {
                case CellType.Blank:
                    newCell.SetCellValue(oldCell.StringCellValue);
                    break;
                case CellType.Boolean:
                    newCell.SetCellValue(oldCell.BooleanCellValue);
                    break;
                case CellType.Error:
                    newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                    break;
                case CellType.Formula:
                    newCell.SetCellFormula(oldCell.CellFormula);
                    break;
                case CellType.Numeric:
                    newCell.SetCellValue(oldCell.NumericCellValue);
                    break;
                case CellType.String:
                    newCell.SetCellValue(oldCell.RichStringCellValue);
                    break;
                case CellType.Unknown:
                    newCell.SetCellValue(oldCell.StringCellValue);
                    break;
            }
        }

        // If there are are any merged regions in the source row, copy to new row
        for (int i = 0; i < worksheet.NumMergedRegions; i++)
        {
            CellRangeAddress cellRangeAddress = worksheet.GetMergedRegion(i);
            if (cellRangeAddress.FirstRow == sourceRow.RowNum)
            {
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                                                                            (newRow.RowNum +
                                                                             (cellRangeAddress.FirstRow -
                                                                              cellRangeAddress.LastRow)),
                                                                            cellRangeAddress.FirstColumn,
                                                                            cellRangeAddress.LastColumn);
                worksheet.AddMergedRegion(newCellRangeAddress);
            }
        }

    }

    /// <summary>
    /// 檢查兩個儲存格是否在同一個合併儲存格內
    /// </summary>
    /// <param name="cell1"></param>
    /// <param name="cell2"></param>
    /// <returns></returns>
    private static bool chkSameMerged(ICell cell1, ICell cell2)
    {
        bool boolReturn = false;
        try
        {
            ISheet sheet = cell1.Sheet;

            for (int i = 0; i < sheet.NumMergedRegions; i++)  // 循环所有合并的单元格
            {
                CellRangeAddress cellRange = sheet.GetMergedRegion(i);

                if (cell1 != null)
                {
                    if (cellRange.FirstColumn <= cell1.ColumnIndex
                        && cell1.ColumnIndex <= cellRange.LastColumn
                        && cellRange.FirstRow <= cell1.RowIndex
                        && cell1.RowIndex <= cellRange.LastRow)
                    {
                        if (cell2 != null)
                        {
                            boolReturn = cellRange.FirstColumn <= cell2.ColumnIndex
                                        && cell2.ColumnIndex <= cellRange.LastColumn
                                        && cellRange.FirstRow <= cell2.RowIndex
                                        && cell2.RowIndex <= cellRange.LastRow;
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return boolReturn;
    }

    private static ICellStyle getHeaderStyle(ref IWorkbook workbook)
    {
        ICellStyle csReturn = null;
        try
        {
            csReturn = workbook.CreateCellStyle();
            csReturn.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;
            csReturn.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
        }
        catch (Exception)
        {
            throw;
        }
        return csReturn;
    }

    private static ICellStyle getContainStyle(ref IWorkbook workbook)
    {
        ICellStyle csReturn = null;
        try
        {
            csReturn = workbook.CreateCellStyle();
            csReturn.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
            csReturn.BottomBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.LeftBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.RightBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
            csReturn.TopBorderColor = NPOI.HSSF.Util.HSSFColor.Grey50Percent.Index;
        }
        catch (Exception)
        {
            throw;
        }
        return csReturn;
    }
}