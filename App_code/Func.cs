﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// 共用函式
/// </summary>
public class Func
{
    public Func()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
        
    }

    /// <summary>
    /// 取得單一欄位資訊
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="Table"></param>
    /// <param name="OrderStr"></param>
    /// <param name="WhereStr"></param>
    /// <param name="ParItem"></param>
    /// <param name="ParVal"></param>
    /// <returns></returns>
    public static string GetRow(string Row, string Table, string OrderStr, string WhereStr, string ParItem, string ParVal)
    {
        string re_txt = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2} {3}", Row, Table, WhereStr, OrderStr);
            if(ParItem.IndexOf("|")>0 && ParVal.IndexOf("|") > 0)
            {
                var ParItemAry = ParItem.Split('|');
                var ParValAry = ParVal.Split('|');
                for (int i=0;i<ParItemAry.Length;i++)
                {
                    cmd.Parameters.AddWithValue("@" + ParItemAry[i], ParValAry[i]);
                }
            }
            else
            {
                if(ParItem!="")
                {
                    cmd.Parameters.AddWithValue("@" + ParItem, ParVal);
                }
            }
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    re_txt = dt.Rows[0][Row].ToString().Trim();
                }
            }
        }
        return re_txt;
    }
    /// <summary>
    /// 取得單一欄位總和資訊
    /// </summary>
    /// <param name="Row"></param>
    /// <param name="Table"></param>
    /// <param name="OrderStr"></param>
    /// <param name="WhereStr"></param>
    /// <param name="ParItem"></param>
    /// <param name="ParVal"></param>
    /// <returns></returns>
    public static string GetRowSum(string Row, string Table, string OrderStr, string WhereStr, string ParItem, string ParVal)
    {
        string re_txt = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = string.Format("SELECT {0} as SumNum FROM {1} WHERE {2} {3}", Row, Table, WhereStr, OrderStr);
            cmd.Parameters.AddWithValue("@" + ParItem, ParVal);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    re_txt = dt.Rows[0]["SumNum"].ToString().Trim();
                }
            }
        }
        return re_txt;
    }
    /// <summary>
    /// 下載EXCEL
    /// </summary>
    /// <param name="ttFilePath"></param>
    /// <param name="ttFileName"></param>
    /// <returns></returns>
    public static string DownloadExcel(string ttFilePath, string ttFileName)
    {
        string re_txt = "";
        FileInfo xpath_file = new FileInfo(ttFilePath + ttFileName);
        // 將傳入的檔名以 FileInfo 來進行解析（只以字串無法做）
        System.Web.HttpContext.Current.Response.Clear(); //清除buffer
        System.Web.HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
        System.Web.HttpContext.Current.Response.Buffer = false;
        System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
        // 檔案類型還有下列幾種"application/pdf"、"application/vnd.ms-excel"、"text/xml"、"text/HTML"、"image/JPEG"、"image/GIF"
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(ttFileName, System.Text.Encoding.UTF8));
        // 考慮 utf-8 檔名問題，以 out_file 設定另存的檔名
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Length", xpath_file.Length.ToString()); //表頭加入檔案大小
        System.Web.HttpContext.Current.Response.WriteFile(xpath_file.FullName);
        // 將檔案輸出
        System.Web.HttpContext.Current.Response.Flush();
        // 強制 Flush buffer 內容
        System.Web.HttpContext.Current.Response.End();
        return re_txt;
    }
    /// <summary>
    /// 取得地址郵遞區號
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    public static string GetZipCode(string address)           //代入三碼郵遞區號，用在API無法辨識時使用
    {
        string result = string.Empty;
        //string patternString = "\"PZip5\":\"";
        //WebRequest wr = WebRequest.Create(@"http://query1.e-can.com.tw:8080/datasnap/rest/tservermt/lookupzip/" + address);
        //HttpWebResponse response = (HttpWebResponse)wr.GetResponse();
        //Stream dataStream = response.GetResponseStream();
        //StreamReader reader = new StreamReader(dataStream);
        //string responseFromServer = reader.ReadToEnd().Replace("{\"result\":[", "").Replace("]}", "");
        ///*int indexForResult = responseFromServer.IndexOf(patternString);           //用Jsonconvert代替
        //result = responseFromServer.Substring(indexForResult + 9, 5);*/

        string apiURL = @"http://query1.e-can.com.tw:8080/datasnap/rest/tservermt/lookupzip/";

        var client = new RestClient(apiURL);

        //   var request = new RestRequest(address);

        var request = new RestRequest("{address}", Method.GET);
        request.AddParameter("address", address, ParameterType.UrlSegment);//處理可能出現的保留字

        var response = client.Get<object>(request);
        try
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var json = JsonConvert.DeserializeObject<lookupzip>(response.Content);

                var rr = json.result.FirstOrDefault();
                if (rr == null)
                {
                    return result;
                    //result.PZip5 = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).PZip5;
                    //result.Area = JsonConvert.DeserializeObject<PelicanCreateReuturnEntity>(responseFromServer).Area;
                    //reader.Close();
                    //dataStream.Close();
                    //response.Close();

                }
                else
                {
                    result = rr.PZip5;

                    int temp;
                    if (result.Length == 5 && int.TryParse(result, out temp))
                    {
                        return result;
                    }
                    else
                    {
                        //result.PZip5 = zip3 + "00";
                        return result;
                    }
                }
            }
            else
            {
                return result;
            }
        }
        catch (Exception)
        {

            return string.Empty;
        }
        
    }

    /// <summary>
    /// 取得地址郵遞區號
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    //public static string GetZipCode(string address)
    //{
    //    var zipcode = string.Empty;
    //    string apiURL = string.Format(@"https://zip5.5432.tw/zip5json.py?adrs={0}", address);
    //    var client = new RestClient(apiURL);
    //    var request = new RestRequest();
    //    //request.AddParameter("adrs", address, ParameterType.UrlSegment);//處理可能出現的保留字
    //    var response = client.Get<object>(request);
    //    if (response.StatusCode == HttpStatusCode.OK)
    //    {
    //        //var json = JsonConvert.DeserializeObject<lookupzip>(response.Content);
    //        DataTable zipdata = JsonConvert.DeserializeObject<DataTable>("[" + response.Content.Trim() + "]");
    //        //zipcode = zipdata.Rows[0]["zipcode6"].ToString();
    //        zipcode = zipdata.Rows[0]["zipcode"].ToString();
    //    }
    //    return zipcode;
    //}
    /// <summary>
    /// 取得新流水號
    /// </summary>
    /// <returns></returns>
    public static string GetPostNumber()
    {
        bool odd = false;

        int oddSum = 1234567.ToString().Sum(c => (odd = !odd) ? c - '0' : 0);
        var result = string.Empty;
        using (SqlCommand sqlPost = new SqlCommand())
        {
            var post_number = 1;
            sqlPost.CommandText = string.Format(@"SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY request_id DESC) as row_id, post_number FROM [Post_request] with(nolock)) P WHERE row_id = 1 ");
            DataTable tbPost = dbAdapter.getDataTable(sqlPost);
            post_number = tbPost.Rows.Count == 0 || tbPost.Rows[0]["post_number"].ToString() == "999999" ? post_number : int.Parse(tbPost.Rows[0]["post_number"].ToString()) + 1;
            result = string.Format("{0,6:000000}", post_number);
        }
        return result;
    }
    /// <summary>
    /// 取得檢查碼
    /// </summary>
    /// <param name="postNumber"></param>
    /// <param name="zipcode"></param>
    /// <returns></returns>
    public static string GetPostNumberCheckCode(string postNumber,string zipcode)
    {
        string postNumberFull = postNumber + "300221" + "18" + zipcode;
        var evenDigit = false;
        int checkEvenSum = 0;
        int checkOddSum = 0;
        var checkCode = string.Empty;
        foreach (var digit in postNumberFull)
        {
            //if (!char.IsDigit(digit))
            //{
            //    return "";
            //}
            if (evenDigit)
            {
                checkEvenSum = checkEvenSum + int.Parse(digit.ToString());
            }
            else
            {
                checkOddSum = checkOddSum + int.Parse(digit.ToString());
            }
            evenDigit = !evenDigit;
        }
        checkCode =  ((10 - (checkEvenSum + checkOddSum * 3) % 10) % 10).ToString();
        return checkCode;
    }
    /// <summary>
    /// 是否為正常配達
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    public static bool IsNormalDelivery(string checkNumber)
    {
        DataTable tbRequest = new DataTable();
        using (SqlCommand sql = new SqlCommand())
        {
            sql.CommandText = string.Format(
            @"Select request_id , check_number , latest_scan_item , latest_arrive_option , 
            ISNULL((Select count(*) From ttDeliveryScanLog B with (nolock) Where A.check_number = B.check_number),0) as counts
            From tcDeliveryRequests A with (nolock)
            Where check_number = '{0}'
            and latest_scan_item = '3' 
            and latest_arrive_option = '3'", checkNumber);
            tbRequest = dbAdapter.getDataTable(sql);
        }
        if (tbRequest.Rows.Count == 0)
        {
            return false;
        }
        else if (int.Parse(tbRequest.Rows[0]["counts"].ToString()) > 0)
        {
            return true;
        }
        return false;
    }
    /// <summary>
    /// 是否已銷單
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    public static bool IsCancelled(string checkNumber)
    {
        DataTable tbRequest = new DataTable();
        using (SqlCommand sql = new SqlCommand())
        {
            sql.CommandText = string.Format(
            @"Select request_id , check_number , cancel_date
            From tcDeliveryRequests  with (nolock)
            Where check_number = '{0}'
            AND cancel_date is not NULL", checkNumber);
            tbRequest = dbAdapter.getDataTable(sql);
        }
        if (tbRequest.Rows.Count > 0)
        {
            return true;
        }
        return false;
    }

    public static bool IsRefused(string checkNumber)
    {
        DataTable tbRequest = new DataTable();
        using (SqlCommand sql = new SqlCommand())
        {
            sql.CommandText = string.Format(
            @"select scan_item, arrive_option,scan_date
                from
                (
                    select  ROW_NUMBER()over(partition by check_number order by cdate desc)　as  ROW_NUMBER　,*
                    from ttDeliveryScanLog
                    where check_number = '{0}'
                ) k
             order by k.ROW_NUMBER asc ", checkNumber);
            tbRequest = dbAdapter.getDataTable(sql);
        }


        if (tbRequest.Rows.Count > 0 && (tbRequest.Rows[0]["scan_item"].ToString() == "4" && tbRequest.Rows[0]["arrive_option"].ToString() == "4"))
        {
            if (tbRequest.Rows.Count > 0 && (tbRequest.Rows[1]["scan_item"].ToString() == "3" && tbRequest.Rows[1]["arrive_option"].ToString() == "4"))
            {
                return true;
            }
        }
        else if (tbRequest.Rows.Count > 0 && (tbRequest.Rows[0]["scan_item"].ToString() =="3" && tbRequest.Rows[0]["arrive_option"].ToString() == "4"))
        {
            return true;
        }
        return false;
    }
    //已經產生拒收單
    public static bool IsRefusedOrder(string checkNumber)
    {
        DataTable tbRequest = new DataTable();
        using (SqlCommand sql = new SqlCommand())
        {
            sql.CommandText = string.Format(
            @"select scan_item, arrive_option
                from
                (
                    select  ROW_NUMBER()over(partition by check_number order by cdate desc)　as  ROW_NUMBER　,*
                    from ttDeliveryScanLog
                    where check_number = '{0}'
                ) k
             where k.ROW_NUMBER = 1", checkNumber);
            tbRequest = dbAdapter.getDataTable(sql);
        }
        if (tbRequest.Rows.Count > 0 && (tbRequest.Rows[0]["scan_item"].ToString() == "3" && tbRequest.Rows[0]["arrive_option"].ToString() == "49"))
        {
            return true;
        }
        return false;
    }


}