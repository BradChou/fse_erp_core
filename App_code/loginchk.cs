﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// BasePage 的基礎類別
/// </summary>
public partial class loginchk : Page
{

    #region 是否登入
    /// <summary>
    /// 是否登入
    /// </summary>
    /// <returns></returns>
    public static bool IsLogin()
    {
        bool isBool = true;
        //if (HttpContext.Current.Session["account_code"] == null || HttpContext.Current.Session["master_code"].ToString() == "" || HttpContext.Current.Session["job_title"] == null || HttpContext.Current.Session["user_name"].ToString() == "")
        if (HttpContext.Current.Session["account_code"] == null || HttpContext.Current.Session["master_code"].ToString() == null || HttpContext.Current.Session["job_title"] == null || HttpContext.Current.Session["user_name"].ToString() == null)
        {
            isBool = false;
        }
        return isBool;
    }

    #endregion

}
