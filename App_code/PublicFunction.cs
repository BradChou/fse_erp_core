﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;

/// <summary>
/// PublicFunction 的摘要描述
/// </summary>
public class PublicFunction
{
    internal const int LOCALE_SYSTEM_DEFAULT = 0x0800;
    internal const int LCMAP_SIMPLIFIED_CHINESE = 0x02000000;
    internal const int LCMAP_TRADITIONAL_CHINESE = 0x04000000;


    /// <summary>
    /// 使用OS的kernel.dll做為簡繁轉換工具，只要有裝OS就可以使用，不用額外引用dll，但只能做逐字轉換，無法進行詞意的轉換
    /// <para>所以無法將電腦轉成計算機</para>
    /// </summary>
    [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern int LCMapString(int Locale, int dwMapFlags, string lpSrcStr, int cchSrc, [Out] string lpDestStr, int cchDest);

    /// <summary>依類型取得標籤列印DataTable</summary>
    /// <param name="search"></param>
    /// <returns></returns>
    public DataTable GetDataTableForList(LableListData search)
    {
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = "";

                switch (search.type)
                {
                    case 2:
                        #region 列印標籤資訊
                        strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS rowid
                                         ,CONVERT(CHAR(10), dr.print_date,111) print_date,dr.check_number
                                         ,CASE WHEN cus.supplier_code IN('001','002') THEN REPLACE(REPLACE(cus.customer_shortname,'站',''),'HCT','') ELSE dr.supplier_name  END sendsite --'發送站'
                                         ,send_contact
                                         ,dr.receive_contact
                                         ,CASE WHEN (ISNULL(dr.receive_tel1_ext,'')='') THEN receive_tel1 ELSE receive_tel1 + ' #' + dr.receive_tel1_ext END receive_tel1
                                         ,dr.receive_city,dr.receive_area
                                         ,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
                                         ,ISNULL(dr.receive_city,'') + ISNULL(dr.receive_area,'') + ISNULL(dr.receive_address,'') receive_addr
                                         ,dr.supplier_code,sup.supplier_name,_special.code_name
                                         ,CONVERT(CHAR(10),dr.supplier_date,111) supplier_date
                                         ,CASE dr.receipt_flag WHEN 1 THEN 1 ELSE 0 END receipt_flag 
                                         ,_fee.supplier_fee + ISNULL(dr.remote_fee,'') ship_fee --'貨件運費'
                                         ,dr.customer_code
                                         ,dr.uniform_numbers
                                         ,NULL account_type --'月結／現收'
                                         ,_ticket.code_name ticket -- '傳票區分'
                                         ,dr.collection_money
                                         ,NULL account_date --'入帳日'
                                         ,_arr.arrive_state --'配達狀況
                                         ,_arr.arrive_date  --'配達時間
                                         ,_arr.arrive_item  arrive_item -- '配送異常說明
                                         ,CASE _arr.arrive_scan WHEN 1 THEN '是' ELSE '否' END arrive_scan --是否已掃瞄
                                         ,'','',''
                                         ,_fee.cscetion_fee -- 配送費用'    
                                    FROM tcDeliveryRequests dr With(Nolock)
                                    LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.supplier_code = cus.supplier_code AND cus.supplier_code IN('001','002')
                                   CROSS APPLY dbo.fu_GetShipFeeByCheckNumber(dr.check_number) _fee 
                                   CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
                                   WHERE CONVERT(CHAR(10),dr.supplier_date,111) BETWEEN @dateS AND @dateE 
                                     AND (dr.supplier_code = @supplier OR @supplier = '0')                                     
                                     AND dr.supplier_date IS NOT NULL
                                     AND (@keyword = '0'
                                      OR ISNULL(dr.check_number,'') LIKE '%' + @keyword + '%'
                                      OR ISNULL(dr.receive_contact,'') LIKE '%' + @keyword + '%'
                                      OR ISNULL(dr.send_contact,'') LIKE '%' + @keyword + '%'
                                      ) ";

                        if (!search.check_all && search.check_item.Split(',').Length > 0)
                        {
                            strSQL += " AND dr.request_id IN (" + search.check_item + ") ";
                        }



                        #endregion

                        break;
                    default:
                        #region 網頁list顯示
                        strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS rowid
                                         ,CONVERT(CHAR(10), dr.print_date,111) print_date,dr.check_number
                                         ,CASE WHEN cus.supplier_code IN('001','002') THEN REPLACE(REPLACE(cus.customer_shortname,'站',''),'HCT','') ELSE dr.supplier_name  END sendsite --'發送站'
                                         ,send_contact
                                         ,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
                                         ,ISNULL(dr.supplier_code,'') +' '+ISNULL(sup.supplier_name,'') 'supplier' 
                                         ,CONVERT(CHAR(10),dr.supplier_date,111) supplier_date                                         
                                         ,_fee.supplier_fee + ISNULL(dr.remote_fee,'') ship_fee --'貨件運費'
                                         ,dr.collection_money
                                         ,_arr.arrive_state --配達狀況 
                                         ,_arr.arrive_date  --配達時間 
                                         ,_arr.arrive_item  arrive_item -- 配送異常說明 
                                         ,_fee.cscetion_fee -- 配送費用 
                                         ,CASE WHEN (Len(dr.receive_contact)>2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'+ SUBSTRING(dr.receive_contact,3,Len(dr.receive_contact)) 
                                               WHEN (Len(dr.receive_contact)=2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'
                                          ELSE dr.receive_contact END 'receive_contact',dr.request_id  
                                    FROM tcDeliveryRequests dr With(Nolock)
                                    LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.supplier_code = cus.supplier_code AND cus.supplier_code IN('001','002')
                                   CROSS APPLY dbo.fu_GetShipFeeByCheckNumber(dr.check_number) _fee 
                                   CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
                                   WHERE CONVERT(CHAR(10),dr.supplier_date,111) BETWEEN @dateS AND　@dateE
                                     AND (dr.supplier_code = @supplier OR @supplier = '0')
                                     AND dr.supplier_date IS NOT NULL
                                     AND (@keyword = '0'
                                      OR ISNULL(dr.check_number,'') LIKE '%' + @keyword + '%'
                                      OR ISNULL(dr.receive_contact,'') LIKE '%' + @keyword + '%'
                                      OR ISNULL(dr.send_contact,'') LIKE '%' + @keyword + '%'
                                      )  ";

                        #endregion
                        break;
                }

                strSQL += " ORDER BY dr.supplier_date ;";
                cmd.CommandText = strSQL;
                cmd.Parameters.AddWithValue("@supplier", search.supplyer);
                cmd.Parameters.AddWithValue("@dateS", search.date_start.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", search.date_end.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@keyword", search.keyword);
                dt = dbAdapter.getDataTable(cmd);
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            dt.Dispose();
        }
        return dt;

    }


    /// <summary>記錄系統LOG檔</summary>
    /// <param name="strLog">LOG文字</param>
    /// <param name="strType">關鍵字:S:SYSLOG ; U:USERLOG ; Default</param>
    public void Log(string strLog, string strType)
    {

        lock (new object())
        {
            string strPath = ConfigurationManager.AppSettings["LogPath"];    //"D:\\JunFu\\JunFuLog\\";
            //string strPath = "E:\\JunFu\\JunFuLog\\";
            string strFilePath = "";
            switch (strType)
            {

                case "S":
                    strFilePath = strPath + "SYSLOG" + GetYMD8() + ".txt";
                    break;
                case "U":
                    strFilePath = strPath + "USERLOG" + GetYMD8() + ".txt";
                    break;
                default:
                    strFilePath = strPath + GetYMD8() + ".txt";
                    break;
            }
            if (!Directory.Exists(strPath))
            {
                Directory.CreateDirectory(strPath);
            }

            StreamWriter sw = File.AppendText(strFilePath);
            LogRecord(strLog, sw);
            sw.Close();

        }

    }

    public string GetYMD8()
    {
        string strValue = DateTime.Now.Year.ToString();
        strValue += DateTime.Now.Month.ToString("00");
        strValue += DateTime.Now.Day.ToString("00");
        return strValue;
    }

    public static void LogRecord(String strLog, TextWriter tw)
    {
        tw.Write("\r\nLog : ");
        tw.WriteLine("{0} {1}  Event  {2}", DateTime.Now.ToLongTimeString(),
            DateTime.Now.ToLongDateString(), strLog);
        tw.Flush();
    }


    #region rdlc 產生報表用
    public static void ShowLocalReport_PDF(Page pg, string ReportPath, string ReportName, string[] ParsName, string[] ParsValue, string reportSourceName, DataTable dt)
    {
        Microsoft.Reporting.WebForms.ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();

        rv.LocalReport.ReportPath = string.Format(@"{0}\{1}.rdlc", ReportPath, ReportName);
        Microsoft.Reporting.WebForms.ReportParameter[] parameters = new Microsoft.Reporting.WebForms.ReportParameter[ParsName.Length];
        for (int i = 0; i < ParsName.Length; i++)
        {
            parameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(ParsName[i].ToString(), ParsValue[i].ToString());
        }
        rv.LocalReport.SetParameters(parameters);
        rv.LocalReport.DataSources.Clear();
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource(reportSourceName, dt));


        Microsoft.Reporting.WebForms.Warning[] tWarnings;
        string[] tStreamids;
        string tMimeType;
        string tEncoding;
        string tExtension;

        //呼叫ReportViewer.LoadReport的Render function，將資料轉成想要轉換的格式，並產生成Byte資料  

        byte[] tBytes = rv.LocalReport.Render("PDF", null, out tMimeType, out tEncoding, out tExtension, out tStreamids, out tWarnings);
        string ss = HttpUtility.UrlEncode(ReportName + ".pdf", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client  

        pg.Response.Clear();
        pg.Response.ContentType = tMimeType;
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));


        pg.Response.BinaryWrite(tBytes);
        pg.Response.End();

        String ED = DateTime.Today.ToString("yyyy/MM/dd HH:mm:ss");
    }

    public static void ShowLocalReport_PDF1(Page pg, string ReportPath, string ReportName, string[] ParsName, string[] ParsValue, string reportSourceName, DataSet ds)
    {
        Microsoft.Reporting.WebForms.ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();

        rv.LocalReport.ReportPath = string.Format(@"{0}\{1}.rdlc", ReportPath, ReportName);
        Microsoft.Reporting.WebForms.ReportParameter[] parameters = new Microsoft.Reporting.WebForms.ReportParameter[ParsName.Length];
        for (int i = 0; i < ParsName.Length; i++)
        {
            parameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(ParsName[i].ToString(), ParsValue[i].ToString());
        }
        rv.LocalReport.SetParameters(parameters);
        rv.LocalReport.DataSources.Clear();
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL1", ds.Tables[0]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL2", ds.Tables[1]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL3", ds.Tables[2]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL4", ds.Tables[3]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL5", ds.Tables[4]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL6", ds.Tables[5]));


        Microsoft.Reporting.WebForms.Warning[] tWarnings;
        string[] tStreamids;
        string tMimeType;
        string tEncoding;
        string tExtension;

        //呼叫ReportViewer.LoadReport的Render function，將資料轉成想要轉換的格式，並產生成Byte資料  

        byte[] tBytes = rv.LocalReport.Render("PDF", null, out tMimeType, out tEncoding, out tExtension, out tStreamids, out tWarnings);
        string ss = HttpUtility.UrlEncode(ReportName + ".pdf", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client  

        pg.Response.Clear();
        pg.Response.ContentType = tMimeType;
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));


        pg.Response.BinaryWrite(tBytes);
        pg.Response.End();

        String ED = DateTime.Today.ToString("yyyy/MM/dd HH:mm:ss");
    }


    public static void ShowLocalReport_EXCEL(Page pg, string ReportPath, string ReportName, string[] ParsName, string[] ParsValue, string reportSourceName, DataTable dt)
    {
        Microsoft.Reporting.WebForms.ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();
        rv.LocalReport.ReportPath = string.Format(@"{0}\{1}.rdlc", ReportPath, ReportName);
        Microsoft.Reporting.WebForms.ReportParameter[] parameters = new Microsoft.Reporting.WebForms.ReportParameter[ParsName.Length];
        for (int i = 0; i < ParsName.Length; i++)
        {
            parameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(ParsName[i].ToString(), ParsValue[i].ToString());
        }
        rv.LocalReport.SetParameters(parameters);
        rv.LocalReport.DataSources.Clear();
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource(reportSourceName, dt));

        Microsoft.Reporting.WebForms.Warning[] tWarnings;
        string[] tStreamids;
        string tMimeType;
        string tEncoding;
        string tExtension;

        //呼叫ReportViewer.LoadReport的Render function，將資料轉成想要轉換的格式，並產生成Byte資料  

        byte[] tBytes = rv.LocalReport.Render("EXCEL", null, out tMimeType, out tEncoding, out tExtension, out tStreamids, out tWarnings);
        string ss = HttpUtility.UrlEncode(ReportName + ".xls", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client  
        pg.Response.Clear();
        pg.Response.ContentType = tMimeType;
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));


        pg.Response.BinaryWrite(tBytes);

        pg.Response.End();

    }

    public static void Show_CSV(Page pg, string ReportName, DataTable dt, string title)
    {
        string res = "";
        res += title + "\n";
        res += string.Join(Environment.NewLine, dt.Rows.OfType<DataRow>().Select(x => string.Join(",", x.ItemArray))); // datatable to string

        byte[] byteArray = System.Text.Encoding.Default.GetBytes(res); // string to byte

        string ss = HttpUtility.UrlEncode(ReportName + ".csv", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client          

        pg.Response.Clear();
        pg.Response.ContentType = "application/CSV";
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));
        pg.Response.BinaryWrite(byteArray);
        pg.Response.End();
    }


    #endregion

    #region
    /// <summary>
    /// 簡體轉繁體
    /// </summary>
    /// <param name="pSource">要轉換的繁體字：體</param>
    /// <returns>轉換後的簡體字：體</returns>
    public static string ToTraditional(string pSource)
    {
        String tTarget = new String(' ', pSource.Length);
        int tReturn = LCMapString(LOCALE_SYSTEM_DEFAULT, LCMAP_TRADITIONAL_CHINESE, pSource, pSource.Length, tTarget, pSource.Length);
        return tTarget;
    }

    #endregion

    //使用者操作紀錄
    public void ExeOpLog(string account_code, string UseItem, string Memo)
    {
        SqlCommand cmdInsert = new SqlCommand();
        cmdInsert.Parameters.AddWithValue("@account_code", account_code);       //操作人員
        cmdInsert.Parameters.AddWithValue("@cdate", DateTime.Now);             //操作時間
        cmdInsert.Parameters.AddWithValue("@UseItem", UseItem);                 //操作項目
        cmdInsert.Parameters.AddWithValue("@Memo", Memo);                       //操作說明
        cmdInsert.Parameters.AddWithValue("@IP", GetIP());                      //操作者ip
        cmdInsert.CommandText = dbAdapter.SQLdosomething("ttUseLog", cmdInsert, "insert");
        dbAdapter.execNonQuery(cmdInsert);
    }

    /// <summary>取得登入者IP </summary>
    /// <returns>IPv4 address</returns>
    public static string GetIP()
    {
        String name = Dns.GetHostName();
        IPAddress[] ip = Dns.GetHostEntry(name).AddressList;
        string str_thisIP = string.Empty;
        for (int i = 0; i < ip.Length; i++)
        {
            if (ip[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                str_thisIP = ip[i].ToString();
            }
        }
        return str_thisIP;
    }
    public void RepairRoundtrip_checknumber(string check_number, string roundtrip_checknumber)
    {
        using (SqlCommand sqlCommand2 = new SqlCommand())
        {
            sqlCommand2.Parameters.AddWithValue("@roundtrip_checknumber", roundtrip_checknumber);
            sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "check_number", check_number);
            sqlCommand2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", sqlCommand2);
            dbAdapter.execNonQuery(sqlCommand2);
        }
    }

}

public class LableListData
{
    /// <summary>1:網頁list顯示 2.列印標籤資訊</summary>
    public int type { get; set; }
    /// <summary>區配商 </summary>
    public string supplyer { get; set; }

    public DateTime date_start { get; set; }
    public DateTime date_end { get; set; }

    /// <summary>貨號、寄件人、收貨人(where條件)</summary>
    public String keyword { get; set; }
    /// <summary>自選項目(ex:5,6,8)</summary>
    public String check_item { get; set; }
    /// <summary>全選 </summary>
    public Boolean check_all { get; set; }


    public LableListData()
    {
        type = 1;
        check_item = "";
        check_all = false;
        keyword = "0";
    }

}

public static class DateTimeExtensions
{
    static GregorianCalendar _gc = new GregorianCalendar();
    /// <summary>取得指定日期於當月的週數 </summary>
    public static int GetWeekOfMonth(this DateTime time)
    {
        DateTime first = new DateTime(time.Year, time.Month, 1);
        return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
    }

    /// <summary>取得指定日期於年度週數 </summary>
    static int GetWeekOfYear(this DateTime time)
    {
        return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
    }

    /// <summary>取得當月最大天數 </summary>
    public static int GetDaysOfMonth(this DateTime time)
    {
        DateTime first = new DateTime(time.Year, time.Month, 1);
        DateTime dt_last = new DateTime(first.Year, first.Month, 01).AddMonths(1).AddDays(-1); //本最後一天
        return dt_last.Day;
    }

    /// <summary>取得本月總週數</summary>
    public static int GetWeekNumOfMonth(this DateTime time)
    {
        int i_DayMax = time.GetDaysOfMonth();//本月總天數       
        DateTime dt_last = new DateTime(time.Year, time.Month, i_DayMax); //本最後一天
        return dt_last.GetWeekOfMonth();
    }



    /// <summary>取得本週每週的天數 (陣列長度為該月的總週數)</summary>
    public static int[] GetWeekDaysInMonth(this DateTime time)
    {
        int i_DayMax = time.GetDaysOfMonth();//本月總天數
        //DateTime dt_last = new DateTime(time.Year, time.Month, i_DayMax); //本月最後一天
        //int WeekNumOfMonth = dt_last.GetWeekOfMonth();//本月總週數
        int WeekNumOfMonth = time.GetWeekNumOfMonth();
        int[] weeknum = new int[WeekNumOfMonth];

        DayOfWeek day = new DayOfWeek();
        int i_add = 0;
        for (int i = 0; i < WeekNumOfMonth; i++)
        {
            if (i > 0 && i < (WeekNumOfMonth - 1))
            {
                weeknum[i] = 7;
            }
            else
            {
                i_add = 0;
                if (i == 0)
                {
                    #region first week (該週未日-該月1日)
                    DateTime startDate = new DateTime(time.Year, time.Month, 1);
                    day = _gc.GetDayOfWeek(startDate);
                    for (DayOfWeek _dd = day; _dd <= DayOfWeek.Saturday; _dd++) i_add++;
                    //endDate = startDate.AddDays(i_add - 1);
                    weeknum[i] = i_add;
                    #endregion
                }
                else
                {
                    #region last week (該月未日-該週首日)
                    DateTime endDate = new DateTime(time.Year, time.Month, i_DayMax);//本月最後一天
                    day = _gc.GetDayOfWeek(endDate);
                    for (DayOfWeek _dd = day; _dd >= DayOfWeek.Sunday; _dd--) i_add++;
                    //startDate = endDate.AddDays((i_add - 1) * -1);  
                    #endregion
                }
                weeknum[i] = i_add;
            }
        }

        //驗證:每週天數相加 == 本月總天數
        Boolean IsChk = weeknum.Sum() == i_DayMax;

        return weeknum;
    }




}