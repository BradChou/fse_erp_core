﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;

//Imports System.Linq 
//Imports System.Net
//Imports System.Net.Mail
//Imports System.IO 

/// <summary>
/// MailSend 的摘要描述
/// </summary>
public class MailSend
{
    public MailSend()
    {
        //
        // TODO: 在此加入建構函式的程式碼
        //
    }

	/// <summary>
	/// Send Mail
	/// </summary>
	/// <param name="mailFrom"></param>
	/// <param name="mailTo"></param>
	/// <param name="mailSub"></param>
	/// <param name="mailBody"></param>
	/// <param name="att1"></param>
	/// <param name="att2"></param>
	/// <param name="att3"></param>
	/// <param name="att4"></param>
	/// <param name="att5"></param>
	/// <returns></returns>
    public static string sendMail(string mailFrom, string mailTo, string mailSub, string mailBody, string att1, string att2, string att3, string att4, string att5)
    {
        string strRet = "";
        SmtpClient client = new SmtpClient();
        MailMessage mms = new MailMessage();

        if (mailFrom == "")
            strRet += "無寄件人\\n";
        if (mailTo == "")
            strRet += "無收件人\\n";

        if (strRet == "")
        {
            MailMessage msg = new MailMessage(mailFrom, mailTo);//寄件者，收件者
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            //郵件標題 
            msg.Subject = mailSub;
            //郵件標題編碼  
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            if (att1 != "")
            {
                Attachment amAnnex = new Attachment(att1);
                msg.Attachments.Add(amAnnex);
            }
            if (att2 != "")
            {
                Attachment amAnnex = new Attachment(att2);
                msg.Attachments.Add(amAnnex);
            }
            if (att3 != "")
            {
                Attachment amAnnex = new Attachment(att3);
                msg.Attachments.Add(amAnnex);
            }
            if (att4 != "")
            {
                Attachment amAnnex = new Attachment(att4);
                msg.Attachments.Add(amAnnex);
            }
            if (att5 != "")
            {
                Attachment amAnnex = new Attachment(att5);
                msg.Attachments.Add(amAnnex);
            }

            msg.Body = mailBody;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
            msg.Priority = MailPriority.Normal;//郵件優先級 
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 
            #region 其它 Host
            /*
     *  outlook.com smtp.live.com port:25
     *  yahoo smtp.mail.yahoo.com.tw port:465
    */
            #endregion
            SmtpClient MySmtp = new SmtpClient("smtp.gmail.com", Convert.ToInt16("25"));
            //SmtpClient MySmtp = new SmtpClient("smtp.live.com", Convert.ToInt16("25"));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(mailFrom, "gio951753");
           //MySmtp.Credentials = new System.Net.NetworkCredential(mailFrom, "");
            //Gmial 的 smtp 使用 SSL
            MySmtp.EnableSsl = Convert.ToBoolean(true);
 
            try
            {
                MySmtp.Send(msg);
               // taroko_fun.Log_Txt("【mailTo" + mailTo + "】【mailBody=" + mailBody+"】");
            }
            catch (Exception ex)
            {
                strRet = ex.Message;
              //  taroko_fun.Log_Txt("【mailTo" + mailTo + "】【mailBody=" + mailBody + "】ERROR=" + strRet);
            }

        }
        return strRet;
    }


    /// <summary>
    /// Send Mail
    /// </summary>
    /// <param name="mailFrom"></param>
    /// <param name="mailTo"></param>
    /// <param name="mailSub"></param>
    /// <param name="mailBody"></param>
    /// <param name="att1"></param>
    /// <param name="att2"></param>
    /// <param name="att3"></param>
    /// <param name="att4"></param>
    /// <param name="att5"></param>
    /// <returns></returns>
    public static string sendMail2(string mailFrom, string mailTo, string mailSub, string mailBody, string att1, string att2, string att3, string att4, string att5)
    {
        string strRet = "";
        SmtpClient client = new SmtpClient();
        MailMessage mms = new MailMessage();

        if (mailFrom == "")
            strRet += "無寄件人\\n";
        if (mailTo == "")
            strRet += "無收件人\\n";

        if (strRet == "")
        {
            MailMessage msg = new MailMessage(mailFrom, mailTo);//寄件者，收件者
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            //郵件標題 
            msg.Subject = mailSub;
            //郵件標題編碼  
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            if (att1 != "")
            {
                Attachment amAnnex = new Attachment(att1);
                msg.Attachments.Add(amAnnex);
            }
            if (att2 != "")
            {
                Attachment amAnnex = new Attachment(att2);
                msg.Attachments.Add(amAnnex);
            }
            if (att3 != "")
            {
                Attachment amAnnex = new Attachment(att3);
                msg.Attachments.Add(amAnnex);
            }
            if (att4 != "")
            {
                Attachment amAnnex = new Attachment(att4);
                msg.Attachments.Add(amAnnex);
            }
            if (att5 != "")
            {
                Attachment amAnnex = new Attachment(att5);
                msg.Attachments.Add(amAnnex);
            }

            msg.Body = mailBody;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
            msg.Priority = MailPriority.Normal;//郵件優先級 
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 
            #region 其它 Host
            /*
     *  outlook.com smtp.live.com port:25
     *  yahoo smtp.mail.yahoo.com.tw port:465
    */
            #endregion
            SmtpClient MySmtp = new SmtpClient("127.0.0.1", Convert.ToInt16(25));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(mailFrom, "" );
            //Gmial 的 smtp 使用 SSL
            MySmtp.EnableSsl = false  ; //  Convert.ToBoolean(System.Web.Configuration.WebConfigurationManager.AppSettings["Question_mailsmtpssl"]);

            try
            {
                MySmtp.Send(msg);
            }
            catch (Exception ex)
            {
                strRet = ex.Message;
            }

        }
        return strRet;
    } 
}