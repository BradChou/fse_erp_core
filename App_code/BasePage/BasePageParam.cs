﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public partial class BasePage : Page
{
	#region 變數

	#region 廣域變數

	/// <summary>
	/// A4寬度(參考JSP版)
	/// </summary>
	public static int intA4px = 1050;		//A4寬度(參考JSP版)

	/// <summary>
	/// A4彈出視窗寬度
	/// </summary>
	public static int intPopA4px = 1070;		//A4彈出視窗寬度

	/// <summary>
	/// 依整數、小數位數有值的長度顯示格式
	/// </summary>
	public static string formatAutoDecimal = "{0:0.##########}";

	/// <summary>
	/// 依整數、小數位數有值的長度顯示格式，並加千分位符號
	/// </summary>
	public static string formatAutoDecimalComma = "{0:#,0.##########}";

	/// <summary>
	/// 系統時間及取件時間相差幾小時
	/// </summary>
	public static int intSysGetBuf = 0;

	/// <summary>
	/// 取件起迄時間相差幾小時
	/// </summary>
	public static int intGetStrEndBuf = 1;

	/// <summary>
	/// 送件起迄時間相差幾小時
	/// </summary>
	public static int intSendStrEndBuf = 2;

	/// <summary>
	/// 取件送件相差小時
	/// </summary>
	public static int intGetSendBuf = 1;

	/// <summary>
	/// 取件開始服務時間
	/// </summary>
	public static int intGetTimeS = 9;

	/// <summary>
	/// 取件結束服務時間
	/// </summary>
	public static int intGetTimeE = 20;

	/// <summary>
	/// 送件開始服務時間
	/// </summary>
	public static int intSendTimeS = 9;

	/// <summary>
	/// 送件結束服務時間
	/// </summary>
	public static int intSendTimeE = 22;

	/// <summary>
	/// 預計上線日期
	/// </summary>
	public static DateTime dtOnline
	{
		get
		{
			return new DateTime(2016, 12, 31);
		}
	}

	#endregion

	public static Hashtable hashDefault_Payment_Type = new Hashtable()  {
            {"MC", "月結"},
            {"0", "一般"},
            {"GP", "點數"},
            {"GC", "現金"},
        };



	/// <summary>
	/// 不檢查登入資訊的頁面(沒有登入資訊時需要開啟的頁面)
	/// </summary>
	public static List<string> lisFreeLoginPages = new List<string>(new string[] {		//不檢查登入資訊的頁面(沒有登入資訊時需要開啟的頁面)
														"login",	//登入畫面
														"error",	//錯誤導向畫面
														"test",		//測試
														"join",		//會員註冊
														"tocs",		//CyberSource導向頁
														"fromcs",	//CyberSource回傳頁
														"membership",	//會員條款
														"protection",	//個資保護政策
														"forget",	//忘記密碼
														});

	/// <summary>
	/// 不檢查使用權限的按鈕
	/// </summary>
	private static List<string> lisNoAuthContains = new List<string>(new string[]{	//不檢查使用權限的按鈕
														"btnlogout",	//登出
														});
	/// <summary>
	/// 共用的程式，以CaseType參數傳遞，因此這些開頭的程式TaskId要以以「pageFileName_ILL_KIND」方式表示
	/// </summary>
	private static List<string> lisSharePages = new List<string>(new string[]{			//共用的程式，以CaseType參數傳遞，因此這些開頭的程式TaskId要以「pageFileName_ILL_KIND」方式表示
														});

	/// <summary>
	/// 允許進入登入頁面的合法IP
	/// </summary>
	public static List<string> lisAllowLoginIPs = new List<string>(new string[] {	//允許進入登入頁面的合法IP
														"140.134.48.253",	//GIS
														"219.87.183.2",		//GIS
														"220.130.56.22", 	//GIS
														"211.23.241.241", 	//GIS(中華電信 100MB 光纖，20150819切換上線)
														"140.134.48.249",	//GIS暫時的防火牆(20150527加入)
														"10.56.49.34",		//29內部IP
														"127.0.0.1",		//Local，供遠登進入系統測試用
														});

	/// <summary>
	/// GIS的IP
	/// </summary>
	public static List<string> lisGisIPs = new List<string>(new string[] {	//允許進入登入頁面的合法IP
														"140.134.48.253",	//GIS
														"219.87.183.2",		//GIS
														"220.130.56.22", 	//GIS
														"211.23.241.241", 	//GIS(中華電信 100MB 光纖，20150819切換上線)
														"140.134.48.249",	//GIS暫時的防火牆(20150527加入)
														});

	/// <summary>
	/// 允許免登入進入系統取資料的IP
	/// </summary>
	public static List<string> lisFreeLoginIPs = new List<string>(new string[] {	//允許免登入進入系統取資料的IP
														"10.56.49.38",	//數位河管內部IP
														});

	/// <summary>
	/// 輸出UI會包含的HTML TAG，需置換回去的
	/// </summary>
	public static List<string> listHtmlEncodeTag = new List<string>(new string[] {
														"<br />",
														});

	/// <summary>
	/// 開發電腦名稱
	/// </summary>
	public static List<string> listDevelopServer = new List<string>(new string[] {
														"conan",
														"chris",
														});

	/// <summary>
	/// 常用Split分隔符號","
	/// </summary>
	public static string[] separatorComma = new string[] { "," };

	/// <summary>
	/// Server端拋Confirm訊息給Client端，Client確認後，在畫面元件輸入該文字作確認用
	/// </summary>
	public static string ConfirmFlag = "ConfirmFlag";

	/// <summary>
	/// 先僅開放開發機可以連CyberSource
	/// </summary>
	public static bool flagHidCyberSource
	{
		get
		{
			//TODO: CyberSource上線後，刪除該屬性
			//不能讓人家看到線上刷卡功能orz
			return true;
		}
	}

	#endregion

	#region 屬性

	/// <summary>
	/// 請求網頁名稱
	/// </summary>
	public string pageFileName
	{
		get
		{
			string strFileName = Path.GetFileNameWithoutExtension(Request.FilePath);

			return strFileName;
		}
	}

	/// <summary>
	/// 判斷權限用的TaskId，共用的程式會用「taskId_ILL_KIND」表示
	/// </summary>
	public string taskId
	{
		get
		{
			string strTaskId = Request.FilePath.ToLower();

			return strTaskId;
		}
	}

	/// <summary>
	/// 改以正式機環境連線
	/// </summary>
	public bool ChgToProduction
	{
		set
		{
			Session["ChgToProduction"] = value;
		}
		get
		{
			if (string.IsNullOrEmpty(Convert.ToString(Session["ChgToProduction"])))
			{
				ChgToProduction = false;
			}
			return (bool)Session["ChgToProduction"];
		}
	}



	/// <summary>
	/// 開啟/關閉DB新增資料的Flag，防止頁面重新整理造成重復提交的問題
	/// (注意：Get後將自動關閉！)
	/// </summary>
	public bool OpeningInsFlag
	{
		get
		{
			if (Session["OpeningInsFlag"] == null)
			{
				OpeningInsFlag = false;
			}
			else if (Convert.ToBoolean(Session["OpeningInsFlag"]) == true)
			{
				OpeningInsFlag = false;
				return true;
			}
			return Convert.ToBoolean(Session["OpeningInsFlag"]);
		}
		set
		{
			Session["OpeningInsFlag"] = value;
		}
	}

	/// <summary>
	/// 開啟/關閉DB刪除資料的Flag，防止頁面重新整理造成重復提交的問題
	/// (注意：Get後將自動關閉！)
	/// </summary>
	public bool OpeningDelFlag
	{
		get
		{
			if (Session["OpeningDelFlag"] == null)
			{
				OpeningDelFlag = false;
			}
			else if (Convert.ToBoolean(Session["OpeningDelFlag"]) == true)
			{
				OpeningDelFlag = false;
				return true;
			}
			return Convert.ToBoolean(Session["OpeningDelFlag"]);
		}
		set
		{
			Session["OpeningDelFlag"] = value;
		}
	}

	/// <summary>
	/// 開啟/關閉DB刪除資料的Flag，主要用在Server端控制confirm後刪除資料用
	/// (注意：Get後將自動關閉！)
	/// </summary>
	public bool ToConfirmFlag
	{
		get
		{
			if (Session["ToConfirmFlag"] == null)
			{
				ToConfirmFlag = true;
			}
			else if (Convert.ToBoolean(Session["ToConfirmFlag"]) == false)
			{
				ToConfirmFlag = true;
				return false;
			}
			return Convert.ToBoolean(Session["ToConfirmFlag"]);
		}
		set
		{
			Session["ToConfirmFlag"] = value;
		}
	}



	#endregion
}