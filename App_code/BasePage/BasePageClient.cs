﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;
using System.Drawing;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public partial class BasePage : Page
{
	/// <summary>
	/// 將檔案以Buffer方式匯出
	/// </summary>
	/// <param name="strFileName"></param>
	public static void responseBuffer(string strPath, object objFileName)
	{
		string strContentType = string.Empty;

		try
		{
			string strDownloadURL = formatDownloadURL(strPath, objFileName);
			HttpContext.Current.Response.Redirect(strDownloadURL, false);
		}
		catch (Exception)
		{
			//重新導向會Error因此直接不處理Exception
		}
	}


	

	#region JavaScript

	/// <summary>
	/// 注冊Client端JavaScript
	/// </summary>
	/// <param name="qJavaScript"></param>
    public static void clientRegisterJS(string qJavaScript)
	{
        try
        {
            ScriptManager.RegisterStartupScript((Page)HttpContext.Current.CurrentHandler, ((Page)HttpContext.Current.CurrentHandler).GetType(), Guid.NewGuid().ToString(), qJavaScript, true);
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }
	}

	/// <summary>
	/// 注冊Client端JavaScript
	/// </summary>
	/// <param name="qJavaScript"></param>
	/// <param name="con"></param>
	public static void clientRegisterJS(string qJavaScript, Control con)
	{
		try
		{
			ScriptManager.RegisterStartupScript(con, con.GetType(), Guid.NewGuid().ToString(), qJavaScript, true);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}


	/// <summary>
	/// Client端顯示Alert訊息
	/// </summary>
	/// <param name="strMsg"></param>
	public void clientAlertMsg(object objMsg)
	{
		string strMsg = string.Empty;
		try
		{
			BasePage.clientAlertMsg(objMsg, this.Page);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// Client端顯示Alert訊息
	/// </summary>
	/// <param name="strMsg"></param>
	/// <param name="con"></param>
	public static void clientAlertMsg(object objMsg, Control con)
	{
		string strMsg = string.Empty;
		try
		{
			strMsg = JSStringEscape(checkGetString(objMsg), true);
			string strJavaScript = string.Format(@"
                        $(function() {{
                            successAlert('{0}');
                        }});", strMsg);
			clientRegisterJS(strJavaScript, con);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}


    /// <summary>
    /// 置換特殊字元之後的字串
    /// </summary>
    /// <param name="raw"></param>
    /// <param name="inHtmlAttribute"></param>
    /// <returns></returns>
    public static string JSStringEscape(string raw, bool inHtmlAttribute)
    {
        string strReturn = raw;
        try
        {
            strReturn = strReturn.Replace("\r\n", "\\n").Replace("\r", "").Replace("\n", "\\n");
            if (inHtmlAttribute)
            {
                strReturn = strReturn.Replace("\"", " &quot;").Replace("'", " \\'");
            }
            else
            {
                strReturn = strReturn.Replace("'", " \\'").Replace("\"", " \\\"");
            }
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }
        return checkGetHtmlEncode(strReturn);
    }



	/// <summary>
	/// Client端顯示Alert Exception訊息
	/// </summary>
	/// <param name="con"></param>
	/// <param name="ex"></param>
	public static void clientPopErrorMsg(Control con, Exception ex)
	{
		ExceptionAdapter.logSysException(ex);		//記錄

		string strMsg = ExceptionAdapter.getAlertMsg(ex);

		clientAlertMsg(strMsg, con);
	}

	#endregion

	#region 畫面元件的控制

	/// <summary>
	/// 附加元件的class
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strCssName"></param>
	public static void appendControlCss(Control con, string strCssName)
	{
		try
		{
			if (con.GetType() == typeof(Button))
			{
				string[] aryClass = ((Button)con).CssClass.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				List<string> listClass = new List<string>(aryClass);
				listClass.Add(strCssName);
				((Button)con).CssClass = String.Join(" ", listClass);
			}
			else if (con.GetType() == typeof(HtmlInputButton))
			{
				string[] aryClass = ((HtmlInputButton)con).Attributes["class"].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				List<string> listClass = new List<string>(aryClass);
				listClass.Add(strCssName);
				((HtmlInputButton)con).Attributes["class"] = String.Join(" ", listClass);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	/// <summary>
	/// 移除元件的class
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strCssName"></param>
	public static void removeControlCss(Control con, string strCssName)
	{
		try
		{
			if (con.GetType() == typeof(Button))
			{
				string[] aryClass = ((Button)con).CssClass.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				List<string> listClass = new List<string>(aryClass);
				listClass.Remove(strCssName);
				((Button)con).CssClass = String.Join(" ", listClass);
			}
			else if (con.GetType() == typeof(HtmlInputButton))
			{
				string[] aryClass = ((HtmlInputButton)con).Attributes["class"].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				List<string> listClass = new List<string>(aryClass);
				listClass.Remove(strCssName);
				((HtmlInputButton)con).Attributes["class"] = String.Join(" ", listClass);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	/// <summary>
	/// 清除元件中的值
	/// </summary>
	/// <param name="conToClears"></param>
	public static void clrControlVal(params Control[] conToClears)
	{
		try
		{
			foreach (Control conToClear in conToClears)
			{
				if (conToClear.ViewStateMode == ViewStateMode.Enabled)
				{
					continue;		//ViewStateMode.Enabled不做清除的動作
				}
				else if (conToClear.GetType().Name == "Label")
				{
					((Label)conToClear).Text = "";
				}
				else if (conToClear.GetType().Name == "HiddenField")
				{
					((HiddenField)conToClear).Value = "";
				}
				else if (conToClear.GetType().Name == "TextBox")
				{
					((TextBox)conToClear).Text = "";
				}
				else if (conToClear.GetType().Name == "RadioButton")
				{
					((RadioButton)conToClear).Checked = false;
				}
				else if (conToClear.GetType().Name == "CheckBox")
				{
					((CheckBox)conToClear).Checked = false;
				}
				else if (conToClear.GetType().Name == "HyperLink")
				{
					((HyperLink)conToClear).Text = "";
					((HyperLink)conToClear).NavigateUrl = "";
				}
				else if (conToClear.GetType().Name == "RadioButtonList")
				{
					((RadioButtonList)conToClear).SelectedIndex = -1;
				}
				else if (conToClear.GetType().Name == "DropDownList")
				{
					((DropDownList)conToClear).SelectedIndex = -1;
				}
				else if (conToClear.GetType().Name == "CheckBoxList")
				{
					((CheckBoxList)conToClear).SelectedIndex = -1;
				}
				else if (conToClear.GetType().Name == "FileUpload")
				{
					//怎麼清？
				}
				else if (conToClear.GetType().Name == "GridView")
				{
					((GridView)conToClear).DataSource = null;
					((GridView)conToClear).DataBind();
				}
				else if (conToClear.GetType().Name == "view_usercontrol_uccheckfield_ascx")
				{
					//標題元件，不做清除動作
				}
				else if (conToClear.GetType().Name == "view_usercontrol_ucbankinfo_ascx")
				{
					//銀行資訊元件，不做清除動作
				}
				else if (conToClear.GetType().Name == "view_usercontrol_cuccoorpoints_ascx")
				{
					GridView gvPath = (GridView)conToClear.FindControl("gvPath");
					foreach (GridViewRow gvRow in gvPath.Rows)
					{
						clrControlVal(gvRow.FindControl("ucMapE"));
						clrControlVal(gvRow.FindControl("ucMapN"));
						clrControlVal(gvRow.FindControl("ucTM297E"));
						clrControlVal(gvRow.FindControl("ucTM297N"));
					}
				}
				//else if (conToClear.GetType().Name == "view_usercontrol_cucapplicant_ascx")
				//{
				//    if (conToClear.FindControl("txtPeoname") != null) clrControlVal(conToClear.FindControl("txtPeoname"));
				//    if (conToClear.FindControl("txtHomeId") != null) clrControlVal(conToClear.FindControl("txtHomeId"));
				//    if (conToClear.FindControl("txtPeocid") != null) clrControlVal(conToClear.FindControl("txtPeocid"));
				//    if (conToClear.FindControl("txtAddfull") != null) clrControlVal(conToClear.FindControl("txtAddfull"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_ucdatepicker_ascx")
				//{
				//    if (con.FindControl("txtDate") != null) clrControlVal(con.FindControl("txtDate"));
				//    if (con.FindControl("ddlNoon") != null) clrControlVal(con.FindControl("ddlNoon"));
				//    if (con.FindControl("ddlHour") != null) clrControlVal(con.FindControl("ddlHour"));
				//    if (con.FindControl("ddlMinute") != null) clrControlVal(con.FindControl("ddlMinute"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_cucfileupload_ascx")
				//{
				//    if (con.FindControl("fuFile") != null) clrControlVal(con.FindControl("fuFile"));
				//    if (con.FindControl("linkFIle") != null) clrControlVal(con.FindControl("linkFIle"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_ucddlsysparamdata_ascx")
				//{
				//    if (con.FindControl("ddlParam") != null) clrControlVal(con.FindControl("ddlParam"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_ucchksysparamdata_ascx")
				//{
				//    if (con.FindControl("chkParam") != null) clrControlVal(con.FindControl("chkParam"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_ucradsysparamdata_ascx")
				//{
				//    if (con.FindControl("radParam") != null) clrControlVal(con.FindControl("radParam"));
				//}
				//else if (con.GetType().Name == "view_usercontrol_ucunit_ascx")
				//{
				//    if (con.FindControl("ddlUnit") != null) clrControlVal(con.FindControl("ddlUnit"));
				//}
				else
				{
					foreach (Control con in conToClear.Controls)
					{
						clrControlVal(con);
					}
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 設定物件的值
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="conToSet"></param>
	public static void setControlVal(object objVal, params Control[] conToSet)
	{
		Control con;

		try
		{
			for (int i = 0; i < conToSet.Length; i++)
			{
				con = conToSet[i];

				if (con == null)
				{
					continue;
				}
				else if (con.GetType().Name == "Label")
				{
					((Label)con).Text = checkGetHtmlEncode(objVal);
				}
				else if (con.GetType().Name == "TextBox")
				{
					((TextBox)con).Text = checkGetString(objVal);
				}
				else if (con.GetType().Name == "RadioButton")
				{
					((RadioButton)con).Checked = ((RadioButton)con).Text == checkGetString(objVal);
				}
				else if (con.GetType().Name == "CheckBox")
				{
					((CheckBox)con).Checked = ((CheckBox)con).Text == checkGetString(objVal);
				}
				else if (con.GetType().Name == "RadioButtonList")
				{
					if (((RadioButtonList)con).Items.FindByValue(checkGetString(objVal)) != null)
					{
						((RadioButtonList)con).SelectedIndex = ((RadioButtonList)con).Items.IndexOf(((RadioButtonList)con).Items.FindByValue(checkGetString(objVal)));
					}
				}
				else if (con.GetType().Name == "DropDownList")
				{
					if (((DropDownList)con).Items.FindByValue(checkGetString(objVal)) != null)
					{
						((DropDownList)con).SelectedIndex = ((DropDownList)con).Items.IndexOf(((DropDownList)con).Items.FindByValue(checkGetString(objVal)));
					}
				}
				else if (con.GetType().Name == "CheckBoxList")
				{
					if (((CheckBoxList)con).Items.FindByValue(checkGetString(objVal)) != null)
					{
						((CheckBoxList)con).SelectedIndex = ((CheckBoxList)con).Items.IndexOf(((CheckBoxList)con).Items.FindByValue(checkGetString(objVal)));
					}
				}
				else if (con.GetType().Name == "view_usercontrol_ucdatepicker_ascx")
				{
					DateTime dt = Convert.ToDateTime(objVal);

					if (con.FindControl("txtDate") != null) setControlVal(checkGetDateTime(dt.ToString()), con.FindControl("txtDate"));
					if (con.FindControl("ddlNoon") != null) setControlVal(dt.Hour >= 12 ? "下午" : "上午", con.FindControl("ddlNoon"));
					if (con.FindControl("ddlHour") != null) setControlVal(dt.Hour >= 12 ? (dt.Hour - 12).ToString("00") : (dt.Hour).ToString("00"), con.FindControl("ddlHour"));
					if (con.FindControl("ddlMinute") != null) setControlVal(dt.ToString("mm"), con.FindControl("ddlMinute"));
				}
				else if (con.GetType().Name == "view_usercontrol_ucddlsysparamdata_ascx")
				{
					if (con.FindControl("ddlParam") != null) setControlVal(checkGetString(objVal), con.FindControl("ddlParam"));
				}
				else if (con.GetType().Name == "view_usercontrol_ucchksysparamdata_ascx")
				{
					if (con.FindControl("chkParam") != null) setControlVal(checkGetString(objVal), con.FindControl("chkParam"));
				}
				else if (con.GetType().Name == "view_usercontrol_ucradsysparamdata_ascx")
				{
					if (con.FindControl("radParam") != null) setControlVal(checkGetString(objVal), con.FindControl("radParam"));
				}
				else
				{
					throw new Exception(string.Format("{0} 未定義", con.GetType().Name));
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 顯示元件(Visible=true)
	/// </summary>
	/// <param name="conToShows"></param>
	public static void showControl(params Control[] conToShows)
	{
		try
		{
			foreach (Control conToShow in conToShows)
			{
				conToShow.Visible = true;
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 隱藏元件(Visible=false)
	/// </summary>
	/// <param name="conToHids"></param>
	public static void hidControl(params Control[] conToHids)
	{
		try
		{
			foreach (Control conToHid in conToHids)
			{
				conToHid.Visible = false;
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 隱藏元件(SYSTEM身份不隱藏，只顯示訊息，一般使用者用css隱藏，主要用於不同河川局不允許修改資料等權限的卡控，業務邏輯的儘量不要使用，避免操作時造成錯誤)
	/// </summary>
	/// <param name="conToHids"></param>
	public static void hidControlAuth(params Control[] conToHids)
	{
		string strHiddenTag = "(H)";

		try
		{
			foreach (Control conToHid in conToHids)
			{
				if (conToHid.GetType() == typeof(Button))
				{
					if (((Button)conToHid).Text.EndsWith(strHiddenTag) == false) ((Button)conToHid).Text += strHiddenTag;
				}
				else if (conToHid.GetType() == typeof(HtmlInputButton))
				{
					if (((HtmlInputButton)conToHid).Value.EndsWith(strHiddenTag) == false) ((HtmlInputButton)conToHid).Value += strHiddenTag;
				}
				else
				{
					//其它類別，未來可註冊JS指令，直接在Client端作顯示
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 顯示元件(SYSTEM身份不隱藏，只顯示訊息，一般使用者用css隱藏，主要用於不同河川局不允許修改資料等權限的卡控，業務邏輯的儘量不要使用，避免操作時造成錯誤)
	/// </summary>
	/// <param name="conToHids"></param>
	public static void showControlAuth(params Control[] conToHids)
	{
		string strHiddenTag = "(H)";

		try
		{
			foreach (Control conToHid in conToHids)
			{
				if (conToHid.GetType() == typeof(Button))
				{
					if (((Button)conToHid).Text.EndsWith(strHiddenTag) == true) ((Button)conToHid).Text = ((Button)conToHid).Text.Replace(strHiddenTag, "");
				}
				else if (conToHid.GetType() == typeof(HtmlInputButton))
				{
					if (((HtmlInputButton)conToHid).Value.EndsWith(strHiddenTag) == false) ((HtmlInputButton)conToHid).Value.Replace(strHiddenTag, "");
				}
				else
				{
					//其它類別，未來可註冊JS指令，直接在Client端作顯示
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 元件設成唯讀
	/// </summary>
	/// <param name="conToReadOnlys"></param>
	public static void readonlyControl(params Control[] conToReadOnlys)
	{
		try
		{
			foreach (Control conToReadOnly in conToReadOnlys)
			{
				if (conToReadOnly.GetType().Name == "Label")
				{
				}
				else if (conToReadOnly.GetType().Name == "LiteralControl")
				{
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_uccheckfield_ascx")
				{
				}
				else if (conToReadOnly.GetType().Name == "HiddenField")
				{
				}
				else if (conToReadOnly.GetType().Name == "HyperLink")
				{
				}
				else if (conToReadOnly.GetType().Name == "UpdatePanel")
				{
				}
				else if (conToReadOnly.GetType().Name == "Button")
				{
				}
				else if (conToReadOnly.GetType().Name == "FileUpload")
				{
					((FileUpload)conToReadOnly).Attributes.Add("disabled", "disabled");
				}
				else if (conToReadOnly.GetType().Name == "TextBox")
				{
					((TextBox)conToReadOnly).Attributes.Add("ReadOnly", "ReadOnly");
				}
				else if (conToReadOnly.GetType().Name == "RadioButton")
				{
					clientRegisterJS(string.Format("$('#{0}:radio:not(:checked)').attr('disabled', 'disabled');", conToReadOnly.ClientID), conToReadOnly.Page);
				}
				else if (conToReadOnly.GetType().Name == "CheckBox")
				{
					((CheckBox)conToReadOnly).Attributes.Add("onclick", "return false");
				}
				else if (conToReadOnly.GetType().Name == "RadioButtonList")
				{
					clientRegisterJS(string.Format("$('#{0} :radio:not(:checked)').attr('disabled', 'disabled');", conToReadOnly.ClientID), conToReadOnly.Page);
				}
				else if (conToReadOnly.GetType().Name == "DropDownList")
				{
					((DropDownList)conToReadOnly).Attributes.Add("disabled", "disabled");
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_cucbranch_ascx")
				{
					if (conToReadOnly.FindControl("ddl") != null) readonlyControl(conToReadOnly.FindControl("ddl"));
				}
				else if (conToReadOnly.GetType().Name == "CheckBoxList")
				{
					clientRegisterJS(string.Format("$(function () {{$('#' + $('#{0} label:contains(全選)').attr('for')).unbind();}});", conToReadOnly.ClientID), conToReadOnly.Page);
					clientRegisterJS(string.Format("$(function () {{$('#{0} :checkbox').live('click', function () {{return false;}});}});", conToReadOnly.ClientID), conToReadOnly.Page);
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucdatepicker_ascx")
				{
					if (conToReadOnly.FindControl("txtDate") != null) clientRegisterJS(string.Format("$(function () {{$('#{0}').unbind();}});", conToReadOnly.FindControl("txtDate").ClientID), conToReadOnly.FindControl("txtDate").Page);
					if (conToReadOnly.FindControl("ddlNoon") != null) readonlyControl(conToReadOnly.FindControl("ddlNoon"));
					if (conToReadOnly.FindControl("ddlHour") != null) readonlyControl(conToReadOnly.FindControl("ddlHour"));
					if (conToReadOnly.FindControl("ddlMinute") != null) readonlyControl(conToReadOnly.FindControl("ddlMinute"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucddlsysparamdata_ascx")
				{
					if (conToReadOnly.FindControl("ddlParam") != null) readonlyControl(conToReadOnly.FindControl("ddlParam"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucchksysparamdata_ascx")
				{
					if (conToReadOnly.FindControl("chkParam") != null) readonlyControl(conToReadOnly.FindControl("chkParam"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucradsysparamdata_ascx")
				{
					if (conToReadOnly.FindControl("radParam") != null) readonlyControl(conToReadOnly.FindControl("radParam"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucunit_ascx")
				{
					if (conToReadOnly.FindControl("ddlUnit") != null) readonlyControl(conToReadOnly.FindControl("ddlUnit"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_ucchkunit_ascx")
				{
					if (conToReadOnly.FindControl("chkUnit") != null) readonlyControl(conToReadOnly.FindControl("chkUnit"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_cucchkvocabulary_ascx")
				{
					if (conToReadOnly.FindControl("chkParam") != null) readonlyControl(conToReadOnly.FindControl("chkParam"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_cucfileupload_ascx")
				{
					if (conToReadOnly.FindControl("fuFile") != null) readonlyControl(conToReadOnly.FindControl("fuFile"));
				}
				else if (conToReadOnly.GetType().Name == "view_usercontrol_cucuser_ascx")
				{
					if (conToReadOnly.FindControl("txtUserName") != null)
					{
						((TextBox)conToReadOnly.FindControl("txtUserName")).Attributes.Add("onclick", "return false");
						readonlyControl(conToReadOnly.FindControl("txtUserName"));
					}
					if (conToReadOnly.FindControl("hidUserId") != null)
					{
						readonlyControl(conToReadOnly.FindControl("hidUserId"));
					}
				}
				else if (conToReadOnly.GetType().Name == "GridView")
				{
					GridView gv = ((GridView)conToReadOnly);
					for (int j = 0; j < gv.Rows.Count; j++)
					{
						GridViewRow gvRow = gv.Rows[j];
						for (int k = 0; k < gvRow.Cells.Count; k++)
						{
							TableCell gvCell = gvRow.Cells[k];
							foreach (Control conCellToReadOnly in gvCell.Controls)
							{
								readonlyControl(conCellToReadOnly);
							}
						}
					}
				}
				else
				{
					throw new Exception(string.Format("{0} 未定義", conToReadOnly.GetType().Name));
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	public static void focusControl(Control conToFocus)
	{
		try
		{
			if (conToFocus.GetType().Name == "Label")
			{
			}
			else if (conToFocus.GetType().Name == "TextBox")
			{
				((TextBox)conToFocus).Focus();
			}
			else if (conToFocus.GetType().Name == "DropDownList")
			{
				((DropDownList)conToFocus).Focus();
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	#endregion

	#region 驗證檢核

	/// <summary>
	/// 取得已選擇的ListItem值
	/// </summary>
	/// <param name="liCollection"></param>
	/// <returns></returns>
	public static List<string> getSelValList(ListItemCollection liCollection)
	{
		List<string> listReturn = new List<string>();
		try
		{
			foreach (ListItem item in liCollection)
			{
				if (item.Selected)
				{
					listReturn.Add(item.Value);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return listReturn;
	}

	/// <summary>
	/// 取得已選擇的ListItem文字
	/// </summary>
	/// <param name="liCollection"></param>
	/// <returns></returns>
	public static List<string> getSelTextList(ListItemCollection liCollection)
	{
		List<string> listReturn = new List<string>();
		try
		{
			foreach (ListItem item in liCollection)
			{
				if (item.Selected)
				{
					listReturn.Add(item.Text);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return listReturn;
	}

	/// <summary>
	/// 取得控制項的值
	/// </summary>
	/// <param name="con"></param>
	/// <returns></returns>
	public static string getVal(Control con)
	{
		string strReturn = string.Empty;
		try
		{
			if (con.GetType().Name == "TextBox")
			{
				strReturn = ((TextBox)con).Text;
			}
			else if (con.GetType().Name == "Label")
			{
				strReturn = ((Label)con).Text;
			}
			else if (con.GetType().Name == "HiddenField")
			{
				strReturn = ((HiddenField)con).Value;
			}
			else if (con.GetType().Name == "RadioButton")
			{
				strReturn = ((RadioButton)con).Checked ? Convert.ToString(((RadioButton)con).Checked) : "";
			}
			else if (con.GetType().Name == "RadioButtonList")
			{
				strReturn = ((RadioButtonList)con).SelectedValue;
			}
			else if (con.GetType().Name == "DropDownList")
			{
				strReturn = ((DropDownList)con).SelectedValue;
			}
			else if (con.GetType().Name == "CheckBoxList")
			{
				List<string> listReutrn = getSelValList(((CheckBoxList)con).Items);
				strReturn = string.Join(",", listReutrn);
			}
			else if (con.GetType().Name == "GridView")
			{
				strReturn = checkGetString(((GridView)con).Rows.Count) == "0" ? "" : checkGetString(((GridView)con).Rows.Count);
			}
			else if (con.GetType().Name == "FileUpload")
			{
				if (((FileUpload)con).HasFile)
				{
					strReturn = ((FileUpload)con).FileName;
				}
			}
			else if (con.GetType().Name == "ListBox")
			{
				List<string> listReutrn = getSelValList(((ListBox)con).Items);
				strReturn = string.Join(",", listReutrn);
			}
			else if (con.GetType().Name == "view_usercontrol_cucbranch_ascx")
			{
				strReturn = ((DropDownList)con.FindControl("ddl")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_ucdatepicker_ascx")
			{
				strReturn = ((TextBox)con.FindControl("txtDate")).Text;
			}
			else if (con.GetType().Name == "view_usercontrol_ucddlfeeyear_ascx")
			{
				strReturn = ((DropDownList)con.FindControl("ddlFeeYear")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_ucddlsysparamdata_ascx")
			{
				strReturn = ((DropDownList)con.FindControl("ddlParam")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_ucchksysparamdata_ascx")
			{
				CheckBoxList chkParam = (CheckBoxList)con.FindControl("chkParam");
				List<string> listReutrn = new List<string>();
				foreach (ListItem li in chkParam.Items)
				{
					if (li.Text != "全選" && li.Value != "")
					{
						if (li.Selected) listReutrn.Add(li.Value);
					}
				}
				strReturn = string.Join(",", listReutrn);
			}
			else if (con.GetType().Name == "view_usercontrol_ucradsysparamdata_ascx")
			{
				strReturn = ((RadioButtonList)con.FindControl("radParam")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_ucunit_ascx")
			{
				strReturn = ((DropDownList)con.FindControl("ddlUnit")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_cucaddress_ascx")
			{
				strReturn += ((DropDownList)con.FindControl("ddlCity")).SelectedValue;
				strReturn += ((DropDownList)con.FindControl("ddlCityArea")).SelectedValue;
				strReturn += ((DropDownList)con.FindControl("ddlStreetName")).SelectedValue;
			}
			else if (con.GetType().Name == "view_usercontrol_ucchkunit_ascx")
			{
				CheckBoxList chkParam = (CheckBoxList)con.FindControl("chkUnit");
				List<string> listReutrn = new List<string>();
				foreach (ListItem li in chkParam.Items)
				{
					if (li.Text != "全選" && li.Value != "")
					{
						if (li.Selected) listReutrn.Add(li.Value);
					}
				}
				strReturn = string.Join(",", listReutrn);
			}
			else if (con.GetType().Name == "view_usercontrol_cucuser_ascx")
			{
				strReturn = ((HiddenField)con.FindControl("hidUserId")).Value;
			}
			else if (con.GetType().Name == "view_usercontrol_cucchkvocabulary_ascx")
			{
				CheckBoxList chkParam = ((CheckBoxList)con.FindControl("chkParam"));
				List<string> listReutrn = new List<string>();
				foreach (ListItem li in chkParam.Items)
				{
					if (li.Text != "全選" && li.Value != "")
					{
						if (li.Selected) listReutrn.Add(li.Value);
					}
				}
				strReturn = string.Join(",", listReutrn);
			}
			else if (con.GetType().Name == "view_usercontrol_cucfileupload_ascx")
			{
				FileUpload fuFile = (FileUpload)con.FindControl("fuFile");
				HyperLink linkFIle = (HyperLink)con.FindControl("linkFIle");
				if (fuFile.HasFile)
				{
					strReturn = fuFile.PostedFile.FileName;
				}
				else
				{
					strReturn = linkFIle.NavigateUrl;
				}
			}
			else if (con.GetType().Name == "view_usercontrol_uclocation_ascx")
			{
				if (((DropDownList)con.FindControl("ddlType")).SelectedValue != "") strReturn += ((DropDownList)con.FindControl("ddlType")).SelectedItem.Text;
				if (((DropDownList)con.FindControl("ddlRvid")).SelectedValue != "") strReturn += ((DropDownList)con.FindControl("ddlRvid")).SelectedItem.Text;
				if (((DropDownList)con.FindControl("ddlCntid")).SelectedValue != "") strReturn += ((DropDownList)con.FindControl("ddlCntid")).SelectedItem.Text;
				if (((DropDownList)con.FindControl("ddlTwnid")).SelectedValue != "") strReturn += ((DropDownList)con.FindControl("ddlTwnid")).SelectedItem.Text;
				if (((DropDownList)con.FindControl("ddlSecid")).SelectedValue != "") strReturn += ((DropDownList)con.FindControl("ddlSecid")).SelectedItem.Text;
				strReturn += ((TextBox)con.FindControl("txtMNo")).Text;
				strReturn += ((TextBox)con.FindControl("txtSNo")).Text;
			}
			else if (con.GetType().Name == "view_usercontrol_cucapplicant_ascx")
			{
				strReturn = formatNoteBySymbol(getVal(con.FindControl("ucPeoTyp")), getVal(con.FindControl("txtPeocid")), ",", true);
			}
			else if (con.GetType().Name == "view_usercontrol_cuctextbox_ascx")
			{
				strReturn = ((TextBox)con.FindControl("txt")).Text;
			}
			else if (con.GetType().Name == "view_usercontrol_ucaddress_ascx")
			{
				strReturn = ((Label)con.FindControl("lbAddress")).Text;
			}
			else if (con.GetType().Name == "view_usercontrol_ucddlyyymm_ascx")
			{
				strReturn += ((DropDownList)con.FindControl("ddlYear")).SelectedItem.Text;
				strReturn += ((DropDownList)con.FindControl("ddlMonth")).SelectedItem.Text;
			}
			else if (con.GetType().Name == "view_sur_sur_uccontrol_ucsurveyoption_ascx")
			{
				GridView gv = ((GridView)con.FindControl("gv"));

				#region 解析出ANSWER_TYPE
				string strAnswerType = string.Empty;
				foreach (DataControlField col in gv.Columns)
				{
					if (col.HeaderText.StartsWith("ANSWER_TYPE"))
					{
						if (col.Visible)
						{
							strAnswerType = col.HeaderText.Replace("ANSWER_TYPE", "");
							break;
						}
					}
				}
				#endregion

				foreach (GridViewRow gvRow in gv.Rows)
				{
					#region 勾選的OPTIONS_ID
					if (strAnswerType == "1")
					{
						if (((RadioButton)gvRow.FindControl("rad")).Checked)
						{
							strReturn = formatNoteBySymbol(strReturn, ((RadioButton)gvRow.FindControl("rad")).Text, ",", true);
						}
					}
					else if (strAnswerType == "2")
					{
						if (((CheckBox)gvRow.FindControl("chk")).Checked)
						{
							strReturn = formatNoteBySymbol(strReturn, ((CheckBox)gvRow.FindControl("chk")).Text, ",", true);
						}
					}
					else if (strAnswerType == "3")
					{
						strReturn = formatNoteBySymbol(strReturn, ((TextBox)gvRow.FindControl("txt")).Text, ",", true);
					}
					#endregion
				}
			}
			else if (con.GetType().Name == "view_usercontrol_cucddlvocabulary_ascx")
			{
				strReturn = ((DropDownList)con.FindControl("ddlParam")).SelectedValue;
			}
			else
			{
				throw new Exception(string.Format("{0} 未定義", con.GetType().Name));
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否空白
	/// </summary>
	/// <param name="conCheckFields"></param>
	/// <returns></returns>
	public static string checkField(params Control[] conCheckFields)
	{
		string strReturn = string.Empty;

		try
		{
			foreach (Control conCheckField in conCheckFields)
			{
				List<KeyValuePair<Control, string>> listVerification = new List<KeyValuePair<Control, string>>();

				if (conCheckField.GetType().Name.IndexOf("uccheckfield_ascx") > -1)
				{
					string strForObjId = checkGetString(((Label)conCheckField.FindControl("lblTitle")).Attributes["for"]);
					Control conCheckFieldParent = conCheckField.Parent;
					while (conCheckFieldParent != null
						&& conCheckFieldParent.FindControl(strForObjId) == null	//祖先節點找得到物件的話則離開
						&& conCheckFieldParent.GetType().Name != "GridView"		//祖先節點是GridView則離開，找GridView內的物件檢查
						&& conCheckFieldParent.GetType().Name.ToLower().StartsWith("view_usercontrol") == false	//祖父節點找到UserControl的話則離開
						)
					{
						conCheckFieldParent = conCheckFieldParent.Parent;
					}

					if (conCheckFieldParent != null)
					{
						string tagStart = "<span style='color:Red;'>*</span>";

						//ucFieldHead放在GridView內的話，只檢查GridView內的物件
						if (conCheckFieldParent.GetType().Name == "GridView")
						{
							GridView gvParent = (GridView)conCheckFieldParent;
							for (int i = 0; i < gvParent.Rows.Count; i++)
							{
								GridViewRow gvRow = gvParent.Rows[i];
								if (gvRow.RowType == DataControlRowType.DataRow)
								{
									listVerification.Add(new KeyValuePair<Control, string>(gvRow.FindControl(strForObjId), ((Label)conCheckField.FindControl("lblTitle")).Text.Replace(tagStart, "").Replace(HttpContext.Current.Server.HtmlEncode(tagStart), "") + string.Format("(第{0}列)", i + 1)));
								}
							}
						}
						//ucFieldHead的祖先有找到物件的話，檢查找到的物件
						else if (conCheckFieldParent.FindControl(strForObjId) != null)
						{
							listVerification.Add(new KeyValuePair<Control, string>(conCheckFieldParent.FindControl(strForObjId), ((Label)conCheckField.FindControl("lblTitle")).Text.Replace(tagStart, "").Replace(HttpContext.Current.Server.HtmlEncode(tagStart), "")));
						}
					}

					foreach (KeyValuePair<Control, string> conVerification in listVerification)
					{
						//輸入元件有顯示才檢查
						if (conVerification.Key != null && conVerification.Key.Visible)
						{
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsRequest"]))
							{
								strReturn += checkEmpty(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsNumber"]))
							{
								strReturn += checkNumber(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsAlphanumeric"]))
							{
								strReturn += checkAlphanumeric(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsPosNumber"]))
							{
								strReturn += checkPosNumber(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsEmail"]))
							{
								strReturn += checkEmail(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsTel"]))
							{
								strReturn += checkTel(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsMobile"]))
							{
								strReturn += checkMobile(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsUrl"]))
							{
								strReturn += checkUrl(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsPeoIdNum"]))
							{
								strReturn += checkPeoIdNum(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsComIdNum"]))
							{
								strReturn += checkComIdNum(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsImageFileType"]))
							{
								strReturn += checkImageFileType(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsMapE"]))
							{
								strReturn += checkMapE(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsMapN"]))
							{
								strReturn += checkMapN(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsTm2_97E"]))
							{
								strReturn += checkTm2_97E(conVerification.Key, conVerification.Value);
							}
							if (Convert.ToBoolean(((Label)conCheckField.FindControl("lblTitle")).Attributes["IsTm2_97N"]))
							{
								strReturn += checkTm2_97N(conVerification.Key, conVerification.Value);
							}
							if (checkGetString(((Label)conCheckField.FindControl("lblTitle")).Attributes["MaxLen"]) != "")
							{
								strReturn += checkMaxLen(conVerification.Key, conVerification.Value, Convert.ToInt32(((Label)conCheckField.FindControl("lblTitle")).Attributes["MaxLen"]));
							}
						}
					}
				}
				foreach (Control con in conCheckField.Controls)
				{
					strReturn += checkField(con);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否空白
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkEmpty(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkEmpty(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否空白
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkEmpty(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "")
			{
				strReturn = strColName + " 為必填!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為數字
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkNumber(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkNumber(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為數字
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkNumber(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^[-+]?\d+(\.\d+)?$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為數字!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為英文或數字
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkAlphanumeric(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkAlphanumeric(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為英文或數字
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkAlphanumeric(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^[+]?\d+(\.\d+)?$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為英數字!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為正整數
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkPosNumber(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkPosNumber(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為正整數
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkPosNumber(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^[+]?[1-9]+(\d)?(\.\d)?", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為正整數!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為身份證字號
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkPeoIdNum(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkPeoIdNum(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為身份證字號
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkPeoIdNum(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			bool d = false;
			if (strVal.Length == 10)
			{
				strVal = strVal.ToUpper();
				if (strVal[0] >= 0x41 && strVal[0] <= 0x5A)
				{
					var a = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
					var b = new int[11];
					b[1] = a[(strVal[0]) - 65] % 10;
					var c = b[0] = a[(strVal[0]) - 65] / 10;
					for (var i = 1; i <= 9; i++)
					{
						b[i + 1] = strVal[i] - 48;
						c += b[i] * (10 - i);
					}
					if (((c % 10) + b[10]) % 10 == 0)
					{
						d = true;
					}
				}
			}

			if (d == false)
			{
				strReturn = strColName + " 須為正確身份證字號!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為統一編號
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkComIdNum(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkComIdNum(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為統一編號
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkComIdNum(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^\d{8}$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為統一編號格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為圖片檔案格式
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkImageFileType(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkImageFileType(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為圖片檔案格式
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkImageFileType(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"\.(PNG|JPG|JPEG|GIF|BMP|TIF|TIFF)$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為圖片檔案格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為e-mail
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkEmail(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkEmail(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為e-mail
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkEmail(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為e-mail格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為電話
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTel(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkTel(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為電話
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTel(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^\d{9,10}$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為電話格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為行動電話
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMobile(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkMobile(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為行動電話
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMobile(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^09(\d{8})$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為行動電話格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為網址
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkUrl(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkUrl(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為網址
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkUrl(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			Regex regex = new Regex(@"^http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?$", RegexOptions.IgnoreCase);
			if (regex.IsMatch(strVal) == false)
			{
				strReturn = strColName + " 須為網址格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為日期/時間
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkAnnoDomini(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			DateTime dateValue;
			if (DateTime.TryParse(strVal, out dateValue))
			{
				if (dateValue.Year <= 1911)
				{
					strReturn = strColName + " 須為西元日期格式!<br />";
				}
			}
			else
			{
				strReturn = strColName + " 須為日期/時間格式!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為經度範圍
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMapE(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkMapE(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為經度範圍
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMapE(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			string strCheckNumber = checkNumber(strVal, strColName);
			if (strCheckNumber.Length > 0)
			{
				strReturn = strCheckNumber;
			}
			else if (Convert.ToDouble(strVal) < 120 || Convert.ToDouble(strVal) > 122)
			{
				strReturn = strColName + " 須為經度範圍!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為緯度範圍
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMapN(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkMapN(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為緯度範圍
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMapN(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			string strCheckNumber = checkNumber(strVal, strColName);
			if (strCheckNumber.Length > 0)
			{
				strReturn = strCheckNumber;
			}
			else if (Convert.ToDouble(strVal) < 22 || Convert.ToDouble(strVal) > 25)
			{
				strReturn = strColName + " 須為緯度範圍!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為TM2_97經度範圍
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTm2_97E(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkTm2_97E(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為TM2_97經度範圍
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTm2_97E(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			string strCheckNumber = checkNumber(strVal, strColName);
			if (strCheckNumber.Length > 0)
			{
				strReturn = strCheckNumber;
			}
			else if (Convert.ToDouble(strVal) < 150974.442353 || Convert.ToDouble(strVal) > 350470.674679)
			{
				strReturn = strColName + " 須為TM2_97經度範圍!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位是否為TM2_97緯度範圍
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTm2_97N(Control con, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkTm2_97N(getVal(con), strColName);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value是否為TM2_97緯度範圍
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkTm2_97N(string strVal, string strColName)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal == "") return "";

			string strCheckNumber = checkNumber(strVal, strColName);
			if (strCheckNumber.Length > 0)
			{
				strReturn = strCheckNumber;
			}
			else if (Convert.ToDouble(strVal) < 2415980.483744 || Convert.ToDouble(strVal) > 2802271.652993)
			{
				strReturn = strColName + " 須為TM2_97緯度範圍!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查欄位字數是否超出限制
	/// </summary>
	/// <param name="con"></param>
	/// <param name="maxLength"></param>
	/// <param name="strColName"></param>
	/// <returns></returns>
	public static string checkMaxLen(Control con, string strColName, int maxLength)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkMaxLen(getVal(con), strColName, maxLength);
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 檢查Value字數是否超出限制
	/// </summary>
	/// <param name="strVal"></param>
	/// <param name="strColName"></param>
	/// <param name="maxLength"></param>
	/// <returns></returns>
	public static string checkMaxLen(string strVal, string strColName, int maxLength)
	{
		string strReturn = string.Empty;
		try
		{
			if (strVal.Length > maxLength)
			{
				strReturn = strColName + "字串長度超過限制：" + maxLength + "!<br />";
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}

		return strReturn;
	}

	#endregion
}