﻿using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;
using System;
using System.Data;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public partial class BasePage : Page
{
	

	/// <summary>
	/// BATCH_USEFEE.OUT_FILE檔名格式
	/// </summary>
	/// <param name="objPKValue"></param>
	/// <param name="objCount"></param>
	/// <returns></returns>
	public static string formatBatchOutFileName(object objPKValue, object objCount)
	{
		string strResult = "";
		try
		{
			strResult += (DateTime.Now.Year - 1911).ToString("000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00");
			strResult += "-";
			strResult += checkGetString(objPKValue);
			strResult += "-";
			strResult += String.Format("{0:00}", objCount);
			strResult += ".zip";
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
		return strResult;
	}



	/// <summary>
	/// 格式化備註的字串-括弧：ex：新申請(0)
	/// </summary>
	/// <param name="objMain"></param>
	/// <param name="objNote"></param>
	/// <returns></returns>
	public static string formatNoteByPare(object objMain, object objNote)
	{
		string strReturn = "";
		try
		{
			if (checkGetString(objNote) != "") strReturn = ("(" + checkGetString(objNote) + ")");
			strReturn = checkGetString(objMain) + strReturn;
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 格式化備註的字串-破折號：ex：母號-子號
	/// </summary>
	/// <param name="strNumStatus"></param>
	/// <param name="strNote"></param>
	/// <returns></returns>
	public static string formatNoteByDash(object objMain, object objNote)
	{
		string strReturn = "";
		try
		{
			strReturn += checkGetString(objMain);
			strReturn += (checkGetString(objNote) != "" ? "-" : "");
			strReturn += checkGetString(objNote);
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 格式化備註的字串Symbol：ex：段別 + Symbol + 小段
	/// </summary>
	/// <param name="strMain"></param>
	/// <param name="strNote"></param>
	/// <param name="strSymbol"></param>
	/// <param name="chkMainEmpty">檢查Main是空時，不顯示Symbol</param>
	/// <returns></returns>
	public static string formatNoteBySymbol(object objMain, object objNote, object objSymbol, bool chkMainEmpty = false)
	{
		string strReturn = "";
		try
		{
			strReturn += checkGetString(objMain);
			if (checkGetString(objNote) != "")
			{
				if (chkMainEmpty == false || checkGetString(objMain) != "")
					strReturn += checkGetString(objSymbol);
			}
			strReturn += checkGetString(objNote);
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 修改河川局名稱格式為：經濟部水利署(第一河川局)
	/// </summary>
	/// <param name="strUnitName"></param>
	/// <returns></returns>
	public static string formatUnitName(string strUnitName)
	{
		string strReturn = strUnitName;
		try
		{
			if (strUnitName.IndexOf("第") < strReturn.IndexOf("河川局"))
			{
				strReturn = strReturn.Insert(strReturn.IndexOf("第"), "(");
				strReturn = strReturn.Insert(strReturn.Length, ")");

			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 依obj有沒有值，回傳空字串，或套格式回傳
	/// </summary>
	/// <param name="strFormat"></param>
	/// <param name="aryObj"></param>
	/// <returns></returns>
	public static string formatString(string strFormat, params object[] aryObj)
	{
		string strReturn = string.Empty;
		bool boolHasValue = false;

		try
		{
			#region 參數陣列，有任一元素有值才套格式
			foreach (object obj in aryObj)
			{
				if (checkGetString(obj) != "")
				{
					boolHasValue = true;
					break;
				}
			}
			#endregion

			if (boolHasValue)
			{
				strReturn = string.Format(strFormat, aryObj);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 子字串，若startIndex大於主字串長度，回傳空字串
	/// </summary>
	/// <param name="strMain"></param>
	/// <param name="startIndex"></param>
	/// <param name="length"></param>
	/// <returns></returns>
	public static string formatSubstring(string strMain, int startIndex, int? length = null)
	{
		string strReturn = string.Empty;

		try
		{
			if (strMain.Length > startIndex)
			{
				if (length == null)
				{
					strReturn = strMain.Substring(startIndex);
				}
				else if (strMain.Length >= startIndex + length.Value)
				{
					strReturn = strMain.Substring(startIndex, length.Value);
				}
				else
				{
					strReturn = strMain.Substring(startIndex, strMain.Length - startIndex);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 字串加上遮罩
	/// </summary>
	/// <param name="objCode"></param>
	/// <returns></returns>
	public static string markingString(object objCode)
	{
		char[] chrResult = null;
		try
		{
			chrResult = BasePage.checkGetString(objCode).ToCharArray();
			for (int i = 0; i < chrResult.Length; i++)
			{
				//身份證字號顯示前三後三，中間四碼加密
				//戶號正常八碼，顯示前三後二，中間三碼加密
				if (i > 2 && i < 2 + (chrResult.Length / 2)) chrResult[i] = '*';
			}

			return new string(chrResult);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 縮短字串
	/// </summary>
	/// <param name="strCode"></param>
	/// <returns></returns>
	public static string shortingString(string strCode, int intLength)
	{
		string strResult = string.Empty;
		try
		{
			if (strCode.Length > intLength)
			{
				strResult = (strCode.Substring(0, intLength) + "...");
			}
			else
			{
				strResult = strCode;
			}

			return strResult;
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 判斷某元素是否存在於strBaseValues中
	/// </summary>
	/// <param name="strValue">要判斷的元素</param>
	/// <param name="strBaseValues">要搜尋的母體</param>
	/// <param name="strSplitSymbol">分隔的符號，預設","</param>
	/// <returns></returns>
	public static bool containString(string strValue, string strBaseValues, string strSplitSymbol = ",")
	{
		bool boolReturn;
		try
		{
			string[] aryBase = checkGetString(strBaseValues).Split(new string[] { strSplitSymbol }, StringSplitOptions.RemoveEmptyEntries);
			List<string> listBase = new List<string>(aryBase);

			boolReturn = listBase.Contains(strValue);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
		return boolReturn;
	}

	/// <summary>
	/// 取得勾選狀態符號(■ / □)
	/// </summary>
	/// <param name="boolChk">是否勾選</param>
	/// <returns></returns>
	public static string printSymbol(bool boolChk)
	{
		try
		{
			if (boolChk)
			{
				return "■";
			}
			else
			{
				return "□";
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
	}

	/// <summary>
	/// 轉換成民國日期
	/// </summary>
	/// <param name="objQuery"></param>
	/// <param name="boolTitle">是否加「中華民國」標題</param>
	/// <returns></returns>
	public static string printDateTime(object objQuery, bool boolTitle = false)
	{
		string strReturn = "";
		try
		{
			string strFormat = "yyy年MM月dd日";
			if (boolTitle) strFormat = "中華民國" + strFormat;
			strReturn = printDateTime(objQuery, strFormat);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 轉換成民國日期，並依自訂格式輸出
	/// </summary>
	/// <param name="objQuery"></param>
	/// <param name="strFormat">
	/// yy:	年
	/// MM:	月
	/// dd:	日
	/// tt:	上午/下午
	/// hh:	時(12時制)
	/// HH:	時(24時制)
	/// mm:	分
	/// </param>
	/// <returns></returns>
	public static string printDateTime(object objQuery, string strFormat)
	{
		string strReturn = "";
		try
		{
			try
			{
				if (objQuery == DBNull.Value)
				{
					strReturn = strFormat
						.Replace("y", "　")
						.Replace("M", "　")
						.Replace("d", "　")
						.Replace("t", "　")
						.Replace("h", "　")
						.Replace("H", "　")
						.Replace("m", "　");
				}
				else if (objQuery == null)
				{
					strReturn = strFormat
						.Replace("y", "　")
						.Replace("M", "　")
						.Replace("d", "　")
						.Replace("t", "　")
						.Replace("h", "　")
						.Replace("H", "　")
						.Replace("m", "　");
				}
				else
				{
					strReturn = checkGetDateTime(objQuery, strFormat);
				}
			}
			catch (Exception)
			{
				return "";
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 列印字串，如果變數是空字串，則印列出空格
	/// </summary>
	/// <param name="obj"></param>
	/// <param name="intWidth">空白空度</param>
	/// <returns></returns>
	public static string printStringFill(object obj, int intWidth)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = checkGetString(obj);
			if (string.IsNullOrEmpty(strReturn))
			{
				for (int i = 0; i < intWidth; i++)
				{
					strReturn += "　";
				}
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 列印直排字串
	/// </summary>
	/// <param name="isHtml"></param>
	/// <param name="strs"></param>
	/// <returns></returns>
	public static string printStringList(bool isHtml, params object[] objs)
	{
		string strReturn = string.Empty;
		try
		{
			foreach (string str in objs)
			{
				if (string.IsNullOrEmpty(str)) continue;
				strReturn += str;
				if (isHtml)
				{
					strReturn += "<br />";
				}
				else
				{
					strReturn += "\r\n";
				}

			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return strReturn;
	}

	/// <summary>
	/// 串接字串，並以符號間隔
	/// </summary>
	/// <param name="strSplitStrings">原始字串</param>
	/// <param name="strToAddSplitString">要增加的字串</param>
	/// <param name="strSeparator">要間格的符號，預設","</param>
	/// <returns></returns>
	public static string addSplitString(string strSplitStrings, string strToAddSplitString, string strSeparator = ",")
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = strSeparator + strSplitStrings + strSeparator;
			strReturn = strReturn + strToAddSplitString + strSeparator;
			while (strReturn.IndexOf(strSeparator + strSeparator) > -1)
			{
				strReturn = strReturn.Replace(strSeparator + strSeparator, strSeparator);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}

    /// <summary>
    /// 取得檔案下載的路徑
    /// </summary>
    /// <param name="strPath"></param>
    /// <param name="objFileName"></param>
    /// <returns></returns>
    public static string formatDownloadURL(string strPath, object objFileName)
    {
        string strResult = "";
        try
        {
            string strParameters = "";
            strParameters = addParameter(strParameters, "path", strPath);
            strParameters = addParameter(strParameters, "file", checkGetString(objFileName));

            strResult = "~/view/pop/DownLoading.aspx" + strParameters;
        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }
        return strResult;
    }

	/// <summary>
	/// 移除以符號間隔的字串
	/// </summary>
	/// <param name="strSplitStrings">原始字串</param>
	/// <param name="strToRmSplitString">要移除的字串</param>
	/// <param name="strSeparator">要間格的符號，預設","</param>
	/// <returns></returns>
	public static string rmSplitString(string strSplitStrings, string strToRmSplitString, string strSeparator = ",")
	{
		string strReturn = string.Empty;
		try
		{
			strReturn = strSeparator + strSplitStrings + strSeparator;
			strReturn = strReturn.Replace(strSeparator + strToRmSplitString + strSeparator, strSeparator);
			while (strReturn.IndexOf(strSeparator + strSeparator) > -1)
			{
				strReturn = strReturn.Replace(strSeparator + strSeparator, strSeparator);
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
		return strReturn;
	}
}