﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;
using System.Web.Script.Serialization;
using System.Text;
/// <summary>
/// BasePage 的基礎類別
/// </summary>
public partial class BasePage : Page
{

    /// <summary>
    /// 路線部分關閉日期
    /// </summary>
    /// <returns></returns>
    public static string GotStringClose_Date(string totalday, string tb, string selectValue)
    {

        string str = string.Empty;
        try
        {

            SqlCommand cmd_M = new SqlCommand();

            if (tb == "Fixedclimbmain")
            {
                cmd_M.Parameters.AddWithValue("@f_id", selectValue);
                cmd_M.CommandText = string.Format(@" select * from Fixedclimbmain where chk=2 and f_id=@f_id");

            }
            else if (tb == "Fixedclimb")
            {
                cmd_M.Parameters.AddWithValue("@c_id", selectValue);
                cmd_M.CommandText = string.Format(@" select * from Fixedclimb where chk=2 and c_id=@c_id");
            }
            DataTable dt_M = dbAdapter.getDataTable(cmd_M);
            if (dt_M.Rows.Count > 0)
            {
                str = GotStringListClose_Date(totalday, tb, selectValue);
            }
            dt_M.Dispose();
            cmd_M.Dispose();

        }
        catch (Exception ex)
        {
            // throw ex;
        }

        return str;
    }

    public static void popDownload(string strPath, object objFileName, Boolean toDownloadSesssion = false)
    {
        string strContentType = string.Empty;

        try
        {
            string strDownloadURL = formatDownloadURL(strPath, objFileName);

            string strJs = string.Empty;
            strJs += @"
    $(function () {
     window.open('" + GetFullUrl(strDownloadURL) + @"');
    });";
            
            if (!toDownloadSesssion) BasePage.clientRegisterJS(strJs);
            else BasePage.responseBuffer(strPath, objFileName);

        }
        catch (Exception ex)
        {
            ExceptionAdapter.logSysException(ex);
            throw ex;
        }
    }

    public static string GetFullUrl(string relativeUrl)
    {
        string strReturn = string.Empty;
        try
        {
            string strServerName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
            string strServerPort = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
            string strAppendPort = ConfigurationManager.AppSettings["appendport"];
            string strPath = VirtualPathUtility.ToAbsolute(relativeUrl);
            strReturn = "http://" + strServerName + ":"  +  strServerPort + strPath;
            //strReturn = "http://" + strServerName + ":" + strAppendPort +  strServerPort + strPath;
        }
        catch (Exception)
        {
            throw;
        }
        return strReturn;
    }
    /// <summary>
    /// 次路線部分關閉日期回傳
    /// </summary>
    /// <returns></returns>
    public static string GotStringListClose_Date(string totalday, string tb, string selectValue)
    {

        string str = string.Empty;
        Int32 totaldayInt = 0;
        if (totalday != "") { totaldayInt = Convert.ToInt32(totalday) - 1; }
        try
        {



            SqlCommand cmd_ML = new SqlCommand();
            if (tb == "Fixedclimbmain")
            {
                cmd_ML.Parameters.AddWithValue("@f_id", selectValue);
                cmd_ML.CommandText = string.Format(@" select * from Fixedclimbmain_closedate where   f_id=@f_id");
            }
            else if (tb == "Fixedclimb")
            {
                cmd_ML.Parameters.AddWithValue("@c_id", selectValue);
                cmd_ML.CommandText = string.Format(@" select * from Fixedclimb_closedate where   c_id=@c_id");
            }
            DataTable dt_ML = dbAdapter.getDataTable(cmd_ML);
            if (dt_ML.Rows.Count > 0)
            {
                for (Int32 i = 0; i < dt_ML.Rows.Count; i++)
                {
                    DateTime dtstart = Convert.ToDateTime(dt_ML.Rows[i]["sdate"].ToString()).AddDays(-totaldayInt);
                    DateTime dtend = Convert.ToDateTime(dt_ML.Rows[i]["edate"].ToString());
                    int countday = Math.Abs(((TimeSpan)(dtend - dtstart)).Days);
                    for (int i1 = 0; i1 <= countday; i1++)
                    {
                        str += Utility.getCDate(dtstart.AddDays(i1).ToString()).ToString() + "|";
                    }
                }
            }
            dt_ML.Dispose();
            cmd_ML.Dispose();


        }
        catch (Exception ex)
        {
            // throw ex;
        }

        return str;
    }




    public static string getDayOfWeek(DateTime d, string robackstyle)
    {
        string str = string.Empty;
        if (robackstyle == "EN") { str = Convert.ToDateTime(d).DayOfWeek.ToString(); }
        else if (robackstyle == "CH")
        { str = DayOfWeek_CH(Convert.ToDateTime(d).DayOfWeek.ToString(), "0"); }
        else if (robackstyle == "NUM") { str = DayOfWeek_CH(Convert.ToDateTime(d).DayOfWeek.ToString(), "1"); }
        return str;
    }

    public static string DayOfWeek_CH(string strs, string isnums)
    {
        string str = string.Empty;
        string str2 = string.Empty;
        switch (strs)
        {
            case "Monday":
                str = "星期一"; str2 = "1";
                break;
            case "Tuesday":
                str = "星期二"; str2 = "2";
                break;
            case "Wednesday":
                str = "星期三"; str2 = "3";
                break;
            case "Thursday":
                str = "星期四"; str2 = "4";
                break;
            case "Friday":
                str = "星期五"; str2 = "5";
                break;
            case "Saturday":
                str = "星期六"; str2 = "6";
                break;
            case "Sunday":
                str = "星期日"; str2 = "7";
                break;
        }
        if (isnums == "1") { str = str2; }
        return str;
    }
    /// <summary>
    /// DBNull和null轉成空字串，轉成string型態並處理前後空白字元(trim)後回傳
    /// </summary>
    /// <param name="objQuery"></param>
    /// <param name="boolTrim"></param>
    /// <returns></returns>
    public static string checkGetString(object objQuery, bool boolTrim = true)
    {
        string strReturn = "";
        try
        {
            if (objQuery == DBNull.Value)
            {
                strReturn = "";
            }
            else if (Convert.ToString(objQuery) == null)
            {
                strReturn = "";
            }
            else
            {
                strReturn = Convert.ToString(objQuery);
                if (boolTrim) strReturn = strReturn.Trim();
            }
        }
        catch (Exception)
        {

            throw;
        }
        return strReturn;
    }
    /// <summary>
    /// DBNull和null轉成0，並轉成Double型態回傳
    /// </summary>
    /// <param name="strQuery"></param>
    /// <returns></returns>
    public static Double checkGetDouble(object objQuery)
    {
        Double douReutrn = new Double();
        try
        {
            if (objQuery == DBNull.Value)
            {
                douReutrn = 0;
            }
            else if (Convert.ToString(objQuery) == null)
            {
                douReutrn = 0;
            }
            else if (Convert.ToString(objQuery) == "")
            {
                douReutrn = 0;
            }
            else
            {
                douReutrn = Convert.ToDouble(objQuery);
            }
        }
        catch (Exception)
        {

            throw;
        }
        return douReutrn;
    }
    /// <summary>
    /// DBNull和null轉成空字串，轉成string型態並處理前後空白字元(trim)後HtmlEncode回傳
    /// </summary>
    /// <param name="objQuery"></param>
    /// <returns></returns>
    public static string checkGetHtmlEncode(object objQuery)
    {
        string strReturn = "";
        try
        {
            if (objQuery == DBNull.Value)
            {
                strReturn = "";
            }
            else if (Convert.ToString(objQuery) == null)
            {
                strReturn = "";
            }
            else
            {
                strReturn = HttpContext.Current.Server.HtmlEncode(checkGetString(objQuery));

                foreach (string tag in listHtmlEncodeTag)
                {
                    strReturn = strReturn.Replace(HttpContext.Current.Server.HtmlEncode(tag), tag);
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
        return strReturn;
    }

    /// <summary>
    /// 並依自訂格式輸出
    /// </summary>
    /// <param name="objQuery"></param>
    /// <param name="strFormat">
    /// yy:	年
    /// MM:	月
    /// dd:	日
    /// tt:	上午/下午
    /// hh:	時(12時制)
    /// HH:	時(24時制)
    /// mm:	分
    /// </param>
    /// <returns></returns>
    public static string checkGetDateTime(object objQuery, string strFormat = null)
    {
        string strReturn = "";
        try
        {
            if (string.IsNullOrEmpty(strFormat))
            {
                strFormat = "yyyy/MM/dd";
            }

            if (objQuery == DBNull.Value)
            {
                strReturn = "";
            }
            else if (checkGetString(objQuery) == "")
            {
                strReturn = "";
            }
            else
            {
                DateTime? dt = tranToDateTime(objQuery);
                if (dt.HasValue)
                {
                    strReturn = dt.Value.ToString(strFormat);
                }
            }
        }
        catch (Exception)
        {

            throw;
        }

        return strReturn;
    }

    /// <summary>
    /// 將物件轉換成西元DateTime
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static DateTime? tranToDateTime(object obj)
    {
        DateTime dt;
        try
        {
            CultureInfo taiwanCultureInfo = new CultureInfo("zh-TW");
            taiwanCultureInfo.DateTimeFormat.Calendar = new TaiwanCalendar();

            //先以西元年解析
            if (DateTime.TryParse(checkGetString(obj), out dt))
            {
                if (dt.Year > 1911)		//年度大於1911代表為西元年
                {
                    return dt;
                }
                else		//否則改以民國年解析
                {
                    return DateTime.Parse(checkGetString(obj), taiwanCultureInfo);
                }
            }
            //以民國年解析
            else if (DateTime.TryParse(checkGetString(obj), taiwanCultureInfo, DateTimeStyles.None, out dt))
            {
                return DateTime.Parse(checkGetString(obj), taiwanCultureInfo);
            }
            else
            {
                return null;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// 增加Query String傳遞參數(參數已存在時，會取代原參數的值)
    /// </summary>
    /// <param name="strParameters">原始參數字串</param>
    /// <param name="strToAddParaKey">參數名稱</param>
    /// <param name="strToAddParaValue">參數值</param>
    /// <returns></returns>
    public static string addParameter(string strParameters, string strToAddParaKey, string strToAddParaValue)
    {
        string strResult = string.Empty;
        try
        {
            string[] aryPars = strParameters.Split(new char[] { '?', '&' }, StringSplitOptions.RemoveEmptyEntries);
            bool hasPar = false;

            foreach (string par in aryPars)
            {
                //以字串代表參數名稱及值，避免有參數沒值的問題
                string strKey = string.Empty;
                string strVal = string.Empty;

                string[] strKV = par.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (strKV.Length > 0) { strKey = strKV[0]; }
                if (strKV.Length > 1) { strVal = HttpUtility.UrlDecode(strKV[1]); }

                #region 加分隔符號
                if (strResult == "")
                    strResult += "?";
                else
                    strResult += "&";
                #endregion

                #region 填入回傳變數中
                if (strKey == strToAddParaKey)
                {
                    //要加入的參數已經存在，取代原參數的值
                    strResult += strToAddParaKey + '=' + HttpUtility.UrlEncode(strToAddParaValue);
                    hasPar = true;
                }
                else
                {
                    strResult += strKey + '=' + HttpUtility.UrlEncode(strVal);
                }
                #endregion
            }

            if (hasPar == false)
            {
                if (strResult == "")
                    strResult += "?";
                else
                    strResult += "&";
                strResult += strToAddParaKey + '=' + HttpUtility.UrlEncode(strToAddParaValue);
            }
        }
        catch (Exception)
        {

            throw;
        }

        return strResult;
    }

}
