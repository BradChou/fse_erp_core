﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public partial class BasePage : Page
{


    /// <summary>
    /// 水晶報表RPT檔路徑
    /// </summary>
    public static string pathRPT = "~/report/";
    /// <summary>
    /// Temp Log 路徑
    /// </summary>
    public static string pathTempLog
    {
        get
        {
            string strPath = System.Web.HttpContext.Current.Server.MapPath("~/Log/Temp/");
            if (Directory.Exists(strPath) == false)
            {
                Directory.CreateDirectory(strPath);
            }
            return strPath;
        }
    }

    /// <summary>
    /// Execption Log 路徑
    /// </summary>
    public static string pathErrorLog
    {
        get
        {
            string strPath = System.Web.HttpContext.Current.Server.MapPath("~/Log/Error/");
            if (Directory.Exists(strPath) == false)
            {
                Directory.CreateDirectory(strPath);
            }
            return strPath;
        }
    }

}