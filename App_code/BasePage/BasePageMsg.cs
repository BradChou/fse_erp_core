﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Collections.Specialized;
using System.Net.Sockets;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public partial class BasePage : Page
{
	/// <summary>
	/// 請依正常單一簽入方式登入！
	/// </summary>
	public static string msgAuth_Security = "請透過WRISP單一簽入！";

	/// <summary>
	/// 系統使用權限不足！
	/// </summary>
	public static string msgAuth_Fail = "系統使用權限不足！";

	/// <summary>
	/// 找不到登入資訊！
	/// </summary>
	public static string msgAuth_Timeout = "找不到登入資訊！";

	/// <summary>
	/// 發生錯誤，請聯絡管理員！\n
	/// </summary>
	public static string msgError_Serious = "發生錯誤，請聯絡管理員！\n";

	/// <summary>
	/// 發生錯誤，請聯絡管理員！\n參數傳遞錯誤
	/// </summary>
	public static string msgError_Para_Incomplete = msgError_Serious + "參數傳遞不完整";

	/// <summary>
	/// 資料已經存在！
	/// </summary>
	public static string msgDB_Repeat = "資料已經存在！";

	/// <summary>
	/// 請先選取資料
	/// </summary>
	public static string msgMustSelect = "請先選取資料";

	/// <summary>
	/// 確定刪除資料嗎？
	/// </summary>
	public static string msgConfirmDelete = "確定刪除資料嗎？";

	/// <summary>
	/// 儲存成功
	/// </summary>
	public static string msgSaveSuccess = "儲存成功";

	/// <summary>
	/// 刪除成功
	/// </summary>
	public static string msgDeleteSuccess = "刪除成功";

	/// <summary>
	/// 查無資料
	/// </summary>
	public static string msgNoData = "查無資料";

	/// <summary>
	/// 案件非本河川局建立，不允許修改
	/// </summary>
	public static string msgRej_SameUnit = "案件非本河川局建立，不允許修改";

	/// <summary>
	/// 非管轄範圍內之案件，不允許修改
	/// </summary>
	public static string msgRej_ManageDept = "非管轄範圍內之案件，不允許修改";
	
	#region 提示訊息函式

	/// <summary>
	/// 顯示不允許修改的提示訊息(該[strType]狀態「[strStatus]」，不允許修改)
	/// </summary>
	/// <param name="con"></param>
	/// <param name="strType">資料的類別，ex:案件，土地，使用費...</param>
	/// <param name="strStatus">資料的狀態，ex:已結案，已廢止，已繳費</param>
	/// <param name="strAction">動作，ex:修改，列印，刪除</param>
	/// <param name="strAppendMsg">附加的訊息</param>
	public static string showCantActionMsg(string strType, string strStatus, string strAction, string strAppendMsg = "")
	{
		string strReturn = "";
		try
		{
			if (checkGetString(strType) == "")
			{
				strReturn = string.Format("狀態為「{0}」，不允許「{1}」\n{2}", strStatus, strAction, strAppendMsg);
			}
			else
			{
				strReturn = string.Format("該{0}狀態為「{1}」，不允許「{2}」\n{3}", strType, strStatus, strAction, strAppendMsg);
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return JSStringEscape(strReturn, true);
	}

	/// <summary>
	/// 唯一的DataRow查詢筆數有誤回傳的訊息
	/// </summary>
	/// <param name="cmd"></param>
	/// <returns></returns>
	public static string showReturnNotOnly1Msg(SqlCommand cmd)
	{
		string strReturn = string.Empty;
		try
		{
			strReturn += BasePage.msgError_Serious;
			strReturn += "回傳資料不是唯一\n";

			SqlParameterCollection paras = cmd.Parameters;
			foreach (SqlParameter item in paras)
			{
				strReturn += string.Format("{0} = '{1}'\n", item.ParameterName.Substring(1), item.Value);
			}
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}
		return strReturn;
	}

	/// <summary>
	/// 顯示項目錯誤訊息(strItem」有誤，請確認)
	/// </summary>
	/// <param name="strItem">資料的類別，ex:檔案名稱...</param>
	/// <param name="strAppendMsg">附加的訊息</param>
	/// <returns></returns>
	public static string showWrongInfMsg(string strType, string strAppendMsg)
	{
		string strReturn = "";
		try
		{
			strReturn = string.Format("「{0}」有誤，請確認\n{1}", strType, strAppendMsg);
			strReturn = JSStringEscape(strReturn, true);
		}
		catch (Exception ex)
		{
			ExceptionAdapter.logSysException(ex);
			throw ex;
		}

		return strReturn;
	}

	#endregion
}