﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetReport.aspx.cs" Inherits="GetReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>DownLoad</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .lb_set{font-size:12pt;color:#777;font-weight:bold;}
        </style>
</head>
<body>
    <form id="form1" runat="server">

        <div style="overflow: auto; height: 500px; vertical-align: top;">
            <asp:ListBox ID="lbMsg" runat="server" Height="490px" Width="100%" CssClass="lb_set"></asp:ListBox>
        </div>
    </form>
</body>
</html>
