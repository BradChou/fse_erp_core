﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="assets1_2_carsel.aspx.cs" Inherits="assets1_2_carsel" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇車輛</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">
                                <label>關鍵字查詢：</label>
                                <asp:TextBox ID="keyword" runat="server" class="form-control"></asp:TextBox>
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                            </div>
                        </div>
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>選取</th>
                                 <th class="_th"></th>
                        <th class="_th">車牌</th>
                        <th class="_th">所有權</th>
                        <th class="_th">噸數</th>
                        <th class="_th">廠牌</th>
                        <th class="_th">車行 </th>
                        <th class="_th">年份</th>
                        <th class="_th">月份</th>
                        <th class="_th">輪胎數</th>
                        <th class="_th">車廂型式</th>
                        <th class="_th">油卡</th>
                        <th class="_th">油料折扣</th>
                        <th class="_th">ETC</th>
                        <th class="_th">輪胎統包</th>
                        <th class="_th">使用人</th>
                        <%--<th class="_th">業務別1</th>--%>
                        <th class="_th">司機</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="選取">
                                            <asp:HiddenField ID="Hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>' />
                                            <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                        </td>

                                         <td data-th="車牌">
                                             <asp:Literal ID="Licar_license" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%>'></asp:Literal></td>
                                        <td data-th="所有權"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ownership_text").ToString())%></td>
                                <td data-th="噸數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                <td data-th="廠牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.brand").ToString())%> </td>
                                <td data-th="車行"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_retailer_text").ToString())%></td>
                                <td data-th="年份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.year").ToString())%></td>
                                <td data-th="月份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.month").ToString())%></td>
                                <td data-th="輪胎數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tires_number").ToString())%></td>
                                <td data-th="車廂型式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cabin_type_text").ToString())%></td>
                                <td data-th="油卡"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.oil").ToString())%></td>
                                <td data-th="油料折扣"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.oil_discount").ToString())%></td>
                                <td data-th="ETC"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ETC").ToString())%></td>
                                <td data-th="輪胎統包"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tires_monthlyfee").ToString())%></td>
                                <td data-th="使用人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user").ToString())%></td>
                                <%--<td data-th="業務別1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user").ToString())%></td>--%>
                                <td data-th="司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td data-th="備註" style="width:300px;text-align :left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                       
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="8" style="text-align: center">查無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
