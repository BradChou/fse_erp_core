﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using NPOI.SS.Formula.Functions;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System.Windows.Forms;
using Dapper;
using System.Configuration;

public partial class JunFuTraceS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            divMaster.Visible = true;
            Label1.Text = "";
            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }
            string viewlist = "";
            viewlist += "<tr> ";
            viewlist += "<td colspan = '2' style ='text-align:center' > 尚無資料 </td >";
            viewlist += "</tr> ";
            Literal1.Text = viewlist;
            readdata();
        }

    }

    protected void search_Click(object sender, EventArgs e)
    {

        string captcha = string.Empty;


        if (check_number.Text == "" && rb1.Checked)
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('請填寫貨號');</script><noscript></noscript>");
            return;
        }
        if (order_number.Text == "" && rb2.Checked)
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('請填寫訂單編號');</script><noscript></noscript>");
            return;
        }

        readdata();

        //if (Session["Captcha"] != null && Session["Captcha"].ToString() != "")
        //{
        //    captcha = Session["Captcha"].ToString();
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='JunFuTraceS.aspx';alert('驗證碼無輸入，請重新輸入');</script>", false);
        //}
        //if (captcha == xcode.Text.ToString())
        //{
        //    readdata();
        //    xcode.Text = "";
        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='JunFuTraceS.aspx';alert('驗證碼錯誤，請重新輸入');</script>", false);
        //    return;
        //}




    }

    private void readdata()
    {
        if (rb1.Checked)
        {
            check_number.Text = check_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入貨號');</script><noscript></noscript>");
                return;
            }

            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('貨號有誤');</script><noscript></noscript>");
                return;
            }

            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE check_number = @check_number  
";
                    request_id = cn.Query<string>(sql, new { check_number = check_number.Text }).ToList();
                }
            }
            catch (Exception exx)
            {

            }
            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
              
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }

        }
        else if (rb2.Checked)
        {

            order_number.Text = order_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入訂單編號');</script><noscript></noscript>");
                return;
            }
            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('編號有誤');</script><noscript></noscript>");
                return;
            }


            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE order_number = @order_number 

";
                    request_id = cn.Query<string>(sql, new { order_number = order_number.Text }).ToList();
                }
            }
            catch (Exception exx)
            {

            }

            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
               
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }
        }


        if (check_number.Text != "" || order_number.Text != "")
        {
            Label1.Text = "查貨時間：" +  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");





            //using (SqlCommand cmd = new SqlCommand())
            //{
            //    cmd.Parameters.Clear();
            //    #region 關鍵字
            //    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            //    #endregion

            //    cmd.CommandText = string.Format(@"DECLARE  @CHKNUM NVARCHAR(100)  --貨號
            //             DECLARE  @GOODS INT   --貨件運費
            //             DECLARE  @YUAN  INT   --元付運費
            //             DECLARE  @PEI   INT   --配送費用
            //             DECLARE  @aUNIT INT   --每板費用
            //             DECLARE  @TOTAL INT   --總費用
            //             DECLARE  @ttERR   NVARCHAR(100) 
            //             --select check_number , 0 '貨件運費',0'元付運費', 0'配送費用',0'每板費用', 0'總費用' 
            //             --into 
            //             --#mylist	  
            //             --from tcDeliveryRequests with(nolock)
            //             --where  check_number = @check_number
            //             --
            //             --DECLARE R_cursor CURSOR FOR
            //             -- select * FROM #mylist
            //             --OPEN R_cursor
            //             --FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
            //             --WHILE (@@FETCH_STATUS = 0) 
            //       --    BEGIN
            //    --      CREATE TABLE #tmp (supplier_fee INT , supplier_unit_fee  INT , cscetion_fee INT,cscetion_unit_fee  INT,status_code  INT,status_msg NVARCHAR(500));
            //    --      INSERT INTO #tmp EXEC  usp_GetShipFeeByCheckNumber @CHKNUM; 
            //    --      SELECT @GOODS=supplier_fee, @PEI= cscetion_fee, @aUNIT= supplier_unit_fee ,@YUAN=supplier_fee , @TOTAL=supplier_fee, @ttERR= status_msg FROM #tmp
            //    --       IF (@ttERR ='成功') BEGIN
            // --           UPDATE #mylist
            // --             SET 貨件運費 = @GOODS, 元付運費=@YUAN, 配送費用= @PEI, 每板費用 =@aUNIT ,總費用=@TOTAL
            // --           WHERE check_number = @CHKNUM
            //       --    END
            //             --    IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
            //    --        DROP TABLE #tmp
            //             --
            //             --  FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
            //             --END
            //             --CLOSE R_cursor
            //             --DEALLOCATE R_cursor

            //             Select  CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(D.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation,
            //             E.showname,
            //             A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
            //             A.time_period, A.order_number,
            //             CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
            //             A.product_category, C.code_name as product_category_name, 
            //             A.subpoena_category, B.code_name as subpoena_category_name,
            //             A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
            //             isnull(_fee.total_fee,0) '貨件運費' ,
            //             isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) '元付運費',
            //             A.arrive_to_pay_append '到付追加未稅', A.arrive_to_pay_append  *1.05 '到付追加含稅',
            //             A.collection_money '代收金額',
            //             isnull(_fee.cscetion_fee,0)  '配送費用',
            //             isnull(_fee.supplier_unit_fee,0)  '每板費用',	
            //             A.remote_fee '偏遠區加價',
            //             isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) '總費用'
            //             from tcDeliveryRequests A with(nolock)
            //             LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
            //             LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = '2' and C.code_sclass = 'S3'
            //             LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
            //             LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
            //             --LEFT JOIN #mylist Z ON Z.check_number = A.check_number
            //             CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
            //             where   A.check_number = @check_number

            //             IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
            //             DROP TABLE #mylist");
            //    using (DataTable dt = dbAdapter.getDataTable(cmd))
            //    {

            //        if (dt.Rows.Count > 0)
            //        {
            //            string customer_code = dt.Rows[0]["customer_code"].ToString();
            //            string send_contact = dt.Rows[0]["send_contact"].ToString();
            //            send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
            //            string send_tel = dt.Rows[0]["send_tel"].ToString();
            //            send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
            //            string receive_contact = dt.Rows[0]["receive_contact"].ToString();
            //            receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
            //            string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
            //            receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;


            //        }

            //    }

            //}


          
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/" + days);
                //}
                string wher = "";
                if (rb1.Checked)
                {
                    if (check_number.Text != "")
                    {
                        cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                        wher = wher + "and  A.check_number = @check_number";
                    }
                }
             
                if (rb2.Checked)
                {

                    if (order_number.Text != "")
                    {
                     


                        cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                        wher = wher + "and  R.order_number = @order_number";
                    }
                }

                #endregion

                string viewlist = "";
                string aaa = "";
               
               

              


                cmd.CommandText = string.Format(@"Select ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO ,
                                                 A.scan_item,  CASE A.scan_item WHEN '5' THEN '集貨' WHEN '6' THEN '卸集' WHEN '7' THEN '發送' WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  END  AS scan_name,
                                                 Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,WH.code_name as warehouse,
                                                 A.arrive_option,A.exception_option , A.receive_option,R.pieces ,
                                                 CASE A.scan_item WHEN '3' THEN AO.code_name WHEN '5' THEN RO.code_name ELSE EO.code_name  END as status,R.Less_than_truckload,
                                                 A.driver_code , C.driver_name , A.sign_form_image, ISNULL(S.station_name,'') as station_name,
                                                 R.area_arrive_code as station_area_arrive_code ,S.Station_Scode as station_scode,R.supplier_code as supplier_code
                                                 from ttDeliveryScanLog A with(nolock)
                                                 Left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                 Left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                 Left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                 Left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                 Left outer join tbItemCodes RO with(nolock) on A.receive_option = RO.code_id and RO.code_sclass = 'RO' and RO.active_flag = 1
                                                 Left join tbStation S with(nolock) on C.station = S.Station_Scode 
                                                 Left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate() 
                                                 Left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = 'warehouse' and WH.active_flag = 1 
                                                 where 1=1    {0}  and   scan_item <> 4 
                                                 order by a.scan_date desc", wher);
                int temp = 0;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    int jjj = 0;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //if (temp != Convert.ToInt32(dt.Rows[i]["scan_item"].ToString()))
                            //{
                                temp = Convert.ToInt32(dt.Rows[i]["scan_item"].ToString());
                                jjj = i;
                                string scan_item = dt.Rows[i]["scan_item"].ToString().Trim();
                                string station = "";
                                if (dt.Rows[i]["Less_than_truckload"].ToString() == "False")//棧板時站所顯示
                                {
                                    if (scan_item != "6")
                                    {
                                        station = dt.Rows[i]["supplier_name"].ToString();
                                    }
                                    else
                                    {
                                        station = dt.Rows[i]["warehouse"].ToString();
                                    }
                                }
                                else//零擔站所顯示
                                {
                                    station = dt.Rows[i]["station_name"].ToString();
                                }
                                if (Convert.ToInt32(dt.Rows[i]["NO"].ToString()) % 2 == 0)
                                {
                                    viewlist += "<tr> ";
                                }
                                else
                                {
                                    viewlist += "<tr style='background-color:rgb(224,237,248)'> ";
                                }
                                //viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                if (scan_item == "5")
                                {   

                                        var receive_option = dt.Rows[i]["receive_option"].ToString();
                                        if (receive_option=="16") //另約時間
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>與寄件人另約時間取件，取件尚未成功。</td>";
                                        }
                                        else if (receive_option == "6") //電連不上
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>未能聯繫寄件人，取件尚未成功。</td>";
                                        }
                                        else if (receive_option == "18") //無貨可取
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>無貨可取，取件尚未成功。</td>";
                                        }
                                        else if (receive_option == "15") //取件中
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>司機取件中。</td>";
                                        }
                                        else if (receive_option == "8") //超大超重 超過規格
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>貨件超過規格，取件尚未成功。</td>";
                                        }

                                        else if (receive_option == "7") //同業已取
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>貨件已被同業收走，取件尚未成功。</td>";
                                        }
                                        else if (receive_option == "22") //公司休息
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>公司休息，取件尚未成功。</td>";
                                        }

                                        else if (receive_option == "12") //地址錯誤
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>地址錯誤，取件尚未成功。</td>";
                                        }
                                        
                                        else if (receive_option == "17" || receive_option == "9") //取消退貨
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>客戶取消退貨，取件尚未成功</td>";
                                        }

                                        else if (receive_option == "24") //拒絕交貨
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>拒絕交貨，取件尚未成功</td>";
                                        }

                                        else if (receive_option == "23") //地址錯誤
                                        {
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>未用MOMO原標籤，取件尚未成功</td>";
                                        }

                                        else if ( receive_option == "20") //集貨成功
                                        {
                                            var pick_up_station_name = dt.Rows[i]["station_name"].ToString() ?? "站";
                                            viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                            viewlist += "<td data-th='貨物追蹤' class='padd8'>司機已成功取件</td>";

                                        }

                                    
                                    else
                                    {
                                        var pick_up_station_name = dt.Rows[i]["station_name"].ToString() ?? "站";
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>取件尚未成功。</td>";
                                    }
                                }

                                else if (scan_item == "6")
                                {

                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>貨件已抵達集貨站所</td>";
                                    
                                }
                                else if (scan_item == "7")
                                {
                                    var driver_code = dt.Rows[i]["driver_code"].ToString();
                                    var transfer_station = "";   //轉運站
                                    if (driver_code == "PP990" || driver_code == "D2901" || driver_code == "PP991" || driver_code == "PP002" || driver_code == "PP992" || driver_code == "PP001")
                                    {
                                        transfer_station = "龜山";
                                    }
                                    else if (driver_code == "PP993" || driver_code == "PP520" || driver_code == "PP920")
                                    {
                                        transfer_station = "高雙";
                                    }
                                    else if (driver_code == "C1016" || driver_code == "PP491" || driver_code == "Z010960")
                                    {
                                        transfer_station = "台中";
                                    }
                                    else if (driver_code == "PP003")
                                    {
                                        transfer_station = "大園";
                                    }
                                    else { }
                                    viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                    viewlist += "<td data-th='貨物追蹤' class='padd8'>貨件已抵達"+ transfer_station + "轉運中心</td>";
                                }
                                else if (scan_item == "1")
                                {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>貨件已抵達配送站所</td>";
                                }
                                else if (scan_item == "2")
                                {
                                    viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                    viewlist += "<td data-th='貨物追蹤' class='padd8'>司機正在進行貨件配送</td>";
                                }
                                else if (scan_item == "3")
                                {

                                    if (dt.Rows[i]["arrive_option"].ToString() == "1")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>客戶不在，貨件尚未配達</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "2")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>另約時間配送，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "3")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>司機已成功配達</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "4")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>收貨人拒絕收件，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "5")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>收貨人地址異常，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "6")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>該地址查無此人，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "11")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>公司行號休息，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "26")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>聯絡不上收件人，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "21")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>收件人預約到站自取，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "47")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>投遞異常，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "H")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>貨物喪失，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "48")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>收件人預約到站自取，貨件尚未配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "8")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>司機已成功配達。</td>";
                                    }
                                    else if (dt.Rows[i]["arrive_option"].ToString() == "51")
                                    {
                                        viewlist += "<td data-th='作業時間' class='padd8' style='width:180px'>" + dt.Rows[i]["scan_date"].ToString() + "</td>";
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'>司機正在進行貨件配送。</td>";
                                    }

                                    else
                                    {
                                        viewlist += "<td data-th='貨物追蹤' class='padd8'></td>";
                                    }
                                }
                                viewlist += "</tr> ";
                            //}
                            //else
                            //{
                            //    temp = Convert.ToInt32(dt.Rows[i]["scan_item"].ToString());
                            //}
                        }
                    }
                    else
                    {

                    }
                    using (SqlCommand cmdA = new SqlCommand())
                    {
                        if (rb1.Checked)
                        {
                            if (check_number.Text != "")
                            {
                                cmdA.Parameters.AddWithValue("@check_number", check_number.Text);
                                cmdA.CommandText = string.Format(@"Select top 1  Convert(varchar, Cdate,120) as Cdate from  tcDeliveryRequests  with(nolock)  where  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate() and check_number = @check_number order by request_id DESC ");
                            }
                        }
                        if (rb2.Checked)
                        {
                            if (order_number.Text != "")
                            {
                                cmdA.Parameters.AddWithValue("@order_number", order_number.Text);
                                cmdA.CommandText = string.Format(@"Select top 1  Convert(varchar, Cdate,120) as Cdate from  tcDeliveryRequests  with(nolock)   where  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate() and order_number = @order_number order by request_id DESC ");
                            }
                        }
                        using (DataTable dtt = dbAdapter.getDataTable(cmdA))
                        {
                            if(dtt.Rows.Count > 0)
                            {
                                if (jjj % 2 == 0)
                                {
                                    viewlist += "<tr> ";
                                }
                                else
                                {
                                    viewlist += "<tr style='background-color:rgb(224,237,248)'> ";
                                }

                                aaa = "1";
                            }


                           
                        }

                    }

                    if (aaa == "" && jjj ==0)
                    {


                        viewlist += "<tr> ";
                        viewlist += "<td colspan = '2' style ='text-align:center' > 尚無資料 </td >";
                        viewlist += "</tr> ";
                    }


                    Literal1.Text = viewlist;


                }

            }
        }


    }


    protected void rb_CheckedChanged(Object sender, EventArgs e)
    {
        string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
        if (rb1.Checked)
        {
            check_number.Enabled = true;
            order_number.Enabled = false;
            check_number.Text = "";
            order_number.Text = "";
        }
        if (rb2.Checked)
        {
            check_number.Enabled = false;
            order_number.Enabled = true;
            check_number.Text = "";
            order_number.Text = "";
        }

    }
}