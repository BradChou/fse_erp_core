﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMember.master" AutoEventWireup="true" CodeFile="member_3-edit.aspx.cs" Inherits="member_3_edit" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />





    <script type="text/javascript">
        function chkfloat(value) {
            value = value.replace(/[^\d.]/g, '');
            value = value.replace(/^\./g, '');
            value = value.replace(/\.{2,}/g, '');
            value = value.replace('.', '$#$').replace(/\./g, '').replace('$#$', '.');

            if (value == "") {
                return value;
            }
            else {
                if (parseInt(value) < 0 || isNaN(parseInt(value)))
                    return 0;
                else if (parseInt(value) > 100)
                    return 100;
                else return value;
            }


        }

        function chknum(value) {
            value = value.replace(/[^\d]/g, '');
            if (value == "") {
                return value;
            }
            else {
                if (parseInt(value) < 0 || isNaN(parseInt(value)))
                    return 0;
                else if (parseInt(value) > 999999999999)
                    return 999999999999;
                else return value;
            }
        }


        $(document).ready(function () {
            $('.fancybox').fancybox();

            $("#hlFeeAdd").click(function () {
                var _link = "feeupload.aspx?";
                _link = _link + "customer_code=" + $(".cus_code1").val() + $(".cus_code2").val() + $(".cus_code3").val();
                _link = _link + "&pricing_code=" + $(".pricing_code").val();
                _link = _link + "&cus=" + $("._cus_id").html();
                $(this).prop("href", _link);
            });

            $("#fee").click(function () {
                var link = "member_3_edit_view.aspx?";
                link = link + "customer_code=" + $(".cus_code1").val() + $(".cus_code2").val() + $(".cus_code3").val();
                $(this).prop("href", link);
            });



            $("#<%= ticket_period.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '-1':
                        $("#<%= ticket.ClientID%>").show();
                        $("#<%= ticket.ClientID%>").focus();
                        break;
                    default:
                        $("#<%= ticket.ClientID%>").hide();
                        break;

                }
            });


            $("#<%= close_date.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '-1':
                        $("#<%= tbclose.ClientID%>").show();
                        $("#<%= tbclose.ClientID%>").focus();
                        break;
                    default:
                        $("#<%= tbclose.ClientID%>").hide();
                        $("#<%= rv1.ClientID%>").html('');
                        $("#<%= tbclose.ClientID%>").val('1');
                        break;

                }
            });

            $("#<%= individual_fee.ClientID%>").change(function () {
                if ($(this).is(":checked")) {
                    $("#<%= tariffd_table.ClientID%>").hide();
                    $("#<%= pan_price_list.ClientID%>").hide();

                } else {
                    $("#<%= tariffd_table.ClientID%>").show();
                    $("#<%= pan_price_list.ClientID%>").show();
                }
            });

        });
        $.fancybox.update();

        function price_del(e) {

            var the_del = $("input[name=del_" + e);
            var the_t = the_del.closest("tr").find(".p_date").html();
            var ischk = confirm("確定刪除" + the_t + "價格資訊? ");
            if (ischk) {
                $.ajax({
                    type: "POST",
                    url: "member_3-edit.aspx/PriceDel",
                    data: '{pid: "' + e + '",type:"' + document.getElementById("HidetypeDiv").innerHTML + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    urlasync: false,//同步               
                    failure: function (response) {
                        alert(response.d);
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d == '1') {
                        the_del.closest('tr').remove();
                        alert("刪除成功");
                    } else {
                        alert(data.d);
                        location.reload();
                    }
                });
            }
        }


        function price_dw(e, c) {
            var cus_code = $(".cus_code1").val() + $(".cus_code2").val() + $(".cus_code3").val();
            var the_dw = $("input[name=dw_" + e);
            var the_t = the_dw.closest("tr").find(".p_date").html();
            var url_dw = "";
            var the_e = TryParseInt(e, 0);


            if (the_e == 0) {
                url_dw = "GetDownload.aspx?cus=" + $("._cus_id").html() + "&dw=1&pub=1&cus_c=" + cus_code;
            } else {
                url_dw = "GetDownload.aspx?cus=" + c + "&dw=1&pub=0&cus_c=" + cus_code + "&atdate=" + the_t;
            }

            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            //setTimeout("parent.$.fancybox.close()", 2000);
        }

        function TryParseInt(str, defaultValue) {
            var retValue = defaultValue;
            if (str != null) {
                if (str.toString().length > 0) {
                    if (!isNaN(str)) {
                        retValue = parseInt(str);
                    }
                }
            }
            return retValue;
        }

    </script>
    <style type="text/css">
        .hide {
            display: none;
        }

        input[type="checkbox"] {
            display: inherit;
            margin-right: -40px;
        }

        input[type="radio"] {
            display: inline-flex;
            margin-left: 150px;
            margin-right: -60px;
        }

        .SpaceInline {
            display: inline;
            margin-left: 5px;
        }

        .Space label {
            margin-right: 10px;
            margin-top: 7px;
            font-size: 11px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .span_tip {
            margin-left: 15px;
            color: red;
        }

        .col-lg-12 {
            width: 50%;
        }

        .tb_price_log {
            width: 40%;
            margin: 10px 0px;
            border: 1px solid #ddd;
        }

            .tb_price_log th {
                background-color: #39ADB4;
                color: white;
                text-align: center;
                padding: 5px;
            }

            .tb_price_log td {
                text-align: center;
                padding: 5px;
            }

        .bk_color1 {
            background-color: #f9f9f9;
        }

        .tb_price_th {
            width: 15%;
        }

        .p_date {
            width: 27%;
        }

        .p_type {
            width: 23%;
        }

        .p_set {
            width: 35%;
        }

        .p_td_null {
            text-align: center;
            padding: 10px;
        }

        ._price_today {
            color: blue;
            font-weight: bold;
        }

        ._ship_title {
            font-size: 20px;
            margin-right: 30px;
        }

        ._cus_title {
            width: 13%;
        }

        ._cus_titleA {
            width: 16%;
        }

        ._cus_data {
            color: #8a9092;
        }

        .cus_main {
            height: 720px;
            overflow-y: auto;
            padding: 5px;
        }

        .row {
            margin: 0px;
        }

        .btn {
            margin: 0px 5px;
        }

        ._btn_area {
            margin-top: 10px;
            margin-left: 150px
        }

        .div_price_log {
            max-height: 210px;
            overflow-y: auto;
        }

        .tb_price_log tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .scrollbar::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }

        .wi100 {
            width: 100%;
        }

        .fl {
            float: left;
        }

        .heightauto {
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper" class="cus_main scrollbar">
        <div class="row">
            <h2 class="margin-bottom-10">客戶資料修改</h2>
            <div class="templatemo-login-form">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 form-group form-inline">
                                <label class="_cus_title">客戶代號</label>
                                <asp:TextBox ID="master_code" runat="server" Enabled="false" CssClass="cus_code1"></asp:TextBox>-
                                <asp:TextBox ID="second_code_1" runat="server" Enabled="false" Width="40px" CssClass="cus_code2"></asp:TextBox>-
                                <asp:TextBox ID="second_code_2" runat="server" Enabled="false" Width="40px" CssClass="cus_code3"></asp:TextBox>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label class="_cus_title">
                                    <asp:Literal ID="lisupplier_code" runat="server" Text="負責區配商"></asp:Literal></label>
                                <asp:Label ID="supplier_code" runat="server" CssClass="_cus_data"></asp:Label>
                                -
                                <asp:Label ID="supplier_name" runat="server" CssClass="_cus_data"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="customer_name" class="_cus_title _tip_important">客戶名稱</label>
                                <asp:TextBox ID="customer_name" CssClass="form-control" runat="server" MaxLength="45"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="customer_name" ForeColor="Red" ValidationGroup="validate">請輸入客戶名稱</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="shortname" class="_cus_title _tip_important">簡　　　稱</label>
                                <asp:TextBox ID="shortname" CssClass="form-control" runat="server" MaxLength="18" placeholder="請輸入簡稱"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="shortname" ForeColor="Red" ValidationGroup="validate">請輸入簡稱</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                <label for="uni_number" class="_cus_title _tip_important">統一編號</label>
                                <asp:TextBox ID="uni_number" CssClass="form-control" runat="server" MaxLength="8" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="ex: 12345678"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="reg_uni_number" runat="server" ControlToValidate="uni_number" ForeColor="Red" ValidationGroup="validate">請輸入統一編號</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="reg_uni_number_type" runat="server" ErrorMessage="統一編號格式有誤" ValidationExpression="(^\d{8}$)"
                                    ControlToValidate="uni_number" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="principal" class="_cus_title _tip_important">對　帳　人</label>
                                <asp:TextBox ID="principal" CssClass="form-control" runat="server" placeholder="ex: 王小明" MaxLength="18"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="principal" ForeColor="Red" ValidationGroup="validate">請輸入對帳人</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="tel" class="_cus_title _tip_important">電　　話</label>
                                <asp:TextBox ID="tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="15" placeholder="ex:0910XXXXXX"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="fax" class="_cus_title">傳　　　真</label>
                                <asp:TextBox ID="fax" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="15"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <label class="_cus_title _tip_important">出貨地址</label>
                                        <asp:DropDownList ID="City" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="CityArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:TextBox ID="address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="City" ForeColor="Red" ValidationGroup="validate">請選擇縣市</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="CityArea" ForeColor="Red" ValidationGroup="validate">請選擇鄉鎮區</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                        <br />
                                        <div>
                                            <div>
                                                <asp:RadioButton ID="shippingAddress" runat="server" Text="由出貨地址判斷" CssClass="Space" GroupName="rad" />
                                            </div>
                                            <div class="SpaceInline">
                                                <asp:RadioButton ID="exclusiveSendStation" runat="server" Text="綁訂專屬區配商" CssClass="Space" GroupName="rad" />
                                                <asp:DropDownList ID="supplier" runat="server" AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="business_people" class="_cus_title _tip_important">營業人員</label>
                                <asp:TextBox ID="business_people" CssClass="form-control" runat="server" placeholder="ex:王大明" MaxLength="15"></asp:TextBox>

                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">計績營業員</label>
                                <asp:DropDownList ID="ddl_sales_id" CssClass="form-control chosen-select" runat="server" Enabled="true"></asp:DropDownList>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <label for="email" class="_cus_title _tip_important">E-mail</label>
                                <asp:TextBox ID="email" CssClass="form-control" runat="server" MaxLength="40" placeholder="ex: aaa@gmail.com"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req9" runat="server" ControlToValidate="email" ForeColor="Red" ValidationGroup="validate">請輸入e-mail</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="email" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">計績站所</label>
                                <asp:DropDownList ID="ddl_sale_station" CssClass="form-control  _ddl chosen-select" runat="server" Enabled="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="ticket_period" class="_cus_title _tip_important">票期</label>
                                <asp:DropDownList ID="ticket_period" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">請選擇</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem Value="-1">其他</asp:ListItem>
                                </asp:DropDownList><asp:TextBox ID="ticket" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入票期天數"></asp:TextBox><label for="inputFirstName" class="_cus_title">(天)</label>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req11" runat="server" ControlToValidate="ticket_period" ForeColor="Red" ValidationGroup="validate">請選擇票期</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="SPM_code" class="_cus_title">計價模式</label>
                                <asp:DropDownList ID="SPM_code" runat="server" CssClass="form-control pricing_code" Enabled="false"></asp:DropDownList>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="stop_code" class="_cus_title _tip_important">停運原因</label>
                                <asp:DropDownList ID="stop_code" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="0" Selected="True">正常</asp:ListItem>
                                    <asp:ListItem Value="1">同業競爭</asp:ListItem>
                                    <asp:ListItem Value="2">公司倒閉</asp:ListItem>
                                    <asp:ListItem Value="3">呆帳</asp:ListItem>
                                    <asp:ListItem Value="4">其他原因</asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="stop_memo" CssClass="form-control" runat="server" placeholder="其他說明" MaxLength="80"></asp:TextBox>
                            </div>
                        </div>



                        <%--<div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="dlLT_fee">零擔運價</label>
                                <asp:DropDownList ID="dlLT_fee" runat="server" CssClass=" form-control">
                                    <asp:ListItem Value="">請選擇</asp:ListItem>
                                    <asp:ListItem Value="1">經理</asp:ListItem>
                                    <asp:ListItem Value="2">協理</asp:ListItem>
                                    <asp:ListItem Value="3">總經理</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        <div class="row" id="div_range" runat="server">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label>貨號區間</label>
                                <asp:Label ID="lbNumberRange" runat="server" CssClass="form-control heightauto"></asp:Label>
                            </div>
                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <label class="_cus_title _tip_important">指派SD</label>
                                        <asp:DropDownList ID="DropDownListSD" runat="server" CssClass="form-control" AutoPostBack="true" Width="250"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="row" id="delivery" runat="server">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label>Logo設定</label>
                                <asp:DropDownList ID="delivery_Type" runat="server" CssClass="form-control _ddl_stop">
                                    <asp:ListItem Value="">請選擇</asp:ListItem>
                                    <asp:ListItem Value="1">FSE</asp:ListItem>
                                    <asp:ListItem Value="2">JF</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <label class="_cus_title _tip_important">指派MD</label>
                                        <asp:DropDownList ID="DropDownListMD" runat="server" CssClass="form-control" AutoPostBack="true" Width="250"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <label class="_cus_title _tip_important">發票地址</label>
                                        <asp:DropDownList ID="InvoiceCity" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="InvoiceCity_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="InvoiceArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:TextBox ID="InvoiceAddress" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                        <asp:Button ID="sameAddress" runat="server" OnClick="insertSameAddress" Text="同出貨地址" class="btn btn-primary"></asp:Button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="product_type_label" runat="server">商品類型</asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:DropDownList ID="product_type" runat="server" CssClass="form-control _ddl_stop">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req20" runat="server" ControlToValidate="product_type" ForeColor="Red" ValidationGroup="validate">請輸入商品類型</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label class="_tip_important">選擇主客代</label>
                                <asp:DropDownList ID="ddlMasterCustomerCode" runat="server" CssClass="form-control _ddl chosen-select">
                                </asp:DropDownList>

                            </div>

                        </div>

                        <div class="col-lg-6 col-md-6 form-group form-inline">
                            <span class="checkbox checkbox-success">
                                <asp:CheckBox ID="return_address_chk" runat="server" Text="依照寄件地址退貨" />
                            </span>


                            <div class="row">

                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="is_new_customer" runat="server" Text="是否為 2022/5/1後 新合約客戶" Enabled="false" />
                                    </span>
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="OldCustomerOverCBM" runat="server" Text="是否為 舊制超材費" Enabled="false" />
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="is_weekend_delivered" runat="server" Style="display: none" Text="啟用假日配送" />
                                    </span>
                                    <span>
                                        <asp:TextBox ID="weekend_delivery_fee" CssClass="form-control" runat="server" Style="display: none" placeholder="假日配送加價費用" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                    </span>
                                </div>
                            </div>
                            <hr />
                            <div class="row" id="bank_data" runat="server">
                                <div class="col-lg-12 col-md-12 form-group form-inline">
                                    <label for="inputLastName">銀　　行</label>
                                    <asp:DropDownList ID="bank" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true"></asp:DropDownList>

                                </div>

                                <div class="col-lg-12 col-md-12 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">分　　　行</label>

                                    <asp:TextBox ID="bank_branch" CssClass="form-control　_bank" Width="45%" runat="server" placeholder="例如:南港分行，不需填銀行全名" MaxLength="15"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title ">帳　　號</label>
                                        <asp:TextBox ID="bank_account" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="14" Width="45%" placeholder="不需填銀行代碼與特殊符號，例如' - '"></asp:TextBox>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="contract_sdate" class="_cus_title">合約生效日</label>
                                    <asp:TextBox ID="contract_sdate" runat="server" class="form-control date_picker" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="contract_edate" class="_cus_title">到　期　日</label>
                                    <asp:TextBox ID="contract_edate" runat="server" class="form-control date_picker" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 form-group form-inline">
                                    <label class="_cus_title">合約內容</label>
                                    <div style="display: none">
                                        <asp:TextBox ID="filename" runat="server" />
                                    </div>
                                    <a href="#" id="hlFileView" class="fa fa-file-text" aria-hidden="true">合約內容</a>
                                    <a href="fileupload.aspx?filename=<%=filename.ClientID %>&fileview=hlFileView" class="fancybox fancybox.iframe" id="hlFileAdd"><span class="btn btn-primary" id="buttonA" runat="server">上傳新合約</span></a>
                                </div>
                                <div class="col-lg-12 form-group form-inline">
                                    <label for="close_date" class="_cus_title _tip_important">結　帳　日</label>
                                    <asp:DropDownList ID="close_date" runat="server" class="form-control">
                                        <asp:ListItem Value="">請選擇</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="25">25</asp:ListItem>
                                        <asp:ListItem Value="31">(31)30</asp:ListItem>
                                        <asp:ListItem Value="-1">其他</asp:ListItem>
                                    </asp:DropDownList><asp:TextBox ID="tbclose" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入結帳日"></asp:TextBox>
                                    <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="tbclose" ErrorMessage="結帳日需需介於 1~31" ForeColor="Red" MaximumValue="31" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator Display="Dynamic" ID="req12" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請選擇結帳日</asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="individual_fee" runat="server" Text="獨立計價(不採運價表計算託運單價格)" />
                                    </span>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="billing_special_needs" class="_cus_title">帳單特殊需求</label>
                                    <asp:TextBox ID="billing_special_needs" CssClass="form-control" runat="server" MaxLength="220"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div id="tariffd_table" runat="server" class=" col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">運價生效日</label>
                                    <asp:Label ID="fee_sdate" runat="server" class="_fee_sdate _cus_data"></asp:Label>
                                    <asp:Label ID="lbl_PriceTip" runat="server" CssClass="text-primary span_tip"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">發包價</label>
                                    <asp:TextBox ID="contract_price" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row" runat="server" id="div_LT_fee">
                                <hr />
                                <span class="margin-bottom-10 _ship_title fl wi100 ">費用設定</span>
                                <asp:Literal ID="Price" runat="server"></asp:Literal>
                            </div>
                            <asp:Panel runat="server" ID="pan_price_list">
                                <hr />
                                <span class="margin-bottom-10 _ship_title">運價表設定</span>
                                <input name="dw_0" type="button" id="buttonC" runat="server" class="btn btn-primary" value="下載公版運價" onclick='price_dw(0, 0)' />

                                <a class="fancybox fancybox.iframe" id="hlFeeAdd">
                                    <span class="btn btn-primary" id="buttonB" runat="server">上傳新運價</span>
                                </a>
                                <a class="fancybox fancybox.iframe" id="fee">
                                    <span class="btn btn-primary" id="Span1" runat="server">檢視目前運價</span>
                                </a>
                            </asp:Panel>
                        </div>
                    </div>


                    <div style="display: none" id="HidetypeDiv">
                        <asp:Literal ID="Hidetype" runat="server"></asp:Literal>
                    </div>
                    <hr />
                    <div style="overflow: auto; height: auto; width: 100%">
                        <%--<asp:Chart ID="LineChart" runat="server" BackColor="#D3DFF0" Width="1000px" BorderColor="26, 59, 105" Palette="BrightPastel" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2">
                        <Titles>
                            <asp:Title ShadowColor="32,0,0,0" Font="Times New Roman, 14pt, style=Bold" ShadowOffset="3" Text="過去12個月的趨勢圖 (來源：月結帳資料) " ForeColor="26,59,105"></asp:Title>
                        </Titles>
                        <Legends>
                            <asp:Legend LegendStyle="Column" IsTextAutoFit="false" DockedToChartArea="LineChartArea" Docking="Top" Name="Line" BackColor="Transparent" Font="Times New Roman, 10pt, style=Bold"></asp:Legend>
                            <%--<asp:Legend LegendStyle="Column" IsTextAutoFit="false" DockedToChartArea="LineChartArea" Docking="Top" Name="Line2" BackColor="Transparent" Font="Times New Roman, 10pt, style=Bold"></asp:Legend>
                        </Legends>
                        <BorderSkin SkinStyle="Emboss"></BorderSkin>
                        <Series>
                            <asp:Series IsValueShownAsLabel="true" ChartArea="LineChartArea" Name="Line" CustomProperties="LabelStyle=Bottom" BorderColor="180, 26, 59, 105" LabelFormat=""></asp:Series>
                            <%--<asp:Series IsValueShownAsLabel="true" ChartArea="LineChartArea" Name="Line2" CustomProperties="LabelStyle=Bottom" BorderColor="18, 26, 59, 105" LabelFormat=""></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="LineChartArea" BorderColor="64,64,64,64" BorderDashStyle="Solid" BackSecondaryColor="White" BackColor="64,165,191,228" ShadowColor="Transparent" BackGradientStyle="TopBottom">
                                <Area3DStyle Rotation="10" Perspective="10" Inclination="15" IsRightAngleAxes="false" WallWidth="0" IsClustered="false" />
                                <AxisY Title="金額" LineColor="64,64,64,64" IsLabelAutoFit="false" ArrowStyle="Triangle">
                                    <LabelStyle Font="Times New Roman, 10pt, style=Bold" />
                                    <MajorGrid LineColor="64,64,64,64" />
                                </AxisY>
                                <AxisX Title="月份" LineColor="64,64,64,64" IsLabelAutoFit="false" ArrowStyle="Triangle" Interval="1">
                                    <LabelStyle Font="Times New Roman, 10pt, style=Bold" IsStaggered="true" />
                                    <MajorGrid LineColor="64,64,64,64" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>--%>
                    </div>
                    <div class="form-group text-center _btn_area ">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button btn" runat="server" Text="確　認" OnClick="btnsave_Click" ValidationGroup="validate" />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="member_3.aspx" class="templatemo-white-button btn"><span>取　消 / 回到上一頁</span></a>
                        <asp:Label ID="lb_cusid" CssClass="_cus_id hide" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

