﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Net.Mail;
using System.IO;


public partial class system_6 : System.Web.UI.Page
{

    public string ssmanager_type
    {
        get { return ViewState["ssmanager_type"].ToString(); }
        set { ViewState["ssmanager_type"] = value; }
    }

    public string sssupplier_code
    {
        get { return ViewState["sssupplier_code"].ToString(); }
        set { ViewState["sssupplier_code"] = value; }
    }

    public int  ssAccount_id
    {
        get { return (int)ViewState["ssAccount_id"]; }
        set { ViewState["ssAccount_id"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            ssmanager_type = Session["manager_type"].ToString(); //管理單位類別   
            sssupplier_code = Session["master_code"].ToString();
            sssupplier_code = (sssupplier_code.Length >= 3) ? sssupplier_code.Substring(0, 3) : "";
            if (sssupplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (ssmanager_type)
                {
                    case "1":
                        sssupplier_code = "";
                        break;
                    default:
                        sssupplier_code = "000";
                        break;
                }
            }


            SetDefaultMagType();
            if (Request.QueryString["manager_type"] != null)
            {
                rbl_typeForSh.SelectedValue = Request.QueryString["manager_type"];

            }
            readdata();

            //div_add.Visible = false;
            //div_list.Visible = true;


            // RadioButtonList1_SelectedIndexChanged(null, null);

        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();
        #region 關鍵字
        if (rbl_typeForSh.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@manager_type", rbl_typeForSh.SelectedValue.ToString());
            strWhereCmd += " and manager_type = @manager_type";
            querystring += "&manager_type=" + rbl_typeForSh.SelectedValue;
        }
        if (sssupplier_code != "")
        {
            cmd.Parameters.AddWithValue("@head_code", sssupplier_code);
            strWhereCmd += " and head_code = @head_code";
        }
        

        #endregion

        //cmd.CommandText = string.Format(@"select A.* , B.customer_shortname, CASE A.active_flag WHEN 0 THEN '停用' ELSE '啟用' END AS statustext
        //                                  from tbAccounts A With(Nolock) 
        //                                  left join tbCustomers B With(Nolock) on A.master_code = B.master_code
        //                                  where 0 = 0   {0} order by account_code", strWhereCmd);

        cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                            select CASE  WHEN  c.emp_name IS NULL THEN a.user_name ELSE c.emp_name END AS user_name,
                                            A.account_id, A.account_code, A.password, A.active_flag, A.head_code, A.body_code, A.master_code, A.manager_type, 
                                            A.manager_unit, A.job_title, A.emp_code, A.user_email, A.cuser, A.cdate, A.uuser, A.udate,
                                            B.customer_shortname, CASE A.active_flag WHEN 0 THEN '停用' ELSE '啟用' END AS statustext,
                                            ROW_NUMBER() OVER (PARTITION BY A.master_code ORDER BY A.master_code DESC) AS rn
                                            from tbAccounts A With(Nolock) 
                                            left join tbCustomers B With(Nolock) on A.master_code = B.master_code
                                            left join tbEmps C With(Nolock) on A.emp_code=C.emp_code                                            
                                            where 0 = 0 {0} 
                                            )
                                            SELECT *
                                            FROM cus
                                            WHERE rn = 1 ", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    protected void btnew_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false; //true:add false:edit     
        Boolean IsOk = true;
        if (ssAccount_id > 0) IsEdit = true;

        lbErr.Text = "";
        lbErr.Visible = false;
        lbuserErr.Text = "";
        lbuserErr.Visible = false;
        string account_code = "";
        string password = "";
        string serialnum = "";
        string master_code = "";
        Boolean IsGunfu = false;
        if (new String[] { "1", "2" }.Contains(rbmanager_type.SelectedValue)) IsGunfu = true;


        if (IsEdit)
        {
            SqlCommand cmd = new SqlCommand();
           
            cmd.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());              //啟動狀態(0:停用 1:啟用)
            cmd.Parameters.AddWithValue("@job_title", job_title.Text.ToString());                         //職稱
            if (!IsGunfu)
            {
                cmd.Parameters.AddWithValue("@user_name", user_name.Text.ToString()); //人員名稱
            }

            cmd.Parameters.AddWithValue("@user_email", email.Text.ToString());                            //信箱
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_id", ssAccount_id.ToString());
            cmd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmd);
            dbAdapter.execNonQuery(cmd);
            SetEditOrAdd(0);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('修改成功');</script>", false);
            return;
        }
        else
        {


            #region 檢查人員是否重覆建 
            if (IsGunfu)
            {
                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Parameters.AddWithValue("@emp_code", ddl_user.SelectedValue);
                    _cmd.CommandText = "SELECT 1 FROM tbAccounts WHERE ISNULL(emp_code,'') = @emp_code ";
                    if (dbAdapter.getDataTable(_cmd).Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + account_code + "已經有此人員紀錄，請重新確認。');</script>", false);
                        return;
                    }
                }
            }

            #endregion

            switch (rbmanager_type.SelectedValue)
            {
                case "1": //峻富總公司
                case "2": //峻富一般員工
                    if (ddl_user.SelectedValue == "")
                    {
                        lbuserErr.Text = "請選取人員";
                        lbuserErr.Visible = true;
                        ddl_user.Focus();
                        return;
                    }
                    SqlCommand cmdb = new SqlCommand();
                    DataTable dtb = new DataTable();
                    cmdb.CommandText = "select isnull(max(body_code),0) from tbAccounts where head_code = '999'";
                    dtb = dbAdapter.getDataTable(cmdb);
                   

                    if (dtb.Rows.Count > 0)
                    {
                        serialnum = (Convert.ToInt32(dtb.Rows[0][0].ToString()) + 1).ToString();
                        while (serialnum.Length < 4)
                        {
                            serialnum = "0" + serialnum;
                        }
                    }
                    account_code = "999" + serialnum;
                    master_code = "0000000";
                    break;
                case "3":  //峻富自營
                    if (dlmaster2.SelectedValue == "")
                    {
                        lbErr.Text = "請指定要建立帳號的峻富自營商";
                        lbErr.Visible = true;
                        dlmaster2.Focus();
                        return;
                    }
                    else if (user_name.Text == "")
                    {
                        lbuserErr.Text = "請輸入人員";
                        lbuserErr.Visible = true;
                        user_name.Focus();
                        return;
                    }
                    else
                    {
                        account_code = account2.Text + dlmaster2.SelectedValue;
                        master_code = account_code;
                    }
                    break;
                case "4":  //區配商
                    SqlCommand cmd2 = new SqlCommand();                   
                    cmd2.CommandText = string.Format("select id_no, uniform_numbers from tbSuppliers where supplier_name like  '" + dlmaster3.SelectedValue + "' ");

                    DataTable dt2 = dbAdapter.getDataTable(cmd2);
                   
                    if (dlmaster3.SelectedValue == "")
                    {
                        lbErr.Text = "請指定要建立帳號區配商";
                        lbErr.Visible = true;
                        dlmaster3.Focus();
                        return;
                    }
                    else if (user_name.Text == "")
                    {
                        lbuserErr.Text = "請輸入人員";
                        lbuserErr.Visible = true;
                        user_name.Focus();
                        return;
                    } 
                    else if (dt2.Rows[0]["uniform_numbers"].ToString() != "")
                    {

                        account_code = dt2.Rows[0]["uniform_numbers"].ToString();
                    }
                    
                    else if (dt2.Rows[0]["id_no"].ToString() != "")
                    {
                        account_code = dt2.Rows[0]["id_no"].ToString();
                        //master_code = account_code;
                    }
                    
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('未填入身分證字號(統一編號)，請至「區配商設定」修改');</script>", false);
                        return;
                    }


                    break;
                case "5":  //區配自營

                    //    SqlCommand cmd4 = new SqlCommand();
                    //    cmd4.CommandText = string.Format("select id_no, uniform_numbers from tbSuppliers where supplier_name like  '" + head_code.SelectedValue + "' ");

                    //    DataTable dt4 = dbAdapter.getDataTable(cmd4);

                    //    if (head_code.SelectedValue == "")
                    //    {
                    //        lbErr.Text = "請指定要建立帳號區配商";
                    //        lbErr.Visible = true;
                    //        dlmaster3.Focus();
                    //        return;
                    //    }
                    //    else if (user_name.Text == "")
                    //    {
                    //        lbuserErr.Text = "請輸入人員";
                    //        lbuserErr.Visible = true;
                    //        user_name.Focus();
                    //        return;
                    //    }
                    //    else if (dt4.Rows[0]["uniform_numbers"].ToString() != "")
                    //    {

                    //        account_code = dt4.Rows[0]["uniform_numbers"].ToString();
                    //    }

                    //    else if (dt4.Rows[0]["id_no"].ToString() != "")
                    //    {
                    //        account_code = dt4.Rows[0]["id_no"].ToString();
                    //        //master_code = account_code;
                    //    }

                    //    else
                    //    {
                    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('未填入身分證字號(統一編號)，請至「區配商設定」修改');</script>", false);
                    //        return;
                    //    }
                    //    break;

                    if (head_code.SelectedValue == "" || body_code.SelectedValue == "")
                    {
                        if (head_code.SelectedValue == "")
                        {
                            head_code.Focus();
                        }
                        else
                        {
                            body_code.Focus();
                        }
                        lbErr.Text = "請指定要建立帳號的自營商";
                        lbErr.Visible = true;
                        return;
                    }
                    else if (user_name.Text == "")
                    {
                        lbuserErr.Text = "請輸入人員";
                        lbuserErr.Visible = true;
                        user_name.Focus();
                        return;
                    }
                    else
                    {
                        account_code = head_code.SelectedValue + body_code.SelectedValue;
                        master_code = account_code;
                    }

                    break;
            }

            #region 密碼產生(大小寫英數字夾雜)
            string randPwd = "";
            do
            {
                randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
            } while (!System.Text.RegularExpressions.Regex.IsMatch(randPwd,
                                                               "\\d+[A-F]+\\d+[A-F]"));
            //具有兩個以上的英文字母，並且中間夾數字才算合格
            //將第二個英文字母變小寫
            int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
            randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

            MD5 md5 = MD5.Create();//建立一個MD5
            password = Utility.GetMd5Hash(md5, randPwd);
            #endregion


            #region 檢查帳號是否存在
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@account_code", account_code);
            cmda.CommandText = "SELECT 1 FROM tbAccounts WHERE account_code=@account_code ";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + account_code + "已經有此帳號紀錄，請重新確認。');</script>", false);
                return;
            }
            #endregion

            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@account_code", account_code);                                   //帳號
            cmd.Parameters.AddWithValue("@password", password);                                           //密碼   大小寫英數字夾雜 (MD5編碼後存入)
            cmd.Parameters.AddWithValue("@head_code", account_code.Substring(0, 3));                      //帳號前三碼
            cmd.Parameters.AddWithValue("@body_code", account_code.Substring(3, 4));                      //帳號後四碼流水號
            cmd.Parameters.AddWithValue("@active_flag", "1");                                             //啟動狀態(0:停用 1:啟用)
            cmd.Parameters.AddWithValue("@master_code", account_code.Substring(0, 7));                                    //主客代碼
            cmd.Parameters.AddWithValue("@manager_type", rbmanager_type.SelectedValue);                   //管理單位類別(0:天眼公司 1:峻富總公司、2:峻富自營、3:區配商、4:區配商自營)
            cmd.Parameters.AddWithValue("@manager_unit", rbmanager_type.SelectedItem.Text);               //管理單位
            cmd.Parameters.AddWithValue("@job_title", job_title.Text.ToString());                         //職稱

            if (IsGunfu)
            {
                cmd.Parameters.AddWithValue("@emp_code", ddl_user.SelectedValue.ToString()); //員工代碼  (tbAccounts.emp_code = tbEmps.emp_code)
                cmd.Parameters.AddWithValue("@user_name", ddl_user.SelectedItem.Text.Split('(')[0]); //人員名稱
            }
            else
            {
                cmd.Parameters.AddWithValue("@user_name", user_name.Text.ToString()); //人員名稱
            }

            cmd.Parameters.AddWithValue("@user_email", email.Text.ToString());                            //信箱
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 
            cmd.CommandText = dbAdapter.SQLdosomething("tbAccounts", cmd, "insert");
            dbAdapter.execNonQuery(cmd);


          
            SqlCommand cmd3 = new SqlCommand();
            DataTable dt3 = new DataTable();
            cmd3.CommandText = string.Format("select vendor_name from truckvendor where vendor_name =  '" + user_name.Text.ToString() + "' ");
            dt3 = dbAdapter.getDataTableForJunfuTrans(cmd3);
            if (dt3.Rows.Count > 0)
            {
                SqlCommand cmd5 = new SqlCommand();
                string updateAccountCode = "update TruckVendor set account_code = @account_code  where vendor_name = '" + user_name.Text.ToString() + "'";
                cmd5.CommandText= string.Format(updateAccountCode);               
                {
                    cmd5.Parameters.AddWithValue("@account_code", account_code);
                    dbAdapter.execNonQueryForJunfuTrans(cmd5);
                }
            }
            else
            {
                cmd3.Parameters.AddWithValue("@vendor_name", user_name.Text.ToString());
                cmd3.Parameters.AddWithValue("@account_code", account_code);
                cmd3.Parameters.AddWithValue("@create_user", account_code);
                cmd3.Parameters.AddWithValue("@create_date", DateTime.Now);
                cmd3.CommandText = dbAdapter.SQLdosomething("TruckVendor", cmd3, "insert");
                dbAdapter.execNonQueryForJunfuTrans(cmd3);
            }
            

            //產生完畢，跳出新視窗提示寄送密碼>>確認>>關閉視窗
            ModalPopupExtender1.Show();
            lbaccount_code.Text = account_code;
            lbpassword.Text = randPwd;
            lbmail.Text = email.Text;
        }
            

    }

    //protected void SetAddArea()
    //{
    //    lbErr.Text = "";
    //    lbErr.Visible = false;
    //    lbuserErr.Text = "";
    //    lbuserErr.Visible = false;
    //    pan_list.Visible = false;
    //    Boolean IsChk = true;
    //    lbCustomer_name.Text = "";
    //    ddl_user.Visible =
    //    div1.Visible =
    //    div2.Visible =
    //    div3.Visible =
    //    div4.Visible = false;
    //    user_name.Visible = true;
    //    string wherestr = "";
    //    switch (rbmanager_type.SelectedValue)
    //    {
    //        case "1"://峻富總公司
    //        case "2"://峻富一般員工

    //            div1.Visible = true;
    //            account1.Text = "999-";
    //            if (SetJunFudriver())
    //            {
    //                ddl_user.Visible = true;
    //                user_name.Visible = false;
    //            }
    //            else
    //            {
    //                IsChk = false;
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('目前無可新增人員，重新確認!');", true);
    //            }
    //            break;
    //        case "3":
    //            div2.Visible = true;
    //            account2.Text = "000";
    //            SqlCommand cmd1 = new SqlCommand();
    //            cmd1.CommandText = string.Format(@"WITH cus AS
    //                                        (
    //                                           SELECT *,
    //                                                 ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
    //                                           FROM tbCustomers where supplier_code ='000' and supplier_id <>'0000'
    //                                        )
    //                                        SELECT supplier_id, supplier_id + '-' +  customer_name as showname
    //                                        FROM cus
    //                                        WHERE rn = 1 ");

    //            dlmaster2.DataSource = dbAdapter.getDataTable(cmd1);
    //            dlmaster2.DataValueField = "supplier_id";
    //            dlmaster2.DataTextField = "showname";
    //            dlmaster2.DataBind();
    //            dlmaster2.Items.Insert(0, new ListItem("請選擇", ""));
    //            dlmaster2_SelectedIndexChanged(null, null);
    //            break;
    //        case "4":
    //            div3.Visible = true;
    //            account3.Text = "0000";

    //            SqlCommand cmd2 = new SqlCommand();                
    //            if (sssupplier_code != "")
    //            {
    //                cmd2.Parameters.AddWithValue("@supplier_code", sssupplier_code);
    //                wherestr = " and supplier_code = @supplier_code";
    //            }
    //            cmd2.CommandText = string.Format (@"select distinct supplier_code, supplier_code + '-' +  supplier_name as  showname  from tbSuppliers where supplier_code <>'000' {0}  order by supplier_code ", wherestr);
    //            dlmaster3.DataSource = dbAdapter.getDataTable(cmd2);
    //            dlmaster3.DataValueField = "supplier_code";
    //            dlmaster3.DataTextField = "showname";
    //            dlmaster3.DataBind();
    //            dlmaster3.Items.Insert(0, new ListItem("請選擇", ""));
    //            dlmaster3_SelectedIndexChanged(null, null);
    //            break;
    //        case "5":
    //            div4.Visible = true;
    //            SqlCommand cmd3 = new SqlCommand();
    //            if (sssupplier_code != "")
    //            {
    //                cmd3.Parameters.AddWithValue("@supplier_code", sssupplier_code);
    //                wherestr = " and supplier_code = @supplier_code";
    //            }
    //            cmd3.CommandText = string.Format (@"select distinct supplier_code , supplier_code + '-' +  supplier_name as  showname  from tbSuppliers  where supplier_code <>'000' {0}   order by supplier_code ", wherestr);
    //            head_code.DataSource = dbAdapter.getDataTable(cmd3);
    //            head_code.DataValueField = "supplier_code";
    //            head_code.DataTextField = "showname";
    //            head_code.DataBind();
    //            head_code.Items.Insert(0, new ListItem("請選擇", ""));
    //            head_code_SelectedIndexChanged(null, null);
    //            body_code_SelectedIndexChanged(null, null);
    //            break;
    //    }

    //    pan_list.Visible = !IsChk;
    //    pan_add.Visible = IsChk;
    //}


    protected void head_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
                                               FROM tbCustomers where supplier_code ='{0}' and supplier_id <>'0000'
                                            )
                                            SELECT supplier_id, supplier_id + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 ", head_code.SelectedValue);

        body_code.DataSource = dbAdapter.getDataTable(cmd);
        body_code.DataValueField = "supplier_id";
        body_code.DataTextField = "showname";
        body_code.DataBind();
        body_code.Items.Insert(0, new ListItem("請選擇", ""));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void btMail_Click(object sender, EventArgs e)
    {
        #region 寄發通知信
        string mailbody = "您好：<br/>帳號：" + lbaccount_code.Text + "已啟用，密碼為：" + lbpassword.Text + "<br>" + ConfigurationManager.AppSettings["systemurl"] + "<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";
        Utility.Mail_Send(ConfigurationManager.AppSettings["Support_sendmail"], new[] { lbmail.Text }, null, "峻富物流管理帳號開通通知", mailbody, true, null);
        #endregion
        ModalPopupExtender1.Hide();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='system_6.aspx';alert('寄送完畢');</script>", false);

        return;

    }
    

    protected void dlmaster2_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.CommandText = "select shipments_principal,shipments_email,customer_shortname from tbCustomers where master_code =  '" + account2.Text + dlmaster2.SelectedValue + "'  ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            user_name.Text = dt.Rows[0]["shipments_principal"].ToString();
            email.Text = dt.Rows[0]["shipments_email"].ToString();
            lbCustomer_name.Text = "【" + dt.Rows[0]["customer_shortname"].ToString() + "】";
        }
    }

    protected void dlmaster3_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.CommandText = "select * from tbSuppliers where supplier_name like  '" + dlmaster3.SelectedValue + "' ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            user_name.Text = dt.Rows[0]["supplier_name"].ToString();
            email.Text = dt.Rows[0]["email"].ToString();
            lbCustomer_name.Text = "【" + dt.Rows[0]["supplier_name"].ToString() + "】";

        }
    }

    protected void body_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.CommandText = "select * from tbCustomers where master_code = '" + head_code.SelectedValue + body_code.SelectedValue + "' ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            user_name.Text = dt.Rows[0]["shipments_principal"].ToString();
            email.Text = dt.Rows[0]["shipments_email"].ToString();
            lbCustomer_name.Text = "【" + dt.Rows[0]["customer_shortname"].ToString() + "】";

        }
    }



    /// <summary>設定峻富司及員工尚未設資料 </summary>
    /// <returns>true:設定成功 false:無未設定資料</returns>
    protected Boolean SetJunFudriver()
    {
        Boolean IsOk = false;
        ddl_user.Items.Clear();
        using (SqlCommand cmd = new SqlCommand())
        {
            String strSQL = @" SELECT emp.emp_code 'id',emp.emp_name +'('+ emp.emp_code+')' 'name'
                                 FROM tbEmps emp With(Nolock)
                                WHERE emp.active_flag IN(1) AND emp.emp_code NOT IN (SELECT emp_code FROM tbAccounts With(Nolock) WHERE ISNULL(emp_code,'') <> ''AND active_flag IN(1)) ";
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    IsOk = true;
                    ddl_user.DataSource = dt;
                    ddl_user.DataValueField = "id";
                    ddl_user.DataTextField = "name";
                    ddl_user.DataBind();
                    ddl_user.Items.Insert(0, new ListItem("請選擇", ""));
                }
            }
        }
        return IsOk;
    }

    /// <summary>設定管理單位初始資料</summary>
    protected void SetDefaultMagType()
    {
        using (SqlCommand cmd = new SqlCommand())
        { 
            String wherestr = "";
            switch (ssmanager_type)
            {
                case "1":
                    wherestr = " and code_id in(1,2,3,4,5) ";
                    break;
                case "2":
                    wherestr = " and code_id in(-1) ";
                    break;
                case "3":
                    wherestr = " and code_id in(-1) ";
                    break;
                case "4":
                    wherestr = " and code_id in(5) ";
                    break;
                case "5":
                    wherestr = " and code_id in(-1) ";
                    break;
            }
            String strSQL = string.Format (@" SELECT code_id 'id',code_name 'name'
                                 FROM tbItemCodes With(Nolock)
                                WHERE code_sclass ='MT' AND active_flag=1 {0}", wherestr);
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                rbmanager_type.Items.Clear();
                rbmanager_type.DataSource = dt;
                rbmanager_type.DataValueField = "id";
                rbmanager_type.DataTextField = "name";
                rbmanager_type.DataBind();
                rbmanager_type.SelectedIndex = 0;

                rbl_typeForSh.Items.Clear();
                rbl_typeForSh.DataSource = dt;
                rbl_typeForSh.DataValueField = "id";
                rbl_typeForSh.DataTextField = "name";
                rbl_typeForSh.DataBind();
                rbl_typeForSh.SelectedIndex = 0;
            }
        }

    }


    protected void btn_Search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        ssAccount_id =0;
        job_title.Text = "";
        user_name.Text = "";
        email.Text = "";
        rbmanager_type_SelectedIndexChanged(rbmanager_type, e);
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        pan_add.Visible = false;
        btn_Add.Visible =
        btn_Search.Visible =
        pan_list.Visible = true;
    }

    protected void rbmanager_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        //SetAddArea();
        SetEditOrAdd(1, 0);
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int i_id = 0;
        if (!int.TryParse(((HiddenField)e.Item.FindControl("hid_id")).Value.ToString(), out i_id)) i_id = 0;

        switch (e.CommandName)
        {
            case "reset":
                string email = "";

                using (SqlCommand cmd = new SqlCommand())
                {
                    DataTable dt;
                    cmd.Parameters.AddWithValue("@account_id", i_id.ToString());
                    cmd.CommandText = " Select user_email from tbAccounts where account_id = @account_id  ";
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                        email = dt.Rows[0]["user_email"].ToString();

                    if (email == "")
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('該帳戶未設定e-mail，無法通知重置後的密碼，如有問題，請聯繫系統管理員');</script>", false);                       
                    }
                    else
                    {
                        string randPwd = "";
                        do
                        {
                            randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
                        } while (!System.Text.RegularExpressions.Regex.IsMatch(randPwd,
                                                                           "\\d+[A-F]+\\d+[A-F]"));
                        //具有兩個以上的英文字母，並且中間夾數字才算合格
                        //將第二個英文字母變小寫
                        int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
                        randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

                        MD5 md5 = MD5.Create();//建立一個MD5
                                               //byte[] source = Encoding.Default.GetBytes(newGuid_Id);//將字串轉為Byte[]
                                               //byte[] crypto = md5.ComputeHash(source);//進行MD5加密
                                               //string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
                        string result = Utility.GetMd5Hash(md5, randPwd);
                        string body = "您好，您的新密碼為【" + randPwd + "】。<br>請按此<a href=\"" + ConfigurationManager.AppSettings["systemurl"] + "\">連結</a>登入系統<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";

                        //bool success =  Utility.Mail_Send(ConfigurationManager.AppSettings["MailSender"], new[] { email }, null, "峻富雲端物流管理-密碼重置通知", body, true, null);
                        bool success = Utility.Mail_Send(ConfigurationManager.AppSettings["Support_sendmail"], new[] { email }, null, "峻富雲端物流管理-密碼重置通知", body, true, null);
                        if (success)   // 寄信成功
                        {
                            SqlCommand cmdupd = new SqlCommand();
                            cmdupd.Parameters.AddWithValue("@password", result);
                            cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_id", i_id.ToString());
                            cmdupd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmdupd);
                            dbAdapter.execNonQuery(cmdupd);

                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('重置成功，密碼已寄到該帳戶的信箱。');</script>", false);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('重置失敗，請聯繫系統管理員');</script>", false);
                        }
                    }
                }

                break;
            case "Mod":
                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }

    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢示(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        int i_tmp = 0;
        lbErr.Text = "";
        lbErr.Visible = false;
        lbuserErr.Text = "";
        lbuserErr.Visible = false;
        pan_list.Visible = false;
        Boolean IsChk = true;
        lbCustomer_name.Text = "";
        ddl_user.Visible =
        div1.Visible =
        div2.Visible =
        div3.Visible =
        div4.Visible = false;
        user_name.Visible = true;
        string wherestr = "";


        switch (type)
        {
            case 1:
                #region 新增                 
                switch (rbmanager_type.SelectedValue)
                {
                    case "1"://峻富總公司
                    case "2"://峻富一般員工

                        div1.Visible = true;
                        account1.Text = "999-";
                        if (SetJunFudriver())
                        {
                            ddl_user.Visible = true;
                            ddl_user.Enabled = true;
                            user_name.Visible = false;
                        }
                        else
                        {
                            IsChk = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('目前無可新增人員，重新確認!');", true);
                        }
                        break;
                    case "3":
                        div2.Visible = true;
                        account2.Text = "000";
                        SqlCommand cmd1 = new SqlCommand();
                        cmd1.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
                                               FROM tbCustomers where supplier_code ='000' and supplier_id <>'0000'
                                            )
                                            SELECT supplier_id, supplier_id + '-' +  customer_shortname as showname
                                            FROM cus
                                            WHERE rn = 1 ");

                        dlmaster2.DataSource = dbAdapter.getDataTable(cmd1);
                        dlmaster2.DataValueField = "supplier_id";
                        dlmaster2.DataTextField = "showname";
                        dlmaster2.DataBind();
                        dlmaster2.Items.Insert(0, new ListItem("請選擇", ""));
                        dlmaster2_SelectedIndexChanged(null, null);
                        dlmaster2.Enabled = true;
                        break;
                    case "4":
                        div3.Visible = true;
                        account3.Text = "0000";

                        SqlCommand cmd2 = new SqlCommand();
                        if (sssupplier_code != "")
                        {
                            cmd2.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                            wherestr = " and supplier_code = @supplier_code";
                        }
                        cmd2.CommandText = string.Format(@"select distinct supplier_id, supplier_code, id_no, uniform_numbers, supplier_name as  showname  from tbSuppliers where active_flag = '1'  order by supplier_id ", wherestr);
                        dlmaster3.DataSource = dbAdapter.getDataTable(cmd2);
                        //dlmaster3.DataValueField = "supplier_code";
                        dlmaster3.DataTextField = "showname" ;
                        dlmaster3.DataBind();
                        dlmaster3.Items.Insert(0, new ListItem("請選擇", ""));
                        dlmaster3_SelectedIndexChanged(null, null);
                        dlmaster3.Enabled = true;
                        break;
                    case "5":
                        div4.Visible = true;
                        SqlCommand cmd3 = new SqlCommand();
                        if (sssupplier_code != "")
                        {
                            cmd3.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                            wherestr = " and supplier_code = @supplier_code";
                        }
                        cmd3.CommandText = string.Format(@"select distinct supplier_code , supplier_code + '-' +  supplier_name as  showname  from tbSuppliers  where supplier_code <>'000' {0}  and supplier_code <> '' order by supplier_code ", wherestr);
                        head_code.DataSource = dbAdapter.getDataTable(cmd3);
                        head_code.DataValueField = "supplier_code";
                        head_code.DataTextField = "showname";
                        head_code.DataBind();
                        head_code.Items.Insert(0, new ListItem("請選擇", ""));
                        head_code_SelectedIndexChanged(null, null);
                        body_code_SelectedIndexChanged(null, null);
                        head_code.Enabled = true;
                        body_code.Enabled = true;
                        break;
                        //div4.Visible = true;
                        //account4.Text = "0000";

                        //SqlCommand cmd3 = new SqlCommand();
                        //if (sssupplier_code != "")
                        //{
                        //    cmd3.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                        //    wherestr = " and supplier_code = @supplier_code";
                        //}
                        //cmd3.CommandText = string.Format(@"select distinct supplier_id,supplier_code, id_no, uniform_numbers, supplier_name as  showname  from tbSuppliers where active_flag = '1'  order by supplier_id ", wherestr);
                        //dlmaster4.DataSource = dbAdapter.getDataTable(cmd3);
                        ////dlmaster3.DataValueField = "supplier_code";
                        //dlmaster4.DataTextField = "showname";
                        //dlmaster4.DataBind();
                        //dlmaster4.Items.Insert(0, new ListItem("請選擇", ""));
                        //dlmaster4_SelectedIndexChanged(null, null);
                        //dlmaster4.Enabled = true;
                        //break;

                        //head_code.DataValueField = "supplier_code";
                        //head_code.DataTextField = "showname";
                        //head_code.DataBind();
                        //head_code.Items.Insert(0, new ListItem("請選擇", ""));
                        //head_code_SelectedIndexChanged(null, null);
                        //body_code_SelectedIndexChanged(null, null);
                        //head_code.Enabled = true;
                        //body_code.Enabled = true;
                        //break;
                }
                rbmanager_type.Enabled = true;
                pan_list.Visible = !IsChk;
                pan_add.Visible = IsChk;
                #endregion

                break;
            case 2:
                #region 編輯
               

                if (id > 0)
                {                    
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @" select account_code, active_flag,head_code,body_code,master_code,manager_type,manager_unit,job_title,emp_code,user_name,user_email 
                                           from tbAccounts where account_id = @account_id";
                        cmd.Parameters.AddWithValue("@account_id", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                rbmanager_type.SelectedValue = row["manager_type"].ToString();
                                rbmanager_type_SelectedIndexChanged(null, null);
                                
                                switch (rbmanager_type.SelectedValue)
                                {
                                    case "1"://峻富總公司
                                    case "2"://峻富一般員工

                                        div1.Visible = true;
                                        account1.Text = row["account_code"].ToString();
                                        ddl_user.Visible = true;
                                        ddl_user.Items.Insert(0, new ListItem(row["user_name"].ToString() + "("+ row["emp_code"].ToString() + ")", row["emp_code"].ToString()));
                                        ddl_user.Enabled = false;
                                        user_name.Visible = false;
                                        
                                        break;
                                    case "3":
                                        div2.Visible = true;
                                        account2.Text = "000";
                                        SqlCommand cmd1 = new SqlCommand();
                                        cmd1.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
                                               FROM tbCustomers where supplier_code ='000' and supplier_id <>'0000'
                                            )
                                            SELECT supplier_id, supplier_id + '-' +  customer_shortname as showname
                                            FROM cus
                                            WHERE rn = 1 ");

                                        dlmaster2.DataSource = dbAdapter.getDataTable(cmd1);
                                        dlmaster2.DataValueField = "supplier_id";
                                        dlmaster2.DataTextField = "showname";
                                        dlmaster2.DataBind();
                                        //dlmaster2.Items.Insert(0, new ListItem("請選擇", ""));
                                        dlmaster2.SelectedValue = row["body_code"].ToString();
                                        dlmaster2_SelectedIndexChanged(null, null);
                                        dlmaster2.Enabled = false;
                                        break;
                                    case "4":
                                        div3.Visible = true;
                                        account3.Text = "0000";

                                        SqlCommand cmd2 = new SqlCommand();
                                        if (sssupplier_code != "")
                                        {
                                            cmd2.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                                            wherestr = " and supplier_code = @supplier_code";
                                        }
                                        cmd2.CommandText = string.Format(@"select distinct supplier_id,supplier_code, id_no, uniform_numbers, supplier_name as showname from tbSuppliers where active_flag = '1'  order by supplier_id  ", wherestr);
                                        dlmaster3.DataSource = dbAdapter.getDataTable(cmd2);
                                       // dlmaster3.DataValueField = "supplier_code";
                                        dlmaster3.DataTextField = "showname ";
                                        dlmaster3.DataBind();
                                        dlmaster3.Items.Insert(0, new ListItem("請選擇", ""));
                                        dlmaster3.SelectedValue = row["head_code"].ToString();
                                        account3.Text = row["body_code"].ToString();
                                        dlmaster3_SelectedIndexChanged(null, null);
                                        dlmaster3.Enabled = false;
                                        break;
                                    case "5":
                                        div4.Visible = true;
                                        SqlCommand cmd3 = new SqlCommand();
                                        if (sssupplier_code != "")
                                        {
                                            cmd3.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                                            wherestr = " and supplier_code = @supplier_code";
                                        }
                                        cmd3.CommandText = string.Format(@"select distinct supplier_code , supplier_code + '-' +  supplier_name as  showname  from tbSuppliers  where supplier_code <>'000' and supplier_code <> ''  {0}   order by supplier_code ", wherestr);
                                        head_code.DataSource = dbAdapter.getDataTable(cmd3);
                                        head_code.DataValueField = "supplier_code";
                                        head_code.DataTextField = "showname";
                                        head_code.DataBind();
                                        //head_code.Items.Insert(0, new ListItem("請選擇", ""));
                                        head_code.SelectedValue = row["head_code"].ToString();
                                        head_code_SelectedIndexChanged(null, null);
                                        body_code.SelectedValue = row["body_code"].ToString();
                                        body_code_SelectedIndexChanged(null, null);
                                        head_code.Enabled = false;
                                        body_code.Enabled = false;
                                        break;
                                        //div4.Visible = true;
                                        //SqlCommand cmd3 = new SqlCommand();
                                        //if (sssupplier_code != "")
                                        //{
                                        //    cmd3.Parameters.AddWithValue("@supplier_code", sssupplier_code);
                                        //    wherestr = " and supplier_code = @supplier_code";
                                        //}
                                        //cmd3.CommandText = string.Format(@"select distinct supplier_id,supplier_code, id_no, uniform_numbers, supplier_name as  showname  from tbSuppliers where active_flag = '1'  order by supplier_id  ", wherestr);
                                        //dlmaster4.DataSource = dbAdapter.getDataTable(cmd3);
                                        //// dlmaster3.DataValueField = "supplier_code";
                                        //dlmaster4.DataTextField = "showname";
                                        //dlmaster4.DataBind();
                                        //dlmaster4.Items.Insert(0, new ListItem("請選擇", ""));
                                        ////dlmaster4.SelectedValue = row["head_code"].ToString();
                                        ////account4.Text = row["body_code"].ToString();
                                        //dlmaster4_SelectedIndexChanged(null, null);
                                        //dlmaster4.Enabled = false;
                                        //head_code.DataSource = dbAdapter.getDataTable(cmd3);
                                        ////head_code.DataValueField = "supplier_code";
                                        //head_code.DataTextField = "showname";
                                        //head_code.DataBind();
                                        ////head_code.Items.Insert(0, new ListItem("請選擇", ""));
                                        //head_code.SelectedValue = row["head_code"].ToString();
                                        //head_code_SelectedIndexChanged(null, null);
                                        //body_code.SelectedValue = row["body_code"].ToString();
                                        //body_code_SelectedIndexChanged(null, null);
                                        //head_code.Enabled = false ;
                                        //body_code.Enabled = false;
                                        //break;
                                }

                                job_title.Text = row["job_title"].ToString();
                                email.Text = row["user_email"].ToString();
                                user_name.Text = row["user_name"].ToString();
                                rb_Active.SelectedValue = Convert.ToInt16(row["active_flag"]).ToString();

                                ssAccount_id = id;

                                
                            }
                        }
                    }
                   
                }
                rbmanager_type.Enabled = false;
                pan_list.Visible = !IsChk;
                pan_add.Visible = IsChk;
                #endregion

                break;
            default:
                pan_add.Visible = false;
                btn_Add.Visible =
                btn_Search.Visible =
                pan_list.Visible = true;
                readdata();
                break;
        }

    }


    /// <summary>
    /// 完整的寄信函數
    /// </summary>
    /// <param name="MailFrom">寄信人Email Address</param>
    /// <param name="MailTos">收信人Email Address</param>
    /// <param name="Ccs">副本Email Address</param>
    /// <param name="MailSub">主旨</param>
    /// <param name="MailBody">內文</param>
    /// <param name="isBodyHtml">是否為Html格式</param>
    /// <param name="files">要夾帶的附檔</param>
    /// <returns>回傳寄信是否成功(true:成功,false:失敗)</returns>
    public static bool Mail_Send2(string MailFrom, string[] MailTos, string[] Ccs, string MailSub, string MailBody, bool isBodyHtml, Dictionary<string, Stream> files)
    {

        try
        {

            //沒給寄信人mail address
            if (string.IsNullOrEmpty(MailFrom))
            {//※有些公司的Smtp Server會規定寄信人的Domain Name須是該Smtp Server的Domain Name，例如底下的 system.com.tw
                MailFrom = ConfigurationManager.AppSettings["MailSender"];
            }

            //命名空間： System.Web.Mail已過時，http://msdn.microsoft.com/zh-tw/library/system.web.mail.mailmessage(v=vs.80).aspx
            //建立MailMessage物件
            MailMessage mms = new MailMessage();
            //指定一位寄信人MailAddress
            //mms.From = new MailAddress(MailFrom);
            //信件主旨
            mms.Subject = MailSub;
            //信件內容
            mms.Body = MailBody;
            //信件內容 是否採用Html格式
            mms.IsBodyHtml = isBodyHtml;

            if (MailTos != null)//防呆
            {
                for (int i = 0; i < MailTos.Length; i++)
                {
                    //加入信件的收信人(們)address
                    if (!string.IsNullOrEmpty(MailTos[i].Trim()))
                    {
                        mms.From = new MailAddress(MailTos[i].Trim());
                        mms.To.Add(new MailAddress(MailTos[i].Trim()));
                    }

                }
            }//End if (MailTos !=null)//防呆

            if (Ccs != null) //防呆
            {
                for (int i = 0; i < Ccs.Length; i++)
                {
                    if (!string.IsNullOrEmpty(Ccs[i].Trim()))
                    {
                        //加入信件的副本(們)address
                        mms.CC.Add(new MailAddress(Ccs[i].Trim()));
                    }

                }
            }//End if (Ccs!=null) //防呆


            //附件處理
            if (files != null && files.Count > 0)//寄信時有夾帶附檔
            {
                foreach (string fileName in files.Keys)
                {
                    Attachment attfile = new Attachment(files[fileName], fileName);
                    mms.Attachments.Add(attfile);
                }//end foreach
            }//end if 

            //using (SmtpClient client = new SmtpClient(smtpServer, smtpPort))//或公司、客戶的smtp_server
            //{
            //    if (!string.IsNullOrEmpty(mailAccount) && !string.IsNullOrEmpty(mailPwd))//.config有帳密的話
            //    {//寄信要不要帳密？眾說紛紜Orz，分享一下經驗談....

            //        //網友阿尼尼:http://www.dotblogs.com.tw/kkc123/archive/2012/06/26/73076.aspx
            //        //※公司內部不用認證,寄到外部信箱要特別認證 Account & Password

            //        //自家公司MIS:
            //        //※要看smtp server的設定呀~

            //        //結論...
            //        //※程式在客戶那邊執行的話，問客戶，程式在自家公司執行的話，問自家公司MIS，最準確XD
            //        client.Credentials = new NetworkCredential(mailAccount, mailPwd);//寄信帳密
            //    }
            //    client.Send(mms);//寄出一封信
            //}//end using 

            using (SmtpClient client = new SmtpClient())//或公司、客戶的smtp_server
            {
                client.Send(mms);//寄出一封信
            }//end using 



            //釋放每個附件，才不會Lock住
            if (mms.Attachments != null && mms.Attachments.Count > 0)
            {
                for (int i = 0; i < mms.Attachments.Count; i++)
                {
                    mms.Attachments[i].Dispose();

                }
            }

            return true;//成功
        }
        catch (Exception ex)
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
            string strErr = string.Empty;
            strErr = "寄信失敗-" + ": " + ex.ToString();


            //寄信失敗，寫Log文字檔
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
            return false;
        }
    }//End 寄信
  

}

