﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreport_1_2.aspx.cs" Inherits="LTreport_1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />   
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
    
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

       
        
        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">發送總表</h2>


            <div class="templatemo-login-form" >            
                <div class="form-group form-inline">

                    <label>發送日期：</label>
                    <asp:TextBox ID="date1" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>~
                    <asp:TextBox ID="date2" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                    
                    <label>部門：</label>
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack ="true"  OnSelectedIndexChanged ="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                    <label>選擇客代：</label>
                    <asp:DropDownList ID="second_code" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                    <asp:DropDownList ID="issendcontact" runat="server" CssClass="form-control chosen-select" ></asp:DropDownList>
                    <label>類型：</label>
                    <asp:DropDownList ID="dlRType" runat="server" CssClass="form-control " >
                        <asp:ListItem Value="R0">全部</asp:ListItem>
                        <asp:ListItem Value="R1">來回件</asp:ListItem>
                        <asp:ListItem Value="R2">退貨</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group form-inline">
                    <asp:Button ID="btnQry" CssClass="btn btn-primary btn_view" runat="server" Text="查　詢" OnClick="btnQry_Click"  />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="匯　出" OnClick="btExport_Click"  />
                </div>
            </div>
             <div style="overflow: auto; height: 550px; width: 100%">
                <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <%--<th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">來源單號</th>
                    <th style="white-space: nowrap;">發送日期</th>
                    <th style="white-space: nowrap;">配送日期</th>
                    <th style="white-space: nowrap;">明細貨號</th>
                    <th style="white-space: nowrap;">訂單編號</th>
                    <th style="white-space: nowrap;">發送站代碼</th>
                    <th style="white-space: nowrap;">發送站</th>
                    <th style="white-space: nowrap;">寄件人</th>
                    <th style="white-space: nowrap;">收件人</th>
                    <th style="white-space: nowrap;">收貨人電話號碼<br /></th>
                    <th style="white-space: nowrap;">退回縣市</th>
                    <th style="white-space: nowrap;">退回鄉鎮</th>
                    <th style="white-space: nowrap;">件數</th>
                    <th style="white-space: nowrap;">退回地址</th>
                    <th style="white-space: nowrap;">取件縣市區</th>
                    <th style="white-space: nowrap;">取件地址</th>    
                    <th style="white-space: nowrap;">到著站代碼</th>
                    <th style="white-space: nowrap;">到著站</th>
                    <th style="white-space: nowrap;">特殊配送說明</th>
                    <th style="white-space: nowrap;">回單</th>
                    <th style="white-space: nowrap;">逆物流類型</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">入帳日</th>
                    <th style="white-space: nowrap;">作業站</th>
                    <th style="white-space: nowrap;">最新貨況</th>
                    <th style="white-space: nowrap;">掃描日</th>
                    <th style="white-space: nowrap;">掃描時間</th>
                    <th style="white-space: nowrap;">取件狀態</th>--%>
                    <th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">發送日期</th>
                    <th style="white-space: nowrap;">取件日期</th> <%--th 是橫向欄位標題名稱--%>
                    <th style="white-space: nowrap;">配送日期</th>
                    <th style="white-space: nowrap;">明細貨號</th>
                    <th style="white-space: nowrap;">訂單編號</th>
                    <th style="white-space: nowrap;">發送站</th>
                    <th style="white-space: nowrap;">寄件人</th>
                    <th style="white-space: nowrap;">收件人</th>
                    <th style="white-space: nowrap;">收貨人電話號碼<br /></th>
                    <th style="white-space: nowrap;">配送<br>縣市</th>
                    <th style="white-space: nowrap;">配送<br>區域</th>
                    <th style="white-space: nowrap;">重量</th>
                    <th style="white-space: nowrap;">件數</th>
                    <th style="white-space: nowrap;">收件人地址</th>
                    <th style="white-space: nowrap;">站所<br>代碼</th>
                    <th style="white-space: nowrap;">站所<br>名稱</th>
                    <th style="white-space: nowrap;">傳票<br>區分</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">備註</th>
                    <th style="white-space: nowrap;">作業站</th>
                    <th style="white-space: nowrap;">作業人員</th>
                    <th style="white-space: nowrap;">貨態狀況</th>
                    <th style="white-space: nowrap;">貨態情形</th>
                    <%--<th style="white-space: nowrap;">集貨異常情形</th>--%>
                    <th style="white-space: nowrap;">掃讀日期</th>
                    <th style="white-space: nowrap;">類型區分</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server" >
                    <ItemTemplate>
                        <tr class ="paginate">
                            <%--<td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="來源單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.來源單號").ToString())%></td>
                            <td style="white-space: nowrap;"  data-th="發送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送日期").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="明細貨號">
                                <a href="LT_trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%></a>
                            </td>
                            <td style="white-space: nowrap;" data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單編號").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站代碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收貨人電話號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收貨人電話號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送縣市別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.退回縣市").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.退回區域").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="退回地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.退回地址").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="取件縣市區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.取件縣市區").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="取件地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.取件地址").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到著站代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所代碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所名稱").ToString())%></td>
                            <td class="_left" style="min-width :200px" data-th="特殊配送說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.特殊配送說明").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="回單"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.回單").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="逆物流類型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.逆物流類型").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="入帳日"></td>
                            <td style="white-space: nowrap;" data-th="作業站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.作業站").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="最新貨況"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.最新貨況").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="掃描日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.掃描日").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="掃描時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.掃描時間").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="取件狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.取件狀態").ToString())%></td>--%>
                             <td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;"  data-th="發送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <%--td是th欄位下的資料，data-th可以實現響應式--%>
                            <td style="white-space: nowrap;" data-th="出貨日期">
                                 <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.取件日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送日期").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="明細貨號">
                                <a href="LT_trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%></a>
                            </td>
                            <td style="white-space: nowrap;" data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單編號").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收貨人電話號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收貨人電話號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送縣市別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送縣市別").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送區域").ToString())%></td>
                            <td style="white-space: nowrap;" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.重量").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                             <td class="_left" style=" min-width :200px" data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人地址").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="站所代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所代碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="站所名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.站所名稱").ToString())%></td> 
                            <td style="white-space: nowrap;" data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.傳票區分").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.備註").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業站"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.作業站").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業人員"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.作業人員").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="貨態狀況"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨態狀況").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="貨態情形"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨態情形").ToString())%></td>
                            <%--<td style="white-space: nowrap;" data-th="集貨異常情形"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.集貨異常情形").ToString())%></td>--%>
                            <td style="white-space: nowrap;" data-th="掃讀日期"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.掃讀日期").ToString())%></td>
                             <td style="white-space: nowrap;" data-th="類型區分"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.類型區分").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="24" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            </div>

           <%-- <div style="overflow: auto; height: 550px; width: 100%">
                <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th style="white-space: nowrap;">序號</th>
                    <th style="white-space: nowrap;">發送日期</th>
                    <th style="white-space: nowrap;">配送日期</th>
                    <th style="white-space: nowrap;">明細貨號</th>
                    <th style="white-space: nowrap;">訂單編號</th>
                    <th style="white-space: nowrap;">發送站</th>
                    <th style="white-space: nowrap;">寄件人</th>
                    <th style="white-space: nowrap;">收件人</th>
                    <th style="white-space: nowrap;">收貨人電話號碼<br /></th>
                    <th style="white-space: nowrap;">配送縣市別</th>
                    <th style="white-space: nowrap;">配送區域</th>
                    <th style="white-space: nowrap;">系統才數</th>
                    <th style="white-space: nowrap;">丈量才數</th>
                    <th style="white-space: nowrap;">重量</th>
                    <th style="white-space: nowrap;">件數</th>
                    <th style="white-space: nowrap;">棧板數</th>
                    <th style="white-space: nowrap;">才</th>    
                    <th style="white-space: nowrap;">寄件人地址</th>
                    <th style="white-space: nowrap;">收件人地址</th>
                    <th style="white-space: nowrap;">配送商代碼</th>
                    <th style="white-space: nowrap;">配送廠商</th>
                    <th style="white-space: nowrap;">特殊配送說明</th>
                    <th style="white-space: nowrap;">回單張數</th>
                    <th style="white-space: nowrap;">棧板回收</th>
                    <th style="white-space: nowrap;">翻板</th>
                    <th style="white-space: nowrap;">上樓</th>
                    <th style="white-space: nowrap;">困配</th>
                    <th style="white-space: nowrap;">貨件運費</th>
                    <th style="white-space: nowrap;">請款客代</th>
                    <th style="white-space: nowrap;">請款統編</th>
                    <th style="white-space: nowrap;">計價模式</th>
                    <th style="white-space: nowrap;">月結／現收</th>
                    <th style="white-space: nowrap;">傳票區分</th>
                    <th style="white-space: nowrap;">元付運費</th>
                    <th style="white-space: nowrap;">到付追加(未稅)</th>
                    <th style="white-space: nowrap;">到付追加(含稅)</th>
                    <th style="white-space: nowrap;">到付追加</th>
                    <th style="white-space: nowrap;">到付運費</th>
                    <th style="white-space: nowrap;">代收金額</th>
                    <th style="white-space: nowrap;">入帳日</th>
                    <th style="white-space: nowrap;">配達狀況</th>
                    <th style="white-space: nowrap;">配達時間</th>
                    <th style="white-space: nowrap;">配送異常說明</th>
                    <th style="white-space: nowrap;">是否已掃簽單</th>
                    <th style="white-space: nowrap;">入帳金額</th>
                    <th style="white-space: nowrap;">匯款日</th>
                    <th style="white-space: nowrap;">帳項異常說明</th>
                    <th style="white-space: nowrap;">配送費用</th>
                    <th style="white-space: nowrap;">發送區</th>
                    <th style="white-space: nowrap;">每板費用</th>
                    <th style="white-space: nowrap;">偏遠區</th>
                    <th style="white-space: nowrap;">總費用</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server"  OnItemDataBound="New_List_ItemDataBound">
                    <ItemTemplate>
                        <tr class ="paginate">
                            <td data-th="序號">
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                            </td>
                            <td style="white-space: nowrap;"  data-th="發送日期" >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%>
                            </td>
                            <td style="white-space: nowrap;" data-th="配送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送日期").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="明細貨號">
                                <a href="trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.明細貨號").ToString())%></a>
                            </td>
                            <td style="white-space: nowrap;" data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單編號").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="收貨人電話號碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收貨人電話號碼").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送縣市別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送縣市別").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="配送區域"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送區域").ToString())%></td>
                            <td style="white-space: nowrap;" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.系統才數").ToString())%></td>
                            <td style="white-space: nowrap;" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.丈量才數").ToString())%></td>
                            <td style="white-space: nowrap;" ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.重量").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="棧板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.棧板數").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="才"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.才數").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="寄件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.寄件件人地址").ToString())%></td>
                            <td class="_left" style=" min-width :200px" data-th="收件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人地址").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送商代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送商代碼").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送廠商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送廠商").ToString())%></td>
                            <td class="_left" style="min-width :200px" data-th="特殊配送說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.特殊配送說明").ToString())%></td>                            
                            <td style="white-space: nowrap;" data-th="回單張數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.是否回單").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="棧板回收"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.棧板回收").ToString())%></td>    
                            <td style="white-space: nowrap;" data-th="翻板"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.翻板").ToString())%></td>    
                            <td style="white-space: nowrap;" data-th="上樓"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.上樓").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="困配"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.困配").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="貨件運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件運費").ToString())%></td>
                            <td style="white-space: nowrap; text-align:left" data-th="請款客代"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.請款客代").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="請款統編"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.請款統編").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.計價模式").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="月結／現收"></td>
                            <td style="white-space: nowrap;" data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.傳票區分").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="元付運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.元付運費").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付追加(未稅)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付追加未稅").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付追加(含稅)"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付追加含稅","{0:#0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付追加"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付追加").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="到付運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付運費").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="入帳日"></td>
                            <td style="white-space: nowrap;" data-th="配達狀況"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配達狀況").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配達時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配達時間","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送異常說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送異常說明").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="是否已掃簽單"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.是否已掃簽單").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="入帳金額"></td>
                            <td style="white-space: nowrap;" data-th="匯款日"></td>
                            <td style="white-space: nowrap;" data-th="帳項異常說明"></td>
                            <td style="white-space: nowrap;" data-th="配送費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送費用").ToString())%></td>
                            <td class="_left" style="white-space: nowrap;" data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="每板費用"><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.每板費用").ToString()) == "-1" ? "採獨立計價" : Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.每板費用").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="偏遠區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.偏遠區").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="總費用"> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.總費用").ToString())%></td>
                            
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="41" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            </div>--%>
            
             共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
              <div id="page-nav" class="page"></div>
                        <%--<nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>--%>
        </div>
    </div>
</asp:Content>

