﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMember.master" AutoEventWireup="true" CodeFile="member_4.aspx.cs" Inherits="member_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        ._th {
            text-align: center;
        }

        .td_no {
            width: auto;
        }

        .td_customer_hcode {
            width: 20%;
        }

        .td_customer_code {
            width: 15%;
        }

        .td_shipments_city {
            width: 30%;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            $("._btn_customer_hcode").click(function () {
                var the_parent = $(this).parent('div.form-group');
                var _val = the_parent.find("._inp_customer_hcode").val();
                var _cus = the_parent.find("._cid").val();
                var re = /^[0-9]{11}$/.test(_val);
                if (re & _val.length == 11 & _cus != "") {
                    var ischk = confirm("確定修改竹運客代資訊? ");
                    if (ischk) {
                        return ischk;
                    }
                } else {
                    alert("請確認竹運客代是否正確!");
                    return false;
                }

            });


            $("._btn_hcode_mod2").click(function () {
                var the_parent = $(this).parent('div.form-group');
                the_parent.find("span,._btn_hcode_mod").hide();
                the_parent.find("._mod").show();
            });

            $('.fancybox').fancybox();
            $("#addclick").fancybox({
                'width': 980,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });            
            $.fancybox.update();

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">竹運客代管理</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">

                    <asp:TextBox ID="txcustomer_hcode" class="form-control" runat="server" placeholder="輸入竹運客代"></asp:TextBox>
                    OR                    
                    <asp:TextBox ID="txcustomer_code" class="form-control" runat="server" placeholder="輸入峻富客代"></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <asp:Button ID="btExport" CssClass="templatemo-blue-button" runat="server" Text="下　載" OnClick="btExport_Click" />
                    <%--<asp:Button ID="btAdd" CssClass="templatemo-blue-button" runat="server" Text="新　增" OnClick="btAdd_Click" />--%>
                    <a href="member_4_edit.aspx" class="fancybox fancybox.iframe " id="addclick"><span class="templatemo-blue-button">新　增</span></a>

                </div>
                <hr>
                <p class="text-primary">※點選任一客代可查詢及修改</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th">No.</th>
                        <th class="_th">竹運客代</th>
                        <th class="_th">客戶代號</th>
                        <th class="_th">負責區配商</th>
                        <th class="_th">客戶名稱</th>
                        <th class="_th">出貨地址</th>
                        <th class="_th">合約到期日</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td data-th="No." class="td_no"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.no").ToString())%></td>
                                <td data-th="竹運客代" class="td_customer_hcode">
                                    <div class="form-group form-inline">
                                        <asp:TextBox runat="server" ID="txt_customer_hcode" placeholder="請輸入竹運客代" CssClass="_inp_customer_hcode form-control" MaxLength="11" ReadOnly='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_hcode").ToString()) != "" %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_hcode").ToString())%>'></asp:TextBox>
                                        <asp:Button runat="server" ID="btnMob" Text="修 改" CommandName="Mod" CssClass="btn _btn_hcode_mod templatemo-white-button" Visible='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_hcode").ToString()) != "" %>' />

                                        <asp:Button runat="server" ID="btnSet" CommandName="Set" Text="確 認" CssClass="btn _btn_customer_hcode btn-primary" Visible='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_hcode").ToString()) == "" %>' />
                                        <input type="text" class="_cid" value="<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_id").ToString())%>" readonly="readonly" style="display: none; visibility: hidden;" />
                                        <asp:HiddenField ID="cid" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_id").ToString())%>' />

                                    </div>
                                </td>
                                <td data-th="客戶代號" class="td_customer_code">
                                    <a href="member_3-edit.aspx?src=member_4.aspx&customer_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_id").ToString())%>" class="fancybox fancybox.iframe btn btn-link " id="updclick2"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString()))%></a>
                                </td>
                                <td data-th="負責區配商" class="td_supplier_code"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_code").ToString())%> <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                <td data-th="客戶名稱" class="td_customer_shortname"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_shortname").ToString())%></td>
                                <td data-th="出貨地址" class="td_shipments_city"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shipments_road").ToString())%></td>
                                <td data-th="合約到期日" class="td_contract_expired_date"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.contract_expired_date","{0:yyyy/MM/dd}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="7" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <p class="text-primary">註：【001－零擔】【002－流通】</p>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span> 筆
                </div>


            </div>
        </div>
    </div>
</asp:Content>

