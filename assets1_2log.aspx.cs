﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class assets1_2log : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lbcheck_number.Text = Request.QueryString["check_number"].ToString();
            car_retailer.Text = Func.GetRow("code_name", "tbItemCodes", "", "code_id = @code_id and code_bclass = '6' AND code_sclass = 'CD'", "code_id", Func.GetRow("car_retailer", "ttAssets", "", "car_license = @car_license", "car_license", lbcheck_number.Text));
            owner.Text = Func.GetRow("owner", "ttAssets", "", "car_license = @car_license", "car_license", lbcheck_number.Text);
            driver.Text = Func.GetRow("driver", "ttAssets", "", "car_license = @car_license", "car_license", lbcheck_number.Text);
            string Type = Request.QueryString["Type"]!= null ? Request.QueryString["Type"].ToString() :"";
            readdata();
        }
    }

    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = "order by id desc";
            cmd.Parameters.Clear();
            cmd.CommandText = string.Format(@"Select * from ttAssetsDeptChange With(Nolock) where 0 = 0 and car_license  = @car_license {0} ", strWhereCmd);
            cmd.Parameters.AddWithValue("@car_license", lbcheck_number.Text);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }           

    }

    
}