﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
public partial class trace_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            #region 配送站所
            using (SqlCommand cmd1 = new SqlCommand())
            {
                manager_type = Session["manager_type"].ToString();        //管理單位類別
                string account_code = Session["account_code"].ToString(); //使用者帳號
                string supplier_code = Session["master_code"].ToString(); //客戶代碼前7碼
                Master_code = Session["master_code"].ToString();
                Suppliers.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
                Suppliers.DataValueField = "supplier_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();

                if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
                Suppliers_SelectedIndexChanged(null, null);
                if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4") Suppliers.Items.Insert(0, new ListItem("全部", ""));

            }
            #endregion

            
            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            }

            if (Request.QueryString["Customer"] != null)
            {
                Customer.SelectedValue = Request.QueryString["Customer"];
            }
            
            if (Request.QueryString["Driver"] != null)
            {
                Driver.SelectedValue = Request.QueryString["Driver"];
            }

            if (Request.QueryString["stime"] != null)
            {
                stime.SelectedValue = Request.QueryString["stime"];
            }

            if (Request.QueryString["etime"] != null)
            {
                etime.SelectedValue = Request.QueryString["etime"];
            }

            string print_date = DateTime.Today.ToString("yyyy/MM/dd");
            if (Request.QueryString["ReportType"] != null)
            {
                rbReportType.SelectedValue = Request.QueryString["ReportType"];
            }

            if (Request.QueryString["sdate"] != null)
            {
                sdate.Text = Request.QueryString["sdate"];
            }
            else
            {   
                sdate.Text = print_date;
            }

            if (Request.QueryString["edate"] != null)
            {
                edate.Text = Request.QueryString["edate"];
            }
            else
            {  
                edate.Text = print_date;
            }


            readdata();
        }
    }

    private void readdata()
    {

        using (SqlCommand cmd = new SqlCommand())
        {
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            cmd.Parameters.Clear();

            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            if (sdate.Text != "")
            {
                cmd.Parameters.AddWithValue("@sdate", sdate.Text);
                querystring += "&sdate=" + sdate.Text;
            }

            if (edate.Text != "")
            {
                cmd.Parameters.AddWithValue("@edate", edate.Text);
                querystring += "&edate=" + edate.Text;
            }

            cmd.Parameters.AddWithValue("@Supplier_code", Suppliers.SelectedValue);
            querystring += "&Suppliers=" + Suppliers.SelectedValue;

            if (Customer.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@Customer", Suppliers.SelectedValue + Customer.SelectedValue);
            }
            querystring += "&Customer=" + Customer.SelectedValue;

            cmd.Parameters.AddWithValue("@Driver", Driver.SelectedValue);
            querystring += "&Driver=" + Driver.SelectedValue;

            cmd.Parameters.AddWithValue("@stime", stime.SelectedValue);
            querystring += "&stime=" + stime.SelectedValue;

            cmd.Parameters.AddWithValue("@etime", etime.SelectedValue);
            querystring += "&etime=" + etime.SelectedValue;

            cmd.Parameters.AddWithValue("@Type", rbReportType.SelectedValue);
            querystring += "&ReportType=" + rbReportType.SelectedValue;
            

            #endregion
            cmd.CommandText = string.Format(@"DECLARE  @NO NVARCHAR(4)
                                                DECLARE  @driver_code NVARCHAR(10)
                                                DECLARE  @driver_name NVARCHAR(10)
                                                DECLARE  @CHKNUM NVARCHAR(20)  --貨號
                                                DECLARE  @send_contact NVARCHAR 
                                                DECLARE  @receive_contact NVARCHAR 
                                                DECLARE  @product_category  NVARCHAR 
                                                DECLARE  @product_category_name  NVARCHAR 
                                                DECLARE  @quant  NVARCHAR(5)
                                                DECLARE  @print_date NVARCHAR(20)
                                                DECLARE  @supplier_date  NVARCHAR(20)
                                                DECLARE  @arrive_date  NVARCHAR(20)
                                                DECLARE  @arrive_option NVARCHAR(10) 

                                                select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
                                                '          ' as driver_code ,
                                                '                    ' as driver_name ,
                                                A.check_number,
                                                A.send_contact,
                                                A.receive_contact, 
                                                A.product_category ,
                                                D.code_name as product_category_name,
                                                CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
                                                LEFT(CONVERT(varchar, A.print_date, 23),7) as print_date,
                                                '                    ' as supplier_date, 
                                                '                    ' as arrive_date,
                                                '          ' as arrive_option
                                                into #mylist	  
                                                from tcDeliveryRequests  A with(nolock)
                                                left join tbItemCodes D with(nolock) on D.code_id = A.product_category and D.code_bclass = '2' and D.code_sclass = 'S3'
                                                where  A.check_number <> ''
                                                and (CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@sdate,111) and  CONVERT(VARCHAR,A.print_date,111) <=CONVERT(VARCHAR,@edate,111))
                                                and A.supplier_code LIKE '%'+  ISNULL(@Supplier_code,'') +'%'
                                                and A.customer_code LIKE ''+  ISNULL(@Customer,'') +'%'
                                                and A.Less_than_truckload = @Less_than_truckload
                                                order by A.check_number 

                                                DECLARE R_cursor CURSOR FOR
                                                select * FROM #mylist
                                                OPEN R_cursor
                                                FETCH NEXT FROM R_cursor INTO @NO,@driver_code, @driver_name, @CHKNUM,@send_contact ,@receive_contact ,@product_category , @product_category_name,@quant ,@print_date,@supplier_date,@arrive_date,@arrive_option
                                                WHILE (@@FETCH_STATUS = 0) 
	                                                BEGIN
		                                                select top 1 @driver_code=B.driver_code  , @driver_name = C.driver_name , @supplier_date = Convert(varchar,B.scan_date,120) from ttDeliveryScanLog B with (nolock)  
		                                                left join tbDrivers C with(nolock) on B.driver_code = C.driver_code
		                                                where check_number = @CHKNUM and scan_item = '2'  
                                                        order by scan_date desc
	
		                                                select top 1 @arrive_date =Convert(varchar,B.scan_date,120) , @arrive_option= isnull(AO.code_name,'') from ttDeliveryScanLog B with (nolock)  
		                                                left join tbItemCodes AO with(nolock) on B.arrive_option = AO.code_id and AO.code_sclass = 'AO'
		                                                where check_number = @CHKNUM and scan_item = '3'  
                                                        order by scan_date desc
		
		                                                UPDATE #mylist
				                                                SET driver_code  =@driver_code , 
				                                                driver_name =@driver_name, 
				                                                supplier_date = @supplier_date, 
				                                                arrive_date=@arrive_date ,
				                                                arrive_option= @arrive_option
		                                                WHERE check_number = @CHKNUM
					
	                                                FETCH NEXT FROM R_cursor INTO @NO,@driver_code, @driver_name, @CHKNUM,@send_contact ,@receive_contact ,@product_category , @product_category_name,@quant,@print_date,@supplier_date,@arrive_date,@arrive_option
	                                                END
                                                CLOSE R_cursor
                                                DEALLOCATE R_cursor

                                                IF (@TYPE =1)  --未配達/異常配達明細
                                                BEGIN 
	                                                select * from #mylist  WHERE 
                                                (arrive_date IS NULL or arrive_date= '')
												                          
                                                END ELSE BEGIN 
                                                select * from #mylist WHERE driver_code LIKE '%'+  ISNULL(@Driver,'') +'%' 
                                                            AND  ((supplier_date IS NULL) OR (CONVERT(VARCHAR,supplier_date,108)   >=CONVERT(VARCHAR,@stime,111)  and CONVERT(VARCHAR,supplier_date,108)   <=CONVERT(VARCHAR,@etime,111) ))
                                                            OR ((arrive_date IS NULL) OR (CONVERT(VARCHAR,arrive_date,108)   >=CONVERT(VARCHAR,@stime,111)  and CONVERT(VARCHAR,arrive_date,108)   <=CONVERT(VARCHAR,@etime,111) ))
                                                END                                                
                                                IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
                                                DROP TABLE #mylist");
                                                           

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource objPds = new PagedDataSource();
                objPds.DataSource = dt.DefaultView;
                objPds.AllowPaging = true;
                objPds.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                objPds.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!objPds.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!objPds.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (objPds.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
                }

                #endregion

                New_List.DataSource = objPds;
                New_List.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();
            }

        }

    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            //0: 系統管理者  1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營
            Customer.DataSource = Utility.getCustomerDT (Suppliers.SelectedValue,( manager_type == "3" || manager_type == "5") ? Master_code :"" );
            Customer.DataValueField = "supplier_id";
            Customer.DataTextField = "showname";
            Customer.DataBind();
        }

        if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
        {
            //Suppliers.Items.Insert(0, new ListItem("全部", ""));
            //員工
            if (Suppliers.SelectedValue != "")
            {
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    cmd1.CommandText = string.Format(@"select driver_code, driver_name from tbDrivers where active_flag  = 1 and supplier_code = @supplier_code order by driver_code");
                    Driver.DataSource = dbAdapter.getDataTable(cmd1);
                    Driver.DataValueField = "driver_code";
                    Driver.DataTextField = "driver_name";
                    Driver.DataBind();
                    Driver.Items.Insert(0, new ListItem("全選", ""));
                }
            }
        }
        else
        {
            Driver.Items.Insert(0, new ListItem("全選", ""));
        }
        
        
        #endregion
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (Suppliers.SelectedValue != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }
        querystring += "&Customer=" + Customer.SelectedValue;

        if (Driver.SelectedValue.ToString() != "")
        {
            querystring += "&Driver=" + Driver.SelectedValue.ToString();
        }
        if (stime.SelectedValue != "")
        {
            querystring += "&stime=" + stime.SelectedValue.ToString();
        }

        if (etime.SelectedValue != "")
        {
            querystring += "&etime=" + etime.SelectedValue.ToString();
        }

        if (rbReportType.SelectedValue != "")
        {
            querystring += "&ReportType=" + rbReportType.SelectedValue.ToString();
        }

        if (sdate.Text != "")
        {
            querystring += "&sdate=" + sdate.Text;
        }

        if (edate.Text != "")
        {
            querystring += "&edate=" + edate.Text;
        }

        Response.Redirect(ResolveUrl("~/trace_2.aspx?search=yes" + querystring));
    }
}