﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_10-edit.aspx.cs" Inherits="system_10_edit" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();

        });
        $.fancybox.update();


        $(function () {
            $("#<%=chkpri.ClientID%>").click(function () {
                if ($("#<%=chkpri.ClientID%>").prop("checked")) {
                    $("#<%=contact.ClientID%>").val($("#<%=pri_name.ClientID%>").val());
                } else {
                    $("#<%=contact.ClientID%>").val("");
                }
            });

            $("#<%=chkname.ClientID%>").click(function () {
                if ($("#<%=chkname.ClientID%>").prop("checked")) {
                    $("#<%=sup_name.ClientID%>").val($("#<%=shortname.ClientID%>").val());
                } else {
                    $("#<%=sup_name.ClientID%>").val("");
                }
            });

            $("#<%=chknumber.ClientID%>").click(function () {
                if ($("#<%=chknumber.ClientID%>").prop("checked")) {
                    $("#<%=receipt_number.ClientID%>").val($("#<%=uni_number.ClientID%>").val());
                } else {
                    $("#<%=receipt_number.ClientID%>").val("");
                }
            });

            $("#datepicker").datepicker();

            //$("#send_upload").mouseover(function () {
            //    var input_data = "";
            //    input_data = input_data + '<input type="hidden" name="member_id" value="001">';
            //    input_data = input_data + '<input type="hidden" name="member_name" value="toreoo">';
            //    $('<form action="WebForm2.aspx" method="POST">' + input_data + '</form>').appendTo('body').submit();
            //});
        });


    </script>
    <style type="text/css">
        .hide {
            display: none;
        }

        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .span_tip {
            margin-left: 15px;
            color: red;
        }

        .col-lg-12 {
            width: 50%;
        }

        .tb_price_log {
            width: 40%;
            margin: 10px 0px;
            border: 1px solid #ddd;
        }

            .tb_price_log th {
                background-color: #39ADB4;
                color: white;
                text-align: center;
                padding: 5px;
            }

            .tb_price_log td {
                text-align: center;
                padding: 5px;
            }

        .bk_color1 {
            background-color: #f9f9f9;
        }

        .tb_price_th {
            width: 15%;
        }

        .p_date {
            width: 27%;
        }

        .p_type {
            width: 23%;
        }

        .p_set {
            width: 35%;
        }

        .p_td_null {
            text-align: center;
            padding: 10px;
        }

        ._price_today {
            color: blue;
            font-weight: bold;
        }

        ._ship_title {
            font-size: 20px;
            margin-right: 30px;
        }

        ._cus_title {
            width: 13%;
        }

        ._cus_data {
            color: #8a9092;
        }

        .cus_main {
            height: 720px;
            overflow-y: auto;
            padding: 5px;
        }

        .row {
            margin: 0px;
        }

        .btn {
            margin: 0px 5px;
        }

        ._btn_area {
            margin-top: 10px;
        }

        .div_price_log {
            max-height: 210px;
            overflow-y: auto;
        }

        .tb_price_log tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .scrollbar::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper" class="cus_main scrollbar">
        <div class="row">
            <h2 class="margin-bottom-10">區配商資料資料-<asp:Literal ID="statustext" runat="server"></asp:Literal></h2>
            <div class="templatemo-login-form">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                <label for="inputFirstName" class="_cus_title">區配編號</label>
                                <asp:TextBox ID="supplier_code" CssClass="form-control"  runat="server" MaxLength="3" onkeyup="this.value=this.value.replace(^[a-zA-Z]\d{2}$,'')"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="區配編號格式有誤" ValidationExpression="(^[a-zA-Z]\d{2}$)"
                                    ControlToValidate="supplier_code" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">供應商編號</label>
                                <asp:TextBox ID="supplier_no" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="供應商編號"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="supplier_no" ForeColor="Red" ValidationGroup="validate">請輸入供應商編號</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">


                            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                <label for="inputFirstName" class="_cus_title">統一編號</label>
                                <asp:TextBox ID="uni_number" CssClass="form-control" runat="server" MaxLength="8" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="ex: 12345678"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="reg_uni_number" runat="server" ControlToValidate="uni_number" ForeColor="Red" ValidationGroup="validate">請輸入統一編號</asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="reg_uni_number_type" runat="server" ErrorMessage="統一編號格式有誤" ValidationExpression="(^\d{8}$)"
                                    ControlToValidate="uni_number" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">身分證字號</label>
                                <asp:TextBox ID="id_no" CssClass="form-control" runat="server" MaxLength="10" onkeyup="this.value=this.value.replace(^[a-zA-Z]\d{9}$,'')" placeholder="ex: A123456789"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="身分證字號格式有誤" ValidationExpression="(^[a-zA-Z]\d{9}$)"
                                    ControlToValidate="id_no" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">名　　稱</label>
                                <asp:TextBox ID="shortname" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="請輸入名稱"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator3" runat="server" ControlToValidate="shortname" ForeColor="Red" ValidationGroup="validate">請輸入名稱</asp:RequiredFieldValidator>

                            </div>
                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">負 責 人</label>
                                <asp:TextBox ID="pri_name" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="請輸入負責人"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator5" runat="server" ControlToValidate="pri_name" ForeColor="Red" ValidationGroup="validate">請輸入負責人</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">聯 絡 人</label>
                                <asp:TextBox ID="contact" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="請輸入聯絡人"></asp:TextBox>
                                <asp:CheckBox ID="chkpri" CssClass=" checkbox checkbox-success" Text='同負責人' runat="server" />

                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">連絡電話</label>
                                <asp:TextBox ID="tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="15" placeholder="ex:0910XXXXXX"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator6" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">請輸入連絡電話</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-12 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">E-mail</label>
                                <asp:TextBox ID="email" CssClass="form-control" runat="server" MaxLength="40" placeholder="ex: aaa@gmail.com"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    ControlToValidate="email" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <label for="inputFirstName" class="_cus_title _tip_important">通訊地址</label>
                                        <asp:DropDownList ID="City" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="CityArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:TextBox ID="address" CssClass="form-control" runat="server" placeholder="請輸入通訊地址"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator11" runat="server" ControlToValidate="City" ForeColor="Red" ValidationGroup="validate">請選擇通訊縣市</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator12" runat="server" ControlToValidate="CityArea" ForeColor="Red" ValidationGroup="validate">請選擇通訊鄉鎮區</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator7" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請輸入通訊地址</asp:RequiredFieldValidator>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <hr />
                        <div class="row">

                            <div class="col-lg-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">發票抬頭</label>
                                <asp:TextBox ID="sup_name" runat="server" CssClass="cus_code1" MaxLength="10" placeholder="請輸入發票抬頭"></asp:TextBox>
                                <asp:CheckBox ID="chkname" CssClass=" checkbox checkbox-success" Text='同名稱' runat="server" />
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="sup_name" ForeColor="Red" ValidationGroup="validate">請輸入發票抬頭</asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline" >
                          <label for="inputFirstName" class="_cus_title _tip_important">發票類型</label>
                            <asp:DropDownList ID="option" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="option_SelectedIndexChanged">
                                <asp:ListItem Value="0">請選擇</asp:ListItem>
                                <asp:ListItem Value="1">二聯式</asp:ListItem>
                                <asp:ListItem Value="2">三聯式</asp:ListItem>
                            </asp:DropDownList>
                                </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                           
                                <label for="inputFirstName" class="_cus_title _tip_important">發票統一編號</label>
                                <asp:TextBox ID="receipt_number"  CssClass="form-control" runat="server" Length="8" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="ex: 12345678"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="統一編號錯誤" ValidationExpression="(^[a-zA-Z]\d{2}$)"
                                ControlToValidate="supplier_code" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                                <asp:CheckBox ID="chknumber" CssClass=" checkbox checkbox-success" Text='同統一編號' runat="server" />
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="receipt_number" ForeColor="Red" ValidationGroup="validate">請輸入發票統一編號</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="發票統一編號格式有誤" ValidationExpression="(^\d{8}$)"
                                    ControlToValidate="receipt_number" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 form-group form-inline">
                                <label for="bank_name" class="_cus_title _tip_important">銀行/分行</label>
                                <asp:TextBox ID="bank_name" runat="server" CssClass="cus_code1" MaxLength="20"></asp:TextBox>
                            </div>

                            <div class="col-lg-12 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <label for="inputFirstName" class="_cus_title _tip_important">發票寄送地址</label>
                                        <asp:DropDownList ID="city_receipt" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CityRec_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:DropDownList ID="cityarea_receipt" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                        <asp:TextBox ID="address_receipt" CssClass="form-control" runat="server" placeholder="請輸入發票地址"></asp:TextBox>
                                        <asp:CheckBox ID="chkaddress" CssClass=" checkbox checkbox-success" Text='同通訊地址' runat="server" AutoPostBack="True" OnCheckedChanged="Check_Clicked" />
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator10" runat="server" ControlToValidate="city_receipt" ForeColor="Red" ValidationGroup="validate">請選擇發票寄送縣市</asp:RequiredFieldValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator9" runat="server" ControlToValidate="cityarea_receipt" ForeColor="Red" ValidationGroup="validate">請選擇發票寄送鄉鎮區</asp:RequiredFieldValidator>

                                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator8" runat="server" ControlToValidate="address_receipt" ForeColor="Red" ValidationGroup="validate">請輸入發票地址</asp:RequiredFieldValidator>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <label for="bank_code" class="_cus_title _tip_important">收款銀行代號</label>
                                <asp:TextBox ID="bank_code" runat="server" CssClass="cus_code1" MaxLength="3" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="60px"></asp:TextBox>
                                <label for="account_no" class="_cus_title _tip_important">銀行帳戶</label>
                                <asp:TextBox ID="account_no" runat="server" CssClass="cus_code1" MaxLength="30" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="ex: 12345678" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator4" runat="server" ControlToValidate="account_no" ForeColor="Red" ValidationGroup="validate">請輸入銀行帳戶</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">身分證影像</label>
                                <div style="display: none">
                                    <asp:TextBox ID="filename1" runat="server" />
                                </div>
                                <a href="#" id="h1FileView" class="fa fa-file-text" aria-hidden="true">身分證影像</a>
                                <a href="fileupload.aspx?filename=<%=filename1.ClientID %>&fileview=h1FileView" class="fancybox fancybox.iframe" id="h1FileAdd"><span class="btn btn-primary">上傳新身分證</span></a>
                            </div>

                            <div class="col-lg-12 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">駕照影像</label>
                                <div style="display: none">
                                    <asp:TextBox ID="filename2" runat="server" />
                                </div>
                                <a href="#" id="h2FileView" class="fa fa-file-text" aria-hidden="true">駕照影像</a>
                                <a href="fileupload.aspx?filename=<%=filename2.ClientID %>&fileview=h2FileView" class="fancybox fancybox.iframe" id="h2FileAdd"><span class="btn btn-primary">上傳新駕照</span></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">存摺影像</label>
                                <div style="display: none">
                                    <asp:TextBox ID="filename3" runat="server" />
                                </div>
                                <a href="#" id="h3FileView" class="fa fa-file-text" aria-hidden="true">存摺影像</a>
                                <a href="fileupload.aspx?filename=<%=filename3.ClientID %>&fileview=h3FileView" class="fancybox fancybox.iframe" id="h3FileAdd"><span class="btn btn-primary">上傳新存摺</span></a>
                            </div>

                            <div class="col-lg-12 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title">合約內容</label>
                                <div style="display: none">
                                    <asp:TextBox ID="filename4" runat="server" />
                                </div>
                                <a href="#" id="h4FileView" class="fa fa-file-text" aria-hidden="true">合約內容</a>
                                <a href="fileupload.aspx?filename=<%=filename4.ClientID %>&fileview=h4FileView" class="fancybox fancybox.iframe" id="h4FileAdd"><span class="btn btn-primary">上傳新合約</span></a>
                            </div>
                        </div>

                        <hr />

                        <div class="form-group text-center _btn_area">
                            <asp:Button ID="btnsave" CssClass="templatemo-blue-button btn" runat="server" Text="確　認" ValidationGroup="validate" OnClick="btnsave_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="system_10.aspx" class="templatemo-white-button btn"><span>取　消</span></a>
                            <asp:Label ID="lb_susid" CssClass="_cus_id hide" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div>
        <h3>運價表設定</h3>
        <asp:Button ID="btnViewFreight" runat="server" Text="檢視目前運價" OnClick="OpenViewWindow" />
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload_freight">上傳新運價</button>
    </div>

    <div class="modal fade" id="upload_freight" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">上傳運價表</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <asp:TextBox ID="datepicker" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:FileUpload ID="FileUpload1" CssClass="button" runat="server" />

                    <asp:Label runat="server" ID="fileTypeWrong" class="text-danger"></asp:Label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    <asp:Button ID="btnImport" CssClass="button" runat="server" Text="匯入檔案" OnClick="ImportCSV" />
                </div>
            </div>
        </div>
    </div>

    <hr />
    <table class="table">
        <asp:Repeater runat="server" ID="update_log_repeater">
            <HeaderTemplate>
                <tr>
                    <th>No</th>
                    <th>生效日期</th>
                    <th>更新人員</th>
                    <th>更新時間</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Container.ItemIndex + 1 %></td>
                    <td><%# Eval("effective_date") %></td>
                    <td><%# Eval("create_user") %></td>
                    <td><%# Eval("create_date") %></td>
                </tr>
            </ItemTemplate>


        </asp:Repeater>
    </table>

</asp:Content>


