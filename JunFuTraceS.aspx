﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="JunFuTraceS.aspx.cs" Inherits="JunFuTraceS" %>


<!DOCTYPE html>

<html lang="zh">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>峻富雲端物流管理-貨物追踨</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="" />
    <meta name="author" content="templatemo" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <!-- jQuery文件 -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    
    <style>

        .hrtitle {
            color:rgb(51,146,192) !important ;
            font-weight:bold
        }

        .tdtitle{
            background-color:rgb(51,146,192);
            font-weight:bold;
            color:white;
        }
        .tdtime{
              width:250px;
        }
        
   

       
        .button_nor{
            background-color:rgb(51,146,192);
            padding: 8px;
           
            border: 0;
            color: white;
        }

        .button {
 
            cursor: pointer;
            transition: all 0.3s;
                }

        .button:focus {
            outline: 0;
        }

        .button_em {
            padding: 8px 18px 8px 18px;
            border: 0;
            border-radius: 5px;
            color: #FFFFFF;
            font-weight:bold;
       
            box-shadow: inset 0 0 0 4.5px rgba(255, 255, 255, 0.65);
}
        .button_em:hover {
            box-shadow: inset 0 0 0 4.5px rgba(255, 255, 255, 0.25);
        }
    
        .button_arrow:hover {
            padding-left: 1.9em;
            padding-right: 1.9em;
        }
        .table_shadow{
            -moz-box-shadow: 10px 10px 5px #CCCCCC;
            box-shadow: 10px 10px 5px #CCCCCC; 
        }
        .tdborder{
            border:2px solid #ddd;
        }
        .padd8{
            padding:8px !important
        }
        .wi100{
            width:100% !important 
        }
        .odd{ background-color:White;}
        .even{ background-color:Yellow;}
        .bordernon{
            border:none !important 
        }
        .search {
            margin-left:10px;
        }

        input[type="radio"] { display:inline;}
    </style>


</head>
<body>
    <form id="form1" runat="server">
        <div class="">
            <div class="templatemo-content-widget white-bg">
               <h2 class="margin-bottom-10 hrtitle">貨號查詢</h2>
                <div class="templatemo-login-form">
                    <div class="row form-group">
                        <div class="col-lg-12 col-md-12 form-group form-inline" >
                          
                            
                      
                          
                            <label for="check_number"> <asp:RadioButton id="rb1" Text="" GroupName="rbd"  RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged" Checked="true"/> 貨　號</label>
                            <asp:TextBox ID="check_number" runat="server" MaxLength="20"  CssClass="form-control"></asp:TextBox>
                          

                             <label for="inputLastName" style="display:none">次貨號</label>

                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton id="rb2" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged"/>
                              <asp:Label ID="Label4" runat="server" Text="訂單編號"></asp:Label>
                <asp:TextBox ID="order_number" runat="server" MaxLength="30" Enabled="false"></asp:TextBox>

                            <asp:TextBox ID="sub_chknum" runat="server" Visible="false">000</asp:TextBox>
                            <%--<button type="submit" class="btn btn-darkblue">影像查詢</button>--%>
                                   <%-- <label for="inputLastName">驗證碼</label>


                           
                            <asp:TextBox ID="xcode" runat="server" MaxLength="5" CssClass="form-control" placeholder="請輸入驗證碼"></asp:TextBox>
                            
                             <asp:Image ID="Image2" runat="server" ImageUrl="~/CheckImageCode.aspx"></asp:Image>--%>

                            <asp:Button ID="search" CssClass="button_em button_nor button_arrow button search" runat="server" Text="查 詢" ValidationGroup="validate" OnClick="search_Click" />
                        </div>
                    </div>

                    <div style="width:100%;padding:2px 8px 2px 0px ;text-align:right;font-weight:bold;font-size:16px"><asp:Label ID="Label1" runat="server"></asp:Label></div>


                    <hr>
                   <%-- <table class="table table-striped table-bordered templatemo-user-table">
                        <tr class="tr-only-hide">
                            <th>發送站</th>
                            <th>到著站</th>
                            <th>發送日</th>
                            <th>指配日期</th>
                            <th>指配時間</th>
                            <th>訂單編號</th>
                            <th>件數</th>
                            <th>傳票區分</th>
                            <th>商品種類</th>
                            <th>請款金額(元付)</th>
                            <th>到付追加</th>
                            <th>代收金額</th>
                        </tr>
                        <asp:Repeater ID="New_List_01" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                                    <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.showname").ToString())%></td>
                                    <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                                    <td data-th="指配日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                                    <td data-th="指配時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.time_period").ToString())%></td>
                                    <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                                    <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                                    <td data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_name").ToString())%></td>
                                    <td data-th="商品種類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.product_category_name").ToString())%></td>
                                    <td data-th="請款金額">*****</td>
                                    <td data-th="到付追加">*****</td>
                                    <td data-th="代收金額">*****</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List_01.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="12" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>--%>



<%--                    <table class="table table-striped table-bordered templatemo-user-table">
                        <thead>
                            <tr>
                                <td>寄件人資料</td>
                                <td>收件人資料</td>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:Label ID="lbSend" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lbReceiver" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>--%>



                    <div id="divMaster" runat="server" class="templatemo-login-form panel panel-default wi100">
                        <table class="table table-striped table-bordered table_shadow">
                            <tr class="tr-only-hide">
                          
                                <th class="tdtitle tdtime tdborder padd8">作業時間</th>
                                <th class="tdtitle tdtrace tdborder padd8">貨物追蹤</th>
                  
                                <%--<th class="auto-style1">影像查詢</th>--%>
                            </tr>

                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </table>
                    </div>
                    <div style="overflow: auto; height: 400px; vertical-align: top;font-weight:bold">
                         FSE貨物追蹤資訊僅提供您追蹤交由FSE配送貨件的資訊，未經授權請勿作為他用。
                    </div>                      
                </div>
            </div>
        </div>
    </form>
</body>
</html>
