﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_1.aspx.cs" Inherits="system_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script src="js/jquery-1.11.2.min.js"></script>
    <style type="text/css">
        .radio label {
            margin-right: 15px;
            text-align :left ;
        }
        .td_th {
            text-align: center;
        }

        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding:5px;
        }

        input[type=radio] {
            display: inline-block;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }
        .table_list tr:hover {
            background-color: lightyellow;
        }
        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
          .radio label {
            margin-right: 15px;
            text-align :left ;
        }    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="templatemo-content-widget white-bg">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="人員基本資料" runat="server"></asp:Label></h2>



        <asp:Panel ID="pan_view" runat="server">
            <div class="div_view">
                <!-- 查詢 -->
                <div class="form-group form-inline">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 70%;">
                                <label>查詢條件：</label>
                                <asp:DropDownList ID="ddl_supplier" runat="server" CssClass="form-control" ToolTip="所屬區配商"></asp:DropDownList>
                                <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control" ToolTip="所屬站所"></asp:DropDownList>
                                <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 員工代碼、姓名"></asp:TextBox>
                                <asp:DropDownList ID="ddl_job_type" runat="server" CssClass="form-control" ToolTip="職屬歸類"></asp:DropDownList>
                                <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control" ToolTip="是否生效"></asp:DropDownList>
                                <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                                <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1" />
                            </td>
                            <td style="width: 30%; float: right;">
                                <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- 查詢 end -->
                <!--內容-list -->
                <p class="text-primary">※總公司可設定各區配商APP使用帳號數，由各區配依帳號數自行管理司機APP登入姓名</p>
                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr>
                        <th class="td_th td_no">No</th>
                        <th class="td_th td_empcode">員工編號</th>
                        <th class="td_th td_name">姓名</th>
                        <th class="td_th td_job">職屬歸類</th>
                        <th class="td_th td_app">APP帳號</th>
                        <th class="td_th td_yn">是否生效</th>
                        <th class="td_th">更新人員</th>
                        <th class="td_th">更新日期</th>
                        <th class="td_th td_edit">功　　能</th>
                    </tr>
                    <asp:Repeater ID="list_customer" runat="server" OnItemCommand="list_customer_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                                <td class="td_data td_empcode" data-th="員工編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.emp_code").ToString())%></td>
                                <td class="td_data td_name" data-th="姓名"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.emp_name").ToString())%></td>
                                <td class="td_data td_job" data-th="職屬歸類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.job").ToString())%></td>
                                <td class="td_data td_app" data-th="APP帳號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                                <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.IsActive").ToString())%></td>
                                <td class="td_data " data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td class="td_data " data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td class="td_data td_edit" data-th="編輯">
                                    <asp:HiddenField ID="emp_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.emp_id").ToString())%>' />
                                    <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_customer.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="10" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總筆數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
                <!--內容-end -->
            </div>
        </asp:Panel>

        <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
            <hr />
            <table class="tb_edit">
                <tr class="form-group">
                    <th class="_edit_title">
                        <label class="_tip_important">員工編號</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:DropDownList ID="ddl_customer_code" runat="server" CssClass="form-control" ToolTip="所屬區配商" Width="48%"></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_customer_code" runat="server" ControlToValidate="emp_code" ForeColor="Red" ValidationGroup="validate">請選擇所屬區配商</asp:RequiredFieldValidator>

                        <asp:TextBox ID="emp_code" runat="server" class="form-control" placeholder="ex: 13000" MaxLength="20" Width="50%"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_emp_code" runat="server" ControlToValidate="emp_code" ForeColor="Red" ValidationGroup="validate">請輸入員工編號</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reg_emp_code_type" runat="server" ErrorMessage="員工編號格式有誤" ValidationExpression="(^[A-Za-z0-9]{3,20}$)"
                            ControlToValidate="emp_code" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                        </asp:RegularExpressionValidator>
                    </td>
                    <th class="_edit_title">
                        <label class="_tip_important">員工姓名</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:TextBox ID="emp_name" runat="server" class="form-control" placeholder="ex: 王小明" MaxLength="20" OnTextChanged="emp_name_TextChanged"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_emp_name" runat="server" ControlToValidate="emp_name" ForeColor="Red" ValidationGroup="validate">請輸入員工姓名</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label class="_tip_important">職屬歸類</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_job_edit" runat="server" CssClass="form-control" ToolTip="請選擇職屬歸類!" OnSelectedIndexChanged="ddl_job_edit_OnChanged" Autopostback="true"></asp:DropDownList>
                    </td>
                    <th>
                        <label>APP帳號</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:Label ID="driver_code" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label class="_tip_important">是否生效</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:RadioButtonList ID="rb_Active" runat="server" ToolTip="請設定是否生效!" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px">
                            <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                            <asp:ListItem Value="0">否</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <th>
                        <asp:Label ID="lbstation" runat="server" Text="所屬站所　" Visible="false"></asp:Label></th>
                    <td>
                        <asp:DropDownList ID="ddl_staion" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList></td>
                </tr>
                <tr>
                    <th>
                        <asp:Label ID="lbSDMDTitle" runat="server" Text="集區SD/MD" Visible="false" Width="150px"></asp:Label>
                    </th>
                    <td class="SD_MD">
                        <asp:RadioButtonList ID="RadioButtonListSDMD" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px;" OnSelectedIndexChanged="RadioSDMD" AutoPostBack="true">
                            <asp:ListItem Value="0">SD</asp:ListItem>
                            <asp:ListItem Value="1">MD</asp:ListItem>
                        </asp:RadioButtonList>
                    <asp:DropDownList ID="SDMDList" runat="server" CssClass="form-control" Visible="false" Width="100px"></asp:DropDownList>
                    </td>
                </tr>



                <tr>
                    <td colspan="4" style="text-align: center;">
                        <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                        <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>
</asp:Content>

