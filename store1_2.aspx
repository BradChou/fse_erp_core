﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_2.aspx.cs" Inherits="store1_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <style>
        .ui-datepicker-calendar {
    display: none;
    }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".btn-primary").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });



        });
        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }
        <%--function Edit(str,num) {
            $("#<%=code_id.ClientID%>").val(num);
            $("#<%=code_name.ClientID%>").val(str);
            $("#<%=btnsave.ClientID%>").val("修 改");
            $("#<%=btnsave.ClientID%>").prop("class","btn btn-info");
            $("#<%=code_name.ClientID%>").focus();
        }--%>

    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }

      
        .radio label {
            margin-right: 15px;
            text-align :left ;
        }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="場區維護" runat="server"></asp:Label>
        </h2>
        <hr />
        <div  >
            <div class="row">
                <div class="col-lg-6 form-group form-inline">
                    <label class="">管理單位：</label>
                    <asp:Label ID="lbitem" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">人　　員：</label>
                    <asp:Label ID="lbitem_employee" runat="server"></asp:Label>
                </div>                
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">帳　　號：</label>
                    <asp:Label ID="lbaccount_code" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">名　　稱：</label>
                    <asp:Label ID="lbaccount_name" runat="server"></asp:Label>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 form-group form-inline text-right">                  
                                     
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline text-right">                  
                    <a href="store1_2-edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>                    
                </div>
                </div>
            
            <hr />
            <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide"> 
                        <th class="_th" style="width:10%"></th>
                        <th class="_th">場區名稱</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">      
                                <td>
                                   <a href="store1_2-edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>" class="btn btn-info  " id="updclick">修改</a>
                                    <%--<a href="#" class="btn btn-info" id="updclick" onclick="Edit('<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%>',<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.seq").ToString())%>)">修改</a>--%>
                                </td>                         
                                <td style="text-align:left; width:60%"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="16" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
            共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>

        </div>

        
        
            </div>
    </div>
</asp:Content>

