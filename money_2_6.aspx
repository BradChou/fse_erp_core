﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_2_6.aspx.cs" Inherits="money_2_6" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        ._th {
            text-align: center;
        }
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">超商應付</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label id="lbdaterange" runat="server" class="_daterange" >請款期間</label>
                            <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate" autocomplete="off"></asp:TextBox>
                            ~ 
                            <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate" autocomplete="off"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <label for="Suppliers">供應商</label>
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                            <asp:DropDownList ID="dlclose_type" CssClass="form-control" runat="server" OnSelectedIndexChanged ="dlclose_type_SelectedIndexChanged" AutoPostBack ="true" >
                                <asp:ListItem Value="1">1. 已出帳</asp:ListItem>
                                <asp:ListItem Value="2">2. 未出帳</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="帳單查詢" OnClick="btQry_Click" />
                            <asp:Button ID="btClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');" />
                            <asp:Button ID="btExport" runat="server" class="templatemo-white-button " Text="匯出總表" OnClick="btExport_Click" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Suppliers" EventName="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btExport" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label id="lbCloseDay" runat ="server"  class="_close" >結帳日</label>
                            <asp:DropDownList ID="dlCloseDay" runat="server" CssClass="form-control _close"></asp:DropDownList>
                            <%--<button type="submit" class="btn btn-primary _close">重寄帳單</button>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <hr>
                

                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <p class="text-primary">
                            供應商：<asp:Label ID="lbSuppliers" runat="server"></asp:Label><br>
                            請款期間：<asp:Label ID="lbdate" runat="server"></asp:Label>
                        </p>
                        <asp:Panel ID="Panel1" runat="server">
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th">序號</th>
                                    <th class="_th">日期</th>
                                    <th class="_th">日/夜</th>
                                    <th class="_th">溫層</th>
                                    <th class="_th">車老闆</th>
                                    <th class="_th">運務士</th>
                                    <th class="_th">車號</th>
                                    <th class="_th">車型</th>
                                    <th class="_th">主線</th>
                                    <th class="_th">爆量</th>
                                    <th class="_th">店到店</th>
                                    <th class="_th">回頭車</th>
                                    <th class="_th">加派車</th>
                                </tr>
                                <asp:Repeater ID="New_List_01" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="序號">
                                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                            </td>
                                            <td data-th="日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.InDate","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="日/夜"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DayNight").ToString())%></td>
                                            <td data-th="溫層"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Temperature_Text").ToString())%></td>
                                            <td data-th="車老闆"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.PaymentObject").ToString())%></td>
                                            <td data-th="運務士"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                            <td data-th="車號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                            <td data-th="車型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cabin_type_text").ToString())%></td>
                                            <td data-th="主線"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses_01","{0:N0}").ToString())%></td>
                                            <td data-th="爆量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses_02","{0:N0}").ToString())%></td>
                                            <td data-th="店到店"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses_03","{0:N0}").ToString())%></td>
                                            <td data-th="回頭車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses_04","{0:N0}").ToString())%></td>
                                            <td data-th="加派車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses_05","{0:N0}").ToString())%></td>

                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List_01.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="13" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                            <table class="paytable">
                                <tr>
                                    <th>小計：</th>
                                    <td>NT$ 
                            <asp:Label ID="subtotal_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>5%營業稅：</th>
                                    <td>NT$  
                            <asp:Label ID="tax_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>應收帳款：</th>
                                    <td>NT$   
                            <asp:Label ID="total_01" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                            </table>

<%--
                            <table class="table table-striped table-bordered templatemo-user-table" style="width: 50%">
                                <tr class="tr-only-hide">
                                    <th>場區</th>
                                    <th>實收運費</th>
                                    <th>稅額</th>
                                    <th>應收帳款</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="場區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                            <td data-th="實收運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subtotal","{0:N0}").ToString())%></td>
                                            <td data-th="稅額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax","{0:N0}").ToString())%></td>
                                            <td data-th="應收帳款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total","{0:N0}").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>--%>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>

