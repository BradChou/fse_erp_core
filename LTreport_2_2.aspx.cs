﻿using BObject.Bobjects;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BarcodeLib;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public partial class LTreport_2_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            SetDefault();
            
            if (Request.QueryString["dateS"] != null)
            {
                SetViewData();
            }
        }
    }

    /// <summary>始初資料綁定 </summary>
    protected void SetDefault()
    {
        string strSQL = string.Empty;


        //區配商

        manager_type = Session["manager_type"].ToString(); //管理單位類別   
        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                supplier_code = "";
                //一般員工/管理者
                //有綁定站所只該該站所客戶
                //無則可查所有站所下的所有客戶
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        supplier_code = dt.Rows[0]["station_code"].ToString();
                    }
                    else
                    {

                    }
                }
                break;
            case "4":                 //站所主管只能看該站所下的客戶
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        supplier_code = dt.Rows[0]["station_code"].ToString();
                    }
                }
                break;
            case "5":
                supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                break;
        }
        string wherestr = "";


        if (supplier_code != "")
        {

            SqlCommand cmd1 = new SqlCommand();
            wherestr = " and station_code in ( @supplier_code)";
            cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname,
station_level,station_area,station_scode,management                                               
from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

            DataTable dt1 = dbAdapter.getDataTable(cmd1);

            var management = "";
            var station_scode = "";
            var station_area = "";
            var station_level = "";

            if (dt1 != null && dt1.Rows.Count > 0)
            {
                management = dt1.Rows[0]["management"].ToString();
                station_scode = dt1.Rows[0]["station_scode"].ToString();
                station_area = dt1.Rows[0]["station_area"].ToString();
                station_level = dt1.Rows[0]["station_level"].ToString();
            }
            var station = "";
            if (station_level == "1")
            {
                station = management;
                SqlCommand cmdstatiom = new SqlCommand();
                cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_code, station_scode + '-' +station_name as showname from tbStation where management= @station
");

                ddl_supplyer.DataSource = dbAdapter.getDataTable(cmdstatiom);
                ddl_supplyer.DataValueField = "station_code";
                ddl_supplyer.DataTextField = "showname";
                ddl_supplyer.DataBind();
            }
            else if (station_level == "2")
            {
                station = station_scode;
                SqlCommand cmdstatiom = new SqlCommand();
                cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_code, station_scode + '-' +station_name as showname from tbStation where station_scode= @station
");

                ddl_supplyer.DataSource = dbAdapter.getDataTable(cmdstatiom);
                ddl_supplyer.DataValueField = "station_code";
                ddl_supplyer.DataTextField = "showname";
                ddl_supplyer.DataBind();

            }
            //取出相同區屬站所
            else if (station_level == "4")
            {
                station = station_area;

                SqlCommand cmdstatiom = new SqlCommand();
                cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_code, station_scode + '-' +station_name as showname from tbStation where station_area= @station
");

                ddl_supplyer.DataSource = dbAdapter.getDataTable(cmdstatiom);
                ddl_supplyer.DataValueField = "station_code";
                ddl_supplyer.DataTextField = "showname";
                ddl_supplyer.DataBind();
            }

            else if (station_level == "5")
            {

                SqlCommand cmdstatiom = new SqlCommand();
                cmdstatiom.CommandText = string.Format(@"
select station_code, station_scode + '-' +station_name as showname from tbStation
");

                ddl_supplyer.DataSource = dbAdapter.getDataTable(cmdstatiom);
                ddl_supplyer.DataValueField = "station_code";
                ddl_supplyer.DataTextField = "showname";
                ddl_supplyer.DataBind();
            }

        }
        

        else
        {

            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname,
station_level,station_area,station_scode,management                                               
from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

            cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
            DataTable dt1 = dbAdapter.getDataTable(cmd1);
            ddl_supplyer.DataSource = dbAdapter.getDataTable(cmd1);
            ddl_supplyer.DataValueField = "station_code";
            ddl_supplyer.DataTextField = "showname";
            ddl_supplyer.DataBind();
        }

        //if (manager_type == "0" || manager_type == "1")
        //{
        //    ddl_supplyer.Items.Insert(0, new ListItem("全部", ""));
        //}
        //else if (supplier_code == "" && manager_type == "2")
        //{
        //    ddl_supplyer.Items.Insert(0, new ListItem("全部", ""));
        //}
        if (ddl_supplyer.Items.Count > 0) ddl_supplyer.SelectedIndex = 0;
    }

    /// <summary>始初資料 </summary>
    protected void SetViewData()
    {
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();

        #region Set QueryString

        if (Request.QueryString["supplyer"] != null
            && Request.QueryString["dateS"] != null
            && Request.QueryString["dateE"] != null)
        {
            ddl_supplyer.SelectedValue = Request.QueryString["supplyer"];

            if (DateTime.TryParse(Request.QueryString["dateS"], out dt_s)
                && DateTime.TryParse(Request.QueryString["dateE"], out dt_e)
                && dt_e >= dt_s)
            {
                sscan_date.Text = Request.QueryString["dateS"];
                escan_date.Text = Request.QueryString["dateE"];
            }
        }
        else
        {
            sscan_date.Text = Request.QueryString["dateS"];
            escan_date.Text = Request.QueryString["dateE"];
        }

        if (Request.QueryString["keyword"] != null)
        {
            tb_search.Text = Request.QueryString["keyword"].ToString();
        }

        if (Request.QueryString["strCheck_number"] != null)
        {
            strCheck_number.Text = Request.QueryString["strCheck_number"].ToString();
        }

        if (Request.QueryString["sign_paper_print_flag"] != null)
        {

           // string a = Request.QueryString["sign_paper_print_flag"].ToString();

            for (int i = 0; i < sign_paper_print_flag.Items.Count; i++)
            {
                sign_paper_print_flag.Items[i].Selected = false;
            }

            sign_paper_print_flag.Items[int.Parse(Request.QueryString["sign_paper_print_flag"])].Selected = true;

            //sign_paper_print_flag.DataValueField = Request.QueryString["sign_paper_print_flag"];
        }

        #endregion



        //if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇掃讀期間!');</script>", false);
        //    return;
        //}
        //if (dt_s > dt_e)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認掃讀期間是否正確!');</script>", false);
        //    return;
        //}

        ListData _search = new ListData();
        _search.type = 1;
        _search.arrive = ddl_supplyer.SelectedValue;
        _search.date_start = dt_s;
        _search.date_end = dt_e;
        _search.keyword = tb_search.Text;
        _search.strCheck_number = strCheck_number.Text;
        _search.rbpricingtype = "02";
        _search.sign_paper_print_flag = sign_paper_print_flag.SelectedValue;

        if (tb_search.Text != "")
        {
            _search.keyword = tb_search.Text;
        }
        else
        {
            _search.keyword = "";
        }

        
            _search.dttype = "2";
       
        //if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
        //    return;
        //}
        //if (tb_search.Text.Trim().Length == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入司機工號!');</script>", false);
        //    return;
        //}
        using (DataTable dt = GetDataTableForList(_search))
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                rowcount.Text = string.Format(@"筆數:{0}", dt.Rows.Count);
                rowcount.Visible = true;
            }
            New_List.DataSource = dt;
            New_List.DataBind();
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();
        DateTime.TryParse(sscan_date.Text, out dt_s);
        DateTime.TryParse(escan_date.Text, out dt_e);

        if (strCheck_number.Text.Length > 0)
        {}
        else
        {
            if (dt_s == DateTime.MinValue || dt_e == DateTime.MinValue)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
            if (tb_search.Text.Trim().Length == 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入司機工號!');</script>", false);
                return;
            }
        }


        String str_Query = "supplyer=" + ddl_supplyer.SelectedValue
                         + "&keyword=" + tb_search.Text
                         + "&strCheck_number=" + strCheck_number.Text;
                          
        

            str_Query += "&dateS=" + sscan_date.Text + "&dateE=" + escan_date.Text;
            str_Query += "&dttype=" + "2";
            str_Query += "&sign_paper_print_flag=" + sign_paper_print_flag.SelectedIndex.ToString();
        
        //if (sign_paper_print_flag.SelectedIndex == 1)
        //    {
        //    str_Query += "&sign_paper_print_flag= " + "1";
        //    }
        //    else
        //    {
        //    str_Query += "&sign_paper_print_flag = null ";
        //}


        //if (sign_paper_print_flag.SelectedValue == "1")
        //{
        //    str_Query += "&sign_paper_print_flag=" + "1";
        //}
        //else
        //{
        //    str_Query += "&sign_paper_print_flag != " + "1";
        //}
  

        Response.Redirect(ResolveUrl("~/LTreport_2_2.aspx?" + str_Query));
    }

    protected void btn_Print_Click(object sender, EventArgs e)
    {
        //if (tb_ckeck.Text.Trim().Length == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇欲列印的項目!');</script>", false);
        //    return;
        //}
        
        string requestIds = string.Empty;

        if (New_List.Items.Count > 0)
        {
            if (tb_ckeck.Text.Trim() != "0")
            {
                requestIds = tb_ckeck.Text;

                var r = requestIds.Split(',');
                
                foreach (var request_id in r)
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@request_id", request_id);
                        cmd.CommandText = "select request_id, check_number,sign_paper_print_flag from tcDeliveryRequests where request_id = @request_id";
                        DataTable dt = new DataTable();
                        dt = dbAdapter.getDataTable(cmd);
                        string sign_paper_print_flag = dt.Rows[0]["sign_paper_print_flag"].ToString();
                        string check_number = dt.Rows[0]["check_number"].ToString();

                        if (sign_paper_print_flag != "1")
                        {
                            //更新主表的列印狀態sign_paper_print_flag
                            using (SqlCommand cmd2 = new SqlCommand())
                            {
                                cmd2.Parameters.AddWithValue("@sign_paper_print_flag", "1");                   //是否列印
                                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                                cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);
                                dbAdapter.execNonQuery(cmd2);

                            }


                            //寫入log
                            using (SqlCommand cmd3 = new SqlCommand())
                            {
                                cmd3.Parameters.AddWithValue("@sign_paper_print_flag", "1");
                                cmd3.Parameters.AddWithValue("@request_id", request_id);
                                cmd3.Parameters.AddWithValue("@check_number", check_number);
                                cmd3.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd3.CommandText = " insert into FirstPrintDate (request_id,check_number,sign_paper_print_flag,cdate) values (@request_id,@check_number,@sign_paper_print_flag,@cdate)";
                                dbAdapter.execNonQuery(cmd3);
                            }                            
                        }
                       
                    }
                }
            }
            else
            {
                for (int i = 0; i <= New_List.Items.Count - 1; i++)
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.Parameters.AddWithValue("@request_id", id);
                        cmd.CommandText = "select request_id,  check_number, sign_paper_print_flag from tcDeliveryRequests where request_id = @request_id";
                        DataTable dt = new DataTable();
                        dt = dbAdapter.getDataTable(cmd);
                        string sign_paper_print_flag = dt.Rows[0]["sign_paper_print_flag"].ToString();
                        string check_number = dt.Rows[0]["check_number"].ToString();
                        
                        if (sign_paper_print_flag != "1")
                        {
                            //更新主表的列印狀態sign_paper_print_flag
                            using (SqlCommand cmd2 = new SqlCommand())
                            {
                                cmd2.Parameters.AddWithValue("@sign_paper_print_flag", "1");                   //是否列印
                                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);
                                dbAdapter.execNonQuery(cmd2);
                            }


                            //寫入log
                            using (SqlCommand cmd3 = new SqlCommand())
                            {
                                cmd3.Parameters.AddWithValue("@sign_paper_print_flag", "1");
                                cmd3.Parameters.AddWithValue("@request_id", id);
                                cmd3.Parameters.AddWithValue("@check_number", check_number);
                                cmd3.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd3.CommandText = " insert into FirstPrintDate (request_id,check_number,sign_paper_print_flag,cdate) values (@request_id,@check_number,@sign_paper_print_flag,@cdate)";
                                dbAdapter.execNonQuery(cmd3);
                            }
                            
                        }
                            
                    }

                    requestIds += id.ToString() + ",";

                }

                requestIds = requestIds.Substring(0,requestIds.Length - 1);

            }

            if (requestIds == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的簽收單');</script>", false);
                return;
            }
            else
            {

                Boolean isChkOk = requestIds.Length > 0 ? true : false;


                if(isChkOk)
                {

                    NameValueCollection nvcParamters = new NameValueCollection();
                    
                    nvcParamters["ids"] = requestIds;

                    string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintDeliverySignReceiptTagByCheckNumbers";

                    string strForm = PreparePOSTForm(url, nvcParamters);

                    Response.Clear();

                    Response.Write(strForm);
                    Response.End();
                }
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無要列印的簽收單');</script>", false);
            return;
        }
        //if (ddl_supplyer.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送廠商!');</script>", false);
        //    return;
        //}
        //DateTime dt_s = new DateTime();
        //DateTime dt_e = new DateTime();
        
        //    if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
        //        return;
        //    }
        //    if (dt_s > dt_e)
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
        //        return;
        //    }

        //string resule = tb_ckeck.Text;

        //ListData _search = new ListData();
        //_search.type = 2;
        //_search.arrive = ddl_supplyer.SelectedValue;
        //_search.date_start = dt_s;
        //_search.date_end = dt_e;
        //_search.strCheck_number = strCheck_number.Text;
        //_search.rbpricingtype = "02";
        //if (tb_search.Text != "")
        //{
        //    _search.keyword = tb_search.Text;
        //}
        //else
        //{
        //    _search.keyword = "";
        //}
        
        //    _search.dttype = "2";


        //if (tb_ckeck.Text.Trim() == "0") _search.check_all = true;
        //else _search.check_item = tb_ckeck.Text.Trim();

        //using (DataTable dt = GetDataTableForList(_search))
        //{
        //    try
        //    {
        //        #region way2 

        //        //範本檔路徑
        //        String strSampleFileFullName = Request.PhysicalApplicationPath + @"report\零擔簽收單.xls"; ;
        //        if (strSampleFileFullName.EndsWith(".xls") == false) strSampleFileFullName += ".xls";

        //        #region new
        //        #region 初始化參數
        //        NPOITable ttExlApp = new NPOITable(strSampleFileFullName);
        //        ttExlApp.OpenFile();
        //        ttExlApp.SRow = 1;
        //        ttExlApp.SCol = 1;
        //        ttExlApp.Line = 45;
        //        ttExlApp.Column = 24;
        //        ttExlApp.NextPage();
        //        ttExlApp.GoToLine(1);

        //        DateTime dt_tmp = new DateTime();
        //        string print_dateYY = "";
        //        string print_date_mm = "";
        //        string print_date_DD = "";
        //        string check_number = "";
        //        string check_number_detail = "";
        //        string send_contact = "";
        //        string send_tel = "";

        //        string order_number = "";
        //        string ArticleName = "";

        //        string send_addr = "";

        //        //發送站
        //        string on_station_code = "";
        //        //到著站
        //        string off_station_code = "";

        //        string receive_contact = "";
        //        string receive_tel1 = "";
        //        string receive_addr = "";
        //        string sendsite = "";
        //        string supplier_name = "";
        //        string ticket = "";
        //        string pricing_type = "";
        //        string str_unit = "";
        //        int pieces = 0;//件
        //        int plates = 0;//板
        //        int cbm = 0;//才
        //        int addcolumn = 0;
        //        bool receipt_flag = false;            // 是否回單
        //        bool pallet_recycling_flag = false;   // 是否棧板回收
        //        bool turn_board = false;              // 翻板
        //        bool upstairs = false;                // 上樓
        //        bool difficult_delivery = false;      // 困配
        //        string invoice_desc = "";             // 備註
        //        string arrive = "";                   // 到著
        //        string arrive_assign_date = "";       // 指配日期
        //        int collection_money = 0;
        //        int total_fee = 0;
        //        int arrive_to_pay_freight = 0;
        //        string subpoena_category = "";
        //        string holiday_delivery = ""; //是否假日配


        //        #endregion

        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            #region 取得資料
        //            if (DateTime.TryParse(dt.Rows[i]["print_date"].ToString(), out dt_tmp))
        //            {
        //                print_dateYY = dt_tmp.Year.ToString();
        //                print_date_mm = dt_tmp.Month.ToString();
        //                print_date_DD = dt_tmp.Day.ToString();
        //            }
        //            else
        //            {
        //                print_dateYY = "";
        //                print_date_mm = "";
        //                print_date_DD = "";
        //            }


        //            order_number = dt.Rows[i]["order_number"].ToString();
        //            ArticleName = dt.Rows[i]["ArticleName"].ToString();


        //            arrive_assign_date = dt.Rows[i]["arrive_assign_date"].ToString();

        //            send_contact = dt.Rows[i]["send_contact"].ToString();
        //            send_tel = dt.Rows[i]["send_tel"].ToString();
        //            send_addr = dt.Rows[i]["send_addr"].ToString();
        //            on_station_code = dt.Rows[i]["sta_name"].ToString();
        //            off_station_code = dt.Rows[i]["eta_name"].ToString();

        //            receive_contact = dt.Rows[i]["receive_contact"].ToString();
        //            receive_tel1 = dt.Rows[i]["receive_tel1"].ToString();
        //            receive_addr = dt.Rows[i]["receive_addr"].ToString();
        //            sendsite = dt.Rows[i]["sendsite"].ToString();
        //            supplier_name = dt.Rows[i]["supplier_name"].ToString();
        //            pricing_type = dt.Rows[i]["pricing_type"].ToString();
        //            check_number = dt.Rows[i]["check_number"].ToString().Trim();
        //            ticket = dt.Rows[i]["ticket"].ToString();
        //            if (!int.TryParse(dt.Rows[i]["ship_fee"].ToString(), out total_fee)) total_fee = 0;
        //            if (!int.TryParse(dt.Rows[i]["arrive_to_pay_freight"].ToString(), out arrive_to_pay_freight)) arrive_to_pay_freight = 0;
        //            if (!int.TryParse(dt.Rows[i]["collection_money"].ToString(), out collection_money)) collection_money = 0;
        //            if (!int.TryParse(dt.Rows[i]["pieces"].ToString(), out pieces)) pieces = 0;
        //            if (!int.TryParse(dt.Rows[i]["plates"].ToString(), out plates)) plates = 0;
        //            if (!int.TryParse(dt.Rows[i]["cbm"].ToString(), out cbm)) cbm = 0;
        //            receipt_flag = dt.Rows[i]["receipt_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["receipt_flag"]) : false;
        //            pallet_recycling_flag = dt.Rows[i]["pallet_recycling_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["pallet_recycling_flag"]) : false;
        //            turn_board = dt.Rows[i]["turn_board"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["turn_board"]) : false;
        //            upstairs = dt.Rows[i]["upstairs"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["upstairs"]) : false;
        //            difficult_delivery = dt.Rows[i]["difficult_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["difficult_delivery"]) : false;
        //            invoice_desc = dt.Rows[i]["invoice_desc"].ToString().Trim();
        //            arrive = dt.Rows[i]["arrive"].ToString().Trim();
        //            subpoena_category = dt.Rows[i]["subpoena_category"].ToString().Trim();
        //            holiday_delivery = dt.Rows[i]["holiday_delivery"].ToString().Trim();

        //            check_number_detail = check_number;
        //            if (check_number.Length == 10)
        //            {
        //                check_number_detail = string.Format(@"{0}-{1}-{2}"
        //                    , check_number.Substring(0, 3)
        //                    , check_number.Substring(3, 3)
        //                    , check_number.Substring(6, 4));
        //            }
        //            #endregion


        //            #region second way

        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, print_dateYY, 0);
        //            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date_mm, 0);
        //            ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, print_date_DD, 0);
        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_tel1, 0);

        //            ttExlApp.RowC += 1;//換行
        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 0);
        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_contact, 0);

        //            ttExlApp.RowC += 1;//換行
        //            string spec = "";
        //            if (receipt_flag == true) spec += "回單/";
        //            if (pallet_recycling_flag == true) spec += "棧板回收/";
        //            if (turn_board == true) spec += "翻板/";
        //            if (upstairs == true) spec += "上樓/";
        //            if (difficult_delivery == true) spec += "困配/";
        //            if (spec != "") spec = spec.Substring(0, spec.Length - 1);


        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, receive_addr + " " + spec + " " + invoice_desc, 0);

        //            ttExlApp.RowC += 1;//換行
        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 0);
        //            //ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, sendsite, 0);
        //            ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, on_station_code, 0);

        //            ttExlApp.RowC += 2;//換行
        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_addr, 0);
        //            //ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, arrive, 0);
        //            ttExlApp.Setvalue(11 + addcolumn, ttExlApp.RowC, off_station_code, 0);

        //            ttExlApp.RowC += 3;//換行
        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, holiday_delivery + arrive_assign_date, 0);
        //            str_unit = (pieces > 0 ? pieces.ToString() : "") + "件  /  ";
        //            if (pricing_type == "01")
        //            {
        //                str_unit += (plates > 0 ? plates.ToString() : "") + "大板";
        //            }
        //            else if (pricing_type == "03")
        //            {
        //                str_unit += (cbm > 0 ? cbm.ToString() : "") + "才";
        //            }
        //            else if (pricing_type == "04")
        //            {
        //                str_unit += (plates > 0 ? plates.ToString() : "") + "小板";
        //            }

        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, order_number, 0);
        //            ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, str_unit, 0);
        //            ttExlApp.RowC += 1;//換行
        //            ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ArticleName, 0);
        //            ttExlApp.RowC += 1;//換行

        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ticket, 0);
        //            switch (subpoena_category)
        //            {
        //                case "21":
        //                    if (arrive_to_pay_freight > 0)
        //                        ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight, 0);
        //                    else if (total_fee > 0)
        //                        ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, total_fee, 0);
        //                    break;
        //                default:
        //                    if (collection_money > 0) ttExlApp.Setvalue(8 + addcolumn, ttExlApp.RowC, collection_money, 0);
        //                    break;
        //            }



        //            ttExlApp.RowC += 1;//換行
        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, check_number_detail, 0);

        //            ttExlApp.RowC += 1;//換行
        //            ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, "*" + check_number + "*", 0);
        //            ttExlApp.RowC += 1;//換行
        //            switch (i % 2)
        //            {
        //                case 0:
        //                    addcolumn = 12;
        //                    ttExlApp.RowC -= 13;

        //                    break;
        //                case 1:
        //                    addcolumn = 0;
        //                    if ((i + 1) % 6 == 0 && i != dt.Rows.Count - 1)
        //                    {
        //                        ttExlApp.NextPage();
        //                    }
        //                    else
        //                    {
        //                        ttExlApp.RowC += 2;
        //                    }
        //                    break;
        //            }

        //            #endregion


        //        }
        //        #endregion
        //        ttExlApp.DeleteTable(1);

        //        FileInfo fi = new FileInfo(strSampleFileFullName);
        //        string FileName = String.Empty;


               
        //        //Server→轉Excel
        //        Boolean IsTest = false; //是否為測試模試
        //                                //FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);
        //        FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
        //        string ExportPath = "files/" + FileName;
        //        ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);

        //        if (!IsTest)
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);

        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);

        //        }


        //        #endregion

        //        //接下來測HTML + PDF

        //    }
        //    catch (Exception ex)
        //    {

        //        //組錯誤訊息
        //        string strErr = string.Empty;
        //        if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
        //        strErr = "簽收單列印-" + Request.RawUrl + strErr + ": " + ex.ToString();

        //        //錯誤寫至Log
        //        PublicFunction _fun = new PublicFunction();
        //        _fun.Log(strErr, "S");

        //        //導至錯誤頁面
        //        String _theUrl = string.Format("Oops.aspx?err_title={0}&err_msg={1}", "系統發生錯誤!", "系統資訊異常，請通知系統管理人員。");
        //        Server.Transfer(_theUrl, true);
        //    }
        //}
    }

    protected void btn_Excel_Click(object sender, EventArgs e)
    {
        //if (tb_ckeck.Text.Trim().Length == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇欲列印的項目!');</script>", false);
        //    return;
        //}
        if (tb_search.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入司機工號!');</script>", false);
            return;
        }
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();
        
            if (!DateTime.TryParse(sscan_date.Text, out dt_s) || !DateTime.TryParse(escan_date.Text, out dt_e))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
                return;
            }
            if (dt_s > dt_e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
                return;
            }
        

        string resule = tb_ckeck.Text;

        ListData _search = new ListData();
        _search.type = 2;
        _search.arrive = ddl_supplyer.SelectedValue;
        _search.date_start = dt_s;
        _search.date_end = dt_e;
        _search.strCheck_number = strCheck_number.Text;
        _search.rbpricingtype = "02";
        _search.sign_paper_print_flag = sign_paper_print_flag.SelectedValue;



        if (tb_search.Text != "")
        {
            _search.keyword = tb_search.Text;
        }
        else
        {
            _search.keyword = "";
        }
        _search.dttype = "2";


        if (tb_ckeck.Text.Trim() == "0") _search.check_all = true;
        else _search.check_item = tb_ckeck.Text.Trim();

        using (DataTable dt = GetDataTableList(_search))
        {
            try
            {
                string sheet_title = "到著簽單列印";
                string file_name = "到著簽單列印" + DateTime.Now.ToString("yyyyMMdd");
                using (ExcelPackage p = new ExcelPackage())
                {
                    p.Workbook.Properties.Title = sheet_title;
                    p.Workbook.Worksheets.Add(sheet_title);
                    ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    int colIndex = 1;
                    int rowIndex = 1;
                    foreach (DataColumn dc in dt.Columns)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(Color.LightGray);

                        //Setting Top/left,right/bottom borders.
                        var border = cell.Style.Border;
                        border.Bottom.Style =
                            border.Top.Style =
                            border.Left.Style =
                            border.Right.Style = ExcelBorderStyle.Thin;

                        //Setting Value in cell
                        cell.Value = dc.ColumnName;
                        colIndex++;
                    }
                    rowIndex++;

                    using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                    {
                        col.Style.Numberformat.Format = "yyyy/MM/dd";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }


                    foreach (DataRow dr in dt.Rows)
                    {
                        colIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            cell.Value = dr[dc.ColumnName];

                            //Setting borders of cell
                            var border = cell.Style.Border;
                            border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;
                            colIndex++;
                        }

                        rowIndex++;
                    }
                    ws.Cells.AutoFitColumns();
                    ws.View.FreezePanes(2, 1);
                    ws.Cells.Style.Font.Size = 12;
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(p.GetAsByteArray());
                    Response.End();

                }

            }
            catch (Exception ex)
            {

                //組錯誤訊息
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "簽收單列印-" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                //導至錯誤頁面
                String _theUrl = string.Format("Oops.aspx?err_title={0}&err_msg={1}", "系統發生錯誤!", "系統資訊異常，請通知系統管理人員。");
                Server.Transfer(_theUrl, true);

            }
        }



    }

    /// <summary>依類型取得標籤列印DataTable</summary>
    /// <param name="search"></param>
    /// <returns></returns>
    protected DataTable GetDataTableForList(ListData search)
    {
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = "";

                string wherestr1 = "";
                string wherestr = "";
                cmd.Parameters.AddWithValue("@Less_than_truckload", 1);

                switch (search.type)
                {
                    case 2:
                        #region 列印標籤資訊
                        strSQL = @"
SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS rowid
                                          ,CONVERT(CHAR(10), dr.print_date,111) print_date,
                                        case when (dr.holiday_delivery = 1) 
										then REPLACE (SUBSTRING(convert(nvarchar(10),dr.arrive_assign_date,120), 6, 10),'-','') 
                                        end arrive_assign_date
                                         ,dr.check_number
										 ,CASE WHEN cus.supplier_code IN('001','002') THEN REPLACE(REPLACE(cus.customer_shortname,'站',''),'HCT','') ELSE dr.supplier_name  END sendsite 
                                         ,CASE When dr.customer_code in ('F1300600002 ','F2900210002','F2900720002') THEN tb.station_name  ELSE sta1.station_name END sta_name --'發送站'
                                         ,eta1.station_name eta_name--'到著站'
                                         ,dr.send_contact
										 ,dr.send_tel
                                         ,dr.receive_contact
										 ,CASE WHEN (Len(dr.receive_tel1)>5) THEN SUBSTRING( dr.receive_tel1,0,6 )+'*9'+ SUBSTRING(dr.receive_tel1,6,Len(dr.receive_tel1)) 
                                               END receive_tel1
                                         ,dr.receive_city,dr.receive_area
                                         ,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
                                         ,ISNULL(dr.send_city,'') + ISNULL(dr.send_area,'') + ISNULL(dr.send_address,'') send_addr
                                         ,ISNULL(dr.receive_city,'') + ISNULL(dr.receive_area,'') + ISNULL(dr.receive_address,'') receive_addr
                                         ,sta1.station_scode
                                         --,sup.supplier_name,
                                         _special.code_name
                                         ,CONVERT(CHAR(10),dr.supplier_date,111) supplier_date
                                         ,CASE dr.receipt_flag WHEN 1 THEN 1 ELSE 0 END receipt_flag 
                                         ,_fee.total_fee ship_fee --'貨件運費'
                                         ,dr.arrive_to_pay_freight  --'到付運費'
                                         ,dr.customer_code
                                         ,dr.uniform_numbers
                                         ,NULL account_type --'月結／現收'
                                         ,_ticket.code_name ticket -- '傳票區分'
                                         ,dr.collection_money
                                         ,NULL account_date --'入帳日'
                                         ,_arr.arrive_state --'配達狀況
                                         ,_arr.arrive_date  --'配達時間
                                         ,_arr.arrive_item  arrive_item -- '配送異常說明
                                         ,CASE _arr.arrive_scan WHEN 1 THEN '是' ELSE '否' END arrive_scan --是否已掃瞄
                                         ,'','',''
                                         ,_fee.cscetion_fee -- 配送費用'    
                                         ,dr.receipt_flag
                                         ,dr.pallet_recycling_flag
                                         ,dr.turn_board
                                         ,dr.upstairs
                                         ,dr.difficult_delivery
                                         ,dr.invoice_desc
                                         ,ISNULL(arr.station_Name,'') as arrive
                                         ,dr.subpoena_category
                                         ,dr.order_number
										 ,dr.ArticleName
                                         ,case when( cus.is_weekend_delivered = 1 and dr.holiday_delivery = 1 and dr.area_arrive_code <> '99')or (cus.is_weekend_delivered = 1 and cal.KindOfDay in ('SATURDAY','SUNDAY','HOLIDAY') and dr.area_arrive_code <> '99') then '假日配'
                                          else ''
                                          end as holiday_delivery
                                          ,F.cdate  as FirstPrintDate  -- 初次列印時間
                                            ,case when dr.sign_paper_print_flag = '1' then '已列印'
                                                  else '未列印'
                                                   end as IsPrint
                                    FROM tcDeliveryRequests dr With(Nolock)
                                    --LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.customer_code = cus.customer_code 
                                    LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
									LEFT JOIN ttArriveSitesScattered sta With(nolock) on dr.send_city = sta.post_city and dr.send_area = sta.post_area
									LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
								    LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
									LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode
                                    LEFT JOIN pickup_request_for_apiuser tt with(nolock) on dr.check_number = tt.check_number
                                    LEFT JOIN tbStation tb with(nolock) on tb.station_code = tt.supplier_code
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr 
                                    left join tbDrivers D on D.driver_code = _arr.newest_driver_code
                                    left join tbCalendar cal on convert(nvarchar(10),dr.arrive_assign_date,120 ) = convert(nvarchar(10),cal.Date,120 )
                                    LEFT JOIN FirstPrintDate F with(nolock) on F.request_id = dr.request_id
                                    WHERE  
									dr.latest_delivery_date BETWEEN @dateS AND @dateE  
AND dr.supplier_date IS NOT NULL
AND dr.cancel_date IS NULL 
and dr.latest_delivery_driver = @keyword
AND dr.Less_than_truckload = 1 
and dr.customer_code <> 'F3500010002'
and dr.pricing_type  = '02'";

                        if (!search.check_all && search.check_item.Split(',').Length > 0)
                        {
                            strSQL += " AND dr.request_id IN (" + search.check_item + ") ";
                        }



                        #endregion

                        break;
                    default:
                        #region 網頁list顯示
                        //單筆託運單列印
                        if (search.strCheck_number != "")
                        {
                            strSQL = @"
SELECT ROW_NUMBER() OVER(ORDER BY dr.latest_delivery_date desc) AS rowid
,CONVERT(CHAR(10), dr.print_date,111) print_date,dr.check_number
,CASE When dr.customer_code in ('F1300600002 ','F2900210002 ','F2900720002 ') THEN tb.station_name  ELSE sta1.station_name END sta_name --'發送站'
,eta1.station_name eta_name--'到著站'
,send_contact
,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
--,ISNULL(dr.supplier_code,'') +' '+ISNULL(sup.supplier_name,'') 'supplier' 
,ISNULL(dr.area_arrive_code,'') +' '+ISNULL(arr.station_name,'') 'arr'  
,CONVERT(CHAR(10),_arr.send_date,111) supplier_date     --配送日                                    
,dr.collection_money
,_arr.arrive_state --配達狀況 
,_arr.arrive_date  --配達時間 
,_arr.arrive_item  arrive_item -- 配送異常說明
,F.cdate  as FirstPrintDate  -- 初次列印時間
,case when dr.sign_paper_print_flag = '1' then '已列印'
                                                  else '未列印'
                                                   end as IsPrint
,CASE WHEN (Len(dr.receive_contact)>2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'+ SUBSTRING(dr.receive_contact,3,Len(dr.receive_contact)) 
    WHEN (Len(dr.receive_contact)=2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'
ELSE dr.receive_contact END 'receive_contact',dr.request_id  
FROM tcDeliveryRequests dr With(Nolock)
--LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode
left join tbDrivers D on D.driver_code = dr.latest_delivery_driver
LEFT JOIN pickup_request_for_apiuser tt with(nolock) on dr.check_number = tt.check_number
LEFT JOIN tbStation tb with(nolock) on tb.station_code = tt.supplier_code
LEFT JOIN FirstPrintDate F with(nolock) on F.request_id = dr.request_id
CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
WHERE dr.check_number = @strCheck_number
";
                        }
                        //以司機掃讀配送列印
                        else
                        {
                            strSQL = @" WITH scanLog AS
(
SELECT B.*,ROW_NUMBER() OVER (PARTITION BY B.check_number ORDER BY B.check_number,B.scan_date DESC) AS rn
FROM ttDeliveryScanLog B 
INNER JOIN tcDeliveryRequests A with(nolock) ON A.check_number = B.check_number
INNER JOIN tbDrivers C with(nolock) ON B.driver_code = C.driver_code
WHERE scan_item = 2
AND ( C.driver_code = @keyword or  C.driver_name = @keyword)
AND CONVERT(CHAR(10),B.scan_date,111) BETWEEN @dateS AND  @dateE 
AND A.Less_than_truckload  = 1
AND A.DeliveryType = 'D'
AND A.pricing_type in ('02')
and A.customer_code <> 'F3500010002'
--AND (A.latest_arrive_option_driver='GY061' or A.latest_arrive_option_driver IS NULL)
--AND (@check_number ='' or A.check_number = @check_number)
AND ( C.station = Replace (@supplier, 'F', '') OR @supplier ='' OR C.station=@supplier )
)
SELECT ROW_NUMBER() OVER(ORDER BY dr.latest_delivery_date desc) AS rowid
,CONVERT(CHAR(10), dr.print_date,111) print_date,dr.check_number
,CASE When dr.customer_code in ('F1300600002 ','F2900210002','F2900720002 ') THEN tb.station_name  ELSE sta1.station_name END sta_name --'發送站'
,eta1.station_name eta_name--'到著站'
,send_contact
,dr.pricing_type,dr.pieces,dr.plates,dr.cbm 
--,ISNULL(dr.supplier_code,'') +' '+ISNULL(sup.supplier_name,'') 'supplier' 
,ISNULL(dr.area_arrive_code,'') +' '+ISNULL(arr.station_name,'') 'arr'  
,CONVERT(CHAR(10),_arr.send_date,111) supplier_date     --配送日                                    
,_fee.total_fee ship_fee --'貨件運費'
,dr.arrive_to_pay_freight  --'到付運費'
,dr.collection_money
,_arr.arrive_state --配達狀況 
,_arr.arrive_date  --配達時間 
,_arr.arrive_item  arrive_item -- 配送異常說明 
,_fee.cscetion_fee -- 配送費用 
,CASE WHEN (Len(dr.receive_contact)>2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'+ SUBSTRING(dr.receive_contact,3,Len(dr.receive_contact)) 
    WHEN (Len(dr.receive_contact)=2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'
ELSE dr.receive_contact END 'receive_contact',dr.request_id  
,F.cdate  as FirstPrintDate  -- 初次列印時間
,case when dr.sign_paper_print_flag = 1 then '已列印'
                                                  else '未列印'
                                                   end as IsPrint
FROM tcDeliveryRequests dr With(Nolock)
inner JOIN (SELECT * FROM scanLog WHERE rn= 1) Log on dr.check_number = Log.check_number  
--LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode
LEFT JOIN pickup_request_for_apiuser tt with(nolock) on dr.check_number = tt.check_number
LEFT JOIN tbStation tb with(nolock) on tb.station_code = tt.supplier_code
CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
left join tbDrivers D on D.driver_code = Log.driver_code
CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
LEFT JOIN FirstPrintDate F with(nolock) on F.request_id = dr.request_id
WHERE   CONVERT(CHAR(10),Log.scan_date,111) BETWEEN @dateS AND  @dateE  
AND dr.supplier_date IS NOT NULL
AND dr.cancel_date IS NULL 
AND ( Log.driver_code = @keyword or  D.driver_name = @keyword)
--AND ( _arr.newest_driver_code = @keyword or  _arr.driver_name = @keyword)
AND dr.Less_than_truckload = @Less_than_truckload
--AND (D.station = Replace (@supplier, 'F', '') OR @supplier ='' )
--AND (@strCheck_number ='' or dr.Check_number = @strCheck_number )
AND dr.Less_than_truckload =1
AND dr.DeliveryType = 'D'
and dr.customer_code <> 'F3500010002'
--and _arr.arrive_state = '配送'
and dr.pricing_type  = '02'

";
                        }
                        #endregion
                        break;
                }


                if (sign_paper_print_flag.SelectedValue == "1")
                {
                    strSQL += " and dr.sign_paper_print_flag = 1";
                }
                else if (sign_paper_print_flag.SelectedValue == "0")
                {
                    strSQL += " and dr.sign_paper_print_flag is null";
                }
                else 
                { 
                
                }

                strSQL += " ORDER BY dr.latest_delivery_date desc ;";

                cmd.CommandText = string.Format(strSQL, wherestr, wherestr1);
                cmd.Parameters.AddWithValue("@supplier", search.arrive);
                cmd.Parameters.AddWithValue("@dateS", search.date_start.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", search.date_end.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@keyword", search.keyword);
                cmd.Parameters.AddWithValue("@strCheck_number", search.strCheck_number);
               
                
                
                cmd.CommandTimeout = 600;
                dt = dbAdapter.getDataTable(cmd);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
        }
        return dt;
    }

    protected DataTable GetDataTableList(ListData search)
    {
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = "";
                string wherestr = "";
                string wherestr1 = "";

                wherestr += " ";

                cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                
                #region 網頁list顯示

                strSQL = @" WITH scanLog AS
(
SELECT B.*,ROW_NUMBER() OVER (PARTITION BY B.check_number ORDER BY B.check_number,B.scan_date DESC) AS rn
FROM ttDeliveryScanLog B 
INNER JOIN tcDeliveryRequests A with(nolock) ON A.check_number = B.check_number
INNER JOIN tbDrivers C with(nolock) ON B.driver_code = C.driver_code
WHERE scan_item = 2
AND ( C.driver_code = @keyword or  C.driver_name = @keyword)
AND CONVERT(CHAR(10),B.scan_date,111) BETWEEN @dateS AND  @dateE 
AND A.Less_than_truckload  = 1
AND A.pricing_type in ('02')
and A.customer_code <> 'F3500010002'
--AND (@check_number ='' or A.check_number = @check_number)
AND ( C.station = Replace (@supplier, 'F', '') OR @supplier ='' )
)
SELECT ROW_NUMBER() OVER(ORDER BY dr.supplier_date) AS 'NO.'
                                         ,CONVERT(CHAR(10), dr.print_date,111) '發送日'--print_date
                                         ,dr.check_number '貨號'
                                         ,sta1.station_name '發送站' --'發送站'
                                         ,send_contact  '寄件人'
                                         ,dr.pieces '數量'
                                         ,ISNULL(dr.area_arrive_code,'') +' '+ISNULL(arr.station_name,'') '配送站'  
                                         ,_fee.total_fee '貨件運費' --'貨件運費'
                                         ,_arr.arrive_state '配達狀況'--配達狀況 
                                         ,CONVERT(CHAR(10),_arr.send_date,111) '配送日'
                                         ,_arr.arrive_date  '配達日'--配達時間 
                                         ,_arr.arrive_item  '配送異常' -- 配送異常說明 
                                         ,_fee.cscetion_fee '配送費用'-- 配送費用 
                                         ,CASE WHEN (Len(dr.receive_contact)>2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'+ SUBSTRING(dr.receive_contact,3,Len(dr.receive_contact)) 
                                               WHEN (Len(dr.receive_contact)=2) THEN SUBSTRING( dr.receive_contact,1,1 )+'＊'
                                          ELSE dr.receive_contact END '收件者'
                                       --,dr.request_id  
FROM tcDeliveryRequests dr With(Nolock)
inner JOIN (SELECT * FROM scanLog WHERE rn= 1) Log on dr.check_number = Log.check_number  
LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
LEFT JOIN tbStation arr With(Nolock) ON arr.station_scode = dr.area_arrive_code
LEFT JOIN tbStation sta1 With(nolock) on dr.supplier_code  = sta1.station_code
LEFT JOIN ttArriveSitesScattered eta With(nolock) on dr.receive_city = eta.post_city and dr.receive_area = eta.post_area
LEFT JOIN tbStation eta1 With(nolock) on eta.station_code  = eta1.station_scode
CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
left join tbDrivers D on D.driver_code = Log.driver_code
CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
WHERE   CONVERT(CHAR(10),Log.scan_date,111) BETWEEN @dateS AND  @dateE  
AND dr.supplier_date IS NOT NULL
AND dr.cancel_date IS NULL 
AND ( Log.driver_code = @keyword or  D.driver_name = @keyword)
AND ( _arr.newest_driver_code = @keyword or  _arr.driver_name = @keyword)
AND dr.Less_than_truckload = @Less_than_truckload
AND (D.station = Replace (@supplier, 'F', '') OR @supplier ='' )
AND (@strCheck_number ='' or dr.Check_number = @strCheck_number )
AND dr.Less_than_truckload =1
and dr.customer_code <> 'F3500010002'
and _arr.arrive_state = '配送'
and dr.pricing_type  = '02' 
";

                #endregion
                if (!search.check_all && search.check_item.Split(',').Length > 0)
                {
                    strSQL += " AND dr.request_id IN (" + search.check_item + ") ";
                }

                strSQL += " ORDER BY dr.supplier_date ;";
                cmd.CommandText = string.Format(strSQL, wherestr, wherestr1);
                cmd.Parameters.AddWithValue("@supplier", search.arrive);
                cmd.Parameters.AddWithValue("@dateS", search.date_start.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", search.date_end.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@keyword", search.keyword);
                cmd.Parameters.AddWithValue("@sign_paper_print_flag", search.sign_paper_print_flag);
                cmd.CommandTimeout = 600;
                dt = dbAdapter.getDataTable(cmd);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
        }
        return dt;
    }

    protected void tbsearch_TextChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@driver_code", tb_search.Text.ToUpper());
            cmd.CommandText = @"
  select ts.station_code 
  from [tbDrivers] td With(Nolock) 
  LEFT JOIN tbStation ts With(Nolock) on td.station = ts.station_scode
  where td.driver_code = @driver_code ";
            dt = dbAdapter.getDataTable(cmd);
        }
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["station_code"].ToString() != "")
            {
                ddl_supplyer.SelectedValue = dt.Rows[0]["station_code"].ToString();
            }
            else
            {
                ddl_supplyer.SelectedIndex = 0;
            }
        }
        else
        {
            ddl_supplyer.SelectedIndex = 0;
        }
    }

    
    public class ListData
    {
        /// <summary>1:網頁list顯示 2.列印標籤資訊</summary>
        public int type { get; set; }
        /// <summary>區配商 </summary>
        public string supplyer { get; set; }

        /// <summary>配送廠商 </summary>
        public string arrive { get; set; }

        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        /// <summary>論板專車 </summary>
        public string rbpricingtype { get; set; }
        /// <summary>貨號、寄件人、收貨人(where條件)</summary>
        public String keyword { get; set; }

        /// <summary>單筆託運單號</summary>
        public String strCheck_number { get; set; }

        /// <summary>自選項目(ex:5,6,8)</summary>
        public String check_item { get; set; }
        /// <summary>全選 </summary>
        public Boolean check_all { get; set; }

        /// <summary></summary>
        public String dttype { get; set; }
        public ListData()
        {
            type = 1;
            check_item = "";
            check_all = false;
            keyword = "0";
        }
        public String sign_paper_print_flag { get; set; }
    }

    private static String PreparePOSTForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }


    protected void sign_paper_print_flag_SelectedIndexChanged(object sender, EventArgs e)
    {
     
    }
}