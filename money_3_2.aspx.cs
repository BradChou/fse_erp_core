﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

public partial class money_3_2 : System.Web.UI.Page
{

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Boolean IsManager = false;
            if (Session["account_code"] != null & (Session["account_code"].ToString() == "skyeyes"|| Session["account_code"].ToString() == "9990008") )
            {
                IsManager = true;
            }
            else
            {
                manager_type = Session["manager_type"].ToString(); //管理單位類別   
                switch (manager_type)
                {
                    case "4":  //區配商
                        customer_type.Enabled = false;
                        customer_type.SelectedValue = "1";
                        break;
                    case "3":  //峻富自營
                    case "5":  //區配商自營
                        customer_type.Enabled = false;
                        customer_type.SelectedValue = "3";
                        break;
                }
            }

            

            customer_type_SelectedIndexChanged(null, null);

            div_Upload.Visible = false;
            if (DateTime.Now.Day >= 20 && DateTime.Now.Day <= 25
                || IsManager)
            {
                div_Upload.Visible = true;
            }

        }
    }

    protected void customer_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime date_tmp;
        level.Items.Clear();
        switch (customer_type.SelectedValue)
        {
            case "1":  //區配論板
                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandText = "select  distinct class_level    from tbPriceSupplier order by class_level";
                level.DataSource = dbAdapter.getDataTable(cmd1);
                level.DataValueField = "class_level";
                level.DataTextField = "class_level";
                level.DataBind();
                textbox1.Text = "區配棧板公版";
                //default_level.SelectedValue = "9級距";

                using (SqlCommand cmd_p = new SqlCommand())
                {
                    cmd_p.CommandText = "select top(1) class_level,active_date  from ttPriceClassLog  where active_date <= getdate() order by active_date desc";
                    DataTable dt = dbAdapter.getDataTable(cmd_p);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        default_level.SelectedValue = dt.Rows[0]["class_level"].ToString();
                        if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
                    }
                }
                    
                break;
            case "2":  //竹運論板
                textbox1.Text = "竹運棧板公版";
                default_level.SelectedValue = "0";
                level.Items.Insert(0, new ListItem("0級距", ""));
                break;
            case "3":  //自營論板
                textbox1.Text = "自營棧板公版";
                using (SqlCommand cmd_p = new SqlCommand())
                {
                    cmd_p.CommandText = "select top(1) class_level,active_date  from ttPriceClassLog  where active_date <= getdate() order by active_date desc";
                    DataTable dt = dbAdapter.getDataTable(cmd_p);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        default_level.SelectedValue = dt.Rows[0]["class_level"].ToString();
                        if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
                    }
                }

                //level.Items.Insert(0, new ListItem("0級距", ""));
                SqlCommand cmd3= new SqlCommand();
                cmd3.CommandText = "select  distinct class_level    from tbPriceBusiness order by class_level";
                level.DataSource = dbAdapter.getDataTable(cmd3);
                level.DataValueField = "class_level";
                level.DataTextField = "class_level";
                level.DataBind();
                break;
            case "4":  //c配
                SqlCommand cmd4 = new SqlCommand();
                cmd4.CommandText = "select  distinct class_level    from tbPriceCSection order by class_level";
                level.DataSource = dbAdapter.getDataTable(cmd4);
                level.DataValueField = "class_level";
                level.DataTextField = "class_level";
                level.DataBind();
                textbox1.Text = "C配棧板公版";
                using (SqlCommand cmd_p = new SqlCommand())
                {
                    cmd_p.CommandText = "select top(1) class_level,active_date  from ttPriceClassLog  where active_date <= getdate() order by active_date desc";
                    DataTable dt = dbAdapter.getDataTable(cmd_p);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        default_level.SelectedValue = dt.Rows[0]["class_level"].ToString();
                        if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
                    }
                }

                break;

            case "5":  //合發專用
                SqlCommand cmd5 = new SqlCommand();
                cmd5.CommandText = "select  distinct class_level    from tbPriceCSectionA05 order by class_level";
                level.DataSource = dbAdapter.getDataTable(cmd5);
                level.DataValueField = "class_level";
                level.DataTextField = "class_level";
                level.DataBind();

                textbox1.Text = "合發專用棧板公版";
                default_level.SelectedValue = "0";
                level.Items.Insert(0, new ListItem("0級距", ""));
                break;
        }

    }

    ////取得生效日(公版)
    //public void SetActive_date(int QryNum)
    //{
    //    DateTime date_tmp;

    //    //預設值
    //    fee_sdate.Text = "";
    //    fee_sdate.Enabled = false;
    //    switch (QryNum)
    //    {
    //        case 1:
    //        case 4:
    //        case 5:
    //            SqlCommand cmdt = new SqlCommand();
    //            DataTable dt;
    //            cmdt.CommandText = "SELECT TOP 1 active_date FROM ttPriceClassLog With(Nolock) ORDER BY active_date DESC";
    //            dt = dbAdapter.getDataTable(cmdt);
    //            if (dt.Rows.Count > 0)
    //            {
    //                if (DateTime.TryParse(dt.Rows[0]["active_date"].ToString(), out date_tmp)) fee_sdate.Text = date_tmp.ToString("yyyy/MM/dd");
    //            }
    //            dt.Dispose();
    //            break;
    //    }
    //}

    protected void btQry_Click(object sender, EventArgs e)
    {
        pan_PriceLog.Visible = false;
        int iQry;
        if (!int.TryParse(customer_type.SelectedValue, out iQry)) iQry = 0;
        //SetActive_date(iQry);

        SqlCommand cmd1 = new SqlCommand();
        switch (customer_type.SelectedValue)
        {
            case "1":  //區配論板
                cmd1.CommandText = "select A.*, B.code_name from tbPriceSupplier A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM'  where pricing_code = '" + pricing_code.SelectedValue + "' and class_level = '" + level.SelectedValue + "' ";
                break;
            case "2":  //竹運論板
                cmd1.CommandText = "select A.*, B.code_name , '0' as class_level from tbPriceHCT A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
                break;
            case "3":  //自營論板
                cmd1.CommandText = "select A.*, B.code_name  from tbPriceBusiness A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' and class_level = '" + level.SelectedValue + "' ";
                break;
            case "4":  //C配
                cmd1.CommandText = "select A.*, B.code_name from tbPriceCSection A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' and class_level = '" + level.SelectedValue + "' ";
                break;
            case "5":  //合發專用
                cmd1.CommandText = "select '' as start_city , '' as  end_city, A.*, B.code_name from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
                //cmd1.CommandText = "select A.*, B.code_name from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM' where pricing_code = '" + pricing_code.SelectedValue + "' ";
                break;
        }
        New_List.DataSource = dbAdapter.getDataTable(cmd1);
        New_List.DataBind();

        pan_PriceList.Visible = true;


    }


    protected void btndownload_Click(object sender, EventArgs e)
    {
        #region 匯出excel樣版
        string ttServerPath = this.Server.MapPath(".") + "\\files\\";
        string FileName = "公版運價";
        string tablename = "";
        string wherestr = "";
        SqlCommand objCommand = new SqlCommand();
        Hashtable hash = new Hashtable();
        DataSet ds = new DataSet();
        DataTable dt = null;
        SqlCommand cmd1 = new SqlCommand();
        switch (customer_type.SelectedValue)
        {
            case "1":  //區配論板
                tablename = "tbPriceSupplier";
                cmd1.CommandText = string.Format(@"select A.start_city ,A.end_city , A.class_level , A.pricing_code , 
                                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                                                 from tbPriceSupplier A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM'
                                                 where pricing_code = '" + pricing_code.SelectedValue + "' and class_level = '" + level.SelectedValue + "' ");

                break;
            case "2":  //竹運論板
                tablename = "tbPriceHCT";
                cmd1.CommandText = string.Format(@"select A.start_city ,A.end_city , '0' as class_level  , A.pricing_code , 
                                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                                                 from tbPriceHCT A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM' 
                                                 where pricing_code = '" + pricing_code.SelectedValue + "' ");

                break;
            case "3":  //自營論板
                tablename = "tbPriceBusiness";
                cmd1.CommandText = string.Format(@"select A.start_city ,A.end_city , '0' as class_level  , A.pricing_code , 
                                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                                                 from tbPriceBusiness A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM' 
                                                 where pricing_code = '" + pricing_code.SelectedValue + "' ");

                break;
            case "4":  //C配
                tablename = "tbPriceCSection";
                cmd1.CommandText = string.Format(@"select A.start_city ,A.end_city , A.class_level , A.pricing_code , 
                                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                                                 from tbPriceCSection A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM' 
                                                 where pricing_code = '" + pricing_code.SelectedValue + "' ");

                break;
            case "5":  //合發專用
                FileName = "合發公版運價";
                tablename = "tbPriceCSectionA05";
                cmd1.CommandText = string.Format(@"select  A.class_level , A.pricing_code , 
                                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                                                 from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM' 
                                                 where pricing_code = '" + pricing_code.SelectedValue + "' ");
                //tablename = "tbPriceCSectionA05";
                //cmd1.CommandText = string.Format(@"select A.start_city ,A.end_city , A.class_level , A.pricing_code , 
                //                                 A.plate1_price ,A.plate2_price,A.plate3_price,A.plate4_price,A.plate5_price,A.plate6_price , '" + tablename + @"' as tablename
                //                                 from tbPriceCSectionA05 A Left join tbItemCodes B on B.code_id = A.pricing_code and code_sclass = 'PM' 
                //                                 where pricing_code = '" + pricing_code.SelectedValue + "' ");

                break;
        }
        dt = dbAdapter.getDataTable(cmd1);


        //#region 調整Table內容     

        //foreach (DataRow row in dt.Rows)
        //{
        //    row["tablename"] = tablename;
        //}

        //hash.Add("##paradate1", date_range_start.Text);
        //hash.Add("##paradate2", date_range_end.Text);
        //hash.Add("##parasts", ttStatus);
        //hash.Add("##paracar", car_select.SelectedItem.Text == "" ? car_select.SelectedItem.Text : "全部車輛");
        //#endregion

        ds.Tables.Add(dt);
        
        dt.TableName = FileName;
        string printName = FileName +"-" + level.SelectedValue.ToString() + "級距";

        #region 製作匯出
        //tablename = "公版運價-" + tablename;
        reportAdapter.npoiSampleToExcel(ttServerPath, ds, hash, FileName, printName);
        #endregion
        #endregion

    }

    protected void btnLog_Click(object sender, EventArgs e)
    {
        pan_PriceList.Visible = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"SELECT TOP 100 ROW_NUMBER() OVER(ORDER BY cdate DESC) AS 'no'
                                      ,CASE(tariffs_table_type) WHEN '1' THEN '竹運公版' WHEN '2' THEN '區配公版' WHEN '3' THEN '自營公版' WHEN '4' THEN 'C配公版' WHEN '5' THEN 'C配合發' ELSE'其他' END　'table_type'
                                      ,B.code_name, class_level
                                      ,Format(cdate, N'yyyy/MM/dd HH:mm') 'cdate'                                    
                                      ,CASE WHEN (Len(cuser)>=3) THEN SUBSTRING( cuser,1,1 )+'*'+ SUBSTRING( cuser,3,Len(cuser))
                                               WHEN (Len(cuser)=2) THEN SUBSTRING( cuser,1,1 )+'*'
                                          ELSE cuser END 'newuser'

                                FROM ttPriceUpdateLog A With(Nolock)
                                LEFT JOIN tbItemCodes B on B.code_id =A.pricing_code and code_sclass = 'PM'  
                                ORDER BY cdate DESC";
            List_log.DataSource = dbAdapter.getDataTable(cmd);
            List_log.DataBind();
        }
        pan_PriceLog.Visible = true;
    }
}