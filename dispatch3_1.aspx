﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterDispatch.master" AutoEventWireup="true" CodeFile="dispatch3_1.aspx.cs" Inherits="dispatch3_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $(document).on("click", "#plus", function () {
                var maxplates = $("#<%= Hid_maxplates.ClientID%>").val();
                var addplates = $(".addplates").val();
                if ($.isNumeric(addplates) && (parseInt(addplates) + 1 <= maxplates)) {
                    $(".addplates").val(parseInt(addplates) + 1);
                }
            });

            $(document).on("click", "#minus", function () {
                var addplates = $(".addplates").val();
                if ($.isNumeric(addplates) && ((addplates - 1) >=0)) {
                    $(".addplates").val(addplates - 1);
                }
            });

            $("#carsel").fancybox({
                'width': 1200,
                'height': 800,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

        });

        $(document).ready(function () {
            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 15;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

            var _width = $(document).width() * 0.8;  //850;
            var _height = $(document).height() * 0.8;//300;

            $("#editclick").fancybox({
                'width': _width,
                'height': _height,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
        });


        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        function OpenLinkByFancyBox(the_url, df_width, df_height) {
            if ($.trim(the_url) != "") {

                if (the_url.indexOf('?') >= 0) {
                    the_url = the_url + '&r=' + Math.random();
                } else {
                    the_url = the_url + '?r=' + Math.random();
                }

                var _width = $(document).width() * 0.8;  //850;
                var _height = $(document).height() * 0.8;//300;
                if (typeof (df_width) != "undefined") {
                    _width = parseInt(df_width);
                }
                if (typeof (df_height) != "undefined") {
                    _height = parseInt(df_height);
                }

                $.fancybox({
                    'type': 'iframe',
                    'width': _width,
                    'height': _height,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'href': the_url
                });
            }
        }

       
    </script>

    <style>
        ._th {
            text-align: center;
        }

        input[type="checkbox"] {
            display: inherit;
        }
        .addplates {
            text-align: center;
            font-size :larger ;
            font-weight :bold  ;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">B段集貨</h2>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>卸集日期：</label>
                    <asp:TextBox ID="date" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>
                    <%--<asp:TextBox ID="dateS" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                    <asp:TextBox ID="dateE" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>--%>
                    <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="search_Click" />
                </div>
                <div class="form-group form-inline">
                   <%-- <div>
                        <label>關鍵字：</label>
                        <asp:TextBox ID="keyword" runat="server" class="form-control " autocomplete="off"></asp:TextBox>
                    </div>--%>
                    <div class="text-right ">
                        <%--<a href="dispatch1_1_edit.aspx"  ><span class="btn btn-warning glyphicon glyphicon-plus">新增任務</span></a>--%>

                                <%--<asp:Button ID="AddTask" CssClass="btn btn-warning glyphicon glyphicon-plus" runat="server" Text="新增任務" OnClick="AddTask_Click" Style="left: 0px; top: 1px" />--%>
                                <asp:Button ID="AddBTask" CssClass="btn btn-warning glyphicon glyphicon-plus" runat="server" Text="新增任務"  Style="left: 0px; top: 1px" OnClick="AddBTask_Click" />

                    </div>
                </div>
            </div>
                       
              <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="max-height: 600px; overflow: auto;">
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table" />
                        <tr class="tr-only-hide">
                            <th class="_th"></th>
                            <th class="_th">000</th>
                            <th class="_th">A01</th>
                            <th class="_th">A02</th>
                            <th class="_th">A03</th>
                            <th class="_th">A04</th>
                            <th class="_th">A05</th>
                            <th class="_th">A06</th>
                            <th class="_th">A08</th>
                            <th class="_th">B01</th>
                            <th class="_th">B02</th>
                            <th class="_th">B03</th>
                            <th class="_th">B04</th>
                            <th class="_th">B05</th>
                            <th class="_th">C01</th>
                            <th class="_th">C02</th>
                            <th class="_th">C03</th>
                            <th class="_th">C04</th>
                            <th class="_th">C05</th>
                            <th class="_th">C06</th>
                            <th class="_th">D01</th>
                            <th class="_th">E01</th>
                            <th class="_th">E02</th>
                            <th class="_th">F01</th>
                            <th class="_th">F02</th>
                            <th class="_th">F03</th>
                            <th class="_th">G02</th>
                            <th class="_th">G03</th>
                            <th class="_th">總計</th>
                        </tr>
                        <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Li_warehouse" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.warehouse").ToString())%>'></asp:Literal>
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.warehouse_text").ToString())%></td>
                                    <td><asp:LinkButton ID="bt000" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="000" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.000").ToString())%>' ></asp:LinkButton>
                                        <asp:Label ID="lb000" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.000").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A01").ToString())%>' ></asp:LinkButton>
                                        <asp:Label ID="lbA01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A02" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A02").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbA02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A02").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA03" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A03" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A03").ToString())%>' ></asp:LinkButton>
                                        <asp:Label ID="lbA03" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A03").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA04" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A04" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A04").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbA04" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A04").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA05" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A05" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A05").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbA05" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A05").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA06" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A06" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A06").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbA06" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A06").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btA08" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="A08" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A08").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbA08" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.A08").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btB01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="B01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbB01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btB02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="B02" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B02").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbB02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B02").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btB03" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="B03" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B03").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbB03" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B03").ToString())%>'></asp:Label></td>
                                   <td>
                                        <asp:LinkButton ID="btB04" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="B04" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B04").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbB04" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B04").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btB05" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="B05" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B05").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbB05" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.B05").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbC01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C02" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C02").ToString())%>' ></asp:LinkButton>
                                        <asp:Label ID="lbC02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C02").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC03" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C03" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C03").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbC03" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C03").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC04" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C04" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C04").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbC04" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C04").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC05" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C05" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C05").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbC05" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C05").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btC06" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="C06" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C06").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbC06" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.C06").ToString())%>'></asp:Label></td>
                                   <td>
                                        <asp:LinkButton ID="btD01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="D01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.D01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbD01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.D01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btE01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="E01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.E01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbE01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.E01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btE02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="E02" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.E02").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbE02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.E02").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btF01" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="F01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbF01" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btF02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="F02" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F02").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbF02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F02").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btF03" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="F03" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F03").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbF03" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.F03").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btG02" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="G01" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.G01").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbG02" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.G01").ToString())%>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton ID="btG03" runat="server"  Visible ='<%# (ssApStatus =="Edit" && DataBinder.Eval(Container, "DataItem.warehouse").ToString()!= "") ?true :false  %>' CommandName ="Add" CommandArgument="Z99" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Z99").ToString())%>'></asp:LinkButton>
                                        <asp:Label ID="lbG03" runat="server" Visible ='<%# (ssApStatus =="Browse" ||  DataBinder.Eval(Container, "DataItem.warehouse").ToString()== "") ?true :false  %>' Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Z99").ToString())%>'></asp:Label></td>
                                    <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tot").ToString())%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="29" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                        </table>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnQry" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="modal fade" id="modal-default_01">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">新增稽查排班表</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group text-center  ">
                                    <asp:TextBox ID="addwarehouse" runat="server" Visible ="false"  ></asp:TextBox>
                                    <asp:TextBox ID="addsupplier" runat="server" Visible ="false"></asp:TextBox>
                                    <asp:HiddenField ID="Hid_maxplates" runat="server" />
                                    <a id="minus"><span class="btn btn-warning glyphicon glyphicon-minus"></span></a><asp:TextBox ID="Addplates"  runat="server" CssClass="addplates" Width="100px" ></asp:TextBox><a id="plus" ><span class="btn btn-warning glyphicon glyphicon-plus"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                            <asp:Button ID="btn_Add" class="btn btn-primary" runat="server" Text="加入" OnClick="btn_Add_Click"  />                           
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <asp:Panel ID="pl_AddTadk" runat="server" Visible="false">

                <div class="row">
                    <div class="form-group col-lg-4  form-inline">
                        <label for="Task_id">任務編號</label>
                        <asp:TextBox ID="Task_id" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-4  form-inline">
                        <label for="tbplates"><span class="REDWD_b">*</span>板數</label>
                        <asp:TextBox ID="tbplates" runat="server" CssClass="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"   Enabled ="false"  ></asp:TextBox><span >請點選上方要加入的板數</span>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="tbplates" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入板數</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-lg-4  form-inline">
                        <label for="task_date"><span class="REDWD_b">*</span>日期</label>
                        <asp:TextBox ID="task_date" runat="server" CssClass="form-control date_picker" autocomplete="off"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="task_date" ForeColor="Red" Font-Size="Small" ValidationGroup="validate">請輸入任務日期</asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-lg-4  form-inline">
                        <label for="supplier">供應商</label>
                        <asp:DropDownList ID="supplier" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
                    </div>
                    <div class="form-group col-lg-4  form-inline">
                        <label for="driver">司　　機</label>
                        <asp:TextBox ID="driver" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group col-lg-4  form-inline">
                        <label for="car_license">車牌</label>
                        <asp:TextBox ID="car_license" runat="server" CssClass="form-control"></asp:TextBox>                        
                        <asp:HiddenField ID="Hid_assets_id" runat="server" />
                        <a id="carsel" href="assets1_9_carsel.aspx?car_license=<%= car_license.ClientID %>&Hid_assets_id=<%=Hid_assets_id.ClientID%>&driver=<%=driver.ClientID%>" class="fancybox fancybox.iframe "><span class="btn btn-link small  ">...</span></a>
                    </div>

                    <div class="form-group col-lg-4  form-inline">
                        <label for="price">發包價</label>
                        <asp:TextBox ID="price" runat="server" CssClass="form-control" onkeyup="this.value=this.value.replace(/[^0-9.]/g,'')"></asp:TextBox>
                    </div>

                    <div class="form-group col-lg-4  form-inline">
                        <label for="memo">備　　註</label>
                        <asp:TextBox ID="memo" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btn_save" runat="server" Text="儲存" CssClass="templatemo-blue-button" ValidationGroup="validate" OnClick="btn_save_Click" />
                    <asp:Button ID="btn_cancel" runat="server" Text="取消" CssClass="templatemo-white-button" OnClick="btn_cancel_Click" />

                </div>

            </asp:Panel>

        </div>
    </div>
</asp:Content>

