﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.XSSF;
using System.Text;
using System.Collections;
using OfficeOpenXml;

public partial class LTPriceUpload : System.Web.UI.Page
{

    /// <summary>Excel匯入DB-運價檔案 </summary>
    protected void btImport_Click(object sender, EventArgs e)
    {


        //string business = dlbusiness.SelectedValue;
        List<int> ttSizeList = new List<int>();
        SortedList ttCbmSizeList = new SortedList();
        string user = "";
        if (Session["account_code"] != null) user = Session["account_code"].ToString();

        #region 材積大小
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select id, CbmID, CbmSize from tcCbmSize with(nolock)  ";
            DataTable dtb = dbAdapter.getDataTable(cmd);
            if (dtb != null && dtb.Rows.Count > 0)
            {
                for (int i = 0; i <= dtb.Rows.Count - 1; i++)
                {
                    string ID = dtb.Rows[i]["CbmID"].ToString();
                    string Size = dtb.Rows[i]["CbmSize"].ToString();
                    if (ttCbmSizeList.IndexOfKey(Size) == -1)
                    {
                        ttCbmSizeList.Add(Size, ID);
                    }
                }
            }
        }
        #endregion


        lbMsg.Items.Clear();
        if (file02.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            //string ttFileName = ttPath + file02.PostedFile.FileName;
            string ttFileName = ttPath + file02.FileName;
            int chkdate = 1;
            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            if ((ttExtName == ".xls"))
            {
                string tablename = "tbPriceLessThan";
                string ttErrStr = "";
                string start_city = "";
                string end_city = "";
                string Cbmsize = "";
                int CbmID = 0;
                string first_price = "";
                string add_price = "";
                int _first_price = 0;
                int _add_price = 0;


                file02.PostedFile.SaveAs(ttFileName);
                HSSFWorkbook workbook = null;
                HSSFSheet u_sheet = null;
                string ttSelectSheetName = "";
                workbook = new HSSFWorkbook(file02.FileContent);


                try
                {
                    for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                    {
                        u_sheet = (HSSFSheet)workbook.GetSheetAt(x);
                        ttSelectSheetName = workbook.GetSheetName(x);

                        if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                        {
                            Boolean IsSheetOk = true;//工作表驗證
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                            //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                            string strRow, strInsCol, strInsVal;
                            List<StringBuilder> sb_del_list = new List<StringBuilder>();
                            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                            StringBuilder sb_temp_del = new StringBuilder();
                            StringBuilder sb_temp_ins = new StringBuilder();

                            HSSFRow Row_title = new HSSFRow();
                            if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                            string a = "";

                            for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                            {
                                #region 初始化

                                HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列   
                                if (Row.GetCell(0) == null || Row.GetCell(0).ToString() == "")
                                {
                                    break;

                                }
                                start_city = "";
                                end_city = "";
                                Cbmsize = "";
                                CbmID = 0;
                                first_price = "";
                                add_price = "";
                                _first_price = 0;
                                _add_price = 0;



                                strInsCol = "";
                                strInsVal = "";
                                strRow = (i + 1).ToString();

                                #endregion

                                #region 驗證&取值 IsSheetOk                                

                                Boolean IsChkOk = true;



                                if (Row.GetCell(0) != null) start_city = Row.GetCell(0).ToString();
                                if (String.IsNullOrEmpty(start_city))
                                {
                                    IsChkOk = false;
                                    IsSheetOk = false;

                                }
                                start_city = start_city.Replace("台", "臺");


                                if (Row.GetCell(1) != null) end_city = Row.GetCell(1).ToString();
                                if (String.IsNullOrEmpty(end_city))
                                {
                                    IsChkOk = false;
                                    IsSheetOk = false;

                                }
                                end_city = end_city.Replace("台", "臺");


                                if (Row.GetCell(2) != null) Cbmsize = Row.GetCell(2).ToString();
                                if (String.IsNullOrEmpty(Cbmsize))
                                {
                                    IsChkOk = false;
                                    IsSheetOk = false;

                                }

                                for (int z = 1; z <= 4; z++)
                                {
                                    if (Row.GetCell(3 * z) != null) first_price = Row.GetCell(3 * z).ToString().Replace(",", "");
                                    if (String.IsNullOrEmpty(first_price) || !IsNumeric(first_price))
                                    {
                                        IsChkOk = false;
                                        IsSheetOk = false;

                                    }
                                    else
                                    {
                                        int.TryParse(first_price, out _first_price);
                                    }

                                    if (Row.GetCell(3 * z + 1) != null) add_price = Row.GetCell(3 * z + 1).ToString().Trim().Replace(",", "");
                                    if (String.IsNullOrEmpty(add_price) || !IsNumeric(add_price))
                                    {

                                        IsChkOk = false;
                                        IsSheetOk = false;

                                    }
                                    else
                                    {
                                        int.TryParse(add_price, out _add_price);
                                    }



                                    //每100筆組成一字串

                                    if (Row_title.Cells.Count != 14)
                                    {
                                        IsSheetOk = false;
                                        IsChkOk = false;
                                    }


                                    if (IsSheetOk)
                                    {

                                        SqlCommand cmda = new SqlCommand();
                                        DataTable dta = new DataTable();
                                        cmda.Parameters.AddWithValue("@start_city", start_city);
                                        cmda.Parameters.AddWithValue("@end_city", end_city);
                                        cmda.Parameters.AddWithValue("@Cbmsize", z);
                                        cmda.Parameters.AddWithValue("@Enable_date", Enable_date.Text);
                                        cmda.CommandText = "Select id from tbPriceLessThan With(Nolock) where start_city =@start_city  AND  end_city=@end_city AND Cbmsize=@Cbmsize AND Enable_date > @Enable_date and Enable_date Is Not NULL and   (customer_code IS NULL)";
                                        dta = dbAdapter.getDataTable(cmda);

                                        if (dta.Rows.Count > 0)
                                        {
                                            RetuenMsg("啟用日期必須大於現有啟用日期");
                                            chkdate = 0;
                                            break;
                                        }
                                        else
                                        {
                                            using (SqlCommand cmd = new SqlCommand())
                                            {
                                                cmd.Parameters.AddWithValue("@start_city", start_city);
                                                cmd.Parameters.AddWithValue("@end_city", end_city);
                                                cmd.Parameters.AddWithValue("@Cbmsize", z);
                                                cmd.Parameters.AddWithValue("@first_price", _first_price);
                                                cmd.Parameters.AddWithValue("@add_price", _add_price);
                                                //cmd.Parameters.AddWithValue("@business", "");
                                                cmd.Parameters.AddWithValue("@cuser", user);
                                                cmd.Parameters.AddWithValue("@Enable_date", Enable_date.Text.ToString());
                                                cmd.CommandText = dbAdapter.genInsertComm("tbPriceLessThan", true, cmd);
                                                int result = 0;
                                                if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                                            }
                                        }
                                    }

                                }

                                #endregion

                                #region 組新增


                                #endregion
                            }

                            //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳




                            if (IsSheetOk)
                            {
                                #region 執行SQL

                                //刪除
                                //foreach (StringBuilder sb in sb_del_list)
                                //{
                                //    if (sb.Length > 0)
                                //    {
                                //        String strSQL = sb.ToString();
                                //        using (SqlCommand cmd = new SqlCommand())
                                //        {
                                //            cmd.CommandText = strSQL;
                                //            dbAdapter.execNonQuery(cmd);
                                //        }
                                //    }
                                //}

                                //新增

                                #endregion
                                if (chkdate == 0)
                                {
                                    lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】啟用日期異常!");
                                }
                                else
                                {
                                    lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                }
                            }
                            else
                            {
                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                            }
                            lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                        }

                    }
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    System.IO.File.Delete(ttPath + file02.PostedFile.FileName);
                }
                lbMsg.Items.Add("匯入完畢");
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "setTimeout('parent.$.fancybox.close()', 50000); parent.location.reload(true);", true);
            }
            else if (ttExtName == ".xlsx")
            {
                Boolean IsSheetOk = true;//工作表驗證
                DataTable dtS = new DataTable();

                using (ExcelPackage workbook = new ExcelPackage(file02.FileContent))
                {
                    foreach (ExcelWorksheet u_sheet in workbook.Workbook.Worksheets)
                    {
                        string ttSelectSheetName = u_sheet.Name;
                        int row = u_sheet.Dimension.End.Row;
                        int col = u_sheet.Dimension.End.Column;
                        object[,] valueArray = u_sheet.Cells.GetValue<object[,]>();
                        string first_price = "";
                        string add_price = "";
                        string[] Row_title = new string[col];
                        //foreach (object[,] _this in u_sheet.Cells.GetValue<object[,]>())
                        if (row < 2)
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName + "】欄位資訊有誤，請重新確認!");
                            continue;
                        }
                        else
                        {
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
                        }

                        for (int i = 0; i < col; i++) Row_title[i] = valueArray[0, i].ToString();
                        #region 定義 foreach sheet

                        string tablename = string.Empty;
                        string start_city = string.Empty;//起點區域 
                        string end_city = string.Empty;//迄點區域                    

                        //板數1 ~6
                        string plate1_price = string.Empty;
                        string plate2_price = string.Empty;
                        string plate3_price = string.Empty;
                        string plate4_price = string.Empty;
                        string plate5_price = string.Empty;
                        string plate6_price = string.Empty;
                        string strRow = string.Empty;
                        string strInsCol = string.Empty;
                        string strInsVal = string.Empty;

                        //寫入DB的資訊
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        #endregion
                        for (int j = 1; j < row; j++)
                        {
                            #region 初始化
                            tablename = "";
                            start_city = "";
                            end_city = "";
                            plate1_price = "";
                            plate2_price = "";
                            plate3_price = "";
                            plate4_price = "";
                            plate5_price = "";
                            plate6_price = "";
                            strRow = (j + 1).ToString();
                            #endregion




                            Boolean IsChkOk = true;
                            if (valueArray[j, 0] != null) start_city = valueArray[j, 0].ToString();
                            if (String.IsNullOrEmpty(start_city))
                            {
                                IsChkOk =
                                IsSheetOk = false;
                            }
                            start_city = start_city.Replace("台", "臺");

                            if (valueArray[j, 1] != null) end_city = valueArray[j, 1].ToString();
                            if (String.IsNullOrEmpty(end_city))
                            {
                                IsChkOk =
                                IsSheetOk = false;
                            }
                            end_city = end_city.Replace("台", "臺");

                            for (int z = 1; z <= 4; z++)
                            {
                                if (valueArray[j, z * 3] != null) first_price = valueArray[j, z * 3].ToString();
                                if (String.IsNullOrEmpty(first_price) || !Utility.IsNumeric(first_price))
                                {
                                    IsChkOk =
                                    IsSheetOk = false;
                                }

                                if (valueArray[j, z * 3 + 1] != null) add_price = valueArray[j, z * 3 + 1].ToString();
                                if (String.IsNullOrEmpty(add_price) || !Utility.IsNumeric(add_price))
                                {
                                    IsChkOk =
                                    IsSheetOk = false;
                                }


                                if (IsSheetOk)
                                {

                                    SqlCommand cmda = new SqlCommand();
                                    DataTable dta = new DataTable();
                                    cmda.Parameters.AddWithValue("@start_city", start_city);
                                    cmda.Parameters.AddWithValue("@end_city", end_city);
                                    cmda.Parameters.AddWithValue("@Cbmsize", z);
                                    cmda.Parameters.AddWithValue("@Enable_date", Enable_date.Text);
                                    cmda.CommandText = "Select id from tbPriceLessThan With(Nolock) where start_city =@start_city  AND  end_city=@end_city AND Cbmsize=@Cbmsize AND Enable_date > @Enable_date and Enable_date Is Not NULL and   (customer_code IS NULL)";
                                    dta = dbAdapter.getDataTable(cmda);

                                    if (dta.Rows.Count > 0)
                                    {
                                        RetuenMsg("啟用日期必須大於現有啟用日期");
                                        chkdate = 0;
                                        break;
                                    }


                                    //只有「自營公版」及 「自訂運價表」能匯到「自訂運價表 tbPriceBusinessDefine」

                                    using (SqlCommand cmdI = new SqlCommand())
                                    {
                                        cmdI.Parameters.Clear();
                                        cmdI.Parameters.AddWithValue("@start_city", start_city);
                                        cmdI.Parameters.AddWithValue("@end_city", end_city);
                                        cmdI.Parameters.AddWithValue("@Cbmsize", z);
                                        cmdI.Parameters.AddWithValue("@first_price", first_price);
                                        cmdI.Parameters.AddWithValue("@add_price", add_price);
                                        cmdI.Parameters.AddWithValue("@cuser", user);
                                        cmdI.Parameters.AddWithValue("@Enable_date", Enable_date.Text.ToString());
                                        cmdI.CommandText = dbAdapter.genInsertComm("tbPriceLessThan", true, cmdI);
                                        int result = 0;
                                        if (!int.TryParse(dbAdapter.getDataTable(cmdI).Rows[0][0].ToString(), out result)) result = 0;
                                    }


                                }



                            }
                        }
                        if (IsSheetOk)
                        {

                            if (chkdate == 0)
                            {
                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】啟用日期異常!");
                            }
                            else
                            {
                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                            }
                        }
                        else
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");

                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                    }
                }
            } 


        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    if (!IsPostBack)
    //    {
    //        if (Request.QueryString["fee_type"] != null) dlbusiness.SelectedValue = Request.QueryString["fee_type"].ToString();
    //    }
    //}


    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }
}
