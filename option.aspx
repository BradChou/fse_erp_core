﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="option.aspx.cs" Inherits="option" %>

<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-託運服務</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">
    <!-- JS -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <style type="text/css">
        .auto-style1 {
            text-align: center;
            font-size: xx-large;
            color: #289aa1;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
         <!-- Left column -->
   <div class="templatemo-flex-row">
        <div id="mySidenav" class="templatemo-sidebar">
            <header class="templatemo-site-header">
                <img src="images/logo.png" alt="logo" width="200">
            </header>
            <div class="profile-photo-container">
                <img src="images/profile-photo.png" alt="Profile Photo" class="img-responsive">
                <div class="profile-photo-overlay"></div>
            </div>
            <div class="login">
                <p>
                    <i class="fa fa-user"></i><asp:Literal ID="UserName" runat="server"></asp:Literal> 您好! <a href="log_out.aspx" class="btn btn-danger pull-right">登出</a>
                </p>
            </div>
            <nav class="sidebar-collapse templatemo-left-nav">
                <ul class="nav" >
                    <asp:Literal ID="useritemlist" runat="server"></asp:Literal>
                </ul>
            </nav>
        </div>

       
        <!-- Main content -->
        <div id="mainp" class="templatemo-content col-1 light-gray-bg">
            <div class="templatemo-top-nav-container">
                
            </div>
            <div class="templatemo-content-container">
               
                <!-- 內容 -->
                <div class="templatemo-content-widget white-bg">
                    <div>
            <p class="auto-style1">
                歡迎光臨峻富雲端物流管理系統
            </p>
            <br />
            <br />
            <div class="row">
                <div class="col-lg-6 text-right">
                    <strong>
                        <a class="btn btn-success  btn-lg " style="font-size: 60pt; font-weight: bold" href="index.aspx?type=0">棧板運輸 </a>

                    </strong>
                </div>
                <div class="col-lg-6">
                    <strong>
                        <a class="btn btn-info  btn-lg " style="font-size: 60pt; font-weight: bold" href="Lt_index.aspx?type=1">零擔運輸 </a>
                    </strong>
                </div>
            </div>

        </div>
                    
                </div>
            </div>
        </div>
    </div>

    

        
    </form>
</body>
</html>
