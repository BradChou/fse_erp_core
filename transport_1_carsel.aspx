﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="transport_1_carsel.aspx.cs" Inherits="transport_1_carsel" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請選擇棧板種類</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th>選取</th>
                        <th class="_th">棧板種類</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand ="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="選取">
                                            <asp:HiddenField ID="Hid_Pallet_type" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_id").ToString())%>' />
                                            <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                        </td>
                                         <td data-th="棧板種類">
                                             <asp:Literal ID="Pallet_type_text" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%>'></asp:Literal></td>                                      
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="8" style="text-align: center">查無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
