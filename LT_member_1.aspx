﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterMember.master" AutoEventWireup="true" CodeFile="LT_member_1.aspx.cs" Inherits="LT_member_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#hlFileAdd").fancybox({
                'width': 980,
                'height': 600,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });

            $("#hlFeeAdd").click(function () {
                var _link = "feeupload.aspx?";
                _link = _link + "customer_code=" + $(".cus_code1").val() + $(".cus_code2").val();
                _link = _link + "&pricing_code=" + $(".pricing_code").html();
                _link = _link + "&cus=" + $("._cus_id").html();
                _link = _link + "&ctlid=" + <%= btnQry_view2.ClientID%>.id;
                $(this).prop("href", _link);
            });

            $("#<%= ticket_period.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '-1':
                        $("#<%= ticket.ClientID%>").show();
                        $("#<%= ticket.ClientID%>").focus();
                        break;
                    default:
                        $("#<%= ticket.ClientID%>").hide();
                        break;

                }
            });

            $("#<%= close_date.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '-1':
                        $("#<%= tbclose.ClientID%>").show();
                        $("#<%= tbclose.ClientID%>").focus();
                        break;
                    default:
                        $("#<%= tbclose.ClientID%>").hide();
                        $("#<%= rv1.ClientID%>").html('');
                        $("#<%= tbclose.ClientID%>").val('1');
                        break;
                }
            });

            $("#<%= individual_fee.ClientID%>").change(function () {
                if ($(this).is(":checked")) {
                    $("#<%= pan_price_list.ClientID%>").hide();

                } else {
                    $("#<%= pan_price_list.ClientID%>").show();
                }
            });

        });
        $.fancybox.update();



        function price_del(e) {
            var the_del = $("input[name=del_" + e);
            var the_t = the_del.closest("tr").find(".p_date").html();
            var ischk = confirm("確定刪除" + the_t + "價格資訊? ");
            if (ischk) {
                $.ajax({
                    type: "POST",
                    url: "member_3-edit.aspx/PriceDel",
                    data: '{pid: "' + e + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    urlasync: false,//同步               
                    failure: function (response) {
                        alert(response.d);
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d == '1') {
                        the_del.closest('tr').remove();
                        alert("刪除成功");
                    } else {
                        alert(data.d);
                        location.reload();
                    }
                });
            }
        }
        function price_dw(e, c) {
            var cus_code = $(".cus_code1").val() + $(".cus_code2").val();
            var the_dw = $("input[name=dw_" + e);
            var the_t = the_dw.closest("tr").find(".p_date").html();
            var url_dw = "";
            var the_e = TryParseInt(e, 0);
            if (the_e == 0) {
                url_dw = "GetDownload.aspx?cus=" + $("._cus_id").html() + "&dw=1&pub=1&cus_c=" + cus_code;
            } else {
                url_dw = "GetDownload.aspx?cus=" + c + "&dw=1&pub=0&cus_c=" + cus_code + "&atdate=" + the_t;
            }

            $.fancybox({
                'width': '40%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': url_dw
            });
            //setTimeout("parent.$.fancybox.close()", 2000);
        }

        function TryParseInt(str, defaultValue) {
            var retValue = defaultValue;
            if (str != null) {
                if (str.toString().length > 0) {
                    if (!isNaN(str)) {
                        retValue = parseInt(str);
                    }
                }
            }
            return retValue;
        }

    </script>
    <style>
        .enabledinput {
            pointer-events: all;
        }

        .disabledinput {
            pointer-events: none;
        }

        ._ddl_city, ._ddl_area, ._ddl_stop {
            min-width: 85px;
        }

        .hide {
            display: none;
        }

        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .span_tip {
            margin-left: 15px;
            color: red;
        }


        .tb_price_log {
            width: 40%;
            margin: 10px 0px;
            border: 1px solid #ddd;
        }

            .tb_price_log th {
                background-color: #39ADB4;
                color: white;
                text-align: center;
                padding: 5px;
            }

            .tb_price_log td {
                text-align: center;
                padding: 5px;
            }

        .bk_color1 {
            background-color: #f9f9f9;
        }

        .tb_price_th {
            width: 15%;
        }

        .p_date {
            width: 27%;
        }

        .p_type {
            width: 23%;
        }

        .p_set {
            width: 35%;
        }

        .p_td_null {
            text-align: center;
            padding: 10px;
        }

        ._price_today {
            color: blue;
            font-weight: bold;
        }

        ._ship_title {
            font-size: 20px;
            margin-right: 30px;
        }

        ._cus_title {
            width: 13%;
        }

        ._cus_data {
            color: #8a9092;
        }

        .cus_main {
            height: 720px;
            overflow-y: auto;
            padding: 5px;
        }

        .row {
            margin: 0px;
        }

        .btn {
            margin: 0px 5px;
        }

        ._btn_area {
            margin-top: 10px;
        }

        .div_price_log {
            max-height: 210px;
            overflow-y: auto;
        }

        .tb_price_log tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

        .scrollbar::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar {
            width: 12px;
            background-color: #F5F5F5;
        }

        .scrollbar::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">新增主客戶</h2>
            <!-- 流程 -->
            <ul class="wizard">
                <li id="li_01" class="current">填寫基本資料</li>
                <%--<li id="li_02">設定運費</li>
                <li id="li_03">完成</li>--%>
            </ul>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <div class="templatemo-login-form">
                        <div class="form-group form-inline">
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control"></asp:DropDownList>
                            <asp:DropDownList ID="PM_code" runat="server" CssClass="form-control" OnSelectedIndexChanged="PM_code_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="PM_code" ForeColor="Red" ValidationGroup="validate">請選擇計價模式</asp:RequiredFieldValidator>
                        </div>
                        <div id="customer_body" class="panel panel-default ">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6 form-group form-inline">
                                        <label>客戶代號</label>
                                        <asp:TextBox ID="master_code" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="second_code" runat="server" Enabled="false"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label>站所</label>
                                        <asp:Label ID="supplier_name" runat="server" CssClass="_cus_data"></asp:Label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="customer_name" class="_cus_title _tip_important">客戶名稱</label>
                                        <asp:TextBox ID="customer_name" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="customer_name" ForeColor="Red" ValidationGroup="validate">請輸入客戶名稱</asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">簡　　　稱</label>
                                        <asp:TextBox ID="shortname" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="shortname" ForeColor="Red" ValidationGroup="validate">請輸入簡稱</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">統一編號</label>
                                        <asp:TextBox ID="uni_number" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="8" placeholder="ex: 12345678"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="uni_number" ForeColor="Red" ValidationGroup="validate">請輸入統一編號</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reg_uni_number_type" runat="server" ErrorMessage="統一編號格式有誤" ValidationExpression="(^\d{8}$)"
                                    ControlToValidate="uni_number" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                                </asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">對　帳　人</label>
                                        <asp:TextBox ID="principal" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="principal" ForeColor="Red" ValidationGroup="validate">請輸入對帳人</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">電　　話</label>
                                        <asp:TextBox ID="tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="tel" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title ">傳　　　真</label>
                                        <asp:TextBox ID="fax" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <label for="inputLastName" class="_cus_title _tip_important">出貨地址</label>
                                                <asp:DropDownList ID="City" runat="server" CssClass="form-control _ddl_city" AutoPostBack="true" OnSelectedIndexChanged="City_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:DropDownList ID="CityArea" runat="server" CssClass="form-control _ddl_area" AutoPostBack="true"></asp:DropDownList>
                                                <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="City" ForeColor="Red" ValidationGroup="validate">請選擇縣市</asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="CityArea" ForeColor="Red" ValidationGroup="validate">請選擇鄉鎮區</asp:RequiredFieldValidator>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">營業人員</label>
                                        <asp:TextBox ID="business_people" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label class="_cus_title"></label>
                                        <asp:TextBox ID="address" CssClass="form-control　_addr" Width="45%" runat="server" placeholder="請輸入地址" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req8" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                                    </div>
<%--                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" >計績營業員</label>
                                        <asp:DropDownList ID="sales_id" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" >計績站所</label>
                                        <asp:DropDownList ID="sale_station" CssClass="form-control" runat="server" Enabled="false"></asp:DropDownList>
                                    </div>--%>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputFirstName" class="_cus_title _tip_important">E-mail　</label>
                                        <asp:TextBox ID="email" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req9" runat="server" ControlToValidate="email" ForeColor="Red" ValidationGroup="validate">請輸入e-mail</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="email" Display="Dynamic" ForeColor="Red">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputFirstName" class="_cus_title _tip_important">票　　期</label>
                                        <asp:DropDownList ID="ticket_period" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="">請選擇</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>15</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>25</asp:ListItem>
                                            <asp:ListItem>30</asp:ListItem>
                                            <asp:ListItem Value="-1">其他</asp:ListItem>
                                        </asp:DropDownList><asp:TextBox ID="ticket" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入票期天數"></asp:TextBox>
                                        <label for="inputFirstName" class="_cus_title">(天)</label>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req11" runat="server" ControlToValidate="ticket_period" ForeColor="Red" ValidationGroup="validate">請選擇票期</asp:RequiredFieldValidator>

                                    </div>
                                    

                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title">停運原因</label>
                                        <asp:DropDownList ID="stop_code" runat="server" CssClass="form-control _ddl_stop"></asp:DropDownList>
                                        <asp:TextBox ID="stop_memo" CssClass="form-control" runat="server" placeholder="其他說明"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">計價模式</label>
                                        <asp:DropDownList ID="SPM_code" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <label for="invoiceAddress" class="_cus_title">發票地址</label>
                                                <asp:DropDownList ID="ddInvoiceCity" runat="server" CssClass="form-control _ddl_city" AutoPostBack="true" OnSelectedIndexChanged="InvoiceCity_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:DropDownList ID="ddInvoiceArea" runat="server" CssClass="form-control _ddl_area" AutoPostBack="true"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label class="_tip_important">商品類型</label>
                                        <asp:DropDownList ID="product_type" runat="server" CssClass="form-control _ddl_stop" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req20" runat="server" ControlToValidate="product_type" ForeColor="Red" ValidationGroup="validate">請輸入商品類型</asp:RequiredFieldValidator>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label class="_cus_title"></label>
                                        <asp:TextBox ID="txtInvoiceRoad" CssClass="form-control　_addr" Width="45%" runat="server" placeholder="請輸入地址" MaxLength="50"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <label class="_cus_title">SD/MD</label>
                                                <asp:DropDownList ID="ddSD" runat="server" CssClass="form-control _ddl_area" AutoPostBack="true"></asp:DropDownList>
                                                <asp:DropDownList ID="ddMD" runat="server" CssClass="form-control _ddl_area" AutoPostBack="true"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label >選擇主客代</label>
                                        <asp:DropDownList ID="ddlMasterCustomerCode" runat="server"  CssClass="form-control _ddl chosen-select">
                                        </asp:DropDownList>
                                        
                                    </div>                   
                                <%--</div>--%>
<%--                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label class="_cus_title"></label>
                                        <asp:TextBox ID="txtInvoiceRoad" CssClass="form-control　_addr" Width="45%" runat="server" placeholder="請輸入地址" MaxLength="50"></asp:TextBox>
                                    </div>--%>
                                    
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="is_new_customer"  runat="server" Text="是否為 2022/5/1後 新合約客戶" Enabled="false" />
                                        </span>
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="OldCustomerOverCBM"  runat="server" Text="是否為 舊制超材費" Enabled="false" />
                                        </span>
                                         </div>
                                                     <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="chkMasterCustomerCode"  runat="server" Text="是否本身為主客代"  />
                                            <asp:Label ID="lbMasterCustomerCode" Text="  請擇一填寫"  runat="server" ForeColor="Red" Visible="false"></asp:Label>
                                        </span>
                                         </div>
                                       
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="is_weekend_delivered" Style="display: none" runat="server" Text="啟用假日配送" />
                                        </span>
                                        <span>
                                            <asp:TextBox ID="weekend_delivery_fee" CssClass="form-control" runat="server" Style="display: none" placeholder="輸入假日配送加價費用" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName">銀　　行</label>
                                    <asp:DropDownList ID="bank" runat="server" CssClass="form-control _ddl chosen-select" AutoPostBack="true"></asp:DropDownList>

                                </div>

                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label for="inputLastName" class="_cus_title ">分　　　行</label>

                                    <asp:TextBox ID="bank_branch" CssClass="form-control　_bank" Width="45%" runat="server" placeholder="例如:南港分行，不需填銀行全名" MaxLength="15"></asp:TextBox>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title ">帳　　號</label>
                                        <asp:TextBox ID="bank_account" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="14" Width="45%" placeholder="不需填銀行代碼與特殊符號，例如' - '"></asp:TextBox>

                                    </div>

                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName">合約生效日</label>
                                        <asp:TextBox ID="contract_sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName">到　期　日</label>
                                        <asp:TextBox ID="contract_edate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName">合約內容</label>
                                        <%--<button class="btn btn-darkblue"><span class="fa fa-file-text" aria-hidden="true"></span> 合約內容</button>--%>
                                        <%--  <button type="submit" class="btn btn-primary">上傳新合約</button>--%>
                                        <div style="display: none">
                                            <asp:TextBox ID="filename" runat="server" />
                                        </div>
                                        <a href="#" id="hlFileView" class="fa fa-file-text" aria-hidden="true">合約內容</a>
                                        <a href="fileupload.aspx?filename=<%=filename.ClientID %>&fileview=hlFileView" class="fancybox fancybox.iframe" id="hlFileAdd"><span class="btn btn-primary">上傳新合約</span></a>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title _tip_important">結　帳　日</label>
                                        <asp:DropDownList ID="close_date" runat="server" class="form-control">
                                            <asp:ListItem Value="">請選擇</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="15">15</asp:ListItem>
                                            <asp:ListItem Value="20">20</asp:ListItem>
                                            <asp:ListItem Value="25">25</asp:ListItem>
                                            <asp:ListItem Value="31">(31)30</asp:ListItem>
                                            <asp:ListItem Value="-1">其他</asp:ListItem>
                                        </asp:DropDownList><asp:TextBox ID="tbclose" CssClass="form-control" runat="server" Style="display: none" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="3" placeholder="請輸入結帳日"></asp:TextBox>
                                        <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="tbclose" ErrorMessage="結帳日需需介於 1~31" ForeColor="Red" MaximumValue="31" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req12" runat="server" ControlToValidate="address" ForeColor="Red" ValidationGroup="validate">請選擇結帳日</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                        <%--<label for="inputLastName">運價生效日</label>--%>
                                        <%--<asp:TextBox ID="fee_sdate" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>--%>

                                        <label for="contract_price" class="_cus_title ">發包價</label>
                                        <asp:TextBox ID="contract_price" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-6 col-md-6 form-group form-inline">
                                        <label for="billing_special_needs">帳單特殊需求</label>
                                        <asp:TextBox ID="billing_special_needs" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>

                                <div id="setFee" runat="server" visible="false" class="templatemo-login-form">
                                    <hr>

                                    <ul class="wizard">
                                        <li class="current" id="li_02">設定運費</li>
                                    </ul>
                                    <div class=" form-group form-inline">
                                        <span class="margin-bottom-10 _ship_title">客代編號:</span>
                                        <asp:TextBox ID="tbmaster_code" runat="server" Enabled="false" CssClass="cus_code1"></asp:TextBox>
                                        <asp:TextBox ID="tbsecond_code" runat="server" Enabled="false" CssClass="cus_code2"></asp:TextBox>
                                        <div style="display: none">
                                            <asp:Button ID="btnQry_view2" runat="server" CssClass="templatemo-blue-button" Text="查 詢" OnClick="btnQry_view2_Click" />
                                        </div>
                                        <asp:Label ID="lb_cusid" CssClass="_cus_id hide" runat="server"></asp:Label>
                                        <asp:Label ID="lb_pricing_code" CssClass="pricing_code hide" runat="server"></asp:Label>
                                        <asp:Label ID="lbcustomer_type" CssClass=" hide" runat="server"></asp:Label>
                                    </div>
                                    <div class=" form-group form-inline">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="individual_fee" runat="server" Text="獨立計價(不採運價表計算託運單價格)" />
                                        </span>
                                    </div>
                                    <div class=" col-lg-6 col-md-6 form-group form-inline">
                                        <label for="inputLastName" class="_cus_title ">發包價</label>
                                        <asp:TextBox ID="Contract_rice" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                    </div>
                                    <asp:Panel runat="server" ID="pan_price_list">
                                        <hr>
                                        <asp:Label ID="fee_sdate" runat="server" class="_fee_sdate _cus_data" Text="※目前採用公版運價"></asp:Label>
                                        <asp:Label ID="lbl_PriceTip" runat="server" CssClass="text-primary span_tip"></asp:Label>
                                        <hr>
                                        <span class="margin-bottom-10 _ship_title">運價表設定</span>
                                        <input name="dw_0" type="button" class="btn btn-primary" value="下載公版運價" onclick='price_dw(0, 0)' />

                                        <a class="fancybox fancybox.iframe" id="hlFeeAdd">
                                            <span class="btn btn-primary">上傳新運價</span>
                                        </a>
                                    </asp:Panel>
                                    <hr>
                                </div>

                                <%--<div class="row">
                                    <div class="col-lg-12 form-group form-inline">
                                        <label for="inputLastName">運價表　</label>
                                        <div style="display: none">
                                            <asp:TextBox ID="feename" runat="server" />
                                        </div>
                                        <a href="#" id="hlFeeView" class="fa fa-file-text" aria-hidden="true">運價表　</a>
                                        <a href="Feesetting.aspx" class="fancybox fancybox.iframe" id="hlFeeDefault"><span class="btn btn-primary">採公版運價</span></a>
                                        <a href="feeupload.aspx?filename=<%=feename.ClientID %>&fileview=hlFeeView" class="fancybox fancybox.iframe" id="hlFeeAdd"><span class="btn btn-primary">上傳新運價</span></a>
                                        <span class="text-primary">※可選擇採用公版運價</span>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                        <%--     <div class="form-group text-center">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取 消" />
                    </div>--%>
                        <div class="form-group text-center">
                            <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="前往設定運價" ValidationGroup="validate" OnClick="btnsave_Click" />
                            <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取 消" />
                        </div>
                    </div>
                    <div runat="server" id="FinalCheck" visible="false" class="form-group text-center">
                        <asp:Button ID="Button4" CssClass="templatemo-blue-button" runat="server" Text="確認新增" ValidationGroup="validate" OnClick="Button1_Click" />
                        <asp:Button ID="Button5" CssClass="templatemo-white-button" runat="server" Text="取 消" />
                    </div>
                </asp:View>
                <%-- <asp:View ID="View2" runat="server">
                    <div class="templatemo-login-form">
                        <div class="form-group text-center">
                            <asp:Button ID="Button1" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate" OnClick="Button1_Click" />
                            <asp:Button ID="Button2" CssClass="templatemo-white-button" runat="server" Text="取 消" />
                        </div>
                        <div class=" form-group form-inline">
                            <hr>
                            <span class="margin-bottom-10 _ship_title">客代編號:</span>
                            <asp:TextBox ID="tbmaster_code" runat="server" Enabled="false" CssClass="cus_code1"></asp:TextBox>
                            <asp:TextBox ID="tbsecond_code" runat="server" Enabled="false" CssClass="cus_code2"></asp:TextBox>
                            <div style="display: none">
                                <asp:Button ID="btnQry_view2" runat="server" CssClass="templatemo-blue-button" Text="查 詢" OnClick="btnQry_view2_Click" />
                            </div>
                            <asp:Label ID="lb_cusid" CssClass="_cus_id hide" runat="server"></asp:Label>
                            <asp:Label ID="lb_pricing_code" CssClass="pricing_code hide" runat="server"></asp:Label>
                            <asp:Label ID="lbcustomer_type" CssClass=" hide" runat="server"></asp:Label>
                        </div>
                        <div class=" form-group form-inline">
                            <span class="checkbox checkbox-success">
                                <asp:CheckBox ID="individual_fee" runat="server" Text="獨立計價(不採運價表計算託運單價格)" />
                            </span>
                        </div>
                        <div class=" col-lg-6 col-md-6 form-group form-inline">
                            <label for="inputLastName" class="_cus_title ">發包價</label>
                            <asp:TextBox ID="Contract_rice" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                        </div>
                        <asp:Panel runat="server" ID="pan_price_list">
                            <hr>
                            <asp:Label ID="fee_sdate" runat="server" class="_fee_sdate _cus_data" Text="※目前採用公版運價"></asp:Label>
                            <asp:Label ID="lbl_PriceTip" runat="server" CssClass="text-primary span_tip"></asp:Label>
                            <hr>
                            <span class="margin-bottom-10 _ship_title">運價表設定</span>
                            <input name="dw_0" type="button" class="btn btn-primary" value="下載公版運價" onclick='price_dw(0, 0)' />

                            <a class="fancybox fancybox.iframe" id="hlFeeAdd">
                                <span class="btn btn-primary">上傳新運價</span>
                            </a>
                        </asp:Panel>

                    </div>--%>
                </asp:View>
        <asp:View ID="View3" runat="server">
            <div class="templatemo-login-form">
                <div class="panel panel-default ">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 form-group form-inline">
                                <label>客戶代號：</label>
                                <asp:Label ID="lbcustomer_code" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label>站所：</label>
                                <asp:Label ID="lsupplier_name" runat="server" CssClass="_cus_data"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="customer_name" class="_cus_title _tip_important">客戶名稱：</label>
                                <asp:Label ID="lbcustomer_name" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label class="_cus_title _tip_important">簡　　　稱：</label>
                                <asp:Label ID="lbshortname" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">統一編號：</label>
                                <asp:Label ID="lbuni_number" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">對　帳　人：</label>
                                <asp:Label ID="lbprincipal" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">電　　話：</label>
                                <asp:Label ID="lbtel" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title ">傳　　真：</label>
                                <asp:Label ID="lbfax" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <label for="inputLastName" class="_cus_title _tip_important">出貨地址：</label>
                                        <asp:Label ID="lbCity" runat="server" Text="Label"></asp:Label>
                                        <asp:Label ID="lbCityArea" runat="server" Text="Label"></asp:Label>
                                        <asp:Label ID="lbaddress" runat="server" Text="Label"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">營業人員：</label>
                                <asp:Label ID="lbbusiness_people" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">E-mail：</label>
                                <asp:Label ID="lbemial" runat="server"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputFirstName" class="_cus_title _tip_important">票　　期：</label>
                                <asp:Label ID="lbticket_period" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title">停運原因：</label>
                                <asp:Label ID="lbstop_code" runat="server"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">計價模式：</label>
                                <asp:Label ID="lbSPM_code" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">合約生效日：</label>
                                <asp:Label ID="lbcontract_sdate" runat="server"></asp:Label>

                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">到　期　日：</label>
                                <asp:Label ID="lbcontract_edate" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">合約內容：</label>
                                <a href="#" id="hlFileView2" class="fa fa-file-text" aria-hidden="true">合約內容：</a>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName" class="_cus_title _tip_important">結　帳　日：</label>
                                <asp:Label ID="lbclose_date" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                <label for="inputLastName">運費計價模式：</label>
                                <asp:Label ID="lbFee" runat="server"></asp:Label>
                            </div>
                            <div class="col-lg-6 col-md-6 form-group form-inline">
                                <label for="inputLastName">帳單特殊需求：</label>
                                <asp:Label ID="lbbilling_special_needs" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center">
                    <asp:Button ID="btnOK" CssClass="templatemo-blue-button" runat="server" Text="確 認" OnClick="btnOK_Click" />
                </div>
            </div>
        </asp:View>
            </asp:MultiView>

        </div>
    </div>
</asp:Content>

