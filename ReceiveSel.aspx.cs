﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ReceiveSel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            if (Request.QueryString["receiver_code"] != null)
            {
                receiver_code.Text = Request.QueryString["receiver_code"];
            }

            if (Request.QueryString["receiver_name"] != null)
            {
                receiver_name.Text = Request.QueryString["receiver_name"];
            }

            readdata();
        }
    }

    private void readdata()
    {

        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        if (receiver_code.Text != "")
        {
            cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
            strWhereCmd += " and A.receiver_code like '%'+@receiver_code+'%' ";
            querystring += "&receiver_code=" + receiver_code.Text;
        }

        if (receiver_name.Text != "")
        {
            cmd.Parameters.AddWithValue("@receiver_name", receiver_name.Text);
            strWhereCmd += " and A.receiver_name like '%'+@receiver_name+'%' ";
            querystring += "&receiver_name=" + receiver_name.Text;
        }
        #endregion

        cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A                                          
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;



        objPds.PageSize = 10;



        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.receiver_id=@receiver_id";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                string receiver_id = Convert.ToString(DT.Rows[0]["receiver_id"]);          
             
                //customer_code.Text = DT.Rows[0]["customer_code"].ToString().Trim();
                //receiver_code2.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                string name = DT.Rows[0]["receiver_name"].ToString().Trim();
                string tel1 = DT.Rows[0]["tel"].ToString().Trim();
                string ext = DT.Rows[0]["tel_ext"].ToString().Trim();
                string tel2= DT.Rows[0]["tel2"].ToString().Trim();
                string city= DT.Rows[0]["address_city"].ToString().Trim();
                string area = DT.Rows[0]["address_area"].ToString().Trim();
                string address = DT.Rows[0]["address_road"].ToString().Trim();
                //memo.Text = DT.Rows[0]["memo"].ToString().Trim();

              
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.$('#" + Request.QueryString["id"] + "').val('" + receiver_id + "');self.parent.$('#" + Request.QueryString["tel1"] + "').val('" + tel1 + "');" +
                                                 "self.parent.$('#" + Request.QueryString["ext"] + "').val('" + ext + "'); self.parent.$('#" + Request.QueryString["tel2"] + "').val('" + tel2 + "');self.parent.$('#" + Request.QueryString["name"] + "').val('" + name + "');"+
                                                 "self.parent.$('#" + Request.QueryString["city"] + "').val('" + city + "');self.parent.$('#" + Request.QueryString["area"] + "').val('" + area + "');self.parent.$('#" + Request.QueryString["address"] + "').val('" + address  + "'); parent.$.fancybox.close();</script>", false);
              
                return;
            }


         }
       
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (receiver_code.Text.ToString() != "")
        {
            querystring += "&receiver_code=" + receiver_code.Text.ToString();
        }
        if (receiver_name.Text.ToString() != "")
        {
            querystring += "&receiver_name=" + receiver_name.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/ReceiveSel.aspx?search=yes" + querystring));
    }
}