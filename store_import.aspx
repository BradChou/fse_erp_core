﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store_import.aspx.cs" Inherits="store_import" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
            });

            init();
            
        });

        function init() {
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: -7, temp
                defaultDate: (new Date())  //預設當日11
            });
        }

    </script>

    <style>
        ._th {
            text-align: center;
        }
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">整批匯入</h2>
            <div class="templatemo-login-form" >
                <!---->
                <div class="form-style-10">
                    <div class="section"><span>1</span>下載空白超商匯入表</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btndownload" class="templatemo-white-button" runat="server" Text="空白超商匯入表" OnClick="btndownload_Click" />
                        <p class="text-primary">※第一次使用請下載空白超商匯入表標準格式</p>
                        <p style="color:coral; font-size:large; font-weight:bold">※版本更新日期：2021/02/26，若您匯入版本相較於本日期為舊，請先下載空白託運單</p>
                    </div>

                    <div class="section"><span>2</span>上傳檔案</div>
                    <div class="inner-wrap ">
                        <table style="margin-bottom: 5px;">
                            <tr>
                                <asp:Label ID="Label4" runat="server" Text="請選擇交易日期　　"></asp:Label><asp:TextBox ID="date" runat="server"  CssClass="datepicker" autocomplete="off"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="date" ForeColor="Red" ValidationGroup="validate">請選擇交易日期</asp:RequiredFieldValidator>
                                <%--<br />
                                <asp:Label ID="Label2" runat="server" Text="請選擇客代"></asp:Label><asp:DropDownList ID="dlcustomer_code" runat="server"  CssClass="chosen-select" ></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇的客代</asp:RequiredFieldValidator>--%>
                                
                                <td><asp:Label ID="Label3" runat="server" Text="請選擇要匯入的檔案(.xls或.xlsx)"></asp:Label></td>
                                <td><asp:FileUpload ID="file01" runat="server" Width="300px" /></td>
                                <td><asp:Button ID="btImport" CssClass="templatemo-blue-button" runat="server" Text="確 認" ValidationGroup="validate"  OnClick="btImport_Click" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="file01" ErrorMessage="請選擇檔案" Display="Dynamic" ForeColor="Red" ValidationGroup="validate" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator1"
                                         ForeColor="Red"
                                        runat="server"
                                        ErrorMessage="格式錯誤!請選擇(.xls或.xlsx)檔案"
                                        ValidationExpression="^.+(.xls|.xlsx)$"
                                        ControlToValidate="file01" ValidationGroup="validate" Display="Dynamic"> </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                        <div>
                            <div style="overflow: auto; height: 200px; vertical-align: top;">
                                <asp:ListBox ID="lbMsg" runat="server" Height="200px" Width="100%"></asp:ListBox>
                            </div>
                        </div>
                    </div>

                    <div class="section"><span>3</span></div>
                    <div class="inner-wrap" style="overflow: auto; height: calc(40vh); width: 100%" >
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <p>※今日總筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>
                                <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">主線/爆量</th>
                                        <th class="_th">日/夜</th>
                                        <th class="_th">溫層</th>
                                        <th class="_th">車型(計價車型)</th>
                                        <th class="_th">店數</th>
                                        <th class="_th">場區</th>
                                        <th class="_th">公里</th>
                                        <th class="_th">請款對象</th>
                                        <th class="_th">路線</th>
                                        <th class="_th">車號</th>
                                        <th class="_th">司機</th>
                                        <th class="_th">車行</th>
                                        <th class="_th">供應商</th>
                                        <th class="_th">承攬價</th>
                                        <th class="_th">發包價</th>
                                    </tr>
                                    <asp:Repeater ID="New_List" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Items_Text").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DayNigh_text").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Temperature_Text").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cabin_type_text").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Stores").ToString())%></td>
                                                <td><%# Func.GetRow("code_name", "ttItemCodesFieldArea", "", "seq=@seq", "seq", Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Area").ToString()))%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.milage").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.RequestObject").ToString())%></td>
                                                <td><%# Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Route_name").ToString()) %></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.CarRetailerName").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.PaymentObject").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.income").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.expenses").ToString())%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="15" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="section"><span>上傳記錄</span></div>
                    <div class="inner-wrap" style="overflow: auto; height: calc(40vh); width: 100%">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="_th">交易日期</th>
                                <th class="_th">筆數</th>
                                <th class="_th">上傳人員</th>
                                <th class="_th">上傳時間</th>
                                <th class="_th">功能</th>
                            </tr>
                            <asp:Repeater ID="rep_IO" runat="server" OnItemCommand ="rep_IO_ItemCommand" >
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date", "{0:yyyy/MM/dd}").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.totalNum").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                        <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate", "{0:yyyy/MM/dd HH:mm:ss}").ToString())%></td>
                                        <td>
                                            <asp:Button ID="btn_del" runat="server" CssClass ="btn btn-danger " Text="刪除" CommandName ="cmdDel" OnClientClick ="return confirm('您確定要刪除此批記錄嗎?');" CommandArgument ='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.randomCode").ToString())%>' /></td>
                                        
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="5" style="text-align: center">本日無上傳資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                </div>
            </div>
           <%-- <hr>--%>
        </div>
    </div>
</asp:Content>

