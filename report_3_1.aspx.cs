﻿using BObject.Bobjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;

public partial class report_3_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            #region 配送站所
            if (Less_than_truckload == "1")
            {
                Suppliers.DataSource = Utility.getArea_Arrive_Code(true);
                Suppliers.DataValueField = "station_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else
            {   
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    //cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where 0=0  order by supplier_code");
                    //Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
                    //Suppliers.DataValueField = "supplier_code";
                    //Suppliers.DataTextField = "showname";
                    //Suppliers.DataBind();
                    string manager_type = Session["manager_type"].ToString(); //管理單位類別
                    string account_code = Session["account_code"].ToString(); //使用者帳號
                    string supplier_code = Session["master_code"].ToString();
                    Suppliers.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
                    Suppliers.DataValueField = "supplier_code";
                    Suppliers.DataTextField = "showname";
                    Suppliers.DataBind();

                    if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
                    if (manager_type == "0" || manager_type == "1" || manager_type == "2") Suppliers.Items.Insert(0, new ListItem("全部", ""));

                }
            }
            
            #endregion

            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            }

            string print_date = DateTime.Today.ToString("yyyy/MM/dd");
            

            if (Request.QueryString["sdate"] != null)
            {
                sdate.Text = Request.QueryString["sdate"];
            }
            else
            {
                sdate.Text = DateTime.Today.ToString("yyyy/MM/01");
            }

            if (Request.QueryString["edate"] != null)
            {
                edate.Text = Request.QueryString["edate"];
            }
            else
            {
                edate.Text = print_date;
            }

            if (Request.QueryString["kind"] != null)
            {
                kind.Text = Request.QueryString["kind"];
                switch (kind.Text)
                {
                    case "1":
                        lbTitle.Text = "已下單已到/未到著明細";
                        break;
                    case "2":
                        lbTitle.Text = "已下單已到著明細";
                        break;
                    case "3":
                        lbTitle.Text = "已下單未到著明細";
                        break;
                }
            }
            else
            {
                kind.Text = "3";
                lbTitle.Text = "已下單未到著明細";
            }
            readdata();
        }
    }

    private void readdata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 9999;
            string strWhereCmd = string.Empty;
            string querystring = string.Empty;
            cmd.Parameters.Clear();

            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            if (sdate.Text != "")
            {
                cmd.Parameters.AddWithValue("@sdate", sdate.Text );
                querystring += "&sdate=" + sdate.Text;
            }

            if (edate.Text != "")
            {
                cmd.Parameters.AddWithValue("@edate",  edate.Text );
                querystring += "&edate=" + edate.Text;
            }

            cmd.Parameters.AddWithValue("@Supplier_code", Suppliers.SelectedValue);
            querystring += "&Suppliers=" + Suppliers.SelectedValue;



            #endregion
            if (kind.Text == "1")       //已下單已到/未到著明細
            {
                cmd.CommandText = string.Format(@"DECLARE @tbl table
                                                (order_number nvarchar(50), scan_date datetime , rn int )
                                                INSERT INTO @tbl
                                                SELECT  distinct B.order_number  , A.scan_date,
	                                            ROW_NUMBER() OVER (PARTITION BY B.order_number ORDER BY B.order_number, A.scan_date ) AS rn
	                                            from  ttDeliveryScanLog A With(Nolock) 
	                                            LEFT JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number 
	                                            WHERE A.scan_date  >=DATEADD(MONTH,-3,GETDATE())
	                                            AND B.print_date  >=@sdate and  B.print_date < DATEADD(day,1 ,@edate ) 
	                                            AND B.area_arrive_code LIKE '%'+  ISNULL('','') +'%'
	                                            AND ISNULL(B.order_number,'') <> ''
                                                

                                                DECLARE @tb2 table 
	                                            (check_number nvarchar(20), scan_date datetime, rn int)
                                                INSERT INTO @tb2
	                                                SELECT distinct A.check_number, A.scan_date  ,
	                                                ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number, A.scan_date ) AS rn
	                                                from  ttDeliveryScanLog A With(Nolock) 
	                                                INNER JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number 
	                                                WHERE  B.print_date  >=@sdate and  B.print_date < DATEADD(day,1 ,@edate )   and A.scan_date  >=DATEADD(MONTH,-3,GETDATE())
                                                

                                                Select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO , CASE WHEN F.scan_date is not null THEN F.scan_date END as scan_date,
                                                   A.send_contact, Convert(VARCHAR, A.print_date,111) 'print_date'  , Convert(VARCHAR, A.arrive_assign_date,111) 'arrive_assign_date' , A.check_number , A.receive_customer_code, 
                                                   E.supplier_name 'area_arrive_station', A.receive_contact,
                                                   A.receive_city + A.receive_area+ CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else A.receive_address end 'receive_address',
                                                   CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END 'receive_tel',
                                                   ISNULL(A.arrive_to_pay_freight,0) 'arrive_to_pay_freight', ISNULL(A.collection_money,0) 'collection_money', 
                                                   A.pieces ,  A.cbm ,  CASE WHEN A.pricing_type = '01' then A.plates else 0 end 'plates',
                                                   CASE WHEN A.pricing_type = '04' then A.plates else 0 end 'splates'
                                                   from  tcDeliveryRequests A with(nolock)
                                                   LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                   LEFT JOIN @tb2 F on A.check_number  =  F.check_number  and F.rn= 1
	                                               LEFT JOIN @tbl G on A.order_number = G.order_number and G.rn= 1 
                                                where  A.cancel_date IS NULL and A.Less_than_truckload =@Less_than_truckload
                                                --and (CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@sdate,111) and  CONVERT(VARCHAR,A.print_date,111) <=CONVERT(VARCHAR,@edate,111))
                                                and A.print_date  >=@sdate and  A.print_date < DATEADD(day,1 ,@edate )  
                                                and A.area_arrive_code LIKE '%'+  ISNULL(@Supplier_code,'') +'%'
                                                
                                            ");
            }
            else if (kind.Text == "2")  //已下單已到著明細
            {
                cmd.CommandText = string.Format(@"DECLARE @tbl table
                                                (order_number nvarchar(50), scan_date datetime , rn int )
                                                INSERT INTO @tbl
                                                SELECT  distinct B.order_number  , A.scan_date,
	                                            ROW_NUMBER() OVER (PARTITION BY B.order_number ORDER BY B.order_number, A.scan_date ) AS rn
	                                            from  ttDeliveryScanLog A With(Nolock) 
	                                            LEFT JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number 
	                                            WHERE A.scan_date  >=DATEADD(MONTH,-3,GETDATE())
	                                            AND B.print_date  >=@sdate and  B.print_date < DATEADD(day,1 ,@edate ) 
	                                            AND B.area_arrive_code LIKE '%'+  ISNULL('','') +'%'
	                                            AND ISNULL(B.order_number,'') <> ''
                                                

                                                DECLARE @tb2 table 
	                                            (check_number nvarchar(20), scan_date datetime, rn int)
                                                INSERT INTO @tb2
	                                                SELECT distinct A.check_number, A.scan_date  ,
	                                                ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number, A.scan_date ) AS rn
	                                                from  ttDeliveryScanLog A With(Nolock) 
	                                                INNER JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number 
	                                                WHERE  B.print_date  >=@sdate and  B.print_date < DATEADD(day,1 ,@edate )   and A.scan_date  >=DATEADD(MONTH,-3,GETDATE())
                                                

                                                Select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO , CASE WHEN F.scan_date is not null THEN F.scan_date END as scan_date,
                                                   A.send_contact, Convert(VARCHAR, A.print_date,111) 'print_date'  , Convert(VARCHAR, A.arrive_assign_date,111) 'arrive_assign_date' , A.check_number , A.receive_customer_code, 
                                                   E.supplier_name 'area_arrive_station', A.receive_contact,
                                                   A.receive_city + A.receive_area+ CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else A.receive_address end 'receive_address',
                                                   CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END 'receive_tel',
                                                   ISNULL(A.arrive_to_pay_freight,0) 'arrive_to_pay_freight', ISNULL(A.collection_money,0) 'collection_money', 
                                                   A.pieces ,  A.cbm ,  CASE WHEN A.pricing_type = '01' then A.plates else 0 end 'plates',
                                                   CASE WHEN A.pricing_type = '04' then A.plates else 0 end 'splates'
                                                   from  tcDeliveryRequests A with(nolock)
                                                   LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                                   LEFT JOIN @tb2 F on A.check_number  =  F.check_number  and F.rn= 1
	                                               LEFT JOIN @tbl G on A.order_number = G.order_number and G.rn= 1 
                                                where  A.cancel_date IS NULL and A.Less_than_truckload =@Less_than_truckload
                                                --and (CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@sdate,111) and  CONVERT(VARCHAR,A.print_date,111) <=CONVERT(VARCHAR,@edate,111))
                                                and A.print_date  >=@sdate and  A.print_date < DATEADD(day,1 ,@edate )  
                                                and A.area_arrive_code LIKE '%'+  ISNULL(@Supplier_code,'') +'%'
                                                and (F.scan_date IS NOT NULL or G.scan_date IS NOT NULL )
                                                
                                            ");
            }
            else if (kind.Text == "3")  //已下單未到著明細
            {
                cmd.CommandText = string.Format(@"
                                             DECLARE @tbl table
                                             (order_number nvarchar(50))
                                             INSERT INTO @tbl
	                                            SELECT  distinct B.order_number  from  ttDeliveryScanLog A With(Nolock) 
	                                            LEFT JOIN tcDeliveryRequests B  With(Nolock) ON A.check_number = B.check_number 
	                                            WHERE A.scan_date  >=DATEADD(MONTH,-3,GETDATE())
	                                            --AND (CONVERT(VARCHAR,B.print_date,111)  >= CONVERT(VARCHAR,@sdate,111) AND  CONVERT(VARCHAR,B.print_date,111) <=CONVERT(VARCHAR,@edate,111))
                                                AND B.print_date  >=@sdate and  B.print_date < DATEADD(day,1 ,@edate ) 
	                                            AND B.area_arrive_code LIKE '%'+  ISNULL(@Supplier_code,'') +'%'
	                                            AND ISNULL(B.order_number,'') <> ''


                                            Select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO , '' as scan_date,
                                               A.send_contact, Convert(VARCHAR, A.print_date,111) 'print_date'  , Convert(VARCHAR, A.arrive_assign_date,111) 'arrive_assign_date' , A.check_number , A.receive_customer_code, 
                                               E.supplier_name 'area_arrive_station', A.receive_contact,
                                               A.receive_city + A.receive_area+ CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else A.receive_address end 'receive_address',
                                               CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END 'receive_tel',
                                               ISNULL(A.arrive_to_pay_freight,0) 'arrive_to_pay_freight', ISNULL(A.collection_money,0) 'collection_money', 
                                               A.pieces ,  A.cbm ,  CASE WHEN A.pricing_type = '01' then A.plates else 0 end 'plates',
                                               CASE WHEN A.pricing_type = '04' then A.plates else 0 end 'splates'
                                               from  tcDeliveryRequests A with(nolock)
                                               LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code 
                                            where  A.cancel_date IS NULL and A.Less_than_truckload =@Less_than_truckload --A.print_date  <= getdate()
                                            --and (CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@sdate,111) and  CONVERT(VARCHAR,A.print_date,111) <=CONVERT(VARCHAR,@edate,111))
                                            and A.print_date  >=@sdate and  A.print_date < DATEADD(day,1 ,@edate )  
                                            and A.area_arrive_code LIKE '%'+  ISNULL(@Supplier_code,'') +'%'
                                            and A.check_number not in (select distinct check_number from  ttDeliveryScanLog With(Nolock) where scan_date  >=DATEADD(MONTH,-3,GETDATE()))
                                            and ISNULL(A.order_number,'') not in (select order_number from @tbl )
                                            ");
            }                                              

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                DT = dt;
                //PagedDataSource objPds = new PagedDataSource();
                //objPds.DataSource = dt.DefaultView;
                //objPds.AllowPaging = true;
                //objPds.PageSize = 20;
                //int sumlistpage = 9;

                //#region 下方分頁顯示
                ////一頁幾筆
                //int CurPage = 0;
                //if ((Request.QueryString["Page"] != null))
                //{
                //    //控制接收的分頁並檢查是否在範圍
                //    if ( Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                //    {
                //        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                //        {
                //            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                //        }
                //        else
                //        {
                //            CurPage = 1;
                //        }
                //    }
                //    else
                //    {
                //        CurPage = 1;
                //    }
                //}
                //else
                //{
                //    CurPage = 1;
                //}
                //objPds.CurrentPageIndex = (CurPage - 1);
                //lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                //lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
                ////第一頁控制
                //if (!objPds.IsFirstPage)
                //{
                //    lnkfirst.Visible = true;
                //    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                //}
                //else
                //{
                //    lnkfirst.Visible = false;
                //}
                ////最後一頁控制
                //if (!objPds.IsLastPage)
                //{
                //    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                //    lnklast.Visible = true;
                //}
                //else
                //{
                //    lnklast.Visible = false;
                //}

                ////上一頁控制
                //if (CurPage - 1 == 0)
                //{
                //    lnkPrev.Visible = false;
                //}
                //else
                //{
                //    lnkPrev.Visible = true;
                //}

                ////下一頁控制
                //if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                //{
                //    lnkNext.Visible = false;
                //}
                //else
                //{
                //    lnkNext.Visible = true;
                //}


                ////跑分頁前五個
                //for (int j = (CurPage - 5); j <= (CurPage - 1); j++)
                //{

                //    if (j <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)) && j > 0)
                //    {
                //        pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + j.ToString() + querystring + "'>" + j.ToString() + "</a></li>"));
                //        sumlistpage = sumlistpage - 1;
                //    }

                //}

                ////跑分頁後面剩餘數，共十個
                //for (int i = CurPage; i <= (CurPage + sumlistpage); i++)
                //{
                //    if (i == CurPage)
                //    {
                //        pagelist.Text += "<li class='active'><a href='#'>" + CurPage.ToString() + "</a></li>";
                //    }
                //    else
                //    {
                //        if (i <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                //        {
                //            pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + i.ToString() + querystring + "'>" + i.ToString() + "</a></li>"));
                //        }

                //    }
                //}

                //#endregion

                New_List.DataSource = dt; // objPds;
                New_List.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();
            }

        }
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        //paramlocation();
        readdata();
    }

    private void paramlocation()
    {
        string querystring = "";
        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

        if (sdate.Text != "")
        {
            querystring += "&sdate=" + sdate.Text;
        }

        if (edate.Text != "")
        {
            querystring += "&edate=" + edate.Text;
        }

        Response.Redirect(ResolveUrl("~/report_3_1.aspx?search=yes" + querystring));
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        this.ExportExcel();
    }

    protected void ExportExcel()
    {
        if (DT == null)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可匯出資料');</script>", false);
            return;

        }

        using (ExcelPackage p = new ExcelPackage())
        {
            //logger.Info("begin epplus");

            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add(lbTitle.Text );

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 10;
            sheet1.Column(6).Width = 20;
            sheet1.Column(7).Width = 11;
            sheet1.Column(8).Width = 11;
            sheet1.Column(9).Width = 40;
            sheet1.Column(10).Width = 11;
            sheet1.Column(11).Width = 11;
            sheet1.Column(12).Width = 11;
            sheet1.Column(13).Width = 11;
            sheet1.Column(14).Width = 11;
            sheet1.Column(15).Width = 11;
            sheet1.Column(16).Width = 11;
            sheet1.Column(17).Width = 11;
            sheet1.Column(18).Width = 11;
            sheet1.Row(1).Height = 16;
            sheet1.Row(2).Height = 16;
            sheet1.Row(3).Height = 16;
            sheet1.Row(4).Height = 16;
            sheet1.Row(5).Height = 16;
            sheet1.Row(6).Height = 16;
            sheet1.Row(7).Height = 16;
            sheet1.Row(8).Height = 16;
            sheet1.Row(9).Height = 16;
            sheet1.Row(10).Height = 16;
            sheet1.Row(11).Height = 16;
            sheet1.Row(12).Height = 16;
            sheet1.Row(13).Height = 16;
            sheet1.Row(14).Height = 16;
            sheet1.Row(15).Height = 16;
            sheet1.Row(16).Height = 16;
            sheet1.Row(17).Height = 16;
            sheet1.Row(18).Height = 16;

            sheet1.Cells[1, 1, 1, 18].Merge = true; //合併儲存格
            sheet1.Cells[1, 1, 1, 18].Value = lbTitle.Text ;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 18].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 18].Style.Font.Bold = true;
            sheet1.Cells[1, 1, 1, 18].Style.Font.Size = 14;
            sheet1.Cells[1, 1, 1, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 18].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 18].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 18].Style.Font.Size = 10;
            sheet1.Cells[2, 1, 2, 18].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "配送站所："+ Suppliers.SelectedItem.Text ;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
           
            sheet1.Cells[3, 1, 3, 18].Merge = true; 
            sheet1.Cells[3, 1, 3, 18].Value = "發送日期：" + sdate.Text + "~" + edate.Text;
            sheet1.Cells[3, 1, 3, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[3, 1, 3, 18].Style.Font.Size = 10;

            sheet1.Cells[4, 1, 4, 18].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 18].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 18].Style.Font.Bold = true;
            sheet1.Cells[4, 1, 4, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[4, 1, 4, 18].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 18].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 18].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "NO.";
            sheet1.Cells[4, 2].Value = "出貨人";
            sheet1.Cells[4, 3].Value = "出貨日期";
            sheet1.Cells[4, 4].Value = "指定日";
            sheet1.Cells[4, 5].Value = "到著日";
            sheet1.Cells[4, 6].Value = "貨號";
            sheet1.Cells[4, 7].Value = "收件人編號";
            sheet1.Cells[4, 8].Value = "到著站所";
            sheet1.Cells[4, 9].Value = "收貨人";
            sheet1.Cells[4, 10].Value = "收貨地址";
            sheet1.Cells[4, 11].Value = "電話";
            sheet1.Cells[4, 12].Value = "異常原因";
            sheet1.Cells[4, 13].Value = "代收金額";
            sheet1.Cells[4, 14].Value = "到付運費";
            sheet1.Cells[4, 15].Value = "板數";
            sheet1.Cells[4, 16].Value = "才數";
            sheet1.Cells[4, 17].Value = "件數";
            sheet1.Cells[4, 18].Value = "小板數";


            for (int i = 0; i < DT.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 5, 2].Value = DT.Rows[i]["send_contact"].ToString();
                sheet1.Cells[i + 5, 3].Value = DT.Rows[i]["print_date"].ToString();
                sheet1.Cells[i + 5, 4].Value = DT.Rows[i]["arrive_assign_date"].ToString();
                sheet1.Cells[i + 5, 5].Value = DT.Rows[i]["scan_date"] != DBNull.Value && DT.Rows[i]["scan_date"].ToString() != "" ?  Convert.ToDateTime(DT.Rows[i]["scan_date"]).ToString("yyyy/MM/dd hh:mm:ss") :"";
                sheet1.Cells[i + 5, 6].Value = DT.Rows[i]["check_number"].ToString();              
                sheet1.Cells[i + 5, 7].Value = DT.Rows[i]["receive_customer_code"].ToString();
                sheet1.Cells[i + 5, 8].Value = DT.Rows[i]["area_arrive_station"].ToString();
                sheet1.Cells[i + 5, 9].Value = DT.Rows[i]["receive_contact"].ToString();
                sheet1.Cells[i + 5, 10].Value = DT.Rows[i]["receive_address"].ToString();
                sheet1.Cells[i + 5, 11].Value = DT.Rows[i]["receive_tel"].ToString();
                sheet1.Cells[i + 5, 12].Value = "";
                sheet1.Cells[i + 5, 13].Value = Convert.ToInt32(DT.Rows[i]["collection_money"]).ToString("N0");
                sheet1.Cells[i + 5, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 14].Value = Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]).ToString("N0");
                sheet1.Cells[i + 5, 14].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 15].Value = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]).ToString("N0") :"0";
                sheet1.Cells[i + 5, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 16].Value = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]).ToString("N0") :"0";
                sheet1.Cells[i + 5, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 17].Value = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]).ToString("N0") : "0";
                sheet1.Cells[i + 5, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[i + 5, 18].Value = DT.Rows[i]["splates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["splates"]).ToString("N0") : "0";
                sheet1.Cells[i + 5, 18].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            }


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("已下單未到著.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }
}