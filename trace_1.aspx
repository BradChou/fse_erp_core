﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTrace.master" AutoEventWireup="true" CodeFile="trace_1.aspx.cs" Inherits="trace_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            //$('.fancybox').fancybox();
            //$("#Imglink").fancybox({
            //    'width': 500,
            //    'height': 500,
            //    'autoScale': false,
            //    'transitionIn': 'none',
            //    'transitionOut': 'none',
            //    'hideOnOverlayClick': 'false',
            //    'type': 'iframe',
            //    'onClosed': function () {
            //        parent.location.reload(true);
            //    }
            //});
            $('.fancybox').fancybox();

            $("#btn_show").click(function () {
                $("div.img-box, #LoadBox").show();
            });

            $("#LoadBox").click(function () {
                $("div.img-box, #LoadBox").hide();
            });


        });
        $.fancybox.update();
        $(function () {
            $('.fancybox').fancybox();

            $("#btn_show").click(function () {
                $("div.img-box, #LoadBox").show();
            });

            $("#LoadBox").click(function () {
                $("div.img-box, #LoadBox").hide();
            });
        });

        $(function () {
            $(".trace_date_picker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm",
                showButtonPanel: true,
                defaultDate: (new Date()),  //預設當日
                onClose: function (dateText, inst) {
                    var month = $("#ui-datepicker-div .ui-datepicker-month option:selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year option:selected").val();
                    month = parseInt(month) + 1;//要对月值加1才是实际的月份
                    if (month < 10) {
                        month = "0" + month;
                    }
                    $(".trace_date_picker").val(year + "-" + month);
                }
            });
        });
    </script>
    <style type="text/css">
        .ui-datepicker-calendar {
            display: none;
        }

        .img-box {
            border: 5px solid #000;
            position: fixed;  
            max-width: 90%;
            padding: 10px;
            left: calc((100vw - 1024px) / 2);
            background: #FFF;
            z-index: 101;
        }

            .img-box a {
            }

        #LoadBox {
            position: fixed;
            width: 100%;
            height: 100%;
            background: #000;
            top: 0;
            left: 0;
            filter: alpha(opacity=70);
            opacity: 0.7;
            display: block;
            cursor: wait;
            z-index: 100;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">貨號查詢</h2>
            <div class="templatemo-login-form">
                <div class="row form-group">
                    <div class="col-lg-4 col-md-6 form-group form-inline">
                        <label for="inputLastName">貨　號</label>
                        <%--<input type="text" class="form-control">--%>
                        <%--<asp:TextBox ID="check_number" runat="server"   MaxLength="10" ></asp:TextBox>--%>
                        <asp:TextBox ID="check_number" runat="server" MaxLength="20"></asp:TextBox>
                        <%--  <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "check_number" ID="RegularExpressionValidator3" ValidationExpression = "^[\s\S]{10,10}$" runat="server" 
                            ErrorMessage="度長需為10" ForeColor="Red"></asp:RegularExpressionValidator>          --%>

                        <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="check_number" ForeColor="Red" ValidationGroup="validate">請輸入貨號</asp:RequiredFieldValidator>
                        <%--<label for="inputLastName">發送年月</label>
                        <asp:TextBox ID="date" runat="server" class="form-control" CssClass="trace_date_picker"></asp:TextBox>--%>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                        <label for="inputLastName">次貨號</label>
                        <%--<asp:TextBox ID="sub_chknum" runat="server">000</asp:TextBox>--%>
                        <asp:DropDownList ID="dlsub_chknum" runat="server" CssClass="form-control">
                            <asp:ListItem>000</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Button ID="search" CssClass="templatemo-blue-button btn" runat="server" Text="查 詢" ValidationGroup="validate" OnClick="search_Click" />
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-lg-4 col-md-6 form-group form-inline">
                        <label for="Imglink">簽單查詢</label>
                        <asp:HyperLink runat="server" ID="Imglink" class="fancybox fancybox.iframe">
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="images/檢視.jpg" Height="20px" Width="20px" ToolTip="簽單查詢" ClientIDMode="AutoID" />
                        </asp:HyperLink>
                        <%--<label for="inputLastName">貨　　號</label>
                        <asp:Label ID="lbcheck_number" runat="server"></asp:Label>--%>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 1px; top: 0px">
                        <label for="RtnImglink">回單查詢</label>
                        <%--<asp:HyperLink runat ="server" id ="RtnImglink" class="fancybox fancybox.iframe"><asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" ToolTip ="回單查詢" /></asp:HyperLink>--%>
                        <%--<asp:LinkButton ID="RtnImglink" runat="server">
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" ToolTip="回單查詢" /></asp:LinkButton>--%>
                      <%--  <input type="image" id="btn_show" src="~/images/檢視.jpg" style="width:20px;height:20px;" value="Open" />--%>
                       <%-- <asp:ImageButton runat="server" ID="btn_show" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" ToolTip="回單查詢" ClientIDMode="Static" />--%>
                        <input type="button" id="btn_show" value="檢視" />
                        <a href="fileupload_receipt.aspx" class="fancybox fancybox.iframe " id="rtnclick" runat="server" visible="false"><span class="btn btn-warning">回單上傳</span></a>
                        <%--<button type="submit" class="btn btn-darkblue">影像查詢</button>--%>
                    </div>
                </div>
                <div class="img-box" style="display: none;">
                   <asp:Panel ID="myPanel" runat="server">
                    </asp:Panel>
                </div>
                <div id="LoadBox" style="display: none"></div>
                <hr>
                <div style="overflow: auto; max-height: 300px;">
                    <table class="table table-striped table-bordered templatemo-user-table">
                        <thead>
                            <tr class="tr-only-hide">
                                <td>發送站</td>
                                <td>到著站</td>
                                <td>發送日</td>
                                <td>指配日期</td>
                                <td>指配時間</td>
                                <td>貨號</td>
                                <td>訂單編號</td>
                                <td>件數</td>
                                <td>傳票區分</td>
                                <td>商品種類</td>
                                <td>請款金額(元付)</td>
                                <td>到付追加</td>
                                <td>代收金額</td>
                            </tr>
                        </thead>
                        <asp:Repeater ID="New_List_01" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                                    <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.showname").ToString())%></td>
                                    <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                                    <td data-th="指配日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                                    <td data-th="指配時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.time_period").ToString())%></td>
                                    <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                    <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                                    <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                                    <td data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_name").ToString())%></td>
                                    <td data-th="商品種類"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.product_category_name").ToString())%></td>
                                    <td data-th="請款金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件運費").ToString())%></td>
                                    <td data-th="到付追加"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到付追加未稅").ToString())%></td>
                                    <td data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List_01.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="12" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>
                </div>


                <table class="table table-striped table-bordered templatemo-user-table">
                    <thead>
                        <tr>
                            <td>寄件人資料</td>
                            <td>收件人資料</td>
                            <%--<td>處置說明</td>--%>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label ID="lbSend" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbReceiver" runat="server"></asp:Label>
                            </td>
                           <%-- <td>
                                <asp:Button ID="btnDetail" runat="server" Text="查詢/新增處置說明" OnClick="btnDetail_Click" />
                            </td>--%>
                        </tr>
                    </tbody>
                </table>
                <div id="divMaster" runat="server" class="templatemo-login-form panel panel-default">
                    <table class="table table-striped table-bordered templatemo-user-table">
                        <thead>
                            <tr>
                                <td>NO.</td>
                                <td>作業時間</td>
                                <td>作業</td>
                                <td>站所</td>
                                <td>到著/發送站</td>
                                <td>件數</td>
                                <td>班次代號</td>
                                <td>狀態</td>
                                <td>運輸方式</td>
                                <td>註區</td>
                                <td>員工</td>
                                <td>快遞單號</td>
                            </tr>
                        </thead>
                        <asp:Repeater ID="New_List_02" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                                    <td data-th="作業時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                                    <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                                    <td data-th="站所"><%# (DataBinder.Eval(Container, "DataItem.scan_item").ToString() !="6") ? Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString()): Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.warehouse").ToString()) %></td>
                                    <td data-th="到著/發送站"></td>
                                    <td data-th="件數">1</td>
                                    <td data-th="班次代號"></td>
                                    <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status").ToString())%></td>
                                    <td data-th="運輸方式"></td>
                                    <td data-th="註區"></td>
                                    <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>  <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                                    <td data-th="快遞單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tracking_number").ToString())%></td>
                                    <%--<td data-th="影像查詢">
                                    <asp:HyperLink runat ="server" id ="Imglink" class="fancybox fancybox.iframe"><asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" /></asp:HyperLink>
                                </td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (New_List_02.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="11" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>
                </div>

                <div id="divDetail" runat="server" visible="true" class="templatemo-login-form panel panel-default">
                    <table class="table table-striped table-bordered templatemo-user-table ">
                        <thead>
                            <tr class="tr-only-hide">
                                <td>NO.</td>
                                <td>輸入時間</td>
                                <td>狀況類別</td>
                                <td>處置說明</td>
                                <td>員工</td>
                            </tr>
                        </thead>
                        <asp:Repeater ID="Datail_List" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                                    <td data-th="輸入時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate").ToString())%></td>
                                    <td data-th="狀況類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                    <td data-th="處置說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dispose_desc").ToString())%></td>
                                    <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (Datail_List.Items.Count == 0)
                            {%>
                        <tr>
                            <td colspan="5" style="text-align: center">尚無資料</td>
                        </tr>
                        <% } %>
                    </table>
                    <hr />
                    <table class="table table-striped table-bordered templatemo-user-table">
                        <tbody>
                            <tr>
                                <td style="width: 20%">狀況類別</td>
                                <td>
                                    <div class="col-lg-12 form-group form-inline" style="text-align: left;">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dlP1" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="dlP1_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="dlP1" ForeColor="Red" ValidationGroup="validetail">請選擇處理說明</asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="dlP2" runat="server" class="form-control"></asp:DropDownList>
                                                <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="dlP2" ForeColor="Red" ValidationGroup="validetail">請選擇處理說明</asp:RequiredFieldValidator>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>處置說明</td>
                                <td>
                                    <asp:TextBox ID="dispose_desc" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnSaveDatail" runat="server" Text="存檔" ValidationGroup="validetail" OnClick="btnSaveDatail_Click" />
                                    <%--<asp:Button ID="btnExit" runat="server" Text="離開" OnClick="btExit_Click" />--%>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>


            </div>
        </div>

    </div>

</asp:Content>

