﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

public partial class LT_money_3_2 : System.Web.UI.Page
{

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Boolean IsManager = false;
            if (Session["account_code"] != null & (Session["account_code"].ToString() == "skyeyes"|| Session["account_code"].ToString() == "9990008") )
            {
                IsManager = true;
            }
            else
            {
                manager_type = Session["manager_type"].ToString(); //管理單位類別   

                //switch (manager_type)
                //{
                //    case "4":  //區配商
                //        customer_type.Enabled = false;
                //        customer_type.SelectedValue = "1";
                //        break;
                //    case "3":  //峻富自營
                //    case "5":  //區配商自營
                //        customer_type.Enabled = false;
                //        customer_type.SelectedValue = "3";
                //        break;
                //}
            }


            #region 材積大小
            
            using (SqlCommand cmd8 = new SqlCommand())
            {

                cmd8.CommandText = "select id, CbmID, CbmSize from tcCbmSize with(nolock)  ";
                dlCbmSize.DataSource = dbAdapter.getDataTable(cmd8);
                dlCbmSize.DataValueField = "CbmID";
                dlCbmSize.DataTextField = "CbmSize";
                dlCbmSize.DataBind();
            }
            

            dlCbmSize.Items.Insert(0, new ListItem("全部", ""));
            #endregion


            #region 定價日期
            RefreshDate();
            #endregion

            ListLoad();

            //div_Upload.Visible = false;
            //if (DateTime.Now.Day >= 20 && DateTime.Now.Day <= 25
            //    || IsManager)
            //{
            //    div_Upload.Visible = true;
            //}

        }
    }

   
    protected void btQry_Click(object sender, EventArgs e)
    {
        
        using (SqlCommand cmd = new SqlCommand())
        {
          

            string SQLstr = "";

            SQLstr = @"Select A.*, B.CbmSize 'sizename' from tbPriceLessThan A 
                            Left join tcCbmSize B on B.id =A.Cbmsize 
                            Where 1=1 ";
            if (dlCbmSize.SelectedValue != "")
            {
                SQLstr += "AND  A.Cbmsize = @Cbmsize  ";
            cmd.Parameters.AddWithValue("@Cbmsize", dlCbmSize.SelectedValue);

            }
            if (money_date.SelectedValue != "")
            {
                SQLstr += "  AND   convert(varchar,A.Enable_date,120) like @Enable_date";
                cmd.Parameters.AddWithValue("@Enable_date", money_date.SelectedValue + "%");
            }


              

            cmd.CommandText = SQLstr;

           


            New_List.DataSource = dbAdapter.getDataTable(cmd);
            New_List.DataBind();
        }
        pan_PriceList.Visible = true;


    }


    protected void btndownload_Click(object sender, EventArgs e)
    {
        #region 匯出excel樣版
        string ttServerPath = this.Server.MapPath(".") + "\\files\\";
        string FileName = "零擔公版運價";
        SqlCommand objCommand = new SqlCommand();
        Hashtable hash = new Hashtable();
        DataSet ds = new DataSet();
        DataTable dt = null;
        DataTable dttemp = null;
        DataTable dttempA = new DataTable();
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmdA = new SqlCommand();      
        string wherestr = string.Empty;
        if (money_date.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@Enable_date", money_date.SelectedValue + "%");
            wherestr += "  AND convert(varchar,Enable_date,120) like @Enable_date";
        }
        else
        {
            wherestr += " AND Enable_date is NULL";
        }
              
        cmd.CommandText = string.Format(@"Select start_city, end_city, Enable_date  
                                            from[dbo].[tbPriceLessThan]  
                                            where   1=1  {0}
                                            group by start_city,end_city,Enable_date", wherestr);
        dttemp = dbAdapter.getDataTable(cmd);

        for(int i =0; i<14; i++)
        {
            dttempA.Columns.Add();
        }


        if (dttemp.Rows.Count > 0)
        {

            for (int i = 0; i < dttemp.Rows.Count; i++)
            {
                DataRow dr2 = dttempA.NewRow();
                //切記 !! DataTable 要產生Row時，必須使用NewRow這個方法
                //可以把它想成為將dt產生一個新的列
                dr2[0] = dttemp.Rows[i]["start_city"].ToString();
                dr2[1] = dttemp.Rows[i]["end_city"].ToString();

                wherestr = "";
                cmdA.Parameters.Clear();
                if (money_date.SelectedValue != "")
                {
                    cmdA.Parameters.AddWithValue("@Enable_date", money_date.SelectedValue + "%");
                    wherestr += "  AND convert(varchar,Enable_date,120) like @Enable_date";
                }
                else
                {
                    wherestr += " AND Enable_date is NULL";
                }
                cmdA.Parameters.AddWithValue("@start_city", dttemp.Rows[i]["start_city"].ToString());
                cmdA.Parameters.AddWithValue("@end_city", dttemp.Rows[i]["end_city"].ToString());
                cmdA.CommandText = string.Format(@"Select A.start_city, A.end_city,A.Cbmsize , B.CbmSize 'sizename', A.first_price, A.add_price from tbPriceLessThan A 
                            Left join tcCbmSize B on B.id =A.Cbmsize 
                            Where  start_city=@start_city and end_city=@end_city {0} ", wherestr);
                dt = dbAdapter.getDataTable(cmdA);
                if (dt.Rows.Count > 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        dr2[Convert.ToInt32(dt.Rows[j]["Cbmsize"].ToString()) * 3 - 1] = dt.Rows[j]["sizename"].ToString();
                        dr2[Convert.ToInt32(dt.Rows[j]["Cbmsize"].ToString()) * 3] = Convert.ToInt32 (dt.Rows[j]["first_price"]);
                        dr2[Convert.ToInt32(dt.Rows[j]["Cbmsize"].ToString()) * 3 + 1] = Convert.ToInt32 (dt.Rows[j]["add_price"]);

                    }
                }


                dttempA.Rows.Add(dr2);


            }






            //cmd.CommandText = SQLstr;
            //dttemp = dbAdapter.getDataTable(cmd);
            //if (dttemp.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dttemp.Rows.Count; i++)
            //    {
            //        DataRow dr2 = dt.NewRow();

            //        dt.Rows.Add(dr2);

            //        //dlcustomer.Items.Add(new ListItem(dta.Rows[i]["customer_code"].ToString().Trim() + dta.Rows[i]["customer_name"].ToString().Trim(), dta.Rows[i]["customer_code"].ToString().Trim()));
            //    }
            //}




            //dt = dbAdapter.getDataTable(cmd);
            ds.Tables.Add(dttempA);


            dttempA.TableName = FileName;
            string printName = "";

            #region 製作匯出
            //tablename = "公版運價-" + tablename;
            reportAdapter.npoiSampleToExcel(ttServerPath, ds, hash, FileName, printName);
            #endregion
            #endregion

        }
    }





    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        TextBox txt_first_price = (TextBox)e.Item.FindControl("txt_first_price");        
        TextBox txt_add_price = (TextBox)e.Item.FindControl("txt_add_price");
        switch (e.CommandName)
        {
            case "Mod":
                e.Item.FindControl("btnMob").Visible = false;
                e.Item.FindControl("btnSet").Visible = true;

               
                txt_first_price.ReadOnly = false;
                txt_add_price.ReadOnly = false;

                break;
            case "Set":
                int first_price = 0;
                int add_price = 0;
                if (!int.TryParse(txt_first_price.Text, out first_price) || first_price <=0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('首件費用有誤，請重新輸入');</script>", false);
                    return ;
                }
                if (!int.TryParse(txt_add_price.Text, out add_price) || add_price <= 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('續件費用有誤，請重新輸入');</script>", false);
                    return;
                }

                int i_id = 0;
                if (!int.TryParse(e.CommandArgument.ToString() , out i_id)) i_id = 0;
                string result = SetCusHcode(i_id.ToString(), first_price, add_price);
                if ("1" == result)
                {
                    txt_first_price.ReadOnly = true;
                    txt_add_price.ReadOnly = true;
                    e.Item.FindControl("btnMob").Visible = true;
                    e.Item.FindControl("btnSet").Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('修改成功');</script>", false);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + result + "');</script>", false);
                }
                break;
            default:
                break;
        }
    }

    public string SetCusHcode(string id,  int first_price, int add_price )
    {
        string str_result = "資訊異常，請重新確認!";
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@first_price", first_price);
                cmd.Parameters.AddWithValue("@add_price", add_price);
                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
                cmd.CommandText = dbAdapter.genUpdateComm("tbPriceLessThan", cmd);
                dbAdapter.execNonQuery(cmd);
            }
            str_result = "1";
        }
        catch (Exception ex)
        {
            str_result = "刪除發生錯誤，請聯絡系統人員!  " + ex.Message.ToString();
        }

        return str_result;

    }



    protected void ListLoad()
    {

        using (SqlCommand cmd = new SqlCommand())
        {


            string SQLstr = "";

            SQLstr = @"Select A.*, B.CbmSize 'sizename' from tbPriceLessThan A 
                            Left join tcCbmSize B on B.id =A.Cbmsize 
                            Where 1=1 ";
            if (dlCbmSize.SelectedValue != "")
            {
                SQLstr += "AND  A.Cbmsize = @Cbmsize  ";
                cmd.Parameters.AddWithValue("@Cbmsize", dlCbmSize.SelectedValue);

            }
            SQLstr += "  AND   convert(varchar,A.Enable_date,120) like @Enable_date";
            cmd.Parameters.AddWithValue("@Enable_date", money_date.SelectedValue + "%");

            cmd.CommandText = SQLstr;




            New_List.DataSource = dbAdapter.getDataTable(cmd);
            New_List.DataBind();
        }
        pan_PriceList.Visible = true;


    }


    protected void lbRefreshDate_Click(object sender, EventArgs e)
    {
        //更新定價日期
        RefreshDate();
    }

   private void RefreshDate()
    {
        using (SqlCommand cmda = new SqlCommand())
        {
            DataTable dta = new DataTable();
            cmda.CommandText = " SELECT  distinct Enable_date   from tbPriceLessThan where Enable_date Is Not NULL and   (customer_code IS NULL)   order by Enable_date DESC";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {

                    string dt12 = Convert.ToDateTime(dta.Rows[i]["Enable_date"]).ToString("yyyy-MM-dd");
                    money_date.Items.Add(new ListItem(dt12, dt12));

                }
            }
            else
            {
                money_date.Items.Add(new ListItem("全部", ""));
            }
        }

    }
}