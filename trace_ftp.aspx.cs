﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Text;
using ClosedXML.Excel;
using System.Net;
using Ionic.Zip;

public partial class trace_ftp : System.Web.UI.Page
{
    public int start_number_type1
    {
        get { return Convert.ToInt32(ViewState["start_number_type1"]); }
        set { ViewState["start_number_type1"] = value; }
    }

    public int end_number_type1
    {
        get { return Convert.ToInt32(ViewState["end_number_type1"]); }
        set { ViewState["end_number_type1"] = value; }
    }

    public int start_number_type2
    {
        get { return Convert.ToInt32(ViewState["start_number_type2"]); }
        set { ViewState["start_number_type2"] = value; }
    }

    public int end_number_type2
    {
        get { return Convert.ToInt32(ViewState["end_number_type2"]); }
        set { ViewState["end_number_type2"] = value; }
    }


    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }


    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }


    }

    public string check_numbers
    {
        get { return (string)ViewState["check_numbers"]; }
        set { ViewState["check_numbers"] = value; }


    }

    public int addvalue
    {
        get
        {
            if (ViewState["addvalue"] == null)
            {
                ViewState["addvalue"] = 13; //預設新增12欄
            }
            return Convert.ToInt32(ViewState["addvalue"]);
        }
        set
        {
            ViewState["addvalue"] = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }
            
            MultiView1.ActiveViewIndex = 0;
            
        }
        BindTextBox();
    }

    private void readdata()
    {
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";
        cmd.Parameters.Clear();

        #region 貨號
        string strfix = "";
        if (fixserial.Text.ToString().Trim() != "")
        {
            strfix = "'" + fixserial.Text.ToString().Trim() + "'";
        }
        for (int i = 0; i < addvalue; i++)
        {
            TextBox dynbox = PlaceHolder1.FindControl("fixserial" + i) as TextBox;

            if (dynbox.Text.Trim() != "")
            {
                if (strfix == "")
                {
                    strfix = "'" + dynbox.Text.Trim() + "'";
                }
                else
                {
                    strfix += ",'" + dynbox.Text.Trim() + "'";
                }
            }
        }

        if (strfix != "")
        {
            strWhereCmd += "  and (scan.check_number in (" + strfix + "))";
            check_numbers = strfix;
        }
                
        #endregion

        cmd.CommandText = string.Format(@"
                                        WITH scan AS 
                                        (
                                        select  ISNULL(dr.request_id,0) as request_id , scan.*  ,
                                        ROW_NUMBER() OVER (PARTITION BY scan.check_number ORDER BY scan.check_number DESC, scan.scan_date DESC) AS rn
                                        from  ttDeliveryScanLog  scan
                                        left join tcDeliveryRequests dr with(nolock) on scan.check_number = dr.check_number and dr.supplier_code in('001','002') and dr.print_date >=  DATEADD(MONTH,-6,getdate())
                                        where scan.scan_date  >=  DATEADD(MONTH,-6,getdate())
                                        and LEN(scan.check_number) = 10 
                                        and(not(scan.check_number between 8158486615 and 8158586601))
                                        and(not(scan.check_number between 5920000000 and 5929999999))
                                        and(not(scan.check_number between 8347120000 and 8347129999))
                                        and(not(scan.check_number between 8819346760 and 8819406750))
                                        and(not(scan.check_number between 9000200010 and 9000229999))
                                        and(not(scan.check_number between 8283490025 and 8286490015))
                                        and scan.scan_item = '3'
                                        {0}
                                        )
                                        select  ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO ,
                                        A.log_id ,A.check_number , 
                                        P3.code_name as scan_name,  Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                        A.arrive_option,A.exception_option , 
                                        CASE A.scan_item WHEN '3' THEN AO.code_name  ELSE EO.code_name  END as status,
                                        A.driver_code , C.driver_name , A.sign_form_image,
                                        A.request_id
                                        from scan A
                                        left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                        left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                        left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                        left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                        left join tbItemCodes P3 with(nolock) on A.scan_item = P3.code_id  and P3.code_sclass = 'P3' and P3.active_flag = 1 
                                        where rn= 1 ", strWhereCmd);

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
            MultiView1.ActiveViewIndex = 1;
        }

    }

    private void readdataftp()
    {
        string ErrStr = "";
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd1 = "";
        string strWhereCmd2 = "";
        cmd.Parameters.Clear();

        #region 貨號

        strWhereCmd1 += "  and (check_number in (" + check_numbers + "))";
        strWhereCmd2 += "  and (A.check_number in (" + check_numbers + "))";

        #endregion

        //cmd.CommandText = string.Format(@"
        //                                WITH ftplog AS 
        //                                (
        //                                select check_number , logdate  ,
        //                                ROW_NUMBER() OVER (PARTITION BY check_number ORDER BY check_number, logdate DESC) AS rn
        //                                from tbHCTFTPLog 
        //                                where 1=1 {0}
        //                                )
        //                                select  * from ftplog where rn= 1  ", strWhereCmd);
        cmd.CommandText = string.Format(@"WITH ftplog AS 
                                            (
                                            SELECT  A.check_number, 
                                                    logdate AS senddate   ,
	                                                photodate,
		                                            ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.check_number DESC) AS rn
                                            FROM  tbHCTFTPLog A
                                            left join (
                                            SELECT  check_number, type,   logdate AS photodate   ,
                                                    ROW_NUMBER() OVER (PARTITION BY check_number ORDER BY check_number, logdate DESC) AS rn
                                            FROM  tbHCTFTPLog 
                                            where 1=1  {0} and type = '簽單' 
                                            ) B on A.check_number  = B.check_number and B.rn=1
                                            where 1=1 {1} and A.type = '配達'
                                            )
                                            select * from ftplog where rn= 1", strWhereCmd1, strWhereCmd2);

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            RpFtoLog.DataSource = dt;
            try
            {
                RpFtoLog.DataBind();
            }
            catch (Exception ex)
            {
                ErrStr = ex.Message;
            }
            
            ltotalpages2.Text = dt.Rows.Count.ToString();
            
        }

    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }
    

    protected void btnQry_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void AddTxtButton_Click(object sender, EventArgs e)
    {
        AddTextBox();
    }


    //動態新增控制項
    private void AddTextBox()
    {
        addvalue += 1;

        BindTextBox();
        //this.ViewState["TextBoxAdded"] = true;
    }

    private void BindTextBox()
    {
        //PlaceHolder1.Controls.Clear(); //先清除所有子控制項

        for (int i = 0; i < addvalue; i++)
        {
            if (PlaceHolder1.FindControl("fixserial" + i) == null)
            {
                TextBox textbox = new TextBox();
                textbox.ID = "fixserial" + i;//重點是要給他一個ID
                textbox.Attributes.Add("placeholder", "請輸入貨號");
                PlaceHolder1.Controls.Add(textbox);
            }
        }

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "cmdAdd")
        //{   
        //    string log_id = ((HiddenField)e.Item.FindControl("Hid_logid")).Value.ToString().Trim();
        //    string check_number = ((Label)e.Item.FindControl("lbcheck_number")).Text.ToString().Trim();
        //    string scan_date = ((Label)e.Item.FindControl("lbscan_date")).Text.ToString().Trim();  
        //    add_check_number.Text = check_number;
        //    btnAdd_Click(null, null);
        //    Shipments_date.Text = Convert.ToDateTime(scan_date).ToString("yyyy/MM/dd");

        //}
    }

    protected void btn_Prev1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }

    protected void btnFtp_Click(object sender, EventArgs e)
    {
        string Send_check_number = "";
        string Photo_check_number = "";
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                string check_number = ((Label)New_List.Items[i].FindControl("lbcheck_number")).Text;

                CheckBox cbSelSend = ((CheckBox)New_List.Items[i].FindControl("cbSelSend"));
                CheckBox cbSelPhoto = ((CheckBox)New_List.Items[i].FindControl("cbSelPhoto"));
                if (cbSelSend.Visible && cbSelSend.Checked)
                {
                    if (Send_check_number == "")
                    {
                        Send_check_number = "'" + check_number + "'";
                    }
                    else
                    {
                        Send_check_number += ",'" + check_number + "'";
                    }
                }

                if (cbSelPhoto.Visible && cbSelPhoto.Checked)
                {
                    if (Photo_check_number == "")
                    {
                        Photo_check_number = "'" + check_number + "'";
                    }
                    else
                    {
                        Photo_check_number += ",'" + check_number + "'";
                    }
                }
            }
        }

        #region 配達紀錄上傳
        string sendmsg = "";
        if (Send_check_number != "")
        {
            sendmsg= HCTftp_Send(Send_check_number);
        }

        #endregion

        #region 簽單上傳
        string photomsg = "";
        if (Photo_check_number != "")
        {
            photomsg = HCTftp_Photo(Photo_check_number);
        }
        
        #endregion

        if (sendmsg == "" && photomsg == "")
        {
            readdataftp();
            MultiView1.ActiveViewIndex = 2;
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('上傳完成');</script>", false);
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('"+ sendmsg+ ";"+ photomsg + "');</script>", false);
            return;
        }

    }

    /// <summary>
    /// 配達紀錄上傳
    /// </summary>
    private string  HCTftp_Send(string Send_check_number)
    {
        #region  配達資料回傳HCT
        //1.檔名格式為：RT_[年月日][時間].CSV
        //2.請於每日 21:30 產生於 新竹物流 FTP 主機
        //2.民國年月日：YYYMMDD，例如 1010208為民國101年2月8日->改為西元年YYYYMMDD
        //3.時間：24HMISS，例如 210001為21點0分1秒
        //4.檔案內不含任何中文字
        //5.特定貨號區間不轉入
        //(1) 8158486615 ~8158586601
        //(2) 5920000000 ~5929999999  天工出貨
        //(3) 8347120000 ~8347129999  勁量出貨
        //(4) 8819346760 ~8819406750  自營貨號
        //(5) 9000200010 ~9000229999  自營貨號
        //(6) 8283490025 ~8286490015
        //如有問題，請參考此方案下的:(FTP)峻富資料交換規格書.xlsx
        #endregion

        string ErrStr = "";
        DateTime dtNow = DateTime.Now;
        System.Globalization.TaiwanCalendar twC = new System.Globalization.TaiwanCalendar();


        string FileName = "RT_" + twC.GetYear(dtNow) + dtNow.ToString("MMddHHmmss") + ".CSV";
        string ExecDate = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd");
        string ExecHour = DateTime.Now.AddHours(-1).ToString("HH");
        string wherestr = @" and scan.check_number in("+ Send_check_number +")";
        //要抓"配達"，且"正常配交"的貨
        //tcDeliveryRequests.HCTstatus  2:配達過但未正常配交  3:配達  4簽單
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = string.Format(@"WITH scan AS(  
                                            Select scan.*,
                                            CASE dr.pricing_type WHEN '01' THEN dr.plates WHEN '02' THEN  dr.pieces  WHEN '03' THEN  dr.cbm WHEN '04' THEN  dr.plates ELSE 1 END AS drpieces  , 
                                            ISNULL(dr.print_date, scan.scan_date ) as print_date , dr.request_id, ROW_NUMBER() OVER (PARTITION BY scan.check_number ORDER BY scan.scan_date DESC) AS rn 
                                            from ttDeliveryScanLog scan
                                            left join tcDeliveryRequests dr with(nolock) on scan.check_number = dr.check_number and dr.supplier_code in('001','002') and dr.print_date >=  DATEADD(MONTH,-3,getdate())
                                            where scan.scan_item = '3'
                                            and LEN(scan.check_number) = 10 
                                            and(not(scan.check_number between 8158486615 and 8158586601))
                                            and(not(scan.check_number between 5920000000 and 5929999999))
                                            and(not(scan.check_number between 8347120000 and 8347129999))
                                            and(not(scan.check_number between 8819346760 and 8819406750))
                                            and(not(scan.check_number between 9000200010 and 9000229999))
                                            and(not(scan.check_number between 8283490025 and 8286490015))
                                            --and ( HCTstatus IS NULL or ISNULL(HCTstatus,0) < 3 )
                                            {0}
                                            )
                                            select print_date,check_number, drpieces,scan_date,arrive_option, request_id from scan where rn = 1", wherestr);
        DataTable dt = dbAdapter.getDataTable(cmd);
        if (dt != null && dt.Rows.Count > 0)
        {
            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
            StringBuilder sb_temp_ins = new StringBuilder();
            SaveToCSV(dt, ConfigurationManager.AppSettings["FTP_FOLDER"] + "HA2HCT\\" + FileName, ref sb_temp_ins, ref sb_ins_list);

            #region 
            NetworkCredential credentials = new NetworkCredential("junfu", "6YDJM#");
            string uploadurl = "ftp://hctrt.hct.com.tw/HA2HCT/";
            using (WebClient client = new WebClient())
            {
                //上傳csv至ftp的方式
                client.Credentials = credentials;
                try
                {
                    client.UploadFile(uploadurl + "/" + FileName, ConfigurationManager.AppSettings["FTP_FOLDER"] + "HA2HCT\\" + FileName);

                    //using (System.IO.StreamWriter file =
                    //new System.IO.StreamWriter(@"E:\WEB\mail\HCTftp\HCTftp_Send\log\HCTftp_Send_Log" + DateTime.Now.ToString("yyyy-MM-dd hhmmss") + ".TXT"))
                    //{
                    //    file.WriteLine("{0} Upload File Complete", FileName);
                    //}

                    #region 執行SQL,更新託運單hct回傳狀態 3:配達  4簽單
                    foreach (StringBuilder sb in sb_ins_list)
                    {
                        if (sb.Length > 0)
                        {
                            String strSQL = sb.ToString();
                            using (SqlCommand cmdupd = new SqlCommand())
                            {
                                cmdupd.CommandText = strSQL;
                                dbAdapter.execNonQuery(cmdupd);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }
                
                #endregion
            }
            #endregion


        }

        return ErrStr;

    }

    /// <summary>
    /// 簽單上傳
    /// </summary>
    /// <param name="Send_check_number">貨號</param>
    private string  HCTftp_Photo(string Photo_check_number)
    {
        #region JUNFU-簽單回傳HCT
        //加密規則
        //* 檔案的密碼為檔案名稱(不含附檔名)加上#kvittering
        //* 索引檔及JPG全壓縮加密在同一ZIP檔中
        //EX: A_2233_2016100315523434_172.17.8.195_0518361003904.zip 的密碼為
        //    A_2233_2016100315523434_172.17.8.195_0518361003904#kvittering
        //如有問題，請參考此方案下的:(FTP)峻富資料交換規格書.xlsx


        //欄位          長度          備註
        //-------------------------------------------------------------------------------
        //掃描類別      1           簽單:A、回單:B
        //掃描日期      8           yyyymmdd
        //掃描時間      10          hh24missms
        //貨號          10          需檢核數字
        //檔案名稱      17          重覆則帶入 _001 序號遞增
        //SD工號        5           1. 需檢核數字 05183(固定)
        //站所代號      4           掃描機設定 customer_code去前3碼後3碼
        //IP            15          junfu(固定)
        //面卡日期      5           ymmdd 西元年(1)
        //箱號          9           (簽名1，整張2)
        //影像種類      4           簽單預設4283(固定)
        //掃描人員工號  5           00001(固定)
        // Ex:  A,20161003,15523434,1809254090,1809254090,05183,2233,172.17.8.195,61003,123456789,4283,37567
        #endregion

        #region 竹運客代
        string ErrStr = "";
        SqlCommand cmdhct = new SqlCommand();
        cmdhct.CommandText = "select customer_code, SUBSTRING(customer_code,4, 4) 'stationcode', customer_shortname from tbCustomers where supplier_code = '001'";
        DataTable dtHCT = dbAdapter.getDataTable(cmdhct);
        if (dtHCT != null && dtHCT.Rows.Count > 0)
        {
            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
            StringBuilder sb_temp_ins = new StringBuilder();
            for (int x = 0; x <= dtHCT.Rows.Count - 1; x++)
            {
                string stationcode = dtHCT.Rows[x]["stationcode"].ToString();
                string customer_code = dtHCT.Rows[x]["customer_code"].ToString();
                string wherestr = " and customer_code='" + customer_code + "'";
                wherestr += @" and scan.check_number in(" + Photo_check_number + ")";
                string ExecDate = DateTime.Now.AddHours(-1).ToString("yyyy-MM-dd");
                string ExecHour = DateTime.Now.AddHours(-1).ToString("HH");

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText  = string.Format(@"WITH scan AS(  
                                                 Select scan.*,
                                                 CASE dr.pricing_type WHEN '01' THEN dr.plates WHEN '02' THEN  dr.pieces  WHEN '03' THEN  dr.cbm WHEN '04' THEN  dr.plates ELSE 1 END AS drpieces  , 
                                                 ISNULL(dr.print_date, scan.scan_date ) as print_date , ISNULL(dr.HCTstatus,0) as HCTstatus , dr.request_id , 
                                                 ROW_NUMBER() OVER (PARTITION BY scan.check_number ORDER BY scan.scan_date DESC,scan.log_id DESC) AS rn 
                                                 from ttDeliveryScanLog scan
                                                 left join tcDeliveryRequests dr with(nolock) on scan.check_number = dr.check_number and dr.supplier_code in('001','002') and dr.print_date >=  DATEADD(MONTH,-3,getdate())
                                                 where scan.scan_item = '4' 
                                                 and LEN(scan.check_number) = 10 
                                                 and(not(scan.check_number between 8158486615 and 8158586601))
                                                 and(not(scan.check_number between 5920000000 and 5929999999))
                                                 and(not(scan.check_number between 8347120000 and 8347129999))
                                                 and(not(scan.check_number between 8819346760 and 8819406750))
                                                 and(not(scan.check_number between 9000200010 and 9000229999))
                                                 and(not(scan.check_number between 8283490025 and 8286490015))
                                                 --and ( ISNULL(dr.HCTstatus,0) <> 4 ) and ( ISNULL(dr.HCTstatus,0) <> 0 )
                                                 {0}
                                                 )
                                            select print_date,check_number, drpieces,scan_date, HCTstatus,sign_form_image, sign_field_image, request_id from scan where rn = 1", wherestr);
                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt != null && dt.Rows.Count > 0)
                {

                    string FileName = "A_" + stationcode + "_" + DateTime.Now.ToString("yyyyMMddHHmmssmmm") + "_junfu_05183" + DateTime.Today.ToString("yyyyMMdd").Substring(3);
                    string DirPath = ConfigurationManager.AppSettings["FTP_FOLDER"] + "IMGDATA5\\" + FileName;
                    if (Directory.Exists(DirPath) == false)
                    {
                        Directory.CreateDirectory(DirPath);
                    }


                    FileStream fs = new FileStream(DirPath + "\\" + FileName + ".txt", FileMode.Create);
                    //獲得位元組陣列
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        string request_id = dt.Rows[i]["request_id"].ToString();
                        int HCTstatus = Convert.ToInt32(dt.Rows[i]["HCTstatus"]);
                        string InsertText = "";
                        string scan_date = Convert.ToDateTime(dt.Rows[i]["scan_date"]).ToString("yyyyMMdd");
                        string scan_time = Convert.ToDateTime(dt.Rows[i]["scan_date"]).ToString("HHmmssmmm");
                        string check_number = dt.Rows[i]["check_number"].ToString();
                        string fileno = check_number;
                        string SD = "05183";
                        string station = stationcode;
                        string IP = "junfu"; //"172.17.8.195";
                        string date = DateTime.Today.ToString("yyyyMMdd").Substring(3);
                        string box = DateTime.Today.Year.ToString().Substring(3, 1) + "000" + DateTime.Today.Month.ToString().PadLeft(2, '0') + "001";
                        string imagetype = "4283";
                        string scan_psn = "00001";

                        //copy簽單jpg
                        InsertText = "";
                        string sign_form_image = dt.Rows[i]["sign_form_image"].ToString();   //大張
                        string sign_field_image = dt.Rows[i]["sign_field_image"].ToString(); //小張
                        if (sign_form_image != "" && HCTstatus != 5) //大張
                        {
                            using (WebClient wc = new WebClient())
                            {

                                try
                                {
                                    wc.DownloadFile(ConfigurationManager.AppSettings["systemurl"] + "JUNFU_APP_WebService/PHOTO/" + sign_form_image, DirPath + "\\" + check_number + ".jpg");
                                    InsertText = "A," + scan_date + "," + scan_time + "," + check_number + "," + fileno + "," + SD + "," + station + "," + IP + "," + date + ",2," + imagetype + "," + scan_psn + "\r\n";
                                    byte[] data = System.Text.Encoding.Default.GetBytes(InsertText);
                                    //開始寫入
                                    fs.Write(data, 0, data.Length);
                                    if (HCTstatus == 2)
                                    {
                                        HCTstatus = 2;    ///配達過但未正常配交，下次還是要繼續傳簽單
                                    }
                                    else if (HCTstatus == 6)
                                    {
                                        HCTstatus = 4;    ///大小張都傳完了
                                    }
                                    else
                                    {
                                        HCTstatus = 5;    //只傳大張
                                    }

                                }
                                catch (Exception ex)
                                {
                                    string ErrMsg = check_number + ex.Message;
                                    using (SqlCommand cmdlog = new SqlCommand())
                                    {
                                        cmdlog.Parameters.AddWithValue("@logdate", DateTime.Now);
                                        cmdlog.Parameters.AddWithValue("@check_number", check_number);
                                        cmdlog.Parameters.AddWithValue("@message", "大張簽單上傳錯誤:" + ErrMsg);
                                        cmdlog.Parameters.AddWithValue("@type", "簽單");
                                        cmdlog.CommandText = dbAdapter.SQLdosomething("tbHCTFTPLog", cmdlog, "insert");
                                        dbAdapter.execNonQuery(cmdlog);                                        

                                    }
                                }

                            }
                        }

                        InsertText = "";
                        if (sign_field_image != "" && HCTstatus != 6) //小張
                        {
                            using (WebClient wc = new WebClient())
                            {
                                try
                                {
                                    wc.DownloadFile(ConfigurationManager.AppSettings["systemurl"] + "JUNFU_APP_WebService/PHOTO/" + sign_field_image, DirPath + "\\" + check_number + "_001.jpg");
                                    InsertText = "A," + scan_date + "," + scan_time + "," + check_number + "," + fileno + "_001" + "," + SD + "," + station + "," + IP + "," + date + ",1," + imagetype + "," + scan_psn + "\r\n";
                                    byte[] data = System.Text.Encoding.Default.GetBytes(InsertText);
                                    //開始寫入
                                    fs.Write(data, 0, data.Length);
                                    if (HCTstatus == 2)
                                    {
                                        HCTstatus = 2;    ///配達過但未正常配交，下次還是要繼續傳簽單
                                    }
                                    else if (HCTstatus == 5)
                                    {
                                        HCTstatus = 4;   //大小張都傳完了
                                    }
                                    else
                                    {
                                        HCTstatus = 6;   //只傳小張
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ErrMsg = check_number + ex.Message;
                                    using (SqlCommand cmdlog = new SqlCommand())
                                    {
                                        cmdlog.Parameters.AddWithValue("@logdate", DateTime.Now);
                                        cmdlog.Parameters.AddWithValue("@check_number", check_number);
                                        cmdlog.Parameters.AddWithValue("@message", "小張簽單上傳錯誤:" + ErrMsg);
                                        cmdlog.Parameters.AddWithValue("@type", "簽單");
                                        cmdlog.CommandText = dbAdapter.SQLdosomething("tbHCTFTPLog", cmdlog, "insert");
                                        dbAdapter.execNonQuery(cmdlog);
                                    }
                                }

                            }
                        }

                        if (HCTstatus > 3)
                        {
                            if (request_id != "")
                            {
                                sb_temp_ins.Append("update tcDeliveryRequests  SET HCTstatus=" + HCTstatus.ToString() + " where check_number = '" + check_number + "' and request_id=" + request_id + "; ");
                                sb_temp_ins.Append("INSERT INTO [dbo].[tbHCTFTPLog] ([request_id],[logdate] ,[check_number] ,[type],[cuser]) VALUES (" + request_id + ", getdate() , '" + check_number + "','簽單','" + Session["account_code"].ToString() + "');");
                            }
                            else
                            {
                                sb_temp_ins.Append("INSERT INTO [dbo].[tbHCTFTPLog] ([logdate] ,[check_number] ,[type],[cuser]) VALUES (getdate() , '" + check_number + "','簽單','" + Session["account_code"].ToString() + "');");
                            }

                        }
                    }
                    //清空緩衝區、關閉流
                    fs.Flush();
                    fs.Close();

                    //壓縮檔案                    
                    ZipFiles(DirPath, FileName + "#kvittering", string.Empty);


                    #region 

                    
                    NetworkCredential credentials = new NetworkCredential("junfu", "6YDJM#");
                    string uploadurl = "ftp://hctrt.hct.com.tw/IMGDATA5/";
                    try
                    {
                        uploadFile.UploadFtpFile(uploadurl, credentials, ConfigurationManager.AppSettings["FTP_FOLDER"] + "IMGDATA5\\" + FileName + ".zip");
                    }
                    catch(Exception ex)
                    {
                        ErrStr = ex.Message;
                    }

                    #endregion

                }

            }


            #region 執行SQL,更新託運單hct回傳狀態 2:配達過但未正常配交  3:配達  4簽單大張張都已上傳  5只傳大張簽單 6只傳小張簽單， 
            if (sb_temp_ins.Length > 0)
            {
                String strSQL = sb_temp_ins.ToString();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = strSQL;
                    dbAdapter.execNonQuery(cmd);
                }
            }
            #endregion
        }
        #endregion

        return ErrStr;

    }


    /// <summary>
    /// 儲存為CSV檔
    /// </summary>
    /// <param name="oTable"></param>
    /// <param name="FilePath"></param>
    /// <param name="sb_temp_ins"></param>
    /// <param name="sb_ins_list"></param>
    public void SaveToCSV(DataTable oTable, string FilePath, ref StringBuilder sb_temp_ins, ref List<StringBuilder> sb_ins_list)
    {
        string data = "";
        System.Globalization.TaiwanCalendar twC = new System.Globalization.TaiwanCalendar();
        StreamWriter wr = new StreamWriter(FilePath, false, System.Text.Encoding.Default);
       
        //
        //欄位     掃讀日期      十碼貨號    件數     配達區分      配達日期     配達時間     配達註記
        //長度     8             10          4        2             8            8            1
        //說明     托運日期                           配達碼        西元年月日   HH24:MI:SS   Y:已配達
        //         民國年月日                         正常配交:1                              空白:其他			         
        //範例     1010208       1272820776  0001     01            20131210     08:05:05     Y

        //配達區分         竹運代碼                峻富代碼           HCTstatus
        //=======================================================================
        //             01  正常配交                  3                   3           
        //             02  缺件配交                  7                   3          
        //             03  破損配交                  8                   3
        //             04  客戶不在                  1                   2           
        //             05  地址錯誤                  5                   2  
        //             06  查無此人                  6                   2           
        //             07  短少配交                  9                   3
        //             09  正常配交(簽單未取回)      0                   3
        //             10  約定再配                  2                   2           
        //             12  超商店取                
        //             13  店取收回                  A                   3 
        //             14  店取收回異常              B                   3
        //             15  店配異常                
        //             16  拒收                      4                   3   
        //             17  公司行號休假                
        //             18  電聯異常                
        //             19  儲物櫃存放                                
        //             21  拒收退回(便利帶)              
        //             22  到站自領                
        //             23  拒收退回(便利帶)
      


        for (int i = 0; i <= oTable.Rows.Count - 1; i++)
        {
            string check_number = oTable.Rows[i]["check_number"].ToString().Trim();
            DateTime print_date = Convert.ToDateTime(oTable.Rows[i]["print_date"]);
            DateTime scan_date = Convert.ToDateTime(oTable.Rows[i]["scan_date"]);
            string arrive_option = oTable.Rows[i]["arrive_option"].ToString().Trim();   //配達區分 
            string request_id = oTable.Rows[i]["request_id"].ToString();
            string updatestr = "";
            string closecode = "";


            data += twC.GetYear(print_date) + print_date.ToString("MMdd") + "," +
                    check_number + "," +
                    oTable.Rows[i]["drpieces"].ToString().Trim().PadLeft(4, '0') + ",";

            switch (arrive_option)
            {
                case "1":  //客戶不在
                    data += "04" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=2 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "2": //約定再配
                          //data += "08" + ",";
                    data += "10" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=2 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "3":  //正常配交
                    data += "01" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    closecode = "Y";
                    break;
                case "4":  //拒收
                           //data += "02" + ",";
                    data += "16" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "5":  //地址錯誤
                           //data += "07" + ",";
                    data += "05" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=2 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "6":  //查無此人
                    data += "06" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=2 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "7":  //缺件配交
                           //data += "03" + ",";
                    data += "02" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "8":  //破損配交
                           //data += "05" + ",";
                    data += "03" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "9":  //短少配交
                           //data += "04" + ",";
                    data += "07" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "0":  //0簽單留置
                           //data += "01" + ",";
                    data += "09" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "A":  //店取收回
                           //data += "23" + ",";
                    data += "13" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
                case "B":  // 店取收回異常
                           //data += "24" + ",";
                    data += "14" + ",";
                    if (request_id != "")
                    {
                        updatestr = "update tcDeliveryRequests  SET HCTstatus=3 where check_number = '" + check_number + "' and ( HCTstatus IS NULL or HCTstatus < 3 ) and request_id=" + request_id + "; ";
                    }
                    break;
            }

            if (request_id != "")
            {
                updatestr += "INSERT INTO [dbo].[tbHCTFTPLog] ([request_id], [logdate] ,[check_number] ,[type] ,[cuser]) VALUES (" + request_id + ", getdate() , '" + check_number + "','配達','"+ Session["account_code"].ToString() + "');";
            }
            else
            {
                updatestr += "INSERT INTO [dbo].[tbHCTFTPLog] ( [logdate] ,[check_number] ,[type] ,[cuser]) VALUES ( getdate() , '" + check_number + "','配達','"+ Session["account_code"].ToString() + "');";
            }

            data += Convert.ToDateTime(oTable.Rows[i]["scan_date"]).ToString("yyyyMMdd") + "," +
                Convert.ToDateTime(oTable.Rows[i]["scan_date"]).ToString("HH:mm:ss") + "," +
                closecode;
            data += "\n";
            wr.Write(data);
            data = "";

            if (updatestr != "")
            {   
                sb_temp_ins.Append(updatestr);
                //每200筆組成一字串
                if ((i % 200 == 0 && i != 0) || i == oTable.Rows.Count - 1)
                {
                    sb_ins_list.Add(sb_temp_ins);
                    sb_temp_ins = new StringBuilder();
                }
            }

        }
        data += "\n";

        wr.Dispose();
        wr.Close();
    }

    /// <summary>
    /// 壓縮為zip檔
    /// </summary>
    /// <param name="path"></param>
    /// <param name="password"></param>
    /// <param name="comment"></param>
    private static void ZipFiles(string path, string password, string comment)
    {
        string zipPath = path + ".zip";
        ZipFile zip = new ZipFile();
        if (password != null && password != string.Empty) zip.Password = password;
        if (comment != null && comment != "") zip.Comment = comment;
        ArrayList files = GetFiles(path);
        foreach (string f in files)
        {
            zip.AddFile(f, string.Empty);//第二個參數設為空值表示壓縮檔案時不將檔案路徑加入
        }
        zip.Save(zipPath);
    }

    /// <summary>
    /// 讀取目錄下所有檔案
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    private static ArrayList GetFiles(string path)
    {
        ArrayList files = new ArrayList();

        if (Directory.Exists(path))
        {
            files.AddRange(Directory.GetFiles(path));
        }

        return files;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        
        check_numbers = "";
        MultiView1.ActiveViewIndex = 0;
    }
}
    