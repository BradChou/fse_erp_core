﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


public partial class system_11 : System.Web.UI.Page
{
    public string func_code = "S11";  //貨號區間
    public Auth _Auth
    {
        get { return (Auth)ViewState[this.ClientID + "_Auth"]; }
        set { ViewState[this.ClientID + "_Auth"] = value; }
    }
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            #region 權限
            _Auth = getAuth(Session["account_code"].ToString(), func_code, Less_than_truckload);
            btn_Add.Visible = _Auth.insert_auth;
            #endregion

            DDLSet();
            DefaultData();
        }
    }

    [Serializable]
    public class Auth
    {
        public int auth_id { get; set; }
        public string func_code { get; set; }
        public string func_name { get; set; }
        public bool view_auth { get; set; }
        public bool insert_auth { get; set; }
        public bool modify_auth { get; set; }
        public bool delete_auth { get; set; }

        public Auth()
        {
            view_auth = false;
            insert_auth = false;
            modify_auth = false;
            delete_auth = false;
        }
    }


    public Auth getAuth(string account_code, string func_code, string type = "0")
    {
        string wherestr = string.Empty;
        string wherestr2 = string.Empty;
        Auth _Auth = new Auth();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@account_code", account_code);
            cmd.Parameters.AddWithValue("@type", type);
            if (func_code != "")
            {
                cmd.Parameters.AddWithValue("@func_code", func_code);
                wherestr += " and func_code like '' + @func_code + '%'";
                wherestr2 += " and func.func_code like '' + @func_code + '%'";
                wherestr2 += " and func.func_code <> 'TA'";
                cmd.CommandText = string.Format(@"declare @@count int;
                                                  declare @@manager_type nvarchar(2);
                                                  select @@count =count(*) from tbFunctionsAuth where account_code  = @account_code {0};
                                                  IF (@@count =0) BEGIN
                                                  select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                  select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type and auth.func_type =@type
                                                   where 0=0 and func.type=@type  {1}
                                                   order by func.sort ,func.level
                                                  END ELSE BEGIN 
                                                   select func.func_code , 
                                                   func.func_name,
                                                   func.upper_level_code , func.func_link,func.level, func.cssclass,
                                                   auth. auth_id , auth.manager_type , auth.account_code , 
                                                   isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                   isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                   auth.uuser , auth.udate 
                                                   from tbFunctions func with(nolock)
                                                   left join tbFunctionsAuth auth with(nolock) on auth.func_code = func.func_code  and auth.account_code  = @account_code and auth.func_type =@type
                                                   where 0=0 and func.type=@type {2}
                                                   order by func.sort,func.level 
                                                  END; ", wherestr, wherestr2, wherestr2);

                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt != null && dt.Rows.Count > 0) _Auth = DataTableExtensions.ToList<Auth>(dt).ToList().FirstOrDefault();

            }
        }
        return _Auth;
    }


    protected void DDLSet()
    {

        String strSQL = @"SELECT code_id id,code_name name FROM tbItemCodes item With(Nolock) WHERE item.code_sclass=N'H1' AND active_flag IN(1)";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = strSQL;
            DataTable dt = dbAdapter.getDataTable(cmd);
        }

        ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));
       

        ddl_station.DataSource = Utility.getArea_Arrive_Code(true);
        ddl_station.DataValueField = "station_code";
        ddl_station.DataTextField = "showname";
        ddl_station.DataBind();
        ddl_station.Items.Insert(0, new ListItem("--站別--", ""));
        ddl_station.SelectedValue = "";




        ddl_staion.DataSource = Utility.getArea_Arrive_Code(true);
        ddl_staion.DataValueField = "station_code";
        ddl_staion.DataTextField = "showname";
        ddl_staion.DataBind();
        ddl_staion.Items.Insert(0, new ListItem("請選擇", ""));
        ddl_staion.SelectedValue = "";

       

    }


    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
            if (Request.QueryString["num"] != null)
            {
                tb_searchNum.Text = Request.QueryString["num"];
                querystring += "&num=" + tb_searchNum.Text;
            }

            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }


            if (Request.QueryString["station"] != null)
            {
                ddl_station.SelectedValue = Request.QueryString["station"];
                querystring += "&station=" + ddl_station.SelectedValue;
            }

            #endregion



            string strWhere = "";



            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY emp.customer_code,emp.seq) AS num,emp.seq,emp.customer_code,emp.Cdate
                             ,emp.cuser
                             ,emp.begin_number
                             ,emp.end_number
                             ,emp.current_number
                             ,CASE emp.IsActive WHEN '1' THEN '是' ELSE '否' END IsActive 
                             ,D.user_name 
                             ,E.station_name 
                             ,item.customer_name
                         FROM tbDeliveryNumberSetting emp With(Nolock)
                    LEFT JOIN tbCustomers item With(Nolock) ON emp.customer_code = item.customer_code 
                    LEFT JOIN tbStation  E With(Nolock) ON item.supplier_code = E.station_code
                    LEFT JOIN tbAccounts  D With(Nolock) ON D.account_code = emp.Cuser
                      where 1=1   ";




            #region 查詢條件
           

           if (tb_searchNum.Text.Trim().Length > 0  )
           {
               
                strWhere += " AND (@num between begin_number AND  End_number)";
                cmd.Parameters.AddWithValue("@num", tb_searchNum.Text.Trim());
               
            }
            if (tb_search.Text != "")
            {

              
                cmd.Parameters.AddWithValue("@Customer_code", tb_search.Text);
                cmd.Parameters.AddWithValue("@Customer_name", tb_search.Text);
                strWhere += " AND(item.Customer_code like '%'+@Customer_code+'%' or item.Customer_name like '%'+@Customer_name+'%') ";
               
            }




            if (ddl_active.SelectedValue != "")
            {

                strWhere += " AND emp.IsActive IN (" + ddl_active.SelectedValue.ToString() + ")";
              
            }

            if (ddl_station.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", ddl_station.SelectedValue.ToString());
                strWhere += " AND item.supplier_code =@supplier_code";

            }

            //if (customer_code != "")
            //{
            //    cmd.Parameters.AddWithValue("@customer_code", customer_code);
            //    strWhere += " and customer_code LIKE N''+@customer_code+'%'";
            //}


            #endregion

            cmd.CommandText = string.Format(strSQL + " {0}", strWhere);


            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }
    

    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "active=" + ddl_active.SelectedValue
                          + "&station=" + ddl_station.SelectedValue;

        if (!string.IsNullOrEmpty(tb_searchNum.Text))
        {
            str_Query += "&num=" + tb_searchNum.Text;
        }
        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/system_11.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("emp_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢視(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        ddl_staion.Enabled = true ;
        ddl_Customer.Enabled = true;
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "貨號區間-新增";
                pan_edit.Visible = true;
                pan_view.Visible = false;
                lbl_id.Text = "0";

                ddl_staion.SelectedValue = "";
                Bnumber.Text = "";
                Enumber.Text = "";
                rb_Active.SelectedValue = "1";
                ddl_Customer.Items.Clear();
                #endregion

                break;
            case 2:
                ddl_staion.Enabled = false;
                ddl_Customer.Enabled = false;
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "貨號區間-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = "";

                        strSQL = @"SELECT emp.customer_code
                             ,emp.begin_number
                             ,emp.end_number
                             ,emp.IsActive 
                             ,E.station_code 
                             ,item.customer_shortname
                         FROM tbDeliveryNumberSetting emp With(Nolock)
                    LEFT JOIN tbCustomers item With(Nolock) ON emp.customer_code = item.customer_code 
                    LEFT JOIN tbStation  E With(Nolock) ON item.supplier_code = E.station_code
                        WHERE seq = @seq ";

                        cmd.Parameters.AddWithValue("@seq", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];

                                Bnumber.Text = row["begin_number"].ToString();
                                Enumber.Text = row["end_number"].ToString();
                                CustomerChanged(row["station_code"].ToString());
                                ddl_staion.SelectedValue = row["station_code"].ToString();
                                ddl_Customer.SelectedValue = row["Customer_code"].ToString();
                                rb_Active.SelectedValue = Convert.ToInt16(row["IsActive"]).ToString();
                                lbl_id.Text = id.ToString();
                            }
                        }
                    }

                    pan_edit.Visible = true;
                    pan_view.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "貨號區間";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";



        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;



        //驗證貨號起訖大小    
        if (Int64.Parse(Bnumber.Text) >= Int64.Parse(Enumber.Text))
        {
            IsOk = false;
            RetuenMsg("貨號區間起必須小於貨號區間迄,請重新確認!");
        }



        //驗證貨號區間    
        //GetValueFromSQL

        if (IsEdit)
        {
            strSQL = "SELECT COUNT(*) FROM  tbDeliveryNumberSetting  With(Nolock) ";
            strSQL += "WHERE  (@begin_number between begin_number And  end_number ";
            strSQL += "OR    @end_number between begin_number And  end_number)   and IsActive=1   and seq <>" + i_id.ToString();
        }
        else
        {
            strSQL = "SELECT COUNT(*) FROM  tbDeliveryNumberSetting  With(Nolock) ";
            strSQL += "WHERE  (@begin_number between begin_number And  end_number ";
            strSQL += "OR    @end_number between begin_number And  end_number)  and IsActive=1 ";
        }

        if (IsOk && GetValueFromSQL(strSQL) > 0)
        {
            IsOk = false;
            RetuenMsg("貨號區間重覆,請重新確認!");
        }



        if (!IsOk) return;

        #region 存檔
        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {
    
                if (IsEdit)
                {
                    cmd.Parameters.AddWithValue("@Customer_code", ddl_Customer.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@IsActive", rb_Active.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", strUser);
                    cmd.Parameters.AddWithValue("@begin_number", Bnumber.Text.ToString());
                    cmd.Parameters.AddWithValue("@end_number", Enumber.Text.ToString());
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm("tbDeliveryNumberSetting", cmd);
                    dbAdapter.execNonQuery(cmd);
                    SetEditOrAdd(0);
                    RetuenMsg("修改成功!");
                }
                else
                {


                    cmd.Parameters.AddWithValue("@Customer_code", ddl_Customer.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@IsActive", rb_Active.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", strUser);
                    cmd.Parameters.AddWithValue("@begin_number", Bnumber.Text.ToString());
                    cmd.Parameters.AddWithValue("@end_number", Enumber.Text.ToString());
                    cmd.CommandText = dbAdapter.genInsertComm("tbDeliveryNumberSetting", true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                    SetEditOrAdd(0);
                    RetuenMsg("新增成功!");
                }

                

            }
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                cmd.Parameters.AddWithValue("@begin_number", Bnumber.Text.Trim());
                cmd.Parameters.AddWithValue("@end_number", Enumber.Text.Trim());
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }
        return result;
    }
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }

    protected void Customer_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlstation = (DropDownList)sender;
        DropDownList dlcustomer = null;
        dlcustomer = ddl_Customer;
        if (dlstation.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@station", dlstation.SelectedValue.ToString());
            cmda.CommandText = @"Select customer_code,customer_name  , customer_code + '-' + customer_name 'showname' 
                                 from tbCustomers With(Nolock) 
                                 where supplier_code =@station  AND type=1  AND stop_shipping_code = 0    
                                 order by customer_id asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                dlcustomer.DataSource = dta;
                dlcustomer.DataValueField = "customer_code";
                dlcustomer.DataTextField = "showname";
                dlcustomer.DataBind();
            }
        }
        else
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
        }
    }

    protected void CustomerChanged(string station)
    {

        DropDownList dlcustomer = null;
        dlcustomer = ddl_Customer;
        if (station != "")
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@station", station);
            cmda.CommandText = "Select customer_code,customer_name from tbCustomers With(Nolock) where supplier_code =@station  AND type=1  AND stop_shipping_code = 0    order by customer_id asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlcustomer.Items.Add(new ListItem(dta.Rows[i]["customer_code"].ToString().Trim() + dta.Rows[i]["customer_name"].ToString().Trim(), dta.Rows[i]["customer_code"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlcustomer.Items.Clear();
            dlcustomer.Items.Add(new ListItem("請選擇", ""));
        }
    }



}
