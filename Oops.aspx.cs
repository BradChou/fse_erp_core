﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Oops : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String strErrTitle = "系統發生錯誤!";
        String strErr = "系統資訊異常，請通知系統管理人員。";

        if (Request.QueryString["err_title"] != null) strErrTitle = Request.QueryString["err_title"];
        if (Request.QueryString["err_msg"] != null) strErr = Request.QueryString["err_msg"];

        lbl_title.Text = "Sorry~ " + strErrTitle;
        lbl_msg.Text = strErr;
    }
}