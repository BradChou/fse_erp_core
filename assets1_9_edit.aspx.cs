﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class assets1_9_edit : System.Web.UI.Page
{
    
    public string id
    {
        get { return ViewState["id"].ToString(); }
        set { ViewState["id"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() : "";

           

            #region 險種
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select * from tbItemCodes with(nolock) where code_bclass= '6' and code_sclass= 'IK' and active_flag= 1 ";
            dlkind.DataSource = dbAdapter.getDataTable(cmd);
            dlkind.DataValueField = "code_id";
            dlkind.DataTextField = "code_name";
            dlkind.DataBind();
            dlkind.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion

            
            readdata(id);

        }
    }
    public void Sdate_TextChanged(object sender, EventArgs e)
    {
        //DateTime getDate = Convert.ToDateTime(Sdate.ToString()).AddYears(1).AddDays(-1);
        Edate.Text = Convert.ToDateTime(Sdate.Text.ToString()).AddYears(1).AddDays(-1).ToString("yyyy/MM/dd");
    }

    private void readdata(string a_id)
    {  
        if (a_id != "" )
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@id", id);
            
            cmd.CommandText = @"Select A.*, B.car_license from  ttInsurance A with(nolock)
                                LEFT JOIN  ttAssets B with(nolock) on A.assets_id = B.a_id
                                Where  A.id = @id ";
            dt = dbAdapter.getDataTable(cmd);

            if (dt.Rows.Count > 0)
            {
                car_license.Text = dt.Rows[0]["car_license"].ToString().Trim();
                form_id.Text = dt.Rows[0]["form_id"].ToString().Trim();
                Insurance_name.Text = dt.Rows[0]["Insurance_name"].ToString().Trim();
                dlkind.SelectedValue = dt.Rows[0]["kind"].ToString().Trim();
                Sdate.Text = Convert.ToDateTime(dt.Rows[0]["sdtate"]).ToString("yyyy/MM/dd");
                Edate.Text = Convert.ToDateTime(dt.Rows[0]["edate"]).ToString("yyyy/MM/dd");
                price.Text = dt.Rows[0]["price"].ToString().Trim();
                apportion.Text = dt.Rows[0]["apportion"].ToString().Trim();
                apportion_price.Text = dt.Rows[0]["apportion_price"].ToString().Trim();
                amount_paid.Text = dt.Rows[0]["amount_paid"].ToString().Trim();
                memo.Text = dt.Rows[0]["memo"].ToString().Trim();
                
                using (SqlCommand cmdpay = new SqlCommand())
                {
                    cmdpay.Parameters.AddWithValue("@Insurance_id", id);
                    cmdpay.CommandText = @"Select ROW_NUMBER() OVER(ORDER BY year, month ) 'row_id', *
                                           FROM  ttInsuranceRecord  with(nolock)
                                           WHERE  Insurance_id = @Insurance_id 
                                           order by year, month";

                    DataTable dtpay = dbAdapter.getDataTable(cmdpay);
                    New_List.DataSource = dtpay;
                    New_List.DataBind();
                    
                    //temp 
                    if (dtpay.Rows.Count == 0)
                    {
                        DateTime sdate = Convert.ToDateTime(dt.Rows[0]["sdtate"]);
                        DateTime edate = Convert.ToDateTime(dt.Rows[0]["edate"]);
                        int idx = 1;
                        int Iprice = Convert.ToInt32(dt.Rows[0]["price"]);
                        int Iapportion_price = Convert.ToInt32(dt.Rows[0]["apportion_price"]);
                        using (SqlCommand cmd_record = new SqlCommand())
                        {
                            StringBuilder sb_temp_add = new StringBuilder();
                            do
                            {   
                                cmd_record.Parameters.AddWithValue("@Insurance_id" + idx.ToString(), id);
                                cmd_record.Parameters.AddWithValue("@year" + idx.ToString(), sdate.Year);
                                cmd_record.Parameters.AddWithValue("@month" + idx.ToString(), sdate.Month);
                                if (sdate.Year == edate.Year && sdate.Month == edate.Month)
                                {
                                    cmd_record.Parameters.AddWithValue("@price" + idx.ToString(), Iprice);   //除不進算在最後一個月
                                }
                                else
                                {
                                    cmd_record.Parameters.AddWithValue("@price" + idx.ToString(), Iapportion_price);
                                }
                                string addstr = "INSERT INTO ttInsuranceRecord([Insurance_id],[year],[month],[price]) VALUES(@Insurance_id" + idx.ToString() + ",@year" + idx.ToString() + ",@month" + idx.ToString()+ ",@price" + idx.ToString()+ ")";
                                Iprice -= Iapportion_price;
                                sb_temp_add.Append(addstr);
                                sdate = sdate.AddMonths(1);
                                idx += 1;
                            } while (sdate < edate);
                            
                            if (sb_temp_add.Length > 0)
                            {
                                cmd_record.CommandText = sb_temp_add.ToString();
                                try
                                {
                                    dbAdapter.execNonQuery(cmd_record);
                                }
                                catch (Exception ex)
                                {  
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            ApStatus = "Add";
        }
        
        SetApStatus(ApStatus);

    }

    private void SetApStatus(string ApStatus)
    {   
        switch (ApStatus)
        {
            case "Modity":
                car_license.ReadOnly  = true ;
                form_id.ReadOnly = true;
                apportion_price.ReadOnly = true;
                apportion.ReadOnly = true;
                price.ReadOnly = true;
                btnavg.Visible = false;
                //carsel.Visible = false;
                statustext.Text = "修改";
                break;
            case "Add":
                apportion.Text = "12";
                statustext.Text = "新增";
                break;
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":
                #region 判斷重覆
                if (form_id.Text != "" && Hid_assets_id.Value != "")
                {
                    SqlCommand cmda = new SqlCommand();
                    DataTable dta = new DataTable();
                    cmda.Parameters.AddWithValue("@form_id", form_id.Text);
                    cmda.Parameters.AddWithValue("@assets_id", Hid_assets_id.Value);
                    cmda.CommandText = "Select id from ttInsurance with(nolock) where form_id=@form_id and assets_id=@assets_id";
                    dta = dbAdapter.getDataTable(cmda);
                    if (dta.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                        return;
                    }
                }                
                #endregion

                SqlCommand cmdadd = new SqlCommand();
                cmdadd.Parameters.AddWithValue("@serial", form_id.Text);
                cmdadd.Parameters.AddWithValue("@assets_id", Hid_assets_id.Value);
                cmdadd.Parameters.AddWithValue("@Insurance_name", Insurance_name.Text);
                cmdadd.Parameters.AddWithValue("@kind", dlkind.SelectedValue);
                cmdadd.Parameters.AddWithValue("@sdtate", Sdate.Text);
                cmdadd.Parameters.AddWithValue("@edate", Edate.Text);
                cmdadd.Parameters.AddWithValue("@price", price.Text);
                cmdadd.Parameters.AddWithValue("@apportion_price", apportion_price.Text);
                cmdadd.Parameters.AddWithValue("@apportion", apportion.Text);
                cmdadd.Parameters.AddWithValue("@memo", memo.Text);
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdadd.CommandText = dbAdapter.SQLdosomething("ttInsurance", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;
            case "Modity":
                int Totpay = 0;  //累計攤提金額
                #region 繳費記錄
                if (New_List.Items.Count > 0)
                {
                    for (int i = 0; i <= New_List.Items.Count - 1; i++)
                    {
                        CheckBox cbpay = ((CheckBox)New_List.Items[i].FindControl("cbpay"));
                        Literal Liprice = ((Literal)New_List.Items[i].FindControl("Liprice"));
                        int _price = 0;
                        string r_id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value;
                        if (cbpay.Enabled)
                        {
                            SqlCommand cmd = new SqlCommand();
                            cmd.Parameters.AddWithValue("@ispay", cbpay.Checked);
                            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", r_id);
                            cmd.CommandText = dbAdapter.genUpdateComm("ttInsuranceRecord", cmd);
                            dbAdapter.execNonQuery(cmd);
                        }
                        if ( cbpay.Checked &&  int.TryParse(Liprice.Text, out _price))
                        {
                            Totpay += _price;
                        }
                    }
                }

                #endregion

                SqlCommand cmdupd = new SqlCommand();
                cmdupd.Parameters.AddWithValue("@Insurance_name", Insurance_name.Text);
                cmdupd.Parameters.AddWithValue("@kind", dlkind.SelectedValue);
                cmdupd.Parameters.AddWithValue("@sdtate", Sdate.Text);
                cmdupd.Parameters.AddWithValue("@edate", Edate.Text);
                cmdupd.Parameters.AddWithValue("@price", price.Text);
                cmdupd.Parameters.AddWithValue("@amount_paid", Totpay);
                cmdupd.Parameters.AddWithValue("@apportion", apportion.Text);
                cmdupd.Parameters.AddWithValue("@memo", memo.Text);
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
                cmdupd.CommandText = dbAdapter.genUpdateComm("ttInsurance", cmdupd);
                try
                {
                    dbAdapter.execNonQuery(cmdupd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;

        }

        

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='assets1_2.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }        
        return;


    }

    

}