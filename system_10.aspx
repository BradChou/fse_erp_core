﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_10.aspx.cs" Inherits="system_10" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/junfucss.css">
    <link rel="stylesheet" href="css/simplePagination.css">
    <script src="js/jquery.simplePagination.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });


    </script>

    <style type="text/css">
        .td_th {
            text-align: center;
        }

        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">區配商設定</h2>
            <div class="form-group form-inline">
                <label>關鍵字：</label>
                <asp:TextBox ID="keyword" class="form-control " runat="server"></asp:TextBox>
                <span class="checkbox checkbox-success" style="left: 0px; top: 0px">
                    <asp:CheckBox ID="chk_supplier" class="font-weight-400" runat="server" Text="區配商" />
                </span>
                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />   
                <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1"   />
                <a href="system_10-edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>             
            </div>
            
            <div class="col-lg-10">
                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr>
                        <th class="td_th ">No</th>
                        <th class="td_th ">區配編號</th>
                        <th class="td_th ">供應商編號</th>
                        <th class="td_th ">名稱</th>
                        <th class="td_th ">發票抬頭</th>
                        <th class="td_th ">負責人</th>
                        <th class="td_th ">狀態</th>
                        <th class="td_th">更新人員</th>
                        <th class="td_th">更新日期</th>
                        <th class="td_th ">功能</th>
                    </tr>
                    <asp:Repeater ID="list_supplier" runat="server" OnItemCommand="list_supplier_ItemCommand">
                        <ItemTemplate>
                            <tr class="paginate">
                                <td data-th="No"><%# Container.ItemIndex +1 %></td>
                                <td data-th="供應商編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_code").ToString())%></td>
                                <td data-th="區配編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_no").ToString())%></td>
                                <td data-th="名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_shortname").ToString())%></td>
                                <td data-th="發票抬頭"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                <td data-th="負責人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.principal").ToString())%></td>
                                <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.s_status").ToString())%></td>
                                <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td data-th="功　　能">
                                    <asp:HiddenField ID="Hid_supplier_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_id").ToString())%>' />
                                    <div class=" form-group form-inline  ">
                                        <%--<asp:RadioButton ID="rbyes" runat="server" GroupName="show_trans" CssClass="radio radio-success" Checked='<%# Convert.ToInt32(Eval("show_trans"))==1 ? true:false %>' Text="是" />
                                        <asp:RadioButton ID="rbno" runat="server" GroupName="show_trans" CssClass="radio radio-success" Checked='<%# Convert.ToInt32(Eval("show_trans"))==1 ? false:true %>' Text="否" />--%>
                                        <asp:Button ID="btn_Stop" CssClass="btn btn-danger" CommandName="cmdStop"
                                            Visible='<%# Convert.ToInt32(Eval("show_trans"))==1?true:false %>' runat="server" Text="停用" />
                                        <asp:Button ID="btn_Show" CssClass="btn btn-success" CommandName="cmdActive"
                                            Visible='<%# Convert.ToInt32(Eval("show_trans"))==0?true:false %>' runat="server" Text="啟用" />
                                        
                                        <asp:Button ID="btn_Edit" CssClass="btn btn-success" CommandName="cmdEdit" runat="server" Text="編輯" Visible =<%# Convert.ToInt32(Eval("show_trans"))==1?true:false %> />
                                        
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_supplier.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="9" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
            </div>
            <div class="col-lg-2">
            </div>

        </div>
        共 <span class="text-danger">
            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
        </span>筆
                <div id="page-nav" class="page"></div>

    </div>



</asp:Content>

