﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Security.Cryptography;

public partial class system_1 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string jobTypeShowSDMDDropList = "司機職";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            if (Less_than_truckload == "1")
            {
                ddl_supplier.Visible = false;
                ddl_station.Visible = true;
            }
            else
            {
                ddl_supplier.Visible = true;
                ddl_station.Visible = false;
            }

            DDLSet();
            DefaultData();
        }
    }

    protected void DDLSet()
    {

        String strSQL = @"SELECT code_id id,code_name name FROM tbItemCodes item With(Nolock) WHERE item.code_sclass=N'H1' AND active_flag IN(1)";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = strSQL;
            DataTable dt = dbAdapter.getDataTable(cmd);
            ddl_job_type.DataSource = dt;
            ddl_job_type.DataValueField = "id";
            ddl_job_type.DataTextField = "name";
            ddl_job_type.DataBind();
            ddl_job_type.Items.Insert(0, new ListItem("全部", ""));


            ddl_job_edit.DataSource = dt;
            ddl_job_edit.DataValueField = "id";
            ddl_job_edit.DataTextField = "name";
            ddl_job_edit.DataBind();
            ddl_job_edit.Items.Insert(0, new ListItem("請選擇", "0"));
            dt.Dispose();
        }

        ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));
        manager_type = Session["manager_type"].ToString(); //管理單位類別  
        customer_code = Session["master_code"].ToString();
        string supplier_code = Session["master_code"].ToString();
        switch (manager_type)
        {
            case "0":
            case "1":
                customer_code = "";
                break;
            case "2":
                customer_code = "000000";
                break;
        }

        if (Less_than_truckload == "1")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = @"select * from tbStation with(nolock) where active_flag = '1' order by station_scode";
                DataTable dt = dbAdapter.getDataTable(cmd);
                ddl_station.DataSource = dt;
                ddl_station.DataValueField = "id";
                ddl_station.DataTextField = "station_name";
                ddl_station.DataBind();
                ddl_station.Items.Insert(0, new ListItem("請選擇", ""));
            }
        }
        else
        {
            ddl_supplier.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
            ddl_supplier.DataValueField = "supplier_code";
            ddl_supplier.DataTextField = "showname";
            ddl_supplier.DataBind();
            if (ddl_supplier.Items.Count > 0) ddl_supplier.SelectedIndex = 0;
        }


        ddl_customer_code.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
        ddl_customer_code.DataValueField = "supplier_code";
        ddl_customer_code.DataTextField = "showname";
        ddl_customer_code.DataBind();
        //if (manager_type == "0" || manager_type == "1") ddl_supplier.Items.Insert(0, new ListItem("全部", ""));

        lbstation.Visible = Less_than_truckload == "1";
        ddl_staion.Visible = Less_than_truckload == "1";

        #region 到著站簡碼
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = string.Format(@" SELECT id, station_code, station_code + ' ' +  station_name as showname, station_scode, station_scode + ' ' +  station_name as showsname    from tbStation with(nolock)
                                            WHERE ISNULL(station_scode,'') <> '' and station_code <>'F53' and station_code <>'F71' 
                                            ORDER BY  station_scode");

            DataTable dt = dbAdapter.getDataTable(cmd);
            ddl_staion.DataSource = dt;
            ddl_staion.DataValueField = "id";
            ddl_staion.DataTextField = "showsname";
            ddl_staion.DataBind();
            ddl_staion.Items.Insert(0, new ListItem("請選擇", ""));
        }


        #endregion
    }


    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";
            if (Request.QueryString["job"] != null)
            {
                ddl_job_type.SelectedValue = Request.QueryString["job"];
                querystring += "&job=" + ddl_job_type.SelectedValue;
            }
            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }

            if (Request.QueryString["supplier"] != null)
            {
                ddl_supplier.SelectedValue = Request.QueryString["supplier"];
                querystring += "&supplier=" + ddl_supplier.SelectedValue;
            }

            if (Request.QueryString["station_id"] != null)
            {
                ddl_station.SelectedValue = Request.QueryString["station_id"];
                querystring += "&station_id=" + ddl_station.SelectedValue;
            }

            #endregion


            string strWhere = "";
            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY emp.emp_id) AS num,emp.emp_id,emp.emp_code,emp.emp_name
                             ,item.code_name job
                            -- ,emp.account_code
                             ,emp.driver_code
                             ,CASE emp.active_flag WHEN '1' THEN '是' ELSE '否' END IsActive 
                             ,D.user_name , emp.udate
                         FROM tbEmps emp With(Nolock)
                    LEFT JOIN tbItemCodes item With(Nolock) ON emp.job_type = item.code_id AND item.code_sclass=N'H1' 
                    left join tbAccounts  D With(Nolock) on D.account_code = emp.uuser
                        WHERE 1=1 ";
            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND (emp.emp_code LIKE N'%'+@search+'%' OR emp.emp_name LIKE N'%'+@search+'%') ";
                cmd.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }

            if (ddl_job_type.SelectedValue != "")
            {
                strWhere += " AND emp.job_type IN (" + ddl_job_type.SelectedValue.ToString() + ")";
            }

            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND emp.active_flag IN (" + ddl_active.SelectedValue.ToString() + ")";
            }

            if (Less_than_truckload == "1")
            {
                var d = ddl_station.SelectedItem;
                if (ddl_station.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@station_id", ddl_station.SelectedValue.ToString());
                    strWhere += " and station = @station_id";
                }
            }
            else
            {
                if (ddl_supplier.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", ddl_supplier.SelectedValue.ToString());
                    strWhere += " and emp.customer_code LIKE N''+@supplier_code+'%'";
                }
            }

            //if (customer_code != "")
            //{
            //    cmd.Parameters.AddWithValue("@customer_code", customer_code);
            //    strWhere += " and emp.customer_code LIKE N''+@customer_code+'%'";
            //}
            #endregion

            cmd.CommandText = string.Format(strSQL + " {0} order by emp.emp_id", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }

    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "job=" + ddl_job_type.SelectedValue
                       + "&active=" + ddl_active.SelectedValue;
        if (Less_than_truckload == "1")
        {
            str_Query += "&station_id=" + ddl_station.SelectedValue;
        }
        else
        {
            str_Query += "&supplier=" + ddl_supplier.SelectedValue;
        }

        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/system_1.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("emp_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }

    protected void ddl_job_edit_OnChanged(object sender, EventArgs e)
    {
        if (ddl_job_edit.SelectedItem.Text.ToString() != jobTypeShowSDMDDropList)
        {
            lbSDMDTitle.Visible = false;
            RadioButtonListSDMD.Visible = false;
            SDMDList.Visible = false;
        }
        else
        {
            lbSDMDTitle.Visible = true;
            RadioButtonListSDMD.Visible = true;

            var j = RadioButtonListSDMD.SelectedItem;
            if (RadioButtonListSDMD.SelectedItem != null)
            {
                SDMDList.Visible = true;
            }
        }
    }

    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢示(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        int i_tmp = 0;
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "人員基本資料-新增";
                pan_edit.Visible = true;
                pan_view.Visible = false;
                lbl_id.Text = "0";

                emp_code.Text =
                emp_name.Text =
                driver_code.Text = "";
                rb_Active.SelectedValue = "1";
                ddl_job_edit.SelectedValue = "0";
                ddl_staion.SelectedValue = "";
                #endregion

                break;
            case 2:
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "人員基本資料-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @" SELECT emp_id,emp_code,emp_name,job_type,account_code,driver_code,active_flag,customer_code,station
                                             FROM tbEmps With(Nolock) WHERE emp_id = @emp_id";
                        cmd.Parameters.AddWithValue("@emp_id", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                ddl_customer_code.SelectedValue = row["customer_code"].ToString();
                                emp_code.Text = row["emp_code"].ToString();
                                emp_name.Text = row["emp_name"].ToString();

                                if (!int.TryParse(row["job_type"].ToString(), out i_tmp)) i_tmp = 0;
                                ddl_job_edit.SelectedValue = i_tmp.ToString();

                                driver_code.Text = row["driver_code"].ToString().Trim();

                                rb_Active.SelectedValue = Convert.ToInt16(row["active_flag"]).ToString();
                                ddl_staion.SelectedValue = row["station"].ToString().Trim();

                                lbl_id.Text = id.ToString();

                                if (ddl_job_edit.SelectedItem.Text.ToString() != jobTypeShowSDMDDropList)
                                {
                                    lbSDMDTitle.Visible = false;
                                    RadioButtonListSDMD.Visible = false;
                                    SDMDList.Visible = false;
                                }
                                else
                                {
                                    lbSDMDTitle.Visible = true;
                                    RadioButtonListSDMD.Visible = true;
                                    SDMDList.Visible = true;
                                }
                            }
                        }
                    }

                    pan_edit.Visible = true;
                    pan_view.Visible = false;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @"select driver_Type, driver_type_code from tbDrivers where driver_code = @driver_code";
                        cmd.Parameters.AddWithValue("@driver_code", driver_code.Text);
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {

                                if (dt.Rows[0]["driver_Type"].ToString() == "SD")
                                {
                                    RadioButtonListSDMD.SelectedValue = "0";
                                    SDMDList.Visible = true;
                                    RadioSDMDProgram();
                                    if (dt.Rows[0]["driver_type_code"] != null)
                                    {
                                        SDMDList.SelectedValue = dt.Rows[0]["driver_type_code"].ToString();
                                    }
                                }
                                else if (dt.Rows[0]["driver_Type"].ToString() == "MD")
                                {
                                    RadioButtonListSDMD.SelectedValue = "1";
                                    SDMDList.Visible = true;
                                    RadioSDMDProgram();
                                    if (dt.Rows[0]["driver_type_code"] != null)
                                    {
                                        SDMDList.SelectedValue = dt.Rows[0]["driver_type_code"].ToString();
                                    }
                                }
                                else
                                {
                                    RadioButtonListSDMD.SelectedValue = null;
                                    RadioSDMDProgram();
                                    SDMDList.Visible = false;
                                }
                            }
                        }
                    }
                }
                #endregion

                break;
            default:
                lbl_title.Text = "人員基本資料";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";
        bool isSDMDCorrectly = false;           //判斷是否有同站所的重複SDMD編號
        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;

        //驗證        
        if (new string[] { "0", "" }.Contains(ddl_job_edit.SelectedValue.ToString()))
        {
            IsOk = false;
            RetuenMsg("請選擇職屬歸類!");
        }
        //全速配司機站所必須選擇
        if (ddl_customer_code.SelectedValue == "Z01" & ddl_job_edit.SelectedValue == "3" & ddl_staion.SelectedValue == "")
        {
            IsOk = false;
            RetuenMsg("請選擇所屬站所!");
        }

        //客戶代碼 不可重覆
        //GetValueFromSQL

        strSQL = "SELECT COUNT(1) FROM  tbEmps With(Nolock) WHERE customer_code like '" + ddl_customer_code.SelectedValue + "%' and emp_code IN('" + emp_code.Text.Trim() + "')　";

        if (IsEdit) strSQL += " AND emp_id NOT IN('" + i_id.ToString() + "') ";

        if (IsOk && GetValueFromSQL(strSQL) > 0)
        {
            IsOk = false;
            RetuenMsg("客戶代碼不可重覆,請重新確認!");
        }

        if (!IsOk) return;

        #region 存檔
        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {
                using (SqlCommand cmd3 = new SqlCommand())
                {
                    string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                    string driverTypeCode = SDMDList.SelectedValue;
                    string driverType = "";
                    string driverStation = lbstation.Text;
                    string rb_Activevalue = rb_Active.SelectedValue;
                    if (rb_Activevalue == "1")
                    {
                        cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                        cmd3.Parameters.AddWithValue("@driverTypeCode", SDMDList.SelectedValue);
                        cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                        cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                        DataTable dt2 = dbAdapter.getDataTable(cmd3);

                        if (dt2.Rows.Count > 0)
                        {
                            string repeat_number = dt2.Rows[0]["driver_code"].ToString();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.location = window.location.href;alert( '不允許生效，SD、MD碼與同站所司機 " + repeat_number + " 重複' );</script>", false);
                            return;
                        }
                    }
                    else if (rb_Activevalue == "0")
                    {
                        cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                        cmd3.Parameters.AddWithValue("@driverTypeCode", SDMDList.SelectedValue);
                        cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                        cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                        DataTable dt2 = dbAdapter.getDataTable(cmd3);

                        if (dt2.Rows.Count > 0)
                        {
                            string repeat_number = dt2.Rows[0]["driver_code"].ToString();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.location = window.location.href;alert( '不允許生效，SD、MD碼與同站所司機 " + repeat_number + " 重複' );</script>", false);
                            return;
                        }

                    }
                    
                }

                //cmd.Parameters.AddWithValue("@customer_code", customer_code != "" ? customer_code : ddl_supplier.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@customer_code", ddl_customer_code.SelectedValue);
                cmd.Parameters.AddWithValue("@emp_code", emp_code.Text.ToString());
                cmd.Parameters.AddWithValue("@emp_name", emp_name.Text.ToString());
                cmd.Parameters.AddWithValue("@job_type", ddl_job_edit.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@driver_code", driver_code.Text.ToString());
                cmd.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());

                if (Less_than_truckload == "1")
                {
                    cmd.Parameters.AddWithValue("@station", ddl_staion.SelectedValue.ToString());
                }

                if (IsEdit)
                {
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "emp_id", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm("tbEmps", cmd);
                    dbAdapter.execNonQuery(cmd);

                    using (SqlCommand cmd3 = new SqlCommand())
                    {
                        string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                        string driverTypeCode = SDMDList.SelectedValue;
                        string driverType = "";
                        string driverStation = lbstation.Text;
                        int temp_;
                        if ((driverTypeValue == "0" || driverTypeValue == "1") & int.TryParse(driverTypeCode, out temp_) & ddl_job_edit.SelectedItem.Text.ToString() == jobTypeShowSDMDDropList)
                        {
                            if (driverTypeValue == "0")
                            {
                                driverType = "SD";
                            }
                            else if (driverTypeValue == "1")
                            {
                                driverType = "MD";
                            }
                            cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                            cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                            cmd3.Parameters.AddWithValue("@driverTypeCode", driverTypeCode);
                            cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                            DataTable dt = dbAdapter.getDataTable(cmd3);

                            if (dt.Rows.Count == 0)
                            {
                                isSDMDCorrectly = true;
                            }
                        }
                    }


                    using (SqlCommand cmd2 = new SqlCommand())
                    {
                        string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                        string driverTypeCode = SDMDList.SelectedValue;
                        string driverType = "";
                        if (SDMDList.Visible != true)
                        {
                            cmd2.Parameters.AddWithValue("@driver_type", DBNull.Value);
                            cmd2.Parameters.AddWithValue("@driver_type_code", DBNull.Value);
                            cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.ToString().Trim());
                            cmd2.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmd2);
                            dbAdapter.execNonQuery(cmd2);
                        }
                        else if (isSDMDCorrectly)
                        {

                            int temp_;
                            if ((driverTypeValue == "0" || driverTypeValue == "1") & int.TryParse(driverTypeCode, out temp_))
                            {
                                if (driverTypeValue == "0")
                                {
                                    driverType = "SD";
                                }
                                else if (driverTypeValue == "1")
                                {
                                    driverType = "MD";
                                }
                                cmd2.Parameters.AddWithValue("@driver_type", driverType);
                                cmd2.Parameters.AddWithValue("@driver_type_code", driverTypeCode);
                                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.ToString().Trim());
                                cmd2.Parameters.AddWithValue("@supplier_code", ddl_customer_code.SelectedValue.ToString());
                                cmd2.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmd2);
                                dbAdapter.execNonQuery(cmd2);
                            }
                        }
                    }

                    #region 同步司機狀態
                    if (driver_code.Text.Trim() != "")
                    {
                        if (Less_than_truckload == "1")
                        {
                            DataTable stdt = new DataTable();
                            using (SqlCommand stdtcmd = new SqlCommand())
                            {
                                stdtcmd.Parameters.AddWithValue("@id", ddl_staion.SelectedValue.ToString());
                                stdtcmd.CommandText = @"select station_scode from tbStation where id = @id";
                                stdt = dbAdapter.getDataTable(stdtcmd);
                            }

                            string stationScode = stdt.Rows.Count > 0 ? stdt.Rows[0][0].ToString() : "";

                            using (SqlCommand cmddriver = new SqlCommand())
                            {
                                cmddriver.Parameters.AddWithValue("@station", stationScode);
                                cmddriver.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                                cmddriver.Parameters.AddWithValue("@driver_name", emp_name.Text.ToString());
                                cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.Trim());
                                cmddriver.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmddriver);
                                dbAdapter.execNonQuery(cmddriver);
                            }                            
                        }
                        else
                        {
                            using (SqlCommand cmddriver = new SqlCommand())
                            {

                                cmddriver.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                                cmddriver.Parameters.AddWithValue("@driver_name", emp_name.Text.ToString());
                                cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.Trim());
                                cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "supplier_code", ddl_supplier.SelectedValue.ToString());
                                cmddriver.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmddriver);
                                dbAdapter.execNonQuery(cmddriver);
                            }
                        }
                    }
                    #endregion

                    int resultType = -1;        //為了跨過SetEditOrAdd();
                    if (RadioButtonListSDMD.Visible & RadioButtonListSDMD.SelectedItem != null & SDMDList.Visible & SDMDList.SelectedIndex == 0)
                    {
                        resultType = 1;
                    }
                    else if (!isSDMDCorrectly & RadioButtonListSDMD.Visible)
                    {
                        resultType = 2;
                    }
                    else
                    {
                        resultType = 0;
                    }

                    SetEditOrAdd(0);

                    switch (resultType)
                    {
                        case 1:
                            RetuenMsg("未選擇SD、MD編號，不儲存SD、MD種類，其餘修改成功!");
                            break;
                        case 2:
                            RetuenMsg("不允許SD、MD與同站所司機重複，其餘修改成功!");
                            break;
                        case 0:
                            RetuenMsg("修改成功!");
                            break;
                    }


                }
                else
                {
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", strUser);
                    cmd.CommandText = dbAdapter.genInsertComm("tbEmps", true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;

                    #region 同步司機狀態
                    if (driver_code.Text.Trim() != "")
                    {
                        if (Less_than_truckload == "1")
                        {
                            DataTable stdt = new DataTable();
                            using (SqlCommand stdtcmd = new SqlCommand())
                            {
                                stdtcmd.Parameters.AddWithValue("@id", ddl_staion.SelectedValue.ToString());
                                stdtcmd.CommandText = @"select station_scode from tbStation where id = @id";
                                stdt = dbAdapter.getDataTable(stdtcmd);
                            }
                            if (stdt.Rows.Count > 0)
                            {
                                using (SqlCommand cmddriver = new SqlCommand())
                                {
                                    cmddriver.Parameters.AddWithValue("@station", stdt.Rows[0][0].ToString());
                                    cmddriver.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                                    cmddriver.Parameters.AddWithValue("@driver_name", emp_name.Text.ToString());
                                    cmddriver.Parameters.AddWithValue("@supplier_code", stdt.Rows[0]["customer_code"].ToString());
                                    cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.Trim());
                                    cmddriver.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmddriver);
                                    dbAdapter.execNonQuery(cmddriver);
                                }
                            }

                        }
                        else
                        {
                            using (SqlCommand cmddriver = new SqlCommand())
                            {

                                cmddriver.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                                cmddriver.Parameters.AddWithValue("@driver_name", emp_name.Text.ToString());
                                cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.Trim());
                                cmddriver.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "supplier_code", ddl_supplier.SelectedValue.ToString());
                                cmddriver.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmddriver);
                                dbAdapter.execNonQuery(cmddriver);
                            }
                        }
                    }
                    #endregion

                    SetEditOrAdd(0);
                    RetuenMsg("新增成功!");
                }

                //#region 自動建登入帳密
                //if (Less_than_truckload == "1" && ddl_job_edit.SelectedValue == "1")
                //{
                //    #region 檢查帳號是否存在
                //    SqlCommand cmda = new SqlCommand();
                //    DataTable dta = new DataTable();
                //    cmda.Parameters.AddWithValue("@account_code", emp_code.Text);
                //    cmda.CommandText = "SELECT 1 FROM tbAccounts WHERE account_code=@account_code ";
                //    dta = dbAdapter.getDataTable(cmda);
                //    if (dta.Rows.Count == 0)
                //    {
                //        using (SqlCommand cmd2 = new SqlCommand())
                //        {
                //            cmd2.Parameters.AddWithValue("@account_code", emp_code.Text);                       //帳號
                //            MD5 md5 = MD5.Create();//建立一個MD5
                //            string password = Utility.GetMd5Hash(md5, emp_code.Text);
                //            cmd2.Parameters.AddWithValue("@password", password);                                             //密碼預設帳帳號 (MD5編碼後存入)
                //                                                                                                             //cmd2.Parameters.AddWithValue("@head_code", account_code.Substring(0, 3));                      //帳號前三碼
                //                                                                                                             //cmd2.Parameters.AddWithValue("@body_code", account_code.Substring(3, 4));                      //帳號後四碼流水號
                //            cmd2.Parameters.AddWithValue("@active_flag", "1");                                               //啟動狀態(0:停用 1:啟用)
                //                                                                                                             //cmd2.Parameters.AddWithValue("@master_code", account_code);                                    //主客代碼
                //            cmd2.Parameters.AddWithValue("@manager_type", "2");                                              //管理單位類別
                //            cmd2.Parameters.AddWithValue("@manager_unit", ddl_customer_code.SelectedItem.Text);              //管理單位
                //            cmd2.Parameters.AddWithValue("@job_title", "");                         //職稱
                //            cmd2.Parameters.AddWithValue("@emp_code", emp_code.Text); //員工代碼  
                //            cmd2.Parameters.AddWithValue("@user_name", emp_name.Text); //人員名稱
                //            cmd2.Parameters.AddWithValue("@account_type", "1");                            //
                //            cmd2.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                //            cmd2.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                //            cmd2.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                //            cmd2.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 
                //            cmd2.CommandText = dbAdapter.SQLdosomething("tbAccounts", cmd2, "insert");
                //            dbAdapter.execNonQuery(cmd2);
                //        }
                //    }
                //    #endregion

                //}
                //#endregion

            }

            #region 同步 tbAccount active_flag
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select count(1) from tbAccounts where account_code = @account_code";
                cmd.Parameters.AddWithValue("@account_code", emp_code.Text);

                var count = dbAdapter.getScalarBySQL(cmd);
                if (count != null && int.Parse(count.ToString()) > 0)
                {
                    using (SqlCommand cmdAccount = new SqlCommand())
                    {
                        cmdAccount.CommandText = "update tbAccounts set active_flag = @active_flag  where account_code = @account_code ";
                        cmdAccount.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                        cmdAccount.Parameters.AddWithValue("@account_code", emp_code.Text);

                        dbAdapter.execQuery(cmdAccount);
                    }
                }
            } 
            #endregion
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }

        return result;
    }
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }

    protected void RadioSDMD(object sender, EventArgs e)
    {
        RadioSDMDProgram();
    }

    protected void RadioSDMDProgram()
    {
        SDMDList.Visible = true;
        SqlCommand cmd = new SqlCommand();

        string driverCode = driver_code.Text.ToString();
        string driverType = RadioButtonListSDMD.SelectedValue;

        if (driverType == "0")                                  //0:SD,1:MD
        {
            SDMDList.Items.Clear();
            cmd.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where driver_code = @driver_code";
            cmd.Parameters.AddWithValue("@driver_code", driverCode);
            DataTable dt = dbAdapter.getDataTable(cmd);
            int temp_sd;
            if (int.TryParse(dt.Rows[0]["sd_range_start"].ToString().Trim(), out temp_sd) & int.TryParse(dt.Rows[0]["sd_range_end"].ToString().Trim(), out temp_sd))
            {
                int startSd = int.Parse(dt.Rows[0]["sd_range_start"].ToString().Trim());
                int endSd = int.Parse(dt.Rows[0]["sd_range_end"].ToString().Trim());
                for (var i = startSd; i < endSd + 1; i++)
                {
                    SDMDList.Items.Add(i.ToString());
                }
                SDMDList.Items.Insert(0, "請選擇");
            }
            else
            {
                SDMDList.Items.Insert(0, "請至站所維護選取SD區間再回本頁面選取SD");
            }
        }
        else if (driverType == "1")
        {
            SDMDList.Items.Clear();
            cmd.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where driver_code = @driver_code";
            cmd.Parameters.AddWithValue("@driver_code", driverCode);
            DataTable dt = dbAdapter.getDataTable(cmd);
            int temp_md;
            if (int.TryParse(dt.Rows[0]["md_range_start"].ToString().Trim(), out temp_md) & int.TryParse(dt.Rows[0]["md_range_end"].ToString().Trim(), out temp_md))
            {
                int startMd = int.Parse(dt.Rows[0]["md_range_start"].ToString().Trim());
                int endMd = int.Parse(dt.Rows[0]["md_range_end"].ToString().Trim());
                for (var i = startMd; i < endMd + 1; i++)
                {
                    SDMDList.Items.Add(i.ToString());
                }
                SDMDList.Items.Insert(0, "請選擇");
            }
            else
            {
                SDMDList.Items.Insert(0, "請至站所維護選取MD區間再回本頁面選取MD");
            }
        }
    }

    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "人員基本資料";
        string file_name = "人員基本資料" + DateTime.Now.ToString("yyyyMMdd");
        String strSQL = string.Empty;
        using (SqlCommand objCommand = new SqlCommand())
        {
            #region Set QueryString

            //string querystring = "";
            //if (Request.QueryString["job"] != null)
            //{
            //    ddl_job_type.SelectedValue = Request.QueryString["job"];
            //    querystring += "&job=" + ddl_job_type.SelectedValue;
            //}
            //if (Request.QueryString["active"] != null)
            //{
            //    ddl_active.SelectedValue = Request.QueryString["active"];
            //    querystring += "&active=" + ddl_active.SelectedValue;
            //}
            //if (Request.QueryString["keyword"] != null)
            //{
            //    tb_search.Text = Request.QueryString["keyword"];
            //    querystring += "&keyword=" + tb_search.Text;
            //}

            //if (Request.QueryString["supplier"] != null)
            //{
            //    ddl_supplier.SelectedValue = Request.QueryString["supplier"];
            //    querystring += "&supplier=" + ddl_supplier.SelectedValue;
            //}

            #endregion


            string strWhere = "";

            if (Less_than_truckload == "0")
            {
                strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY emp.emp_id) AS 'NO',emp.emp_code AS '員工編號',emp.emp_name AS '姓名'
                             ,item.code_name AS '職屬歸類'                             
                             ,emp.driver_code AS 'APP帳號'
                             ,CASE emp.active_flag WHEN '1' THEN '是' ELSE '否' END AS '是否生效' 
                         FROM tbEmps emp With(Nolock)
                    LEFT JOIN tbItemCodes item With(Nolock) ON emp.job_type = item.code_id AND item.code_sclass=N'H1' 
                        WHERE 1=1 ";
            }
            else
            {
                strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY emp.emp_id) AS 'NO',emp.emp_code AS '員工編號',emp.emp_name AS '姓名'
                             ,item.code_name AS '職屬歸類'                             
                             ,emp.driver_code AS 'APP帳號'
                             ,CASE emp.active_flag WHEN '1' THEN '是' ELSE '否' END AS '是否生效' 
							 ,sta.station_name  '站所'
                         FROM tbEmps emp With(Nolock)
                    LEFT JOIN tbItemCodes item With(Nolock) ON emp.job_type = item.code_id AND item.code_sclass=N'H1' 
					LEFT JOIN tbStation sta with(nolock) on emp.station = sta.id 
                        WHERE 1=1 ";
            }

            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND emp.emp_code LIKE N'%'+@search+'%' OR emp.emp_name LIKE N'%'+@search+'%' ";
                objCommand.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }

            if (ddl_job_type.SelectedValue != "")
            {
                strWhere += " AND emp.job_type IN (" + ddl_job_type.SelectedValue.ToString() + ")";
            }

            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND emp.active_flag IN (" + ddl_active.SelectedValue.ToString() + ")";
            }

            if (Less_than_truckload == "1")
            {
                if (ddl_station.SelectedValue != "")
                {
                    objCommand.Parameters.AddWithValue("@station_id", ddl_station.SelectedValue.ToString());
                    strWhere += " and station = @station_id ";
                }
            }
            else
            {
                if (ddl_supplier.SelectedValue != "")
                {
                    objCommand.Parameters.AddWithValue("@supplier_code", ddl_supplier.SelectedValue.ToString());
                    strWhere += " and customer_code LIKE N''+@supplier_code+'%'";
                }
            }

            if (customer_code != "")
            {
                objCommand.Parameters.AddWithValue("@customer_code", customer_code);
                strWhere += " and customer_code LIKE N''+@customer_code+'%'";
            }
            #endregion

            objCommand.CommandText = string.Format(strSQL + " {0} order by emp.emp_id", strWhere);


            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }

    protected void emp_name_TextChanged(object sender, EventArgs e)
    {

    }

    protected void ddl_customer_code_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ddl_staion_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
