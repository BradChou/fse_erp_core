﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class LT_member_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            MultiView1.ActiveViewIndex = 0;
            string manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = "";
            string wherestr = string.Empty;

            #region  站所
            using (SqlCommand cmd = new SqlCommand())
            {
                string station = Session["management"].ToString();
                string station_level = Session["station_level"].ToString();
                string station_area = Session["station_area"].ToString();
                string station_scode = Session["station_scode"].ToString();

                string station_areaList = "";
                string station_scodeList = "";
                string managementList = "";
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";



                        //一般員工/管理者
                        //有綁定站所只該該站所客戶
                        //無則可查所有站所下的所有客戶
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                            cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                            cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                            if (station_level.Equals("1"))
                            {
                                cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                                DataTable dt1 = dbAdapter.getDataTable(cmd);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        managementList += "'";
                                        managementList += dt1.Rows[i]["station_scode"].ToString();
                                        managementList += "'";
                                        managementList += ",";
                                    }
                                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("2"))
                            {
                                cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                                DataTable dt1 = dbAdapter.getDataTable(cmd);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        station_scodeList += "'";
                                        station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                        station_scodeList += "'";
                                        station_scodeList += ",";
                                    }
                                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("4"))
                            {
                                cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                                DataTable dt1 = dbAdapter.getDataTable(cmd);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt1.Rows.Count; i++)
                                    {
                                        station_areaList += "'";
                                        station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                        station_areaList += "'";
                                        station_areaList += ",";
                                    }
                                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                                }
                            }
                            else if (station_level.Equals("5") || station_level.Equals(""))
                            {
                                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dt1 = dbAdapter.getDataTable(cmd);
                                if (dt1 != null && dt1.Rows.Count > 0)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[0]["station_code"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                            }
                            else
                            {
                                cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                                cmda.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                if (dta != null && dta.Rows.Count > 0)
                                {
                                    supplier_code = dta.Rows[0]["station_code"].ToString();
                                }
                            }
                        }
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmda.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dta = dbAdapter.getDataTable(cmda);
                            if (dta != null && dta.Rows.Count > 0)
                            {
                                supplier_code = dta.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        break;
                }

                if (supplier_code != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and station_code=@supplier_code";
                }
                else
                {
                    if (station_level.Equals("1"))
                    {
                        wherestr = " and station_scode in (" + managementList + ")";
                    }
                    else if (station_level.Equals("2"))
                    {
                        wherestr = " and station_scode in (" + station_scodeList + ")";
                    }
                    else if (station_level.Equals("4"))
                    {
                        wherestr = " and station_scode in (" + station_areaList + ")";
                    }
                }
                cmd.CommandText = string.Format(@" SELECT  station_code, station_code + ' ' +  station_name as showname, station_scode, station_scode + ' ' +  station_name as showsname    from tbStation with(nolock)
                                                   WHERE ISNULL(station_scode,'') <> '' {0} and tbStation.station_code <> 'F53' and tbStation.station_code <> 'F71'
                                                   ORDER BY  station_scode", wherestr);

                DataTable dt = dbAdapter.getDataTable(cmd);
                Suppliers.DataSource = dt;
                Suppliers.DataValueField = "station_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
            }

            #endregion


            #region 計價模式
            wherestr = "";
            SqlCommand cmd2 = new SqlCommand();
            if (Less_than_truckload == "1") wherestr = " and code_id  = '02'";
            cmd2.CommandText = string.Format(" select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM' {0} order by code_id asc", wherestr);
            PM_code.DataSource = dbAdapter.getDataTable(cmd2);
            PM_code.DataValueField = "code_id";
            PM_code.DataTextField = "showname";
            PM_code.DataBind();
            PM_code.Items.Insert(0, new ListItem("請選擇", ""));

            SPM_code.DataSource = PM_code.DataSource;
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion

            //readdata();
        }
    }

    private void Clear()
    {
        PM_code.SelectedValue = "";
        SPM_code.Items.Clear();
        City.Items.Clear();
        CityArea.Items.Clear();
        master_code.Text = "";
        second_code.Text = "";
        customer_name.Text = "";
        shortname.Text = "";
        uni_number.Text = "";
        principal.Text = "";
        tel.Text = "";
        fax.Text = "";
        business_people.Text = "";
        address.Text = "";
        ticket_period.SelectedValue = "";
        ticket.Text = "";
        email.Text = "";
        contract_sdate.Text = "";
        contract_edate.Text = "";
        filename.Text = "";
        close_date.SelectedValue = "";
        tbclose.Text = "";
        billing_special_needs.Text = "";
        MultiView1.ActiveViewIndex = 0;
        product_type.Text = "";
        ddlMasterCustomerCode.Text = "";
        is_weekend_delivered.Checked = false;
        weekend_delivery_fee.Text = "";
        bank.SelectedValue = "";
        bank_branch.Text = "";
        bank_account.Text = "";
        is_new_customer.Checked = true;
        OldCustomerOverCBM.Checked = false;

        //sales_id.SelectedValue = "";
        //sale_station.SelectedValue = "";
    }

    private void initForm()
    {
        master_code.Text = Suppliers.SelectedValue + "-";
        second_code.Text = "00" + PM_code.SelectedValue;
        SPM_code.SelectedValue = PM_code.SelectedValue;
        supplier_name.Text = Suppliers.SelectedItem.Text;
        is_new_customer.Checked = true;
        OldCustomerOverCBM.Checked = false;

        #region 郵政縣市        
        using (SqlCommand cmd1 = new SqlCommand())
        {
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            City.DataSource = dbAdapter.getDataTable(cmd1);
            City.DataValueField = "city";
            City.DataTextField = "city";
            City.DataBind();
            City.Items.Insert(0, new ListItem("請選擇", ""));

            ddInvoiceCity.DataSource = dbAdapter.getDataTable(cmd1);
            ddInvoiceCity.DataValueField = "city";
            ddInvoiceCity.DataTextField = "city";
            ddInvoiceCity.DataBind();
            ddInvoiceCity.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion

        #region 停運原因
        using (SqlCommand cmd2 = new SqlCommand())
        {
            cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'TS' and active_flag = 1 order by seq asc ";
            stop_code.DataSource = dbAdapter.getDataTable(cmd2);
            stop_code.DataValueField = "code_id";
            stop_code.DataTextField = "code_name";
            stop_code.DataBind();
        }
        #endregion

        #region SDMD
        ddSD.Items.Add(new ListItem("請選擇SD", "0"));
        ddMD.Items.Add(new ListItem("請選擇MD", "0"));

        for (int i = 21; i <= 30; i++)
        {
            ddSD.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        for (int i = 1; i <= 19; i++)
        {
            ddMD.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        #endregion

        #region 商品類型
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type, description from check_number_prehead where id !=3";
            product_type.DataSource = dbAdapter.getDataTable(cmd);
            product_type.DataValueField = "product_type";
            product_type.DataTextField = "description";
            product_type.DataBind();
            product_type.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion
        using (SqlCommand cmd3 = new SqlCommand())
        {
            cmd3.CommandText = "select bank_code, bank_code+' '+bank_name as showname from tbBanks order by bank_code ";
            bank.DataSource = dbAdapter.getDataTable(cmd3);
            bank.DataValueField = "showname";
            bank.DataTextField = "showname";
            bank.DataBind();
            bank.Items.Insert(0, new ListItem("請選擇", ""));
        }

        #region 母客代
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select customer_code,customer_code+' '+customer_name as showname from tbCustomers where stop_shipping_code = 0 and customer_code like 'F%'";
            ddlMasterCustomerCode.DataSource = dbAdapter.getDataTable(cmd);
            ddlMasterCustomerCode.DataValueField = "customer_code";
            ddlMasterCustomerCode.DataTextField = "showname";
            ddlMasterCustomerCode.DataBind();
            ddlMasterCustomerCode.Items.Insert(0, new ListItem("請選擇", ""));
        }
        #endregion

        ////記績營業員
        //using (SqlCommand cmd4 = new SqlCommand())
        //{
        //    cmd4.CommandText = "select id, name as showname from tbSales where is_active='1' order by id ";
        //    sales_id.DataSource = dbAdapter.getDataTable(cmd4);
        //    sales_id.DataValueField = "id";
        //    sales_id.DataTextField = "showname";
        //    sales_id.DataBind();
        //    sales_id.Items.Insert(0, new ListItem("請選擇", ""));
        //}
        //計績站所
        //using (SqlCommand cmd5 = new SqlCommand())
        //{
        //    cmd5.CommandText = "select station_code, station_code+' '+ station_name as showname from tbStation where active_flag='1' order by id  ";
        //    sale_station.DataSource = dbAdapter.getDataTable(cmd5);
        //    sale_station.DataValueField = "station_code";
        //    sale_station.DataTextField = "showname";
        //    sale_station.DataBind();
        //    sale_station.Items.Insert(0, new ListItem("請選擇", ""));
        //}

    }


    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
        }
    }

    protected void InvoiceCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            ddInvoiceArea.Items.Clear();
            ddInvoiceArea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", ddInvoiceCity.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    ddInvoiceArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            ddInvoiceArea.Items.Clear();
            ddInvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
        }
    }

    protected void PM_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        initForm();
        //應該要判斷該計價模式是否已新增主客代編號
        //如果就不允許新增，只能修改
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#customer_body').removeClass('disabledinput');$('#customer_body').addClass('enabledinput');</script>", false);

    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        DateTime dt_temp = new DateTime();

        int i_tmp;
        int customer_type = 0;
        int customer_id = 0;
        #region 序號
        string serialnum = "";
        SqlCommand cmdt = new SqlCommand();
        DataTable dtt;
        cmdt.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue.ToString());
        cmdt.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
        cmdt.Parameters.AddWithValue("@type", Less_than_truckload);
        cmdt.CommandText = " select max(supplier_id) as serial from tbCustomers where type=@type and supplier_code=@supplier_code  ";   //and pricing_code=@pricing_code
        dtt = dbAdapter.getDataTable(cmdt);
        if (dtt.Rows.Count > 0)
        {
            if (dtt.Rows[0]["serial"] != DBNull.Value)
            {
                serialnum = (Convert.ToInt32(dtt.Rows[0]["serial"].ToString()) + 1).ToString();
                while (serialnum.Length < 4)
                {
                    serialnum = "0" + serialnum;
                }

            }
            else
            {
                serialnum = "0001";
            }
        }
        else
        {
            serialnum = "0001";
        }
        #endregion


        #region 新增        
        SqlCommand cmd = new SqlCommand();
        SqlCommand cmdfse01 = new SqlCommand();
        cmd.Parameters.AddWithValue("@supplier_id", serialnum);                                       //主客代序號
        cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue.ToString());            //區配商代碼
        cmd.Parameters.AddWithValue("@station_scode", Suppliers.SelectedValue.ToString().Substring(1, 2));
        cmdfse01.Parameters.AddWithValue("@CLT_DEPT", Suppliers.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@master_code", Suppliers.SelectedValue.ToString() + serialnum);   //主客代完整代碼
        master_code.Text = Suppliers.SelectedValue.ToString() + serialnum;
        cmd.Parameters.AddWithValue("@second_id", "00");                                               //次客代流水號
        cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());               //計價模式
        cmd.Parameters.AddWithValue("@second_code", "00" + PM_code.SelectedValue.ToString());           //次客代完整代碼
        string customer_code = Suppliers.SelectedValue.ToString() + serialnum + "00" + PM_code.SelectedValue.ToString();
        cmd.Parameters.AddWithValue("@customer_code", customer_code);                                 //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
        cmdfse01.Parameters.AddWithValue("@CUST_CODE", customer_code);
        cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
        cmdfse01.Parameters.AddWithValue("@CHI_NAME", customer_name.Text.ToString());
        cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());                //客戶簡稱
        cmdfse01.Parameters.AddWithValue("@BRF_NAME", shortname.Text.ToString());
        cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
        cmdfse01.Parameters.AddWithValue("@CREATE_USER", Session["account_code"]);
        cmdfse01.Parameters.AddWithValue("@EDIT_USER", Session["account_code"]);
        cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間
        cmdfse01.Parameters.AddWithValue("@CREATE_TIME", DateTime.Now);
        cmdfse01.Parameters.AddWithValue("@EDIT_TIME", DateTime.Now);
        cmd.Parameters.AddWithValue("@product_type", product_type.SelectedValue.ToString());          //商品類型
        if ((chkMasterCustomerCode.Checked == true && ddlMasterCustomerCode.SelectedValue != "") || (chkMasterCustomerCode.Checked == false && ddlMasterCustomerCode.SelectedValue == ""))  //兩者皆填or 皆不填
        {
            lbMasterCustomerCode.Visible = true;
            return;
        }
        else if (ddlMasterCustomerCode.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());  //母客代
        }
        else
        {
            cmd.Parameters.AddWithValue("@MasterCustomerCode", customer_code);
        }

        SqlCommand cmdM = new SqlCommand();
        DataTable dtM = new DataTable();
        cmdM.Parameters.AddWithValue("@MasterCustomerCode", ddlMasterCustomerCode.SelectedValue.ToString());
        cmdM.CommandText = "select top 1  customer_name from tbCustomers where customer_code=@MasterCustomerCode";
        dtM = dbAdapter.getDataTable(cmdM);
        if (dtM.Rows.Count > 0)
        {
            cmd.Parameters.AddWithValue("@MasterCustomerName", dtM.Rows[0]["customer_name"].ToString());  //母客代名稱
        }
        else
        {
            cmd.Parameters.AddWithValue("@MasterCustomerName", customer_name.Text.ToString());
        }

        cmd.Parameters.AddWithValue("@is_weekend_delivered", is_weekend_delivered.Checked);  //是否啟用假日配送
        cmd.Parameters.AddWithValue("@weekend_delivery_fee", weekend_delivery_fee.Text);     //假日配送加價
        cmd.Parameters.AddWithValue("@bank", bank.SelectedValue.ToString());

        if (bank.SelectedValue != "")
        {
            cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString().Substring(0, 3));
            cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString().Substring(bank.SelectedValue.IndexOf(' ') + 1));
        }
        else
        {
            cmdfse01.Parameters.AddWithValue("@BANK_ID", bank.SelectedValue.ToString());
            cmdfse01.Parameters.AddWithValue("@BANK_NAME", bank.SelectedValue.ToString());
        }
        cmd.Parameters.AddWithValue("@bank_branch", bank_branch.Text.ToString());
        cmdfse01.Parameters.AddWithValue("@BANK_BRANCH", bank_branch.Text.ToString());
        cmd.Parameters.AddWithValue("@bank_account", bank_account.Text.ToString());
        cmdfse01.Parameters.AddWithValue("@BANK_ACCT", bank_account.Text.ToString());


        if (product_type.SelectedValue.ToString() == "4" || product_type.SelectedValue.ToString() == "5")  //內部文件即商城出貨不自動取前三碼為計績站所
        {
        }
        else
        {
            cmd.Parameters.AddWithValue("@sale_station", Suppliers.SelectedValue.ToString().Substring(1, 2));
            cmd.Parameters.AddWithValue("@sale_station_cdate", DateTime.Now);
            cmd.Parameters.AddWithValue("@sale_station_udate", DateTime.Now);
        }

        //cmd.Parameters.AddWithValue("@sales_id", sales_id.SelectedValue.ToString());  //記績營業員
        //if (sales_id.SelectedValue.ToString() != "")   
        //{
        //    cmd.Parameters.AddWithValue("@sales_id_cdate", DateTime.Now);  //首次填入記績營業員時間
        //}
        //if (sale_station.SelectedValue.ToString() != "")
        //{
        //    cmd.Parameters.AddWithValue("@sale_station_cdate", DateTime.Now);  //首次填入記績站所時間
        //}
        //customer_type (1:區配 2:竹運 3:自營)
        //if (customer_hcode.Text != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別(保留不帶值)   
        //}

        //customer_type (1:區配 2:竹運 3:自營)
        //if (customer_hcode.Text != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別(保留不帶值)   
        //}

        if (serialnum == "0000")
        {
            if (serialnum == "0000")
                customer_type = 1;
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer_type", "3");
            customer_type = 3;
        }

        cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
        cmdfse01.Parameters.AddWithValue("@CUST_ID", uni_number.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
        cmdfse01.Parameters.AddWithValue("@CONTACT", principal.Text.ToString());
        cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
        cmdfse01.Parameters.AddWithValue("@TEL_NO1", tel.Text.ToString());
        cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
        cmdfse01.Parameters.AddWithValue("@FAX_NO", fax.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
        cmdfse01.Parameters.AddWithValue("@CHI_CITY", City.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
        cmdfse01.Parameters.AddWithValue("@CHI_AREA", CityArea.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
        cmdfse01.Parameters.AddWithValue("@CHI_ADDR", address.Text.ToString());
        cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
        cmdfse01.Parameters.AddWithValue("@EMAIL_LIST", email.Text.ToString());
        cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
        if (stop_code.SelectedValue.ToString() == "0")                                        //啟用or停用
        {
            cmdfse01.Parameters.AddWithValue("@ACT_FG", "Y");
        }
        else
        {
            cmdfse01.Parameters.AddWithValue("@ACT_FG", "N");
        }
        cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
        //合約生效日
        if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", contract_sdate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EFFECT_DATE", DBNull.Value);
        }

        //合約到期日
        if (DateTime.TryParse(contract_edate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", contract_edate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
            cmdfse01.Parameters.AddWithValue("@CONTRACT_EXPIRED_DATE", DBNull.Value);
        }

        //票期
        if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(ticket.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
            }

        }
        else
        {
            cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
        }
        cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
                                                                                             //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日

        //結帳日
        if (int.TryParse(close_date.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(tbclose.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
        }

        //cmd.Parameters.AddWithValue("@tariffs_table", feename.Text.ToString());            //運價表
        cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
        cmd.Parameters.AddWithValue("@customer_hcode", "");                                  //竹運客代
        cmd.Parameters.AddWithValue("@contact_1", "");                                       //竹運-聯絡人1  
        cmd.Parameters.AddWithValue("@email_1", "");                                         //竹運-聯絡人1 email
        cmd.Parameters.AddWithValue("@contact_2", "");                                       //竹運-聯絡人2
        cmd.Parameters.AddWithValue("@email_2", "");                                         //竹運-聯絡人2 email
        cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
        cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
        cmd.Parameters.AddWithValue("@type", Less_than_truckload);                          //棧板/零擔
        cmd.Parameters.AddWithValue("@is_new_customer", is_new_customer.Checked);   //是否為新客戶        
        cmd.Parameters.AddWithValue("@OldCustomerOverCBM", OldCustomerOverCBM.Checked);   //是否為舊制超材費  
        cmd.Parameters.AddWithValue("@invoice_city", ddInvoiceCity.SelectedValue.ToString());
        cmdfse01.Parameters.AddWithValue("@INV_CITY", ddInvoiceCity.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@invoice_area", ddInvoiceArea.SelectedValue.ToString());
        cmdfse01.Parameters.AddWithValue("@INV_AREA", ddInvoiceArea.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@invoice_road", txtInvoiceRoad.Text.ToString());
        cmdfse01.Parameters.AddWithValue("@INV_ADDR", txtInvoiceRoad.Text.ToString());




        if (ddMD.SelectedIndex != 0)
        { cmd.Parameters.AddWithValue("@assigned_md", ddMD.SelectedValue); }

        if (ddSD.SelectedIndex != 0)
        { cmd.Parameters.AddWithValue("@assigned_sd", ddSD.SelectedValue); }

        //cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
        //cmd.Parameters.AddWithValue("@update_date", DateTime.Now);
        //cmd.CommandText = dbAdapter.SQLdosomething("tbCustomers", cmd, "insert");
        //dbAdapter.execNonQuery(cmd);
        cmd.CommandText = dbAdapter.genInsertComm("tbCustomers", true, cmd);        //新增
        cmdfse01.CommandText = @"insert into DT_CUST(CUST_CODE, CUST_ID, CHI_NAME,BRF_NAME,CHI_CITY,CHI_AREA,CHI_ADDR,INV_CITY,INV_AREA,INV_ADDR,CONTACT,TEL_NO1,FAX_NO,EMAIL_LIST,BANK_ID,BANK_NAME,BANK_BRANCH,BANK_ACCT,CLT_DEPT,ACT_FG,CREATE_USER,CREATE_TIME,EDIT_USER,EDIT_TIME,CONTRACT_EFFECT_DATE,CONTRACT_EXPIRED_DATE) 
values(@CUST_CODE, @CUST_ID, @CHI_NAME,@BRF_NAME,@CHI_CITY,@CHI_AREA,@CHI_ADDR,@INV_CITY,@INV_AREA,@INV_ADDR,@CONTACT,@TEL_NO1,@FAX_NO,@EMAIL_LIST,@BANK_ID,@BANK_NAME,@BANK_BRANCH,@BANK_ACCT,@CLT_DEPT,@ACT_FG,@CREATE_USER,@CREATE_TIME,@EDIT_USER,@EDIT_TIME,@CONTRACT_EFFECT_DATE,@CONTRACT_EXPIRED_DATE)";
        dbAdapter.execNonQueryForFSE01(cmdfse01);
        int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out customer_id);
        #endregion

        using (SqlCommand cmdShippingFee = new SqlCommand())
        {
            cmdShippingFee.Parameters.AddWithValue("@customer_code", customer_code);                                 //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)
            cmdShippingFee.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
            string stationShowName = supplier_name.Text;
            string stationName = stationShowName.Substring(stationShowName.IndexOf(' ') + 1);
            cmdShippingFee.Parameters.AddWithValue("@station_name", stationName.Trim());

            cmdShippingFee.CommandText = @"insert into CustomerShippingFee(customer_code, customer_name, station_name, 
no1_bag, no2_bag, no3_bag, no4_bag, s60_cm, s90_cm, s110_cm, s120_cm, s150_cm, s180, holiday, piece_rate) 
values(@customer_code, @customer_name, @station_name, 40, 45, 50, 55, 60, 70, 90, 120, 150, 150, 20, 60)";

            dbAdapter.execNonQuery(cmdShippingFee);
        }


        //#region 使用者操作log
        //SqlCommand cmd5 = new SqlCommand();
        //cmd5.Parameters.AddWithValue("@p_id", pid);
        //cmd5.Parameters.AddWithValue("@sdate", DateTime.Now);
        //cmd5.Parameters.AddWithValue("@user_id", Session["user_id"]);
        //cmd5.Parameters.AddWithValue("@action", "儲存");
        //cmd5.Parameters.AddWithValue("@ip", Utility.GetIPAddress());
        //cmd5.CommandText = dbAdapter.SQLdosomething("vi_project_log", cmd5, "insert");
        //dbAdapter.execNonQuery(cmd5);

        //#endregion

        //MultiView1.ActiveViewIndex = 1;
        SetDefaultView2(customer_code, customer_type, customer_id, PM_code.SelectedValue.ToString());
        //string sScript = " $('#li_01').removeClass('current');" +
        //                 "    $('#li_02').addClass('current');";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        setFee.Visible = true;
        FinalCheck.Visible = true;
        btnsave.Visible = false;
        btncancel.Visible = false;
        Suppliers.Enabled = false;
        PM_code.Enabled = false;
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成,客代編號:" + customer_code + "');</script>", false);
        return;
    }

    /// <summary>
    /// 運價初始資料設定
    /// </summary>
    protected void SetDefaultView2(string customer_code, int customer_type, int customer_id, string pricing_code)
    {
        //運價生效日
        DateTime dt_temp = new DateTime();
        tbmaster_code.Text = customer_code.Substring(0, 7);
        tbsecond_code.Text = customer_code.Substring(7, 4);
        lbcustomer_type.Text = customer_type.ToString();
        lb_cusid.Text = customer_id.ToString();
        lb_pricing_code.Text = pricing_code;
        lbl_PriceTip.Text = "";
        fee_sdate.Text = "※目前採用公版運價";
        if (customer_code.Length > 0)
        {
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.CommandText = "usp_GetTodayPriceBusDefLog";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@customer_code", customer_code);
                cmd2.Parameters.AddWithValue("@return_type", "1");
                using (DataTable the_dt = dbAdapter.getDataTable(cmd2))
                {
                    if (the_dt.Rows.Count > 0)
                    {
                        if (DateTime.TryParse(the_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                        {
                            fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                        }

                        int i_temp;
                        if (!int.TryParse(the_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                        lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                    }
                }
            }
        }


        //自營才開放價格設定
        if (customer_type == 3)
        {
            pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(customer_code, customer_id.ToString())));
            pan_price_list.Visible = true;
        }
        else
        {
            pan_price_list.Visible = false;
        }
    }

    protected void btnQry_view2_Click(object sender, EventArgs e)
    {
        SetDefaultView2(tbmaster_code.Text + tbsecond_code.Text, Convert.ToInt32(lbcustomer_type.Text), Convert.ToInt32(lb_cusid.Text), lb_pricing_code.Text);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string sScript = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@individual_fee", individual_fee.Checked);                //是否個別計價  
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", lb_cusid.Text);
            cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "新增主客戶" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                sScript = "alert('儲存失敗：" + ex.Message.ToString() + "')";
            }
        }
        //MultiView1.ActiveViewIndex = 2;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@customer_code", tbmaster_code.Text + tbsecond_code.Text);
            cmd.CommandText = " select top 1 tbCustomers.* , B.code_name as pricingname, C.code_name as stopname  from tbCustomers " +
                              " left join  tbItemCodes B on B.code_id = tbCustomers.pricing_code and B.code_sclass = 'PM'" +
                              " left join  tbItemCodes C on C.code_id = tbCustomers.stop_shipping_code and C.code_sclass = 'TS'" +
                              " where customer_code = @customer_code ";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                if (dt.Rows[0]["supplier_code"].ToString().Trim() != "")
                {
                    SqlCommand cmds = new SqlCommand();
                    DataTable dts;
                    cmds.Parameters.AddWithValue("@supplier_code", dt.Rows[0]["supplier_code"].ToString().Trim());
                    cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
                    dts = dbAdapter.getDataTable(cmds);
                    if (dts.Rows.Count > 0)
                    {
                        lsupplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
                    }
                }
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();
                lbshortname.Text = dt.Rows[0]["customer_shortname"].ToString();
                lbuni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();
                lbprincipal.Text = dt.Rows[0]["shipments_principal"].ToString();
                lbtel.Text = dt.Rows[0]["telephone"].ToString();
                lbfax.Text = dt.Rows[0]["fax"].ToString();
                lbCity.Text = dt.Rows[0]["shipments_city"].ToString();
                lbCityArea.Text = dt.Rows[0]["shipments_area"].ToString();
                lbaddress.Text = dt.Rows[0]["shipments_road"].ToString();
                lbbusiness_people.Text = dt.Rows[0]["business_people"].ToString();
                lbticket_period.Text = dt.Rows[0]["ticket_period"].ToString();
                lbemial.Text = dt.Rows[0]["shipments_email"].ToString().Trim();
                lbSPM_code.Text = dt.Rows[0]["pricingname"].ToString().Trim();
                lbstop_code.Text = dt.Rows[0]["stopname"].ToString().Trim();
                lbcontract_sdate.Text = dt.Rows[0]["contract_effect_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd") : "";
                lbcontract_edate.Text = dt.Rows[0]["contract_expired_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd") : "";
                sScript += "$('#hlFileView2').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename.Text + "');";
                lbclose_date.Text = dt.Rows[0]["checkout_date"].ToString();
                lbbilling_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString();
                lbFee.Text = Convert.ToBoolean(dt.Rows[0]["individual_fee"]) == true ? "獨立計價" : "採運價表計價";

            }
        }


        // sScript += " $('#li_01').removeClass('current');$('#li_02').removeClass('current');" +
        //                 "    $('#li_03').addClass('current');";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_1.aspx';alert('運價新增完成');</script>", false);
        return;
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        Clear();
    }
}