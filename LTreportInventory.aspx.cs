﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTreportInventory : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();

            string managementList = "";
            string station_areaList = "";
            string station_scodeList = "";
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    managementList += "'";
                                    managementList += dt1.Rows[i]["station_scode"].ToString();
                                    managementList += "'";
                                    managementList += ",";
                                }
                                managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scodeList += "'";
                                    station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                    station_scodeList += "'";
                                    station_scodeList += ",";
                                }
                                station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_areaList += "'";
                                    station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                    station_areaList += "'";
                                    station_areaList += ",";
                                }
                                station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode += dt1.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode = dt.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_scode = @supplier_code";
            }
            else
            {
                if (station_level.Equals("1"))
                {
                    wherestr = " and station_scode in (" + managementList + ")";
                }
                else if (station_level.Equals("2"))
                {
                    wherestr = " and station_scode in (" + station_scodeList + ")";
                }
                else if (station_level.Equals("4"))
                {
                    wherestr = " and station_scode in (" + station_areaList + ")";
                }
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "station_scode";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            if (manager_type == "0" || manager_type == "1")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && manager_type == "2")
            {
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
            Suppliers_SelectedIndexChanged(null, null);
            #endregion

            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["Suppliers"] != null)
            {
                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }


            SqlCommand cmds = new SqlCommand();
            string sqlwhere = "";
            cmds.CommandTimeout = 600;

            cmds.CommandText = @" 
            SELECT count(A.Inventory_ID)
			FROM ttInventory A With(Nolock)
			inner join tcDeliveryRequests X With(Nolock)  on A.check_number = X.check_number
			left join tbDrivers B on B.driver_code = A.driver_code
			left join tbStation C on C.station_scode = A.station_code
            left join tbItemCodes O With(Nolock) on X.latest_scan_item = O.code_id and O.code_sclass='P3' and O.code_bclass='5'
            left outer join tbItemCodes I With(Nolock) on X.latest_scan_arrive_option=I.code_id and I.code_sclass='AO'
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.create_dt,111) > = CONVERT(VARCHAR,@sdt,111) and CONVERT(VARCHAR,A.create_dt,111) < CONVERT(VARCHAR,@edt,111) ";
            //cmds.CommandText = @"AND (A.station_code = @station_code or @station_code = '')";

            if (Suppliers.SelectedValue == "")
            {
                if (station_level.Equals("1"))
                {
                    cmds.CommandText += @" AND A.station_code in (" + managementList + ")";
                }
                else if (station_level.Equals("2"))
                {
                    cmds.CommandText += @" AND A.station_code in (" + station_scodeList + ")";
                }
                else if (station_level.Equals("4"))
                {
                    cmds.CommandText += @" AND A.station_code in (" + station_areaList + ")";
                }
                else
                {
                    cmds.CommandText += @" AND (A.station_code = @station_code or @station_code = '')";
                }
            }
            else {

                cmds.CommandText += @" AND (A.station_code = @station_code or @station_code = '')";

            }


            cmds.Parameters.AddWithValue("@sdt", date1.Text);
            cmds.Parameters.AddWithValue("@edt", Convert.ToDateTime(date1.Text).AddDays(1));
            cmds.Parameters.AddWithValue("@station_code", Suppliers.SelectedValue);

            DataTable dtsum = dbAdapter.getDataTable(cmds);
            if (dtsum.Rows.Count > 0)
            {
                lbSuppliers.Text = Suppliers.SelectedValue;
                totle.Text = dtsum.Rows[0][0].ToString() + "筆";
            }
            if (Request.QueryString["date1"] != null)
            {
                readdata(Suppliers.SelectedValue);
            }
        }
    }
    private void readdata(string suppliers)
    {
        string querystring = "";

        string management = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();

        string managementList = "";
        string station_scodeList = "";
        string station_areaList = "";

        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@sdt", date1.Text);
            cmd.Parameters.AddWithValue("@edt", Convert.ToDateTime(date1.Text).AddDays(1));

            querystring += "&date1=" + date1.Text;

            cmd.Parameters.AddWithValue("@management", management);
            cmd.Parameters.AddWithValue("@station_scode", station_scode);
            cmd.Parameters.AddWithValue("@station_area", station_area);


            if (station_level.Equals("1"))
            {
                cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                DataTable dt1 = dbAdapter.getDataTable(cmd);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        managementList += "'";
                        managementList += dt1.Rows[i]["station_scode"].ToString();
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }
            }
            else if (station_level.Equals("2"))
            {
                cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                DataTable dt1 = dbAdapter.getDataTable(cmd);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        station_scodeList += "'";
                        station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }
            }
            else if (station_level.Equals("4"))
            {
                cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                DataTable dt1 = dbAdapter.getDataTable(cmd);
                if (dt1 != null && dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        station_areaList += "'";
                        station_areaList += dt1.Rows[i]["station_scode"].ToString();
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }
            }
            else if (station_level.Equals("5") || station_level.Equals(""))
            {

            }
            else
            {
                cmd.Parameters.AddWithValue("@station_code", Suppliers.SelectedValue.ToString());
                querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
            }

            cmd.CommandText = string.Format(@"
            SELECT ROW_NUMBER() OVER(ORDER BY A.Inventory_ID) AS NO ,
            CASE DATEDIFF(day,CAST(X.ship_date as datetime),CAST(scanning_dt as datetime)) 
            WHEN 0 THEN 'D+1內'
            WHEN 1 THEN 'D+1內'
            WHEN 2 THEN 'D+2內'
            ELSE 'D+3以上'
            END 'depot_period', --(盤點日-出貨日)
            A.check_number,
			CONVERT(varchar,X.print_date,111) as print_date,--發送日
            CONVERT(varchar,X.ship_date,111) as ship_date, --出貨日
            CONVERT(varchar,DATEADD(day,1, X.ship_date),111) as delivery_date , --配送日
            CONVERT(varchar,scanning_dt,111) as scanning_dt,--盤點日
            M.code_name as subpoena_category,--傳票類別
            case when X.collection_money is null then 0 else X.collection_money end as collection_money, --代收貨款
            O.code_name + CASE WHEN　I.code_name is not null THEN '-'+I.code_name  ELSE '' END as arrive_state, --貨態
            A.driver_code+ ' ' + B.driver_name as driver_code,
		    A.station_code + C.station_name as station_code 
			FROM ttInventory A With(Nolock) 
			inner join tcDeliveryRequests X With(Nolock)  on A.check_number = X.check_number
			left join tbDrivers B on B.driver_code = A.driver_code
			left join tbStation C on C.station_scode = A.station_code
            left join tbItemCodes O With(Nolock) on X.latest_scan_item = O.code_id and O.code_sclass='P3' and O.code_bclass='5'
            left join tbItemCodes M With(Nolock) on X.subpoena_category = M.code_id and M.code_bclass='2' and M.code_sclass='S2'
            left outer join tbItemCodes I With(Nolock) on X.latest_scan_arrive_option=I.code_id and I.code_sclass='AO'
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.create_dt,111) >= CONVERT(VARCHAR,@sdt,111) and CONVERT(VARCHAR,A.create_dt,111) < CONVERT(VARCHAR,@edt,111)");

            if (!suppliers.Equals(""))
            {
                string station = "'";
                station += suppliers;
                station += "'";
                cmd.CommandText += "AND (A.station_code in (" + station + "))";
            }
            else if (station_level.Equals("1") && suppliers.Equals(""))
            {
                cmd.CommandText += "AND (A.station_code in (" + managementList + "))";
            }
            else if (station_level.Equals("2") && suppliers.Equals(""))
            {
                cmd.CommandText += "AND (A.station_code in (" + station_scodeList + "))";
            }
            else if (station_level.Equals("4") && suppliers.Equals(""))
            {
                cmd.CommandText += "AND (A.station_code in (" + station_areaList + "))";
            }

            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                ltotalpages.Text = dt.Rows.Count.ToString();
                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }


    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }

        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

        Response.Redirect(ResolveUrl("~/LTreportInventory.aspx?search=yes" + querystring));
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }
    protected void New_List_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {

        string sheet_title = "站所盤點明細表";
        string strWhereCmd1 = "";
        string strWhereCmd2 = "";
        string file_name = "站所盤點明細表" + DateTime.Now.ToString("yyyyMMdd");
        if (date1.Text == "")
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入匯出日期!!');</script>", false);
            return;
        }


        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@sdt", date1.Text);
            cmd.Parameters.AddWithValue("@edt", Convert.ToDateTime(date1.Text).AddDays(1).ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@station_code", Suppliers.SelectedValue);
            cmd.CommandText = string.Format(@"
            SELECT ROW_NUMBER() OVER(ORDER BY A.Inventory_ID) AS '序號' ,
            CASE DATEDIFF(day,CAST(X.ship_date as datetime),CAST(scanning_dt as datetime)) 
            WHEN 0 THEN 'D+1內'
            WHEN 1 THEN 'D+1內'
            WHEN 2 THEN 'D+2內'
            ELSE 'D+3以上'
            END '留庫天數', --(盤點日-出貨日)
            A.check_number '貨號',
			CONVERT(varchar,X.print_date,111) '發送日',--發送日
            CONVERT(varchar,X.ship_date,111) '出貨日', --出貨日
            CONVERT(varchar,DATEADD(day,1, X.ship_date),111) '配送日期' ,
            CONVERT(varchar,scanning_dt,111) '盤點日',--盤點掃讀日
            M.code_name '傳票類別',--傳票類別
            case when X.collection_money is null then 0 else X.collection_money end '代收貨款', --代收貨款
            O.code_name + CASE WHEN　I.code_name is not null THEN '-'+I.code_name  ELSE '' END '貨態', --貨態
            A.driver_code+ ' ' + B.driver_name as '作業人帳號+姓名',
		    A.station_code + C.station_name as '作業站所'
			FROM ttInventory A With(Nolock) 
			inner join tcDeliveryRequests X With(Nolock)  on A.check_number = X.check_number
			left join tbDrivers B on B.driver_code = A.driver_code
			left join tbStation C on C.station_scode = A.station_code
            left join tbItemCodes O With(Nolock) on X.latest_scan_item = O.code_id and O.code_sclass='P3' and O.code_bclass='5'
            left join tbItemCodes M With(Nolock) on X.subpoena_category = M.code_id and M.code_bclass='2' and M.code_sclass='S2'
            left outer join tbItemCodes I With(Nolock) on X.latest_scan_arrive_option=I.code_id and I.code_sclass='AO'
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.create_dt,111) >= CONVERT(VARCHAR,@sdt,111) and CONVERT(VARCHAR,A.create_dt,111) < CONVERT(VARCHAR,@edt,111)
			  AND (A.station_code = @station_code or @station_code = '')
");
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                    }
                }
            }
        }

    }


    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}