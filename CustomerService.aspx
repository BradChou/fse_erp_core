﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="CustomerService.aspx.cs" Inherits="CustomerService" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= rdchangepwd.ClientID%> input").change(function () {
                 var n = $(this).val();
                 switch (n)
                 {
                     case "1":
                         $(".td_password").removeClass("hide");
                         break; 
                     default:
                         $(".td_password").addClass("hide");
                         break; 
                     
                 }
            });
        });
        
    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }

      
        .radio label {
            margin-right: 15px;
            text-align :left ;
        }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="alert alert-danger div_err hide">
        <strong>注意!</strong>
        <asp:Label ID="lberrormsg" runat="server"></asp:Label>
    </div>
    <div class="templatemo-content-widget white-bg">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="帳戶資料" runat="server"></asp:Label>
        </h2>
        <hr />
        <div  >
            <div class="row">
                <div class="col-lg-6 form-group form-inline">
                    <label class="">管理單位：</label>
                    <asp:Label ID="lbitem" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">人　　員：</label>
                    <asp:Label ID="lbitem_employee" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">帳　　號：</label>
                    <asp:Label ID="lbaccount_code" runat="server"></asp:Label>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="">名　　稱：</label>
                    <asp:Label ID="lbaccount_name" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="_tip_important">信　　箱：</label>                    
                    <asp:TextBox ID="email" runat="server" CssClass="form-control" MaxLength="50" Width="250px" placeholder="請輸入信箱"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="e-mail格式不正確!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="email" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                    </asp:RegularExpressionValidator>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="_tip_important">職　　稱：</label>                    
                    <asp:TextBox ID="job_title" runat="server" CssClass="form-control" MaxLength="20" Width="250px" placeholder="請輸入職稱"></asp:TextBox>
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="_tip_important">修改密碼：</label>
                    <asp:RadioButtonList ID="rdchangepwd" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio radio-success">
                        <asp:ListItem Selected="True" Value="0">否</asp:ListItem>
                        <asp:ListItem Value="1">是</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline td_password hide">
                    <label class="_tip_important">原 密 碼：</label>
                    <asp:TextBox ID="account_password" runat="server" CssClass="form-control" MaxLength="30" placeholder="請輸入原密碼" TextMode="Password"></asp:TextBox>

                </div>
            </div>

            <div class="row td_password hide">
                <div class="col-lg-6 col-md-6 form-group form-inline">
                </div>
                <div class="col-lg-6 col-md-6 form-group form-inline">
                    <label class="_tip_important">新 密 碼：</label>
                    <asp:TextBox ID="account_password_new" runat="server" CssClass="form-control" MaxLength="30" placeholder="請輸入新密碼" TextMode="Password"></asp:TextBox>
                    <span class="text-danger">※ 密碼至少需為8碼有英文大小寫+數字 </span>
                </div>
            </div>

            <div class="form-group text-center">
                <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲 存" OnClientClick="return confirm('確定要修改嗎?');" OnClick="btnsave_Click"  />
                <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取 消" />
            </div>
        </div>

        
        

    </div>
</asp:Content>

