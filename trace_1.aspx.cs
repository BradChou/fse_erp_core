﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Drawing.Imaging;
using System.IO;
using System.Collections;

public partial class trace_1 : System.Web.UI.Page
{
  

    //2017/12/22
    //修改規則
    //如果為來源為同一筆訂單編號，拆成多筆貨號
    //則把多筆貨件group起來，當作單一貨件。
    //掃讀或查詢任何一筆貨號時，要把群組內的貨況掃查資料、簽單影像，全部顯示出來。
    //群組的依據為原訂單編號

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            manager_type = Session["manager_type"].ToString(); //管理單位類別            

            //date.Text = DateTime.Today.Year.ToString() + '-' + (DateTime.Today.Month.ToString().Length < 2 ? '0' + DateTime.Today.Month.ToString() : DateTime.Today.Month.ToString());


            #region 處置說明-狀況類別
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.CommandText = "select * from tbItemCodes where code_bclass = '5' and code_sclass = 'P1' and active_flag = 1";
                dlP1.DataSource = dbAdapter.getDataTable(cmd1);
                dlP1.DataValueField = "code_id";
                dlP1.DataTextField = "code_name";
                dlP1.DataBind();
                dlP1.Items.Insert(0, new ListItem("===請選擇===", ""));
                dlP1_SelectedIndexChanged(null, null);
            }
            #endregion

          

            //if (Request.QueryString["date"] != null)
            //{
            //    date.Text = Request.QueryString["date"];
            //}

            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
                //lbcheck_number.Text = Request.QueryString["check_number"];
            }

            
            readdata();
            readdetail(); //處置說明資料叫出來

        }
        string sScript = " $('#" + dlP2.ClientID + "').change(function() {" +
                               " var n = $('option:selected',this).text(); " +
                               " $('#" + dispose_desc.ClientID + "').val(n); " +
                               " });";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);




    }

    private void readdata()
    {
        if (check_number.Text != "")
        {
            //判斷是否為康軒棧板
            bool isKNSH = false;
            using (SqlCommand cmdCheckIsKNSH = new SqlCommand())
            {
                cmdCheckIsKNSH.Parameters.Clear();
                cmdCheckIsKNSH.Parameters.AddWithValue("@check_number", check_number.Text);
                cmdCheckIsKNSH.CommandText = string.Format(@"select * from tcDeliveryRequests r left join tbCustomers c on r.customer_code = c.customer_code where c.customer_name like '康軒%' and check_number = @check_number");
                using (DataTable dt = dbAdapter.getDataTable(cmdCheckIsKNSH))
                {
                    if (dt.Rows.Count > 0)
                    {
                        isKNSH = true;
                    }
                }
            }


            using (SqlCommand cmd = new SqlCommand())
            {


                cmd.Parameters.Clear();
                #region 關鍵字

                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/"+ days);
                //}

                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmd.Parameters.AddWithValue("@manager_type", Session["manager_type"].ToString());
                //lbcheck_number.Text = check_number.Text;
                #endregion

                //CONVERT(VARCHAR,print_date,111)  >= CONVERT(VARCHAR,@startdate,111) AND CONVERT(VARCHAR,print_date,111)  <= CONVERT(VARCHAR,@enddate,111)

                if (Less_than_truckload == "1")
                {
                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)
                                                  DECLARE @ORDER_NUMBER NVARCHAR(20)

                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SELECT @ORDER_NUMBER = ISNULL(order_number,'') FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 

                                                  SET @statement = 
                                                  'Select  CASE WHEN A.supplier_code IN(''001'',''002'') THEN REPLACE(REPLACE(D.customer_shortname,''所'',''''),''HCT'','''') ELSE A.supplier_name END as sendstation,
                                                  ISNULL(E.supplier_name,'''') as showname,
                                                  A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                                                  A.time_period, A.order_number, A.check_number,
                                                  CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
                                                  A.product_category, C.code_name as product_category_name, 
                                                  A.subpoena_category, B.code_name as subpoena_category_name,
                                                  A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                                                  isnull(_fee.supplier_fee ,0) ''貨件運費'' ,
                                                  A.arrive_to_pay_append ''到付追加未稅'', A.arrive_to_pay_append  *1.05 ''到付追加含稅'',
                                                  A.collection_money ''代收金額'',
                                                  isnull(_fee.supplier_first_fee,0)  ''首件費用'',	
                                                  isnull(_fee.add_price ,0) ''續件費用''
                                                  from tcDeliveryRequests A with(nolock)
                                                  LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
                                                  LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
                                                  LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                                                  LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
                                                  --LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '''') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                                                  CROSS APPLY dbo.fu_GetLTShipFeeByRequestId(A.request_id) _fee  '
                                                  SET @where = ' where 1=1 and  A.print_date >=  DATEADD(MONTH,-6,getdate())   and  Less_than_truckload=1    ' 

                                                  IF @ORDER_NUMBER = '' BEGIN 
                                                   SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
                                                  END ELSE BEGIN 
                                                   SET @where=@where+' and (A.check_number = '''+ @check_number + ''' OR A.order_number =  '''+ @ORDER_NUMBER + ''')'
                                                  END

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@isKNSH", isKNSH);             //是否為康軒

                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)
                                                  DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                  DECLARE @station NVARCHAR(20)
                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SELECT @ORDER_NUMBER = ISNULL(order_number,'') FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 

                                                  SELECT  @station =  (select distinct A.supplier_name + '%')     
												  from tbsuppliers A with(nolock)
												  left join tbcustomers C  with(nolock) on C.supplier_code  = A.supplier_code
												  left join tbAccounts  F With(Nolock) on F.master_code = C.master_code
												  where   F.account_code =  @account_code

                                                  SET @statement = 
                                                  'Select  CASE WHEN A.supplier_code IN(''001'',''002'') THEN REPLACE(REPLACE(D.customer_shortname,''所'',''''),''HCT'','''') ELSE F.supplier_name END as sendstation,
                                                  ISNULL(E.supplier_name,'''') as showname,
                                                  A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                                                  A.time_period, A.order_number, A.check_number,
                                                  CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
                                                  A.product_category, C.code_name as product_category_name, 
                                                  A.subpoena_category, B.code_name as subpoena_category_name,
                                                  A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                                                  isnull(_fee.total_fee,0) ''貨件運費'' ,
                                                  isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) ''元付運費'',
                                                  A.arrive_to_pay_append ''到付追加未稅'', A.arrive_to_pay_append  *1.05 ''到付追加含稅'',
                                                  A.collection_money ''代收金額'',
                                                  isnull(_fee.cscetion_fee,0)  ''配送費用'',
                                                  isnull(_fee.supplier_unit_fee,0)  ''每板費用'',	
                                                  A.remote_fee ''偏遠區加價'',
                                                  isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) ''總費用''
                                                  from tcDeliveryRequests A with(nolock)
                                                  LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
                                                  LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
                                                  LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                                                  LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
                                                  LEFT JOIN tbSuppliers F With(Nolock) on A.send_station_scode = F.supplier_code and ISNULL(F.supplier_code,'''') <> ''''
                                                  left join tbCustomers I with(nolock) on A.customer_code =I.customer_code
												  left join tbAccounts H with(nolock) on h.master_code = I.master_code
                                                  left join tbAccounts  G With(Nolock) on G.account_code = A.cuser
                                                  --LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '''') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                                                  CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee  '
                                                  SET @where = ' where 1=1 and  A.print_date >=  DATEADD(MONTH,-6,getdate())  and  Less_than_truckload =0 ' 


                                                  if @manager_type in ('0' ,'1' ,'2') 
												
												  begin
												   set  @where=@where
												  end else
												  begin
												   set  @where=@where +  'and (H.account_code ='''+ @account_code + ''' or G.account_code ='''+ @account_code + '''or (Select  CASE WHEN A.supplier_code IN(''001'',''002'') THEN REPLACE(REPLACE(D.customer_shortname,''所'',''''),''HCT'','''') ELSE F.supplier_name END) like '''+ @station + ''' or ISNULL(E.supplier_name,'''') like'''+ @station + '''  )'
												  end


                                                  if @isKNSH = 1
												  begin
                                                  IF @ORDER_NUMBER = '' BEGIN 
                                                   SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
                                                  END ELSE BEGIN 
                                                   SET @where=@where+' and (A.check_number = '''+ @ORDER_NUMBER + ''' OR A.order_number =  '''+ @ORDER_NUMBER + ''')'
                                                  END
												  end
												  else
												  begin
												  SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
												  end

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                }


                //   cmd.CommandText = string.Format(@"

                //             DECLARE @ORDER_NUMBER NVARCHAR(20)
                //              SELECT @ORDER_NUMBER = order_number FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 

                //            --DECLARE  @CHKNUM NVARCHAR(100)  --貨號
                //            --DECLARE  @GOODS INT   --貨件運費
                //            --DECLARE  @YUAN  INT   --元付運費
                //            --DECLARE  @PEI   INT   --配送費用
                //            --DECLARE  @aUNIT INT   --每板費用
                //            --DECLARE  @TOTAL INT   --總費用
                //            --DECLARE  @ttERR   NVARCHAR(100) 
                //            --select check_number , 0 '貨件運費',0'元付運費', 0'配送費用',0'每板費用', 0'總費用' 
                //            --into 
                //            --#mylist	  
                //            --from tcDeliveryRequests with(nolock)
                //            --
                //            --where  print_date  >=DATEADD(MONTH,-6,GETDATE()) 
                //            --    and check_number = @check_number
                //            --    and cancel_date  IS NULL
                //            --
                //            --
                //            --DECLARE R_cursor CURSOR FOR
                //            -- select * FROM #mylist
                //            --OPEN R_cursor
                //            --FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
                //            --WHILE (@@FETCH_STATUS = 0) 
                //      --    BEGIN
                //   --      CREATE TABLE #tmp (supplier_fee INT , supplier_unit_fee  INT , cscetion_fee INT,cscetion_unit_fee  INT,status_code  INT,status_msg NVARCHAR(500));
                //   --      INSERT INTO #tmp EXEC  usp_GetShipFeeByCheckNumber @CHKNUM; 
                //   --      SELECT @GOODS=supplier_fee, @PEI= cscetion_fee, @aUNIT= supplier_unit_fee ,@YUAN=supplier_fee , @TOTAL=supplier_fee, @ttERR= status_msg FROM #tmp
                //   --       IF (@ttERR ='成功') BEGIN
                //--           UPDATE #mylist
                //--             SET 貨件運費 = ISNULL(@GOODS,0), 元付運費= ISNULL(@YUAN,0), 配送費用=  ISNULL(@PEI,0), 每板費用 = ISNULL(@aUNIT,0) ,總費用= ISNULL(@TOTAL,0)
                //--           WHERE check_number = @CHKNUM
                //      --    END
                //            --    IF OBJECT_ID('tempdb..#tmp') IS NOT NULL
                //   --        DROP TABLE #tmp
                //            --
                //            --  FETCH NEXT FROM R_cursor INTO @CHKNUM,@GOODS, @YUAN, @PEI,@aUNIT,@TOTAL
                //            --END
                //            --CLOSE R_cursor
                //            --DEALLOCATE R_cursor

                //            Select  CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(D.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation,
                //            E.showname,
                //            A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                //            A.time_period, A.order_number, A.check_number,
                //            CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
                //            A.product_category, C.code_name as product_category_name, 
                //            A.subpoena_category, B.code_name as subpoena_category_name,
                //            A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                //            isnull(_fee.total_fee,0) '貨件運費' ,
                //            isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) '元付運費',
                //            A.arrive_to_pay_append '到付追加未稅', A.arrive_to_pay_append  *1.05 '到付追加含稅',
                //            A.collection_money '代收金額',
                //            isnull(_fee.cscetion_fee,0)  '配送費用',
                //            isnull(_fee.supplier_unit_fee,0)  '每板費用',	
                //            A.remote_fee '偏遠區加價',
                //            isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) '總費用'
                //            from tcDeliveryRequests A with(nolock)
                //            LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
                //            LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = '2' and C.code_sclass = 'S3'
                //            LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                //            LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                //            --LEFT JOIN #mylist Z ON Z.check_number = A.check_number
                //            CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
                //            where   A.check_number = @check_number OR A.order_number =  @ORDER_NUMBER
                //            order by A.check_number

                //            --IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
                //            --DROP TABLE #mylist");
                //cmd.CommandText = string.Format(@"
                //                                  Select  CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(D.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation,
                //                                  ISNULL(E.supplier_name,'') as showname,
                //                                  A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
                //                                  A.time_period, A.order_number, A.check_number,
                //                                  CASE A.pricing_type WHEN  '01' THEN A.plates WHEN '02' THEN A.pieces WHEN '03' THEN A.cbm WHEN '04' THEN A.plates END AS quant, 
                //                                  A.product_category, C.code_name as product_category_name, 
                //                                  A.subpoena_category, B.code_name as subpoena_category_name,
                //                                  A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
                //                                  isnull(_fee.total_fee,0) '貨件運費' ,
                //                                  isnull(_fee.supplier_fee + A.remote_fee - A.arrive_to_pay_append,0) '元付運費',
                //                                  A.arrive_to_pay_append '到付追加未稅', A.arrive_to_pay_append  *1.05 '到付追加含稅',
                //                                  A.collection_money '代收金額',
                //                                  isnull(_fee.cscetion_fee,0)  '配送費用',
                //                                  isnull(_fee.supplier_unit_fee,0)  '每板費用',	
                //                                  A.remote_fee '偏遠區加價',
                //                                  isnull(_fee.supplier_fee + A.remote_fee + A.arrive_to_pay_append,0) '總費用'
                //                                  from tcDeliveryRequests A with(nolock)
                //                                  LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = '2' and B.code_sclass = 'S2'
                //                                  LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = '2' and C.code_sclass = 'S3'
                //                                  LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
                //                                  LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code 
                //                                  --LEFT JOIN (select ttArriveSites.* , tbSuppliers.supplier_name as showname from ttArriveSites left join tbSuppliers  on ttArriveSites.supplier_code  = tbSuppliers.supplier_code where ttArriveSites.supplier_code != '') E  on E.post_city = A.receive_city and E.post_area= A.receive_area
                //                                  CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
                //                                  where 1=1 and  A.print_date >=  DATEADD(MONTH,-6,getdate()) 
                //                                  and A.check_number = @check_number
                //                                  order by A.check_number
                //");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        string customer_code = dt.Rows[0]["customer_code"].ToString();
                        string send_contact = dt.Rows[0]["send_contact"].ToString();
                        send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
                        string send_tel = dt.Rows[0]["send_tel"].ToString();
                        send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                        string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                        receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                        string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                        receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;

                        lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel;
                        lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;
                    }
                    New_List_01.DataSource = dt;
                    New_List_01.DataBind();
                }

            }



            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/"+ days);
                //}

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    //lbcheck_number.Text = check_number.Text;
                }
                cmd.Parameters.AddWithValue("@Less_than_truckload", "0");
                cmd.Parameters.AddWithValue("@isKNSH",isKNSH);
                cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                cmd.Parameters.AddWithValue("@manager_type", Session["manager_type"].ToString());

                #endregion

                cmd.CommandText = string.Format(@"
                                                DECLARE @statement nvarchar(max)
                                                DECLARE @where nvarchar(1000)
                                                DECLARE @orderby nvarchar(40)
	                                            declare @station NVARCHAR(10)


                                                SET @where = ''
                                                SET @orderby = ''
                                                DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                SELECT @ORDER_NUMBER=order_number  FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                 
                                                --帳號自身站所
                                                  SELECT  @station =  (select distinct A.supplier_name + '%')     
												  from tbsuppliers A with(nolock)
												  left join tbcustomers C  with(nolock) on C.supplier_code  = A.supplier_code
												  left join tbAccounts  F With(Nolock) on F.master_code = C.master_code
												  where   F.account_code =  @account_code

                                                SET @statement ='
                                                Select ROW_NUMBER() OVER(ORDER BY A.log_id desc) AS NO ,
                                                A.scan_item,  CASE A.scan_item WHEN ''1'' THEN ''到著'' WHEN ''2'' THEN ''配送'' WHEN ''3'' THEN ''配達'' 
                                                WHEN ''5'' THEN ''集貨'' WHEN ''6'' THEN ''卸集'' WHEN ''7'' THEN ''發送'' END  AS scan_name,
                                                Convert(varchar, A.scan_date,120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , 
                                                CASE A.scan_item WHEN ''3'' THEN AO.code_name  ELSE  CASE WHEN EO.code_name=''無'' THEN '''' ELSE EO.code_name END  END as status,
                                                A.driver_code , C.driver_name , A.sign_form_image, WH.code_name as warehouse, A.tracking_number
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'''') <> ''''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = ''AO'' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = ''EO'' and EO.active_flag = 1 
                                                left join tbItemCodes WH with(nolock) on A.area_arrive_code = WH.code_id and WH.code_sclass = ''warehouse'' and WH.active_flag = 1 
                                                Left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number and  Convert(datetime,print_date) between dateadd(mm,-3,getdate()) and getdate() 
                                                left join tbSuppliers E  with(nolock) on R.send_station_scode = E.supplier_code
                                                left join tbCustomers F with(nolock) on R.customer_code =F.customer_code
												left join tbAccounts H with(nolock) on h.master_code = F.master_code
                                                left join tbAccounts G With(Nolock) on G.account_code = R.cuser'
                                                 SET @where = ' where 1=1 and  R.print_date >=  DATEADD(MONTH,-6,getdate())  and A.scan_item<>''4'' and Less_than_truckload =0 '   

												  if @manager_type in ('0' ,'1' ,'2') 
												
												  begin
												   set  @where=@where
												  end else
												  begin
												   set  @where=@where + 'and ( H.account_code ='''+ @account_code + ''' or G.account_code ='''+ @account_code + '''or E.supplier_name like '''+ @station + ''' or b.supplier_name like'''+ @station + '''  )'
												  end
												  
												
												  if @isKNSH = 1
												  begin
                                                  IF @ORDER_NUMBER = '' 
												  BEGIN 
                                                   SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
                                                  END ELSE 
												  BEGIN 
                                                   SET @where=@where+' and (A.check_number = '''+ @ORDER_NUMBER + ''' OR A.order_number =  '''+ @ORDER_NUMBER + ''')'
                                                  END
												  end
												  else
												  begin
												  SET @where=@where+' and A.check_number = '''+ @check_number+ ''''
												  end

												 

												  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby
                                                  --print @statement
                                                  EXEC sp_executesql @statement 

                                                ");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List_02.DataSource = dt;
                    New_List_02.DataBind();
                }

            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Clear();
                #region 關鍵字
                //if (date.Text != "")
                //{
                //    String yy = date.Text.Split('-')[0];
                //    String mm = date.Text.Split('-')[1];
                //    String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                //    cmd.Parameters.AddWithValue("@startdate", yy + "/" + mm + "/01");
                //    cmd.Parameters.AddWithValue("@enddate", yy + "/" + mm + "/" + days);
                //}

                if (check_number.Text != "")
                {
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    //lbcheck_number.Text = check_number.Text;
                }
                #endregion

                //cmd.CommandText = string.Format(@"select top 1 A.sign_form_image, A.sign_field_image, dr.customer_code ,dr.supplier_code  
                //                                    from ttDeliveryScanLog A with(nolock)
                //                                    left join tcDeliveryRequests  dr on dr.check_number = A.check_number 
                //                                    where 1=1  and A.check_number = @check_number  and A.scan_item in('4')
                //                                    order by A.log_id desc");
                cmd.CommandText = string.Format(@"DECLARE @ORDER_NUMBER NVARCHAR(20)
                                                  DECLARE @sign_form_image nvarchar(50)
                                                  DECLARE @sign_field_image nvarchar(50)
                                                  DECLARE @receipt_image nvarchar(MAX)
                                                  DECLARE @customer_code NVARCHAR(10)
                                                  DECLARE @supplier_code NVARCHAR(3)
                                                  DECLARE @request_id bigint

                                                  select top 1 @sign_form_image=A.sign_form_image, @sign_field_image=A.sign_field_image, @customer_code=dr.customer_code ,@supplier_code=dr.supplier_code  , 
                                                  @request_id=dr.request_id 
                                                  from ttDeliveryScanLog A with(nolock)
                                                  left join tcDeliveryRequests  dr on dr.check_number = A.check_number 
                                                  --left join ttDeliveryFile rtn on A.check_number  = rtn.check_number  and dr.request_id  = rtn.request_id 
                                                  where 1=1  and A.check_number = @check_number  and A.scan_item in('4')
                                                  order by A.log_id desc

                                                  select top 1 @receipt_image = ISNULL(file_name ,'') from ttDeliveryFileN where check_number = @check_number  order by udate desc --ISNULL(file_path,'') +  

                                                  IF (@sign_form_image IS null OR @sign_form_image='') BEGIN 
	                                                  SELECT @ORDER_NUMBER=ISNULL(order_number,'')  FROM tcDeliveryRequests  with(nolock) WHERE check_number = @check_number 
                                                      IF (@ORDER_NUMBER <> '') BEGIN 
	                                                      SELECT top 1 @sign_form_image=A.sign_form_image, @sign_field_image=A.sign_field_image, @customer_code=dr.customer_code ,@supplier_code=dr.supplier_code  
	                                                      from ttDeliveryScanLog A with(nolock)
	                                                      left join tcDeliveryRequests  dr on dr.check_number = A.check_number 
	                                                      where 1=1  and dr.order_number = @ORDER_NUMBER  and A.scan_item in('4')
	                                                      order by A.log_id desc 
                                                      END
                                                  END

                                                  SELECT @sign_form_image as sign_form_image,@sign_field_image as sign_field_image,@customer_code as customer_code,@supplier_code as supplier_code, @receipt_image as receipt_image, @request_id as request_id");
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string filename = "";
                        string rtn_filename = "";
                        switch (manager_type)
                        {
                            case "0":  //總公司、系統管理員才可以看到整張簽單
                            case "1":
                            case "2":
                                filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                break;
                            case "4": //區配商：有開放權限，可看到自已的貨和區配底下自營的貨
                                string supplier_code = Session["master_code"] != null ? Session["master_code"].ToString().Substring(0, 3) : "";
                                string dr_supplier_code = (dt.Rows[0]["supplier_code"] != DBNull.Value && dt.Rows[0]["supplier_code"].ToString() != "") ? dt.Rows[0]["supplier_code"].ToString() : (dt.Rows[0]["customer_code"] != DBNull.Value && dt.Rows[0]["customer_code"].ToString().Length >= 3) ? dt.Rows[0]["customer_code"].ToString().Substring(0, 3) : "";

                                List<string> showSupplierCode = new List<string>();

                                using (SqlCommand s = new SqlCommand())
                                {
                                    s.CommandText = @"select distinct supplier_code from tbCustomers where supplier_code not in ('000' ,'001') and supplier_code not between 'F10' and 'F99' and stop_shipping_code <> '4'";
                                    DataTable dataTable = dbAdapter.getDataTable(s);
                                    showSupplierCode = dataTable.AsEnumerable().Select(a => a.Field<string>("supplier_code")).ToList();
                                }

                                    if (supplier_code == dr_supplier_code &&
                                       showSupplierCode.Contains(supplier_code))
                                       //(supplier_code == "B01" || supplier_code == "B03" || supplier_code == "B04" || supplier_code == "A03" || supplier_code == "F02" || supplier_code == "C01" || supplier_code == "C04" || supplier_code == "C05" || supplier_code == "D01" || supplier_code == "A01" || supplier_code == "G01" || supplier_code == "B02" || supplier_code == "E01"))   //待補，應該要有設定介面
                                    {
                                        //1.以霖   B04 
                                        //2.鴻駿   B01
                                        //3.錦濱   A03
                                        //4.宜益   F02
                                        //5.億鎮   C01
                                        //6.今齊   C04
                                        //7.成邑   C05
                                        //8.長隆   D01
                                        //9.成佑   A01
                                        //10.優鮮配 G01
                                        //11.國瑋   B02
                                        //12.瑞高   B03
                                        //13.百通   E01

                                        filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    }
                                    else
                                    {
                                        filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    }
                                break;
                            case "3":
                            case "5": //自營商：自已的貨可以看到整張簽單
                                string customer_code = dt.Rows[0]["customer_code"] != DBNull.Value ? dt.Rows[0]["customer_code"].ToString() : "";
                                string master_code = Session["master_code"] != null ? Session["master_code"].ToString() : "";
                                if (customer_code.IndexOf(master_code) > -1)
                                {
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                }
                                else
                                {
                                    filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                }
                                break;
                            default:
                                filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                break;
                        }

                        string filename_split = check_number.Text;
                        int split_length = 5; //檔案每幾個字元會建立一個資料夾
                        string path = "";
                        int folder_deepth = filename_split.Length / split_length;
                        for (var i = 0; i < folder_deepth; i++)
                        {
                            path += filename_split.Substring(i * 5, 5) + @"/";
                        }
                        path += check_number.Text + ".jpg";
                        if (filename != "")
                        {
                            Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename + "&ImgUrl2=https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "&ImgUrl3=https://webservice.fs-express.com.tw/PHOTO/" + filename + "&ImgUrl4=http://webserviceground.fs-express.com.tw/PHOTO/" + filename + "&ImgUrl5=https://erp.fs-express.com.tw/UploadImages/SignPaperPhoto/photo/" + path );
                        }
                        else
                        {
                            ImageButton1.Visible = false;
                        }

                        #region 回單影像
                        rtn_filename = dt.Rows[0]["receipt_image"] != DBNull.Value ? dt.Rows[0]["receipt_image"].ToString() : "";
                        if (rtn_filename != "")
                        {
                            Regex reg = new Regex(";");
                            if (reg.IsMatch(rtn_filename))
                            {
                                string[] _rtn_filename = rtn_filename.Split(';');
                                int _count = 0;
                                foreach (string image in _rtn_filename)
                                {
                                    if (image != "")
                                    {
                                        string iconpath = "http://220.128.210.180:10080/files/receipt/" + image;
                                        string pdf = "http://220.128.210.180:10080/files/receipt/" + "pdf.png";
                                        string fileExtension = System.IO.Path.GetExtension(image).ToLower().ToString();

                                        if (fileExtension == ".pdf")
                                        {

                                            Panel _myPanel = myPanel;
                                            LinkButton l = new LinkButton();
                                            l.ID = "Linkimage" + _count;
                                            l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + image);
                                            l.Attributes.Add("target", "_blank");
                                            l.CssClass = "fancybox";
                                            l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", pdf, "imgSize");
                                            
                                            _myPanel.Controls.Add(l);
                                        }
                                        else
                                        {

                                            Panel _myPanel = myPanel;
                                            LinkButton l = new LinkButton();
                                            l.ID = "Linkimage" + _count;
                                            l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + image);
                                            l.Attributes.Add("target", "_blank");
                                            l.CssClass = "fancybox";
                                            l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", iconpath, "imgSize");
                                            //Image i = new Image();
                                            //i.ID = "image" + _count;

                                            //i.ImageUrl = "http://220.128.210.180:10080/files/receipt/" + image;
                                            _myPanel.Controls.Add(l);
                                            //_myPanel.Controls.Add(i);


                                            //RtnImglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/files/receipt/" + "&filename=" + rtn_filename);
                                            //RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + image);
                                            //RtnImglink.CssClass = "fancybox fancybox.iframe";
                                        }

                                    }
                                    _count++;
                                }
                            }
                            else
                            {

                                string fileExtension = System.IO.Path.GetExtension(rtn_filename).ToLower().ToString();

                                string iconpath = "http://220.128.210.180:10080/files/receipt/" + rtn_filename;
                                //if (fileExtension == ".pdf")
                                //{

                                //    // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ImgUrl + filename + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                                //    //RtnImglink.Attributes.Add("onclick", "window.open('" + rtn_filename + "','mywindow','width=800,height=600, toolbar=yes');");
                                //    //RtnImglink.Attributes.Add("onclick", "<script>$('#modal-default_01').modal();</script>");
                                //    //RtnImglink.Attributes.Remove("href");

                                //}
                                //else
                                //{
                                //    //RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + rtn_filename);
                                //    //RtnImglink.CssClass = "fancybox fancybox.iframe";
                                //}
                                Panel _myPanel = myPanel;
                                LinkButton l = new LinkButton();
                                l.ID = "Linkimage" + 0;
                                l.Attributes.Add("href", "http://220.128.210.180:10080/files/receipt/" + rtn_filename);
                                l.CssClass = "fancybox";
                                l.Text = string.Format(@"<img src=""{0}"" class=""{1}"" />", iconpath, "imgSize");
                                //Image i = new Image();
                                //i.ID = "image" + _count;

                                //i.ImageUrl = "http://220.128.210.180:10080/files/receipt/" + image;
                                _myPanel.Controls.Add(l);
                            }




                        }
                        else
                        {
                            //ImageButton2.Visible = false;
                        }
                        #endregion


                        //RtnImglink.ClientID
                        rtnclick.Visible = true;
                        rtnclick.Attributes.Add("href", "fileupload_receipt.aspx?check_number=" + check_number.Text.Trim() + "&request_id=" + dt.Rows[0]["request_id"].ToString() + "&RtnImglink=" +"0" );  //回單上傳鈕

                    }
                    else
                    {
                        ImageButton1.Visible = false;
                        //ImageButton2.Visible = false;
                        rtnclick.Visible = false;
                    }
                }
            }


        }
    }
    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }
    
    private void paramlocation()
    {
        string querystring = "";
        //if (date.Text.ToString() != "")
        //{
        //    querystring += "&date=" + date.Text.ToString();
        //}
        if (check_number.Text.ToString() != "")
        {
            querystring += "&check_number=" + check_number.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/trace_1.aspx?search=yes" + querystring)); // Response.Redirect在預設情況下是在本頁跳轉 → page_load
    }

    

    protected void btnDetail_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divMaster.Visible = false;
        readdetail();
    }

    protected void btExit_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divMaster.Visible = true;

    }

    protected void dlP1_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 處置說明-說明項目
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@code_id", dlP1.SelectedValue);
            cmd.CommandText = "select * from tbItemCodes where code_bclass = '5' and code_sclass = 'P2'  and code_id = @code_id and active_flag = 1 order by seq";
            dlP2.DataSource = dbAdapter.getDataTable(cmd);
            dlP2.DataValueField = "seq";
            dlP2.DataTextField = "code_name";
            dlP2.DataBind();
            dlP2.Items.Insert(0, new ListItem("===請選擇===", ""));
        }
        #endregion
    }

    protected void btnSaveDatail_Click(object sender, EventArgs e)
    {
        //處置說明記錄檔 ttDeliveryDisposeLog
        #region 新增    
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);                              //貨運單號
            cmd.Parameters.AddWithValue("@status_type", dlP1.SelectedValue.ToString());                   //狀況類別1
            //cmd.Parameters.AddWithValue("@status_type", dlP2.SelectedValue.ToString());                   //狀況類別2
            cmd.Parameters.AddWithValue("@dispose_desc", dispose_desc.Text);                              //處置說明

            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間            
            cmd.CommandText = dbAdapter.SQLdosomething("ttDeliveryDisposeLog", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
        }

        #endregion
        readdetail();
        dispose_desc.Text = string.Empty;
        dlP1.Text = string.Empty;
        dlP2.Text = string.Empty;
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);
        return;

    }

    private void readdetail()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            cmd.CommandText = @"select  ROW_NUMBER() OVER(ORDER BY A.cdate desc) AS NO ,
                                Convert(varchar, A.cdate,120) as cdate , B.code_name , A.dispose_desc,C.user_name 
                                from ttDeliveryDisposeLog A with(nolock)
                                left join  tbItemCodes B with(nolock) on A.status_type = B.code_id and   code_bclass = '5' and code_sclass = 'P1'
                                left join tbAccounts C with(nolock) on A.cuser = C.account_code
                                where check_number = @check_number and check_number <>''";
            Datail_List.DataSource = dbAdapter.getDataTable(cmd);
            Datail_List.DataBind();
        }
    }

    //protected void New_List_02_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
    //    {

    //        string scan_item = (DataBinder.GetPropertyValue(e.Item.DataItem, "scan_item")).ToString();
    //        string filename = (DataBinder.GetPropertyValue(e.Item.DataItem, "sign_form_image")).ToString();
    //        ImageButton image = ((ImageButton)e.Item.FindControl("ImageButton1"));
    //        HyperLink Imglink = ((HyperLink)e.Item.FindControl("Imglink"));

    //        if (scan_item == "3")
    //        {
    //            Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename );
    //            //image.ImageUrl =  + imgurl;
    //        }
    //        else
    //        {
    //            image.Visible = false;
    //        }

    //    }


    //}

    //public enum Definition
    //{
    //    One = 1, Two = 2, Three = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10
    //}
    ///// <summary>
    ///// 將PDF文件轉換為圖片的方法
    ///// </summary>
    ///// <param name="pdfInputPath">PDF檔案路徑</param>
    ///// <param name="imageOutputPath">圖片輸出路徑</param>
    ///// <param name="imageName">生成圖片的名字</param>
    ///// <param name="startPageNum">從PDF文件的第幾頁開始轉換</param>
    ///// <param name="endPageNum">從PDF文件的第幾頁開始停止轉換</param>
    ///// <param name="imageFormat">設定所需圖片格式</param>
    ///// <param name="definition">設定圖片的清晰度，數字越大越清晰</param>
    //public  void ConvertPDF2Image(string pdfInputPath, string imageOutputPath,
    //    string imageName, int startPageNum, int endPageNum, ImageFormat imageFormat, Definition definition)
    //{
    //    PDFLibNet.PDFWrapper pdfWrapper = new PDFWrapper();
    //    pdfWrapper.LoadPDF(pdfInputPath);

    //    if (!System.IO.Directory.Exists(imageOutputPath))
    //    {
    //        Directory.CreateDirectory(imageOutputPath);
    //    }

    //    // validate pageNum
    //    if (startPageNum <= 0)
    //    {
    //        startPageNum = 1;
    //    }

    //    if (endPageNum > pdfWrapper.PageCount)
    //    {
    //        endPageNum = pdfWrapper.PageCount;
    //    }

    //    if (startPageNum > endPageNum)
    //    {
    //        int tempPageNum = startPageNum;
    //        startPageNum = endPageNum;
    //        endPageNum = startPageNum;
    //    }

    //    // start to convert each page
    //    for (int i = startPageNum; i <= endPageNum; i++)
    //    {
    //        pdfWrapper.ExportJpg(imageOutputPath + imageName + i.ToString() + ".jpg", i, i, 180, 80);//這裡可以設定輸出圖片的頁數、大小和圖片質量
    //        if (pdfWrapper.IsJpgBusy) { System.Threading.Thread.Sleep(100); }
    //    }

    //    pdfWrapper.Dispose();
    //}
}