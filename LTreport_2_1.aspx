﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTreport_2_1.aspx.cs" Inherits="LTreport_2_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
       input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }
        ._hide{
            display:none;
            visibility:hidden;
        }

        ._table, .wizard {
            width: 100%;
        }

        .table th {
            text-align: center;
        }

        ._li1 {
            width: 60%;
        }

        ._li2 {
            width: 35%;
        }

        .tb_search {
            width: 40% !important;
        }

        .date_picker {
            width: 110px !important;
        }

        ._ddl {
            min-width: 120px;
        }

        ._data {
            margin: 0px 5px;
            padding: 3px;
        }

        ._title {
            width: 5%;
        }

        ._data1 {
            width: 40%;
        }

        ._data2 {
            width: 25%;
        }

        ._data3 {
            width: 33%;
            padding: 0px 2%;
        }

        .btn {
            margin: 0px 10px;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }
        ._unit1,._unit2{
            cursor:help;
            padding:2px;
        }
        .td_ckb{
            width:65px;
        }
        .table_list tr:hover {
            background-color: lightyellow;
        }
        ._td_sender,.td_receiver{
            text-align:left;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
        function SetChk()
        {
            var _ckall = $("._ckb_all:checked").length;
            if (_ckall == 0) {
                $("._ckb_item").prop("checked", false);
            } else {
                $("._ckb_item").prop("checked", true);
            }
            SetPrintActive();
        }

        $(document).on("click", "._ckb_item", function () {           
            SetPrintActive();
        });

        $(document).on("click", ".btn_print", function () {
            var _ckall = $("._ckb_all:checked").length;
            var _ck = $("._ckb_item:checked").length;
            var _return = false;
            if ((_ckall + _ck) == 0) {
                alert("請勾選欲列印的項目!");                
            }
            else {
                var _result;
                if (_ckall == 0) {
                    _result = $("._ckb_item:checked").map(function () { return $(this).attr("_ckv"); }).get().join(',')

                } else {
                    _result = "0";
                }
                if (_result.length > 0) {
                    $("._tb_ckeck").val(_result).attr("value", _result);
                    _return = true;
                }
            }
            return _return;    
        });

        function SetPrintActive() {
            if ($("._ckb_item:checked").length > 0 ) {
                $(".btn_print").removeAttr('disabled');  
                if (!$("._li2").hasClass("current")) {
                    $("._li2").addClass("current");
                }
            } else {
                $(".btn_print").attr('disabled', 'disabled');
                $("._li2").removeClass("current");
            }
        }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">到著簽單列印</h2>
            <div class="form-style-10">
                <!-- 流程 -->
                <ul class="wizard">
                    <li class="current _li1">1. 查 詢 簽 收 單</li>
                    <li class="_li2">2. 選 取 列 印 明 細</li>
                </ul>

                <table class="_table form-group form-inline">
                    <tr>
                        <td class="_data _data1">
                            <asp:TextBox ID="tb_search" placeholder="ex:MD 工號" runat="server" AutoPostBack="true" OnTextChanged="tbsearch_TextChanged"  CssClass="tb_search form-control" MaxLength="20"></asp:TextBox>
                            配送站：
                            <asp:DropDownList ID="ddl_supplyer" runat="server" CssClass="form-control _ddl" placeholder="請選擇配配送站"></asp:DropDownList>
                        </td>
                        <td class="_title">
                             <asp:RadioButton id="rb1" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged" Checked="true"/>
                          <asp:Label ID="Label3" runat="server" Text="發送日期"></asp:Label>
                            <%--<span class="_tip_important">發送期間</span>--%>
                        </td>
                        <td class="_data _data2">
                            <asp:TextBox ID="tb_dateStart" runat="server" CssClass="date_picker form-control" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                            <span>~</span>
                            <asp:TextBox ID="tb_dateEnd" runat="server" CssClass="date_picker form-control" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req_dateStart" runat="server" ControlToValidate="tb_dateStart" ForeColor="Red" ValidationGroup="validate">請選擇配送開始日</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req_dateEnd" runat="server" ControlToValidate="tb_dateEnd" ForeColor="Red" ValidationGroup="validate">請選擇配送結束日</asp:RequiredFieldValidator>--%>
                            <asp:Button ID="btn_Search" runat="server" CssClass="templatemo-blue-button btn" ValidationGroup="validate" Text="查 詢" OnClick="btn_Search_Click" />
                        </td>
                        <td class="_data _data3">
                            <span class="text-danger span_tip">※ 列印格式 : B4 (一式6筆簽收單)</span>
                            <asp:Button ID="btn_Print" runat="server" Text="列 印" CssClass="templatemo-blue-button btn btn_print" ToolTip="列　印" OnClick="btn_Print_Click" disabled="disabled" />
                        </td>
                    </tr>
                    <tr>
                          <td class="_title">
                              &emsp;單筆貨號：
                              <asp:TextBox ID="strCheck_number" placeholder="ex:MD 貨號" runat="server"  CssClass="tb_search form-control" MaxLength="20"></asp:TextBox>
                          </td>
                         <td class="_title"><asp:RadioButton id="rb2" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged"/>
                            <asp:Label ID="Label4" runat="server" Text="掃讀日期"></asp:Label></td>
                         <td class="_data _data2">
                            <asp:TextBox ID="sscan_date" runat="server" CssClass="date_picker form-control" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                            <span>~</span>
                            <asp:TextBox ID="escan_date" runat="server" CssClass="date_picker form-control" placeholder="YYYY/MM/DD" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                </table>

            </div>
            <hr />
            <table class="table table-striped table-bordered templatemo-user-table table_list">
                <tr class="tr-only-hide">
                    <th class="td_ckb">
                        <input type="checkbox" name="ckb_all" class="_ckb _ckb_all" onchange="SetChk()" /><label>全 選</label>
                    </th>
                    <th>No.</th>
                    <th>貨　號</th>
                    <th>寄件者</th>
                    <th>收件者</th>
                    <th>配達狀況</th>
                    <th>發送日</th>
                    <th>配送日</th>
                    <th>配達日</th>
                    <th>配送異常</th>
                    <th>貨件運費</th>
                    <th>配送費用</th>
                    <th>配送商</th>
                    <th>發送站</th>
                    <th>數　量</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr class ="paginate" title='貨號: <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'>
                            <td class="td_ckb">
                             <input type="checkbox" class="_ckb _ckb_item" _ckv='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' name='ckb_<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>'  />
                                
                            </td>
                            <td data-th="No.">
                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.rowid").ToString())%>
                            </td>
                            <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%> </td>
                            <td class="_td_sender" data-th="寄件者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%></td>
                            <td class="td_receiver" data-th="收件者"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                            <td data-th="配達狀況"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_state").ToString())%></td>
                            <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%> </td>
                            <td data-th="配送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date","{0:yyyy/MM/dd}").ToString())%></td>
                            <td data-th="配達日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_date","{0:yyyy/MM/dd}").ToString())%></td>
                            <td data-th="配送異常"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item").ToString())%></td>
                            <td data-th="貨件運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ship_fee").ToString())%></td>
                            <td data-th="配送費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cscetion_fee").ToString())%></td>
                            <td data-th="配送商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arr").ToString())%></td>
                            <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sta_name").ToString())%></td>
                            <td data-th="數　量">
                                <span class="_unit _unit1" title="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> 件</span>
                                <span class="_unit _unitsp">/</span>
                                <span class="_unit _unit2" title='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pricing_type").ToString()== "02"? "件數":"棧板數")%>'>
                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pricing_type").ToString()== "03"? DataBinder.Eval(Container, "DataItem.cbm").ToString() +"才": DataBinder.Eval(Container, "DataItem.plates").ToString()+"板")%>
                                </span>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="15" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <%--<div class="pager">
                <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                共 <span class="text-danger _rownum">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span> 筆
            </div>--%>
            <div id="page-nav" class="page">
            </div>
            <asp:Label runat="server" ID="rowcount" Visible="false" Text=""></asp:Label>
            <asp:TextBox ID="tb_ckeck" CssClass="_hide _tb_ckeck" runat="server"></asp:TextBox>

        </div>
    </div>

</asp:Content>

