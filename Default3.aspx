﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default3.aspx.cs" Inherits="Default3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <asp:Panel runat="server" ID="Panel1">
    <form id="form1" runat="server" style="width:100%">
         <!--設定width為100%以剛好符合PDF文件的寬度-->
      <!--如果width為固定寬度的話，C#那邊可能要改寫成Document doc = new Document(PageSize.A3)-->
      <!--此table已調整好高度剛好符合PDF A4一頁的高度了-->
      <table style='width:100%;'  >
       <%-- <tr >
            <td colspan='2' style='color:#808080;'>
               <h2>[廠商名稱]</h2>
            </td>
        </tr>--%>
       <%-- <tr style='font-size:13px;'>
         <%--<td style='width:600px;color:#808080;font-size:16px;'>Taiwan Limited<br />4F,144 Changchun Rd., Taipei 104, Taiwan</td>
         <%--<td style='width:80%;color:#808080;font-size:16px;text-align:right;'>[頁碼]/[總頁數]</td>
         </tr>--%>
       
        <tr>
            <td  colspan='2'  style='text-align:center; height:90px;'><h1>FSE託運總表</h1></td>
        </tr>
        <tr>
            <td  colspan='2'  style='text-align:left;font-size:20px;'>發送日期：<asp:Label runat="server" ID="date"></asp:Label></td>
        </tr>
        <tr>
            <td  colspan='2'  style='height:280px;vertical-align:top;font-size:20px;'  > 
                <table  style='border:1px solid #000000; width:100%;' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>序號</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>訂單編號</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>貨號</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>收件人</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>件數</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>地址</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>到著站</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>收件人電話</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>代收金額</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>付款別</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>平臺名稱</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>商品名稱</td>
                        <td style='border:1px solid #000000;text-align:center;font-size:20px;'>備註</td>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="序號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.序號").ToString())%></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.訂單編號").ToString())%></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="貨號"><asp:Label ID="lbprint_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%>'></asp:Label></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="收件人"><asp:Label ID="lbSupplier_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人").ToString())%>'></asp:Label></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="件數"><asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%>'></asp:Label></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.地址").ToString())%> </td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著站").ToString())%></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="收件人電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收件人電話").ToString())%></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%> </td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="付款別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.付款別").ToString())%> </td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="平臺名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.平臺名稱").ToString())%> </td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="商品名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.商品名稱").ToString())%></td>
                                        <td  style='border:1px solid #000000;text-align:center;font-size:20px;' data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.備註").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                </table>
            </td>
        </tr> 
          <tr>
            <td  colspan='2'  style='font-size:20px;' >總計：<asp:Label runat="server" ID="Label1"></asp:Label></td>
        </tr>
        <tr>
            <td  colspan='2'  style='font-size:20px;' > 確認無誤，特此證明。  </td>
        </tr>
        <tr>
            <td  colspan='2'  style='height:100px;'> 
                <div style='float:right;'>
                <table style='border:1px solid #000000; ' cellpadding='0' cellspacing='0'>
                   <tr>
                       <td style='border:1px solid #000000;width:180px;height:100px;text-align:right; vertical-align:bottom;padding-bottom:10px;font-size:16px;'>客戶簽收</td>
                       <td style='border:1px solid #000000;width:180px;height:100px;text-align:right;vertical-align:bottom;padding-bottom:10px;font-size:16px;'>收貨司機</td>
                   </tr>
               </table>
                </div>
            </td>
        </tr>
    </table>
    
    </form>
        </asp:Panel>
</body>
</html>
