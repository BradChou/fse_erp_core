﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pallet_add.aspx.cs" Inherits="pallet_add" EnableEventValidation="false" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-託運單修改</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>

    <link href="css/build.css" rel="stylesheet" />

    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").val($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").val($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $(".chosen-select").chosen();

            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date())  //預設當日
            });
        });

    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .radio {
            padding-left: 5px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
            min-width: 0px;
        }

        .checkbox {
            padding-left: 15px;
        }

        ._shortwidth {
            width: 80px;
        }

        .ralign {
            text-align: right;
        }

        .ui-datepicker select.ui-datepicker-month,
        .ui-datepicker select.ui-datepicker-year {
            color: black;
        }

        .ui-datepicker a {
            color: #0275d8;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="templatemo-content-widget white-bg">
            <div class="modal-body">
                <div class="row">
                    <span class="section">1</span>選擇要出單的項目
                <div id="custom_table" class="templatemo-login-form">
                    <div class="inner-wrap">
                        <asp:UpdatePanel ID="upd_return" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th>
                                            <asp:CheckBox ID="chkHeader" runat="server" /></th>
                                        <th>回收時間</th>
                                        <th>回收板數</th>
                                        <th>到著碼</th>
                                        <th>備註</th>
                                        <th>客代</th>
                                    </tr>
                                    <asp:Repeater ID="New_List" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %>
                                                    <asp:CheckBox ID="chkRow" runat="server" />
                                                    <asp:HiddenField ID="Hid_rid" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.id").ToString())%>' />
                                                </td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.return_date").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.return_cnt").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.invoice_desc").ToString())%></td>
                                                <td><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="6" style="text-align: center">查無棧板歸還記錄</td>
                                    </tr>
                                    <% } %>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                       
                    </div>
                </div>
                    <span class="section">2</span>選擇出單資訊
                    <div class=" form-group form-inline ">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode ="Conditional" >
                            <ContentTemplate>
                                <asp:HiddenField ID="Hid_pricing_type" runat="server" />
                                <label><span class="REDWD_b">*</span>寄件人客代</label>
                                <asp:DropDownList ID="dlcustomer_code" runat="server" CssClass="chosen-select" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代</asp:RequiredFieldValidator>


                                <label><span class="REDWD_b">*</span>發送日期</label><asp:TextBox ID="Shipments_date" runat="server" CssClass="date_picker"></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="Shipments_date" ForeColor="Red" ValidationGroup="validate">請選擇發送日期</asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group form-inline ">
                        <label for="inputLastName">指定日</label>
                        <asp:TextBox ID="arrive_assign_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                        <label for="inputLastName">時段</label>
                        <asp:DropDownList ID="time_period" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        &nbsp;
                    </div>
                    <div class="form-group form-inline ">
                        <label><span class="REDWD_b">*</span>收件人</label>
                        <asp:TextBox ID="receive_contact" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                        <div class="form-group">
                            <label><span class="REDWD_b">*</span>電話</label>
                            <asp:TextBox ID="receive_tel1" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                            分機
                                        <asp:TextBox ID="receive_tel1_ext" runat="server" class="form-control" MaxLength="5" Width="80"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group form-inline ">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode ="Conditional" >
                            <ContentTemplate>
                                <label><span class="REDWD_b">*</span>地&nbsp; 址</label>
                                <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control chosen-select _shortwidth" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                <asp:TextBox ID="receive_address" CssClass="form-control" runat="server" MaxLength="70" placeholder="請輸入地址"></asp:TextBox>
                                &nbsp;                                
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="cbarrive" runat="server" Text="站址自領" AutoPostBack="True" OnCheckedChanged="cbarrive_CheckedChanged" /></span>
                                <asp:DropDownList ID="area_arrive_code" runat="server" CssClass="form-control _shortwidth" AutoPostBack="True" OnSelectedIndexChanged="area_arrive_code_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="area_arrive_code" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇到著碼</asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>

                    <div class="form-group form-inline alert-danger  ">
                        <asp:Label ID="Label1" runat="server" Text="件數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="pieces" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Label ID="Label2" runat="server" Text="板數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="plates" CssClass="form-control chkcount" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0" Enabled="false"></asp:TextBox>&nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" Text="才數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="cbm" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>
                    </div>
                    <div class="form-group form-inline">
                        <label for="inputLastName">備　　註</label>
                        <textarea id="invoice_desc" runat="server" cols="30" rows="2" maxlength="50" class="form-control"></textarea>
                    </div>

                    <div class="form-group form-inline">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode ="Conditional" >
                            <ContentTemplate>
                                <label>&nbsp;寄件地址</label>
                                <asp:DropDownList ID="send_city" runat="server" CssClass="form-control chosen-select" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                <asp:DropDownList ID="send_area" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:TextBox ID="send_address" CssClass="form-control" runat="server"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>


                </div>
            </div>

            <div class="modal-footer ">
                <div class="form-group text-center">
                    <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                    <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                    <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
                </div>
            </div>
        </div>

    </form>

</body>
</html>
