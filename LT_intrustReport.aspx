﻿<%@ Page Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LT_intrustReport.aspx.cs" Inherits="LT_intrustReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />   
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>
    
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align :left ;
        }

        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">當日配送、配達明細表</h2>
            <hr />
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                            <label>配送日期：</label>
                            <asp:TextBox ID="Send_date" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                            <label>配送站：</label>
                            <asp:DropDownList ID="ddlSendStation" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSendStation_SelectedIndexChanged"></asp:DropDownList>
                            <label>掃描工號：</label>
                            <asp:DropDownList ID="ddlscanname" runat="server" class="form-control"></asp:DropDownList>
                            <label>區域碼：</label>
                            <asp:DropDownList ID="Station" runat="server" class="form-control"></asp:DropDownList>
                </div>
                <div class="form-group form-inline">
                     <asp:Button ID="btnselect" CssClass="btn btn-primary btn_view" runat="server" Text="查 詢" OnClick="btnQry_Click" />
                     <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="匯　出" OnClick="btExport_Click"  />
                     <a id="_dw" class="_hide" href="#" target="_blank" style="display: none; visibility: hidden;">開窗列印</a>
                </div>
            </div>
            <hr>
            <p class="text-primary"><label>當日配送明細表</label>&emsp;&emsp;&emsp;<label>配送日：</label><asp:Label ID="lbprintdate" runat="server"></asp:Label><label>配送人：</label><asp:Label ID="lbdriver" runat="server"></asp:Label></p>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <th class="header">序號</th>
                    <th class="header">託運單號</th>
                    <th class="header">傳票類別</th>
                    <th class="header">代收金額</th>
                    <th class="header">件數</th>
                    <th class="header">發送站</th>
                    <th class="header">作業站</th>
                    <th class="header">配送站</th>
                    <th class="header">最新作業別</th>
                    <th class="header">作業說明</th>
                    <th class="header">作業時間</th>
                    <th class="header">司機</th>
                    <th class="header">盤點在庫</th>
                </tr>
                <asp:Repeater ID="New_List" runat="server">
                    <ItemTemplate>
                        <tr class="paginate">
                            <td style="white-space: nowrap;" data-th="序號"><%# Container.ItemIndex +1 %></td>
                            <td style="white-space: nowrap;" data-th="託運單號">
<a href="LT_trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.託運單號").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.託運單號").ToString())%></a>
                                </td>
                            <td style="white-space: nowrap;" data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.傳票類別").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額","{0:N0}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.作業站").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="配送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送站").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="最新作業別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.最新作業別").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業說明"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配異說明").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.作業時間").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.司機").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="盤點在庫"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.盤點在庫").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="13" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
            <div id="page-nav" class="page"></div>
        </div>
    </div>
</asp:Content>
