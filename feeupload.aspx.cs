﻿using NPOI.HSSF.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class feeupload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //customer_code=0000005001&pricing_code=01&cus=163
            //Exec usp_GetTodayPriceBusDefLog @customer_code = '0000003001';
            // Exec usp_GetTodayPriceBusDefLog @customer_code = '0000003001',@return_type = 1 ;
            string customer_code = "";
            if (Request.QueryString["customer_code"] != null) customer_code = Request.QueryString["customer_code"].ToString();
            SetMinShipFeeForJs(customer_code);
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid) return;

        DateTime dt_temp = new DateTime();
        int i_ShipType;
        string file01str = "";//new file name
        Boolean IsChk = true;//檢查是否OK

        lbMsg.Items.Clear();

        #region 檢查
        if (!int.TryParse(rad_ShipType.SelectedValue, out i_ShipType))
        {
            MsgShow("請選取運價類型!");
            IsChk = false;
            return;
        }

        if (!DateTime.TryParse(tariffs_effect_date.Text, out dt_temp))
        {
            MsgShow("請設定運價生效日!");
            IsChk = false;
            return;
        }

        // 1.公版 2.自訂
        if (i_ShipType == 2)
        {
            if (!file01.HasFile)
            {
                MsgShow("請選擇要上傳的檔案!");
                IsChk = false;
                return;
            }
            else
            {
                int fileMaxSize = 5242880;
                string[] allowedExtensions = { ".xls", ".xlsx" }; //定義允許的檔案格式
                string fileExtension = "";

                if (file01.PostedFile.ContentLength > fileMaxSize)
                {
                    MsgShow("檔案 [ " + file01.PostedFile.FileName + " ] 限傳5MB, 請重新選擇檔案!");
                    IsChk = false;
                    return;
                }

                //驗證上傳檔案的格式
                fileExtension = System.IO.Path.GetExtension(file01.FileName).ToLower().ToString();
                if (!allowedExtensions.Contains(fileExtension))
                {
                    MsgShow("上傳檔案格式錯誤，請上傳excel格式!");
                    IsChk = false;
                    return;
                }
            }
        }
        #endregion


        if (IsChk)
        {
            try
            {
                String customer_code = "";//客戶代號
                String pricing_code = "";//計價模式
                int i_customer_id = 0;
                if (Request.QueryString["customer_code"] != null) customer_code = Request.QueryString["customer_code"].ToString();
                if (Request.QueryString["pricing_code"] != null) pricing_code = Request.QueryString["pricing_code"].ToString();
                if (Request.QueryString["cus"] != null) int.TryParse(Request.QueryString["cus"].ToString(), out i_customer_id);

                if ((file01.HasFile && i_ShipType == 2))
                {
                    string strFileName = file01.FileName;
                    string strDotExtName = strFileName.Substring(strFileName.IndexOf("."));
                    string strNewFileName = file01.FileName.Substring(0, file01.FileName.Length - strDotExtName.Length) + DateTime.Now.ToString("yyyyMMdd");
                    string fileLocation = Server.MapPath("~/files/") + strNewFileName + strDotExtName;
                    file01.PostedFile.SaveAs(fileLocation);
                    file01str = strNewFileName + strDotExtName;  //new file name
                }

                //開始匯入
                Boolean IsExcelToDB = ExcelToDB(file01, customer_code, pricing_code, tariffs_effect_date.Text, i_ShipType, file01str);
                if (!IsExcelToDB)
                {
                    lbMsg.Items.Add("匯入失敗，請重新確認! ");
                }
                else
                {
                    Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板=0 or 零擔=1



                    //匯入完成後，客戶頁重新整理，運價表才會秀出最近的資料

                    if(Less_than_truckload == "0")
                    {
                        #region 2018/12/06 如果上傳新的運價生效日大於最新結帳日，則重新計算 (生效日~當日)區間的託運單運費
                        using (SqlCommand cmd_fee = new SqlCommand())
                        {
                            cmd_fee.CommandText = "uspWeb_CalShipFee";
                            cmd_fee.CommandType = CommandType.StoredProcedure;
                            cmd_fee.Parameters.AddWithValue("@customer_code", customer_code);
                            cmd_fee.Parameters.AddWithValue("@sdate", tariffs_effect_date.Text.Trim());
                            cmd_fee.Parameters.AddWithValue("@edate", DateTime.Today.ToString("yyyy/MM/dd"));
                            dbAdapter.getDataTable(cmd_fee);
                        }
                        #endregion
                    }


                    if (Request.QueryString["ctlid"] != null)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('匯入完成!');self.parent.$('#" + Request.QueryString["ctlid"].ToString() + "').click(); parent.$.fancybox.close(); ", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('匯入完成!');parent.$.fancybox.close(); parent.location.reload(true);", true);
                    }
                        
                }

            }
            catch (Exception ex)
            {
                MsgShow("發生錯誤，請聯絡系統人員! " + ex.Message);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>parent.$.fancybox.close();</script>", false);
                return;
            }
        }
    }

    /// <summary>回傳訊息至使用者介面</summary>
    /// <param name="str_msg">訊息內容</param>
    /// <param name="other_strJS">其他自訂JS語法，可不填</param>
    public void MsgShow(string str_msg, string other_strJS = "")
    {
        if (!string.IsNullOrEmpty(str_msg))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + str_msg + "');" + other_strJS, true);
        }
    }


    /// <summary>ExcelToDB </summary>
    /// <param name="this_file">Excel file</param>
    /// <param name="customer_code">客戶代碼</param>
    /// <param name="pricing_code">計價模式</param>
    /// <param name="active_date">生效日(yyyy/MM/dd)</param>
    /// <param name="tariffs_type">運價類別 1:公版(預設) 2:自訂</param>
    /// <param name="file_name"></param>
    /// <returns></returns>
    public Boolean ExcelToDB(FileUpload this_file, string customer_code, string pricing_code, string active_date, int tariffs_type = 1, string file_name = null)
    {
        Boolean IsUpdateToDB = true;
        DateTime dt_temp;
        Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板=0 or 零擔=1


        try
        {
            lbMsg.Items.Clear();
            string strUser = string.Empty;
            if (Session["account_code"] != null) strUser = Session["account_code"].ToString();//tbAccounts.account_id
            DateTime.TryParse(active_date, out dt_temp);


            if (tariffs_type == 1 && !string.IsNullOrEmpty(customer_code))
            {
                #region 公版





                using (SqlCommand cmd = new SqlCommand())
                {

                    if (Less_than_truckload == "1")
                    {
                        DataTable dtS = new DataTable();
                        SqlCommand cmdS = new SqlCommand();
                        cmdS.Parameters.AddWithValue("@nowdate", dt_temp.ToString("yyyy-MM-dd"));
                        cmdS.Parameters.AddWithValue("@customer_code", customer_code);
                        cmdS.CommandText = "Select *  from  ttCheckoutCloseLog where close_enddate >= @nowdate and  customer_code=@customer_code";
                        dtS = dbAdapter.getDataTable(cmdS);
                        if (dtS.Rows.Count > 0)
                        {

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('匯入失敗，日期生效日必須大於最新結帳日!');parent.$.fancybox.close(); parent.location.reload(true);", true);
                        }
                        else
                        {
                            string strSQL = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(12)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(12), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                            cmd.CommandText = strSQL;
                            dbAdapter.execNonQuery(cmd);
                        }
                    }
                    else
                    {
                        string strSQL = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(12)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(12), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                        cmd.CommandText = strSQL;
                        dbAdapter.execNonQuery(cmd);
                    }    


                  
                }
              

                lbMsg.Items.Add("設定成功! ");
                #endregion
            }
            else
            {

                if(System.IO.Path.GetExtension(file01.FileName).ToLower().ToString() == ".xlsx")
                {
                    if (Less_than_truckload == "0")
                    {
                        #region 自訂
                        if (this_file.HasFile
                        && !string.IsNullOrEmpty(customer_code)
                        && !string.IsNullOrEmpty(pricing_code))
                        {
                            using (ExcelPackage workbook = new ExcelPackage(this_file.FileContent))
                            {
                                foreach (ExcelWorksheet u_sheet in workbook.Workbook.Worksheets)
                                {
                                    string ttSelectSheetName = u_sheet.Name;
                                    int row = u_sheet.Dimension.End.Row;
                                    int col = u_sheet.Dimension.End.Column;
                                    object[,] valueArray = u_sheet.Cells.GetValue<object[,]>();
                                    string[] Row_title = new string[col];
                                    //foreach (object[,] _this in u_sheet.Cells.GetValue<object[,]>())

                                    if (row < 2)
                                    {
                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName + "】欄位資訊有誤，請重新確認!");
                                        continue;
                                    }
                                    else
                                    {
                                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
                                    }

                                    //標題列
                                    for (int i = 0; i < col; i++) Row_title[i] = valueArray[0, i].ToString();

                                    #region 定義 foreach sheet
                                    Boolean IsSheetOk = true;//工作表驗證
                                    string tablename = string.Empty;
                                    string start_city = string.Empty;//起點區域 
                                    string end_city = string.Empty;//迄點區域                    

                                    //板數1 ~6
                                    string plate1_price = string.Empty;
                                    string plate2_price = string.Empty;
                                    string plate3_price = string.Empty;
                                    string plate4_price = string.Empty;
                                    string plate5_price = string.Empty;
                                    string plate6_price = string.Empty;
                                    string strRow = string.Empty;
                                    string strInsCol = string.Empty;
                                    string strInsVal = string.Empty;

                                    //寫入DB的資訊
                                    List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                                    StringBuilder sb_temp_ins = new StringBuilder();

                                    #endregion

                                    //每一列 
                                    for (int j = 1; j < row; j++)
                                    {
                                        #region 初始化
                                        tablename = "";
                                        start_city = "";
                                        end_city = "";
                                        plate1_price = "";
                                        plate2_price = "";
                                        plate3_price = "";
                                        plate4_price = "";
                                        plate5_price = "";
                                        plate6_price = "";
                                        strRow = (j + 1).ToString();
                                        #endregion

                                        #region 驗證&取值 IsSheetOk

                                        for (int k = 0; k < col; k++)
                                        {
                                            Boolean IsChkOk = true;
                                            switch (Row_title[k])
                                            {
                                                case "起點區域":
                                                    if (valueArray[j, k] != null) start_city = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(start_city))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;

                                                case "迄點區域":
                                                    if (valueArray[j, k] != null) end_city = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(end_city))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;

                                                case "板數1":
                                                    if (valueArray[j, k] != null) plate1_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate1_price) || !Utility.IsNumeric(plate1_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數2":
                                                    if (valueArray[j, k] != null) plate2_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate2_price) || !Utility.IsNumeric(plate2_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數3":
                                                    if (valueArray[j, k] != null) plate3_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate3_price) || !Utility.IsNumeric(plate3_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數4":
                                                    if (valueArray[j, k] != null) plate4_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate4_price) || !Utility.IsNumeric(plate4_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數5":
                                                    if (valueArray[j, k] != null) plate5_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate5_price) || !Utility.IsNumeric(plate5_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數6":
                                                    if (valueArray[j, k] != null) plate6_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate6_price) || !Utility.IsNumeric(plate6_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "運價表":
                                                    if (valueArray[j, k] != null) tablename = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(tablename))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "1件~20件":
                                                    if (valueArray[j, k] != null) plate1_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate1_price) || !Utility.IsNumeric(plate1_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "21件~40件":
                                                    if (valueArray[j, k] != null) plate2_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate2_price) || !Utility.IsNumeric(plate2_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "41件~60件":
                                                    if (valueArray[j, k] != null) plate3_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate3_price) || !Utility.IsNumeric(plate3_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "61件~80件":
                                                    if (valueArray[j, k] != null) plate4_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate4_price) || !Utility.IsNumeric(plate4_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "81件~100件":
                                                    if (valueArray[j, k] != null) plate5_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate5_price) || !Utility.IsNumeric(plate5_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "100件以上":
                                                    if (valueArray[j, k] != null) plate6_price = valueArray[j, k].ToString();
                                                    if (String.IsNullOrEmpty(plate6_price) || !Utility.IsNumeric(plate6_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                
                                            }

                                            if (!IsChkOk)
                                            {
                                                lbMsg.Items.Add("請確認【" + Row_title[k] + "】 第" + strRow + "列是否正確!");
                                            }
                                        }
                                        #endregion

                                        #region 組新增
                                        if (IsSheetOk)
                                        {
                                            //只有「自營公版」及 「自訂運價表」能匯到「自訂運價表 tbPriceBusinessDefine」
                                            if (new string[] { "tbPriceBusiness", "tbPriceBusinessDefine" }.Contains(tablename))
                                            {
                                                strInsCol = "start_city,end_city,customer_code, active_date,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            "N\'" + customer_code + "\', " +
                                                            "N\'" + active_date + "\', " +
                                                            "N\'" + pricing_code + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";

                                                sb_temp_ins.Append("INSERT INTO " + "tbPriceBusinessDefine" + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");
                                            }
                                            else if (new string[] { "PalletPiecesShippingFeePublicVersion", "PalletPiecesShippingFeeDefine" }.Contains(tablename))
                                            {
                                                strInsCol = "start_city,end_city,customer_code, active_date,twenty, forty,sixty,eighty,one_hundred,above_hundred";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            "N\'" + customer_code + "\', " +
                                                            "N\'" + active_date + "\', " +                                                            
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";

                                                sb_temp_ins.Append("INSERT INTO " + "PalletPiecesShippingFeeDefine" + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");
                                            }

                                            //每100筆組成一字串
                                            if (j % 100 == 0 || j == (row - 1))
                                            {
                                                sb_ins_list.Add(sb_temp_ins);
                                                sb_temp_ins = new StringBuilder();
                                            }
                                        }
                                        #endregion

                                    }

                                    if (IsSheetOk)
                                    {
                                        //新增自訂
                                        String strSQL;
                                        foreach (StringBuilder sb in sb_ins_list)
                                        {
                                            if (sb.Length > 0)
                                            {
                                                strSQL = sb.ToString();
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    cmd.CommandText = strSQL;
                                                    dbAdapter.execNonQuery(cmd);
                                                }
                                            }
                                        }

                                        //傳db成功 寫入 ttPriceBusinessDefineLog     
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            strSQL = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(10)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(10), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                                            cmd.CommandText = strSQL;
                                            dbAdapter.execNonQuery(cmd);
                                        }
                                    }
                                    else
                                    {
                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                                        IsUpdateToDB = false;
                                    }
                                    lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                                }

                            }

                            lbMsg.Items.Add("匯入完畢");
                        }
                        else
                        {
                            IsUpdateToDB = false;
                        }
                        #endregion
                    }  //棧板
                    else
                    {

                        using (SqlCommand cmdS = new SqlCommand())
                        {
                            Boolean IsSheetOk = true;//工作表驗證
                            DataTable dtS = new DataTable();
                            cmdS.Parameters.AddWithValue("@nowdate", dt_temp.ToString("yyyy-MM-dd"));
                            cmdS.Parameters.AddWithValue("@customer_code", customer_code);
                            cmdS.CommandText = "Select *  from  ttCheckoutCloseLog where close_enddate >= @nowdate and  customer_code=@customer_code";
                            dtS = dbAdapter.getDataTable(cmdS);
                            if (dtS.Rows.Count > 0)
                            {

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('匯入失敗，日期生效日必須大於最新結帳日!');parent.$.fancybox.close(); parent.location.reload(true);", true);
                                IsSheetOk = false;


                            }
                            else
                            {
                                if (this_file.HasFile && !string.IsNullOrEmpty(customer_code))
                                {
                                    using (ExcelPackage workbook = new ExcelPackage(this_file.FileContent))
                                    {
                                        foreach (ExcelWorksheet u_sheet in workbook.Workbook.Worksheets)
                                        {
                                            string ttSelectSheetName = u_sheet.Name;
                                            int row = u_sheet.Dimension.End.Row;
                                            int col = u_sheet.Dimension.End.Column;
                                            object[,] valueArray = u_sheet.Cells.GetValue<object[,]>();
                                            string first_price = "";
                                            string add_price = "";
                                            string[] Row_title = new string[col];
                                            //foreach (object[,] _this in u_sheet.Cells.GetValue<object[,]>())
                                            if (row < 2)
                                            {
                                                lbMsg.Items.Add("工作表【" + ttSelectSheetName + "】欄位資訊有誤，請重新確認!");
                                                continue;
                                            }
                                            else
                                            {
                                                lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));
                                            }

                                            for (int i = 0; i < col; i++) Row_title[i] = valueArray[0, i].ToString();
                                            #region 定義 foreach sheet

                                            string tablename = string.Empty;
                                            string start_city = string.Empty;//起點區域 
                                            string end_city = string.Empty;//迄點區域                    

                                            //板數1 ~6
                                            string plate1_price = string.Empty;
                                            string plate2_price = string.Empty;
                                            string plate3_price = string.Empty;
                                            string plate4_price = string.Empty;
                                            string plate5_price = string.Empty;
                                            string plate6_price = string.Empty;
                                            string strRow = string.Empty;
                                            string strInsCol = string.Empty;
                                            string strInsVal = string.Empty;

                                            //寫入DB的資訊
                                            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                                            StringBuilder sb_temp_ins = new StringBuilder();

                                            #endregion
                                            for (int j = 1; j < row; j++)
                                            {
                                                #region 初始化
                                                tablename = "";
                                                start_city = "";
                                                end_city = "";
                                                plate1_price = "";
                                                plate2_price = "";
                                                plate3_price = "";
                                                plate4_price = "";
                                                plate5_price = "";
                                                plate6_price = "";
                                                strRow = (j + 1).ToString();
                                                #endregion




                                                Boolean IsChkOk = true;
                                                if (valueArray[j, 0] != null) start_city = valueArray[j, 0].ToString();
                                                if (String.IsNullOrEmpty(start_city))
                                                {
                                                    IsChkOk =
                                                    IsSheetOk = false;
                                                }
                                                start_city = start_city.Replace("台", "臺");

                                                if (valueArray[j, 1] != null) end_city = valueArray[j, 1].ToString();
                                                if (String.IsNullOrEmpty(end_city))
                                                {
                                                    IsChkOk =
                                                    IsSheetOk = false;
                                                }
                                                end_city = end_city.Replace("台", "臺");

                                                for (int z = 1; z <= 4; z++)
                                                {
                                                    if (valueArray[j, z * 3] != null) first_price = valueArray[j, z * 3].ToString();
                                                    if (String.IsNullOrEmpty(first_price) || !Utility.IsNumeric(first_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }

                                                    if (valueArray[j, z * 3 + 1] != null) add_price = valueArray[j, z * 3 + 1].ToString();
                                                    if (String.IsNullOrEmpty(add_price) || !Utility.IsNumeric(add_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }

                                                    if (IsSheetOk)
                                                    {
                                                        //只有「自營公版」及 「自訂運價表」能匯到「自訂運價表 tbPriceBusinessDefine」

                                                        using (SqlCommand cmdI = new SqlCommand())
                                                        {
                                                            cmdI.Parameters.Clear();
                                                            cmdI.Parameters.AddWithValue("@start_city", start_city);
                                                            cmdI.Parameters.AddWithValue("@end_city", end_city);
                                                            cmdI.Parameters.AddWithValue("@Cbmsize", z);
                                                            cmdI.Parameters.AddWithValue("@first_price", first_price);
                                                            cmdI.Parameters.AddWithValue("@add_price", add_price);
                                                            cmdI.Parameters.AddWithValue("@Enable_date", dt_temp.ToString("yyyy-MM-dd"));
                                                            cmdI.Parameters.AddWithValue("@customer_code", customer_code);
                                                            cmdI.Parameters.AddWithValue("@cuser", strUser);
                                                            cmdI.CommandText = dbAdapter.genInsertComm("tbPriceLessThan", true, cmdI);
                                                            int result = 0;
                                                            if (!int.TryParse(dbAdapter.getDataTable(cmdI).Rows[0][0].ToString(), out result)) result = 0;
                                                        }


                                                    }



                                                }
                                            }









                                            if (IsSheetOk)
                                            {
                                                //新增自訂
                                                String strSQL;


                                                //傳db成功 寫入 ttPriceBusinessDefineLog     
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    strSQL = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(12)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(10), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                                                    cmd.CommandText = strSQL;
                                                    dbAdapter.execNonQuery(cmd);
                                                }
                                            }
                                            else
                                            {
                                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                                                IsUpdateToDB = false;
                                            }
                                            lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                                        }
                                    }
                                }

                            }
                        }
                    } //零擔
                }
                else if(System.IO.Path.GetExtension(file01.FileName).ToLower().ToString() == ".xls")
                {

                    if (Less_than_truckload == "0") //棧板
                    {

                        string ttErrStr = "";
                        string start_city = "";
                        string end_city = "";
                        string class_level = "";
                        string pricing_codea = "";
                        string plate1_price = "";
                        string plate2_price = "";
                        string plate3_price = "";
                        string plate4_price = "";
                        string plate5_price = "";
                        string plate6_price = "";
                        string tablename = "";

                        string ttPath = Request.PhysicalApplicationPath + @"files\";

                        //string ttFileName = ttPath + file01.PostedFile.FileName;
                        string ttFileName = ttPath + file01.FileName;

                        string ttExtName = System.IO.Path.GetExtension(ttFileName);
                        file01.PostedFile.SaveAs(ttFileName);
                        HSSFWorkbook workbook = null;
                        HSSFSheet u_sheet = null;
                        string ttSelectSheetName = "";
                        workbook = new HSSFWorkbook(file01.FileContent);
                        try
                        {
                            for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                            {
                                u_sheet = (HSSFSheet)workbook.GetSheetAt(x);
                                ttSelectSheetName = workbook.GetSheetName(x);

                                if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                                {
                                    Boolean IsSheetOk = true;//工作表驗證
                                    lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                                    //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                                    string strRow, strInsCol, strInsVal;
                                    List<StringBuilder> sb_del_list = new List<StringBuilder>();
                                    List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                                    StringBuilder sb_temp_del = new StringBuilder();
                                    StringBuilder sb_temp_ins = new StringBuilder();

                                    HSSFRow Row_title = new HSSFRow();
                                    if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                                    for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                                    {
                                        #region 初始化

                                        HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列                              
                                        start_city = "";
                                        end_city = "";
                                        class_level = "";
                                        pricing_codea = "";
                                        plate1_price = "";
                                        plate2_price = "";
                                        plate3_price = "";
                                        plate4_price = "";
                                        plate5_price = "";
                                        plate6_price = "";
                                        strInsCol = "";
                                        strInsVal = "";
                                        strRow = (i + 1).ToString();

                                        #endregion

                                        #region 驗證&取值 IsSheetOk                                
                                        for (int j = 0; j < Row_title.Cells.Count; j++)
                                        {
                                            Boolean IsChkOk = true;
                                            switch (Row_title.GetCell(j).StringCellValue)
                                            {
                                                case "起點區域":
                                                    if (Row.GetCell(j) != null) start_city = Row.GetCell(j).ToString();
                                                    if (String.IsNullOrEmpty(start_city))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    start_city = start_city.Replace("台", "臺");
                                                    break;
                                                case "迄點區域":
                                                    if (Row.GetCell(j) != null) end_city = Row.GetCell(j).ToString();
                                                    if (String.IsNullOrEmpty(end_city))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    end_city = end_city.Replace("台", "臺");
                                                    break;
                                                case "級距":
                                                    if (Row.GetCell(j) != null) class_level = Row.GetCell(j).ToString();
                                                    if (String.IsNullOrEmpty(class_level) || !IsNumeric(class_level))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "計價模式":
                                                    if (Row.GetCell(j) != null) pricing_codea = Row.GetCell(j).ToString();
                                                    if (String.IsNullOrEmpty(pricing_codea) || !IsNumeric(pricing_codea))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數1":
                                                    if (Row.GetCell(j) != null) plate1_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate1_price) || !IsNumeric(plate1_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數2":
                                                    if (Row.GetCell(j) != null) plate2_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate2_price) || !IsNumeric(plate2_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數3":
                                                    if (Row.GetCell(j) != null) plate3_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate3_price) || !IsNumeric(plate3_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數4":
                                                    if (Row.GetCell(j) != null) plate4_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate4_price) || !IsNumeric(plate4_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數5":
                                                    if (Row.GetCell(j) != null) plate5_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate5_price) || !IsNumeric(plate5_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                case "板數6":
                                                    if (Row.GetCell(j) != null) plate6_price = Row.GetCell(j).ToString().Replace(",", "");
                                                    if (String.IsNullOrEmpty(plate6_price) || !IsNumeric(plate6_price))
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                    break;
                                                    //case "運價表":
                                                    //    if (Row.GetCell(j) != null) tablename = Row.GetCell(j).ToString();
                                                    //    if (String.IsNullOrEmpty(tablename))
                                                    //    {
                                                    //        IsChkOk =
                                                    //        IsSheetOk = false;
                                                    //    }
                                                    //    break;
                                            }

                                            if (!IsChkOk)
                                            {
                                                lbMsg.Items.Add("請確認【" + Row_title.GetCell(j).StringCellValue + "】 第" + strRow + "列是否正確!");
                                            }
                                        }

                                        #endregion

                                        #region 組刪除

                                        if (i == u_sheet.FirstRowNum + 1)
                                        {
                                            switch (tablename)
                                            {
                                                case "tbPriceSupplier":  //區配論板
                                                    sb_temp_del.Append("delete " + tablename + " where class_level = '" + class_level + "' and pricing_code  ='" + pricing_codea + "'");

                                                    break;
                                                case "tbPriceHCT":  //竹運論板
                                                    sb_temp_del.Append("delete " + tablename + " where pricing_code  ='" + pricing_codea + "'");

                                                    break;
                                                case "tbPriceBusiness":  //自營論板
                                                    sb_temp_del.Append("delete " + tablename + " where pricing_code  ='" + pricing_codea + "'");

                                                    break;
                                                case "tbPriceCSection":  //C配
                                                    sb_temp_del.Append("delete " + tablename + " where class_level = '" + class_level + "' and pricing_code  ='" + pricing_codea + "'");

                                                    break;
                                                case "tbPriceCSectionA05":  //合發專用
                                                    sb_temp_del.Append("delete " + tablename + " where  pricing_code  ='" + pricing_codea + "'");
                                                    break;
                                            }
                                            sb_del_list.Add(sb_temp_del);
                                        }
                                        #endregion

                                        #region 組新增
                                        switch (tablename)
                                        {
                                            case "tbPriceSupplier":  //區配論板
                                                strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            "N\'" + class_level + "\', " +
                                                            "N\'" + pricing_codea + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";
                                                break;
                                            case "tbPriceHCT":  //竹運論板
                                                strInsCol = "start_city,end_city,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            // "N\'" + class_level + "\', " +
                                                            "N\'" + pricing_codea + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";
                                                break;
                                            case "tbPriceBusiness":  //自營論板
                                                strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            "N\'" + class_level + "\', " +
                                                            "N\'" + pricing_codea + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";
                                                break;
                                            case "tbPriceCSection":  //C配
                                                strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = "N\'" + start_city + "\', " +
                                                            "N\'" + end_city + "\', " +
                                                            "N\'" + class_level + "\', " +
                                                            "N\'" + pricing_codea + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";
                                                break;
                                            case "tbPriceCSectionA05":  //合發專用
                                                strInsCol = "class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                                strInsVal = //"N\'" + start_city + "\', " +
                                                            //"N\'" + end_city + "\', " +
                                                            "N\'" + class_level + "\', " +
                                                            "N\'" + pricing_codea + "\', " +
                                                            "N\'" + plate1_price + "\', " +
                                                            "N\'" + plate2_price + "\', " +
                                                            "N\'" + plate3_price + "\', " +
                                                            "N\'" + plate4_price + "\', " +
                                                            "N\'" + plate5_price + "\', " +
                                                            "N\'" + plate6_price + "\'  ";
                                                break;
                                        }

                                        sb_temp_ins.Append("INSERT INTO " + tablename + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");

                                        #region 最後一筆同時也寫入「公版運價上傳記錄檔 ttPriceUpdateLog」
                                        if (i == u_sheet.LastRowNum)
                                        {
                                            int i_tableType = 0;//運價表分類 (1:竹運公版 2:區配公版 3:自營公版 4:C配公版 5:C配合發)
                                            switch (tablename)
                                            {
                                                case "tbPriceSupplier":  //區配論板
                                                    i_tableType = 2;
                                                    break;
                                                case "tbPriceHCT":  //竹運論板
                                                    i_tableType = 1;
                                                    break;
                                                case "tbPriceBusiness":  //自營論板
                                                    i_tableType = 3;
                                                    break;
                                                case "tbPriceCSection":  //C配
                                                    i_tableType = 4;
                                                    break;
                                                case "tbPriceCSectionA05":  //合發專用
                                                    i_tableType = 5;
                                                    break;
                                                default:
                                                    i_tableType = 0;
                                                    break;
                                            }
                                            string user = "";
                                            if (Session["account_code"] != null) user = Session["account_code"].ToString();

                                            strInsVal = "N\'" + i_tableType.ToString() + "\', " +
                                                        "N\'" + pricing_codea + "\', " +
                                                        "N\'" + class_level + "\', " +
                                                        "N\'" + user + "\'  ";

                                            sb_temp_ins.Append("INSERT INTO ttPriceUpdateLog(tariffs_table_type, pricing_code, class_level,  cuser) VALUES(" + strInsVal + "); ");
                                        }
                                        #endregion

                                        //每100筆組成一字串
                                        if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                        {
                                            sb_ins_list.Add(sb_temp_ins);
                                            sb_temp_ins = new StringBuilder();
                                        }
                                        #endregion
                                    }

                                    //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                                    if (IsSheetOk)
                                    {
                                        #region 執行SQL

                                        //刪除
                                        foreach (StringBuilder sb in sb_del_list)
                                        {
                                            if (sb.Length > 0)
                                            {
                                                String strSQL = sb.ToString();
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    cmd.CommandText = strSQL;
                                                    dbAdapter.execNonQuery(cmd);
                                                }
                                            }
                                        }

                                        //新增
                                        foreach (StringBuilder sb in sb_ins_list)
                                        {
                                            if (sb.Length > 0)
                                            {
                                                String strSQL = sb.ToString();
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    cmd.CommandText = strSQL;
                                                    dbAdapter.execNonQuery(cmd);
                                                }
                                            }
                                        }

                                        #endregion

                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                           string  strSQLA = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(10)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(10), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                                            cmd.CommandText = strSQLA;
                                            dbAdapter.execNonQuery(cmd);
                                        }



                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                    }
                                    else


                                    {
                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                                    }
                                    lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            ttErrStr = ex.Message;
                        }
                        finally
                        {
                            System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                        }
                        lbMsg.Items.Add("匯入完畢");
                    }
                    else //零擔
                    {
                        string ttPath = Request.PhysicalApplicationPath + @"files\";
                        string tablename = "tbPriceLessThan";
                        string ttErrStr = "";
                        string start_city = "";
                        string end_city = "";
                        string Cbmsize = "";
                        int CbmID = 0;
                        string first_price = "";
                        string add_price = "";
                        int _first_price = 0;
                        int _add_price = 0;
                        string ttFileName = ttPath + file01.FileName;
                        int chkdate = 1;
                        file01.PostedFile.SaveAs(ttFileName);
                        HSSFWorkbook workbook = null;
                        HSSFSheet u_sheet = null;
                        string ttSelectSheetName = "";
                        workbook = new HSSFWorkbook(file01.FileContent);


                        try
                        {
                            for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                            {
                                u_sheet = (HSSFSheet)workbook.GetSheetAt(x);
                                ttSelectSheetName = workbook.GetSheetName(x);

                                if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                                {
                                    Boolean IsSheetOk = true;//工作表驗證
                                    lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                                    //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                                    string strRow, strInsCol, strInsVal;
                                    List<StringBuilder> sb_del_list = new List<StringBuilder>();
                                    List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                                    StringBuilder sb_temp_del = new StringBuilder();
                                    StringBuilder sb_temp_ins = new StringBuilder();

                                    HSSFRow Row_title = new HSSFRow();
                                    if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                                    string a = "";

                                    for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                                    {
                                        #region 初始化

                                        HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列   
                                        if (Row.GetCell(0) == null || Row.GetCell(0).ToString() == "")
                                        {
                                            break;

                                        }
                                        start_city = "";
                                        end_city = "";
                                        Cbmsize = "";
                                        CbmID = 0;
                                        first_price = "";
                                        add_price = "";
                                        _first_price = 0;
                                        _add_price = 0;



                                        strInsCol = "";
                                        strInsVal = "";
                                        strRow = (i + 1).ToString();

                                        #endregion

                                        #region 驗證&取值 IsSheetOk                                

                                        Boolean IsChkOk = true;



                                        if (Row.GetCell(0) != null) start_city = Row.GetCell(0).ToString();
                                        if (String.IsNullOrEmpty(start_city))
                                        {
                                            IsChkOk = false;
                                            IsSheetOk = false;

                                        }
                                        start_city = start_city.Replace("台", "臺");


                                        if (Row.GetCell(1) != null) end_city = Row.GetCell(1).ToString();
                                        if (String.IsNullOrEmpty(end_city))
                                        {
                                            IsChkOk = false;
                                            IsSheetOk = false;

                                        }
                                        end_city = end_city.Replace("台", "臺");


                                        if (Row.GetCell(2) != null) Cbmsize = Row.GetCell(2).ToString();
                                        if (String.IsNullOrEmpty(Cbmsize))
                                        {
                                            IsChkOk = false;
                                            IsSheetOk = false;

                                        }

                                        for (int z = 1; z <= 4; z++)
                                        {
                                            if (Row.GetCell(3 * z) != null) first_price = Row.GetCell(3 * z).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(first_price) || !IsNumeric(first_price))
                                            {
                                                IsChkOk = false;
                                                IsSheetOk = false;

                                            }
                                            else
                                            {
                                                int.TryParse(first_price, out _first_price);
                                            }

                                            if (Row.GetCell(3 * z + 1) != null) add_price = Row.GetCell(3 * z + 1).ToString().Trim().Replace(",", "");
                                            if (String.IsNullOrEmpty(add_price) || !IsNumeric(add_price))
                                            {

                                                IsChkOk = false;
                                                IsSheetOk = false;

                                            }
                                            else
                                            {
                                                int.TryParse(add_price, out _add_price);
                                            }



                                            //每100筆組成一字串

                                            if (Row_title.Cells.Count != 14)
                                            {
                                                IsSheetOk = false;
                                                IsChkOk = false;
                                            }


                                            if (IsSheetOk)
                                            {

                                                SqlCommand cmda = new SqlCommand();
                                                DataTable dta = new DataTable();


                                                cmda.Parameters.Clear();
                                                cmda.Parameters.AddWithValue("@start_city", start_city);
                                                cmda.Parameters.AddWithValue("@end_city", end_city);
                                                cmda.Parameters.AddWithValue("@Cbmsize", z);
                                                cmda.Parameters.AddWithValue("@first_price", first_price);
                                                cmda.Parameters.AddWithValue("@add_price", add_price);
                                                cmda.Parameters.AddWithValue("@Enable_date", dt_temp.ToString("yyyy-MM-dd"));
                                                cmda.Parameters.AddWithValue("@customer_code", customer_code);
                                                cmda.Parameters.AddWithValue("@cuser", strUser);
                                                cmda.CommandText = dbAdapter.genInsertComm("tbPriceLessThan", true, cmda);
                                              
                                                int result = 0;
                                                if (!int.TryParse(dbAdapter.getDataTable(cmda).Rows[0][0].ToString(), out result)) result = 0;


                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                   string strSQLA = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(12)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + tariffs_type.ToString() + @"' 
                                       SET @cuser= '" + strUser + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(10), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                                                    cmd.CommandText = strSQLA;
                                                    dbAdapter.execNonQuery(cmd);
                                                }



                                            }

                                        }

                                        #endregion

                                        #region 組新增


                                        #endregion
                                    }

                                    //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳




                                    if (IsSheetOk)
                                    {
                                        #region 執行SQL

                                        //刪除
                                        //foreach (StringBuilder sb in sb_del_list)
                                        //{
                                        //    if (sb.Length > 0)
                                        //    {
                                        //        String strSQL = sb.ToString();
                                        //        using (SqlCommand cmd = new SqlCommand())
                                        //        {
                                        //            cmd.CommandText = strSQL;
                                        //            dbAdapter.execNonQuery(cmd);
                                        //        }
                                        //    }
                                        //}

                                        //新增

                                        #endregion
                                        if (chkdate == 0)
                                        {
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】啟用日期異常!");
                                        }
                                        else
                                        {

                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                    }
                                    else
                                    {
                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                                    }
                                    lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                                }

                            }
                        }
                        catch (Exception ex)
                        {
                            ttErrStr = ex.Message;
                        }
                        finally
                        {
                            System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                        }
                        lbMsg.Items.Add("匯入完畢");
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "setTimeout('parent.$.fancybox.close()', 50000); parent.location.reload(true);", true);
                    }
                }
               



                    



            }
        }
        catch (Exception ex)
        {
            IsUpdateToDB = false;
        }
        return IsUpdateToDB;
    }



    /// <summary>ExcelToD </summary>
    /// <param name="this_file">Excel file</param>
    /// <param name="customer_code">客戶代碼</param>
    /// <param name="pricing_code">計價模式</param>
    /// <param name="active_date">生效日(yyyy/MM/dd)</param>
    /// <returns></returns>
    public Boolean ExcelToDB_old(FileUpload this_file, string customer_code, string pricing_code, string active_date)
    {



        Boolean IsUpdateToDB = true;
        DateTime dt_temp;
        try
        {
            if (this_file.HasFile
                && !string.IsNullOrEmpty(customer_code)
                && !string.IsNullOrEmpty(pricing_code)
                && DateTime.TryParse(active_date, out dt_temp))
            {
                lbMsg.Items.Clear();
                HSSFWorkbook workbook = new HSSFWorkbook(this_file.FileContent);
                //每個頁籤
                for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                {
                    HSSFSheet u_sheet = (HSSFSheet)workbook.GetSheetAt(x);
                    string ttSelectSheetName = workbook.GetSheetName(x);

                    if (!new string[] { "Print_Titles", "Print_Area" }.Contains(ttSelectSheetName))
                    {
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                        #region 定義 foreach sheet

                        Boolean IsSheetOk = true;//工作表驗證
                        string tablename = string.Empty;
                        string start_city = string.Empty;//起點區域 
                        string end_city = string.Empty;//迄點區域                    

                        //板數1 ~6
                        string plate1_price = string.Empty;
                        string plate2_price = string.Empty;
                        string plate3_price = string.Empty;
                        string plate4_price = string.Empty;
                        string plate5_price = string.Empty;
                        string plate6_price = string.Empty;
                        string strRow = string.Empty;
                        string strInsCol = string.Empty;
                        string strInsVal = string.Empty;
                        HSSFRow Row_title = new HSSFRow();//標題列

                        //寫入DB的資訊
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        #endregion

                        //Get Title
                        if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                        //每一列                       
                        for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)
                        {

                            HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);

                            #region 初始化
                            tablename = "";
                            start_city = "";
                            end_city = "";
                            plate1_price = "";
                            plate2_price = "";
                            plate3_price = "";
                            plate4_price = "";
                            plate5_price = "";
                            plate6_price = "";
                            strRow = (i + 1).ToString();
                            #endregion

                            #region 驗證&取值 IsSheetOk                                
                            for (int j = 0; j < Row_title.Cells.Count; j++)
                            {
                                Boolean IsChkOk = true;
                                switch (Row_title.GetCell(j).StringCellValue)
                                {
                                    case "起點區域":
                                        if (Row.GetCell(j) != null) start_city = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(start_city))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "迄點區域":
                                        if (Row.GetCell(j) != null) end_city = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(end_city))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;

                                    //case "計價模式":
                                    //    if (Row.GetCell(j) != null) pricing_code = Row.GetCell(j).ToString();
                                    //    if (String.IsNullOrEmpty(pricing_code) || !IsNumeric(pricing_code))
                                    //    {
                                    //        IsChkOk =
                                    //        IsSheetOk = false;
                                    //    }                                        
                                    //    break;
                                    case "板數1":
                                        if (Row.GetCell(j) != null) plate1_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate1_price) || !Utility.IsNumeric(plate1_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "板數2":
                                        if (Row.GetCell(j) != null) plate2_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate2_price) || !Utility.IsNumeric(plate2_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "板數3":
                                        if (Row.GetCell(j) != null) plate3_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate3_price) || !Utility.IsNumeric(plate3_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "板數4":
                                        if (Row.GetCell(j) != null) plate4_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate4_price) || !Utility.IsNumeric(plate4_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "板數5":
                                        if (Row.GetCell(j) != null) plate5_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate5_price) || !Utility.IsNumeric(plate5_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "板數6":
                                        if (Row.GetCell(j) != null) plate6_price = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(plate6_price) || !Utility.IsNumeric(plate6_price))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                    case "運價表":
                                        if (Row.GetCell(j) != null) tablename = Row.GetCell(j).ToString();
                                        if (String.IsNullOrEmpty(tablename))
                                        {
                                            IsChkOk =
                                            IsSheetOk = false;
                                        }
                                        break;
                                }

                                if (!IsChkOk)
                                {
                                    lbMsg.Items.Add("請確認【" + Row_title.GetCell(j).StringCellValue + "】 第" + strRow + "列是否正確!");
                                }
                            }

                            #endregion

                            #region 組新增

                            //只有「自營公版」及 「自訂運價表」能匯到「自訂運價表 tbPriceBusinessDefine」
                            if (new string[] { "tbPriceBusiness", "tbPriceBusinessDefine" }.Contains(tablename))
                            {
                                strInsCol = "start_city,end_city,customer_code, active_date,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                strInsVal = "N\'" + start_city + "\', " +
                                            "N\'" + end_city + "\', " +
                                            "N\'" + customer_code + "\', " +
                                            "N\'" + active_date + "\', " +
                                            "N\'" + pricing_code + "\', " +
                                            "N\'" + plate1_price + "\', " +
                                            "N\'" + plate2_price + "\', " +
                                            "N\'" + plate3_price + "\', " +
                                            "N\'" + plate4_price + "\', " +
                                            "N\'" + plate5_price + "\', " +
                                            "N\'" + plate6_price + "\'  ";

                                sb_temp_ins.Append("INSERT INTO " + "tbPriceBusinessDefine" + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");
                            }

                            //每100筆組成一字串
                            if (i % 100 == 0 || i == u_sheet.LastRowNum)
                            {
                                sb_ins_list.Add(sb_temp_ins);
                                sb_temp_ins = new StringBuilder();
                            }

                            #endregion
                        }
                        if (IsSheetOk)
                        {
                            //新增
                            String strSQL;
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                }
                            }

                            //傳db成功 寫入 ttPriceBusinessDefineLog                           
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                strSQL = @"DECLARE @log_id INT 
                                              ,@tariffs_effect_date DATE
	                                          ,@customer_code nvarchar(12)
	                                          ,@tariffs_type char(1)
	                                          ,@cuser nvarchar(20)

                                       SET @tariffs_effect_date = '" + dt_temp.ToString("yyyy/MM/dd") + @"' 
                                       SET @customer_code ='" + customer_code + @"' 
                                       SET @tariffs_type = '" + rad_ShipType.SelectedValue.ToString() + @"' 

                                       SELECT　@log_id = log_id FROM　ttPriceBusinessDefineLog  With(Nolock) 
                                           WHERE customer_code = @customer_code AND CONVERT(CHAR(10), tariffs_effect_date, 111) =  CONVERT(CHAR(10), @tariffs_effect_date, 111)
   
                                       SELECT　@log_id = ISNULL(@log_id,'0') 

                                       IF @log_id = 0
                                           BEGIN    
	                                           INSERT INTO ttPriceBusinessDefineLog (customer_code, tariffs_effect_date, tariffs_type, cuser) 
	                                               VALUES(@customer_code,CONVERT(CHAR(12), @tariffs_effect_date, 111),@tariffs_type,@cuser);
		                                           SELECT SCOPE_IDENTITY();
                                           END";

                                cmd.CommandText = strSQL;
                                dbAdapter.execNonQuery(cmd);
                            }
                        }
                        else
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");
                    }
                }



                lbMsg.Items.Add("匯入完畢");
            }
            else
            {
                IsUpdateToDB = false;
            }
        }
        catch (Exception ex)
        {
            IsUpdateToDB = false;
        }
        return IsUpdateToDB;
    }

    public void SetMinShipFeeForJs(string customer_code)
    {
        if (customer_code.Length > 0)
        {
            string str_MinDate = GetValueStrFromSQL("Exec usp_GetTodayPriceBusDefLog @customer_code = '" + customer_code.ToString() + "',@is_use ='0';");

            DateTime dt_tmp = new DateTime();

            if (str_MinDate.Length == 0 || !DateTime.TryParse(str_MinDate, out dt_tmp))
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "setDate", "<script>$('.datepicker').datepicker({dateFormat: 'yy/mm/dd', minDate: '0'});</script>", false);  temp 
            }
            else
            {
                string DefaultDate = dt_tmp.AddDays(1).ToString("yyyy/MM/dd");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "setDate", "<script>$('.datepicker').datepicker({dateFormat: 'yy/mm/dd', minDate: '" + DefaultDate + "'});</script>", false);
            }
        }
    }
    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }
    protected String GetValueStrFromSQL(string strSQL)
    {
        String result = "";
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0][0].ToString();
                    }
                }
            }

        }

        return result;
    }

    


    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }
}
