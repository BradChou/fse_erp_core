﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

public partial class money_1_3 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            String yy = DateTime.Now.Year.ToString();
            String mm = DateTime.Now.Month.ToString();
            String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
            dates.Text = DateTime.Today.AddMonths(-1).ToString("yyyy/MM/") + "01"; //yy + "/" + mm + "/01";
            datee.Text = yy + "/" + mm + "/" + days;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            Master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();
            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and supplier_code = @supplier_code";
            }
            cmd1.CommandText = string.Format(@"select supplier_id, supplier_code,supplier_code + '-' +supplier_name as showname  from tbSuppliers where active_flag= 1 and ISNULL(supplier_code ,'') <> '' {0} order by supplier_code", wherestr);
            Suppliers.DataSource = dbAdapter.getDataTable(cmd1);
            Suppliers.DataValueField = "supplier_code";
            Suppliers.DataTextField = "showname";
            Suppliers.DataBind();
            //if (Request.QueryString["Suppliers"] != null)
            //{
            //    Suppliers.SelectedValue = Request.QueryString["Suppliers"];
            //}
            Suppliers_SelectedIndexChanged(null, null);
            //Suppliers.Items.Insert(0, new ListItem("全部", ""));
            #endregion
            #region comment
            //if (Request.QueryString["second_code"] != null)
            //{
            //    second_code.Text = Request.QueryString["second_code"];
            //}

            //if (Request.QueryString["dates"] != null)
            //{
            //    dates.Text = Request.QueryString["dates"];
            //}
            //if (Request.QueryString["datee"] != null)
            //{
            //    datee.Text = Request.QueryString["datee"];
            //}
            //lbdate.Text = dates.Text + "~" + datee.Text;

            //if (Request.QueryString["close_type"] != null)
            //{
            //    dlclose_type.SelectedValue = Request.QueryString["close_type"];
            //}
            //dlclose_type_SelectedIndexChanged(null, null);
            //if (Request.QueryString["closeday"] != null)
            //{
            //    dlCloseDay.SelectedValue = Request.QueryString["closeday"];
            //}

            //if (Request.QueryString["close_date"] != null)
            //{
            //    close_date.SelectedValue = Request.QueryString["close_date"];
            //}


            //   //0:天眼公司 1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營
            //   if (manager_type == "3" || manager_type == "5")
            //   {
            //       readdata_5();
            //       Panel1.Visible = false;
            //       Panel2.Visible = true;
            //   }
            //   else
            //   {
            //       readdata();
            //       Panel1.Visible = true;
            //       Panel2.Visible = false;
            //   }
            //   string sScript = string.Empty;
            //   switch (dlclose_type.SelectedValue)
            //   {
            //       case "1":
            //           sScript =  " $('._close').show(); " +
            //                      " $('._daterange').html('請款期間');  ";
            //           break;

            //       case "2":
            //           sScript = " $('._close').hide(); " +
            //                     " $('._daterange').html('發送期間'); ";
            //           break;
            //   }

            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>"+ sScript  + "</script>", false); 
            #endregion
        }

    }

    private void readdata()
    {
        lbSuppliers.Text = second_code.SelectedItem != null ? second_code.SelectedItem.Text : "";

        string[] pricing_type;
        if (dl_pricing_type.SelectedValue == "*")
        {
            pricing_type = new[] { "01", "02", "03", "04", "05" };
        }
        else if (dl_pricing_type.SelectedValue == "")
        {
            pricing_type = new[] { "01", "02", "03", "04" };
        }
        else
        {
            pricing_type = new[] { "05" };
        }
        Panel_01.Visible = false;
        Panel_02.Visible = false;
        Panel_03.Visible = false;
        Panel_04.Visible = false;
        Panel_05.Visible = false;

        for (int i = 0; i <= pricing_type.Length - 1; i++)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetAccountsReceivablebyJUNFU";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pricing_type", pricing_type[i]);  //計價模式
                string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();

                if(second_code.SelectedValue == "*")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }
                
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    Utility.AccountsReceivableInfo _info;
                    if (second_code.SelectedValue == "*")
                    {
                        _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, customer_code, pricing_type[i], "", null, Suppliers.SelectedValue);
                    }
                    else
                    {
                        _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, customer_code, pricing_type[i], "", null, null);
                    }
                    
                    switch (pricing_type[i])
                    {
                        case "01":
                            if (dt != null && dt.Rows.Count > 0 && _info != null)
                            {
                                subtotal_01.Text = _info.subtotal.ToString("N0");
                                tax_01.Text = _info.tax.ToString("N0");
                                total_01.Text = _info.total.ToString("N0");
                                DataRow row = dt.NewRow();
                                row["到著站"] = "總計";
                                row["板數"] = _info.plates;
                                row["貨件費用"] = _info.subtotal;
                                dt.Rows.Add(row);
                                Panel_01.Visible = true;
                            }

                            New_List_01.DataSource = dt;
                            New_List_01.DataBind();
                            break;
                        case "02":
                            if (dt != null && dt.Rows.Count > 0 && _info != null)
                            {
                                subtotal_02.Text = _info.subtotal.ToString("N0");
                                tax_02.Text = _info.tax.ToString("N0");
                                total_02.Text = _info.total.ToString("N0");
                                DataRow row = dt.NewRow();
                                row["到著站"] = "總計";
                                row["件數"] = _info.pieces;
                                row["貨件費用"] = _info.subtotal;
                                dt.Rows.Add(row);
                                Panel_02.Visible = true;
                            }
                            New_List_02.DataSource = dt;
                            New_List_02.DataBind();
                            break;
                        case "03":
                            if (dt != null && dt.Rows.Count > 0 && _info != null)
                            {
                                subtotal_03.Text = _info.subtotal.ToString("N0");
                                tax_03.Text = _info.tax.ToString("N0");
                                total_03.Text = _info.total.ToString("N0");
                                DataRow row = dt.NewRow();
                                row["到著站"] = "總計";
                                row["才數"] = _info.pieces;
                                row["貨件費用"] = _info.subtotal;
                                dt.Rows.Add(row);
                                Panel_03.Visible = true;
                            }

                            New_List_03.DataSource = dt;
                            New_List_03.DataBind();
                            break;
                        case "04":
                            if (dt != null && dt.Rows.Count > 0 && _info != null)
                            {
                                subtotal_04.Text = _info.subtotal.ToString("N0");
                                tax_04.Text = _info.tax.ToString("N0");
                                total_04.Text = _info.total.ToString("N0");
                                DataRow row = dt.NewRow();
                                row["到著站"] = "總計";
                                row["小板數"] = _info.pieces;
                                row["貨件費用"] = _info.subtotal;
                                dt.Rows.Add(row);
                                Panel_04.Visible = true;
                            }

                            New_List_04.DataSource = dt;
                            New_List_04.DataBind();
                            break;
                        case "05":
                            if (dt != null && dt.Rows.Count > 0 && _info != null)
                            {
                                subtotal_05.Text = _info.subtotal.ToString("N0");
                                tax_05.Text = _info.tax.ToString("N0");
                                total_05.Text = _info.total.ToString("N0");
                                DataRow row = dt.NewRow();
                                row["到著站"] = "總.計";
                                row["板數"] = _info.plates;
                                row["貨件費用"] = _info.subtotal;
                                dt.Rows.Add(row);
                                Panel_05.Visible = true;
                            }

                            New_List_05.DataSource = dt;
                            New_List_05.DataBind();
                            break;
                    }
                }
            }
        }


        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsReceivable";
                cmd.CommandType = CommandType.StoredProcedure;
                if (dl_pricing_type.SelectedValue == "05")
                {
                    cmd.Parameters.AddWithValue("@pricing_type", dl_pricing_type.SelectedValue);
                }

                if (second_code.SelectedValue == "*")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue + second_code.Text.Trim());  //客代
                }

                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        New_List.DataSource = dt;
                        New_List.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

    }

    private void readdata_5()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "usp_GetAccountsPayable";
            cmd.CommandType = CommandType.StoredProcedure;
            string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
            cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable_C(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null, "");
                if (dt != null && dt.Rows.Count > 0 && _info != null)
                {
                    DataRow row = dt.NewRow();
                    row["到著站"] = "總計";
                    row["板數"] = _info.plates;
                    row["件數"] = _info.pieces;
                    row["cscetion_fee"] = _info.subtotal;
                    row["remote_fee"] = _info.remote_fee;
                    dt.Rows.Add(row);
                }


                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                if (_info != null)
                {
                    Label1.Text = _info.subtotal.ToString("N0");
                    Label2.Text = _info.tax.ToString("N0");
                    Label3.Text = _info.total.ToString("N0");
                }


            }
        }


        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetSumAccountsPayable";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        Repeater2.DataSource = dt;
                        Repeater2.DataBind();
                    }

                }
            }
        }
        catch (Exception ex)
        {

            string strErr = string.Empty;
            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        //for (int i = Suppliers.SelectedIndex; i <= Suppliers.Items.Count - 1; i++)
        //{
        //    if (Suppliers.Items[i].Value != "")
        //    {
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.CommandText = "usp_GetAccountsPayable";
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@customer_code", Suppliers.Items[i].Value);  //客代
        //            cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
        //            cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
        //            cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
        //            using (DataTable dt = dbAdapter.getDataTable(cmd))
        //            {
        //                Utility.AccountsReceivableInfo _info = Utility.GetSumAccountsPayable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null);
        //                New_List_01.DataSource = dt;
        //                New_List_01.DataBind();

        //                if (_info != null)
        //                {
        //                    subtotal_01.Text = _info.subtotal.ToString("N0");
        //                    tax_01.Text = _info.tax.ToString("N0");
        //                    total_01.Text = _info.total.ToString("N0");
        //                }


        //            }
        //        }


        //        try
        //        {
        //            using (SqlCommand cmd = new SqlCommand())
        //            {
        //                cmd.CommandText = "usp_GetSumAccountsPayable";
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@customer_code", Suppliers.Text);  //客代
        //                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
        //                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
        //                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
        //                using (DataTable dt = dbAdapter.getDataTable(cmd))
        //                {
        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        New_List.DataSource = dt;
        //                        New_List.DataBind();
        //                    }

        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {

        //            string strErr = string.Empty;
        //            strErr = "usp_GetSumAccountsReceivable" + ": " + ex.ToString();

        //            PublicFunction _fun = new PublicFunction();
        //            _fun.Log(strErr, "S");
        //        }


        //        if (Suppliers.Items[i].Value == Suppliers.SelectedValue)
        //        {
        //            break;
        //        }
        //    }
        //}

    }
    protected void dlclose_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 
        if (dlclose_type.SelectedValue == "1")
        {
            if (Suppliers.SelectedValue != "" && second_code.SelectedValue != "")
            {
                string wherestr = "";
                SqlCommand cmd2 = new SqlCommand();
                string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
                
                if(second_code.SelectedValue == "*")
                {
                    cmd2.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    wherestr += " and supplier_code like @supplier_code+'%' ";
                }
                else if (customer_code != "")
                {
                    cmd2.Parameters.AddWithValue("@customer_code", customer_code);
                    wherestr += " and customer_code like @customer_code+'%' ";
                }

                //if (dl_pricing_type.SelectedValue == "05")
                //{
                //    cmd2.Parameters.AddWithValue("@pricing_type", dl_pricing_type.SelectedValue);
                //    wherestr += " and pricing_type = @pricing_type ";
                //}
                //else
                //{
                //    cmd2.Parameters.AddWithValue("@pricing_type", customer_code);
                //    wherestr += " and pricing_type <> '05' ";
                //}
                cmd2.Parameters.AddWithValue("@dates", dates.Text + " 00:00:00.000");  //日期起
                cmd2.Parameters.AddWithValue("@datee", datee.Text + " 23:59:59.997");  //日期迄
                cmd2.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from tcDeliveryRequests with(nolock)   
                                                   where 0=0 AND cancel_date IS NULL and checkout_close_date  >= CONVERT (DATETIME, @dates, 102) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 102)  
                                                   {0} order by checkout_close_date", wherestr);
                dlCloseDay.DataSource = dbAdapter.getDataTable(cmd2);
                dlCloseDay.DataValueField = "checkout_close_date";
                dlCloseDay.DataTextField = "checkout_close_date";
                dlCloseDay.DataBind();
                dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));
                dlCloseDay.SelectedIndex = dlCloseDay.Items.Count - 1;
            }
            else
            {
                dlCloseDay.Items.Clear();
                dlCloseDay.Items.Insert(0, new ListItem("請選擇", ""));
            }
        }
        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
        #endregion
    }

    protected void btQry_Click(object sender, EventArgs e)
    {
        //paramlocation();
        //0:天眼公司 1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營
        if (manager_type == "3" || manager_type == "5")
        {
            readdata_5();
            Panel1.Visible = false;
            Panel2.Visible = true;
        }
        else
        {
            readdata();
            Panel1.Visible = true;
            Panel2.Visible = false;
        }
    }

    private void paramlocation()
    {

        string querystring = "";
        if (dates.Text.ToString() != "")
        {
            querystring += "&dates=" + dates.Text.ToString();
        }
        if (datee.Text.ToString() != "")
        {
            querystring += "&datee=" + datee.Text.ToString();
        }

        if (Suppliers.SelectedValue.ToString() != "")
        {
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        }

        if (second_code.Text != "")
        {
            querystring += "&second_code=" + second_code.Text;
        }

        querystring += "&close_type=" + dlclose_type.SelectedValue.ToString();

        if (dlCloseDay.SelectedValue != "")
        {
            querystring += "&closeday=" + dlCloseDay.SelectedValue.ToString();
        }

        if (close_date.SelectedValue != "")
        {
            querystring += "&close_date=" + close_date.SelectedValue.ToString();
        }

        Response.Redirect(ResolveUrl("~/money_1_3.aspx?search=yes" + querystring));
    }

    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    //0: 系統管理者  1:峻富總公司(管理者)、2:峻富一般員工、3:峻富自營、4:區配商、5:區配商自營
        //    second_code.DataSource = Utility.getCustomerDT(Suppliers.SelectedValue, (manager_type == "3" || manager_type == "5") ? Master_code : "");
        //    second_code.DataValueField = "supplier_id";
        //    second_code.DataTextField = "showname";
        //    second_code.DataBind();
        //}

        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {
            if (Suppliers.Text != "")
            {
                cmd.Parameters.AddWithValue("@Suppliers", Suppliers.SelectedValue);
                wherestr = "  and supplier_code=@Suppliers";
            }

            if (close_date.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@checkout_date", close_date.SelectedValue);
                wherestr += "  and checkout_date=@checkout_date";
            }

            if (Master_code != "")
            {
                cmd.Parameters.AddWithValue("@Master_code", (manager_type == "3" || manager_type == "5") ? Master_code : "");
                wherestr += "  and customer_code like ''+ @Master_code + '%'";
            }

            //cmd.CommandText = string.Format(@"WITH cus AS
            //                                (
            //                                   SELECT *,
            //                                         ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
            //                                   FROM tbCustomers where 1=1 and stop_shipping_code = '0' and supplier_id <> '0000' {0}
            //                                )
            //                                SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname
            //                                FROM cus
            //                                WHERE rn = 1 ", wherestr);
            cmd.CommandText = string.Format(@"WITH cus AS
                                                (
                                                    SELECT *,
                                                            ROW_NUMBER() OVER (PARTITION BY supplier_id,second_id ORDER BY supplier_id DESC,pricing_code) AS rn
                                                    FROM tbCustomers where stop_shipping_code = '0' {0}
                                                )
                                                SELECT supplier_id, master_code, supplier_id + second_id 'branch', customer_name , supplier_id + second_id + '-' +  customer_shortname  as showname
                                                FROM cus
                                                WHERE rn = 1 ", wherestr);
            second_code.DataSource = dbAdapter.getDataTable(cmd);
            second_code.DataValueField = "branch";
            second_code.DataTextField = "showname";
            second_code.DataBind();

            second_code.Items.Insert(0, new ListItem("全選", "*"));
        }
        #endregion

        second_code_SelectedIndexChanged(null, null);
        if (UpdatePanel1.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel1.Update();
        }
    }

    protected void btClose_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        DateTime tmpdt;
        if (Suppliers.SelectedValue == "" || second_code.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇客戶代號');</script>", false);
            return;
        }
        
        if (!DateTime.TryParse(date_end.Text, out tmpdt))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請指定結帳截止日');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "acc_post1";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue + second_code.SelectedValue);   //客代
                cmd.Parameters.AddWithValue("@close_type", "2");                                                      //結帳程序類別(1:自動 2:手動)
                cmd.Parameters.AddWithValue("@clode_edate", tmpdt);                                                   //結帳截止日
                //dbAdapter.execNonQuery(cmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            strErr = "acc_post1" + ": " + ex.ToString();

            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            dlclose_type_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');$('#handclose').modal('hide');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }

        return;
    }

    protected void btClose_Click_New(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        DateTime tmpdt;
        if (Suppliers.SelectedValue == "" || second_code.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇客戶代號');</script>", false);
            return;
        }

        if (!DateTime.TryParse(date_end.Text, out tmpdt))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請指定結帳截止日');</script>", false);
            return;
        }

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "update tcDeliveryRequests set checkout_close_date = DateADD(minute, 45,@clode_edate) where checkout_close_date is null and customer_code like @customer_code + '%' and Less_than_truckload = 0 and check_number != '' and print_date between DATEADD(month,-2,GETDATE()) and @clode_edate";
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue + second_code.SelectedValue);   //客代
                cmd.Parameters.AddWithValue("@close_type", "2");                                                      //結帳程序類別(1:自動 2:手動)
                cmd.Parameters.AddWithValue("@clode_edate", tmpdt);                                                   //結帳截止日
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (Convert.ToBoolean(dt.Rows[0]["result"].ToString()) == false)
                        {
                            strErr = dt.Rows[0]["ErrMsg"].ToString();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            strErr = "系統忙碌，請稍後再試";
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }

        if (strErr == "")
        {
            dlclose_type_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳完成');$('#handclose').modal('hide');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('結帳失敗:" + strErr + "');</script>", false);
        }

        return;
    }


    [WebMethod]
    public static string GetSecondDDLHtml(string supplier_code)
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@Suppliers", supplier_code);
                wherestr = " and supplier_code=@Suppliers";
            }

            //cmd.CommandText = string.Format(@"WITH cus AS
            //                                (
            //                                   SELECT *,
            //                                         ROW_NUMBER() OVER (PARTITION BY supplier_id ORDER BY supplier_id DESC) AS rn
            //                                   FROM tbCustomers where 1=1 {0}
            //                                )
            //                                SELECT supplier_id, master_code, customer_name , supplier_id + '-' +  customer_name as showname
            //                                FROM cus
            //                                WHERE rn = 1 ", wherestr);
            cmd.CommandText = string.Format(@"WITH cus AS
                                                (
                                                    SELECT *,
                                                            ROW_NUMBER() OVER (PARTITION BY supplier_id,second_id ORDER BY supplier_id DESC,pricing_code) AS rn
                                                    FROM tbCustomers where 1=1 and stop_shipping_code = '0' and supplier_id <> '0000'  {0}
                                                )
                                                SELECT supplier_id, master_code, supplier_id + second_id 'branch', customer_name , supplier_id + second_id + '-' +  customer_shortname  as showname
                                                FROM cus
                                                WHERE rn = 1 ", wherestr);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                //sb_html.Append(@"<option  value=''>請選擇</option>");

                foreach (DataRow row in dt.Rows)
                {
                    sb_html.Append(@"<option value='" + row["branch"].ToString() + "'>" + row["showname"].ToString() + "</option>");
                }

            }
        }

        return sb_html.ToString();
    }


    [WebMethod]
    public static string GetCloseDayDDLHtml(string dates, string datee, string supplier_code, string second_code)
    {
        StringBuilder sb_html = new StringBuilder();

        string wherestr = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            if (supplier_code != "")
            {
                string customer_code = second_code != null ? supplier_code + second_code : supplier_code;
                if (customer_code != "")
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);
                    wherestr = " and customer_code like  @customer_code+'%' ";
                }
                cmd.Parameters.AddWithValue("@dates", dates + " 00:00:00.000");  //日期起
                cmd.Parameters.AddWithValue("@datee", datee + " 23:59:59.997");  //日期迄
                cmd.CommandText = string.Format(@"select distinct CONVERT(varchar(100),checkout_close_date, 111) as checkout_close_date from tcDeliveryRequests with(nolock)   
                                                   where 0=0 and checkout_close_date  >= CONVERT (DATETIME, @dates, 121) AND checkout_close_date  <= CONVERT (DATETIME, @datee, 121)  
                                                   {0} order by checkout_close_date", wherestr);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {

                    //sb_html.Append(@"<option  value=''>請選擇</option>");

                    foreach (DataRow row in dt.Rows)
                    {
                        sb_html.Append(@"<option value='" + row["checkout_close_date"].ToString() + "'>" + row["checkout_close_date"].ToString() + "</option>");
                    }

                }
            }

        }

        return sb_html.ToString();
    }



    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        Utility.AccountsReceivableInfo _info = null;
        Utility.AccountsReceivableInfo _info_01 = new Utility.AccountsReceivableInfo();
        Utility.AccountsReceivableInfo _info_05 = new Utility.AccountsReceivableInfo();
        string Title = Suppliers.SelectedItem.Text;
        string customer_name = "";
        string FeeColName = "";
        string uniform_numbers = "";
        string telephone = "";
        string shipments_principal = "";
        string shipments_city = "";
        string shipments_area = "";
        string shipments_road = "";
        string shipments_email = "";
        string billing_special_needs = "";
        int ttRow = 0;

        if (manager_type == "4")   //區配看到的應收為C段運費
        {
            Title = "應收帳款明細表";
            FeeColName = "cscetion_fee";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetAccountsPayable";
                cmd.CommandType = CommandType.StoredProcedure;
                string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
                cmd.Parameters.AddWithValue("@pricing_type", "'01','02','03','04','05'");
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                dt = dbAdapter.getDataTable(cmd);
                _info = Utility.GetSumAccountsPayable_C(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, Suppliers.Text, null, "");
            }
        }
        else
        {
            Title = "峻富物流股份有限公司";
            FeeColName = "貨件費用";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetAccountsReceivablebyJUNFU";
                cmd.CommandType = CommandType.StoredProcedure;
                string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
                cmd.Parameters.AddWithValue("@pricing_type", "'01','02','03','04','05'");

                if (second_code.SelectedValue == "*")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }

                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日            
                dt = dbAdapter.getDataTable(cmd);
                _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, customer_code, "'01','02','03','04','05'", "");
                using (SqlCommand cmd_01 = new SqlCommand())
                {
                    cmd_01.CommandText = "usp_GetSumAccountsReceivable";
                    cmd_01.CommandType = CommandType.StoredProcedure;
                    cmd_01.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    cmd_01.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                    cmd_01.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                    cmd_01.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                    cmd_01.Parameters.AddWithValue("@groupby", "");

                    using (DataTable dt_01 = dbAdapter.getDataTable(cmd_01))
                    {
                        if (dt_01.Rows.Count > 0)
                        {
                            _info_01.subtotal = dt_01.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["subtotal"]) : 0;
                            _info_01.tax = dt_01.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["tax"]) : 0;
                            _info_01.total = dt_01.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["total"]) : 0;
                            _info_01.plates = dt_01.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["plates"]) : 0;
                            _info_01.pieces = dt_01.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["pieces"]) : 0;
                            _info_01.cbm = dt_01.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["cbm"]) : 0;
                            _info_01.special_fee = dt_01.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt_01.Rows[0]["special_fee"]) : 0;
                        }

                    }
                }

                using (SqlCommand cmd_05 = new SqlCommand())
                {
                    cmd_05.CommandText = "usp_GetSumAccountsReceivable";
                    cmd_05.CommandType = CommandType.StoredProcedure;
                    cmd_05.Parameters.AddWithValue("@pricing_type", "05");  //計價模式
                    cmd_05.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    cmd_05.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                    cmd_05.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                    cmd_05.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                    cmd_05.Parameters.AddWithValue("@groupby", "");

                    using (DataTable dt_05 = dbAdapter.getDataTable(cmd_05))
                    {
                        if (dt_05.Rows.Count > 0)
                        {
                            _info_05.subtotal = dt_05.Rows[0]["subtotal"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["subtotal"]) : 0;
                            _info_05.tax = dt_05.Rows[0]["tax"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["tax"]) : 0;
                            _info_05.total = dt_05.Rows[0]["total"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["total"]) : 0;
                            _info_05.plates = dt_05.Rows[0]["plates"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["plates"]) : 0;
                            _info_05.pieces = dt_05.Rows[0]["pieces"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["pieces"]) : 0;
                            _info_05.cbm = dt_05.Rows[0]["cbm"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["cbm"]) : 0;
                            _info_05.special_fee = dt_05.Rows[0]["special_fee"] != DBNull.Value ? Convert.ToInt32(dt_05.Rows[0]["special_fee"]) : 0;
                        }

                    }
                }
            }
        }

        string cmdText = string.Empty;

        if (second_code.SelectedValue == "*")
        {
            cmdText = "Select top 1 * from tbCustomers with(nolock) where supplier_code like '' + @supplier_code + '%' order by customer_code";
        }
        else
        {
            cmdText = "Select top 1 * from tbCustomers with(nolock) where customer_code like '' + @customer_code + '%' order by customer_code";
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = cmdText;
            string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
            cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);  
            DataTable _dt = dbAdapter.getDataTable(cmd);
            if (_dt.Rows.Count > 0)
            {
                uniform_numbers = _dt.Rows[0]["uniform_numbers"].ToString();
                telephone = _dt.Rows[0]["telephone"].ToString();
                shipments_principal = _dt.Rows[0]["shipments_principal"].ToString();
                shipments_city = _dt.Rows[0]["shipments_city"].ToString();
                shipments_area = _dt.Rows[0]["shipments_area"].ToString();
                shipments_road = _dt.Rows[0]["shipments_road"].ToString();
                shipments_email = _dt.Rows[0]["shipments_email"].ToString();
                billing_special_needs = _dt.Rows[0]["billing_special_needs"].ToString();
                customer_name = _dt.Rows[0]["customer_name"].ToString();
            }
        }

        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("應收帳款");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;
            //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向       
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");
            sheet1.PrinterSettings.RepeatRows = sheet1.Cells["1:8"];       //重複表頭    
            sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小

            //所有欄放入同一頁面
            sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            sheet1.PrinterSettings.FitToWidth = 1;
            sheet1.PrinterSettings.FitToHeight= 0;

            //欄寬
            sheet1.Column(1).Width = 5;
            sheet1.Column(2).Width = 11;
            sheet1.Column(3).Width = 11;
            sheet1.Column(4).Width = 9;
            //sheet1.Column(5).Width = 18;
            sheet1.Column(5).Width = 11;
            sheet1.Column(6).Width = 20;
            sheet1.Column(7).Width = 22;
            sheet1.Column(8).Width = 7;
            sheet1.Column(9).Width = 7;
            sheet1.Column(10).Width = 9;



            sheet1.Cells[1, 1, 1, 11].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 11].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 11].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            
            sheet1.Cells[2, 1, 2, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 11].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 11].Merge = true;
            /*if (dlclose_type.SelectedValue == "1")
            {
                using (SqlCommand cmda = new SqlCommand())
                {
                    cmda.CommandText = @"SELECT min( close_begdate)  'close_begdate', max(close_enddate) 'close_enddate'
                                    FROM ttCheckoutCloseLog with(nolock)
                                    WHERE (src = 'Delivery') AND (customer_code LIKE ''+ @customer_code+'%') AND (cdate = @close_date)";
                    string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
                    cmda.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    cmda.Parameters.AddWithValue("@close_date", dlCloseDay.SelectedValue.ToString());  //結帳日
                    DataTable _dt = dbAdapter.getDataTable(cmda);
                    if (_dt.Rows.Count > 0)
                    {
                        if (_dt.Rows[0]["close_begdate"] == DBNull.Value || _dt.Rows[0]["close_begdate"].ToString() == "")
                        {
                            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + (Convert.ToDateTime(dlCloseDay.SelectedValue.ToString()).AddMonths(-1).AddDays(1)).ToString("yyyy/MM/dd") + "~" + Convert.ToDateTime(dlCloseDay.SelectedValue.ToString()).ToString("yyyy/MM/dd");
                        }
                        else
                        {
                            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + Convert.ToDateTime(_dt.Rows[0]["close_begdate"]).ToString("yyyy/MM/dd") + "~" + Convert.ToDateTime(_dt.Rows[0]["close_enddate"]).ToString("yyyy/MM/dd");
                        }
                        
                    }
                }
            }
            else
            {
                sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + dates.Text + "~" + datee.Text;
            } */
            DataTable dt_for_excel_date = null;

            string cmdText2 = string.Empty;
            string excel_date_start = "";
            string excel_date_end = "";

            if (second_code.SelectedValue == "*")
            {
                cmdText2 = @"select TOP 2 Convert(varchar(10),min(checkout_close_date),25)'checkout_close_date' 
from tcDeliveryRequests with(nolock) where supplier_code like @supplier_code + '%' group by Convert(varchar(10),checkout_close_date,25) 
order by Convert(varchar(10),checkout_close_date,25) desc";
            }
            else
            {
                cmdText2 = @"select TOP 2 Convert(varchar(10),min(checkout_close_date),25)'checkout_close_date' 
from tcDeliveryRequests with(nolock) where customer_code like @customer_code + '%' group by Convert(varchar(10),checkout_close_date,25) 
order by Convert(varchar(10),checkout_close_date,25) desc";
            }

            using (SqlCommand cmd = new SqlCommand(cmdText2))
            {
                cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue + second_code.Text.Trim());
                cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                dt_for_excel_date = dbAdapter.getDataTable(cmd);
                if (dt_for_excel_date.Rows.Count > 0 && dt_for_excel_date.Rows[0]["checkout_close_date"].ToString() != "")
                {
                    DateTime dateEnd = DateTime.Parse(dt_for_excel_date.Rows[0]["checkout_close_date"].ToString());
                    excel_date_end = dateEnd.ToString("yyyy/MM/dd");
                    excel_date_start = dateEnd.AddMonths(-1).AddDays(1).ToString("yyyy/MM/dd");
                    if (dateEnd.Day == DateTime.DaysInMonth(dateEnd.Year, dateEnd.Month))
                    {
                        excel_date_start = new DateTime(dateEnd.Year, dateEnd.Month, 1).ToString("yyyy/MM/dd");
                    }
                    if (dt_for_excel_date.Rows.Count > 1 && dt_for_excel_date.Rows[1]["checkout_close_date"].ToString() != "")
                    {
                        excel_date_start = DateTime.Parse(excel_date_start) > DateTime.Parse(dt_for_excel_date.Rows[1]["checkout_close_date"].ToString()) ? DateTime.Parse(excel_date_start).ToString("yyyy/MM/dd") : DateTime.Parse(dt_for_excel_date.Rows[1]["checkout_close_date"].ToString()).AddDays(1).ToString("yyyy/MM/dd");
                    }
                }
            }

            sheet1.Cells[2, 1, 2, 1].Value = excel_date_start + "~" + excel_date_end;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 1, 3, 11].Merge = true;     //合併儲存格
            sheet1.Cells[3, 1, 3, 1].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 1, 3, 1].Style.Font.Size = 12;
            sheet1.Cells[3, 1, 3, 1].Value = "自營客戶：" + customer_name;
            sheet1.Cells[3, 1, 3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;          

            sheet1.Cells[4, 1, 4, 1].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 1].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 1].Value = "統一編號：" + uniform_numbers;
            sheet1.Cells[4, 1, 4, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[4, 1, 4, 4].Merge = true;

            sheet1.Cells[5, 1, 5, 1].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[5, 1, 5, 1].Style.Font.Size = 12;
            sheet1.Cells[5, 1, 5, 1].Value = "聯絡電話：" + telephone;
            sheet1.Cells[5, 1, 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[5, 1, 5, 4].Merge = true;
            sheet1.Cells[5, 7, 5, 7].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[5, 7, 5, 7].Style.Font.Size = 12;
            sheet1.Cells[5, 7, 5, 7].Value = "對帳人：" + shipments_principal;
            sheet1.Cells[5, 7, 5, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet1.Cells[5, 7, 5, 11].Merge = true;

            sheet1.Cells[6, 1, 6, 1].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[6, 1, 6, 1].Style.Font.Size = 12;
            sheet1.Cells[6, 1, 6, 1].Value = "地址：" + shipments_city + shipments_area + shipments_road;
            sheet1.Cells[6, 1, 6, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[6, 1, 6, 5].Merge = true;
            sheet1.Cells[6, 7, 6, 7].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[6, 7, 6, 7].Style.Font.Size = 12;
            sheet1.Cells[6, 7, 6, 7].Value = shipments_email;
            sheet1.Cells[6, 7, 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet1.Cells[6, 7, 6, 11].Merge = true;

            sheet1.Cells[7, 1, 7, 1].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[7, 1, 7, 1].Style.Font.Size = 12;
            sheet1.Cells[7, 1, 7, 1].Value = "開立發票號碼：";
            sheet1.Cells[7, 1, 7, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            sheet1.Cells[7, 1, 7, 4].Merge = true;
            sheet1.Cells[7, 7, 7, 7].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[7, 7, 7, 7].Style.Font.Size = 12;
            sheet1.Cells[7, 7, 7, 7].Value = billing_special_needs;
            sheet1.Cells[7, 7, 7, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            sheet1.Cells[7, 7, 7, 11].Merge = true;



            sheet1.Cells[8, 1, 8, 11].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[8, 1, 8, 11].Style.Font.Size = 12;
            sheet1.Cells[8, 1, 8, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[8, 1, 8, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
            sheet1.Cells[8, 1, 8, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium;    //框線
            sheet1.Cells[8, 1, 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;    //框線
            sheet1.Cells[8, 11, 8, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
            sheet1.Cells[8, 1, 8, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Dotted; //框線
            sheet1.Cells[8, 1, 8, 11].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            sheet1.Cells[8, 1, 8, 11].Style.Fill.BackgroundColor.SetColor(Color.LightYellow);//设置背景色
            sheet1.Row(8).Height = 25;


            sheet1.Cells[8, 1].Value = "序號";
            sheet1.Cells[8, 2].Value = "發送日期";
            sheet1.Cells[8, 3].Value = "配送日期";
            sheet1.Cells[8, 4].Value = "配送模式";
            //sheet1.Cells[8, 5].Value = "客戶名稱";
            sheet1.Cells[8, 5].Value = "貨號";
            sheet1.Cells[8, 6].Value = "發送區";
            sheet1.Cells[8, 7].Value = "到著區";
            sheet1.Cells[8, 8].Value = "件數";
            sheet1.Cells[8, 9].Value = "板數";
            sheet1.Cells[8, 10].Value = "加收費";
            sheet1.Cells[8, 11].Value = "貨件運費";

            ttRow = 9;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[ttRow, 1].Value = (i + 1).ToString();
                sheet1.Cells[ttRow, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[ttRow, 2].Value = dt.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[ttRow, 3].Value = dt.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[ttRow, 4].Value = dt.Rows[i]["配送模式"].ToString();
                //sheet1.Cells[ttRow, 5].Value = dt.Rows[i]["客戶名稱"].ToString();
                sheet1.Cells[ttRow, 5].Value = dt.Rows[i]["貨號"].ToString();
                sheet1.Cells[ttRow, 6].Value = dt.Rows[i]["發送區2"].ToString();
                sheet1.Cells[ttRow, 7].Value = dt.Rows[i]["到著區2"].ToString();
                sheet1.Cells[ttRow, 8].Value = Convert.ToInt32(dt.Rows[i]["件數"]);
                sheet1.Cells[ttRow, 8].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[ttRow, 9].Value = Convert.ToInt32(dt.Rows[i]["板數"]);
                sheet1.Cells[ttRow, 9].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[ttRow, 10].Value = Convert.ToInt32(dt.Rows[i]["加收費"]);
                sheet1.Cells[ttRow, 10].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[ttRow, 11].Value = Convert.ToInt32(dt.Rows[i][FeeColName]);
                sheet1.Cells[ttRow, 11].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[ttRow, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet1.Cells[ttRow, 1, ttRow, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                sheet1.Cells[ttRow, 11, ttRow, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線


                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    if (_info_01 != null)
                    {
                        sheet1.Cells[ttRow + 1, 7].Value = "棧板收入小計";
                        sheet1.Cells[ttRow + 1, 8].Value = _info_01.pieces;                        
                        sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 9].Value = _info_01.plates;
                        sheet1.Cells[ttRow + 1, 9].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Value = _info_01.subtotal;
                        sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                        sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ttRow += 1;
                    }

                    if (_info_05 != null)
                    {
                        sheet1.Cells[ttRow + 1, 7].Value = "專車收入小計";
                        sheet1.Cells[ttRow + 1, 8].Value = _info_05.pieces;
                        sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 9].Value = _info_05.plates;
                        sheet1.Cells[ttRow + 1, 9].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Value = _info_05.subtotal;
                        sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                        sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                        sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ttRow += 1;
                    }
                    sheet1.Cells[ttRow + 1, 7].Value = "總計";
                    sheet1.Cells[ttRow + 1, 8].Formula = "=SUM(H9:H"+ (ttRow-2).ToString() +")";
                    //sheet1.Cells[ttRow + 1, 9].Value = _info.pieces;
                    sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow + 1, 9].Formula = "=SUM(I9:I" + (ttRow - 2).ToString() + ")";
                    //sheet1.Cells[ttRow + 1, 10].Value = _info.plates;
                    sheet1.Cells[ttRow + 1, 8].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow + 1, 11].Formula = "=SUM(K9:K" + (ttRow - 2).ToString() + ")";
                    //sheet1.Cells[ttRow + 1, 10].Value = _info.subtotal;
                    sheet1.Cells[ttRow + 1, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow + 1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                    sheet1.Cells[ttRow + 1, 1, ttRow + 1, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                    sheet1.Cells[ttRow + 1, 11, ttRow + 1, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                    sheet1.Cells[ttRow + 1, 1, ttRow + 1, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                   

                    sheet1.Cells[ttRow + 2, 7].Value = "5%營業稅";
                    sheet1.Cells[ttRow + 2, 11].Value = _info.tax;
                    sheet1.Cells[ttRow + 2, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow + 2, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[ttRow + 2, 1, ttRow + 2, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[ttRow + 2, 1, ttRow + 2, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                    sheet1.Cells[ttRow + 2, 11, ttRow + 3, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                    sheet1.Cells[ttRow + 2, 1, ttRow + 2, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                   

                    sheet1.Cells[ttRow + 3, 7].Value = "應收帳款";
                    sheet1.Cells[ttRow + 3, 11].Formula = "=SUM(K"+ (ttRow + 1).ToString()+ ":K" + (ttRow +2).ToString() +")";
                    //sheet1.Cells[ttRow + 3, 11].Value = _info.total;
                    sheet1.Cells[ttRow + 3, 11].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[ttRow + 3, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[ttRow + 3, 1, ttRow + 3, 11].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[ttRow + 3, 1, ttRow + 3, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;  //框線
                    sheet1.Cells[ttRow + 3, 11, ttRow + 3, 11].Style.Border.Right.Style = ExcelBorderStyle.Medium; //框線
                    sheet1.Cells[ttRow + 3, 1, ttRow + 3, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                  

                }
                ttRow +=1;
            }

            sheet1.Cells[ttRow + 3, 1].Value = "匯入行：玉山銀行林口分行 808-0886";
            sheet1.Cells[ttRow + 3, 1, ttRow + 3,4].Merge = true;
            sheet1.Cells[ttRow + 4, 1].Value = "帳號：0886940035885";
            sheet1.Cells[ttRow + 4, 1, ttRow +4, 4].Merge = true;
            sheet1.Cells[ttRow + 5, 1].Value = "戶名：峻富物流股份有限公司";
            sheet1.Cells[ttRow + 5, 1, ttRow + 5, 4].Merge = true;

            sheet1.Cells[ttRow + 7, 1, ttRow +7, 10].Style.Font.Size = 14;
            sheet1.Cells[ttRow + 7, 1].Value = "核准：";
            sheet1.Cells[ttRow + 7, 1, ttRow +7, 3].Merge = true;
            sheet1.Cells[ttRow + 7, 4].Value = "初審：";
            sheet1.Cells[ttRow + 7, 7].Value = "主管：";
            sheet1.Cells[ttRow + 7, 9].Value = "製表：";
            //sheet1.Cells.AutoFitColumns();


            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("應收帳款明細表.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }

    protected void close_date_SelectedIndexChanged(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }

    protected void btExport_Detail_Click(object sender, EventArgs e)
    {
        DataTable dt = null;
        DataTable dt_for_excel_date = null;
        Utility.AccountsReceivableInfo _info = null;
        string Title = Suppliers.SelectedItem.Text;
        string customer_code = Suppliers.SelectedValue + second_code.Text.Trim();
        string FeeColName = "";

        if (second_code.SelectedValue == "*")
        {
            _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, customer_code, null, "", null, Suppliers.SelectedValue);
        }
        else
        {
            _info = Utility.GetSumAccountsReceivable(dates.Text, datee.Text, dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null, customer_code, null, "", null);
        }

        if (manager_type == "4")   //區配看到的應收為C段運費
        {
            Title = "應收帳款明細表";
            FeeColName = "cscetion_fee";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetAccountsPayable";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日
                dt = dbAdapter.getDataTable(cmd);
            }
        }
        else
        {
            Title = "峻富物流股份有限公司";
            FeeColName = "貨件費用";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "usp_GetAccountsReceivablebyJUNFU";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pricing_type", dl_pricing_type.SelectedValue);

                if (second_code.SelectedValue == "*")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }

                cmd.Parameters.AddWithValue("@dates", dates.Text);  //日期起
                cmd.Parameters.AddWithValue("@datee", datee.Text);  //日期迄
                cmd.Parameters.AddWithValue("@closedate", dlclose_type.SelectedValue == "1" ? dlCloseDay.SelectedValue.ToString() : null);  //結帳日            
                dt = dbAdapter.getDataTable(cmd);
                
            }
        }

        string cmdText = string.Empty;
        string excel_date_start = "";
        string excel_date_end = "";

        if (second_code.SelectedValue == "*")
        {
            cmdText = @"select TOP 2 Convert(varchar(10),min(checkout_close_date),25)'checkout_close_date' 
from tcDeliveryRequests with(nolock) where supplier_code like @supplier_code + '%' group by Convert(varchar(10),checkout_close_date,25) 
order by Convert(varchar(10),checkout_close_date,25) desc";            
        }
        else
        {
            cmdText = @"select TOP 2 Convert(varchar(10),min(checkout_close_date),25)'checkout_close_date' 
from tcDeliveryRequests with(nolock) where customer_code like @customer_code + '%' group by Convert(varchar(10),checkout_close_date,25) 
order by Convert(varchar(10),checkout_close_date,25) desc";  
        }

        using (SqlCommand cmd = new SqlCommand(cmdText))
        {
            cmd.Parameters.AddWithValue("@customer_code", Suppliers.SelectedValue + second_code.Text.Trim());
            cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
            dt_for_excel_date = dbAdapter.getDataTable(cmd);
            if (dt_for_excel_date.Rows.Count > 0)
            {
                excel_date_end = DateTime.Parse(dt_for_excel_date.Rows[0]["checkout_close_date"].ToString()).ToString("yyyy/MM/dd");
                DateTime tempStartDate = DateTime.Parse(dt_for_excel_date.Rows[0]["checkout_close_date"].ToString());
                excel_date_start = tempStartDate.AddMonths(-1).AddDays(1).ToString("yyyy/MM/dd");
                if (tempStartDate.Day == DateTime.DaysInMonth(tempStartDate.Year, tempStartDate.Month))
                {
                    excel_date_start = new DateTime(tempStartDate.Year, tempStartDate.Month, 1).ToString("yyyy/MM/dd");
                }
                if (dt_for_excel_date.Rows.Count > 1)
                {
                    excel_date_start = DateTime.Parse(excel_date_start) > DateTime.Parse(dt_for_excel_date.Rows[1]["checkout_close_date"].ToString()) ? DateTime.Parse(excel_date_start).ToString("yyyy/MM/dd") : DateTime.Parse(dt_for_excel_date.Rows[1]["checkout_close_date"].ToString()).AddDays(1).ToString("yyyy/MM/dd");
                }
            }
        }


        using (ExcelPackage p = new ExcelPackage())
        {
            ExcelPackage pck = new ExcelPackage();
            var sheet1 = pck.Workbook.Worksheets.Add("應收帳款明細");

            #region sheet1                
            //檔案邊界
            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
            sheet1.PrinterSettings.HorizontalCentered = true;

            sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
            // sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
            //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");

            //欄寬
            sheet1.Column(1).Width = 6;
            sheet1.Column(2).Width = 20;
            sheet1.Column(3).Width = 15;
            sheet1.Column(4).Width = 15;
            sheet1.Column(5).Width = 30;
            sheet1.Column(6).Width = 20;
            sheet1.Column(7).Width = 10;
            sheet1.Column(8).Width = 10;
            sheet1.Column(9).Width = 10;
            sheet1.Column(10).Width = 10;
            sheet1.Column(11).Width = 10;



            sheet1.Cells[1, 1, 1, 25].Merge = true;     //合併儲存格
            sheet1.Cells[1, 1, 1, 25].Value = Title;    //Set the value of cell A1 to 1
            sheet1.Cells[1, 1, 1, 25].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[1, 1, 1, 25].Style.Font.Size = 18;
            sheet1.Cells[1, 1, 1, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
            sheet1.Cells[1, 1, 1, 25].Style.VerticalAlignment = ExcelVerticalAlignment.Center; //上下置中

            sheet1.Cells[2, 1, 2, 25].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[2, 1, 2, 25].Style.Font.Size = 12;
            sheet1.Cells[2, 1, 2, 25].Merge = true;
            sheet1.Cells[2, 1, 2, 1].Value = "請款期間：" + excel_date_start + "~" + excel_date_end;
            sheet1.Cells[2, 1, 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet1.Cells[3, 2, 3, 25].Merge = true;     //合併儲存格
            sheet1.Cells[3, 2, 3, 2].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[3, 2, 3, 2].Style.Font.Size = 12;
            sheet1.Cells[3, 2, 3, 2].Value = "自營客戶：" + (second_code.SelectedItem != null ? second_code.SelectedItem.Text : "");
            sheet1.Cells[3, 2, 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;


            sheet1.Cells[4, 1, 4, 25].Style.Font.Name = "微軟正黑體";
            sheet1.Cells[4, 1, 4, 25].Style.Font.Size = 12;
            sheet1.Cells[4, 1, 4, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet1.Cells[4, 1, 4, 25].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
            sheet1.Cells[4, 1, 4, 25].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet1.Cells[4, 25].Style.Border.Right.Style = ExcelBorderStyle.Thin;

            sheet1.Cells[4, 1].Value = "序號";
            sheet1.Cells[4, 2].Value = "發送日期";
            sheet1.Cells[4, 3].Value = "配送日期";
            sheet1.Cells[4, 4].Value = "客戶代碼";
            sheet1.Cells[4, 5].Value = "客戶名稱";
            sheet1.Cells[4, 6].Value = "貨號";
            sheet1.Cells[4, 7].Value = "發送區";
            sheet1.Cells[4, 8].Value = "到著區";
            sheet1.Cells[4, 9].Value = "訂單號碼";
            sheet1.Cells[4, 10].Value = "傳票類別";
            sheet1.Cells[4, 11].Value = "收件人";
            sheet1.Cells[4, 12].Value = "收件人電話";
            sheet1.Cells[4, 13].Value = "分機";
            sheet1.Cells[4, 14].Value = "收件人地址";
            sheet1.Cells[4, 15].Value = "到著碼";
            sheet1.Cells[4, 16].Value = "寄件人";
            sheet1.Cells[4, 17].Value = "寄件人地址";
            sheet1.Cells[4, 18].Value = "指定日";
            sheet1.Cells[4, 19].Value = "時段";
            sheet1.Cells[4, 20].Value = "回單";
            sheet1.Cells[4, 21].Value = "棧板回收";
            sheet1.Cells[4, 22].Value = "備註";
            sheet1.Cells[4, 23].Value = "件數";
            sheet1.Cells[4, 24].Value = "板數";
            sheet1.Cells[4, 25].Value = "貨件運費";


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sheet1.Cells[i + 5, 1].Value = (i + 1).ToString();
                sheet1.Cells[i + 5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet1.Cells[i + 5, 2].Value = dt.Rows[i]["發送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["發送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 3].Value = dt.Rows[i]["配送日期"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["配送日期"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 4].Value = dt.Rows[i]["客戶代碼"].ToString();
                sheet1.Cells[i + 5, 5].Value = dt.Rows[i]["客戶名稱"].ToString();
                sheet1.Cells[i + 5, 6].Value = dt.Rows[i]["貨號"].ToString();
                sheet1.Cells[i + 5, 7].Value = dt.Rows[i]["發送區"].ToString();
                sheet1.Cells[i + 5, 8].Value = dt.Rows[i]["到著區"].ToString();
                sheet1.Cells[i + 5, 9].Value = dt.Rows[i]["訂單號碼"].ToString();
                sheet1.Cells[i + 5, 10].Value = dt.Rows[i]["傳票類別"].ToString();
                sheet1.Cells[i + 5, 11].Value = dt.Rows[i]["收件人"].ToString();
                sheet1.Cells[i + 5, 12].Value = dt.Rows[i]["收件人電話"].ToString();
                sheet1.Cells[i + 5, 13].Value = dt.Rows[i]["分機"].ToString();
                sheet1.Cells[i + 5, 14].Value = dt.Rows[i]["收件人地址"].ToString();
                sheet1.Cells[i + 5, 15].Value = dt.Rows[i]["到著碼"].ToString();
                sheet1.Cells[i + 5, 16].Value = dt.Rows[i]["寄件人"].ToString();
                sheet1.Cells[i + 5, 17].Value = dt.Rows[i]["寄件人地址"].ToString();
                sheet1.Cells[i + 5, 18].Value = dt.Rows[i]["指定日"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["指定日"]).ToString("yyyy/MM/dd") : "";
                sheet1.Cells[i + 5, 19].Value = dt.Rows[i]["時段"].ToString();
                sheet1.Cells[i + 5, 20].Value = dt.Rows[i]["回單"].ToString();
                sheet1.Cells[i + 5, 21].Value = dt.Rows[i]["棧板回收"].ToString();
                sheet1.Cells[i + 5, 22].Value = dt.Rows[i]["備註"].ToString();
                sheet1.Cells[i + 5, 23].Value = Convert.ToInt32(dt.Rows[i]["件數"]);
                sheet1.Cells[i + 5, 23].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 24].Value = Convert.ToInt32(dt.Rows[i]["板數"]);
                sheet1.Cells[i + 5, 24].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 25].Value = Convert.ToInt32(dt.Rows[i][FeeColName]);

                sheet1.Cells[i + 5, 25].Style.Numberformat.Format = "#,##0";
                sheet1.Cells[i + 5, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                if (i == dt.Rows.Count - 1 && _info != null)
                {
                    sheet1.Cells[i + 6, 22].Value = "小計";
                    sheet1.Cells[i + 6, 23].Value = _info.plates;
                    sheet1.Cells[i + 6, 24].Value = _info.pieces;
                    sheet1.Cells[i + 6, 25].Value = _info.subtotal;
                    sheet1.Cells[i + 6, 25].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 6, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 6, 1, i + 6, 25].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 6, 1, i + 6, 25].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 6, 25].Style.Border.Right.Style = ExcelBorderStyle.Thin;


                    sheet1.Cells[i + 7, 22].Value = "5%營業稅";
                    sheet1.Cells[i + 7, 25].Value = _info.tax;
                    sheet1.Cells[i + 7, 25].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 7, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 7, 1, i + 7, 25].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 7, 1, i + 7, 25].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 7, 25].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    sheet1.Cells[i + 8, 22].Value = "應收帳款";
                    sheet1.Cells[i + 8, 25].Value = _info.total;
                    sheet1.Cells[i + 8, 25].Style.Numberformat.Format = "#,##0";
                    sheet1.Cells[i + 8, 25].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet1.Cells[i + 8, 1, i + 8, 25].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[i + 8, 1, i + 8, 25].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[i + 8, 25].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                }
            }



            //頁尾加入頁次
            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁  共" + ExcelHeaderFooter.NumberOfPages + "頁";
            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
            #endregion

            Response.Clear();
            Response.AddHeader("content-disposition", "attachment; filename=" + HttpUtility.UrlEncode("應收帳款明細表.xlsx", Encoding.UTF8));
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.BinaryWrite(pck.GetAsByteArray());
            Response.End();
        }
    }


    protected void second_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        dlclose_type_SelectedIndexChanged(null, null);
    }

    protected void dl_pricing_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        dlclose_type_SelectedIndexChanged(null, null);
    }

    protected void close_date_SelectedIndexChanged1(object sender, EventArgs e)
    {
        Suppliers_SelectedIndexChanged(null, null);
    }

    protected void btClose2_Click(object sender, EventArgs e)
    {
        lSuppliers.Text = Suppliers.SelectedItem.Text;
        lsecond_code.Text = second_code.SelectedItem.Text;
        date_end.Text = DateTime.Today.ToString("yyyy/MM/dd");
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#handclose').modal();</script>", false);
        return;
        
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"Select A.customer_code , b.customer_name , b.customer_shortname ,  
                            sum(a.plates) 'plates' ,
                            sum(_fee.total_fee) 'total_fee' 
                            from tcDeliveryRequests A
                            left join tbCustomers B on A.customer_code  = B.customer_code 
                            CROSS APPLY dbo.fu_GetShipFeeByRequestId(A.request_id) _fee
                            where 1=1 
                            and A.pricing_type = @pricing_type
                            and  print_date >= @dates and  print_date < @datee
                            group by A.customer_code,  b.customer_name , b.customer_shortname ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pricing_type", "05");  //計價模式
            //cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
            cmd.Parameters.AddWithValue("@dates", "2019-12-26");  //日期起
            cmd.Parameters.AddWithValue("@datee", "2020-01-01");  //日期迄
            
            

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    
                }

            }
        }
    }
}