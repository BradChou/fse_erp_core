﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store1_3.aspx.cs" Inherits="store1_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    
    <style>
        input[type="radio"] {
            display: inherit;
        }
        .checkbox, .radio{
            display: inherit;
        }

        .radio label {
           
            text-align :left ;
        }

        ._add_table {
            width: 90%;
        }

        ._add_title {
            width: 9%;
            padding: 5px;
        }

        ._add_data {
            width: 41%;
        }

        ._add_btn {
            text-align: center;
        }

        ._foradd {
            margin: 5px 20px;
        }

        ._setadd {
            right: 10px;
            text-align: right;
        }

        ._hr {
            width: 100%;
            display: inline-table;
        }

        ._btnsh {
            margin: 10px auto;
        }
         ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }      
         .checkbox, .radio{
             display:unset;
         }
            .radio label {
                padding-right: 20px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
        <h2 class="margin-bottom-10"><asp:Label ID="lbl_title" Text="人員場區維護" runat="server"></asp:Label></h2>
            <div class="row col-lg-12">
                    <asp:RadioButtonList ID="rbl_typeForSh" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="radio radio-success">
                    </asp:RadioButtonList><asp:Button ID="btn_Search" class="btn btn-primary _btnsh" runat="server" Text="查 詢" OnClick="btn_Search_Click" />
                <hr />
                </div>
        
        <div  >
            <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide"> 
                        <th>管理單位</th>
                        <th>公司別</th>
                        <th>職稱</th>
                        <th>人員</th>
                        <th>帳號</th>
                        <th class="td_th td_edit">功　　能</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td data-th="管理單位">
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_id").ToString())%>' />
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.manager_unit").ToString())%>
                                </td>
                                <td data-th="公司別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_shortname").ToString())%> </td>
                                <td data-th="職稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.job_title").ToString())%> </td>
                                <td data-th="人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="帳號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_code").ToString())%></td>
                                <td class="td_data td_edit" data-th="狀態">
                                    <a href="store1_3edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.account_code").ToString())%>" class="btn btn-warning">設定</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="16" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
            <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span> 筆
                </div>
        </div>

        
        
            </div>
    </div>
</asp:Content>

