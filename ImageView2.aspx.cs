﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ImageView2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (!IsPostBack)

        if (!loginchk.IsLogin())
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        else
        {       
            string ImgUrl = string.Empty;
            string Type = string.Empty;
            //string ImgUrl2 = string.Empty;
            //string ImgUrl3 = string.Empty;
            //string ImgUrl4 = string.Empty;
            //String filename = string.Empty;
            //String second_filename = string.Empty;

            if (Request.QueryString["ImgUrl"] != null) ImgUrl = Request.QueryString["ImgUrl"].ToString();
            if (Request.QueryString["type"] != null) Type = Request.QueryString["type"].ToString();
            //if (Request.QueryString["ImgUrl2"] != null) ImgUrl2 = Request.QueryString["ImgUrl2"].ToString();
            //if (Request.QueryString["ImgUrl3"] != null) ImgUrl3 = Request.QueryString["ImgUrl3"].ToString();
            //if (Request.QueryString["ImgUrl4"] != null) ImgUrl4 = Request.QueryString["ImgUrl4"].ToString();
            //if (Request.QueryString["filename"] != null) filename = Request.QueryString["filename"].ToString();
            //if (Request.QueryString["second_filename"] != null) second_filename = Request.QueryString["second_filename"].ToString();



            //Image1.ImageUrl = ImgUrl + filename;
            imgSample.Src = "data:image/jpg;base64," + ImgToBase64String(ImgUrl);
            //imgSample2.Src = "data:image/jpg;base64," + ImgToBase64String(ImgUrl2 + second_filename);
            //imgSample2.Src = "data:image/jpg;base64," + ImgToBase64String(ImgUrl2);
            //imgSample3.Src = "data:image/jpg;base64," + ImgToBase64String(ImgUrl3);
            //imgSample4.Src = "data:image/jpg;base64," + ImgToBase64String(ImgUrl4);
            //// 讀取圖片並轉為 Image 物件
            //var image = System.Drawing.Image.FromFile(ImgUrl + filename);
            //// 設定至 imgSample 並且自動判定格式 
            //imgSample.Src = "data:image/" + GetImageFormat(image) + ";base64," + ImageToBase64(image);
            //if (ImgUrl == "")
            //{

            //    imgSample2.Style.Add("display", "none");
            //    if(ImgUrl.Contains("webservice.fs-express.com.tw"))   //電子簽單
            //    { Div1.Style.Remove("display"); }
            //    else
            //    {
            //        { Div3.Style.Remove("display"); }
            //    }
                
            //}
            //else
            //{
            //    Div1.Style.Remove("display");
            //    Div2.Style.Remove("display");
            //}
            if(Type == "1")
            {
                Div1.Style.Remove("display");
            }
            else if (Type =="2")
            {
                Div2.Style.Remove("display");
            }
            else if (Type == "3")
            {
                Div3.Style.Remove("display");
            }
            else if (Type == "4")
            {
                Div4.Style.Remove("display");
            }

        }

    }

    /// <summary>
    /// 自動判斷圖片格式
    /// </summary>
    /// <param name="img"></param>
    /// <returns></returns>
    public static System.Drawing.Imaging.ImageFormat GetImageFormat(System.Drawing.Image img)
    {
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg))
            return System.Drawing.Imaging.ImageFormat.Jpeg;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp))
            return System.Drawing.Imaging.ImageFormat.Bmp;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png))
            return System.Drawing.Imaging.ImageFormat.Png;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Emf))
            return System.Drawing.Imaging.ImageFormat.Emf;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Exif))
            return System.Drawing.Imaging.ImageFormat.Exif;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif))
            return System.Drawing.Imaging.ImageFormat.Gif;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Icon))
            return System.Drawing.Imaging.ImageFormat.Icon;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.MemoryBmp))
            return System.Drawing.Imaging.ImageFormat.MemoryBmp;
        if (img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Tiff))
            return System.Drawing.Imaging.ImageFormat.Tiff;
        else
            return System.Drawing.Imaging.ImageFormat.Wmf;
    }

    /// <summary>
    /// 將Image 物件轉 Base64
    /// </summary>
    /// <param name="image"></param>
    /// <returns></returns>
    //public string ImageToBase64(System.Drawing.Image image)
    //{

    //    MemoryStream ms = new MemoryStream();

    //    System.Drawing.Imaging.ImageFormat format = GetImageFormat(image);

    //    // 將圖片轉成byte[]
    //    image.Save(ms, format);
    //    byte[] imageBytes = ms.ToArray();

    //    // 將 byte[] 轉 base64
    //    string base64String = Convert.ToBase64String(imageBytes);
    //    return base64String;

    //}
    protected string ImgToBase64String(string Imagefilename)
    {
        try
        {
            ////1. 先通过URI建立一个WebRequest（请参考WebRequest构造函数）：
            WebRequest wr = WebRequest.Create(Imagefilename);
            wr.Timeout = 3 * 1000;
            ////2.然后通过其GetResponse方法得到一个WebResponse： 
            WebResponse res = wr.GetResponse();

            ////3. 通过WebResponse.GetResponseStream方法得到的流来创建Bitmap： 
            Bitmap bmp = new Bitmap(res.GetResponseStream());




            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] arr = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(arr, 0, (int)ms.Length);
            ms.Close();
            return Convert.ToBase64String(arr);
        }
        catch (Exception ex)
        {
            return null;
        }
    }





}