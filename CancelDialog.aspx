﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CancelDialog.aspx.cs" Inherits="CancelDialog" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-銷單</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">     
        <div>
            <div>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="H1">請輸入銷單原因(限200字)</h4>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group form-inline">
                                &nbsp;
                                <asp:TextBox ID="delete_memo" runat="server" class="form-control" TextMode ="MultiLine" Height="200px" MaxLength="200" ></asp:TextBox>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="delete_memo" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入銷單原因</asp:RequiredFieldValidator>
                                <br />
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="確定" ValidationGroup="validate" OnClick="search_Click" />
                                <asp:Button ID="search0" CssClass="btn btn-primary" runat="server" Text="取消"  OnClientClick="parent.$.fancybox.close();" />
                                <asp:HiddenField ID="retval" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
