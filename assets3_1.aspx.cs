﻿using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class assets3_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            
            #region 費用類別
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select code_id, code_name from tbItemCodes with(nolock) where code_bclass = '6' and code_sclass = 'fee_type' and active_flag = 1 order by code_id ";
                ddl_fee.DataSource = dbAdapter.getDataTable(cmd);
                ddl_fee.DataValueField = "code_id";
                ddl_fee.DataTextField = "code_name";
                ddl_fee.DataBind();
                ddl_fee.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion
            
            #region 初始化日期
            sdate.Text = DateTime.Now.AddMonths(-1).ToString("yyy/MM/01");
            edate.Text = DateTime.Now.AddMonths(-1).ToString("yyy/MM/dd");
            #endregion            
        }

    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (ddl_fee.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue.ToString());
            wherestr += "  and a.fee_type =@fee_type";
        }

        if (sdate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
            wherestr += "  and a.date >=@sdate";
        }
        if (edate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@edate",Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
            wherestr += "  and a.date <=@edate";
        }

        //objCommand.CommandText = string.Format(@"select A.* ,
        //                                            ROUND(A.litre * A.price_notax,2)  as 'amt_notax' , 
        //                                            ROUND(A.litre * A.buy_price_notax,2) as 'buyamt_notax', 
        //                                            ROUND(A.litre * A.receipt_price_notax,2) as 'receiptamt_notax'
        //                                            from ttAssetsFee A with(nolock)
        //                                        where 1=1  {0} 
        //                                        order by a.date asc", wherestr);
        objCommand.CommandText = string.Format(@"Select A.* , b.code_name 'feename', C.tonnes , D.code_name 'deptname', E.code_name 'car_retailer_name', C.owner , C.driver ,
                                                CASE A.fee_type WHEN  '1' THEN  ROUND(A.litre  * A.price_notax,2) ELSE ROUND(A.quant * A.price_notax,2) END   as 'amt_notax' , 
                                                CASE A.fee_type WHEN  '1' THEN  ROUND(A.litre * A.buy_price_notax,2) ELSE ROUND(A.quant * A.buy_price_notax,2) END  as 'buyamt_notax', 
                                                CASE A.fee_type WHEN  '1' THEN  ROUND(A.litre * A.receipt_price_notax,2) ELSE  ROUND(A.quant * A.receipt_price_notax,2) END  as 'receiptamt_notax'
                                                From ttAssetsFee A with(nolock)
	                                            LEFT JOIN tbItemCodes B WITH(NOLOCK) ON A.fee_type  = b.code_id  AND B.code_bclass = '6'AND B.code_sclass = 'fee_type' and B.active_flag = 1
	                                            LEFT JOIN ttAssets C with (nolock) on A.a_id = C.car_license  
	                                            LEFT JOIN tbItemCodes D with(nolock) on C.dept = D.code_id  AND D.code_bclass = '6' AND D.code_sclass = 'dept'  and D.active_flag = 1
	                                            LEFT JOIN tbItemCodes E with(nolock) on C.car_retailer = E.code_id AND E.code_bclass = '6' AND E.code_sclass = 'CD'  and E.active_flag = 1
                                                where 1=1  {0} 
                                                order by a.date asc", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
            
        }

        

    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = p.Field<double >("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    
    /// <summary>
    /// 列印分析表-應收款及其他收入
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void print_Click(object sender, EventArgs e)
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (ddl_fee.SelectedValue.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@fee_type", ddl_fee.SelectedValue.ToString());
            wherestr += "  and a.fee_type =@fee_type";
        }

        if (sdate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@sdate", Convert.ToDateTime(sdate.Text.Trim().ToString()));
            wherestr += "  and a.date >=@sdate";
        }
        if (edate.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@edate", Convert.ToDateTime(edate.Text.Trim().ToString()).AddDays(1));
            wherestr += "  and a.date <=@edate";
        }

        if (ddl_fee.SelectedValue.ToString() == "1")
        {
            #region 油料
            objCommand.CommandText = string.Format(@"Select  car.car_retailer , car.dept, car.ownership, car.car_license, 
                                                    B.code_name as 'car_retailer_text', C.code_name as 'ownership_text', D.code_name as 'dept_name',
                                                    --a.litre , a.receipt_price_notax , a.price_notax ,
                                                    sum(ROUND(a.litre,2)) as 'litre',  sum(ROUND(a.litre *  a.receipt_price_notax,2)) as 'receiptamt_notax', sum(round(a.litre * a.price_notax,2)) as 'amt_notax'
                                                    From ttAssetsFee a
                                                    inner join ttAssets car on A.a_id  = car.car_license 
                                                    left join tbItemCodes B on car.car_retailer = b.code_id and B.code_bclass = '6'and B.code_sclass= 'CD'
                                                    left join tbItemCodes C on car.ownership = C.code_id and C.code_bclass = '6'and C.code_sclass= 'OS'
                                                    left join tbItemCodes D on car.dept = D.code_id and D.code_bclass = '6'and D.code_sclass= 'dept'
                                                    Where 1=1
                                                    and car.car_retailer <> '' and car.car_retailer <> '1'
                                                    {0}
                                                    group by car.car_retailer, car.dept,  car.ownership,car.car_license,B.code_name , C.code_name , D.code_name 
                                                    order by Convert(int,car.car_retailer)", wherestr);
            #endregion
            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {

                        int ttRow = 0;    //列                    
                        int tmpRow = 0;
                        string tmpretailer = "";
                        ExcelPackage pck = new ExcelPackage();
                        var sheet1 = pck.Workbook.Worksheets.Add("分析表-應收款及其他收入");

                        #region sheet1                
                        //檔案邊界
                        sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                        sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                        sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                        sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                        sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                        sheet1.PrinterSettings.HorizontalCentered = true;

                        //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                        sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小 
                        sheet1.PrinterSettings.RepeatRows = new ExcelAddress("$1:$4");

                        //sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                        //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");


                        //sheet1.DefaultColWidth = 6.75; // 默认所有列宽
                        //sheet1.DefaultRowHeight = 60;  // 默认所有行高



                        sheet1.Cells.Style.Font.Name = "標楷體";
                        sheet1.Cells[1, 1, 1, 8].Merge = true; //合併儲存格
                        sheet1.Cells[1, 1, 1, 8].Value = (Convert.ToDateTime(sdate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(sdate.Text).Month.ToString() + " " + ddl_fee.SelectedItem.Text + "（應收款及其他收入）";    //Set the value of cell A1 to 1
                        sheet1.Cells[1, 1, 1, 8].Style.Font.Bold = true;
                        sheet1.Cells[1, 1, 1, 8].Style.Font.Size = 20;


                        sheet1.Cells[3, 1].Value = "使用者";
                        sheet1.Cells[3, 1, 4, 1].Merge = true; //合併儲存格
                        sheet1.Cells[3, 2].Value = "分攤部門";
                        sheet1.Cells[3, 2, 4, 2].Merge = true; //合併儲存格
                        sheet1.Cells[3, 3].Value = "所有權";
                        sheet1.Cells[3, 3, 4, 3].Merge = true; //合併儲存格
                        sheet1.Cells[3, 4].Value = "車號";
                        sheet1.Cells[3, 4, 4, 4].Merge = true; //合併儲存格
                        sheet1.Cells[3, 5].Value = "數值";
                        sheet1.Cells[3, 5, 3, 8].Merge = true; //合併儲存格
                        sheet1.Cells[4, 5].Value = "加總";
                        sheet1.Cells[4, 6].Value = "代付款";
                        sheet1.Cells[4, 7].Value = "代收款";
                        sheet1.Cells[4, 8].Value = "其他收入";
                        sheet1.Cells[1, 1, 4, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                        sheet1.Cells[1, 1, 4, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                        sheet1.Cells[3, 1, 4, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[3, 1, 4, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet1.Cells[3, 1, 4, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sheet1.Cells[3, 1, 4, 8].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色

                        ttRow = 5;
                        tmpRow = 5;

                        string car_retailer = "";
                        string dept = "";
                        string ownership = "";


                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            if (car_retailer != dt.Rows[i]["car_retailer"].ToString())
                            {
                                if (i != 0)
                                {
                                    sheet1.Cells[ttRow, 1].Value = tmpretailer + "　合計";
                                    sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 5, ttRow - 1, 5).Address);
                                    sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 6, ttRow - 1, 6).Address);
                                    sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 7, ttRow - 1, 7).Address);
                                    sheet1.Cells[ttRow, 8, ttRow, 8].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 8, ttRow - 1, 8).Address);
                                    sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                    sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                                    ttRow += 1;
                                }
                                tmpRow = ttRow;
                                tmpretailer = dt.Rows[i]["car_retailer_text"].ToString();
                                sheet1.Cells[ttRow, 1].Value = dt.Rows[i]["car_retailer_text"].ToString();
                                car_retailer = dt.Rows[i]["car_retailer"].ToString();



                            }
                            if (dept != dt.Rows[i]["dept"].ToString())
                            {
                                sheet1.Cells[ttRow, 2].Value = dt.Rows[i]["dept_name"].ToString();
                                dept = dt.Rows[i]["dept"].ToString();
                            }
                            if (ownership != dt.Rows[i]["ownership"].ToString())
                            {
                                sheet1.Cells[ttRow, 3].Value = dt.Rows[i]["ownership_text"].ToString();
                                ownership = dt.Rows[i]["ownership"].ToString();
                            }
                            sheet1.Cells[ttRow, 4].Value = dt.Rows[i]["car_license"].ToString();
                            sheet1.Cells[ttRow, 5].Value = Convert.ToDouble(dt.Rows[i]["litre"]);  //.ToString("N2");
                            sheet1.Cells[ttRow, 6].Value = Convert.ToDouble(dt.Rows[i]["receiptamt_notax"]); //.ToString("N0");
                            sheet1.Cells[ttRow, 7].Value = Convert.ToDouble(dt.Rows[i]["amt_notax"]);   //.ToString("N0");
                            sheet1.Cells[ttRow, 8, ttRow, 8].Formula = "G" + ttRow.ToString() + "-F" + ttRow.ToString();
                            ttRow += 1;
                        }

                        if (tmpRow != ttRow)
                        {
                            sheet1.Cells[ttRow, 1].Value = tmpretailer + "　合計";
                            sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 5, ttRow - 1, 5).Address);
                            sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 6, ttRow - 1, 6).Address);
                            sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 7, ttRow - 1, 7).Address);
                            sheet1.Cells[ttRow, 8, ttRow, 8].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 8, ttRow - 1, 8).Address);
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                            ttRow += 1;
                        }
                        sheet1.Cells[ttRow, 1].Value = "總計";
                        sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 5, ttRow - 1, 5).Address);
                        sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 6, ttRow - 1, 6).Address);
                        sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 7, ttRow - 1, 7).Address);
                        sheet1.Cells[ttRow, 8, ttRow, 8].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 8, ttRow - 1, 8).Address);
                        sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                        sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        //sheet1.Cells.Style.ShrinkToFit = true;//单元格自动适应大小                    

                        sheet1.Cells[5, 5, ttRow, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[5, 5, ttRow, 5].Style.Numberformat.Format = "#,##0.00";//这是保留两位小数
                        sheet1.Cells[5, 6, ttRow, 8].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells.AutoFitColumns();

                        //頁尾加入頁次
                        sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁，共" + ExcelHeaderFooter.NumberOfPages + "頁";
                        sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                        #endregion

                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment; filename=" + "分析表-應收款及其他收入" + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(pck.GetAsByteArray());
                        Response.End();
                    }

                }
            }
        }
        else
        {
            objCommand.CommandText = string.Format(@"Select  car.car_retailer , car.dept, car.ownership, car.car_license, 
                                                    B.code_name as 'car_retailer_text', C.code_name as 'ownership_text', D.code_name as 'dept_name',
                                                    sum(ROUND(a.quant  *  a.buy_price_notax,2)) as 'buyamt_notax', sum(round(a.quant * a.receipt_price_notax,2)) as 'receiptamt_notax'
                                                    From ttAssetsFee a
                                                    inner join ttAssets car on A.a_id  = car.car_license 
                                                    left join tbItemCodes B on car.car_retailer = b.code_id and B.code_bclass = '6'and B.code_sclass= 'CD'
                                                    left join tbItemCodes C on car.ownership = C.code_id and C.code_bclass = '6'and C.code_sclass= 'OS'
                                                    left join tbItemCodes D on car.dept = D.code_id and D.code_bclass = '6'and D.code_sclass= 'dept'
                                                    Where 1=1
                                                    and car.car_retailer <> '' and car.car_retailer <> '1'
                                                    {0}
                                                    group by car.car_retailer, car.dept,  car.ownership,car.car_license,B.code_name , C.code_name , D.code_name 
                                                    order by Convert(int,car.car_retailer)", wherestr);

            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {

                        int ttRow = 0;    //列                    
                        int tmpRow = 0;
                        string tmpretailer = "";
                        ExcelPackage pck = new ExcelPackage();
                        var sheet1 = pck.Workbook.Worksheets.Add("分析表-應收款及其他收入");

                        #region sheet1                
                        //檔案邊界
                        sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                        sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                        sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                        sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                        sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                        sheet1.PrinterSettings.HorizontalCentered = true;

                        //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                        sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小 
                        sheet1.PrinterSettings.RepeatRows = new ExcelAddress("$1:$4");

                        //sheet1.PrinterSettings.FitToPage = true;  //集中在一頁列印
                        //sheet1.Names.AddFormula("_xlnm.Print_Titles", $"'{sheet1.Name}'!$A:$G,'{sheet1.Name}'!$1:$7");


                        //sheet1.DefaultColWidth = 6.75; // 默认所有列宽
                        //sheet1.DefaultRowHeight = 60;  // 默认所有行高



                        sheet1.Cells.Style.Font.Name = "標楷體";
                        sheet1.Cells[1, 1, 1, 7].Merge = true; //合併儲存格
                        sheet1.Cells[1, 1, 1, 7].Value = (Convert.ToDateTime(sdate.Text).Year - 1911).ToString() + "/" + Convert.ToDateTime(sdate.Text).Month.ToString() + " " + ddl_fee.SelectedItem.Text + "（應收款及其他收入）";    //Set the value of cell A1 to 1
                        sheet1.Cells[1, 1, 1, 7].Style.Font.Bold = true;
                        sheet1.Cells[1, 1, 1, 7].Style.Font.Size = 20;


                        sheet1.Cells[3, 1].Value = "使用者";
                        sheet1.Cells[3, 1, 4, 1].Merge = true; //合併儲存格
                        sheet1.Cells[3, 2].Value = "分攤部門";
                        sheet1.Cells[3, 2, 4, 2].Merge = true; //合併儲存格
                        sheet1.Cells[3, 3].Value = "所有權";
                        sheet1.Cells[3, 3, 4, 3].Merge = true; //合併儲存格
                        sheet1.Cells[3, 4].Value = "車號";
                        sheet1.Cells[3, 4, 4, 4].Merge = true; //合併儲存格
                        sheet1.Cells[3, 5].Value = "數值";
                        sheet1.Cells[3, 5, 3, 7].Merge = true; //合併儲存格
                        sheet1.Cells[4, 5].Value = "代付款";
                        sheet1.Cells[4, 6].Value = "代收款";
                        sheet1.Cells[4, 7].Value = "其他收入";
                        sheet1.Cells[1, 1, 4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                        sheet1.Cells[1, 1, 4, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                        sheet1.Cells[3, 1, 4, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[3, 1, 4, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet1.Cells[3, 1, 4, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sheet1.Cells[3, 1, 4, 7].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色

                        ttRow = 5;
                        tmpRow = 5;

                        string car_retailer = "";
                        string dept = "";
                        string ownership = "";


                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                        {
                            if (car_retailer != dt.Rows[i]["car_retailer"].ToString())
                            {
                                if (i != 0)
                                {
                                    sheet1.Cells[ttRow, 1].Value = tmpretailer + "　合計";
                                    sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 5, ttRow - 1, 5).Address);
                                    sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 6, ttRow - 1, 6).Address);
                                    sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 7, ttRow - 1, 7).Address);                                    
                                    sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                    sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                                    ttRow += 1;
                                }
                                tmpRow = ttRow;
                                tmpretailer = dt.Rows[i]["car_retailer_text"].ToString();
                                sheet1.Cells[ttRow, 1].Value = dt.Rows[i]["car_retailer_text"].ToString();
                                car_retailer = dt.Rows[i]["car_retailer"].ToString();



                            }
                            if (dept != dt.Rows[i]["dept"].ToString())
                            {
                                sheet1.Cells[ttRow, 2].Value = dt.Rows[i]["dept_name"].ToString();
                                dept = dt.Rows[i]["dept"].ToString();
                            }
                            if (ownership != dt.Rows[i]["ownership"].ToString())
                            {
                                sheet1.Cells[ttRow, 3].Value = dt.Rows[i]["ownership_text"].ToString();
                                ownership = dt.Rows[i]["ownership"].ToString();
                            }
                            sheet1.Cells[ttRow, 4].Value = dt.Rows[i]["car_license"].ToString();                            
                            sheet1.Cells[ttRow, 5].Value = Convert.ToDouble(dt.Rows[i]["buyamt_notax"]);
                            sheet1.Cells[ttRow, 6].Value = Convert.ToDouble(dt.Rows[i]["receiptamt_notax"]);
                            sheet1.Cells[ttRow, 7, ttRow, 7].Formula = "F" + ttRow.ToString() + "-E" + ttRow.ToString();
                            ttRow += 1;
                        }

                        if (tmpRow != ttRow)
                        {
                            sheet1.Cells[ttRow, 1].Value = tmpretailer + "　合計";
                            sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 5, ttRow - 1, 5).Address);
                            sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 6, ttRow - 1, 6).Address);
                            sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(tmpRow, 7, ttRow - 1, 7).Address);                            
                            sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                            sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                            ttRow += 1;
                        }
                        sheet1.Cells[ttRow, 1].Value = "總計";
                        sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 5, ttRow - 1, 5).Address);
                        sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 6, ttRow - 1, 6).Address);
                        sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(5, 7, ttRow - 1, 7).Address);                        
                        sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        sheet1.Cells[ttRow, 1, ttRow, 7].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                        sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                        sheet1.Cells[ttRow, 1, ttRow, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        //sheet1.Cells.Style.ShrinkToFit = true;//单元格自动适应大小                    

                        sheet1.Cells[5, 5, ttRow, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        sheet1.Cells[5, 5, ttRow, 5].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[5, 6, ttRow, 7].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells.AutoFitColumns();

                        //頁尾加入頁次
                        sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁，共" + ExcelHeaderFooter.NumberOfPages + "頁";
                        sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                        #endregion

                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment; filename=" + "分析表-應收款及其他收入" + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(pck.GetAsByteArray());
                        Response.End();
                    }

                }
            }
        }
            

        
    }
}