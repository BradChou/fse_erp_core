﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using NPOI.HSSF.UserModel;
using System.Text;

public partial class PriceUpload : System.Web.UI.Page
{

    /// <summary>Excel匯入DB-運價檔案 </summary>
    protected void btImport_Click(object sender, EventArgs e)
    {
        string ttErrStr = "";
        string start_city = "";
        string end_city = "";
        string class_level = "";
        string pricing_code = "";
        string plate1_price = "";
        string plate2_price = "";
        string plate3_price = "";
        string plate4_price = "";
        string plate5_price = "";
        string plate6_price = "";
        string tablename = "";
        switch (customer_type.SelectedValue)
        {
            case "1":  //區配論板
                tablename = "tbPriceSupplier";                
                break;
            case "2":  //竹運論板
                tablename = "tbPriceHCT";              
                break;
            case "3":  //自營論板
                tablename = "tbPriceBusiness";               
                break;
            case "4":  //C配
                tablename = "tbPriceCSection";               
                break;
            case "5":  //合發專用               
                tablename = "tbPriceCSectionA05";
                break;
        }

        lbMsg.Items.Clear();
        if (file02.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            //string ttFileName = ttPath + file02.PostedFile.FileName;
            string ttFileName = ttPath + file02.FileName;

            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
            {
                file02.PostedFile.SaveAs(ttFileName);
                HSSFWorkbook workbook = null;
                HSSFSheet u_sheet = null;
                string ttSelectSheetName = "";
                workbook = new HSSFWorkbook(file02.FileContent);
                try
                {
                    for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                    {
                        u_sheet = (HSSFSheet)workbook.GetSheetAt(x);
                        ttSelectSheetName = workbook.GetSheetName(x);

                        if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                        {
                            Boolean IsSheetOk = true;//工作表驗證
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                            //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                            string strRow, strInsCol, strInsVal;
                            List<StringBuilder> sb_del_list = new List<StringBuilder>();
                            List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                            StringBuilder sb_temp_del = new StringBuilder();
                            StringBuilder sb_temp_ins = new StringBuilder();

                            HSSFRow Row_title = new HSSFRow();
                            if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                            for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                            {
                                #region 初始化

                                HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列                              
                                start_city = "";
                                end_city = "";
                                class_level = "";
                                pricing_code = "";
                                plate1_price = "";
                                plate2_price = "";
                                plate3_price = "";
                                plate4_price = "";
                                plate5_price = "";
                                plate6_price = "";
                                strInsCol = "";
                                strInsVal = "";
                                strRow = (i + 1).ToString();

                                #endregion

                                #region 驗證&取值 IsSheetOk                                
                                for (int j = 0; j < Row_title.Cells.Count; j++)
                                {
                                    Boolean IsChkOk = true;
                                    switch (Row_title.GetCell(j).StringCellValue)
                                    {
                                        case "起點區域":
                                            if (Row.GetCell(j) != null) start_city = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(start_city))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            start_city = start_city.Replace("台", "臺");
                                            break;
                                        case "迄點區域":
                                            if (Row.GetCell(j) != null) end_city = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(end_city))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            end_city = end_city.Replace("台", "臺");
                                            break;
                                        case "級距":
                                            if (Row.GetCell(j) != null) class_level = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(class_level) || !IsNumeric(class_level))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "計價模式":
                                            if (Row.GetCell(j) != null) pricing_code = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(pricing_code) || !IsNumeric(pricing_code))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數1":
                                            if (Row.GetCell(j) != null) plate1_price = Row.GetCell(j).ToString().Replace(",","");
                                            if (String.IsNullOrEmpty(plate1_price) || !IsNumeric(plate1_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數2":
                                            if (Row.GetCell(j) != null) plate2_price = Row.GetCell(j).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(plate2_price) || !IsNumeric(plate2_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數3":
                                            if (Row.GetCell(j) != null) plate3_price = Row.GetCell(j).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(plate3_price) || !IsNumeric(plate3_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數4":
                                            if (Row.GetCell(j) != null) plate4_price = Row.GetCell(j).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(plate4_price) || !IsNumeric(plate4_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數5":
                                            if (Row.GetCell(j) != null) plate5_price = Row.GetCell(j).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(plate5_price) || !IsNumeric(plate5_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case "板數6":
                                            if (Row.GetCell(j) != null) plate6_price = Row.GetCell(j).ToString().Replace(",", "");
                                            if (String.IsNullOrEmpty(plate6_price) || !IsNumeric(plate6_price))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        //case "運價表":
                                        //    if (Row.GetCell(j) != null) tablename = Row.GetCell(j).ToString();
                                        //    if (String.IsNullOrEmpty(tablename))
                                        //    {
                                        //        IsChkOk =
                                        //        IsSheetOk = false;
                                        //    }
                                        //    break;
                                    }

                                    if (!IsChkOk)
                                    {
                                        lbMsg.Items.Add("請確認【" + Row_title.GetCell(j).StringCellValue + "】 第" + strRow + "列是否正確!");
                                    }
                                }

                                #endregion                              

                                #region 組刪除

                                if (i == u_sheet.FirstRowNum + 1)
                                {
                                    switch (tablename)
                                    {
                                        case "tbPriceSupplier":  //區配論板
                                            sb_temp_del.Append("delete " + tablename + " where class_level = '" + class_level + "' and pricing_code  ='" + pricing_code + "'");

                                            break;
                                        case "tbPriceHCT":  //竹運論板
                                            sb_temp_del.Append("delete " + tablename + " where pricing_code  ='" + pricing_code + "'");

                                            break;
                                        case "tbPriceBusiness":  //自營論板
                                            sb_temp_del.Append("delete " + tablename + " where pricing_code  ='" + pricing_code + "'");

                                            break;
                                        case "tbPriceCSection":  //C配
                                            sb_temp_del.Append("delete " + tablename + " where class_level = '" + class_level + "' and pricing_code  ='" + pricing_code + "'");

                                            break;
                                        case "tbPriceCSectionA05":  //合發專用
                                            sb_temp_del.Append("delete " + tablename + " where  pricing_code  ='" + pricing_code + "'");
                                            break;
                                    }
                                    sb_del_list.Add(sb_temp_del);
                                }
                                #endregion

                                #region 組新增
                                switch (tablename)
                                {
                                    case "tbPriceSupplier":  //區配論板
                                        strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                        strInsVal = "N\'" + start_city + "\', " +
                                                    "N\'" + end_city + "\', " +
                                                    "N\'" + class_level + "\', " +
                                                    "N\'" + pricing_code + "\', " +
                                                    "N\'" + plate1_price + "\', " +
                                                    "N\'" + plate2_price + "\', " +
                                                    "N\'" + plate3_price + "\', " +
                                                    "N\'" + plate4_price + "\', " +
                                                    "N\'" + plate5_price + "\', " +
                                                    "N\'" + plate6_price + "\'  ";
                                        break;
                                    case "tbPriceHCT":  //竹運論板
                                        strInsCol = "start_city,end_city,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                        strInsVal = "N\'" + start_city + "\', " +
                                                    "N\'" + end_city + "\', " +
                                                    // "N\'" + class_level + "\', " +
                                                    "N\'" + pricing_code + "\', " +
                                                    "N\'" + plate1_price + "\', " +
                                                    "N\'" + plate2_price + "\', " +
                                                    "N\'" + plate3_price + "\', " +
                                                    "N\'" + plate4_price + "\', " +
                                                    "N\'" + plate5_price + "\', " +
                                                    "N\'" + plate6_price + "\'  ";
                                        break;
                                    case "tbPriceBusiness":  //自營論板
                                        strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                        strInsVal = "N\'" + start_city + "\', " +
                                                    "N\'" + end_city + "\', " +
                                                    "N\'" + class_level + "\', " +
                                                    "N\'" + pricing_code + "\', " +
                                                    "N\'" + plate1_price + "\', " +
                                                    "N\'" + plate2_price + "\', " +
                                                    "N\'" + plate3_price + "\', " +
                                                    "N\'" + plate4_price + "\', " +
                                                    "N\'" + plate5_price + "\', " +
                                                    "N\'" + plate6_price + "\'  ";
                                        break;
                                    case "tbPriceCSection":  //C配
                                        strInsCol = "start_city,end_city,class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                        strInsVal = "N\'" + start_city + "\', " +
                                                    "N\'" + end_city + "\', " +
                                                    "N\'" + class_level + "\', " +
                                                    "N\'" + pricing_code + "\', " +
                                                    "N\'" + plate1_price + "\', " +
                                                    "N\'" + plate2_price + "\', " +
                                                    "N\'" + plate3_price + "\', " +
                                                    "N\'" + plate4_price + "\', " +
                                                    "N\'" + plate5_price + "\', " +
                                                    "N\'" + plate6_price + "\'  ";
                                        break;
                                    case "tbPriceCSectionA05":  //合發專用
                                        strInsCol = "class_level,pricing_code,plate1_price,plate2_price,plate3_price,plate4_price,plate5_price,plate6_price";
                                        strInsVal = //"N\'" + start_city + "\', " +
                                                    //"N\'" + end_city + "\', " +
                                                    "N\'" + class_level + "\', " +
                                                    "N\'" + pricing_code + "\', " +
                                                    "N\'" + plate1_price + "\', " +
                                                    "N\'" + plate2_price + "\', " +
                                                    "N\'" + plate3_price + "\', " +
                                                    "N\'" + plate4_price + "\', " +
                                                    "N\'" + plate5_price + "\', " +
                                                    "N\'" + plate6_price + "\'  ";
                                        break;
                                }

                                sb_temp_ins.Append("INSERT INTO " + tablename + "(" + strInsCol + ") VALUES(" + strInsVal + "); ");

                                #region 最後一筆同時也寫入「公版運價上傳記錄檔 ttPriceUpdateLog」
                                if (i == u_sheet.LastRowNum)
                                {                                    
                                    int i_tableType = 0;//運價表分類 (1:竹運公版 2:區配公版 3:自營公版 4:C配公版 5:C配合發)
                                    switch (tablename)
                                    {
                                        case "tbPriceSupplier":  //區配論板
                                            i_tableType = 2;
                                            break;
                                        case "tbPriceHCT":  //竹運論板
                                            i_tableType = 1;
                                            break;
                                        case "tbPriceBusiness":  //自營論板
                                            i_tableType = 3;
                                            break;
                                        case "tbPriceCSection":  //C配
                                            i_tableType = 4;
                                            break;
                                        case "tbPriceCSectionA05":  //合發專用
                                            i_tableType = 5;
                                            break;
                                        default:
                                            i_tableType = 0;
                                            break;
                                    }
                                    string user = "";
                                    if (Session["account_code"] != null) user = Session["account_code"].ToString();

                                    strInsVal = "N\'" + i_tableType.ToString() + "\', " +
                                                "N\'" + pricing_code + "\', " +
                                                "N\'" + class_level + "\', " +
                                                "N\'" + user + "\'  ";

                                    sb_temp_ins.Append("INSERT INTO ttPriceUpdateLog(tariffs_table_type, pricing_code, class_level,  cuser) VALUES(" + strInsVal + "); ");
                                }
                                #endregion

                                //每100筆組成一字串
                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }
                                #endregion
                            }

                            //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                            if (IsSheetOk)
                            {
                                #region 執行SQL

                                //刪除
                                foreach (StringBuilder sb in sb_del_list)
                                {
                                    if (sb.Length > 0)
                                    {
                                        String strSQL = sb.ToString();
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = strSQL;
                                            dbAdapter.execNonQuery(cmd);
                                        }
                                    }
                                }

                                //新增
                                foreach (StringBuilder sb in sb_ins_list)
                                {
                                    if (sb.Length > 0)
                                    {
                                        String strSQL = sb.ToString();
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = strSQL;
                                            dbAdapter.execNonQuery(cmd);
                                        }
                                    }
                                }
                                
                                #endregion

                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                            }
                            else
                            {
                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                            }
                            lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                        }

                    }
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    System.IO.File.Delete(ttPath + file02.PostedFile.FileName);
                }
                lbMsg.Items.Add("匯入完畢");
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "setTimeout('parent.$.fancybox.close()', 50000); parent.location.reload(true);", true);
            }

        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["customer_type"] != null) customer_type.SelectedValue = Request.QueryString["customer_type"].ToString();
        }
    }
}
