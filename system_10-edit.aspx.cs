﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting; 	//Chart Web 伺服器控制項的方法和屬性
using System.Drawing;                   //繪圖功能的存取
using System.Collections;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Text;
using System.Web.Services;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

public partial class system_10_edit : System.Web.UI.Page
{
    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string ApStatus
    {
        get { return ViewState["ApStatus"].ToString(); }
        set { ViewState["ApStatus"] = value; }
    }

    public int supplier_id
    {
        get { return Convert.ToInt32(ViewState["supplier_id"]); }
        set { ViewState["supplier_id"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            City.DataSource = dbAdapter.getDataTable(cmd1);
            City.DataValueField = "city";
            City.DataTextField = "city";
            City.DataBind();
            City.Items.Insert(0, new ListItem("請選擇縣市別", ""));

            city_receipt.DataSource = dbAdapter.getDataTable(cmd1);
            city_receipt.DataValueField = "city";
            city_receipt.DataTextField = "city";
            city_receipt.DataBind();
            city_receipt.Items.Insert(0, new ListItem("請選擇縣市別", ""));
            #endregion

            supplier_id = Request.QueryString["supplier_id"] != null ? Convert.ToInt32(Request.QueryString["supplier_id"]) : 0;
            lb_susid.Text = supplier_id.ToString();
            readdata(supplier_id);

            ReadFreightUpdateLog();
        }
    }

    private void readdata(int supplier_id)
    {
        if (supplier_id > 0)
        {
            ApStatus = "Modity";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@supplier_id", supplier_id);
            cmd.CommandText = " select top 1 * from  tbSuppliers where supplier_id = @supplier_id ";
            string sScript = "";
            //DateTime dt_temp = new DateTime();
            //int i_tmp;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        supplier_code.Text = dt.Rows[i]["supplier_code"].ToString();      //區配編號
                        supplier_no.Text = dt.Rows[i]["supplier_no"].ToString();           //供應商編號
                        uni_number.Text = dt.Rows[i]["uniform_numbers"].ToString();        //統一編號
                        id_no.Text = dt.Rows[i]["id_no"].ToString();                       //身分證字號
                        shortname.Text = dt.Rows[i]["supplier_shortname"].ToString();      //名稱                        
                        pri_name.Text = dt.Rows[i]["principal"].ToString();                //負責人
                        contact.Text = dt.Rows[i]["contact"].ToString();                   //聯絡人
                        tel.Text = dt.Rows[i]["telephone"].ToString();                     //電話
                        City.SelectedValue = dt.Rows[i]["city"].ToString().Trim();         //通訊地址-縣市
                        City_SelectedIndexChanged(null, null);
                        CityArea.SelectedValue = dt.Rows[i]["area"].ToString().Trim();     //通訊地址-鄉鎮市區
                        address.Text = dt.Rows[i]["road"].ToString().Trim();               //通訊地址-路街巷弄號
                        email.Text = dt.Rows[i]["email"].ToString().Trim();                //出貨人電子郵件
                        option.SelectedValue = dt.Rows[i]["receipt_tpye"].ToString();      //發票類型
                        sup_name.Text = dt.Rows[i]["supplier_name"].ToString();            //發票抬頭                        
                        receipt_number.Text = dt.Rows[i]["receipt_number"].ToString();     //發票統一編號                        
                        account_no.Text = dt.Rows[i]["account"].ToString();                //銀行帳戶
                        bank_name.Text = dt.Rows[i]["bank_name"].ToString();               //銀行/分行
                        bank_code.Text = dt.Rows[i]["bank_code"].ToString();               //收款銀行代號
                        city_receipt.SelectedValue = dt.Rows[i]["city_receipt"].ToString().Trim();         //發票寄送地址-縣市
                        CityRec_SelectedIndexChanged(null, null);
                        cityarea_receipt.SelectedValue = dt.Rows[i]["area_receipt"].ToString().Trim();     //發票寄送地址-鄉鎮市區
                        address_receipt.Text = dt.Rows[i]["road_receipt"].ToString().Trim();               //發票寄送地址-路街巷弄號
                        filename1.Text = dt.Rows[i]["id_image"].ToString().Trim();  //身分證影像
                        filename2.Text = dt.Rows[i]["driver_image"].ToString().Trim();  //駕照影像
                        filename3.Text = dt.Rows[i]["account_image"].ToString().Trim();  //存摺證影像
                        filename4.Text = dt.Rows[i]["contract_content"].ToString().Trim();  //合約內容  
                        if (Request.Url.Host.IndexOf("localhost") >= 0)
                        {
                            if (filename1.Text != "") sScript += "$('#h1FileView').attr('href','" + "../files/contract/" + filename1.Text + "');";
                            if (filename2.Text != "") sScript += "$('#h2FileView').attr('href','" + "../files/contract/" + filename2.Text + "');";
                            if (filename3.Text != "") sScript += "$('#h3FileView').attr('href','" + "../files/contract/" + filename3.Text + "');";
                            if (filename4.Text != "") sScript += "$('#h4FileView').attr('href','" + "../files/contract/" + filename4.Text + "');";
                        }
                        else
                        {
                            if (filename1.Text != "") sScript += "$('#h1FileView').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename1.Text + "');";  //正式機
                            if (filename2.Text != "") sScript += "$('#h2FileView').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename2.Text + "');";  //正式機
                            if (filename3.Text != "") sScript += "$('#h3FileView').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename3.Text + "');";  //正式機
                            if (filename4.Text != "") sScript += "$('#h4FileView').attr('href','" + "http://" + Request.Url.Authority + ":10080/files/contract/" + filename4.Text + "');";  //正式機
                        }

                    }

                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
                }

            }
            cmd.Dispose();
        }
        else
        {
            ApStatus = "Add";
        }
        SetApStatus(ApStatus);
    }

    private void ReadFreightUpdateLog()
    {
        string supplierNo = supplier_no.Text;
        string query = "SELECT TOP 10 effective_date, create_user, create_date FROM supplier_freight WHERE supplier_no = @supplierNo ORDER BY create_date DESC";

        DataTable data = new DataTable();
        using (SqlCommand cmd = new SqlCommand(query))
        {
            cmd.Parameters.AddWithValue("@supplierNo", supplierNo);
            data = dbAdapter.getDataTable(cmd);
        }

        update_log_repeater.DataSource = data;
        update_log_repeater.DataBind();
    }

    private void SetApStatus(string ApStatus)
    {
        switch (ApStatus)
        {
            case "Modity":
                supplier_code.ReadOnly = true;
                statustext.Text = "修改";
                break;
            case "Add":
                //supplier_code.ReadOnly = true;
                statustext.Text = "新增";
                break;
        }

    }
    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void CityRec_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (city_receipt.SelectedValue != "")
        {
            cityarea_receipt.Items.Clear();
            cityarea_receipt.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", city_receipt.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    cityarea_receipt.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            cityarea_receipt.Items.Clear();
            cityarea_receipt.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }
    protected void Check_Clicked(object sender, EventArgs e)
    {

        if (chkaddress.Checked)
        {
            city_receipt.SelectedValue = City.SelectedValue;
            CityRec_SelectedIndexChanged(null, null);
            cityarea_receipt.SelectedValue = CityArea.SelectedValue;
            address_receipt.Text = address.Text;
        }
        else
        {
            city_receipt.SelectedValue = "";
            cityarea_receipt.SelectedValue = "";
            address_receipt.Text = "";
        }

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {

        string ErrStr = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        switch (ApStatus)
        {
            case "Add":
                #region 判斷重覆

                if (supplier_code.Text.Trim() != "")
                {
                    SqlCommand cmda = new SqlCommand();
                    DataTable dta = new DataTable();
                    cmda.Parameters.AddWithValue("@supplier_code", supplier_code.Text);
                    cmda.CommandText = "Select supplier_id from tbSuppliers with(nolock) where supplier_code=@supplier_code ";
                    dta = dbAdapter.getDataTable(cmda);
                    if (dta.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資料已存在，請勿重覆建立');</script>", false);
                        return;
                    }
                }


                #endregion

                SqlCommand cmdadd = new SqlCommand();

                cmdadd.Parameters.AddWithValue("@supplier_code", supplier_code.Text.ToString().ToUpper());         //區配編號
                cmdadd.Parameters.AddWithValue("@supplier_no", supplier_no.Text.ToString());             //供應商編號
                cmdadd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());          //統一編號
                cmdadd.Parameters.AddWithValue("@id_no", id_no.Text.ToString().ToUpper());                         //身分證字號
                cmdadd.Parameters.AddWithValue("@supplier_shortname", shortname.Text.ToString());        //名稱
                cmdadd.Parameters.AddWithValue("@principal", pri_name.Text.ToString());                  //負責人
                cmdadd.Parameters.AddWithValue("@contact", contact.Text.ToString());                     //聯絡人
                cmdadd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                       //電話
                cmdadd.Parameters.AddWithValue("@city", City.SelectedValue.ToString());                  //通訊地址-縣市
                cmdadd.Parameters.AddWithValue("@area", CityArea.SelectedValue.ToString());              //通訊地址-鄉鎮市區
                cmdadd.Parameters.AddWithValue("@road", address.Text.ToString());                        //通訊地址-路街巷弄號
                cmdadd.Parameters.AddWithValue("@email", email.Text.ToString());                         //出貨人電子郵件

                cmdadd.Parameters.AddWithValue("@supplier_name", sup_name.Text.ToString());              //發票抬頭
                cmdadd.Parameters.AddWithValue("@receipt_number", receipt_number.Text.ToString());       //發票統一編號
                cmdadd.Parameters.AddWithValue("@account", account_no.Text.ToString());                  //銀行帳戶
                cmdadd.Parameters.AddWithValue("@bank_code", bank_code.Text.ToString());                  //收款銀行代號
                cmdadd.Parameters.AddWithValue("@bank_name", bank_name.Text.ToString());                  //銀行/分行名稱
                cmdadd.Parameters.AddWithValue("@city_receipt", city_receipt.SelectedValue.ToString());                  //發票寄送地址-縣市
                cmdadd.Parameters.AddWithValue("@area_receipt", cityarea_receipt.SelectedValue.ToString());              //發票寄送地址-鄉鎮市區
                cmdadd.Parameters.AddWithValue("@road_receipt", address_receipt.Text.ToString());                        //發票寄送地址-路街巷弄號
                if (option.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇發票類型');</script>", false);
                    return;
                }
                cmdadd.Parameters.AddWithValue("@receipt_tpye", option.SelectedValue.ToString());         //發票類型
                cmdadd.Parameters.AddWithValue("@id_image", filename1.Text.ToString());                   //身分證影像
                cmdadd.Parameters.AddWithValue("@driver_image", filename2.Text.ToString());               //駕照影像
                cmdadd.Parameters.AddWithValue("@account_image", filename3.Text.ToString());              //存摺影像
                cmdadd.Parameters.AddWithValue("@contract_content", filename4.Text.ToString());           //合約內容
                cmdadd.Parameters.AddWithValue("@active_flag", 1);                                        //啟用 
                cmdadd.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                cmdadd.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                cmdadd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdadd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmdadd.CommandText = dbAdapter.SQLdosomething("tbSuppliers", cmdadd, "insert");
                try
                {
                    dbAdapter.execNonQuery(cmdadd);
                }
                catch (Exception ex)
                {
                    ErrStr = ex.Message;
                }

                break;
            case "Modity":
                #region 修改        
                SqlCommand cmdupd = new SqlCommand();

                cmdupd.Parameters.AddWithValue("@supplier_no", supplier_no.Text.ToString());             //供應商編號
                cmdupd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());          //統一編號
                cmdupd.Parameters.AddWithValue("@id_no", id_no.Text.ToString().ToUpper());                         //身分證字號
                cmdupd.Parameters.AddWithValue("@supplier_shortname", shortname.Text.ToString());        //名稱                
                cmdupd.Parameters.AddWithValue("@principal", pri_name.Text.ToString());                  //負責人
                cmdupd.Parameters.AddWithValue("@contact", contact.Text.ToString());                     //聯絡人
                cmdupd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                       //電話
                cmdupd.Parameters.AddWithValue("@city", City.SelectedValue.ToString());                  //通訊地址-縣市
                cmdupd.Parameters.AddWithValue("@area", CityArea.SelectedValue.ToString());              //通訊地址-鄉鎮市區
                cmdupd.Parameters.AddWithValue("@road", address.Text.ToString());                        //通訊地址-路街巷弄號
                cmdupd.Parameters.AddWithValue("@email", email.Text.ToString());                         //出貨人電子郵件
                cmdupd.Parameters.AddWithValue("@receipt_tpye", option.SelectedValue.ToString());        //發票類型
                cmdupd.Parameters.AddWithValue("@supplier_name", sup_name.Text.ToString());              //發票抬頭
                cmdupd.Parameters.AddWithValue("@receipt_number", receipt_number.Text.ToString());       //發票統一編號
                cmdupd.Parameters.AddWithValue("@account", account_no.Text.ToString());                  //銀行帳戶
                cmdupd.Parameters.AddWithValue("@bank_code", bank_code.Text.ToString());                  //收款銀行代號
                cmdupd.Parameters.AddWithValue("@bank_name", bank_name.Text.ToString());                  //銀行/分行名稱
                cmdupd.Parameters.AddWithValue("@city_receipt", city_receipt.SelectedValue.ToString());                  //發票寄送地址-縣市
                cmdupd.Parameters.AddWithValue("@area_receipt", cityarea_receipt.SelectedValue.ToString());              //發票寄送地址-鄉鎮市區
                cmdupd.Parameters.AddWithValue("@road_receipt", address_receipt.Text.ToString());                        //發票寄送地址-路街巷弄號

                cmdupd.Parameters.AddWithValue("@id_image", filename1.Text.ToString());                   //身分證影像
                cmdupd.Parameters.AddWithValue("@driver_image", filename2.Text.ToString());               //駕照影像
                cmdupd.Parameters.AddWithValue("@account_image", filename3.Text.ToString());              //存摺影像
                cmdupd.Parameters.AddWithValue("@contract_content", filename4.Text.ToString());           //合約內容
                cmdupd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmdupd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 


                cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "supplier_id", Request.QueryString["supplier_id"].ToString());
                cmdupd.CommandText = dbAdapter.genUpdateComm("tbSuppliers", cmdupd);   //修改
                dbAdapter.execNonQuery(cmdupd);
                #endregion

                break;
        }

        if (ErrStr != "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ErrStr + "');</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='system_10.aspx';alert('" + statustext.Text + "完成');</script>", false);
        }


        return;
    }



    /// <summary>回傳訊息至前端 </summary>
    /// <param name="strMsg">訊息內容</param>
    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            return;

        }

    }

    protected void ImportCSV(object sender, EventArgs e)
    {
        string fileName = Server.MapPath("~/files/") + Path.GetFileName(FileUpload1.PostedFile.FileName);

        FileInfo fileInfo = new FileInfo(fileName);

        IWorkbook workBook = null;

        if (fileInfo.Extension.ToLower() != ".xls" && fileInfo.Extension.ToLower() != ".xlsx")
        {
            fileTypeWrong.Text = "匯入檔案格式錯誤!";
            return;
        }

        FileUpload1.PostedFile.SaveAs(fileName);

        if (fileInfo.Extension.ToLower() == ".xls")
        {
            workBook = new HSSFWorkbook(FileUpload1.FileContent);
        }
        else if (fileInfo.Extension.ToLower() == ".xlsx")
        {
            workBook = new XSSFWorkbook(FileUpload1.FileContent);
        }

        ISheet sheet = workBook.GetSheetAt(0);
        IFormulaEvaluator formulaEvaluator = new XSSFFormulaEvaluator(workBook); // Important!! 取公式值的時候會用到
        sheet.ForceFormulaRecalculation = true;

        DataTable table = new DataTable();

        table.Columns.Add(new DataColumn("配送區", typeof(string)));
        table.Columns.Add(new DataColumn("到著區", typeof(string)));
        table.Columns.Add(new DataColumn("A", typeof(int)));
        table.Columns.Add(new DataColumn("B1", typeof(int)));
        table.Columns.Add(new DataColumn("B2", typeof(int)));
        table.Columns.Add(new DataColumn("B3", typeof(int)));
        table.Columns.Add(new DataColumn("B4", typeof(int)));
        table.Columns.Add(new DataColumn("B5", typeof(int)));
        table.Columns.Add(new DataColumn("B6", typeof(int)));
        table.Columns.Add(new DataColumn("C1", typeof(int)));
        table.Columns.Add(new DataColumn("C2", typeof(int)));
        table.Columns.Add(new DataColumn("C3", typeof(int)));
        table.Columns.Add(new DataColumn("C4", typeof(int)));
        table.Columns.Add(new DataColumn("C5", typeof(int)));
        table.Columns.Add(new DataColumn("C6", typeof(int)));

        int cellCount = 15;
        // 標題有三行
        for (int i = (sheet.FirstRowNum + 3); i <= sheet.LastRowNum; i++)
        {
            IRow row = sheet.GetRow(i);
            if (row == null || row.GetCell(0).CellType == CellType.Blank) break;

            DataRow dataRow = table.NewRow();

            for (int j = row.FirstCellNum; j < cellCount; j++)
            {
                ICell cell = row.GetCell(j);
                if (cell != null)
                {
                    switch (cell.CellType)
                    {
                        case CellType.Numeric:
                            dataRow[j] = cell.NumericCellValue;
                            break;
                        case CellType.String:
                            dataRow[j] = cell.StringCellValue;
                            break;
                        case CellType.Blank:
                            dataRow[j] = 0;
                            break;                            
                        default:
                            break;
                    }
                }
            }
            table.Rows.Add(dataRow);
        }

        SaveSupplierFreight(table);
    }

    protected void SaveSupplierFreight(DataTable table)
    {
        string supplierNumber = supplier_no.Text;
        string date = datepicker.Text;
        int id;

        string queryInsertFreight = "INSERT INTO supplier_freight OUTPUT INSERTED.id VALUES(@createDate, @effectiveDate, @supplierNo, @createUser)";

        using (SqlCommand cmd = new SqlCommand(queryInsertFreight))
        {
            cmd.Parameters.AddWithValue("@createDate", DateTime.Now);
            cmd.Parameters.AddWithValue("@effectiveDate", date);
            cmd.Parameters.AddWithValue("@supplierNo", supplierNumber);
            cmd.Parameters.AddWithValue("@createUser", Session["account_code"]);

            id = (int)dbAdapter.getScalarBySQL(cmd);
        }

        string queryInsertDetail = "INSERT INTO supplier_freight_detail VALUES ";

        foreach (DataRow row in table.Rows)
        {

            queryInsertDetail += string.Format("('{0}', '{1}', {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}),",
                new object[] { row["配送區"], row["到著區"], row["A"], row["B1"], row["B2"], row["B3"], row["B4"], row["B5"], row["B6"],
                    row["C1"], row["C2"], row["C3"], row["C4"], row["C5"], row["C6"], id });
        }

        // 去掉最後一個逗號
        queryInsertDetail = queryInsertDetail.Substring(0, queryInsertDetail.Length - 1);

        using (SqlCommand cmd = new SqlCommand(queryInsertDetail))
        {
            dbAdapter.execQuery(cmd);
        }
    }

    protected void OpenViewWindow(object sender, EventArgs e)
    {
        string url = "system_10_edit_1.aspx?supplierNo=" + supplier_no.Text;
        string script = string.Format("<script>window.open('{0}','_blank');</script>", url);
        Response.Write(script);
    }
    protected void option_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (option.SelectedValue == "1")
        {
            receipt_number.ReadOnly = true;
            chknumber.Attributes.Add("style", "display:none");
            RequiredFieldValidator2.ValidationGroup = "";
        }
        else
        {
            receipt_number.ReadOnly = false;
            chknumber.Attributes.Add("style", "display:inline");
        }
    }
}


