﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

public partial class member_3 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            if (Request.QueryString["customer_code"] != null)
            {
                txcustomer_code.Text = Request.QueryString["customer_code"];
            }

            if (Request.QueryString["customer_shortname"] != null)
            {
                txcustomer_shortname.Text = Request.QueryString["customer_shortname"];
            }

            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            string wherestr = string.Empty;
            if (Less_than_truckload == "1") wherestr = " and code_id  = '02'";
            cmd2.CommandText = string.Format(" select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM' {0}  order by code_id asc", wherestr);
            PM_code.DataSource = dbAdapter.getDataTable(cmd2);
            PM_code.DataValueField = "code_id";
            PM_code.DataTextField = "showname";
            PM_code.DataBind();
            PM_code.Items.Insert(0, new ListItem("計價模式", ""));
            #endregion

            if (Request.QueryString["PM_code"] != null)
            {
                PM_code.SelectedValue = Request.QueryString["PM_code"];
            }

            if (Request.QueryString["Status"] != null)
            {
                Status.SelectedValue = Request.QueryString["Status"];
            }

            readdata();
        }
    }

    private DataTable getDT(string qrytype = "query")
    {
        DataTable Result = null;
        manager_type = Session["manager_type"] != null ? Session["manager_type"].ToString() : ""; //管理單位類別   
        supplier_code = "";
        master_code = "";
        string strWhereCmd = "";
        string station = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();



        if (Less_than_truckload == "0")
        {
            if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
            {
                supplier_code = Session["master_code"].ToString();
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000"; //峻富
                            break;
                    }
                }
            }
            else
            {
                master_code = Session["master_code"].ToString();
            }
        }
        else
        {
            switch (manager_type)
            {
                case "0":
                case "1":
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                        if (station_level.Equals("1"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else
                        {
                            supplier_code = "";
                        }
                    }
                    //}else
                    //{
                    //    supplier_code = "";
                    //}
                    break;
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmda.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmda.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmda.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                        if (station_level.Equals("1"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmda.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    supplier_code += "'";
                                    supplier_code += dt.Rows[i]["station_scode"].ToString();
                                    supplier_code += "'";
                                    supplier_code += ",";
                                }
                                supplier_code = supplier_code.Remove(supplier_code.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5"))
                        {
                            cmda.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code += "'";
                                supplier_code += dt.Rows[0]["station_scode"].ToString();
                                supplier_code += "'";
                            }
                        }
                        else
                        {
                            cmda.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmda);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code += "'";
                                supplier_code += dt.Rows[0]["station_scode"].ToString();
                                supplier_code += "'";
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        cmda.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmda.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmda);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code += "'";
                            supplier_code += dt.Rows[0]["station_scode"].ToString();
                            supplier_code += "'";
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.Clear();

            #region 關鍵字
            cmd.Parameters.AddWithValue("@type", Less_than_truckload);

            if (supplier_code != "")
            {
                /*
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                strWhereCmd += " and A.supplier_code in (@supplier_code)";
                */
                if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
                {
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                    strWhereCmd += " and A.station_scode in (" + supplier_code + ")";
                }
            }

            if (master_code != "")
            {
                cmd.Parameters.AddWithValue("@master_code", master_code);
                strWhereCmd += " and A.customer_code like ''+@master_code+'%' ";
            }

            if (txcustomer_code.Text != "")
            {
                cmd.Parameters.AddWithValue("@customer_code", txcustomer_code.Text);
                strWhereCmd += " and A.customer_code like '%'+@customer_code+'%' ";

            }

            if (txcustomer_shortname.Text != "")
            {
                cmd.Parameters.AddWithValue("@customer_shortname", txcustomer_shortname.Text);
                strWhereCmd += " and A.customer_shortname like '%'+@customer_shortname+'%' ";
            }

            if (PM_code.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue);
                strWhereCmd += " and A.pricing_code =@pricing_code";

            }

            if (Status.SelectedValue == "0")
            {
                cmd.Parameters.AddWithValue("@stop_shipping_code", Status.SelectedValue);
                strWhereCmd += " and A.stop_shipping_code =@stop_shipping_code";
            }
            else if (Status.SelectedValue == "1")
            {
                cmd.Parameters.AddWithValue("@stop_shipping_code", "0");
                strWhereCmd += " and A.stop_shipping_code <> @stop_shipping_code";
            }
            /*
            if (station_level.Equals("1"))
            {
                strWhereCmd += " and sale_station in (" + supplier_code + ")";
            }
            else if (station_level.Equals("2"))
            {
                strWhereCmd += " and sale_station in (" + supplier_code + ")";
            }
            else if (station_level.Equals("4"))
            {
                strWhereCmd += " and sale_station in (" + supplier_code + ")";
            }
            else
            {


            }

            */



            #endregion

            if (qrytype == "query")
            {
                cmd.CommandText = string.Format(@"select A.customer_id , A.customer_code, A.supplier_code ,B.supplier_name ,  customer_shortname ,
                                          A.shipments_city ,A.shipments_area , A.shipments_road , A.stop_shipping_code,
                                          CONVERT (DATETIME, A.contract_expired_date, 102) AS contract_expired_date,
                                          D.user_name , A.udate, A.type 
                                          from tbCustomers A with(nolock)
                                          left outer join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                          left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                          where A.customer_code <> '' and type=@type {0} order by A.customer_code", strWhereCmd);
            }
            else
            {
                if (Less_than_truckload == "0")
                {
                    cmd.CommandText = string.Format(@"select  A.customer_code, A.supplier_code+'-'+ B.supplier_name , A.customer_name , A.customer_shortname as customer_shortname ,
                                                  A.shipments_city +A.shipments_area + A.shipments_road , 
                                                  A.uniform_numbers ,
                                                  A.telephone ,
                                                  A.fax ,
                                                  A.shipments_email ,
                                                  A.shipments_principal ,
                                                  A.business_people ,
                                                  CASE A.pricing_code WHEN '01' THEN '01-論板' WHEN '02' THEN '02-論件' WHEN '03' THEN '03-論才' WHEN '04' THEN '04-論小板' WHEN '05' THEN '05-專車' ELSE '' END AS pricing_type,
                                                  CASE WHEN LEN(A.invoice_road)>0 THEN A.invoice_city +A.invoice_area + A.invoice_road ELSE A.shipments_city +A.shipments_area + A.shipments_road END AS invoice_address,
                                                  A.ticket_period ,
                                                  A.checkout_date ,
                                                  A.billing_special_needs ,
                                                  CASE WHEN CONVERT (DATETIME, A.contract_effect_date, 102)  > CONVERT (DATETIME, '1900/01/01', 102) THEN CONVERT(VARCHAR(10), A.contract_effect_date,111)  END AS contract_effect_date,
                                                  CASE WHEN CONVERT (DATETIME, A.contract_expired_date, 102)  > CONVERT (DATETIME, '1900/01/01', 102) THEN CONVERT(VARCHAR(10), A.contract_expired_date,111)  END AS contract_expired_date                                                
                                                  from tbCustomers A
                                                  left outer join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                  where A.customer_code <> '' and type=@type {0} order by A.customer_code", strWhereCmd);
                }
                else
                {
                    cmd.CommandText = string.Format(@"select  
                                                A.customer_code, s.station_name, A.customer_name as customer_shortname, A.uniform_numbers,A.shipments_principal,A.telephone,A.shipments_email,A.shipments_city +A.shipments_area + A.shipments_road , 
                                                CASE WHEN CONVERT (DATETIME, A.contract_expired_date, 102)  > CONVERT (DATETIME, '1900/01/01', 102) THEN A.contract_expired_date  END AS contract_expired_date,
                                                A.assigned_sd, A.assigned_md, A.product_type,
                                                CASE WHEN dn.begin_number is not null then 
	                                                cast(dn.begin_number as varchar) + '~' + cast(dn.end_number as varchar) else
	                                                cast(dnos.begin_number as varchar) + '~' + cast(dnos.end_number as varchar) end as check_number_range,
                                                A.invoice_area + A.invoice_city + A.invoice_road as invoice_address,
                                                sf.no1_bag, sf.no2_bag, sf.no3_bag, sf.no4_bag, sf.s60_cm, sf.s90_cm, sf.s110_cm, sf.s120_cm, s150_cm, sf.piece_rate, sf.holiday,
                                                '' as LY, bank, bank_branch,bank_account,A.cdate, case is_new_customer when '0' then '舊' when'1' then '新' else'' end as 'is_new_customer',
												case OldCustomerOverCBM when '1' then '舊' when '0' then '新' else'' end as 'OldCustomerOverCBM', MasterCustomerCode
                                                from tbCustomers A
                                                left outer join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                left join tbDeliveryNumberSetting dn on A.customer_code = dn.customer_code 
                                                left join tbDeliveryNumberOnceSetting dnos on A.customer_code = dnos.customer_code 
                                                left join CustomerShippingFee sf on A.customer_code = sf.customer_code 
                                                left join tbStation s on A.supplier_code = s.station_code 
                                                where A.customer_code <> '' and type=@type {0} order by A.customer_code", strWhereCmd);
                }
            }

            Result = dbAdapter.getDataTable(cmd);
        }
        return Result;
    }
    private void readdata()
    {
        string querystring = "";
        querystring += "&customer_code=" + txcustomer_code.Text;
        querystring += "&customer_shortname=" + txcustomer_shortname.Text;
        querystring += "&PM_code=" + PM_code.SelectedValue;
        querystring += "&Status=" + Status.SelectedValue;


        //SqlConnection conn = null;
        //String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        //conn = new SqlConnection(strConn);
        //SqlDataAdapter adapter = null;
        //SqlCommand cmd = new SqlCommand();



        //supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
        //if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
        //{
        //    switch (manager_type)
        //    {
        //        case "1":
        //            supplier_code = "";
        //            break;
        //        default:
        //            supplier_code = "000";
        //            break;
        //    }
        //}

        //cmd.Connection = conn;
        //DataSet ds = new DataSet();
        //adapter = new SqlDataAdapter(cmd);
        //adapter.Fill(ds);
        //PagedDataSource objPds = new PagedDataSource();
        //objPds.DataSource = ds.Tables[0].DefaultView;
        //objPds.AllowPaging = true;
        //objPds.PageSize = 10;

        DataTable dt = getDT();
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = dt.DefaultView;
        objPds.AllowPaging = true;
        objPds.PageSize = 10;

        #region 下方分頁顯示
        int sumlistpage = 9;
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Visible = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Visible = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Visible = true;
        }
        else
        {
            lnklast.Visible = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Visible = false;
        }
        else
        {
            lnkPrev.Visible = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Visible = false;
        }
        else
        {
            lnkNext.Visible = true;
        }


        //跑分頁前五個
        for (int j = (CurPage - 5); j <= (CurPage - 1); j++)
        {

            if (j <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)) && j > 0)
            {
                pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + j.ToString() + querystring + "'>" + j.ToString() + "</a></li>"));
                sumlistpage = sumlistpage - 1;
            }

        }

        //跑分頁後面剩餘數，共十個
        for (int i = CurPage; i <= (CurPage + sumlistpage); i++)
        {
            if (i == CurPage)
            {
                pagelist.Text += "<li class='active'><a href='#'>" + CurPage.ToString() + "</a></li>";
            }
            else
            {
                if (i <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    pagelist.Text += Server.HtmlDecode(Server.HtmlEncode("<li><a href='" + Request.CurrentExecutionFilePath.ToString() + "?Page=" + i.ToString() + querystring + "'>" + i.ToString() + "</a></li>"));
                }

            }
        }

        #endregion


        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = dt.Rows.Count.ToString();//總筆數

        //New_List.DataSource = objPds;
        //New_List.DataBind();
        //ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }
    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }
    private void paramlocation()
    {
        string querystring = "";
        if (txcustomer_code.Text.ToString() != "")
        {
            querystring += "&customer_code=" + txcustomer_code.Text.ToString();
        }
        if (txcustomer_shortname.Text.ToString() != "")
        {
            querystring += "&customer_shortname=" + txcustomer_shortname.Text.ToString();
        }

        if (PM_code.SelectedValue != "")
        {
            querystring += "&PM_code=" + PM_code.SelectedValue;
        }

        querystring += "&Status=" + Status.SelectedValue;
        Response.Redirect(ResolveUrl("~/member_3.aspx?search=yes" + querystring));
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        #region 匯出excel樣版
        string ttServerPath = this.Server.MapPath(".") + "\\files\\";

        //string wherestr = "";
        //SqlCommand objCommand = new SqlCommand();
        Hashtable hash = new Hashtable();
        DataSet ds = new DataSet();

        DataTable dt = getDT("export");
        dt.TableName = "客戶資料";

        #region 調整Table內容     

        if (Less_than_truckload == "1")
        {
            // 取得公版運價
            DataTable dtCommonShippingFee = Utility.GetCommonShippingFee();
            DataRow drCommonShippingFee = dtCommonShippingFee.Rows[0];

            foreach (DataRow row in dt.Rows)
            {
                if ((row["no1_bag"] == DBNull.Value) || (row["no1_bag"].ToString() == ""))
                {
                    row["no1_bag"] = drCommonShippingFee["no1_bag"];
                    row["no2_bag"] = drCommonShippingFee["no2_bag"];
                    row["no3_bag"] = drCommonShippingFee["no3_bag"];
                    row["no4_bag"] = drCommonShippingFee["no4_bag"];
                    row["s60_cm"] = drCommonShippingFee["s60_cm"];
                    row["s90_cm"] = drCommonShippingFee["s60_cm"];
                    row["s110_cm"] = drCommonShippingFee["s60_cm"];
                    row["s120_cm"] = drCommonShippingFee["s60_cm"];
                    row["s150_cm"] = drCommonShippingFee["s60_cm"];
                    row["holiday"] = drCommonShippingFee["holiday"];
                    row["piece_rate"] = drCommonShippingFee["piece_rate"];
                }
            }
        }

        #endregion

        ds.Tables.Add(dt);
        #region 製作匯出

        if (Less_than_truckload == "0")
        {
            reportAdapter.npoiSampleToExcel(ttServerPath, ds, hash, "客戶資料", "客戶資料匯出");
        }
        else
        {
            reportAdapter.npoiSampleToExcel(ttServerPath, ds, hash, "客戶資料-零擔", "客戶資料匯出");
        }

        #endregion
        #endregion
    }

    protected string CustomerCodeDisplay(string customer_code, string type = "0")
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            if (type == "0")
            {
                Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
            }
            else
            {
                Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 2), customer_code.Substring(9));
            }

        }
        return Retval;
    }
}