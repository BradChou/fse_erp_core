﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTreport_4_1 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;
            date2.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string station_level = Session["station_level"].ToString(); //層級
            string management = Session["management"].ToString();  //level1
            string station_scode = Session["station_scode"].ToString();  //level2                                                       
            string station_area = Session["station_area"].ToString();  //level4

            string station_areaList = "";
            string station_scodeList = "";
            string managementList = "";

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶

                    if (station_level == "1")  //management
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@management", management);
                            cmd.CommandText = @"select station_scode from tbStation where management=@management";
                            DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    managementList += "'";
                                    managementList += dt.Rows[i]["station_scode"].ToString();
                                    managementList += "'";
                                    managementList += ",";
                                }
                                managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            }

                        }

                    }
                    else if (station_level == "2")  //station_scode
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@station_scode", station_scode);
                            cmd.CommandText = @"select station_scode from tbStation where station_scode=@station_scode";
                            DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    station_scodeList = "'";
                                    station_scodeList += dt.Rows[i]["station_scode"].ToString();
                                    station_scodeList += "'";
                                    station_scodeList += ",";
                                }
                                station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            }
                        }

                    }
                    else if (station_level == "4")  //station_area
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@station_area", station_area);
                            cmd.CommandText = @"select station_scode from tbStation where station_area=@station_area";
                            DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    station_areaList = "'";
                                    station_areaList += dt.Rows[i]["station_scode"].ToString();
                                    station_areaList += "'";
                                    station_areaList += ",";
                                }
                                station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            }
                        }

                    }

                    else if (station_level == "5")  //全部
                    {
                        supplier_code = "";
                    }
                    else
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_scode   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_scode"].ToString();
                            }
                        }
                    }

                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_scode"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                //var station = "";
                //station = management;
                SqlCommand cmdstatiom = new SqlCommand();
                //cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_code, station_scode + '-' +station_name as showname from tbStation where station_scode in ('" + supplier_code + "')");

                Suppliers.DataSource = dbAdapter.getDataTable(cmdstatiom);
                Suppliers.DataValueField = "station_code";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                if (station_level != "2")
                {
                    Suppliers.Items.Insert(0, new ListItem("全部", ""));
                }

            }
            else if (supplier_code == "" && station_level.Equals("1"))
            {
                //var station = "";
                //station = management;
                SqlCommand cmdstatiom = new SqlCommand();
                //cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_scode, station_scode + '-' +station_name as showname from tbStation where station_scode in (" + managementList + ")");

                Suppliers.DataSource = dbAdapter.getDataTable(cmdstatiom);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && station_level.Equals("2"))
            {
                //var station = "";
                //station = management;
                SqlCommand cmdstatiom = new SqlCommand();
                //cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_scode, station_scode + '-' +station_name as showname from tbStation where station_scode in (" + station_scodeList + ")");

                Suppliers.DataSource = dbAdapter.getDataTable(cmdstatiom);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && station_level.Equals("4"))
            {
                //var station = "";
                //station = management;
                SqlCommand cmdstatiom = new SqlCommand();
                //cmdstatiom.Parameters.AddWithValue("@station", station);
                cmdstatiom.CommandText = string.Format(@"
select station_scode, station_scode + '-' +station_name as showname from tbStation where station_scode in (" + station_areaList + ")");

                Suppliers.DataSource = dbAdapter.getDataTable(cmdstatiom);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }


            else if (supplier_code == "" && manager_type == "2")
            {
                cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}
                                               order by station_code", wherestr);

                Suppliers.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
                Suppliers_SelectedIndexChanged(null, null);
                Suppliers.Items.Insert(0, new ListItem("全部", ""));
            }
            else
            {
                cmd1.CommandText = string.Format(@"Select id, station_scode, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}
                                               order by station_code", wherestr);

                Suppliers.DataSource = dbReadOnlyAdapter.getDataTable(cmd1);
                Suppliers.DataValueField = "station_scode";
                Suppliers.DataTextField = "showname";
                Suppliers.DataBind();
                if (Suppliers.Items.Count > 0) Suppliers.SelectedIndex = 0;
                Suppliers_SelectedIndexChanged(null, null);

                if (manager_type == "0" || manager_type == "1")
                {
                    Suppliers.Items.Insert(0, new ListItem("全部", ""));
                }
            }
            #endregion


            #region 
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.Parameters.AddWithValue("@type", "1");
                if (manager_type == "5")
                {
                    cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                    cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock) where A.customer_code = @customer_code");
                }
                else
                {
                    if (Suppliers.SelectedValue != "")

                    {
                        cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                        wherestr = " and A.station_scode = @supplier_code ";
                    }
                    else
                    {

                        if (station_level == "1")
                        {
                            wherestr = " and A.station_scode in (" + managementList + ")";
                            //wherestr = " and A.station_scode in ('" + supplier_code + "')";
                        }
                        else if (station_level.Equals("2"))
                        {
                            wherestr = " and A.station_scode in (" + station_scodeList + ")";
                        }
                        else if (station_level.Equals("4"))
                        {
                            wherestr = " and A.station_scode in (" + station_areaList + ")";
                        }

                        else { }  //全部
                    }
                    cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    Left join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 
                                                    and A.stop_shipping_code = '0' and A.type =@type {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
                }

                second_code.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
                second_code.DataValueField = "customer_code";
                second_code.DataTextField = "name";
                second_code.DataBind();
                if (manager_type == "0" || manager_type == "1" || manager_type == "2") second_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
                //second_code.Items.Insert(0, new ListItem("", ""));
            }

            #endregion 

            issendcontact.Items.Insert(0, new ListItem("全部", ""));
            issendcontact.Items.Insert(1, new ListItem("未銷單", "1"));
            issendcontact.Items.Insert(2, new ListItem("已銷單", "2"));
            issendcontact.Items.Insert(3, new ListItem("無到著站", "3"));

            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["date2"] != null)
            {
                if (Request.QueryString["date2"] != "")
                {
                    date2.Text = Request.QueryString["date2"];
                }
            }

            if (Request.QueryString["Suppliers"] != null || Request.QueryString["Suppliers"] != "")
            {

                Suppliers.SelectedValue = Request.QueryString["Suppliers"];
                Suppliers_SelectedIndexChanged(null, null);
            }

            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue = Request.QueryString["second_code"];
            }

            if (Request.QueryString["issendcontact"] != null)
            {
                if (Request.QueryString["issendcontact"] != "")
                {
                    issendcontact.SelectedValue = Request.QueryString["issendcontact"];
                }
            }
            if (Request.QueryString["DeliveryType"] != null)
            {
                if (Request.QueryString["DeliveryType"] != "")
                {
                    if (Request.QueryString["DeliveryType"].ToString() == "D")
                    {
                        DeliveryType1.Checked = true;
                        DeliveryType2.Checked = false;
                    }
                    else
                    {
                        DeliveryType1.Checked = false;
                        DeliveryType2.Checked = true;
                    }
                }
            }
            if (Request.QueryString["Check_number"] != null)
            {
                if (Request.QueryString["Check_number"] != "")
                {
                    strCheck_number.Text = Request.QueryString["Check_number"];
                }
            }

            SqlCommand cmds = new SqlCommand();
            string sqlwhere = "";

            cmds.CommandTimeout = 600;
            if (issendcontact.SelectedValue == "2")
            {
                sqlwhere = " AND A.cancel_date is not null ";
            }
            else if (issendcontact.SelectedValue == "1")
            {
                sqlwhere = " AND A.cancel_date is null ";
            }
            else if (issendcontact.SelectedValue == "3")
            {
                sqlwhere = "  AND A.area_arrive_code = ''   ";
            }

            if (Suppliers.SelectedValue != "")
            {
                sqlwhere = "  AND cus.station_scode = @supplier_code   ";
            }
            else
            {

                if (station_level.Equals("1"))
                {
                    sqlwhere = "  AND cus.station_scode in (" + managementList + ")";

                }
                else if (station_level.Equals("2"))
                {
                    sqlwhere = "  AND cus.station_scode in (" + station_scodeList + ")";


                }
                else if (station_level.Equals("4"))
                {
                    sqlwhere = "  AND cus.station_scode in (" + station_areaList + ")";


                }



            }


            cmds.CommandText = string.Format(@"
            SELECT count(A.request_id)
			FROM tcDeliveryRequests A With(Nolock) 
		    LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
LEFT JOIN tbCustomers cus With(nolock) on cus.customer_code  = A.customer_code
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			  AND A.print_date IS NOT NULL
			  AND A.Less_than_truckload= 1 
             AND A.pricing_type in ('02')
             AND A.DeliveryType =  @DeliveryType
             and (@customer_code = '' or A.customer_code = @customer_code)
             --and (@supplier_code = '' or A.supplier_code = @supplier_code)
             and (@Check_number = '' or A.Check_number= @Check_number)
			 AND ( _arr.arrive_state is  null and A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R')  )
            {0}
            ", sqlwhere);
            cmds.Parameters.AddWithValue("@Check_number", strCheck_number.Text);
            cmds.Parameters.AddWithValue("@print_dates", date1.Text);
            cmds.Parameters.AddWithValue("@print_datee", date2.Text);
            cmds.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);

            if (DeliveryType1.Checked)
            {
                cmds.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
            }
            else
            {
                cmds.Parameters.AddWithValue("@DeliveryType", "R");
            }
            cmds.Parameters.AddWithValue("@customer_code", second_code.SelectedValue);
            DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
            if (dtsum.Rows.Count > 0)
            {
                lbSuppliers.Text = Suppliers.SelectedValue;
                totle.Text = dtsum.Rows[0][0].ToString() + "筆";
            }

            if (Request.QueryString["date1"] != null)
            {
                readdata();
            }
        }
    }

    private void readdata()
    {
        string sqlwhere = "";
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);

            //cmd.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
            if (DeliveryType1.Checked)
            {
                cmd.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
                querystring += "&DeliveryType=" + "D";//rdDeliveryType.SelectedValue.ToString()
            }
            else
            {
                cmd.Parameters.AddWithValue("@DeliveryType", "R");
                querystring += "&DeliveryType=" + "R";//rdDeliveryType.SelectedValue.ToString()
            }

            cmd.Parameters.AddWithValue("@Check_number", strCheck_number.Text);//rdDeliveryType.SelectedValue

            querystring += "&Check_number=" + strCheck_number.Text;

            sqlwhere += @"AND A.DeliveryType =  @DeliveryType";



            if (issendcontact.SelectedValue != "")
            {
                if (issendcontact.SelectedValue == "1")
                    sqlwhere += @" AND A.cancel_date is null ";
                else if (issendcontact.SelectedValue == "2")
                    sqlwhere += @" AND A.cancel_date is not null  ";
                else if (issendcontact.SelectedValue == "3")
                    sqlwhere += @" AND A.area_arrive_code = ''  ";
            }

            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND cus.station_scode  =   @supplier_id ";
                //sqlwhere += @" AND A.supplier_code  =   @supplier_id ";
            }

            else
            {
                var station_level = Session["station_level"].ToString();

                string station_areaList = "";
                string station_scodeList = "";
                string managementList = "";
                cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());


                if (station_level == "1")
                {
                    cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            managementList += "'";
                            managementList += dt1.Rows[i]["station_scode"].ToString();
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }
                    sqlwhere += @" AND cus.station_scode  in  (" + managementList + ") ";
                }
                else if (station_level.Equals("2"))
                {
                    cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_scodeList += "'";
                            station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }
                    sqlwhere += @" AND cus.station_scode  in  (" + station_scodeList + ") ";
                }
                else if (station_level.Equals("3"))
                {
                    cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                    DataTable dt1 = dbAdapter.getDataTable(cmd);
                    if (dt1 != null && dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            station_areaList += "'";
                            station_areaList += dt1.Rows[i]["station_scode"].ToString();
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }
                    sqlwhere += @" AND cus.station_scode  in  (" + station_areaList + ") ";
                }
                else { cmd.Parameters.AddWithValue("@supplier_id", DBNull.Value); }

            }
            querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                sqlwhere += @" AND A.customer_code =  @customer_code  ";
            }
            querystring += "&second_code=" + second_code.SelectedValue.ToString();


            if (date1.Text.ToString() != "")
            {
                querystring += "&date1=" + date1.Text.ToString();
            }

            if (date2.Text.ToString() != "")
            {
                querystring += "&date2=" + date2.Text.ToString();
            }
            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";



            querystring += "&rb_pricing_type=" + pricing_type;
            querystring += "&issendcontact=" + issendcontact.SelectedValue;

            cmd.CommandText = string.Format(@"
declare @table table
(
check_number nvarchar(50),
put_order nvarchar(20)
)
insert into @table
select distinct check_number,put_order from  JunFuTrans.[dbo].[check_number_sd_mapping]
where  check_number!='' and put_order!='' and  CONVERT(VARCHAR,cdate,111) >= CONVERT(VARCHAR,@print_dates,111) 

            SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
            A.request_id,
			A.print_date '發送日期', 
			CONVERT(CHAR(10),A.supplier_date,111) '配送日期',  
			A.check_number '明細貨號' , 
			A.order_number '訂單編號' 
			,sta1.station_name  '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數',
			CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收件人地址'
			,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end '寄件件人地址'
			,A.area_arrive_code  '站所代碼',G.station_name '站所名稱',
			B.code_name '傳票區分',
			ISNULL(A.collection_money,0) '代收金額',
			A.invoice_desc '備註',
            A.ShuttleStationCode '接駁碼',
			SpecCodeId,
			--S110以下、1才內 MD
			Case when SpecCodeId in ('S120','S150','C002','C003','C004','C005') then ReceiveSD
			when SpecCodeId is null or SpecCodeId='' then ReceiveMD
			else ReceiveMD end as '司機碼' ,
			put_order '堆疊區'

			,_arr.arrive_state '配達狀況'
			,_arr.arrive_date '配達時間'
			,_arr.arrive_item '配送異常說明',
            A.cancel_date,
            A.check_number as logNum
            FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
            LEFT JOIN tbCustomers cus With(nolock) on cus.customer_code  = A.customer_code
            LEFT JOIN @table T  on T.check_number  = A.check_number
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			  AND A.print_date IS NOT NULL
			  AND A.Less_than_truckload= 1 
              AND A.check_number!=''
              AND (@Check_number = '' or A.Check_number= @Check_number) 
            AND ( _arr.arrive_state is  null and A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R') )
            {0}
            ", sqlwhere);
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {

                SqlCommand cmds = new SqlCommand();
                string wherestr = "";
                string sqlwhere1 = "";


                if (issendcontact.SelectedValue != "")
                {
                    if (issendcontact.SelectedValue == "1")
                        sqlwhere1 += @" AND A.cancel_date is null ";
                    else if (issendcontact.SelectedValue == "2")
                        sqlwhere1 += @" AND A.cancel_date is not null  ";
                    else if (issendcontact.SelectedValue == "3")
                        sqlwhere1 += @"  AND A.area_arrive_code = ''   ";
                }
                cmds.CommandTimeout = 600;
                wherestr += dbReadOnlyAdapter.genWhereCommIn(ref cmds, "pricing_type", "02".Split(','));
                cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code in (@supplier_id) OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            AND (@Check_number = '' or A.Check_number = @Check_number)
            AND ( _arr.arrive_state is  null and A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R') )
            {0}
            {1}
                                                 )
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr, sqlwhere1);

                cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                if (DeliveryType1.Checked)
                {
                    cmds.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
                }
                else
                {
                    cmds.Parameters.AddWithValue("@DeliveryType", "R");
                }
                cmds.Parameters.AddWithValue("@Check_number", strCheck_number.Text);
                cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                if (Suppliers.SelectedValue != "")
                {
                    cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue);
                }
                else
                {
                    var station = "";
                    if (Session["station_level"].ToString() == "1")  //management
                    {
                        using (SqlCommand cmdl = new SqlCommand())
                        {
                            cmdl.Parameters.AddWithValue("@management", Session["management"].ToString());
                            cmdl.CommandText = @"select station_code from tbStation where management=@management";
                            DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                            if (dtl != null && dtl.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtl.Rows.Count; i++)
                                {
                                    station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                }

                            }

                            station = station.Substring(0, station.Length - 1);
                            cmds.Parameters.AddWithValue("@supplier_id", station);
                        }
                    }
                    else if (Session["station_level"].ToString() == "2")  //station_scode
                    {
                        using (SqlCommand cmdl = new SqlCommand())
                        {
                            cmdl.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                            cmdl.CommandText = @"select station_code from tbStation where station_scode=@station_scode";
                            DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                            if (dtl != null && dtl.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtl.Rows.Count; i++)
                                {
                                    station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                }

                            }
                            station = station.Substring(0, station.Length - 1);
                            cmds.Parameters.AddWithValue("@supplier_id", station);
                        }

                    }
                    else if (Session["station_level"].ToString() == "4")  //station_area
                    {
                        using (SqlCommand cmdl = new SqlCommand())
                        {
                            cmdl.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                            cmdl.CommandText = @"select station_code from tbStation where station_area=@station_area";
                            DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                            if (dtl != null && dtl.Rows.Count > 0)
                            {
                                for (int i = 0; i < dtl.Rows.Count; i++)
                                {
                                    station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                }

                            }
                            station = station.Substring(0, station.Length - 1);
                            cmds.Parameters.AddWithValue("@supplier_id", station);
                        }

                    }
                    else { cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value); }

                }

                if (second_code.SelectedValue != "")
                {
                    string customer_code = second_code.SelectedValue.Trim();
                    cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                }
                else
                {
                    cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                }

                DataTable dtsum = dbReadOnlyAdapter.getDataTable(cmds);
                ltotalpages.Text = dt.Rows.Count.ToString();
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.NewRow();
                    row["配送區域"] = "總計";
                    row["件數"] = dtsum.Rows[0]["件數"].ToString();

                    dt.Rows.Add(row);
                }

                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }


    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }

        if (date2.Text.ToString() != "")
        {
            querystring += "&date2=" + date2.Text.ToString();
        }

        querystring += "&Suppliers=" + Suppliers.SelectedValue.ToString();
        querystring += "&second_code=" + second_code.SelectedValue.ToString();
        querystring += "&issendcontact=" + issendcontact.SelectedValue;
        if (DeliveryType1.Checked)
        {
            querystring += "&DeliveryType=" + "D";
        }
        else
        {
            querystring += "&DeliveryType=" + "R";
        }
        querystring += "&Check_number=" + strCheck_number.Text;

        Response.Redirect(ResolveUrl("~/LTreport_4_1.aspx?search=yes" + querystring));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void New_List_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        string sheet_title = "EDI訂單管理表";
        string sqlwhere = "";
        string file_name = "EDI訂單管理表" + DateTime.Now.ToString("yyyyMMdd");
        using (SqlCommand cmd = new SqlCommand())
        {
            if (issendcontact.SelectedValue != "")
            {
                if (issendcontact.SelectedValue == "1")
                    sqlwhere += @" AND A.cancel_date is null ";
                else if (issendcontact.SelectedValue == "2")
                    sqlwhere += @" AND A.cancel_date is not null  ";
                else if (issendcontact.SelectedValue == "3")
                    sqlwhere += @"  AND A.area_arrive_code = ''   ";
            }

            if (Suppliers.SelectedValue != "")
            {
                cmd.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());
                sqlwhere += @" AND cus.station_scode  =   @supplier_id ";
            }
            else
            {
                var station_level = Session["station_level"].ToString();
                if (station_level == "1" || station_level == "2" || station_level == "4")
                {

                    sqlwhere += @" AND A.supplier_code  in  ('" + supplier_code + "') ";
                }
                else { cmd.Parameters.AddWithValue("@supplier_id", DBNull.Value); }

            }

            if (second_code.SelectedValue != "")
            {
                string customer_code = second_code.SelectedValue.Trim();
                cmd.Parameters.AddWithValue("@customer_code", customer_code + "%");  //客代
                sqlwhere += @" AND A.customer_code like  @customer_code  ";
            }

            cmd.Parameters.AddWithValue("@Check_number", strCheck_number.Text);
            string pricing_type = "02";

            cmd.Parameters.AddWithValue("@pricing_type", pricing_type);
            sqlwhere += @" AND A.pricing_type in ( @pricing_type ) ";


            //cmd.CommandText = "usp_GetLTDistributionReportD";
            cmd.CommandText = string.Format(@"
declare @table table
(
check_number nvarchar(50),
put_order nvarchar(20)
)
insert into @table
select distinct check_number,put_order from  JunFuTrans.[dbo].[check_number_sd_mapping]
where  check_number!='' and put_order!='' and  CONVERT(VARCHAR,cdate,111) >= CONVERT(VARCHAR,@print_dates,111) 

SELECT ROW_NUMBER() OVER(ORDER BY A.print_date desc) AS NO ,
			A.print_date '發送日期', 
			CONVERT(CHAR(10),A.supplier_date,111) '配送日期',  
			A.check_number '明細貨號' , 
			A.order_number '訂單編號' 
			,sta1.station_name  '發送站'
			,A.send_contact'寄件人',
			A.receive_contact '收件人'
			,CASE WHEN (ISNULL(A.receive_tel1_ext,'')='') THEN A.receive_tel1 ELSE A.receive_tel1 + ' #' + A.receive_tel1_ext END '收貨人電話號碼'
			,A.receive_city '配送縣市別', 
			A.receive_area '配送區域',
			A.cbmWeight '重量',
			A.pieces'件數',
			CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收件人地址'
			,CASE WHEN A.receive_by_arrive_site_flag = '1' then A.receive_address else send_address end '寄件件人地址'
			,A.area_arrive_code  '站所代碼',G.station_name '站所名稱',
			B.code_name '傳票區分',
			ISNULL(A.collection_money,0) '代收金額',
			A.invoice_desc '備註',
            A.ShuttleStationCode '接駁碼',
			SpecCodeId,
			--S110以下、1才內 MD
			Case when SpecCodeId in ('S120','S150','C002','C003','C004','C005') then ReceiveSD
			when SpecCodeId is null or SpecCodeId='' then ReceiveMD
			else ReceiveMD end as '司機碼' ,
			put_order '堆疊區'
			FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code
            LEFT JOIN tbCustomers cus With(nolock) on cus.customer_code  = A.customer_code
            LEFT JOIN @table T  on T.check_number  = A.check_number
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= 1 
            AND A.DeliveryType= @DeliveryType 
            AND A.check_number!=''
            and (@Check_number = '' or A.Check_number = @Check_number)
            {0} 
            AND (_arr.arrive_state is null and A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R'))
",
            sqlwhere);
            cmd.CommandTimeout = 600;
            //cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            cmd.Parameters.AddWithValue("@print_dates", date1.Text);
            cmd.Parameters.AddWithValue("@print_datee", date2.Text);
            //cmd.Parameters.AddWithValue("@DeliveryType", "D");//cmd.Parameters.AddWithValue("@DeliveryType", s.SelectedValue);
            if (DeliveryType1.Checked)
            {
                cmd.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
            }
            else
            {
                cmd.Parameters.AddWithValue("@DeliveryType", "R");
            }


            using (DataTable dt = dbReadOnlyAdapter.getDataTable(cmd))
            {
                if (dt != null && dt.Rows.Count > 0)
                {

                    SqlCommand cmds = new SqlCommand();
                    string wherestr = "";
                    string sqlwhere1 = "";


                    if (issendcontact.SelectedValue != "")
                    {
                        if (issendcontact.SelectedValue == "1")
                            sqlwhere1 += @" AND A.cancel_date is null ";
                        else if (issendcontact.SelectedValue == "2")
                            sqlwhere1 += @" AND A.cancel_date is not null  ";
                        else if (issendcontact.SelectedValue == "3")
                            sqlwhere1 += @" AND  A.area_arrive_code = ''   ";
                    }

                    cmds.CommandTimeout = 600;
                    pricing_type = "'02'";
                    wherestr += dbReadOnlyAdapter.genWhereCommIn(ref cmds, "pricing_type", pricing_type.Split(','));

                    cmds.CommandText = string.Format(@"WITH TOTAL AS
                                                 (
SELECT  CASE pricing_type WHEN '01' THEN plates ELSE 0  END 'plates'  , pieces ,  cbm , CASE pricing_type WHEN '04' THEN plates ELSE 0 END 'splates' 
FROM tcDeliveryRequests A With(Nolock) 
			LEFT JOIN tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
			LEFT JOIN tbStation G With(Nolock) ON A.area_arrive_code = G.station_scode
			LEFT JOIN tbStation sta1 With(nolock) on sta1.station_code  = A.supplier_code	
			CROSS APPLY dbo.fu_GetDeliverInfo(A.request_id) _arr
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.print_date,111) >= CONVERT(VARCHAR,@print_dates,111) and CONVERT(VARCHAR,A.print_date,111) <= CONVERT(VARCHAR,@print_datee,111)
			AND A.print_date IS NOT NULL
			AND A.Less_than_truckload= @Less_than_truckload
            AND A.DeliveryType= @DeliveryType
            AND (A.supplier_code in (@supplier_id) OR @supplier_id IS NULL)
            AND (A.customer_code = @customer_code OR @customer_code IS NULL)
            and (@Check_number = '' or A.Check_number = @Check_number)
            AND (_arr.arrive_state is  null  and A.check_number not like '990%' and not (A.customer_code = 'F3500010002' and A.DeliveryType = 'R'))
            {0}
            {1}
                                                 )
SELECT ISNULL(SUM( plates),0) + ISNULL(SUM(splates),0)  AS '棧板數', ISNULL(SUM(pieces),0) '件數', ISNULL(SUM(cbm),0)  '才數', ISNULL(SUM(splates),0) '小板數' FROM TOTAL", wherestr, sqlwhere1);

                    cmds.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                    //cmds.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue

                    if (DeliveryType1.Checked)
                    {
                        cmds.Parameters.AddWithValue("@DeliveryType", "D");//rdDeliveryType.SelectedValue
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@DeliveryType", "R");
                    }
                    cmds.Parameters.AddWithValue("@Check_number", strCheck_number.Text);

                    cmds.Parameters.AddWithValue("@print_dates", date1.Text);
                    cmds.Parameters.AddWithValue("@print_datee", date2.Text);
                    if (Suppliers.SelectedValue != "")
                    {
                        cmds.Parameters.AddWithValue("@supplier_id", Suppliers.SelectedValue.ToString());

                    }
                    else
                    {
                        var station = "";
                        if (Session["station_level"].ToString() == "1")  //management
                        {
                            using (SqlCommand cmdl = new SqlCommand())
                            {
                                cmdl.Parameters.AddWithValue("@management", Session["management"].ToString());
                                cmdl.CommandText = @"select station_code from tbStation where management=@management";
                                DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                                if (dtl != null && dtl.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtl.Rows.Count; i++)
                                    {
                                        station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                    }
                                }

                                station = station.Substring(0, station.Length - 1);
                                cmds.Parameters.AddWithValue("@supplier_id", station);
                            }
                        }
                        else if (Session["station_level"].ToString() == "2")  //station_scode
                        {
                            using (SqlCommand cmdl = new SqlCommand())
                            {
                                cmdl.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());
                                cmdl.CommandText = @"select station_code from tbStation where station_scode=@station_scode";
                                DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                                if (dtl != null && dtl.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtl.Rows.Count; i++)
                                    {
                                        station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                    }

                                }
                                station = station.Substring(0, station.Length - 1);
                                cmds.Parameters.AddWithValue("@supplier_id", station);
                            }

                        }
                        else if (Session["station_level"].ToString() == "4")  //station_area
                        {
                            using (SqlCommand cmdl = new SqlCommand())
                            {
                                cmdl.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                                cmdl.CommandText = @"select station_code from tbStation where station_area=@station_area";
                                DataTable dtl = dbReadOnlyAdapter.getDataTable(cmdl);
                                if (dtl != null && dtl.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtl.Rows.Count; i++)
                                    {
                                        station += "'" + dtl.Rows[i]["station_code"].ToString() + "'" + ",";
                                    }

                                }
                                station = station.Substring(0, station.Length - 1);
                                cmds.Parameters.AddWithValue("@supplier_id", station);
                            }

                        }
                        else { cmds.Parameters.AddWithValue("@supplier_id", DBNull.Value); }

                    }

                    if (second_code.SelectedValue != "")
                    {
                        string customer_code = second_code.SelectedValue.Trim();
                        cmds.Parameters.AddWithValue("@customer_code", customer_code);  //客代
                    }
                    else
                    {
                        cmds.Parameters.AddWithValue("@customer_code", DBNull.Value);

                    }


                    DataTable dts = dbReadOnlyAdapter.getDataTable(cmds);
                    if (dts != null && dts.Rows.Count > 0)
                    {
                        DataRow row = dt.NewRow();
                        row["配送區域"] = "總計";
                        row["件數"] = dts.Rows[0]["件數"].ToString();

                        dt.Rows.Add(row);
                    }

                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 2, 2 + dt.Rows.Count, 3])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();
                    }
                }
            }

        }

    }

    protected void Suppliers_SelectedIndexChanged(object sender, EventArgs e)
    {
        string wherestr = "";
        #region 
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@type", "1");
            if (manager_type == "5")
            {
                cmd.Parameters.AddWithValue("@customer_code", Session["customer_code"].ToString());
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock) where A.customer_code = @customer_code");
            }
            else
            {
                if (Suppliers.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", Suppliers.SelectedValue);
                    wherestr = " and A.station_scode = @supplier_code ";
                }
                else
                {
                    var station_level = Session["station_level"].ToString();
                    string station = Session["management"].ToString();
                    string station_area = Session["station_area"].ToString();
                    string station_scode = Session["station_scode"].ToString();

                    string station_areaList = "";
                    string station_scodeList = "";
                    string managementList = "";

                    cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                    cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                    cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                    if (station_level == "1")  //1、4的全部
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                        DataTable dt1 = dbAdapter.getDataTable(cmd);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt1.Rows.Count; i++)
                            {
                                managementList += "'";
                                managementList += dt1.Rows[i]["station_scode"].ToString();
                                managementList += "'";
                                managementList += ",";
                            }
                            managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                            wherestr = " and A.station_scode in (" + managementList + ")";
                        }
                        
                    }
                    else if (station_level == "2")
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                        DataTable dt1 = dbAdapter.getDataTable(cmd);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt1.Rows.Count; i++)
                            {
                                station_scodeList += "'";
                                station_scodeList += dt1.Rows[i]["station_scode"].ToString();
                                station_scodeList += "'";
                                station_scodeList += ",";
                            }
                            station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                            wherestr = " and A.station_scode in (" + station_scodeList + ")";
                        }
                    }
                    else if (station_level == "4")
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                        DataTable dt1 = dbAdapter.getDataTable(cmd);
                        if (dt1 != null && dt1.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt1.Rows.Count; i++)
                            {
                                station_areaList += "'";
                                station_areaList += dt1.Rows[i]["station_scode"].ToString();
                                station_areaList += "'";
                                station_areaList += ",";
                            }
                            station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                            wherestr = " and A.station_scode in (" + station_areaList + ")";
                        }
                    }

                    else { }  //全部
                }
                cmd.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    Left join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 
                                                    and A.stop_shipping_code = '0' and A.type =@type {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
            }

            second_code.DataSource = dbReadOnlyAdapter.getDataTable(cmd);
            second_code.DataValueField = "customer_code";
            second_code.DataTextField = "name";
            second_code.DataBind();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2") second_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            //second_code.Items.Insert(0, new ListItem("", ""));
        }
        #endregion
    }
}