﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;

public partial class forget : System.Web.UI.Page
{
    ///// <summary>
    ///// 寄信標題
    ///// </summary>
    //static string mailTitle = ConfigurationManager.AppSettings["mailTitle"].Trim();

    ///// <summary>
    ///// 寄信人Email
    ///// </summary>
    //static string sendMail = ConfigurationManager.AppSettings["sendMail"].Trim();
    ///// <summary>
    ///// 收信人Email(多筆用逗號隔開)
    ///// </summary>
    //static string receiveMails = ConfigurationManager.AppSettings["receiveMails"].Trim();

    ///// <summary>
    ///// 寄信smtp server
    ///// </summary>
    //static string smtpServer = ConfigurationManager.AppSettings["smtpServer"].Trim();

    ///// <summary>
    ///// 寄信smtp server的Port，預設25
    ///// </summary>
    //static int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"].Trim());

    ///// <summary>
    ///// 寄信帳號
    ///// </summary>
    //static string mailAccount = ConfigurationManager.AppSettings["mailAccount"].Trim();

    ///// <summary>
    ///// 寄信密碼
    ///// </summary>
    //static string mailPwd = ConfigurationManager.AppSettings["mailPwd"].Trim();


    private class CustomercodeInfo
    {

        public string Email { get; set; }
        public string Phone { get; set; }

    }


    private CustomercodeInfo GetInfo(string Customercode)
    {
        CustomercodeInfo info = new CustomercodeInfo();
        string Email = string.Empty;
        string Phone = string.Empty;

        try
        {

            SqlCommand cmd2 = new SqlCommand();
            DataTable dt2;
            cmd2.Parameters.AddWithValue("@customer_code", account.Text.ToString().Trim());

            cmd2.CommandText = " Select TOP (1) * from [tbCustomers] where customer_code = @customer_code ";
            dt2 = dbAdapter.getDataTable(cmd2);
            if (dt2.Rows.Count > 0)
            {
                var shipments_email = dt2.Rows[0]["shipments_email"].ToString();
                var telephone = dt2.Rows[0]["telephone"].ToString();
                Email = string.IsNullOrEmpty(shipments_email) ? string.Empty : shipments_email;
                Phone = string.IsNullOrEmpty(telephone) ? string.Empty : telephone;
            }

            if (string.IsNullOrEmpty(Email))
            {
                SqlCommand cmd = new SqlCommand();
                DataTable dt;
                cmd.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());

                cmd.CommandText = " Select TOP (1) * from tbAccounts where account_code = @account_code AND active_flag = 1";
                dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    var user_email = dt.Rows[0]["user_email"].ToString();
                    Email = string.IsNullOrEmpty(user_email) ? string.Empty : user_email;
                }
            }
            info.Phone = Phone.Trim();
            info.Email = Email.Trim();

            return info;
        }
        catch (Exception)
        {

            return null;
        }

    }

    protected void btnsend_Click2(object sender, EventArgs e)
    {
        string Email = string.Empty;

        string Phone = string.Empty;

        if (string.IsNullOrEmpty(account.Text))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入帳號。');</script>", false);
            return;

        }

        var Info = GetInfo(account.Text);

        if (Info == null || (string.IsNullOrEmpty(Info.Email) && string.IsNullOrEmpty(Info.Phone)))
        {
            Label5.Text = "無提示";
            return;
        }

        Email = Info.Email;
        Phone = Info.Phone;

        bool telcheck = Regex.IsMatch(Phone, @"^09[0-9]{8}$");//規則:09開頭，後面接著8

        if (!string.IsNullOrEmpty(Email))
        {

            Label5.Text = "信箱: " + character(Email);

        }
        if (telcheck)
        {

            Label6.Text = "電話: " + character(Phone);

        }
    }


    private string character(string s)
    {
        string f = string.Empty;

        var len = s.Length;
        var len5 = s.Length / 5;


        var aStringBuilder = new StringBuilder(s);
        aStringBuilder.Remove(len5, len5 * 2);

        string mi = string.Empty;

        for (int i = 0; i < len5 * 2; i++)
        {
            mi += "x";
        }

        aStringBuilder.Insert(len5, mi);
        s = aStringBuilder.ToString();



        return s;
    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        string randPwd = "";
        do
        {
            randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
        } while (!System.Text.RegularExpressions.Regex.IsMatch(randPwd,
                                                           "\\d+[A-F]+\\d+[A-F]"));
        //具有兩個以上的英文字母，並且中間夾數字才算合格
        //將第二個英文字母變小寫
        int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
        randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

        MD5 md5 = MD5.Create();//建立一個MD5
                               //byte[] source = Encoding.Default.GetBytes(newGuid_Id);//將字串轉為Byte[]
                               //byte[] crypto = md5.ComputeHash(source);//進行MD5加密
                               //string result = Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
        string result = Utility.GetMd5Hash(md5, randPwd);

        string C = account.Text;

        try
        {
            //則一發送
            if (string.IsNullOrEmpty(phone.Text) && !string.IsNullOrEmpty(email.Text))
            {
                var Info = GetInfo(C);

                if (Info != null && email.Text.Equals(Info.Email))
                {
                    var E = Info.Email;

                    string body = "【全速配】" + randPwd + " 為您的新密碼，請使用該密碼登入。建議登入後進行密碼修改，以利下次登入使用。<br>全速配關心您";
                    var bb = Utility.Mail_Send(ConfigurationManager.AppSettings["Support_sendmail"], new[] { E }, null, "峻富雲端物流管理系統忘記密碼通知", body, true, null);

                    using (SqlCommand cmdInsert = new SqlCommand())
                    {
                        cmdInsert.Parameters.AddWithValue("@CustomerCode", C);
                        cmdInsert.Parameters.AddWithValue("@Email", E);
                        cmdInsert.CommandText = dbAdapter.SQLdosomething("ForgotPasswordLog", cmdInsert, "insert");
                        dbAdapter.execNonQuery(cmdInsert);

                    }
                    if (!bb)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.location.reload();parent.$.fancybox.close();alert('發送失敗稍後再試。');</script>", false);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('信箱有誤。');</script>", false);
                    return;
                }

            }
            else if (string.IsNullOrEmpty(email.Text) && !string.IsNullOrEmpty(phone.Text))
            {
                //連續發送 15 分鐘一次
                using (SqlCommand cmda = new SqlCommand())
                {
                    DateTime dateTime = DateTime.Now;

                    DataTable dta = new DataTable();
                    cmda.Parameters.AddWithValue("@CustomerCode", C);
                    cmda.CommandText = @"SELECT *  FROM [dbo].[ForgotPasswordLog] WITH (NOLOCK) WHERE CustomerCode = @CustomerCode AND [CreateTime] > '" + dateTime.AddMinutes(-15).ToString("yyyy-MM-dd HH:mm:ss") + @"'";
                    dta = dbAdapter.getDataTable(cmda);
                    if (dta.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('15分鐘後再試試。');</script>", false);
                        return;
                    }

                }



                var Info = GetInfo(account.Text);

                if (Info != null && phone.Text.Equals(Info.Phone))
                {
                    var P = Info.Phone;
                    string body = "【全速配】" + randPwd + " 為您的新密碼，請使用該密碼登入。建議登入後進行密碼修改，以利下次登入使用。全速配關心您";
                    Utility.SMS_Send("1", P, "峻富雲端物流管理系統忘記密碼通知", body, C, "");
                    using (SqlCommand cmdInsert = new SqlCommand())
                    {
                        cmdInsert.Parameters.AddWithValue("@CustomerCode", C);
                        cmdInsert.Parameters.AddWithValue("@Telephone", P);
                        cmdInsert.CommandText = dbAdapter.SQLdosomething("ForgotPasswordLog", cmdInsert, "insert");
                        dbAdapter.execNonQuery(cmdInsert);

                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('電話有誤。');</script>", false);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入一個信箱或一個電話，則一。');</script>", false);
                return;
            }
        }
        catch (Exception)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('發送失敗稍後再試。');</script>", false);
            return;
        }


        using (SqlCommand cmdupd = new SqlCommand())
        {
            cmdupd.Parameters.AddWithValue("@password", result);
            cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_code", account.Text.ToString().Trim());
            cmdupd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmdupd);
            dbAdapter.execNonQuery(cmdupd);
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.location.reload();parent.$.fancybox.close();alert('密碼已寄出。');</script>", false);
        return;

        //    SqlCommand cmd = new SqlCommand();
        //    DataTable dt;
        //    cmd.Parameters.AddWithValue("@account_code", account.Text.ToString().Trim());
        //    cmd.Parameters.AddWithValue("@user_email", email.Text.ToString());
        //    cmd.CommandText = " Select * from tbAccounts where account_code = @account_code  and user_email=@user_email and user_email <>''";
        //    dt = dbAdapter.getDataTable(cmd);
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {


        //            string body = "您好，您的新密碼為【" + randPwd + "】。<br>" + ConfigurationManager.AppSettings["systemurl"] + "<br><br><br> 請注意：此郵件是系統自動傳送，請勿直接回覆！";
        //            Utility.Mail_Send(ConfigurationManager.AppSettings["MailSender"], new[] { dt.Rows[i]["user_email"].ToString() }, null, "峻富雲端物流管理系統忘記密碼通知", body, true, null);

        //            SqlCommand cmdupd = new SqlCommand();
        //            cmdupd.Parameters.AddWithValue("@password", result);
        //            cmdupd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "account_code", account.Text.ToString().Trim());
        //            cmdupd.CommandText = dbAdapter.genUpdateComm("tbAccounts", cmdupd);
        //            dbAdapter.execNonQuery(cmdupd);

        //            var httpClient = new HttpClient() { BaseAddress = new Uri(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"]) };
        //            var parameters = new Dictionary<string, string> { { "UsernameAccountCode", account.Text.ToString().Trim() }, { "Password", randPwd } };
        //            string jsonStr = JsonConvert.SerializeObject(parameters);
        //            HttpContent contentPost = new StringContent(jsonStr, Encoding.UTF8, "application/json");
        //            try
        //            {
        //                //Post http callas.  
        //                HttpResponseMessage response = httpClient.PostAsync(ConfigurationManager.AppSettings["UpdateFSEShopPasswordUri"], contentPost).Result;
        //                //nesekmes atveju error..
        //                response.EnsureSuccessStatusCode();
        //                //responsas to string
        //                string responseBody = response.Content.ReadAsStringAsync().Result;

        //                if (responseBody != "null")
        //                {
        //                    Console.WriteLine("True");
        //                }
        //                else
        //                {
        //                    Console.WriteLine("False");
        //                }
        //            }
        //            catch (HttpRequestException a)
        //            {
        //                Console.WriteLine("\nException Caught!");
        //                Console.WriteLine("Message :{0} ", a.Message);

        //            }

        //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>self.parent.location.reload();parent.$.fancybox.close();alert('密碼已寄到您的信箱。');</script>", false);
        //            return;

        //        }
        //    }
        //    else
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('您的帳號和信箱不符，請重新輸入，如有問題，請聯繫系統管理員');</script>", false);
        //        return;
        //    }
    }

}