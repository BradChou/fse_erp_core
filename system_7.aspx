﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_7.aspx.cs" Inherits="system_7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            //$("#resetclick").click(function () {
            //    if (confirm('您確認要重置該司機的APP密碼嗎?') == true) {
            //        $("#resetclick").fancybox({
            //            'width': 350,
            //            'height': 300,
            //            'autoScale': false,
            //            'transitionIn': 'none',
            //            'transitionOut': 'none',
            //            'type': 'iframe',
            //        });
            //    }
            //    else {
            //        return false;
            //    }
            //});
        });
        $.fancybox.update();

        function CarLocation(LastLat, LastLon) {
            window.open('http://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q=' + LastLat + ',' + LastLon + '&z=16&output=embed&t=', '_blank',
                'height=500,width=500,directories=0,location=0,menubar=0,resizable=0,status=0,titlebar=0,toolbar=0');
        }
    </script>

    <style type="text/css">
        .td_th {
            text-align: center;
        }

        .td_no, .td_yn {
            width: 80px;
        }

        .tb_edit {
            width: 60%;
        }

        ._edit_title {
            width: 13%;
        }

        ._edit_data {
            width: 37%;
            padding: 5px;
            padding-left: 25px;
        }

        input[type=radio] {
            display: inline-block;
        }
        .radio label {
            margin-right: 15px;
            text-align :left ;
        }
        .radio{
            padding-left:5px;
        }

        ._edit_data.form-control {
            width: 60% !important;
            margin: 5px 3px;
            padding: 5px;
        }

        .table_list tr:hover {
            background-color: lightyellow;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
            margin-left: -10px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="templatemo-content-widget white-bg">
        <h2 class="margin-bottom-10">
            <asp:Label ID="lbl_title" Text="司機基本資料" runat="server"></asp:Label>
        </h2>



        <asp:Panel ID="pan_view" runat="server">
            <div class="div_view">
                <!-- 查詢 -->
                <div class="form-group form-inline">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 70%;">
                                <label>查詢條件：</label>
                                <asp:TextBox ID="tb_search" runat="server" class="form-control" placeholder="ex: 司機代碼、姓名"></asp:TextBox>
                                <%--<asp:DropDownList ID="ddl_supplier" runat="server" CssClass="form-control" ToolTip="所屬區配商"></asp:DropDownList>--%>
                                <asp:DropDownList ID="ddl_station" runat="server" CssClass="form-control" ToolTip="所屬站所"></asp:DropDownList>
                                <asp:DropDownList ID="ddl_active" runat="server" CssClass="form-control" ToolTip="是否生效"></asp:DropDownList>
                                <asp:Button ID="btn_search" runat="server" Text="查 詢" class="btn btn-primary" OnClick="btn_search_Click" />
                                <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1"   />
                            </td>
                            <td style="width: 30%; float: right;">
                                <asp:Button ID="btn_Add" runat="server" Text="新 增" class="btn btn-warning" OnClick="btn_Add_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- 查詢 end -->
                <!--內容-list -->

                <table class="table table-striped table-bordered templatemo-user-table table_list">
                    <tr>
                        <th class="td_th td_no">No</th>
                        <th class="td_th td_empcode">司機代碼</th>
                        <th class="td_th td_name">司機姓名</th>
                        <th class="td_th td_job">所屬區配商</th>
                        <th class="td_th td_app">手機</th>
                        <th class="td_th td_yn">員工編號 </th>
                        <th class="td_th td_yn">類型 </th>
                        <th class="td_th td_yn">編碼 </th>
                        <th class="td_th td_yn">登入時間 </th>
                        <th class="td_th td_yn">是否生效 </th>
                        <th class="td_th td_yn">經度 </th>
                        <th class="td_th td_yn">緯度 </th>
                        <th class="td_th">更新人員</th>
                        <th class="td_th">更新日期</th>
                        <th class="td_th td_edit">功　　能</th>
                    </tr>
                    <asp:Repeater ID="list_customer" runat="server" OnItemCommand="list_customer_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="td_data td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.num").ToString())%></td>
                                <td class="td_data td_empcode" data-th="司機代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                                <td class="td_data td_name" data-th="司機姓名"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                                <td class="td_data td_job" data-th="所屬區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                <td class="td_data td_app" data-th="手機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_mobile").ToString())%></td>
                                <td class="td_data td_app" data-th="員工編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.emp_code").ToString())%></td>
                                <td class="td_data td_yn" data-th="類型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_type").ToString())%></td>
                                <td class="td_data td_yn" data-th="編碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_type_code").ToString())%></td>
                                <td class="td_data td_yn" data-th="登入時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.login_date").ToString())%></td>
                                <td class="td_data td_yn" data-th="是否生效"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.IsActive").ToString())%></td>
                                <td class="td_data td_yn" data-th="經度"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.wgs_x").ToString())%></td>
                                <td class="td_data td_yn" data-th="緯度"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.wgs_y").ToString())%></td>
                                <td class="td_data " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td class="td_data " ><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                <td class="td_data td_edit" data-th="編輯">
                                    <asp:HiddenField ID="emp_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_id").ToString())%>' />
                                    <asp:Button ID="btn_mod" CssClass="btn btn-info " CommandName="Mod" runat="server" Text="編 輯" />
                                    <%--<asp:Button ID="btn_reset" CssClass="btn btn-info " CommandName="reset" runat="server" Text="重置密碼" OnClientClick="return confirm('您確認要重置該司機的APP密碼嗎?');"  />--%>
                                    <a href="changeAppPw.aspx?driver_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_id").ToString())%>"class="fancybox fancybox.iframe"  id="resetclick"><span class="btn btn-primary">重置APP密碼</span></a>
                                    <%--<span class="btn btn-success  " onclick="CarLocation( <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.wgs_y").ToString())%>,<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.wgs_x").ToString())%> )">車輛位置</span>--%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (list_customer.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="9" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    總筆數: <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>
                </div>
                <!--內容-end -->
            </div>
        </asp:Panel>

        <asp:Panel ID="pan_edit" runat="server" CssClass="div_edit" Visible="False">
            <hr />
            <asp:Label runat="server" ID="lbl_app_login_date" Visible="False" class="text-primary"></asp:Label>
            <table class="tb_edit">
                <tr class="form-group">
                    <th class="_edit_title">
                        <label class="_tip_important">司機代碼</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:TextBox ID="driver_code" runat="server" class="form-control" placeholder="ex: J0000" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_driver_code" runat="server" ControlToValidate="driver_code" ForeColor="Red" ValidationGroup="validate">請輸入司機代碼</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="reg_driver_code_type" runat="server" ErrorMessage="司機代碼格式有誤" ValidationExpression="(^[A-Za-z0-9]{3,20}$)"
                            ControlToValidate="driver_code" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                        </asp:RegularExpressionValidator>
                    </td>
                    <th>
                        <label class="_tip_important">所屬區配商</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:DropDownList ID="ddl_supplier_edit" runat="server" CssClass="form-control" ToolTip="所屬區配商!" AutoPostBack="True" OnSelectedIndexChanged="ddl_supplier_edit_SelectedIndexChanged"></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="req_supplier_edit" runat="server" ControlToValidate="ddl_supplier_edit" ForeColor="Red" ValidationGroup="validate">請選擇所屬區配商</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th class="_edit_title">
                        <label class="_tip_important">司機姓名</label>
                    </th>
                    <td class="_edit_data form-inline ">
                        <asp:TextBox ID="driver_name" runat="server" class="form-control" placeholder="ex: 王小明" MaxLength="20"></asp:TextBox>                        
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_driver_name" runat="server" ControlToValidate="driver_name" ForeColor="Red" ValidationGroup="validate">請輸入司機姓名</asp:RequiredFieldValidator>
                        <asp:DropDownList ID="ddl_driver_name" runat="server" CssClass="form-control" ToolTip="司機姓名!" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="ddl_driver_name_SelectedIndexChanged"></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="reg_ddl_driver_name" runat="server" ControlToValidate="ddl_driver_name" ForeColor="Red" ValidationGroup="validate">請選擇司機姓名</asp:RequiredFieldValidator>
                    </td>
                    <th>
                        <label>手機</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:TextBox ID="driver_mobile" runat="server" class="form-control" placeholder="ex: 0900000000" MaxLength="15"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="手機格式不正確!" ValidationExpression="(^[09]{2}[0-9]{8}$)|(^[0-9]{11,13}$)"
                            ControlToValidate="driver_mobile" ValidationGroup="validate" Display="Dynamic" ForeColor="Red">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label class="_tip_important">是否生效：請至人員基本資料修改</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:RadioButtonList ID="rb_Active" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" Enabled="false">
                            <asp:ListItem Selected="True" Value="1">是</asp:ListItem>
                            <asp:ListItem Value="0">否</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <th class="text-right">
                        <asp:Label runat="server" ID="lbl_emp_code" Text="員工編號"></asp:Label>
                    </th>
                    <td class="_edit_data">
                        <asp:Label ID="emp_code" runat="server"></asp:Label>
                    </td>
                    <th></th>
                </tr>
                <tr>
                    <th>
                        <label class="_tip_important">是否為外車</label>
                    </th>
                    <td class="_edit_data form-inline">
                        <asp:RadioButtonList ID="rb_external_driver" runat="server"  RepeatDirection="Horizontal" CssClass="radio radio-success">
                            <asp:ListItem Value="1">是</asp:ListItem>
                            <asp:ListItem Selected="True" Value="0">否</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <th class="text-right">
                       <asp:Label ID="lbstation" runat="server" Text="所屬站所：請至人員基本資料修改" Visible ="false"  ></asp:Label>
                    </th>
                    <td class="_edit_data">
                        <asp:DropDownList ID="ddl_staion" runat="server" CssClass="form-control" Visible ="false" Enabled="false" OnSelectedIndexChanged="ddl_staion_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                    <th></th>
                </tr>
                <tr>
                    <th>
                        <asp:Label ID="lbSDMDTitle" runat="server" Text="集區SD/MD"  Width="150px"></asp:Label>
                    </th>
                    <td class="SD_MD">
                        <asp:RadioButtonList ID="RadioButtonListSDMD" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" Style="left: 0px; top: 0px;" OnSelectedIndexChanged="RadioSDMD" AutoPostBack="true">
                            <asp:ListItem Value="0">SD</asp:ListItem>
                            <asp:ListItem Value="1">MD</asp:ListItem>
                        </asp:RadioButtonList>
                    <asp:DropDownList ID="SDMDList" runat="server" CssClass="form-control"  Width="100px"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <th>
                       <asp:Label ID="checkout_vendor_label" runat="server" Text="結帳區配商" Visible ="false"  ></asp:Label>
                    </th>
                    <td class="_edit_data">
                        <asp:DropDownList ID="checkout_vendor" runat="server" CssClass="form-control" ToolTip="結帳區配商!" AutoPostBack="True"></asp:DropDownList>
                        <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddl_supplier_edit" ForeColor="Red" ValidationGroup="validate">請選擇所屬區配商</asp:RequiredFieldValidator>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="4" style="text-align: center;">
                        <asp:Button ID="btn_OK" runat="server" Text="確 認" class="btn btn-primary" OnClick="btn_OK_Click" ValidationGroup="validate" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Cancel" runat="server" Text="取 消" class="btn btn-default" OnClick="btn_Cancel_Click" />
                        <asp:Label ID="lbl_id" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>
</asp:Content>

