﻿<%@Page Title="" Language="C#" MasterPageFile="~/LTMasterReturn.master" AutoEventWireup="true"  CodeFile="LT_returnDispatch.aspx.cs" Inherits="LT_returnDispatch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="modal fade" id="modal-default_01" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <div class="alert alert-danger fade in alert-dismissable div_err" style="display: none;">
                                <span id="_err"></span>
                            </div>
                            <div class="form-horizontal">
                                 <div class="box box-primary box-solid">
                                        <div class="box-header with-border">
                                            明細資料
                                            <!-- /.box-tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">

                                            <div class="bs-callout bs-callout-info">
                                                <h3>1.</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                            <label >客戶代號：</label>
                                                               <asp:Label ID="lbcustomer_code" runat="server" Text="" />
                                                            <label>所屬站所：</label>
                                                               <asp:Label ID="lbarea_arrive_code" runat="server" Text="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>2. 原訂單</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                        <div class="form-group">
                                                               <label >訂單號碼：</label>
                                                                <asp:Label ID="lbcheck_number" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>3. 收貨人</h3>
                                                <div class="rowform">
                                                    <div class="row form-inline">
                                                       <div class="form-group">
                                                <label>收貨人：</label>
                                                 <asp:Label ID="lbSendcontact" runat="server" Text=""></asp:Label>   

                                                <label >收貨電話：</label>
                                                 <asp:Label ID="lbsendtel" runat="server" Text=""></asp:Label>  
                                                <label >收貨地址：</label>
                                                 <asp:Label ID="lbsendaddr" runat="server" Text=""></asp:Label>   
                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bs-callout bs-callout-info">
                                                <h3>4. 退貨派遣</h3>
                                                <div class="rowform">
                                                         <div class="form-group">
                                                <label >退貨人：</label>
                                                 <asp:TextBox ID="tbreceivecontact" runat="server"></asp:TextBox>   
                                                <label >退貨人電話：</label>
                                                <asp:TextBox ID="tbreceivetel" runat="server"></asp:TextBox>   
                                                             <table>
                                                                 <tr><td><label >退貨人地址：</label> </td>
                                                                      <td><asp:TextBox ID="tbzip" Width="80" class="form-control" runat="server"></asp:TextBox> </td>
                                                                       <td><asp:TextBox ID="tbreceive_city"  Width="80" class="form-control" runat="server"></asp:TextBox> </td>
                                                                       <td> <asp:TextBox ID="tbreceive_area" Width="80" class="form-control" runat="server"></asp:TextBox></td>
                                                                       <td><asp:TextBox ID="tbreceive_address" Width="180" class="form-control" runat="server"></asp:TextBox> </td>
                                                                 </tr>
                                                                 <tr>
                                                                     <td>
                                                                    <label >備註：</label>
                                                                         </td>  <td colspan ="4">
                                                                                <asp:TextBox ID="tbinvoice_desc" runat="server" TextMode="multiline"></asp:TextBox>  
                                                                             </td>  
                                            
                                                    </tr>
                                                             </table>
                                                </div>
                                          
                                                </div>
                                            </div>
                                           
                                           
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                             <asp:HiddenField ID="hid_request_id" runat="server" Value="" />
                            <asp:Button ID="btn_OK" runat="server" class="btn btn-primary" Text="儲存編輯" ValidationGroup="validate" OnClick="btn_OK_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


   <div id="page-wrapper">
        <div class="row">
             <h2 class="margin-bottom-10">退貨派遣</h2>
            <hr />
             <div class="templatemo-login-form" >
                  <div class="form-group form-inline">
                            <label for="rbcheck_type">站所別：</label>
                                <asp:DropDownList ID="ddlStation" runat="server" >
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlType" runat="server" >
                                     <asp:ListItem  Value="">全部</asp:ListItem>
                                     <asp:ListItem Value="0">退貨</asp:ListItem>
                                     <asp:ListItem Value="1">來回件</asp:ListItem>
                                </asp:DropDownList>
                           <label for="rbcheck_type">司機帳號：</label>
                       <asp:TextBox ID="tb_search" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:Label ID="lbDate" runat="server" Text="建檔日期"></asp:Label>
                    <asp:TextBox ID="cdate1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>~
                    <asp:TextBox ID="cdate2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20" ></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                </div>
             </div>
        </div>
    </div>
      <hr />
     <table class="table table-striped table-bordered templatemo-user-table table_list">
          <tr class="tr-only-hide">
                        <th class="tb_title _h_auto">
                            <asp:CheckBox ID="chkHeader" runat="server" />
                        </th>
                        <th class="tb_title _h_auto">建檔日</th>
                        <th class="tb_title _h_auto">取件日</th>
                        <th class="tb_title _h_auto">貨號</th>
                        <th class="tb_title _h_auto">退貨單號</th>
                        <th class="tb_title _h_auto">退貨人</th>
                        <th class="tb_title _h_auto">電話</th>
                        <th class="tb_title _h_addr">送件人地址</th>
                        <th class="tb_title _h_auto">回覆狀態</th>
                        <th class="tb_title _h_auto">取貨人</th>
                        <th class="tb_title _h_auto">區分</th>
                        <th class="tb_title _h_auto">退貨屬性</th>
                        <th class="tb_title _h_auto">編輯</th>
                    </tr>
                   <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                        <ItemTemplate>
                            <tr>
                                <td class="_d_function" data-th="">
                                   <asp:CheckBox ID="chkRow" runat="server" />
                                   <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                </td>
                                <td class="tb_detail " data-th="建檔日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate").ToString())%></td>
                                <td class="tb_detail " data-th="取件日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                                <td class="tb_detail " data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                <td class="tb_detail " data-th="退貨單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                                <td class="tb_detail " data-th="送件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Send_contact").ToString())%></td>
                                <td class="tb_detail " data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_tel").ToString())%></td>
                                <td class="tb_detail " data-th="送件人地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_addr").ToString())%></td>
                                <td class="tb_detail " data-th="回覆狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_state").ToString())%></td>
                                <td class="tb_detail " data-th="取貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.newest_driver_code").ToString())%></td>
                                <td class="tb_detail " data-th="區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_item").ToString())%></td>
                                <td class="tb_detail " data-th="退貨屬性"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.returntype").ToString())%></td>
                                <td class="tb_detail " data-th="編輯">
                                      <asp:Button ID="btn_Edit" CssClass="btn btn-success" CommandName="cmdEdit" 
                                          runat="server" Text="編輯" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />

                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                    {%>
                    <tr>
                        <td colspan="13" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
         </table>
        <div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                   <%-- 客戶總數: <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span>--%>
        </div>
       <asp:Label runat="server" ID="rowcount" Visible="false" ></asp:Label>
      <%-- <asp:TextBox ID="tb_ckeck" CssClass="_hide _tb_ckeck" runat="server"></asp:TextBox>--%>
</asp:Content>

