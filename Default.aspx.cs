﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int fee = Utility.getremote_fee(city.Text, area.Text, addr.Text);
        TextBox2.Text = fee.ToString();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        #region 密碼產生(大小寫英數字夾雜)
        string randPwd = "";
        do
        {
            randPwd = Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
        } while (!System.Text.RegularExpressions.Regex.IsMatch(randPwd,
                                                           "\\d+[A-F]+\\d+[A-F]"));
        //具有兩個以上的英文字母，並且中間夾數字才算合格
        //將第二個英文字母變小寫
        int p = Regex.Matches(randPwd, "[A-F]")[1].Index;
        randPwd = randPwd.Insert(p, randPwd[p].ToString().ToLower()).Remove(p + 1, 1);

        MD5 md5 = MD5.Create();//建立一個MD5
        password.Text  = GetMd5Hash(md5, randPwd);
        password0.Text = randPwd;
        #endregion
    }

    protected string GetMd5Hash(MD5 md5Hash, string input)
    {

        // Convert the input string to a byte array and compute the hash.
        byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

        // Create a new Stringbuilder to collect the bytes
        // and create a string.
        StringBuilder sBuilder = new StringBuilder();

        // Loop through each byte of the hashed data 
        // and format each one as a hexadecimal string.
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }

        // Return the hexadecimal string.
        return sBuilder.ToString();
    }
}