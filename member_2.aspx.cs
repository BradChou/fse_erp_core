﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class member_2 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            MultiView1.ActiveViewIndex = 0;

            #region 發送站-區配商
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select supplier_code, supplier_code + ' ' + supplier_name as showname from tbSuppliers where LEN(supplier_code) > 0 and supplier_code != '000' and active_flag = 1";
                supplier.DataSource = dbAdapter.getDataTable(cmd);
                supplier.DataValueField = "supplier_code";
                supplier.DataTextField = "showname";
                supplier.DataBind();
                supplier.Items.Insert(0, new ListItem("請選擇", ""));
            }
            #endregion

            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            City.DataSource = dbAdapter.getDataTable(cmd1);
            City.DataValueField = "city";
            City.DataTextField = "city";
            City.DataBind();
            City.Items.Insert(0, new ListItem("請選擇縣市別", ""));
            InvoiceCity.DataSource = dbAdapter.getDataTable(cmd1);
            InvoiceCity.DataValueField = "city";
            InvoiceCity.DataTextField = "city";
            InvoiceCity.DataBind();
            InvoiceCity.Items.Insert(0, new ListItem("請選擇縣市別", ""));
            #endregion

            #region 計價模式
            SqlCommand cmd2 = new SqlCommand();
            string wherestr = string.Empty;
            if (Less_than_truckload == "1") wherestr = " and code_id  = '02'";
            cmd2.CommandText = string.Format(" select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM' {0}  order by code_id asc", wherestr);
            PM_code.DataSource = dbAdapter.getDataTable(cmd2);
            PM_code.DataValueField = "code_id";
            PM_code.DataTextField = "showname";
            PM_code.DataBind();
            PM_code.Items.Insert(0, new ListItem("選擇計價模式", ""));

            SPM_code.DataSource = dbAdapter.getDataTable(cmd2);
            SPM_code.DataValueField = "code_id";
            SPM_code.DataTextField = "showname";
            SPM_code.DataBind();
            #endregion

            #region 停運原因
            SqlCommand cmd3 = new SqlCommand();
            cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'TS' and active_flag = 1 order by seq asc ";
            stop_code.DataSource = dbAdapter.getDataTable(cmd3);
            stop_code.DataValueField = "code_id";
            stop_code.DataTextField = "code_name";
            stop_code.DataBind();
            #endregion

            #region 所有主客代號(要加權限控制)
            string manager_type = Session["manager_type"].ToString(); //管理單位類別   
            string supplier_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }

            using(SqlCommand cmd4 = new SqlCommand())
            {
                wherestr = "";
                if (supplier_code != "")
                {
                    cmd4.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                }
                cmd4.Parameters.AddWithValue("@type", Less_than_truckload);
                cmd4.CommandText = string.Format(@"select distinct master_code, master_code +'-'+ customer_name as name  from tbCustomers  
                                                    where second_id= 0 and type=@type  
                                                    {0} order by master_code ", wherestr);
                dlmaster_code.DataSource = dbAdapter.getDataTable(cmd4);
                dlmaster_code.DataValueField = "master_code";
                dlmaster_code.DataTextField = "name";
                dlmaster_code.DataBind();
                dlmaster_code.Items.Insert(0, new ListItem("請選擇", ""));
            }
            
            #endregion
        }
        

    }

    private void clear()
    {      
        master_code.Text = "";         //主客代完整代碼      
        second_code.Text = "";
        PM_code.SelectedValue = "";
        //PM_code.Items.Clear() ;      //計價模式 
        customer_name.Text = "";       //客戶名稱
        shortname.Text = "";           //客戶簡稱
        uni_number.Text = "";          //統一編號
        principal.Text = "";           //對帳人
        tel.Text = "";                 //電話
        fax.Text = "";                 //傳真

        City.SelectedIndex = -1;       //出貨地址-縣市
        City_SelectedIndexChanged(null, null);
        CityArea.SelectedIndex =-1;     //出貨地址-鄉鎮市區 
        address.Text = "";              //出貨地址-路街巷弄號
        email.Text = "";                //出貨人電子郵件
        SPM_code.SelectedIndex =-1;     //計價模式
        supplier_code.Text = "";        //負責區配商編號
        supplier_name.Text = "";        //負責區配商名稱
        stop_code.SelectedIndex  = -1;  //停運原因代碼  0正常、1同業競爭、2公司倒閉、3呆帳、4其他原因
        stop_memo.Text = "";            //停運原因備註
        //customer_hcode.Text = "";     //竹運客代
        contract_sdate.Text = "";       //合約生效日
        contract_edate.Text = "";       //合約到期日
        filename.Text = "";  //合約內容             
        // http://" + Request.Url.Authority + "/files/contract/
        //fee_sdate.Text = ""; //運價生效日
        close_date.SelectedValue = "";         //結帳日
        tbclose.Text = "";
        //feename.Text = "";                   //運價表
        business_people.Text = "";             //營業人員
        billing_special_needs.Text = "";       //帳單特殊需求
        ticket_period.SelectedValue  = "";     //票期
        ticket.Text = "";
    }

   
    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (City.SelectedValue != "")
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", City.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    CityArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            CityArea.Items.Clear();
            CityArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void InvoiceCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (InvoiceCity.SelectedValue != "")
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", InvoiceCity.SelectedValue.ToString());
            cmda.CommandText = "Select * from tbPostCityArea where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    InvoiceArea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            InvoiceArea.Items.Clear();
            InvoiceArea.Items.Add(new ListItem("選擇鄉鎮區", ""));

        }
    }

    protected void PM_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        string sScript = "";
        cmd.Parameters.AddWithValue("@master_code", dlmaster_code.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
        cmd.CommandText = " select * from  tbCustomers where master_code = @master_code ";  //and pricing_code=@pricing_code
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {  
            master_code.Text = dt.Rows[0]["master_code"].ToString();                //主客代完整代碼      
            //PM_code.SelectedValue = dt.Rows[0]["pricing_code"].ToString().Trim();    //計價模式 
            PM_code.SelectedValue = PM_code.SelectedValue.ToString();    //計價模式 
            customer_name.Text = dt.Rows[0]["customer_name"].ToString();  //客戶名稱
            shortname.Text = dt.Rows[0]["customer_shortname"].ToString();  //客戶簡稱
            uni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();  //統一編號
            principal.Text = dt.Rows[0]["shipments_principal"].ToString();  //對帳人
            tel.Text = dt.Rows[0]["telephone"].ToString();  //電話
            fax.Text = dt.Rows[0]["fax"].ToString();        //傳真
            customer_type.SelectedValue = dt.Rows[0]["customer_type"].ToString(); 
            try
            {
                City.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
            }
            catch
            { }
            
            City_SelectedIndexChanged(null, null);
            try
            {
                CityArea.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
            }
            catch
            { }
           
            address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();   //出貨地址-路街巷弄號
            email.Text = dt.Rows[0]["shipments_email"].ToString().Trim();    //出貨人電子郵件
            //SPM_code.SelectedValue = dt.Rows[0]["pricing_code"].ToString().Trim();    //計價模式
            SPM_code.SelectedValue = PM_code.SelectedValue.ToString();    //計價模式
            supplier_code.Text = dt.Rows[0]["supplier_code"].ToString().Trim();     //負責區配商

            if (supplier_code.Text.Trim() != "")
            {
                SqlCommand cmds = new SqlCommand();
                DataTable dts;
                cmds.Parameters.AddWithValue("@supplier_code", supplier_code.Text);
                cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
                dts = dbAdapter.getDataTable(cmds);
                if (dts.Rows.Count > 0)
                {
                    supplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
                }
            }
            
            stop_code.SelectedValue = dt.Rows[0]["stop_shipping_code"].ToString().Trim();     //停運原因代碼  0正常、1同業競爭、2公司倒閉、3呆帳、4其他原因
            stop_memo.Text = dt.Rows[0]["stop_shipping_memo"].ToString().Trim();    //停運原因備註
            //customer_hcode.Text = dt.Rows[0]["customer_hcode"].ToString().Trim();    //竹運客代


            //合約生效日
            if (dt.Rows[0]["contract_effect_date"] != DBNull.Value &&
                (((DateTime)dt.Rows[0]["contract_effect_date"]) > Convert.ToDateTime("1900/01/01")) &&
                (((DateTime)dt.Rows[0]["contract_effect_date"]) < Convert.ToDateTime("9999/12/31")))
            {
                contract_sdate.Text = ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd");
            }

            //合約到期日
            if (dt.Rows[0]["contract_expired_date"] != DBNull.Value &&
                (((DateTime)dt.Rows[0]["contract_expired_date"]) > Convert.ToDateTime("1900/01/01")) &&
                (((DateTime)dt.Rows[0]["contract_expired_date"]) < Convert.ToDateTime("9999/12/31")))
            {
                contract_edate.Text = ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd");
            }

            filename.Text = dt.Rows[0]["contract_content"].ToString().Trim();  //合約內容     
            sScript += "$('#hlFileView').attr('href','" + "http://" + Request.Url.Authority + "/files/contract/" + filename.Text + "');";
       

            ////運價生效日
            //if (dt.Rows[0]["tariffs_effect_date"] != DBNull.Value &&
            //    (((DateTime)dt.Rows[0]["tariffs_effect_date"]) > Convert.ToDateTime("1900/01/01")) &&
            //    (((DateTime)dt.Rows[0]["tariffs_effect_date"]) < Convert.ToDateTime("9999/12/31")))
            //{
            //    fee_sdate.Text = ((DateTime)dt.Rows[0]["tariffs_effect_date"]).ToString("yyyy/MM/dd");
            //}
            
            //結帳日
            if (dt.Rows[0]["checkout_date"] != DBNull.Value)
            {
                if (close_date.Items.FindByValue(dt.Rows[0]["checkout_date"].ToString()) == null)
                {
                    close_date.SelectedValue = "-1";
                    tbclose.Text = dt.Rows[0]["checkout_date"].ToString();
                    sScript += "$('#" + tbclose.ClientID + "').show();";
                }
                else
                {
                    close_date.SelectedValue = dt.Rows[0]["checkout_date"].ToString();
                }
            }
            else
            {
                close_date.SelectedValue = "";
                tbclose.Text = "";
                sScript += "$('#" + tbclose.ClientID + "').hide();";
            }

            //feename.Text = dt.Rows[0]["tariffs_table"].ToString().Trim();                      //運價表
            //sScript += "$('#hlFeeView').attr('href','" + "http://" + Request.Url.Authority + "/files/fee/" + feename.Text + "');";
            business_people.Text = dt.Rows[0]["business_people"].ToString().Trim();              //營業人員
            billing_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString().Trim();  //帳單特殊需求
            //票期
            if (dt.Rows[0]["ticket_period"] != DBNull.Value)
            {
                if (ticket_period.Items.FindByValue(dt.Rows[0]["ticket_period"].ToString()) == null)
                {
                    ticket_period.SelectedValue = "-1";
                    ticket.Text = dt.Rows[0]["ticket_period"].ToString();
                    sScript += "$('#" + ticket.ClientID + "').show();";
                }
                else
                {
                    ticket_period.SelectedValue = dt.Rows[0]["ticket_period"].ToString();
                }
            }
            else
            {
                ticket_period.SelectedValue = "";
                ticket.Text = "";
                sScript += "$('#" + ticket.ClientID + "').hide();";
            }

        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
        return;
    }

    protected void insertSameAddress(object sender, EventArgs e)
    {
        InvoiceCity.SelectedIndex = City.SelectedIndex;
        InvoiceCity_SelectedIndexChanged(sender, e);
        InvoiceArea.SelectedIndex = CityArea.SelectedIndex;
        InvoiceAddress.Text = address.Text;
    }

    protected void btnew_Click(object sender, EventArgs e)
    {
        #region 序號
        string serialnum = "";
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        cmd.Parameters.AddWithValue("@master_code", dlmaster_code.SelectedValue.ToString());
        cmd.Parameters.AddWithValue("@pricing_code", PM_code.SelectedValue.ToString());
        cmd.CommandText = " select max(second_id)  as serial from tbCustomers where master_code=@master_code and pricing_code=@pricing_code and customer_code <> '' ";
        dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["serial"] != DBNull.Value)
            {
                serialnum = (Convert.ToInt32(dt.Rows[0]["serial"].ToString()) + 1).ToString();
            }
            else
            {
                serialnum = "0";
            }
        }
        else
        {
            serialnum = "0";
        }
        second_code.Text = serialnum + PM_code.SelectedValue.ToString();
        btnsave.Enabled = (second_code.Text != "");
        #endregion
    }

    protected void dlmaster_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        clear();

        //SqlCommand cmd2 = new SqlCommand();
        //cmd2.CommandText = " select code_id, code_name, code_id + '-' +  code_name as showname from tbItemCodes where code_sclass = 'PM'" +
        //                   " and code_id in(select distinct pricing_code from  tbCustomers where master_code = '" + dlmaster_code.SelectedValue.ToString() + "') order by code_id asc";
        //PM_code.DataSource = dbAdapter.getDataTable(cmd2);
        //PM_code.DataValueField = "code_id";
        //PM_code.DataTextField = "showname";
        //PM_code.DataBind();
        //PM_code.Items.Insert(0, new ListItem("選擇計價模式", ""));
        //if (PM_code.Items.Count == 1)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無主客代編號：" + dlmaster_code.SelectedValue.ToString() + "');</script>", false);
        //    return;
        //}
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        DateTime dt_temp = new DateTime();
        int i_tmp;
        string customer_id = "";
        #region 如果有空的(表示剛新增一筆新的廠商的主客代，但還沒建次客代)
        SqlCommand cmdt = new SqlCommand();
        DataTable dtt;
        cmdt.Parameters.AddWithValue("@master_code", master_code.Text);
        cmdt.Parameters.AddWithValue("@pricing_code", SPM_code.SelectedValue.ToString());
        cmdt.CommandText = " select customer_id from tbCustomers where master_code = @master_code and pricing_code = @pricing_code and customer_code = '' ";
        dtt = dbAdapter.getDataTable(cmdt);
        if (dtt.Rows.Count > 0)
        {
            customer_id = dtt.Rows[0]["customer_id"].ToString();
        }
        #endregion

        #region  新增       
        SqlCommand cmd = new SqlCommand();
        if (customer_id == "")
        {
            cmd.Parameters.AddWithValue("@supplier_code", master_code.Text.Substring(0, 3));               //區配商代碼(前3碼)
            cmd.Parameters.AddWithValue("@supplier_id", master_code.Text.Substring(3,4));                  //主客代序號(後4碼)
            cmd.Parameters.AddWithValue("@master_code", master_code.Text);                                 //主客代完整代碼
            cmd.Parameters.AddWithValue("@pricing_code", SPM_code.SelectedValue.ToString());               //計價模式
        }
        cmd.Parameters.AddWithValue("@second_id", second_code.Text.Substring(0,1));                        //次客代流水號
        cmd.Parameters.AddWithValue("@second_code", second_code.Text);                                     //次客代完整代碼
        string customer_code = master_code.Text.Trim() + second_code.Text.Trim();
        cmd.Parameters.AddWithValue("@customer_code", customer_code);  //客戶完整代碼 : 主客代(7碼) + 次客代(3碼)

        cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());        //客戶名稱
        cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());       //客戶簡稱
        if (exclusiveSendStation.Checked == true && supplier.SelectedValue.ToString().Length > 0)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", supplier.SelectedValue.ToString()); //棧板綁定專屬發送站
        if (shippingAddress.Checked == true)
            cmd.Parameters.AddWithValue("@pallet_exclusive_send_station", "0"); //棧板發送站由出貨地址判斷

        //customer_type (1:區配 2:竹運 3:自營)
        //if (customer_hcode.Text != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_type", "2");                              //客戶類別
        //}
        if (master_code.Text.Substring(3, 4) == "0000")
        {
            cmd.Parameters.AddWithValue("@customer_type", "1");
        }
        else
        {
            cmd.Parameters.AddWithValue("@customer_type", "3");
        }
          
        cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
        cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
        cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
        cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
        cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
        cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
        cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
        cmd.Parameters.AddWithValue("@invoice_city", InvoiceCity.SelectedValue.ToString());       //發票地址-縣市
        cmd.Parameters.AddWithValue("@invoice_area", InvoiceArea.SelectedValue.ToString());   //發票地址-鄉鎮市區
        cmd.Parameters.AddWithValue("@invoice_road", InvoiceAddress.Text.ToString());             //發票地址-路街巷弄號
        cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
        cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
        cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
        //合約生效日
        if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
        }

        //合約到期日
        if (DateTime.TryParse(contract_edate.Text, out dt_temp))
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
        }
        else
        {
            cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
        }

        //票期
        if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(ticket.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
            }

        }
        else
        {
            cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
        }
        cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
        //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日
        
        //結帳日
        if (int.TryParse(close_date.SelectedValue, out i_tmp))
        {
            if (i_tmp == -1)
            {
                if (int.TryParse(tbclose.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
            }
        }
        else
        {
            cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
        }

        //cmd.Parameters.AddWithValue("@tariffs_table", feename.Text.ToString());            //運價表
        cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
        cmd.Parameters.AddWithValue("@customer_hcode", "");                                  //竹運客代
        cmd.Parameters.AddWithValue("@contact_1", "");                                       //竹運-聯絡人1  
        cmd.Parameters.AddWithValue("@email_1", "");                                         //竹運-聯絡人1 email
        cmd.Parameters.AddWithValue("@contact_2", "");                                       //竹運-聯絡人2
        cmd.Parameters.AddWithValue("@email_2", "");                                         //竹運-聯絡人2 email
        cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
        cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
        cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
        cmd.Parameters.AddWithValue("@cdate", DateTime.Now );                                //更新時間
        //cmd.Parameters.AddWithValue("@create_date", DateTime.Now);
        //cmd.Parameters.AddWithValue("@update_date", DateTime.Now);

        if (customer_id == "")
        {
            cmd.CommandText = dbAdapter.SQLdosomething("tbCustomers", cmd, "insert") ;
            dbAdapter.execNonQuery(cmd);
            customer_id = dbAdapter.finalindex("tbCustomers", "customer_id");
        }
        else
        {
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", customer_id);
            cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);                    //儲存至空白筆主客代
            dbAdapter.execNonQuery(cmd);
        }

        #endregion

        //MultiView1.ActiveViewIndex = 1;
        SetDefaultView2(customer_code, customer_type.SelectedValue, customer_id, PM_code.SelectedValue.ToString());
        //string sScript = " $('#li_01').removeClass('current');" +
        //                 "    $('#li_02').addClass('current');";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_3-edit.aspx?customer_id=" + customer_id + "';alert('新增完成');</script>", false);
        setFee.Visible = true;
        FinalCheck.Visible = true;
        btnsave.Visible = false;
        btncancel.Visible = false;
        dlmaster_code.Enabled = false;
        PM_code.Enabled = false;
        btnew.Enabled = false;
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('客代編號" + customer_code + "新增完成！');</script>", false);
        return;

    }

    /// <summary>
    /// 運價初始資料設定
    /// </summary>
    protected void SetDefaultView2(string customer_code, string  customer_type, string  customer_id, string pricing_code)
    {
        //運價生效日
        DateTime dt_temp = new DateTime();
        tbmaster_code.Text = customer_code.Substring(0, 7);
        tbsecond_code.Text = customer_code.Substring(7, 3);
        lbcustomer_type.Text = customer_type;
        lb_cusid.Text = customer_id;
        lb_pricing_code.Text = pricing_code;
        lbl_PriceTip.Text = "";
        fee_sdate.Text = "※目前採用公版運價";
        if (customer_code.Length > 0)
        {
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.CommandText = "usp_GetTodayPriceBusDefLog";
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@customer_code", customer_code);
                cmd2.Parameters.AddWithValue("@return_type", "1");
                using (DataTable the_dt = dbAdapter.getDataTable(cmd2))
                {
                    if (the_dt.Rows.Count > 0)
                    {
                        if (DateTime.TryParse(the_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                        {
                            fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                        }

                        int i_temp;
                        if (!int.TryParse(the_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                        lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                    }
                    //else
                    //{
                    //    // 抓父客代的運價
                    //    cmd2.Parameters["@customer_code"].Value = master_code.Text.Trim() + "0" + SPM_code.SelectedValue.ToString();
                    //    using (DataTable parent_dt = dbAdapter.getDataTable(cmd2))
                    //    {
                    //        if (parent_dt.Rows.Count > 0)
                    //        {
                    //            if (DateTime.TryParse(parent_dt.Rows[0]["tariffs_effect_date"].ToString(), out dt_temp))
                    //            {
                    //                fee_sdate.Text = dt_temp.ToString("yyyy/MM/dd");
                    //            }

                    //            int i_temp;
                    //            if (!int.TryParse(parent_dt.Rows[0]["tariffs_type"].ToString(), out i_temp)) i_temp = 1;
                    //            lbl_PriceTip.Text = "※目前採用" + (i_temp == 2 ? "自訂" : "公版") + "運價";
                    //        }
                    //    }
                    //}
                }
            }
        }


        //自營才開放價格設定
        if (Convert.ToInt32 (customer_type) == 3)
        {
            pan_price_list.Controls.Add(new LiteralControl(Utility.GetBusDefLogOfPrice(customer_code, customer_id.ToString())));
            pan_price_list.Visible = true;
        }
        else
        {
            pan_price_list.Visible = false;
        }
    }

    protected void btnQry_view2_Click(object sender, EventArgs e)
    {
        SetDefaultView2(tbmaster_code.Text + tbsecond_code.Text, lbcustomer_type.Text, lb_cusid.Text, lb_pricing_code.Text);
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        DateTime dt_temp = new DateTime();
        int i_tmp;
        string sScript = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@customer_name", customer_name.Text.ToString());                 //客戶名稱
            cmd.Parameters.AddWithValue("@customer_shortname", shortname.Text.ToString());                //客戶簡稱
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                      //更新人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                //更新時間
            cmd.Parameters.AddWithValue("@uniform_numbers", uni_number.Text.ToString());         //統一編號    
            cmd.Parameters.AddWithValue("@shipments_principal", principal.Text.ToString());      //對帳人
            cmd.Parameters.AddWithValue("@telephone", tel.Text.ToString());                      //電話
            cmd.Parameters.AddWithValue("@fax", fax.Text.ToString());                            //傳真
            cmd.Parameters.AddWithValue("@shipments_city", City.SelectedValue.ToString());       //出貨地址-縣市
            cmd.Parameters.AddWithValue("@shipments_area", CityArea.SelectedValue.ToString());   //出貨地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@shipments_road", address.Text.ToString());             //出貨地址-路街巷弄號
            cmd.Parameters.AddWithValue("@invoice_city", InvoiceCity.SelectedValue.ToString());       //發票地址-縣市
            cmd.Parameters.AddWithValue("@invoice_area", InvoiceArea.SelectedValue.ToString());   //發票地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@invoice_road", InvoiceAddress.Text.ToString());             //發票地址-路街巷弄號
            cmd.Parameters.AddWithValue("@shipments_email", email.Text.ToString());              //出貨人電子郵件
            cmd.Parameters.AddWithValue("@stop_shipping_code", stop_code.SelectedValue.ToString());    //停運原因代碼
            cmd.Parameters.AddWithValue("@stop_shipping_memo", stop_memo.Text.ToString());       //停運原因備註
                                                                                                 //合約生效日
            if (DateTime.TryParse(contract_sdate.Text, out dt_temp))
            {
                cmd.Parameters.AddWithValue("@contract_effect_date", contract_sdate.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@contract_effect_date", DBNull.Value);
            }

            //合約到期日
            if (DateTime.TryParse(contract_edate.Text, out dt_temp))
            {
                cmd.Parameters.AddWithValue("@contract_expired_date", contract_edate.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@contract_expired_date", DBNull.Value);
            }

            //票期
            if (int.TryParse(ticket_period.SelectedValue, out i_tmp))
            {
                if (i_tmp == -1)
                {
                    if (int.TryParse(ticket.Text, out i_tmp))
                    {
                        cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ticket_period", i_tmp.ToString());
                }

            }
            else
            {
                cmd.Parameters.AddWithValue("@ticket_period", DBNull.Value);
            }
            cmd.Parameters.AddWithValue("@contract_content", filename.Text.ToString());          //合約內容
                                                                                                 //cmd.Parameters.AddWithValue("@tariffs_effect_date", fee_sdate.Text);               //運價生效日

            //結帳日
            if (int.TryParse(close_date.SelectedValue, out i_tmp))
            {
                if (i_tmp == -1)
                {
                    if (int.TryParse(tbclose.Text, out i_tmp))
                    {
                        cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@checkout_date", 31);   //如果選其他，但沒填日期，預設31日
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@checkout_date", i_tmp.ToString());
                }
            }
            else
            {
                cmd.Parameters.AddWithValue("@checkout_date", 31); //如果選其他，但沒填日期，預設31日
            }

            cmd.Parameters.AddWithValue("@business_people", business_people.Text.ToString());    //營業人員
            cmd.Parameters.AddWithValue("@billing_special_needs", billing_special_needs.Text.ToString());   //帳單特殊需求
            cmd.Parameters.AddWithValue("@contract_price", contract_price.Text);                 //發包價
            cmd.Parameters.AddWithValue("@individual_fee", individual_fee.Checked);                //是否個別計價  
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", lb_cusid.Text);
            cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                string strErr = string.Empty;
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "新增主客戶" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");

                sScript = "alert('儲存失敗：" + ex.Message.ToString() + "')";
            }
        }
        //MultiView1.ActiveViewIndex = 2;
        
        using (SqlCommand cmd2 = new SqlCommand())
        {
            cmd2.Parameters.AddWithValue("@customer_code", tbmaster_code.Text + tbsecond_code.Text);
            cmd2.CommandText = " select top 1 tbCustomers.* , B.code_name as pricingname, C.code_name as stopname  from tbCustomers " +
                              " left join  tbItemCodes B on B.code_id = tbCustomers.pricing_code and B.code_sclass = 'PM'" +
                              " left join  tbItemCodes C on C.code_id = tbCustomers.stop_shipping_code and C.code_sclass = 'TS'" +
                              " where customer_code = @customer_code ";
            using (DataTable dt = dbAdapter.getDataTable(cmd2))
            {
                lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                if (dt.Rows[0]["supplier_code"].ToString().Trim() != "")
                {
                    SqlCommand cmds = new SqlCommand();
                    DataTable dts;
                    cmds.Parameters.AddWithValue("@supplier_code", dt.Rows[0]["supplier_code"].ToString().Trim());
                    cmds.CommandText = " select supplier_name from  tbSuppliers where supplier_code = @supplier_code ";
                    dts = dbAdapter.getDataTable(cmds);
                    if (dts.Rows.Count > 0)
                    {
                        lsupplier_name.Text = dts.Rows[0]["supplier_name"].ToString().Trim();     //負責區配商名稱
                    }
                }
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();
                lbshortname.Text = dt.Rows[0]["customer_shortname"].ToString();
                lbuni_number.Text = dt.Rows[0]["uniform_numbers"].ToString();
                lbprincipal.Text = dt.Rows[0]["shipments_principal"].ToString();
                lbtel.Text = dt.Rows[0]["telephone"].ToString();
                lbfax.Text = dt.Rows[0]["fax"].ToString();
                lbCity.Text = dt.Rows[0]["shipments_city"].ToString();
                lbCityArea.Text = dt.Rows[0]["shipments_area"].ToString();
                lbaddress.Text = dt.Rows[0]["shipments_road"].ToString();
                lbbusiness_people.Text = dt.Rows[0]["business_people"].ToString();
                lbticket_period.Text = dt.Rows[0]["ticket_period"].ToString();
                inputInvoiceCity.Text = dt.Rows[0]["invoice_city"].ToString();
                inputInvoiceArea.Text = dt.Rows[0]["invoice_area"].ToString();
                inputInvoiceAddress.Text = dt.Rows[0]["invoice_road"].ToString();
                lbemial.Text = dt.Rows[0]["shipments_email"].ToString().Trim();
                lbSPM_code.Text = dt.Rows[0]["pricingname"].ToString().Trim();
                lbstop_code.Text = dt.Rows[0]["stopname"].ToString().Trim();
                lbcontract_sdate.Text = dt.Rows[0]["contract_effect_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_effect_date"]).ToString("yyyy/MM/dd") : "";
                lbcontract_edate.Text = dt.Rows[0]["contract_expired_date"] != DBNull.Value ? ((DateTime)dt.Rows[0]["contract_expired_date"]).ToString("yyyy/MM/dd") : "";
                sScript += "$('#hlFileView2').attr('href','" + "http://" + Request.Url.Authority + "/files/contract/" + filename.Text + "');";
                lbclose_date.Text = dt.Rows[0]["checkout_date"].ToString();
                lbbilling_special_needs.Text = dt.Rows[0]["billing_special_needs"].ToString();
                lbFee.Text = Convert.ToBoolean(dt.Rows[0]["individual_fee"]) == true ? "獨立計價" : "採運價表計價";

            }
        }


        //sScript += " $('#li_01').removeClass('current');$('#li_02').removeClass('current');" +
        //                "    $('#li_03').addClass('current');";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);    
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='member_2.aspx';alert('運價新增完成');</script>", false);
        return;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        clear();
        //MultiView1.ActiveViewIndex = 0;
    }
}