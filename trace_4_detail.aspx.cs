﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class trace_4_detail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Boolean IsChk = true;
            int i_tmp = 0;
            DateTime dt_tmp = new DateTime();
            if (Request.QueryString["type"] != null)
                if (!int.TryParse(Request.QueryString["type"].ToString(), out i_tmp))
                {
                    IsChk = false;
                }

            if (Request.QueryString["dates"] != null)
                if (!DateTime.TryParse(Request.QueryString["dates"].ToString(), out dt_tmp))
                {
                    IsChk = false;
                }

            if (Request.QueryString["datee"] != null)
                if (!DateTime.TryParse(Request.QueryString["datee"].ToString(), out dt_tmp))
                {
                    IsChk = false;
                }

            //if (Request.QueryString["supplier"] != null)
            //    if (Request.QueryString["supplier"].ToString().Length == 0)
            //    {
            //        IsChk = false;
            //    }

            if (Request.QueryString["type"] != null)
                if (Request.QueryString["type"].ToString().Length == 0)
                {
                    IsChk = false;
                }
            

            if (!IsChk)
            {
                RetuenMsg("資訊異常，請重新確認!");
                return;
            }
            else
            {
                DefaultData(i_tmp, Request.QueryString["dates"].ToString(), Request.QueryString["datee"].ToString(), Request.QueryString["supplier"].ToString(), Request.QueryString["scan"].ToString());
            }
        }
    }

    protected void DefaultData(int i_type, string str_DateS, string str_DateE, string str_supplier, string scan)
    {
        Boolean IsChk = false;
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();

        if (DateTime.TryParse(str_DateS, out dt_s)
            && DateTime.TryParse(str_DateE, out dt_e))
        {
            IsChk = true;
        }
        if (!IsChk)
        {
            lbl_tip.Text = "資訊異常，請重新確認!";            
        }
        else
        {
            //lbl_tip.Text = i_type == 1 ? "回單" : "簽單";
            switch (i_type)
            {
                case 0:
                    lbl_tip.Text = "簽單";
                    break;
                case 1:
                    lbl_tip.Text = "回單";
                    break;
                case 2:
                    lbl_tip.Text = "棧板回收";
                    break;
            }

            lbl_tip.Text = lbl_tip.Text + ("　　發送日期:" + dt_s.ToString("yyyy/MM/dd") + " ~ " + dt_e.ToString("yyyy/MM/dd"));

            using (SqlCommand cmd = new SqlCommand())
            {
                #region 組SQL
                String strSQL = @" SELECT ROW_NUMBER() OVER(ORDER BY info.send_date DESC) ord
                                         ,sup.supplier_name+'('+ drs.supplier_code +')' supplier_name
                                         ,send_area ,drs.check_number ,send_contact
                                         ,CASE WHEN (Len(receive_contact)>=3) THEN SUBSTRING( receive_contact,1,1 )+'*'+ SUBSTRING(receive_contact,3,Len(receive_contact)) 
                                               WHEN (Len(receive_contact)=2) THEN SUBSTRING( receive_contact,1,1 )+'*'
                                          ELSE receive_contact END 'receive_contact'
                                         ,CONVERT(CHAR(10),info.send_date,111) send_date,info.arrive_item
                                         ,CONVERT(CHAR(10),drs.print_date,111) print_date 
                                         ,CASE WHEN drs.supplier_code IN('001','002') THEN REPLACE(REPLACE(D.customer_shortname,'所',''),'HCT','') ELSE drs.supplier_name END as sendstation
                                    FROM dbo.tcDeliveryRequests drs With(Nolock)
                                    LEFT JOIN dbo.tbSuppliers sup With(Nolock) ON drs.supplier_code = sup.supplier_code  
                                    LEFT JOIN dbo.tbCustomers D With(Nolock) on D.customer_code = drs.customer_code 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(drs.request_id) info
                                    WHERE cancel_date IS NULL and CONVERT(CHAR(10),drs.print_date,111) BETWEEN @dateS AND @dateE ";
                if (str_supplier != "")
                {
                    cmd.Parameters.AddWithValue("@supplier_code", str_supplier);
                    strSQL += " AND  drs.supplier_code IN(@supplier_code) ";
                }
                if (i_type == 1) strSQL += " AND drs.receipt_flag =1 ";
                if (i_type == 2) strSQL += " AND drs.pallet_recycling_flag =1 ";
                if (scan == "yes") strSQL += " AND drs.check_number  IN(SELECT dlog.check_number FROM ttDeliveryScanLog dlog With(Nolock) WHERE  scan_date>=DATEADD(MONTH,-3,GETDATE()))";
                if (scan == "no") strSQL += " AND drs.check_number NOT IN(SELECT dlog.check_number FROM ttDeliveryScanLog dlog With(Nolock) WHERE  scan_date>=DATEADD(MONTH,-3,GETDATE()))";
                strSQL += "ORDER BY drs.print_date DESC ";

                #endregion

                cmd.Parameters.AddWithValue("@dateS", dt_s.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", dt_e.ToString("yyyy/MM/dd"));
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                  
                   

                    list_scan.DataSource = dt;
                    list_scan.DataBind();
                    ltotalpages.Text = dt.Rows.Count.ToString();
                }
            }

        }
    }



    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }
}