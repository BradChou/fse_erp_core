﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class loan4_1pay : System.Web.UI.Page
{
    public string id
    {
        get { return ViewState["id"].ToString(); }
        set { ViewState["id"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            id = Request.QueryString["id"] != null ? Request.QueryString["id"].ToString() :"";
            readdata(id);
        }
    }

    private void readdata( string id)
    {
        int _i = 0;
        int _p = 0;
        using (SqlCommand cmd = new SqlCommand())
        {   
            cmd.Parameters.Clear();
            cmd.CommandText = string.Format(@"Select id, loan_id, type, due_date, pay_date, opening_balance, installment_price, principal, interest, end_balance, total_interest, paid, ISNULL(paid_price,0) 'paid_price', memo
                                              from ttRepayment With(Nolock) 
                                              where 0 = 0 and id  = @id ");
            cmd.Parameters.AddWithValue("@id", id);
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    int.TryParse(dt.Rows[0]["installment_price"].ToString(), out _i);
                    int.TryParse(dt.Rows[0]["paid_price"].ToString(), out _p);

                    lb_duedate.Text = Convert.ToDateTime (dt.Rows[0]["due_date"]).ToString("yyyy-MM-dd");
                    paydate.Text = Convert.ToDateTime(dt.Rows[0]["due_date"]).ToString("yyyy-MM-dd");
                    paid_price.Text = (_i- _p).ToString();
                    lb_paid_price.Text  = dt.Rows[0]["paid_price"].ToString();
                }
            }
        }
    }
    

    protected void btnsave_Click(object sender, EventArgs e)
    {
        int _paid_price = 0;
        int _paided_price = 0;
        int.TryParse(lb_paid_price.Text, out _paided_price);        

        if (!int.TryParse(paid_price.Text, out _paid_price) || _paid_price <=0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('付款金額有誤，請重新輸入');</script>", false);
            return;
        }
        string strErr = string.Empty;
        if (id != "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@paid", true);
            cmd.Parameters.AddWithValue("@paid_price", _paid_price+ _paided_price);
            cmd.Parameters.AddWithValue("@pay_date", paydate.Text);
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "id", id);
            cmd.CommandText = dbAdapter.genUpdateComm("ttRepayment", cmd);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr =  Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");
            }
        }
        

        if (strErr == "")
        {   
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('付款成功'); parent.$.fancybox.close();parent.location.reload(true);</script>", false);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('付款失敗:" + strErr + "');</script>", false);
        }
        return;
    }
}