﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using BarcodeLib;
using System.Drawing;
using System.IO;
using BObject.Bobjects;

public partial class palletmag_2 : System.Web.UI.Page
{
   

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
                ssMaster_code = "0000000";
            #region 
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";
                if (ssManager_type == "4" || ssManager_type == "5" )
                {
                    if (ssMaster_code.Substring(3, 4) == "0000")
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code.Substring(0, 3));
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code);
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                }
                    
                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = 0 {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , master_code + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 order by  master_code", wherestr);
                second_code.DataSource = dbAdapter.getDataTable(cmd);
                second_code.DataValueField = "master_code";
                second_code.DataTextField = "showname";
                second_code.DataBind();
            }
            second_code.SelectedValue = ssMaster_code;
            
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                second_code.Enabled = true;
                second_code.Items.Insert(0, new ListItem("全部", ""));
            }
               
            #endregion


            
            
            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue  = Request.QueryString["second_code"];
            }

            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }

            readdata();
        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        if (check_number.Text != "")
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            strWhereCmd += " and A.check_number = @check_number";
            querystring += "&check_number=" + check_number.Text;
        }
        
        if (second_code.SelectedValue != "")
        {
            cmd.Parameters.AddWithValue("@customer_code", second_code.SelectedValue);
            strWhereCmd += " and A.customer_code like @customer_code+'%' ";
            querystring += "&second_code=" + second_code.SelectedValue;
        }

        #endregion

        cmd.CommandText = string.Format(@"SELECT distinct A.pricing_type,A.request_id
                                                --,ROW_NUMBER() OVER(ORDER BY A.print_date, A.check_number,CONVERT(INT,A.sub_check_number) DESC) AS ROWID                                               
                                                ,CONVERT(CHAR(10),A.supplier_date,111) supplier_date, A.check_number , LEFT( A.customer_code,3) as sendstation , A.receive_contact , 
                                                 CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(C.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation2,
                                                 A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area ,
                                                 CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                                 A.pieces , A.plates , A.cbm ,
                                                 B.code_name , A.customer_code , C.customer_shortname ,
                                                 A.send_contact,A.send_city , A.send_area , A.send_address , A.cancel_date,A.add_transfer
                                                ,(SELECT COUNT(1) FROM ttDeliveryScanLog With(Nolock) WHERE check_number = A.check_number) logNum 
                                                ,A.uuser, D.user_name ,A.print_date , A.udate
                                          from tcDeliveryRequests A With(Nolock)
                                          inner join ttPlletLog P with(Nolock) on A.request_id = P.pallet_id
                                          left join tbItemCodes B With(Nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C With(Nolock) on C.customer_code = A.customer_code 
                                          left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                          where 1= 1 
                                          {0} order by A.print_date, A.check_number", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

   

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "cmdPrint":
                string ErrStr = string.Empty;
                string request_id = e.CommandArgument.ToString();
                //if (option.SelectedValue == "1")
                //{
                //捲筒
                //PrintReelLabel(request_id.ToString());
                //}
                //else
                //{
                PrintLabel(request_id.ToString());
                //}
                break;
        }
    }


    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";

        querystring += "&second_code=" + second_code.SelectedValue;

        if (check_number.Text != "")
        {
            querystring += "&check_number=" + check_number.Text;
        }
        Response.Redirect(ResolveUrl("~/palletmag_2.aspx?search=yes" + querystring));
    }
    

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    /// <summary>
    /// 列印標籤(以板列印)
    /// </summary>
    /// <param name="print_requestid">託運單id(以","隔開)</param>
    /// <param name="format">紙張格式(預設A4,一式6筆)</param>
    private void PrintLabel(string print_requestid, string format = "0")
    {
        string ttErrStr = "";
        NPOITable ttExlApp = null;
        string strSampleFileFullName = string.Empty;                                      //範本檔路徑



        SqlCommand cmd = new SqlCommand();
        string wherestr = string.Empty;
        DataTable DT = null;
        if (print_requestid != "")
        {
            // cmd.Parameters.AddWithValue("@check_number", print_chknum);
            wherestr = " and request_id in(" + print_requestid + ")";
            cmd.CommandText = string.Format(@"select A.supplier_date  , A.print_date, A.check_number , A.receipt_flag, A.pallet_recycling_flag, A.arrive_assign_date,
                                          A.collection_money,A.arrive_to_pay_freight,A.arrive_to_pay_append,
                                          A.receive_contact ,  A.receive_tel1 , A.receive_tel1_ext , A.receive_tel2, A.receive_city , A.receive_area ,
                                          CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                          A.pricing_type ,A.pieces , A.plates , A.cbm ,
                                          A.subpoena_category, A.invoice_memo, A.invoice_desc,
                                          B.code_name  , A.customer_code , C.customer_shortname ,
                                          A.send_contact,A.send_tel,
                                          A.send_city , A.send_area , A.send_address ,A.area_arrive_code
                                          from tcDeliveryRequests A WITH(Nolock)
                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C on C.customer_code = A.customer_code
                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} order by  A.check_number", wherestr);
            DT = dbAdapter.getDataTable(cmd);
        }
        if (DT != null)
        {
            try
            {
                #region 設定範本檔路徑
                switch (format)
                {
                    case "0":  //A4 (一式6筆託運標籤)
                        strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label.xls";
                        break;
                    case "1":  //捲筒列印
                        strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_1.xls";
                        break;
                    case "2":  //A4 (一式1筆託運標籤)
                        strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_2.xls";
                        break;
                }

                if (strSampleFileFullName.EndsWith(".xls") == false) strSampleFileFullName += ".xls";
                #endregion

                try
                {
                    ttExlApp = new NPOITable(strSampleFileFullName);
                    ttExlApp.OpenFile();

                    string print_date = "";         //發送日期
                    Boolean receipt_flag = false;   //是否回單
                    Boolean pallet_recycling_flag = false; //是否棧板回收
                    string supplier_date = "";      // 配送日期
                    string pricing_type = "";       //計價模式
                    string check_number = "";       //貨號
                    string subpoena_category = "";  //傳票類別
                    string subpoena_category_name = "";
                    string receive_tel1 = "";       //收件人電話
                    string receive_tel1_ext = "";
                    string receive_tel2 = "";
                    string receive_contact = "";    //收件人
                    string receive_city = "";       //收件地址-縣市
                    string receive_area = "";       //收件地址-鄉鎮市區
                    string receive_address = "";    //收件地址-路街巷弄號
                    int pieces = 0;                 //件數
                    int plates = 0;                 //板數
                    int cbm = 0;                    //才數
                    int collection_money = 0;       //代收金 
                    int arrive_to_pay_freight = 0;  //到付運費
                    int arrive_to_pay_append = 0;   //到付追加
                    string send_contact = "";       //寄件人
                    string send_tel = "";           //寄件人電話
                    string send_city = "";          //寄件人地址-縣市
                    string send_area = "";          //寄件人地址-鄉鎮市區
                    string send_address = "";       //寄件人地址-號街巷弄號
                    string invoice_desc = "";       //備註
                    string area_arrive_code = "";   //到著碼
                    string ttStr = "";
                    int addcolumn = 0;
                    string pricing_type_name = "";
                    string StartFrom = "1";
                    switch (format)
                    {
                        case "0":
                            ttExlApp.SRow = 1;
                            ttExlApp.SCol = 1;
                            ttExlApp.Line = 42;
                            ttExlApp.Column = 19;
                            ttExlApp.NextPage();
                            ttExlApp.GoToLine(1);
                            int s_tmp;
                            if (!int.TryParse(StartFrom, out s_tmp)) s_tmp = 1;

                            #region 

                            #region 清除前面框線
                            switch (s_tmp)
                            {
                                case 2:
                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    break;
                                case 3:
                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(16);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(18);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(21);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(24);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(25);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    break;
                                case 4:
                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(16);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(18);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(21);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(24);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(25);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(30);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(32);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(35);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(38);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(39);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.RowC -= 38;
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                    break;
                                case 5:
                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(16);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(18);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(21);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(24);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(25);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(30);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(32);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(35);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(38);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(39);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    break;
                                case 6:
                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(16);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(18);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(21);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(24);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(25);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(30);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(32);
                                    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(35);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(38);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(39);
                                    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                    ttExlApp.GoToLine(2);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(4);
                                    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(7);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(10);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(11);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(16);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(18);
                                    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(21);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(24);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(25);
                                    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    ttExlApp.GoToLine(1);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                    break;

                            }
                            #endregion

                            if (DT.Rows.Count > 0)
                            {
                                for (int i = 0; i < DT.Rows.Count; i++)
                                {
                                    pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                    plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                    pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                    //int printplates = 1;
                                    int printplates = pricing_type == "01" ? plates : pricing_type == "02" ? pieces : 1;    //如果是論板，那有幾板就要印幾張; 如果是論件，那有幾件就要印幾張
                                    for (int j = 1; j <= printplates; j++)
                                    {
                                        switch (s_tmp)
                                        {
                                            case 1:
                                                ttExlApp.GoToLine(1);
                                                addcolumn = 0;
                                                break;
                                            case 2:

                                                ttExlApp.GoToLine(15);
                                                addcolumn = 0;
                                                break;
                                            case 3:
                                                ttExlApp.GoToLine(29);
                                                addcolumn = 0;
                                                break;
                                            case 4:
                                                ttExlApp.GoToLine(1);
                                                addcolumn = 10;
                                                break;
                                            case 5:
                                                ttExlApp.GoToLine(15);
                                                addcolumn = 10;
                                                break;
                                            case 6:
                                                ttExlApp.GoToLine(29);
                                                addcolumn = 10;
                                                break;
                                        }

                                        print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
                                        receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
                                        pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
                                        supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
                                        //pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                        check_number = DT.Rows[i]["check_number"].ToString();
                                        subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
                                        subpoena_category_name = DT.Rows[i]["code_name"].ToString();
                                        receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
                                        receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
                                        receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
                                        receive_contact = DT.Rows[i]["receive_contact"].ToString();
                                        receive_city = DT.Rows[i]["receive_city"].ToString();
                                        receive_area = DT.Rows[i]["receive_area"].ToString();
                                        receive_address = DT.Rows[i]["address"].ToString();
                                        //pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                        //plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                        cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
                                        collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
                                        arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
                                        arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
                                        send_contact = DT.Rows[i]["send_contact"].ToString();
                                        send_tel = DT.Rows[i]["send_tel"].ToString();
                                        send_city = DT.Rows[i]["send_city"].ToString();
                                        send_area = DT.Rows[i]["send_area"].ToString();
                                        send_address = DT.Rows[i]["send_address"].ToString();
                                        invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
                                        area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
                                        ttStr = "";

                                        switch (pricing_type)
                                        {
                                            case "01":  //論板
                                                pricing_type_name = "論板";
                                                break;
                                            case "02":  //論件
                                                pricing_type_name = "論件";
                                                break;
                                            case "04":  //論小板
                                                pricing_type_name = "論小板";
                                                break;
                                            case "03":  //論才
                                                pricing_type_name = "論才";
                                                break;
                                            case "05":  //專車
                                                pricing_type_name = "專車";
                                                supplier_date = (DT.Rows[i]["arrive_assign_date"] != DBNull.Value) ? Convert.ToDateTime(DT.Rows[i]["arrive_assign_date"]).ToString("MMdd") : Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("MMdd"); //指配日期 = 發送日期,專車通常都是當天到貨

                                                break;
                                        }
                                        ttStr += pricing_type_name;

                                        if (receipt_flag == true)
                                            ttStr += "回單";
                                        if (pallet_recycling_flag == true)
                                            ttStr += "棧板回收";
                                        ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
                                        switch (subpoena_category)
                                        {
                                            //case "11":   //元付
                                            //    break;
                                            case "21":   //到付
                                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
                                                break;
                                            case "41":   //代收貨款
                                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
                                                break;
                                            case "25":   //元付-到付追加
                                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
                                                break;
                                        }
                                        ttExlApp.RowC += 1;
                                        switch (pricing_type)
                                        {
                                            case "01":  //論板
                                            case "02":  //論件
                                            case "04":  //論小板
                                            case "05":  //專車
                                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
                                                ttExlApp.RowC += 1;
                                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
                                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                                break;
                                            case "03":  //論才
                                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
                                                ttExlApp.RowC += 1;
                                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
                                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                                break;
                                        }
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
                                        ttExlApp.RowC += 1;
                                        ttStr = "";
                                        if (receive_tel1 != "") ttStr += receive_tel1;
                                        if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
                                        if (receive_tel2 != "")
                                        {
                                            ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
                                        }
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
                                        ttExlApp.RowC += 1;
                                        ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);



                                        switch (s_tmp)
                                        {
                                            case 3:
                                                ttExlApp.RowC -= 40;
                                                s_tmp += 1;
                                                break;
                                            case 6:
                                                if ((i < DT.Rows.Count - 1) || (j < printplates))
                                                {
                                                    ttExlApp.NextPage();
                                                }
                                                s_tmp = 1;
                                                break;
                                            default:
                                                s_tmp += 1;
                                                break;
                                        }
                                    }

                                }


                                #region 清除後面框線

                                switch (s_tmp)
                                {
                                    //case 1:
                                    //    ttExlApp.GoToLine(2);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(4);
                                    //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(7);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(10);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(11);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(16);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(18);
                                    //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(21);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(24);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(25);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(30);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(32);
                                    //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(35);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(38);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(39);
                                    //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(1);
                                    //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                    //    ttExlApp.GoToLine(2);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(4);
                                    //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(7);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(10);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(11);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(16);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(18);
                                    //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(21);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(24);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(25);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(30);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(32);
                                    //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(35);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(38);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(39);
                                    //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                    //    ttExlApp.GoToLine(1);
                                    //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                    //    break;
                                    case 2:
                                        ttExlApp.GoToLine(16);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(18);
                                        ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(21);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(24);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(25);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(15);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                        ttExlApp.GoToLine(2);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(4);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(7);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(10);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(11);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(16);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(18);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(21);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(24);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(25);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(1);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        break;
                                    case 3:
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(29);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                        ttExlApp.GoToLine(2);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(4);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(7);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(10);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(11);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(16);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(18);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(21);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(24);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(25);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(1);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        break;
                                    case 4:
                                        ttExlApp.GoToLine(2);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(4);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(7);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(10);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(11);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(16);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(18);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(21);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(24);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(25);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(1);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                        break;
                                    case 5:
                                        ttExlApp.GoToLine(16);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(18);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(21);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(24);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(25);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(15);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        break;
                                    case 6:

                                        ttExlApp.GoToLine(30);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(32);
                                        ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(35);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(38);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(39);
                                        ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                        ttExlApp.GoToLine(29);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                        ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                        break;

                                }
                                #endregion
                            }
                            #endregion
                            break;
                        case "1":
                            break;
                        case "2":
                            ttExlApp.SRow = 1;
                            ttExlApp.SCol = 1;
                            ttExlApp.Line = 13;
                            ttExlApp.Column = 9;
                            ttExlApp.NextPage();
                            ttExlApp.GoToLine(1);

                            if (DT.Rows.Count > 0)
                            {
                                for (int i = 0; i < DT.Rows.Count; i++)
                                {
                                    print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
                                    receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
                                    pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
                                    supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
                                    pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                    check_number = DT.Rows[i]["check_number"].ToString();
                                    subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
                                    subpoena_category_name = DT.Rows[i]["code_name"].ToString();
                                    receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
                                    receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
                                    receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
                                    receive_contact = DT.Rows[i]["receive_contact"].ToString();
                                    receive_city = DT.Rows[i]["receive_city"].ToString();
                                    receive_area = DT.Rows[i]["receive_area"].ToString();
                                    receive_address = DT.Rows[i]["address"].ToString();
                                    pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                    plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                    cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
                                    collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
                                    arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
                                    arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
                                    send_contact = DT.Rows[i]["send_contact"].ToString();
                                    send_tel = DT.Rows[i]["send_tel"].ToString();
                                    send_city = DT.Rows[i]["send_city"].ToString();
                                    send_area = DT.Rows[i]["send_area"].ToString();
                                    send_address = DT.Rows[i]["send_address"].ToString();
                                    invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
                                    area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


                                    ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
                                    ttStr = "";

                                    switch (pricing_type)
                                    {
                                        case "01":  //論板
                                            pricing_type_name = "論板";
                                            break;
                                        case "02":  //論件
                                            pricing_type_name = "論件";
                                            break;
                                        case "04":  //論小板
                                            pricing_type_name = "論小板";
                                            break;
                                        case "03":  //論才
                                            pricing_type_name = "論才";
                                            break;
                                        case "05":  //專車
                                            pricing_type_name = "專車";
                                            supplier_date = (DT.Rows[i]["arrive_assign_date"] != DBNull.Value) ? Convert.ToDateTime(DT.Rows[i]["arrive_assign_date"]).ToString("MMdd") : Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("MMdd"); //指配日期 = 發送日期,專車通常都是當天到貨
                                            break;
                                    }
                                    ttStr += pricing_type_name;

                                    if (receipt_flag == true)
                                        ttStr += "回單";
                                    if (pallet_recycling_flag == true)
                                        ttStr += "棧板回收";
                                    ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
                                    ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
                                    switch (subpoena_category)
                                    {
                                        //case "11":   //元付
                                        //    break;
                                        case "21":   //到付
                                            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
                                            break;
                                        case "41":   //代收貨款
                                            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
                                            break;
                                        case "25":   //元付-到付追加
                                            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
                                            break;
                                    }
                                    ttExlApp.RowC += 1;
                                    switch (pricing_type)
                                    {
                                        case "01":  //論板
                                        case "02":  //論件
                                        case "04":  //論小板
                                        case "05":  //專車
                                            ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
                                            ttExlApp.RowC += 1;
                                            ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
                                            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                            ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                            break;
                                        case "03":  //論才
                                            ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
                                            ttExlApp.RowC += 1;
                                            ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
                                            ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                            ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                            break;
                                    }
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
                                    ttExlApp.RowC += 1;
                                    ttStr = "";
                                    if (receive_tel1 != "") ttStr += receive_tel1;
                                    if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
                                    if (receive_tel2 != "")
                                    {
                                        ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
                                    }
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
                                    ttExlApp.RowC += 1;
                                    ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);

                                    if (i < DT.Rows.Count - 1) ttExlApp.NextPage();


                                }
                            }
                            break;
                    }

                    ttExlApp.DeleteTable(1);
                    //Workbook workbook = new Workbook();
                    //workbook.LoadFromFile(filename);
                    //workbook.SaveToFile("result.pdf", FileFormat.PDF);

                    FileInfo fi = new FileInfo(strSampleFileFullName);
                    string FileName = String.Empty;
                    if (Request.Url.Host.IndexOf("localhost") >= 0)
                    {
                        //本機→轉PDF
                        FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                        string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                        string pathDownload = Path.Combine(pathUser, "Downloads", FileName);
                        ttExlApp.Save(pathDownload, TableFormat.TFPDF);
                        BasePage.popDownload(Path.Combine(pathUser, "Downloads"), FileName, true);

                    }
                    else
                    {
                        //Server→轉Excel
                        Boolean IsTest = false; //是否為測試模式
                        FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                        string ExportPath = "files/" + FileName;

                        try
                        {
                            ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);

                            if (!IsTest)
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            }
                        }
                        catch (Exception ex)
                        {
                            string strErr = string.Empty;
                            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                            strErr = "標籤列印-" + Request.RawUrl + strErr + ": " + ex.ToString();


                            //錯誤寫至Log
                            PublicFunction _fun = new PublicFunction();
                            _fun.Log(strErr, "S");
                        }
                    }

                    //string outputFolder = string.Empty;                                             //匯出檔路徑
                    //string outputFileName = string.Empty;
                    //FileInfo fi = new FileInfo(strSampleFileFullName);
                    //outputFolder = Request.PhysicalApplicationPath + @"files\";
                    //outputFileName =string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);

                    //try
                    //{ ttExlApp.Save(outputFolder + outputFileName, TableFormat.TFPDF); }
                    //catch (Exception ex)
                    //{
                    //    string strErr = string.Empty;
                    //    if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                    //    strErr = "標籤列印-" + Request.RawUrl + strErr + ": " + ex.ToString();


                    //    //錯誤寫至Log
                    //    PublicFunction _fun = new PublicFunction();
                    //    _fun.Log(strErr, "S");

                    //    //outputFileName = "label-20170427071500.xls";
                    //}

                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + ":10080/files/" + outputFileName  + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);  //iis                    
                    //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + "/files/" + outputFileName  + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);   //local


                    //Response.AddHeader(
                    // "Content-Disposition",
                    // string.Format("attachment; filename=label-20170427071500.pdf"));
                    //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                    //Response.WriteFile(Request.PhysicalApplicationPath + @"files\abel-20170427071500.pdf");
                    //Response.End();


                }
                catch (Exception ex)
                {
                    ttErrStr = "開啟範本失敗-E-" + ex.Message;
                    ttExlApp = null;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
                }

            }
            catch (Exception ex)
            {
                //ExceptionAdapter.logSysException(ex);
                //throw;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.Message + "');</script>", false);
            }
        }
        else
        {

            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可列印標籤');</script>", false);
            //return;
        }


    }

    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {

        using (var barcode = new Barcode()
        {

            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 250,
            Height = 50,
            LabelFont = new Font("verdana", 10f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            return barcode.Encode(TYPE.Codabar, content);
        }
    }


}