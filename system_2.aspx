﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSystem.master" AutoEventWireup="true" CodeFile="system_2.aspx.cs" Inherits="system_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">車輛基本資料</h2>

            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr>
                        <td>公司別</td>
                        <td>車號</td>
                        <td>車型</td>
                        <td>車種</td>
                        <td>站所代碼</td>
                        <td>產權歸屬</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

