﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_trace.ascx.cs" Inherits="uc_uc_trace" %>


<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
<link href="../css/font-awesome.min.css" rel="stylesheet">
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/templatemo-style.css" rel="stylesheet">

<!-- jQuery文件 -->
<script src="../js/jquery-1.11.2.min.js"></script>
<script src="../js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/source/jquery.fancybox.js?v=2.1.5"></script>
<script type="text/javascript" src="../js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../js/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" href="../css/jquery.ui.datepicker.css">
<%--<link rel="stylesheet" href="css/jquery-ui.css">--%>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/Model_Base.js?"></script>
<script src="../js/StorageCurrentPage.js?"></script>
<script src="../js/datepicker-zh-TW.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.enter2tab.js"></script>
<link href="../js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
<script src="../js/chosen_v1.6.1/chosen.jquery.js"></script>
<link rel="stylesheet" href="../css/simplePagination.css">
<link rel="stylesheet" href="../css/junfucss.css">
<script src="../js/jquery.simplePagination.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js"></script>
<link href="../css/build.css" rel="stylesheet" />





<link href="../js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" />
<script src="../js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
<script src="../js/timepicker/jquery-ui-sliderAccess.js" type="text/javascript"></script>
<script src="../js/timepicker/jquery-ui-timepicker-zh-TW.js" type="text/javascript"></script>




<script>
    $(document).ready(function () {
        $('.fancybox').fancybox();
        $("#Imglink").fancybox({
            'width': 500,
            'height': 500,
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'hideOnOverlayClick': 'false',
            'type': 'iframe',
            'onClosed': function () {
                parent.location.reload(true);
            }
        });


        var times = new Date();
        $('.datetimepicker').prop("readonly", true).datetimepicker({
            //showSecond: true, //顯示秒  
            timeFormat: 'HH:mm', //格式化時間  
            dateFormat: 'yy/mm/dd',
            defaultDate: (new Date()),  //預設當日
            hour: times.getHours(),
            minute: times.getMinutes(),
            controlType: "select"
        });

        $(".btn_view").click(function () {
            showBlockUI();
        });

        function openModal() {
            $('#UpdateAddressModal').modal('show');
        }

    });
    $.fancybox.update();

    function showBlockUI() {
        $.blockUI({
            message: '<p><h1>查詢中,請稍後<h1></p>',
            css:
            {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    function openModal() {
        $('#UpdateAddressModal').modal('show');
    }

    $(document).on("click", "td[data-th='員工']", function (event) {
        if ($(this).text().startsWith("PP003")) {
            var checkNumber = $(this).parents("tr").find("td[data-th='check_number']").text();
            var postData = { checkNumber: checkNumber };
            $.ajax({
                type: "POST",
                url: "LT_trace_1.aspx/GetPelicanCheckNumber",
                data: JSON.stringify(postData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d && data.d.length > 0) {
                        window.open("http://query2.e-can.com.tw/self_link/id_link.asp?txtMainID=" + data.d);
                    }
                }
            });
        }
    });

    $(document).on("dblclick", "td[data-th='快遞單號']", function (event) {
        if ($(this).text() != "" && !$(this).text().startsWith("202")) {
            window.open("https://postserv.post.gov.tw/pstmail/main_mail.html");
        }


    });


    $(document).on("click", "td[data-th='快遞單號']", function (event) {
        if ($(this).text().startsWith("202")) {
            window.open("http://supererp.junfu.asia/LT_trace_1.aspx?check_number=" + $(this).text());
        }

    });




</script>




<style type="text/css">
    input[type="radio"] {
        display: inherit;
    }

    .tdtype {
        width: 100px;
    }

    .fancybox-inner {
        width: 1410px !important
    }

    .fancybox-type-iframe {
        width: 1450px !important;
        left: 250px !important
    }
</style>

<div class="row">
    <h2 class="margin-bottom-10">
        <asp:Label ID="lbTitle" runat="server" Text=""></asp:Label></h2>
    <div class="templatemo-login-form">
        <div class="row form-group">
            <div class="col-lg-4 col-md-6 form-group form-inline">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rb1" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged" Checked="true" />
                <%--<label for="check_number">貨　號</label>--%>
                <asp:Label ID="Label1" runat="server" Text="貨　號"></asp:Label>
                <asp:TextBox ID="check_number" runat="server" MaxLength="20"></asp:TextBox>
                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="check_number" ForeColor="Red" ValidationGroup="validate">請輸入貨號</asp:RequiredFieldValidator>--%>
            </div>
            <div class="col-lg-4 col-md-6 form-group form-inline">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rb2" Text="" GroupName="rbd" RepeatDirection="horizontal" AutoPostBack="true" runat="server" OnCheckedChanged="rb_CheckedChanged" />
                <%--<label for="check_number">訂單編號</label>--%>
                <asp:Label ID="Label4" runat="server" Text="訂單編號"></asp:Label>
                <asp:TextBox ID="order_number" runat="server" MaxLength="20" Enabled="false"></asp:TextBox>
                <asp:TextBox ID="check_number_in_order_number" runat="server" Style="display: none"></asp:TextBox>
                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="order_number" ForeColor="Red" ValidationGroup="validate">訂單編號</asp:RequiredFieldValidator>--%>
            </div>
            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                <label for="dlsub_chknum">次貨號</label>
                <asp:DropDownList ID="dlsub_chknum" runat="server" CssClass="form-control">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="001">001</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="search" CssClass="templatemo-blue-button btn" runat="server" Text="查 詢" ValidationGroup="validate" OnClick="search_Click" />
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-4 col-md-6 form-group form-inline">
                <label for="Imglink">
                    簽單查詢
                </label>
                <asp:HyperLink runat="server" ID="Imglink" class="fancybox fancybox.iframe">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" ToolTip="簽單查詢" />
                </asp:HyperLink>
                <div id="NonReceipt" runat="server" style="display: none; color: red">此筆貨號尚未有簽單</div>
            </div>
            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 1px; top: 0px">
                <label for="RtnImglink">回單查詢</label>
                <asp:LinkButton ID="RtnImglink" runat="server">
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/檢視.jpg" Height="20px" Width="20px" ToolTip="回單查詢" />
                </asp:LinkButton>
                <a href="fileupload_receipt.aspx" class="fancybox fancybox.iframe " id="rtnclick2" runat="server" visible="true"><span class="btn btn-warning">回單上傳</span></a>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 1px; top: 0px">
                <label for="RtnImglink">補簽收單上傳</label>
                <a href="../fileupload_signreceipt.aspx" class="fancybox fancybox.iframe " id="rtnclick" runat="server" visible="true"><span class="btn btn-warning">補簽單上傳</span></a>
            </div>
        </div>

        <hr>
        <div style="overflow: auto; max-height: 300px;">
            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr class="tr-only-hide">
                        <td>發送站</td>
                        <td>到著站</td>
                        <td>發送日</td>
                        <td>出貨日期</td>
                        <td>指定配送</td>
                        <td>指配日期</td>
                        <td>指配時間</td>
                        <td>貨號</td>
                        <td>訂單編號</td>
                        <td>件數</td>
                    </tr>
                </thead>
                <asp:Repeater ID="Delivery_Info_Repeater" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation").ToString())%></td>
                            <td data-th="到著站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrivestation").ToString())%></td>
                            <td data-th="發送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date").ToString())%></td>
                            <td data-th="出貨日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ship_date").ToString())%></td>
                            <td data-th="指定配送"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.holiday_delivery").ToString())%></td>
                            <td data-th="指配日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date").ToString())%></td>
                            <td data-th="指配時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.time_period").ToString())%></td>
                            <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                            <td data-th="訂單編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                            <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (Delivery_Info_Repeater.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="12" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr class="tr-only-hide">
                        <td>傳票區分</td>
                        <td>代收金額</td>
                        <td>對應單號</td>
                    </tr>
                </thead>
                <asp:Repeater ID="Delivery_Info_Repeater2" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="傳票區分"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subpoena_category_name").ToString())%></td>
                            <td data-th="代收金額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.代收金額").ToString())%></td>
                            <td data-th="對應單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.return_check_number").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (Delivery_Info_Repeater2.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="12" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
        </div>


        <table class="table table-striped table-bordered templatemo-user-table">
            <thead>
                <tr>
                    <td>寄件人資料</td>
                    <td>收件人資料</td>
                    <td>處置說明</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="lbSend" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbReceiver" runat="server"></asp:Label>
                    </td>
                    <td>
                        <%--<asp:Button ID="btnDetail" runat="server" Text="查詢/新增處置說明" OnClick="btnDetail_Click" />--%>
                        <asp:Button ID="Bohotype" runat="server" Text="補貨態" Visible="false" OnClick="Bohotype_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div id="divMaster" runat="server" class="templatemo-login-form panel panel-default">
            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr>
                        <td>NO.</td>
                        <td>作業時間</td>
                        <td>作業</td>
                        <td>站所</td>
                        <%-- <td>到著/發送站</td>--%>
                        <td>件數</td>
                        <td>班次代號</td>
                        <td>狀態</td>
                        <td>運輸方式</td>
                        <td>註區</td>
                        <td>員工</td>

                        <td>電話</td>
                        <td>快遞單號</td>
                        <td>物流編號</td>
                        <td>異動人</td>
                        <td>功能</td>
                    </tr>
                </thead>
                <asp:Repeater ID="New_List_02" runat="server" OnItemCommand="New_List_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="check_number" hidden="hidden"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                            <td data-th="作業時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_date").ToString())%></td>
                            <td data-th="作業"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scan_name").ToString())%></td>
                            <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <%-- <td data-th="到著/發送站"></td>--%>
                            <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.quant").ToString())%></td>
                            <td data-th="班次代號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.shift").ToString())%></td>
                            <td data-th="狀態"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status").ToString())%></td>
                            <td data-th="運輸方式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.write_off_type").ToString())%></td>
                            <td data-th="註區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_type_code").ToString())%></td>
                            <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%>  <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_name").ToString())%></td>
                            <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tel").ToString())%></td>
                            <td data-th="快遞單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tracking_number").ToString())%></td>
                            <td data-th="物流編號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.order_number").ToString())%></td>
                            <td data-th="異動人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cuser").ToString())%></td>

                            <td data-th="">
                                <%--<asp:Button ID="btn_del" Visible='<%#(DataBinder.Eval(Container, "DataItem.del_status").ToString() =="1")  ? true : false  %>' CssClass="btn btn-danger _d_btn" CommandName="cmddelete" OnClientClick="return confirm('確定要刪除此筆資料嗎?');" runat="server" Text="刪 除" />--%>
                                <asp:label ID="btn_del" Visible='<%#(DataBinder.Eval(Container, "DataItem.del_status").ToString() =="1")  ? true : false  %>'  runat="server" Text="補貨態" />
                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.log_id").ToString())%>' />

                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List_02.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="11" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
        </div>

        <div id="Inventory" runat="server" visible="false">
            <table class="table table-striped table-bordered templatemo-user-table">
                <thead>
                    <tr>
                        <td>No.</td>
                        <td>作業時間</td>
                        <td>作業</td>
                        <td>站所</td>
                        <td>員工</td>
                    </tr>
                </thead>
                <asp:Repeater ID="Inventory_List" runat="server" OnItemCommand="New_List_ItemCommand">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="作業時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scanning_dt").ToString())%></td>
                            <td data-th="作業">盤點</td>
                            <td data-th="站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_name").ToString())%></td>
                            <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (Inventory_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="5" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
        </div>


        <div id="divDetail" runat="server" visible="true" class="templatemo-login-form panel panel-default">
            <table class="table table-striped table-bordered templatemo-user-table ">
                <thead>
                    <tr class="tr-only-hide">
                        <td>NO.</td>
                        <td>輸入時間</td>
                        <td>狀況類別</td>
                        <td>來源</td>
                        <td>處置說明</td>
                        <td>員工</td>
                    </tr>
                </thead>
                <asp:Repeater ID="Datail_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                            <td data-th="輸入時間"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate").ToString())%></td>
                            <td data-th="狀況類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.status_desc").ToString())%></td>
                            <td data-th="來源"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.origin").ToString())%></td>
                            <td data-th="處置說明" style="text-align: left"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dispose_desc").ToString())%></td>
                            <td data-th="員工"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (Datail_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="5" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            <hr />
            <table class="table table-striped table-bordered templatemo-user-table">
                <tbody>
                    <tr>
                        <td style="width: 20%">狀況類別</td>
                        <td>
                            <div class="col-lg-12 form-group form-inline" style="text-align: left;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="dlP1" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="dlP1_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="dlP1" ForeColor="Red" ValidationGroup="validetail">請選擇處理說明</asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="dlP2" runat="server" class="form-control" AutoPostBack="True" OnSelectedIndexChanged="dlP2_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="dlP2" ForeColor="Red" ValidationGroup="validetail">請選擇處理說明</asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="dlP3" runat="server" class="form-control"></asp:DropDownList>
                                        <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="dlP3" ForeColor="Red" ValidationGroup="validetail">請選擇處理說明</asp:RequiredFieldValidator>
                                        <asp:Label ID="ErrLabel" Visible="False" Text="" runat="server" ForeColor="Red"></asp:Label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>處置說明</td>
                        <td>
                            <asp:TextBox ID="dispose_desc" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSaveDatail" runat="server" Text="存檔" ValidationGroup="validetail" OnClick="btnSaveDatail_Click" />
                            <%--<asp:Button ID="btnExit" runat="server" Text="離開" OnClick="btExit_Click" />--%>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
       
        <div id="Hotype" runat="server" visible="false" class="templatemo-login-form panel panel-default">

            <table class="table table-striped table-bordered templatemo-user-table">
                <tbody>
                    <tr>
                        <td class="tdtype">作業</td>
                       <td style="text-align: left">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="scanItem" runat="server" AutoPostBack="true" OnSelectedIndexChanged="scanItem_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>作業時間</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="DateA" class="form-control datetimepicker" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="DateA" ForeColor="Red" ValidationGroup="validetail2">請選擇作業時間</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>站所</td>
                        <td style="text-align: left">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="sta" runat="server" AutoPostBack="true" OnSelectedIndexChanged="sta_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr style="display: none">
                        <td>件數</td>
                        <td>
                            <asp:TextBox ID="CountA" class="form-control" runat="server" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>狀態</td>
                        <td style="text-align: left">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="states" runat="server"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td>註區</td>
                        <td>
                            <asp:TextBox ID="AreaA" class="form-control" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>員工</td>
                        <td style="text-align: left">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="emp" runat="server"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="Button1" runat="server" Text="存檔" ValidationGroup="validetail2" OnClick="btnHotypeSaveDatail_Click" />
                            <asp:Button ID="Button2" runat="server" Text="離開" OnClick="btExittype_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        </table>
    </div>


    <div class="modal fade" id="UpdateAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">修改地址</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>新地址：</span>

                    <asp:UpdatePanel ID="UpdateAddressPanel" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="City" OnSelectedIndexChanged="UpdateAreaDropDown" AutoPostBack="true"></asp:DropDownList>
                            <asp:DropDownList runat="server" ID="Area"></asp:DropDownList>
                            <asp:TextBox ID="NewAddress" runat="server"></asp:TextBox>
                                                                        <asp:Label ID="lbSpecialAreaId" runat="server"  Visible="false" ></asp:Label>
                                            <asp:Label ID="lbSpecialAreaFee" runat="server"  Visible="false" ></asp:Label>
                            <asp:Label ID="lbAreaArriveCode" runat="server"  Visible="false" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
                    <asp:Button ID="Button3" CssClass="button" runat="server" Text="儲存" OnClick="UpdateAddress" />
                </div>
            </div>
        </div>
    </div>
