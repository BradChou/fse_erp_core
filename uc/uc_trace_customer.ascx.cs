﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using RestSharp;

public partial class uc_uc_trace : System.Web.UI.UserControl
{
    bool check_address = true;
    string areaArriveCode = "";
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public bool isNeedInventoryTable
    {
        get { return manager_type != "5" & Less_than_truckload == "1"; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public Label Title
    {
        get { return lbTitle; }
    }

    public string DeliveryType
    {
        // 配送類型 D:正物流  R:逆物流
        get { return ViewState["DeliveryType"].ToString(); }
        set { ViewState["DeliveryType"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            manager_type = Session["manager_type"].ToString(); //管理單位類別            

            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }



            divMaster.Visible = true;
            ImageButton1.Visible = false;



            readdata();


        }

    }

    protected void search_Click(object sender, EventArgs e)
    {

        //鎖權限
        string manager_type = Session["manager_type"].ToString(); //管理單位類別   
        string supplier_code = string.Empty;
        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                break;
            case "4":                 //站所主管只能看該站所下的客戶
                using (SqlCommand cmd2 = new SqlCommand())
                {
                    cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                    DataTable dt = dbAdapter.getDataTable(cmd2);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        supplier_code = dt.Rows[0]["station_code"].ToString();
                    }
                }
                break;
            case "5":
                supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                break;
        }

        if (rb1.Checked)
        {
            check_number.Text = check_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入貨號');</script><noscript></noscript>");
                return;
            }

            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('貨號有誤');</script><noscript></noscript>");
                return;
            }

            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE check_number = @check_number and (@customer_code = '' or customer_code =@customer_code) and Less_than_truckload = 1

";
                    request_id = cn.Query<string>(sql, new { check_number = check_number.Text, customer_code = supplier_code }).ToList();
                }
            }
            catch (Exception exx)
            {

            }
            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
                readdata();

            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }

        }
        else if (rb2.Checked)
        {

            order_number.Text = order_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入訂單編號');</script><noscript></noscript>");
                return;
            }
            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('編號有誤');</script><noscript></noscript>");
                return;
            }


            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE order_number = @order_number and (@customer_code = '' or customer_code =@customer_code) and Less_than_truckload = 1

";
                    request_id = cn.Query<string>(sql, new { order_number = order_number.Text, customer_code = supplier_code }).ToList();
                }
            }
            catch (Exception exx)
            {

            }

            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
                readdata();


            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }
        }
    }

    private void readdata()
    {
        if (rb1.Checked)
        {
            if (check_number.Text != "")
            {

                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.Clear();
                    cmd1.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd1.CommandText = "select top 1  view_auth  from tbFunctionsAuth where func_code = 'TA' and  account_code=@account_code";
                    DataTable dta = new DataTable();
                    dta = dbAdapter.getDataTable(cmd1);
                    if (dta.Rows.Count > 0)
                    {

                    }


                }

                //鎖權限
                string manager_type = Session["manager_type"].ToString(); //管理單位類別   
                string supplier_code = string.Empty;
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd2);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        break;
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                    cmd.Parameters.AddWithValue("@DeliveryType", DeliveryType);
                    #endregion

                    cmd.Parameters.AddWithValue("@customer_code", supplier_code);


                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)

                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SET @statement = 
'Select 
A.send_city+A.send_area  sendstation,
A.receive_city+A.receive_area arrivestation,
A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
A.time_period, A.order_number, A.check_number,
CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
A.product_category, A.subpoena_category, B.code_name as subpoena_category_name,
A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1, A.request_id, A.receive_city+A.receive_area+A.receive_address receive_address,
case A.holiday_delivery when 1 then ''假日配送'' else ''一般配送'' end as holiday_delivery,
A.collection_money ''代收金額'',
isnull(_fee.supplier_first_fee,0)  ''首件費用'',	
isnull(_fee.add_price ,0) ''續件費用'',
A.return_check_number ''return_check_number'', A.roundtrip_checknumber, 
Convert(VARCHAR, A.ship_date,111) as ship_date                                   
from tcDeliveryRequests A with(nolock)
LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
LEFT JOIN tbStation F1 with(nolock) on  F1.station_code =  a.supplier_code --F.station_code 
LEFT JOIN ttArriveSitesScattered G With(Nolock) on G.post_city = A.receive_city and G.post_area= A.receive_area 
LEFT JOIN tbStation G1 with(nolock) on G1.station_scode =  A.area_arrive_code
LEFT JOIN pickup_request_for_apiuser p1 with(nolock) on p1.check_number = a.check_number
LEFT JOIN tbStation F2 with(nolock) on  F2.station_code = p1.supplier_code
LEFT JOIN tbStation F3 with(nolock) on  F3.station_scode = a.send_station_scode
CROSS APPLY dbo.fu_GetLTShipFeeByRequestId(A.request_id) _fee'
SET @where = ' where ( A.print_date >=  DATEADD(MONTH,-12,getdate()) or A.print_date is null)' 

SET @where = @where+' and A.check_number = '''+ @check_number+ ''''
SET @where=@where+' and A.Less_than_truckload = '''+ @Less_than_truckload+ ''''
                                                  
IF (@DeliveryType <> '') BEGIN 
SET @where=@where+' and A.DeliveryType = '''+ @DeliveryType+ ''''
END


                                                  IF (@customer_code <> '') BEGIN 
                                                      SET @where=@where+' and A.customer_code like '''+ @customer_code +'%'''
                                                  END

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string customer_code = dt.Rows[0]["customer_code"].ToString();
                            string send_contact = dt.Rows[0]["send_contact"].ToString();
                            send_contact = send_contact.Length > 1 ? (IsChineseRegex(send_contact.Substring(0, 1)) ? "*" + send_contact.Substring(1) : "*" + send_contact.Substring(2)) : "*";
                            string send_tel = dt.Rows[0]["send_tel"].ToString();
                            send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                            string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                            string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                            string receive_address= dt.Rows[0]["receive_address"].ToString();
                            string id = dt.Rows[0]["request_id"].ToString();
                            lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel ;

                            // 有退貨單號先抓，沒有的話在抓來回件
                            if (dt.Rows[0]["return_check_number"] == null || dt.Rows[0]["return_check_number"].ToString().Length == 0)
                            {
                                string check_number_R = dt.Rows[0]["roundtrip_checknumber"].ToString();
                                dt.Rows[0]["return_check_number"] = check_number_R;
                            }

                            lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1 + "<BR/>" + receive_address;

                            ImageButton1.Visible = true;
                        }
                        else
                        {
                            //查詢結果不屬於此客代的不能查簽單
                            ImageButton1.Visible = false;
                        }
                        Delivery_Info_Repeater.DataSource = dt;
                        Delivery_Info_Repeater.DataBind();
                        Delivery_Info_Repeater2.DataSource = dt;
                        Delivery_Info_Repeater2.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    string ReturnCheckNumber = "";
                    using (SqlCommand cmdr = new SqlCommand())
                    {
                        cmdr.Parameters.AddWithValue("@check_number", check_number.Text);
                        cmdr.CommandText = string.Format(@"select return_check_number from tcDeliveryRequests where check_number=@check_number");
                        using (DataTable dtr = dbAdapter.getDataTable(cmdr))
                        {
                            if (dtr != null && dtr.Rows.Count > 0)
                            {
                                ReturnCheckNumber = dtr.Rows[0]["return_check_number"].ToString();
                            }
                        }
                    }

                        cmd.Parameters.Clear();
                    string wherestr1 = string.Empty;
                    string wherestr2 = string.Empty;
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@check_number", check_number.Text );

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    cmd.Parameters.AddWithValue("@return_check_number", ReturnCheckNumber);
                    if (supplier_code != "")
                    {
                        cmd.Parameters.AddWithValue("@customer_code", supplier_code);
                        wherestr1 = " inner join tcDeliveryRequests dr with(nolock) on A.check_number = dr.check_number ";
                        wherestr2 = " and dr.customer_code like @customer_code +'%'";
                    }

                    #endregion
                    cmd.CommandText = string.Empty;

                    string ss = Session["customer_code"].ToString();

                    if (ss.IndexOf("F") == 0)
                    {
                        cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO , 
                                                A.check_number, 
                                                Convert(varchar, A.scan_date,120) as scan_date, 
case 
when A.scan_item ='3' and arrive_option in ('49','50')
then replace(t.status,'X',isnull(car_number,''))
when A.scan_item not in ('6','7','1','2')
then t.status
when driver_code in ('PP990','D2901','PP991','PP002','PP992','PP001')
then replace(t2.status,'XX','龜山')
when driver_code in ('PP993','PP520','PP920')
then replace(t2.status,'XX','高雙')
when driver_code in ('C1016','PP491','Z010960')
then replace(t2.status,'XX','台中')
when driver_code in ('PP003')
then replace(t2.status,'XX','大園')
else replace(t2.status,'XX','')
end as status

                                                from ttDeliveryScanLog A with(nolock)
                                                left join  trace t on ((A.scan_item=t.scan_item) and (receive_option=t.options))
                                                or
                                                ((A.scan_item=t.scan_item) and (arrive_option=t.options))
                                                left join  trace t2 on
                                                ((A.scan_item=t2.scan_item) and A.scan_item in ('6','7','1','2'))
                                                {0}
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and A.check_number = @check_number  {1}
												order by scan_date desc", wherestr1, wherestr2);
                    }
                    else
                    {

                        cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO , 
                                                A.check_number, 
                                                Convert(varchar, A.scan_date,120) as scan_date, 
case 
when A.scan_item ='3' and arrive_option in ('49','50')
then replace(t.status,'X',isnull(car_number,''))
when A.scan_item not in ('6','7','1','2')
then t.status
when driver_code in ('PP990','D2901','PP991','PP002','PP992','PP001')
then replace(t2.status,'XX','龜山')
when driver_code in ('PP993','PP520','PP920')
then replace(t2.status,'XX','高雙')
when driver_code in ('C1016','PP491','Z010960')
then replace(t2.status,'XX','台中')
when driver_code in ('PP003')
then replace(t2.status,'XX','大園')
else replace(t2.status,'XX','')
end as status
                                                from ttDeliveryScanLog A with(nolock)
                                                left join  trace t on ((A.scan_item=t.scan_item) and (receive_option=t.options))
                                                or
                                                ((A.scan_item=t.scan_item) and (arrive_option=t.options))
                                                left join  trace t2 on
                                                ((A.scan_item=t2.scan_item) and A.scan_item in ('6','7','1','2'))
                                                {0}
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and A.check_number = @check_number  {1}
												order by scan_date desc", wherestr1, wherestr2);
                    }




                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        New_List_02.DataSource = dt;
                        New_List_02.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    //if (check_number.Text != "")
                    //{
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text );
                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    #endregion

                    cmd.CommandText = string.Format(@"Select a.request_id , 
  a.check_number , 
  a.customer_code , 
  a.supplier_code , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok] , 
  sign_form_image , 
  sign_field_image
From 
(
Select request_id , 
  check_number , 
  customer_code , 
  supplier_code 
From [dbo].[tcDeliveryRequests] with (nolock)
Where check_number = @check_number
) a 
left join
(
Select request_id , 
  check_number , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok]
From CheckDeliveryPhotoData with (nolock)
Where check_number = @check_number
) b on a.request_id = b.request_id and a.check_number = b.check_number
left join
(
Select Top 1 check_number , sign_form_image , sign_field_image
From ttDeliveryScanLog with (nolock)
Where check_number = @check_number
and scan_item = '4'
Order by log_id desc
) c on a.check_number = c.check_number");
                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string filename = "";
                            string rtn_filename = "";
                            switch (manager_type)
                            {
                                case "0":  //總公司、系統管理員才可以看到整張簽單
                                case "1":
                                case "2":
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "4": //區配商：有開放權限，可看到自已的貨和區配底下自營的貨
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "3":
                                case "5": //自營商：自已的貨可以看到整張簽單
                                    string customer_code = dt.Rows[0]["customer_code"] != DBNull.Value ? dt.Rows[0]["customer_code"].ToString() : "";
                                    string master_code = Session["master_code"] != null ? Session["master_code"].ToString() : "";
                                    if (customer_code.IndexOf(master_code) > -1)
                                    {
                                        filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    }
                                    else
                                    {
                                        filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    }
                                    break;
                                default:
                                    filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    break;
                            }


                            //string filename_split = check_number.Text;
                            //int split_length = 5; //檔案每幾個字元會建立一個資料夾
                            //string path = "";
                            //int folder_deepth = filename_split.Length / split_length;
                            //for (var i = 0; i < folder_deepth; i++)
                            //{
                            //    path += filename_split.Substring(i * 5, 5) + @"/";
                            //}
                            //path += check_number.Text + ".jpg";

                            string realUrl = "";
                            string type = "";

                            string yymmdd = dt.Rows[0]["yyyymmdd"].ToString();
                            string papaerPhoto = dt.Rows[0]["photo_paper"].ToString();
                            string digitPhoto = dt.Rows[0]["photo_digi_1"].ToString();
                            string sign_form_image = dt.Rows[0]["sign_form_image"].ToString();

                            string domain = ConfigurationManager.AppSettings["Sign_Receipt"];


                            if (dt.Rows[0]["is_photo_digi_1_ok"].ToString() == "True")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + digitPhoto;
                                type = "2";
                            }
                            else if (dt.Rows[0]["is_photo_paper_ok"].ToString() == "true")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + papaerPhoto;
                                type = "1";
                            }
                            else if (sign_form_image != "")
                            {
                                realUrl = domain + @"/UploadImages45/_No_Partition_Photo/" + sign_form_image;
                                type = "3";
                            }
                            else
                            {
                                type = "4";
                            }

                            Imglink.Attributes.Add("href", "ImageView2.aspx?ImgUrl=" + realUrl + "" + "&type=" + type + "");


                            if (realUrl == "")
                            {
                                Imglink.Style.Add("pointer-events", "none");
                                NonReceipt.Style.Add("display", "inline-block");
                            }
                            else
                            {
                                Imglink.Style.Add("pointer-events", "all");
                                NonReceipt.Style.Add("display", "none");
                                ImageButton1.Visible = true;
                            }

                            #region 舊查簽單邏輯
                            //if (filename != "")
                            //{
                            //    //先判斷網址是否有效，只出現有效的圖
                               
                               
                            //    string strUrl3 = "https://webservice.fs-express.com.tw/PHOTO/" + filename + "";
                            //    //string strUrl4 = "http://webserviceground.fs-express.com.tw/PHOTO/" + filename + "";                              
                            //    bool isUrl3oK = UrlCheck(strUrl3);
                            //    //bool isUrl4oK = UrlCheck(strUrl4);

                            //    string realUrl = "";
                            //    if (isUrl3oK)
                            //    {
                            //        realUrl = strUrl3;
                            //    }
                            //    else
                            //    {
                            //        string strUrl2Old = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //        string strUrl2 = "https://erp.fs-express.com.tw/UploadImages/SignPaperPhoto/photo/" + path + "";
                            //        bool isUrl2OldoK = UrlCheck(strUrl2Old);
                            //        bool isUrl2oK = UrlCheck(strUrl2);

                            //        if (isUrl2oK)
                            //        {
                            //            realUrl = strUrl2;
                            //        }
                            //        else if(isUrl2OldoK)
                            //        {
                            //            realUrl = strUrl2Old;
                            //        }
                            //        else
                            //        {
                            //            string strUrl1 = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename + "";
                            //            bool isUrl1oK = UrlCheck(strUrl1);

                            //            if (isUrl1oK)
                            //            {
                            //                realUrl = strUrl1;
                            //            }
                            //            else
                            //            {
                            //                realUrl = string.Empty;
                            //            }
                            //        }
                            //    }

                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= "+ realUrl + "");


                            //        string second_filename = check_number.Text + ".jpg";

                            //        string secondUrlold = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //        string secondUrl = "https://erp.fs-express.com.tw/UploadImages/SignPaperPhoto/photo/" + path + "";

                            //        bool issecondUrloK = UrlCheck(secondUrl);
                            //        bool issecondUrloldoK = UrlCheck(secondUrlold);

                            //        string realSecondUrl = "";

                            //        if (issecondUrloK)
                            //        {
                            //            realSecondUrl = secondUrl;
                            //        }
                            //        else if (issecondUrloldoK)
                            //        {
                            //            realSecondUrl = secondUrlold;
                            //        }
                            //    if (!String.IsNullOrEmpty(realSecondUrl))
                            //    {
                            //        Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= " + realUrl + "" + "&ImgUrl2=" + realSecondUrl + "");


                            //    }
                            //    else
                            //    {

                            //        Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= " + realUrl + "");

                            //    }


                            //    if (realUrl == "")
                            //    {
                            //        Imglink.Style.Add("pointer-events", "none");
                            //        NonReceipt.Style.Add("display", "inline-block");
                            //    }
                            //    else
                            //    {
                            //        Imglink.Style.Add("pointer-events", "all");
                            //        NonReceipt.Style.Add("display", "none");
                            //    }

                            //}
                            //else
                            //{
                            //    filename = check_number.Text + ".jpg";

                            ////先判斷網址是否有效，只出現有效的圖
                            //    string strUrl6Old = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //    string strUrl6 = "https://erp.fs-express.com.tw/UploadImages/SignPaperPhoto/photo/" + path + "";

                            //    bool isUrl6oK = UrlCheck(strUrl6);
                            //    bool isUrl6OldoK = UrlCheck(strUrl6Old);

                            //    string realUrl = "";

                            //    if (isUrl6oK)
                            //    {
                            //        realUrl = strUrl6;
                            //    }
                            //    else if (isUrl6OldoK)
                            //    {
                            //        realUrl = strUrl6Old;
                            //    }
                            //    else
                            //    {
                            //        string strUrl5 = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename + "";
                            //        bool isUrl5oK = UrlCheck(strUrl5);
                            //        if(isUrl5oK)
                            //        {
                            //            realUrl = strUrl5;
                            //        }
                            //        else
                            //        {
                            //            realUrl = string.Empty;
                            //        }
                            //    }
      

                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=" + realUrl + "");

                            //    if (realUrl == "")
                            //    {
                            //        Imglink.Style.Add("pointer-events", "none");
                            //        NonReceipt.Style.Add("display", "inline-block");
                            //    }
                            //    else
                            //    {
                            //        Imglink.Style.Add("pointer-events", "all");
                            //        NonReceipt.Style.Add("display", "none");
                            //    }

                            //}
                            #endregion

                        }
                        else
                        {
                            ImageButton1.Visible = false;

                        }
                    }
                }
            }
        }
        else if (rb2.Checked)
        {
            if (order_number.Text != "")
            {

                string check_number_in_receipt = string.Empty;

                //鎖權限
                string manager_type = Session["manager_type"].ToString(); //管理單位類別   
                string supplier_code = string.Empty;
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd2);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        break;
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@DeliveryType", DeliveryType);
                    #endregion

                    cmd.Parameters.AddWithValue("@customer_code", supplier_code);

                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)

                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SET @statement = 
                                                  'Select 
--F1.station_name as sendstation,
--G1.station_name as arrivestation,

A.send_city+A.send_area sendstation,
A.receive_city+A.receive_area arrivestation,
 A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
 A.time_period, A.order_number, A.check_number,
CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
A.product_category, C.code_name as product_category_name, 
A.subpoena_category, B.code_name as subpoena_category_name,
A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
A.request_id, Convert(VARCHAR, A.ship_date,111) as ship_date,  
case A.holiday_delivery when 1 then ''假日配送'' else ''一般配送'' end as holiday_delivery,
isnull(_fee.supplier_fee ,0) ''貨件運費'' ,
 A.arrive_to_pay_append ''到付追加未稅'', A.arrive_to_pay_append  *1.05 ''到付追加含稅'',
 A.collection_money ''代收金額'',
isnull(_fee.supplier_first_fee,0)  ''首件費用'',	
 isnull(_fee.add_price ,0) ''續件費用'',
A.return_check_number ''return_check_number'', A.roundtrip_checknumber
from tcDeliveryRequests A with(nolock)
LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
 LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
--LEFT JOIN ttArriveSitesScattered F With(Nolock) on F.post_city = A.send_city and F.post_area= A.send_area 
LEFT JOIN tbStation F1 with(nolock) on  F1.station_code =  a.supplier_code --F.station_code 
LEFT JOIN ttArriveSitesScattered G With(Nolock) on G.post_city = A.receive_city and G.post_area= A.receive_area 
LEFT JOIN tbStation G1 with(nolock) on G1.station_scode =  A.area_arrive_code  
CROSS APPLY dbo.fu_GetLTShipFeeByRequestId(A.request_id) _fee'
SET @where = ' where ( A.print_date >=  DATEADD(MONTH,-1,getdate()) or A.print_date is null)' 

SET @where = @where+' and A.order_number = '''+ @order_number+ ''''
                                                  SET @where=@where+' and A.Less_than_truckload = '''+ @Less_than_truckload+ ''''
                                                  
                                                  IF (@DeliveryType <> '') BEGIN 
                                                      SET @where=@where+' and A.DeliveryType = '''+ @DeliveryType+ ''''
                                                  END

                                                  IF (@customer_code <> '') BEGIN 
                                                      SET @where=@where+' and A.customer_code like '''+ @customer_code +'%'''
                                                  END

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string customer_code = dt.Rows[0]["customer_code"].ToString();
                            string send_contact = dt.Rows[0]["send_contact"].ToString();
                            send_contact = send_contact.Length > 1 ? (IsChineseRegex(send_contact.Substring(0, 1)) ? "*" + send_contact.Substring(1) : "*" + send_contact.Substring(2)) : "*";
                            string send_tel = dt.Rows[0]["send_tel"].ToString();
                            send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                            string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                            receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                            string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                            receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;
                            string id = dt.Rows[0]["request_id"].ToString();
                            lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel + "<BR/><a href='LT_transport_2_edit.aspx?BK=Y&r_type=0&request_id=" + id + "'  class='fancybox fancybox.iframe btn btn-primary _d_btn ' id='updclick'>查看</a>";

                            lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;
                            check_number_in_order_number.Text = dt.Rows[0]["check_number"].ToString();
                            check_number_in_receipt = dt.Rows[0]["check_number"].ToString();
                        }
                        Delivery_Info_Repeater.DataSource = dt;
                        Delivery_Info_Repeater.DataBind();
                        Delivery_Info_Repeater2.DataSource = dt;
                        Delivery_Info_Repeater2.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    string wherestr1 = string.Empty;
                    string wherestr2 = string.Empty;
                    #region 關鍵字


                    //if (check_number.Text != "")
                    //{
                    //cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);

                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    if (supplier_code != "")
                    {
                        cmd.Parameters.AddWithValue("@customer_code", supplier_code);
                        wherestr2 = "and  dr.customer_code like @customer_code + '%' ";
                    }

                    #endregion


                    cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY A.scan_date desc) AS NO , 
                                                A.check_number, 
                                                Convert(varchar, A.scan_date,120) as scan_date, 
case 
when A.scan_item ='3' and arrive_option in ('49','50')
then replace(t.status,'X',isnull(car_number,''))
when A.scan_item not in ('6','7','1','2')
then t.status
when driver_code in ('PP990','D2901','PP991','PP002','PP992','PP001')
then replace(t2.status,'XX','龜山')
when driver_code in ('PP993','PP520','PP920')
then replace(t2.status,'XX','高雙')
when driver_code in ('C1016','PP491','Z010960')
then replace(t2.status,'XX','台中')
when driver_code in ('PP003')
then replace(t2.status,'XX','大園')
else replace(t2.status,'XX','')
end as status
                                                from ttDeliveryScanLog A with(nolock)
                                                left join  trace t on ((A.scan_item=t.scan_item) and (receive_option=t.options))
                                                or
                                                ((A.scan_item=t.scan_item) and (arrive_option=t.options))
                                                left join  trace t2 on
                                                ((A.scan_item=t2.scan_item) and A.scan_item in ('6','7','1','2'))
                                                left join tcDeliveryRequests dr on dr.check_number=A.check_number
                                                
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and dr.order_number = @order_number  {0}
												order by scan_date desc", wherestr2);
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        New_List_02.DataSource = dt;
                        New_List_02.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    //if (check_number.Text != "")
                    //{
                    //cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);
                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@check_number", check_number_in_receipt);
                    #endregion

                    cmd.CommandText = string.Format(@"Select a.request_id , 
  a.check_number , 
  a.customer_code , 
  a.supplier_code , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok] , 
  sign_form_image , 
  sign_field_image
From 
(
Select request_id , 
  check_number , 
  customer_code , 
  supplier_code 
From [dbo].[tcDeliveryRequests] with (nolock)
Where check_number = @check_number
) a 
left join
(
Select request_id , 
  check_number , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok]
From CheckDeliveryPhotoData with (nolock)
Where check_number = @check_number
) b on a.request_id = b.request_id and a.check_number = b.check_number
left join
(
Select Top 1 check_number , sign_form_image , sign_field_image
From ttDeliveryScanLog with (nolock)
Where check_number = @check_number
and scan_item = '4'
Order by log_id desc
) c on a.check_number = c.check_number");
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string filename = "";
                            string rtn_filename = "";
                            switch (manager_type)
                            {
                                case "0":  //總公司、系統管理員才可以看到整張簽單
                                case "1":
                                case "2":
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "4": //區配商：有開放權限，可看到自已的貨和區配底下自營的貨
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "3":
                                case "5": //自營商：自已的貨可以看到整張簽單
                                    string customer_code = dt.Rows[0]["customer_code"] != DBNull.Value ? dt.Rows[0]["customer_code"].ToString() : "";
                                    string master_code = Session["master_code"] != null ? Session["master_code"].ToString() : "";
                                    if (customer_code.IndexOf(master_code) > -1)
                                    {
                                        filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    }
                                    else
                                    {
                                        filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    }
                                    break;
                                default:
                                    filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    break;
                            }

                            string realUrl = "";
                            string type = "";

                            string yymmdd = dt.Rows[0]["yyyymmdd"].ToString();
                            string papaerPhoto = dt.Rows[0]["photo_paper"].ToString();
                            string digitPhoto = dt.Rows[0]["photo_digi_1"].ToString();
                            string sign_form_image = dt.Rows[0]["sign_form_image"].ToString();

                            string domain = ConfigurationManager.AppSettings["Sign_Receipt"];


                            if (dt.Rows[0]["is_photo_digi_1_ok"].ToString() == "True")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + digitPhoto;
                                type = "2";
                            }
                            else if (dt.Rows[0]["is_photo_paper_ok"].ToString() == "true")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + papaerPhoto;
                                type = "1";
                            }
                            else if (sign_form_image != "")
                            {
                                realUrl = domain + @"/UploadImages45/_No_Partition_Photo/" + sign_form_image;
                                type = "3";
                            }
                            else
                            {
                                type = "4";
                            }

                            Imglink.Attributes.Add("href", "ImageView2.aspx?ImgUrl=" + realUrl + "" + "&type=" + type + "");


                            if (realUrl == "")
                            {
                                Imglink.Style.Add("pointer-events", "none");
                                NonReceipt.Style.Add("display", "inline-block");
                            }
                            else
                            {
                                Imglink.Style.Add("pointer-events", "all");
                                NonReceipt.Style.Add("display", "none");
                                ImageButton1.Visible = true;
                            }

                            #region 舊簽單查詢邏輯
                            //if (filename != "")
                            //{
                            //    ImageButton1.Visible = true;
                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename);
                            //}
                            //else
                            //{
                            //    ImageButton1.Visible = false;
                            //    filename = check_number.Text + ".jpg";
                            //    //ImageButton1.Visible = false;
                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename);

                            //}
                            #endregion

                        }
                        else
                        {
                            ImageButton1.Visible = false;
                        }
                    }
                }

            }
        }
    }

    private void readInventory()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            cmd.CommandText = @"select ROW_NUMBER() OVER(ORDER BY inventory_id desc) 'NO', i.scanning_dt, s.station_name, d.driver_code + d.driver_name 'driver' from ttInventory i left join tbStation s on i.station_code = s.station_scode left join tbDrivers d on i.driver_code = d.driver_code where check_number = @check_number order by inventory_id desc";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

            }
        }
    }
        


    



    protected void btExit_Click(object sender, EventArgs e)
    {

        divMaster.Visible = true;
    }

    protected void btExittype_Click(object sender, EventArgs e)
    {

        divMaster.Visible = true;

    }

    protected void dlP1_SelectedIndexChanged(object sender, EventArgs e)
    {


    }


    protected void dlP2_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void UpdateAddress(object sender, EventArgs e)
    {
        string strcustomer_code = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_Number", check_number.Text);
            cmd.CommandText = @"select top 1 customer_code  from tcDeliveryRequests where check_Number = @check_Number order by request_id desc";
            DataTable dt = dbAdapter.getDataTable(cmd);
            if (dt != null && dt.Rows.Count > 0)
            {
                strcustomer_code = dt.Rows[0]["customer_code"].ToString();
            }
        }


        //dlP2.SelectedValue = "更改收件資料";   // 選自行輸入 original magic number is 10380

        // 抓舊地址，填進處置說明
        string oldAddress = string.Empty;
        string getAddressSql = @"select top 1 receive_city + receive_area + receive_address as receive_address
            from tcDeliveryRequests where check_number = @checkNumber order by request_id desc";

        using (SqlCommand cmd = new SqlCommand(getAddressSql))
        {
            cmd.Parameters.AddWithValue("@checkNumber", check_number.Text);
            oldAddress = dbAdapter.getScalarBySQL(cmd) as string;
        }

    }

    protected void sta_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
      


    

    
    protected void rb_CheckedChanged(Object sender, EventArgs e)
    {
        string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
        if (rb1.Checked)
        {
            check_number.Enabled = true;
            order_number.Enabled = false;
            check_number.Text = "";
            order_number.Text = "";
        }
        if (rb2.Checked)
        {
            check_number.Enabled = false;
            order_number.Enabled = true;
            check_number.Text = "";
            order_number.Text = "";
        }
    }

    private bool IsChineseRegex(string s)
    {
        Regex rx = new Regex("^[\u4e00-\u9fa5]$");
        if (rx.IsMatch(s))
            return true;
        else
            return false;
    }



    protected bool UrlCheck(string strUrl)
    {
        try
        {
            //System.IO.File.Exists( Server.MapPath("")) 
            //System.IO.File.Exists(@"//172..../);
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(strUrl);
            myRequest.Method = "HEAD";
            myRequest.Timeout = 3000;  
            HttpWebResponse res = (HttpWebResponse)myRequest.GetResponse();
            return (res.StatusCode == HttpStatusCode.OK);
        }
        catch
        {
            return false;
        }
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    check_address = true;
                    Info = response.Data.Data;
                    areaArriveCode = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }
}