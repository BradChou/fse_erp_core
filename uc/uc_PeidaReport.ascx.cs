﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uc_uc_PeidaReport : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void readdata(string dates, string datee, string Less_than_truckload, string supplier_code, string customer_code , string type ="D")
    {
        Hid_dates.Value = dates;
        Hid_datee.Value = datee;
        Hid_customer_code.Value = customer_code;
        Hid_type.Value = type;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandTimeout = 9999;
            cmd.Parameters.Clear();
            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);

            if (dates != "")
            {
                cmd.Parameters.AddWithValue("@dates", dates);
            }

            if (datee != "")
            {
                cmd.Parameters.AddWithValue("@datee", datee);
            }
            //string manager_type = Session["manager_type"].ToString(); //管理單位類別  
            //string customer_code = Session["master_code"].ToString();

            //switch (manager_type)
            //{
            //    case "0":
            //    case "1":
            //    case "2":
            //        customer_code = "";
            //        break;
            //    default:
            //        customer_code = customer_code.Substring(0, 3);
            //        break;
            //}
            cmd.Parameters.AddWithValue("@_supplier_code", supplier_code);

            cmd.Parameters.AddWithValue("@customer_code", customer_code);

            cmd.Parameters.AddWithValue("@DeliveryType", type);
            #endregion

            cmd.CommandText = string.Format(@"DECLARE  @NO NVARCHAR(5) 
                                                DECLARE  @CHKNUM NVARCHAR(20)  --貨號
                                                DECLARE  @supplier_code NVARCHAR
	                                            DECLARE  @area_arrive_code NVARCHAR
                                                DECLARE  @supplier_date DateTime 
                                                DECLARE  @issupply INT 
                                                DECLARE  @noon_arrive  INT
                                                DECLARE  @day_arrive  INT
                                                DECLARE  @ok_arrive INT
                                                DECLARE  @first_time  NVARCHAR(10)
                                                DECLARE  @last_time NVARCHAR(10) 
                                                DECLARE  @OKsupply_date DateTime 
                                                DECLARE  @OKarrive_date DateTime 
                                                DECLARE  @OKsupplier_date DateTime 
                                                DECLARE  @should  INT
                                                DECLARE  @LTT BIT 

                                                Select ROW_NUMBER() OVER(ORDER BY A.check_number ) AS NO ,
                                                A.check_number,
                                                A.area_arrive_code ,
                                                A.supplier_date ,				   --應配送日期
                                                0 as issupply,					   --是否配送
                                                0 as noon_arrive,				   --是否中午前配達(13:00前)
                                                0 as day_arrive ,				   --當日配達(17:00前)
                                                0 as ok_arrive ,				   --已配達
                                                @OKarrive_date  as OKarrive_date,  --配達時間
                                                1 as should,
                                                --'00:00:00' as first_time , --首筆配達時間
                                                --'00:00:00' as last_time    --末筆配達時間
                                                Less_than_truckload
                                                into #mylist	  
                                                from tcDeliveryRequests  A with(nolock)
                                                where  A.check_number <> ''
                                                and CONVERT(VARCHAR,A.print_date,111)  >= CONVERT(VARCHAR,@dates,111) AND CONVERT(VARCHAR,A.print_date,111)  <= CONVERT(VARCHAR,@datee,111)                                                
	                                            and A.area_arrive_code LIKE ''+  ISNULL(@_supplier_code,'') +'%'
                                                and A.customer_code LIKE ''+  ISNULL(@customer_code,'') +'%'
                                                and A.cancel_date  IS NULL and A.Less_than_truckload = @Less_than_truckload
                                                and A.DeliveryType =@DeliveryType
                                                order by A.check_number 

                                                DECLARE R_cursor CURSOR FOR
                                                select * FROM #mylist
                                                OPEN R_cursor
                                                FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@area_arrive_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@ok_arrive,@OKarrive_date,@should, @LTT
                                                WHILE (@@FETCH_STATUS = 0) 
	                                                BEGIN	
		                                                select top 1  @OKsupply_date = scan_date from ttDeliveryScanLog  with (nolock)  
		                                                where check_number = @CHKNUM and scan_item = '2' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc
		
		                                                IF @OKsupply_date IS NOT NULL 
		                                                BEGIN 
			                                                SET @issupply = 1 
		                                                END
	
                                                        SET @OKsupplier_date = NULL
														SET @OKarrive_date = NULL

                                                        Select top 1 @OKsupplier_date =scan_date  from ttDeliveryScanLog  with (nolock) 
		                                                where check_number = @CHKNUM and scan_item = '2' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc	

		                                                Select top 1 @OKarrive_date =scan_date , @ok_arrive= case when scan_date IS NOT NULL then 1 End from ttDeliveryScanLog  with (nolock) 
		                                                where check_number = @CHKNUM and scan_item = '3' AND cdate >=  DATEADD(MONTH,-6,getdate()) order by scan_date desc				
		
		                                                --如果配達時間小於13:00 且配達日期=應配送日期-->午前配達率=1		
		                                                IF @OKarrive_date IS NOT NULL AND @OKsupplier_date IS NOT NULL AND  (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'13:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@OKsupplier_date, 112))
		                                                BEGIN 
			                                                set  @noon_arrive = 1
		                                                END
		
		                                                --如果配達時間小於17:00 且配達日期=應配送日期-->午前配達率=1
		                                                IF @OKarrive_date IS NOT NULL AND @OKsupplier_date IS NOT NULL AND (CONVERT(varchar ,@OKarrive_date, 108)<=CONVERT(varchar ,'17:00:00', 108)) and (CONVERT(varchar ,@OKarrive_date, 112)<=CONVERT(varchar ,@OKsupplier_date, 112))
		                                                BEGIN 
			                                                set @day_arrive = 1
		                                                END

		                                                UPDATE #mylist
				                                                set issupply  =@issupply , 
				                                                noon_arrive = @noon_arrive, 
				                                                day_arrive=@day_arrive ,
                                                                ok_arrive=@ok_arrive ,
				                                                OKarrive_date= @OKarrive_date
		                                                WHERE check_number = @CHKNUM
					
	                                                FETCH NEXT FROM R_cursor INTO @NO,@CHKNUM,@area_arrive_code ,@supplier_date ,@issupply, @noon_arrive , @day_arrive,@ok_arrive,@OKarrive_date,@should, @LTT
	                                                END
                                                CLOSE R_cursor
                                                DEALLOCATE R_cursor

                                                SELECT a.area_arrive_code, a.MIN , b.MAX 
                                                into #tb1
                                                FROM   (SELECT area_arrive_code, MIN(OKarrive_date) AS MIN FROM #mylist group by area_arrive_code) a,  
                                                        (SELECT area_arrive_code, MAX(OKarrive_date) AS MAX FROM #mylist group by area_arrive_code) b
                                                where a.area_arrive_code = b.area_arrive_code;

                                                select  ROW_NUMBER() OVER(ORDER BY tb1.area_arrive_code ) AS NO , tb1.*, CONVERT(varchar , #tb1.MIN, 108) as MIN , CONVERT(varchar , #tb1.MAX, 108) as MAX
                                                from (select A.area_arrive_code,  
                                                        CASE WHEN A.Less_than_truckload  = 1 THEN A.area_arrive_code ELSE (CASE A.area_arrive_code WHEN  '001' THEN '零擔'  WHEN  '002' THEN '流通' ELSE B.supplier_name END) END	as area_arrive_name  ,
                                                        sum(A.should ) '應配筆數', sum(A.issupply ) '配送筆數', sum(A.noon_arrive) '午前配達筆數', sum(A.day_arrive)  '當日配達筆數'  , sum(A.ok_arrive )  '已配達筆數' 
                                                        from #mylist A
                                                        left join tbSuppliers B on A.area_arrive_code = B.supplier_code and  ISNULL(B.supplier_code,'') <> ''
                                                        group by A.area_arrive_code, CASE WHEN A.Less_than_truckload  = 1 THEN A.area_arrive_code ELSE (CASE A.area_arrive_code WHEN  '001' THEN '零擔'  WHEN  '002' THEN '流通' ELSE B.supplier_name END) END ) tb1, #tb1
                                                where tb1.area_arrive_code = #tb1.area_arrive_code


                                                IF OBJECT_ID('tempdb..#mylist') IS NOT NULL
                                                DROP TABLE #mylist

                                                IF OBJECT_ID('tempdb..#tb1') IS NOT NULL
                                                DROP TABLE #tb1");


            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                if (dt.Rows.Count > 0)
                {
                    double[] Tot = { 0, 0, 0, 0, 0, 0 };
                    dt.Columns.Add(new DataColumn("noonRate", typeof(String)));   //午前配達率
                    dt.Columns.Add(new DataColumn("dayRate", typeof(String)));    //當日配達率
                    dt.Columns.Add(new DataColumn("noArrive", typeof(String)));   //未配達數
                    dt.Columns.Add(new DataColumn("noRate", typeof(String)));     //異常率
                    dt.Columns.Add(new DataColumn("yesRate", typeof(String)));    //配達率 

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        //string supplier_code = dt.Rows[i]["area_arrive_code"].ToString();
                        double should = dt.Rows[i]["應配筆數"] != DBNull.Value ? (int)dt.Rows[i]["應配筆數"] : 0;
                        Tot[0] += should;
                        double supply = dt.Rows[i]["配送筆數"] != DBNull.Value ? (int)dt.Rows[i]["配送筆數"] : 0;
                        Tot[1] += supply;
                        double noon = dt.Rows[i]["午前配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["午前配達筆數"] : 0;
                        Tot[2] += noon;
                        double day = dt.Rows[i]["當日配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["當日配達筆數"] : 0;
                        Tot[3] += day;
                        double okArrive = dt.Rows[i]["已配達筆數"] != DBNull.Value ? (int)dt.Rows[i]["已配達筆數"] : 0;
                        Tot[4] += okArrive;
                        double noArrive = should - okArrive;
                        Tot[5] += noArrive;
                        dt.Rows[i]["noonRate"] = ((noon / should) * 100).ToString("N2");
                        dt.Rows[i]["dayRate"] = ((day / should) * 100).ToString("N2");
                        dt.Rows[i]["yesRate"] = ((okArrive / should) * 100).ToString("N2");
                        dt.Rows[i]["noArrive"] = noArrive.ToString();
                        dt.Rows[i]["noRate"] = ((noArrive / should) * 100).ToString("N2");
                        dt.Rows[i]["MIN"] = dt.Rows[i]["MIN"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["MIN"]).ToString("HH:mm") : "";
                        dt.Rows[i]["MAX"] = dt.Rows[i]["MAX"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["MAX"]).ToString("HH:mm") : "";

                        //string firstTime = string.Empty;
                        //string lastTime = string.Empty;


                        //send_contact = send_contact.Length > 1 ? "*" + send_contact.Substring(1) : "*";
                        //string send_tel = dt.Rows[0]["send_tel"].ToString();
                        //send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                        //string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                        //receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                        //string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                        //receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;
                    }

                    DataRow row = dt.NewRow();
                    row["area_arrive_code"] = "all";
                    row["area_arrive_name"] = "總計";
                    row["應配筆數"] = Tot[0];
                    row["配送筆數"] = Tot[1];
                    row["午前配達筆數"] = Tot[2];
                    row["當日配達筆數"] = Tot[3];
                    row["已配達筆數"] = Tot[4];
                    row["noArrive"] = Tot[5];
                    row["noonRate"] = ((Tot[2] / Tot[0]) * 100).ToString("N2");
                    row["dayRate"] = ((Tot[3] / Tot[0]) * 100).ToString("N2");
                    row["yesRate"] = ((Tot[4] / Tot[0]) * 100).ToString("N2");
                    row["noRate"] = ((Tot[5] / Tot[0]) * 100).ToString("N2");



                    dt.Rows.Add(row);

                }
                New_List.DataSource = dt;
                New_List.DataBind();
                //return  dt;
            }

        }
    }
}