﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using RestSharp;

public partial class uc_uc_trace : System.Web.UI.UserControl
{
    bool check_address = true;
    string areaArriveCode = "";
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public bool isNeedInventoryTable
    {
        get { return manager_type != "5" & Less_than_truckload == "1"; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public Label Title
    {
        get { return lbTitle; }
    }

    public string DeliveryType
    {
        // 配送類型 D:正物流  R:逆物流
        get { return ViewState["DeliveryType"].ToString(); }
        set { ViewState["DeliveryType"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            manager_type = Session["manager_type"].ToString(); //管理單位類別            
            if (DeliveryType == "R")
            {
                dlsub_chknum.SelectedValue = "001";
            }

            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }


            #region 處置說明-狀況類別
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.CommandText = "select * from condition_category where level = '1' ";
                dlP1.DataSource = dbAdapter.getDataTable(cmd1);
                dlP1.DataValueField = "category";
                dlP1.DataTextField = "category";
                dlP1.DataBind();
                dlP1.Items.Insert(0, new ListItem("===請選擇===", ""));
                //dlP1_SelectedIndexChanged(null, null);
            }

            // 修改地址
            string selectCitySql = "SELECT city FROM tbPostCity";
            using (SqlCommand cmd = new SqlCommand(selectCitySql))
            {
                City.DataSource = dbAdapter.getDataTable(cmd);
                City.DataTextField = "city";
                City.DataValueField = "city";
                City.DataBind();
            }

            string defaultAreaSql = "SELECT area FROM tbPostCityArea WHERE city = '基隆市'";
            using (SqlCommand cmd = new SqlCommand(defaultAreaSql))
            {
                Area.DataSource = dbAdapter.getDataTable(cmd);
                Area.DataTextField = "area";
                Area.DataValueField = "area";
                Area.DataBind();
            }
            #endregion

            #region 站所
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.CommandText = "select * from tbStation  where  active_flag = 1 ";
                sta.DataSource = dbAdapter.getDataTable(cmd1);
                sta.DataValueField = "station_scode";
                sta.DataTextField = "station_name";
                sta.DataBind();
                sta.Items.Insert(0, new ListItem("===請選擇===", ""));
                sta_SelectedIndexChanged(null, null);
            }
            #endregion

            #region 貨態
            using (SqlCommand cmd1 = new SqlCommand())
            {
                List<string> account = new List<string>();

                try
                {
                    using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                    {
                        string sql = @"
select a.account_code from tbAccounts a
left join tbEmps e on e.emp_code=a.emp_code
where (station='53'
and a.active_flag=1)
or a.account_code='skyeyes'
order by a.account_code ";

                        account = cn.Query<string>(sql, new { }).ToList();
                    }
                }
                catch (Exception exx)
                {

                }
                //var account = new List<string> { "skyeyes", "9990224", "9990223", "9990507", "9990427", "9990172", "9990193", "9990250", "Z011076", "9999013", "9999026" };
                if (account.Contains(Session["account_code"].ToString()))
                {
                    cmd1.CommandText = "select * from tbItemCodes  where  active_flag = 1  and code_sclass='P3' and code_id in ('3','5')";
                }
                else
                {
                    cmd1.CommandText = "select * from tbItemCodes  where  active_flag = 1  and code_sclass='P3' and code_id in ('3')";
                }

                scanItem.DataSource = dbAdapter.getDataTable(cmd1);
                scanItem.DataValueField = "code_id";
                scanItem.DataTextField = "code_name";
                scanItem.DataBind();
                scanItem.Items.Insert(0, new ListItem("===請選擇===", ""));
            }
            #endregion




            divDetail.Visible = true;
            divMaster.Visible = true;
            ImageButton1.Visible = false;

            if (isNeedInventoryTable)
            {
                Inventory.Visible = true;
            }

            readdata();


        }
        //string sScript = " $('#" + dlP2.ClientID + "').change(function() {" +
        //                       " var n = $('option:selected',this).text(); " +
        //                       " $('#" + dispose_desc.ClientID + "').val(n); " +
        //                       " });";
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);

    }

    protected void search_Click(object sender, EventArgs e)
    {

        //鎖權限
        string manager_type = Session["manager_type"].ToString(); //管理單位類別
        string station_level = Session["station_level"].ToString(); //層級
        string management = Session["management"].ToString();  //level1
        string station_scode = Session["station_scode"].ToString();  //level2                                                       
        string station_area = Session["station_area"].ToString();  //level4

        string supplier_code = string.Empty;

        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                supplier_code = "";
                //一般員工/管理者
                //有綁定站所只該該站所客戶
                //無則可查所有站所下的所有客戶

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                    DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        supplier_code = dt.Rows[0]["station_code"].ToString();
                    }
                }
                /*
                if (station_level == "1")  //management
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", management);
                        cmd.CommandText = @"select station_code from tbStation where management=@management";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = "";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                //supplier_code += "'" + dt.Rows[i]["station_code"].ToString() + "'" + ",";
                                supplier_code +=  dt.Rows[i]["station_code"].ToString() + "','";
                            }
                            supplier_code = supplier_code.Substring(0, supplier_code.Length - 3);
                        }
                    }
                }
                else if (station_level == "2")  //station_scode
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@station_scode", station_scode);
                        cmd.CommandText = @"select station_code from tbStation where station_scode=@station_scode";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = "";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                supplier_code +=  dt.Rows[i]["station_code"].ToString() + "','";
                            }
                            supplier_code = supplier_code.Substring(0, supplier_code.Length - 3);
                        }
                    }
                }
                else if (station_level == "4")  //station_area
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@station_area", station_area);
                        cmd.CommandText = @"select station_code from tbStation where station_area=@station_area";
                        DataTable dt = dbReadOnlyAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = "";
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                supplier_code +=  dt.Rows[i]["station_code"].ToString() +  "','";
                            }
                            supplier_code = supplier_code.Substring(0, supplier_code.Length - 3);
                        }
                    }
                }
                else if (station_level == "5")  //全部
                {

                }
                else
                {

                }
                */
                break;

            case "4":                 //站所主管只能看該站所下的客戶
                using (SqlCommand cmd2 = new SqlCommand())
                {
                    cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                    DataTable dt = dbAdapter.getDataTable(cmd2);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        supplier_code = dt.Rows[0]["station_code"].ToString();
                    }
                }
                break;
            case "5":
                supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                break;
        }

        if (rb1.Checked)
        {
            check_number.Text = check_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入貨號');</script><noscript></noscript>");
                return;
            }

            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(check_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('貨號有誤');</script><noscript></noscript>");
                return;
            }

            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE check_number = @check_number ";
//and (@customer_code = ''  or supplier_code in ('"+supplier_code+ "')  or 'F'+send_station_scode in ('" + supplier_code + "') or 'F'+area_arrive_code in ('" + supplier_code + "')) and Less_than_truckload = 1";
//                    if (station_area != "") { sql += @"and
//  (send_station_scode in (select station_scode from tbStation where station_area = @station_area)
//  or
//  area_arrive_code in (select station_scode from tbStation where station_area = @station_area) 
//  )"; }

//                    if (station_scode != "") { sql += @"and
//  (send_station_scode in (select station_scode from tbStation where station_scode = @station_scode)
//  or
//  area_arrive_code in (select station_scode from tbStation where station_scode = @station_scode) 
//  )"; }

//                    if (management != "") { sql += @"and
//  (send_station_scode in (select station_scode from tbStation where management = @management)
//  or
//  area_arrive_code in (select station_scode from tbStation where management = @management) 
//  )"; }
                    request_id = cn.Query<string>(sql, new { check_number = check_number.Text, customer_code = supplier_code, station_area, station_scode, management }).ToList();
                }
            }
            catch (Exception exx)
            {

            }
            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
                readdata();
                readdetail();
                if (isNeedInventoryTable)
                {
                    readInventory();
                }
                divDetail.Visible = true;
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }

        }
        else if (rb2.Checked)
        {

            order_number.Text = order_number.Text.Trim().Replace(" ", "");
            //不能空白
            if (string.IsNullOrEmpty(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('請輸入訂單編號');</script><noscript></noscript>");
                return;
            }
            //只能有數字跟英文
            if (!(new System.Text.RegularExpressions.Regex("^[A-Za-z0-9]+$")).IsMatch(order_number.Text))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('編號有誤');</script><noscript></noscript>");
                return;
            }


            List<string> request_id = new List<string>();

            try
            {
                using (var cn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString()))
                {
                    string sql = @"
  SELECT request_id
  FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
  WHERE order_number = @order_number and (@customer_code = '' or supplier_code  in ('" + supplier_code + "')  or 'F'+send_station_scode in ('" + supplier_code + "') or 'F'+area_arrive_code in ('" + supplier_code + "')) and Less_than_truckload = 1";
                    request_id = cn.Query<string>(sql, new { order_number = order_number.Text, customer_code = supplier_code }).ToList();
                }
            }
            catch (Exception exx)
            {

            }

            if (request_id != null && request_id.Count() > 0)
            {
                if (request_id.Count() > 100)
                {
                    HttpContext.Current.Response.Write("<script language='javascript'>alert('查詢結果異常，如需要請洽詢資訊人員');</script><noscript></noscript>");
                    return;
                }
                readdata();
                readdetail();
                if (isNeedInventoryTable)
                {
                    readInventory();
                }
                divDetail.Visible = true;
            }
            else
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('無資料');</script><noscript></noscript>");
                return;
            }
        }
    }

    private void readdata()
    {
        if (rb1.Checked)
        {
            if (check_number.Text != "")
            {

                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.Clear();
                    cmd1.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd1.CommandText = "select top 1  view_auth  from tbFunctionsAuth where func_code = 'TA' and  account_code=@account_code";
                    DataTable dta = new DataTable();
                    dta = dbAdapter.getDataTable(cmd1);
                    if (dta.Rows.Count > 0)
                    {
                        if (dta.Rows[0]["view_auth"].ToString() == "0")
                        {
                            Bohotype.Visible = false;
                        }
                        else
                        {
                            Bohotype.Visible = true;
                        }
                    }
                    else
                    {
                        Bohotype.Visible = false;
                    }

                }

                //鎖權限
                string manager_type = Session["manager_type"].ToString(); //管理單位類別   
                string supplier_code = string.Empty;
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd2);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        break;
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);
                    cmd.Parameters.AddWithValue("@DeliveryType", DeliveryType);
                    #endregion

                    cmd.Parameters.AddWithValue("@customer_code", supplier_code);


                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)

                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SET @statement = 
'Select 
case 
when (a.check_number like ''98%'' or a.check_number like ''99%'') then F3.station_name
when A.DeliveryType = ''R'' then  (select B1.station_name from 
ttArriveSitesScattered A1 With(Nolock) 
LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
where A1.post_city = A.send_city and A1.post_area= A.send_area ) 
when a.customer_code = ''F1300600002'' or a.customer_code = ''F2900210002 '' or a.customer_code = ''F2900720002 '' then F2.station_name
else F1.station_name end sendstation,
case when A.DeliveryType = ''R'' AND A.supplier_code =''F35'' then (select D1.station_name from 
ttArriveSitesScattered C1 With(Nolock) 
LEFT JOIN tbStation D1 with(nolock) on  D1.station_scode =  C1.station_code
where C1.post_city = A.receive_city and C1.post_area= A.receive_area ) else
G1.station_name end arrivestation,
A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
A.time_period, A.order_number, A.check_number,
CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
A.product_category, A.subpoena_category, B.code_name as subpoena_category_name,
A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1, A.request_id,
case A.holiday_delivery when 1 then ''假日配送'' else ''一般配送'' end as holiday_delivery,
A.collection_money ''代收金額'',
isnull(_fee.supplier_first_fee,0)  ''首件費用'',	
isnull(_fee.add_price ,0) ''續件費用'',
A.return_check_number ''return_check_number'', A.roundtrip_checknumber, 
Convert(VARCHAR, A.ship_date,111) as ship_date                                   
from tcDeliveryRequests A with(nolock)
LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
LEFT JOIN tbStation F1 with(nolock) on  F1.station_code =  a.supplier_code --F.station_code 
LEFT JOIN ttArriveSitesScattered G With(Nolock) on G.post_city = A.receive_city and G.post_area= A.receive_area 
LEFT JOIN tbStation G1 with(nolock) on G1.station_scode =  A.area_arrive_code
LEFT JOIN pickup_request_for_apiuser p1 with(nolock) on p1.check_number = a.check_number
LEFT JOIN tbStation F2 with(nolock) on  F2.station_code = p1.supplier_code
LEFT JOIN tbStation F3 with(nolock) on  F3.station_scode = a.send_station_scode
CROSS APPLY dbo.fu_GetLTShipFeeByRequestId(A.request_id) _fee'
SET @where = ' where ( A.print_date >=  DATEADD(MONTH,-12,getdate()) or A.print_date is null)' 

SET @where = @where+' and A.check_number = '''+ @check_number+ ''''
SET @where=@where+' and A.Less_than_truckload = '''+ @Less_than_truckload+ ''''
                                                  
IF (@DeliveryType <> '') BEGIN 
SET @where=@where+' and A.DeliveryType = '''+ @DeliveryType+ ''''
END


                                                  IF (@customer_code <> '') BEGIN 
                                                      SET @where=@where+' and A.customer_code in ( '''+ @customer_code +''')'
                                                  END

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string customer_code = dt.Rows[0]["customer_code"].ToString();
                            string send_contact = dt.Rows[0]["send_contact"].ToString();
                            send_contact = send_contact.Length > 1 ? (IsChineseRegex(send_contact.Substring(0, 1)) ? "*" + send_contact.Substring(1) : "*" + send_contact.Substring(2)) : "*";
                            string send_tel = dt.Rows[0]["send_tel"].ToString();
                            send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                            string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                            receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                            string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                            receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;
                            string id = dt.Rows[0]["request_id"].ToString();
                            lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel + "<BR/><a href='LT_transport_2_edit.aspx?BK=Y&r_type=0&request_id=" + id + "'  class='fancybox fancybox.iframe btn btn-primary _d_btn ' id='updclick'>查看</a>";

                            // 有退貨單號先抓，沒有的話在抓來回件
                            if (dt.Rows[0]["return_check_number"] == null || dt.Rows[0]["return_check_number"].ToString().Length == 0)
                            {
                                string check_number_R = dt.Rows[0]["roundtrip_checknumber"].ToString();
                                dt.Rows[0]["return_check_number"] = check_number_R;
                            }

                            lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;

                            ImageButton1.Visible = true;
                        }
                        else
                        {
                            //查詢結果不屬於此客代的不能查簽單
                            ImageButton1.Visible = false;
                        }
                        Delivery_Info_Repeater.DataSource = dt;
                        Delivery_Info_Repeater.DataBind();
                        Delivery_Info_Repeater2.DataSource = dt;
                        Delivery_Info_Repeater2.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    string wherestr1 = string.Empty;
                    string wherestr2 = string.Empty;
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    if (supplier_code != "")
                    {
                        cmd.Parameters.AddWithValue("@customer_code", supplier_code);
                        wherestr1 = " inner join tcDeliveryRequests dr with(nolock) on A.check_number = dr.check_number ";
                        wherestr2 = " and dr.supplier_code  in ('" + supplier_code + "')";
                    }

                    #endregion
                    cmd.CommandText = string.Empty;

                    string ss = Session["customer_code"].ToString();

                    if (ss.IndexOf("F") == 0)
                    {
                        cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY min(A.scan_date) desc) AS NO , A.check_number, 
                                                SI.code_name AS scan_name,
                                                Convert(varchar, min(A.scan_date),120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , min(A.log_id)'log_id',A.Cuser,
                                                CASE A.scan_item WHEN '2' THEN DO.code_name WHEN '3' THEN AO.code_name WHEN '5' THEN RO.code_name WHEN '6' THEN RO.code_name WHEN '7' THEN SO.code_name ELSE  CASE WHEN EO.code_name='無' THEN '' ELSE EO.code_name END  END as status,
                                                CASE A.write_off_type WHEN 1 THEN '人工銷號' WHEN 0 THEN '籠車路線' else '籠車路線' END AS write_off_type,
                                                A.driver_code , C.driver_name , A.sign_form_image,
case
when A.tracking_number is not null then A.tracking_number
when @check_number like '990%' then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = R.send_contact order by cdate desc)
when @check_number like '102%' then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = R.send_contact order by cdate desc)
when (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = @check_number order by cdate desc) <> '' then (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = @check_number order by cdate desc)
else
A.tracking_number end tracking_number,
T.order_number,
CASE WHEN S.station_name IS　NOT NULL THEN S.station_name ELSE 
     CASE C.driver_code
     WHEN 'Z010474' THEN (select top 1 station_name from tcDeliveryRequests d with(nolock) left join ttArriveSitesScattered a with(nolock) on d.send_area = a.post_area and d.send_city = a.post_city left join tbStation s with(nolock) on a.station_code = s.station_scode where check_number = @check_number)
     ELSE S.station_name
     END
END as station_name, S.tel,C.driver_type_code,
Case A.Del_status When 1 then '1' Else '0' END as Del_status,
                                                Convert(varchar, min(A.scan_date),112)+'01' as shift,
												Convert(varchar(16), min(A.scan_date), 121),
                                                CASE when A.scan_item = '3' and A.arrive_option = '3' then R.pieces
                                                ELSE Count(A.log_id) end 'quant'
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'') <> ''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                left join tbItemCodes RO with(nolock) on A.receive_option = RO.code_id and RO.code_sclass = 'RO' and RO.active_flag = 1
												left join tbItemCodes SO with(nolock) on A.send_option = SO.code_id and SO.code_sclass = 'SO' and SO.active_flag = 1
												left join tbItemCodes DO with(nolock) on A.delivery_option = DO.code_id and DO.code_sclass = 'DO' and DO.active_flag = 1
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number
                                                left join tbItemCodes SI with(nolock) on A.scan_item = SI.code_id and SI.code_sclass = 'P3' and SI.active_flag = 1
                                                left join (select check_number,order_number from ttKuoYangScanLog group by check_number , order_number) T  on A.check_number = T.check_number 
                                                left join tbStation S with(nolock) on C.station = S.station_scode
                                                {0}
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and A.check_number = @check_number and R.Less_than_truckload = @Less_than_truckload {1}
												group by r.pieces, A.check_number, A.area,A.scan_item,A.area_arrive_code,B.supplier_name,arrive_option,exception_option,A.Cuser,AO.code_name,EO.code_name,write_off_type,A.driver_code,C.driver_code,driver_name,A.sign_form_image,tracking_number,tracking_number,T.order_number,station_name,driver_mobile,s.tel,Del_status,Convert(varchar(16), A.scan_date, 25),R.send_contact, driver_type_code, RO.code_name, SO.code_name, DO.code_name, SI.code_name
												order by scan_date desc", wherestr1, wherestr2);
                    }
                    else
                    {

                        cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY min(A.scan_date) desc) AS NO , A.check_number, 
                                                SI.code_name AS scan_name,
                                                Convert(varchar, min(A.scan_date),120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , min(A.log_id)'log_id',A.Cuser,
                                                CASE A.scan_item WHEN '2' THEN DO.code_name WHEN '3' THEN AO.code_name WHEN '5' THEN RO.code_name WHEN '6' THEN RO.code_name WHEN '7' THEN SO.code_name ELSE  CASE WHEN EO.code_name='無' THEN '' ELSE EO.code_name END  END as status,
                                                CASE A.write_off_type WHEN 1 THEN '人工銷號' WHEN 0 THEN '籠車路線' else '籠車路線' END AS write_off_type,
                                                A.driver_code , C.driver_name , A.sign_form_image,
case
when A.tracking_number is not null then A.tracking_number
when @check_number like '990%' then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = R.send_contact order by cdate desc)
when @check_number like '102%' then (select top 1 check_number from tcDeliveryRequests with(nolock) where check_number = R.send_contact order by cdate desc)
when (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = @check_number order by cdate desc) <> '' then (select top 1 check_number from tcDeliveryRequests with(nolock) where send_contact = @check_number order by cdate desc)
else
A.tracking_number end tracking_number,
T.order_number,
CASE WHEN S.station_name IS　NOT NULL THEN S.station_name ELSE 
     CASE C.driver_code
     WHEN 'Z010474' THEN (select top 1 station_name from tcDeliveryRequests d with(nolock) left join ttArriveSitesScattered a with(nolock) on d.send_area = a.post_area and d.send_city = a.post_city left join tbStation s with(nolock) on a.station_code = s.station_scode where check_number = @check_number)
     ELSE S.station_name
     END
END as station_name, 
C.driver_mobile AS tel,C.driver_type_code,
Case A.Del_status When 1 then '1' Else '0' END as Del_status,
                                                Convert(varchar, min(A.scan_date),112)+'01' as shift,
												Convert(varchar(16), min(A.scan_date), 121),
                                                CASE when  A.scan_item = '3' and A.arrive_option = '3'　then R.pieces
                                                ELSE Count(A.log_id) end 'quant'
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'') <> ''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                left join tbItemCodes RO with(nolock) on A.receive_option = RO.code_id and RO.code_sclass = 'RO' and RO.active_flag = 1
												left join tbItemCodes SO with(nolock) on A.send_option = SO.code_id and SO.code_sclass = 'SO' and SO.active_flag = 1
												left join tbItemCodes DO with(nolock) on A.delivery_option = DO.code_id and DO.code_sclass = 'DO' and DO.active_flag = 1
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number
                                                left join tbItemCodes SI with(nolock) on A.scan_item = SI.code_id and SI.code_sclass = 'P3' and SI.active_flag = 1
                                                left join (select check_number,order_number from ttKuoYangScanLog group by check_number , order_number) T  on A.check_number = T.check_number 
                                                left join tbStation S with(nolock) on C.station = S.station_scode
                                                {0}
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and A.check_number = @check_number and R.Less_than_truckload = @Less_than_truckload {1}
												group by r.pieces, A.check_number, A.area,A.scan_item,A.area_arrive_code,B.supplier_name,arrive_option,exception_option,A.Cuser,AO.code_name,EO.code_name,write_off_type,A.driver_code,C.driver_code,driver_name,A.sign_form_image,tracking_number,tracking_number,T.order_number,station_name,driver_mobile,s.tel,Del_status,Convert(varchar(16), A.scan_date, 25),R.send_contact, driver_type_code, RO.code_name, SO.code_name, DO.code_name, SI.code_name
												order by scan_date desc", wherestr1, wherestr2);
                    }




                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        New_List_02.DataSource = dt;
                        New_List_02.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    //if (check_number.Text != "")
                    //{
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);
                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    #endregion

                    //cmd.CommandText = string.Format(@"--DECLARE @ORDER_NUMBER NVARCHAR(20)
                    //                              DECLARE @sign_form_image nvarchar(50)
                    //                              DECLARE @sign_field_image nvarchar(50)
                    //                              DECLARE @receipt_image nvarchar(100)
                    //                              DECLARE @customer_code NVARCHAR(10)
                    //                              DECLARE @supplier_code NVARCHAR(3)
                    //                              DECLARE @request_id bigint

                    //                              select top 1 @sign_form_image=A.sign_form_image, @sign_field_image=A.sign_field_image, @customer_code=dr.customer_code ,@supplier_code=dr.supplier_code  , 
                    //                              @request_id=dr.request_id   
                    //                              from ttDeliveryScanLog A with(nolock)
                    //                              left join tcDeliveryRequests  dr with(nolock) on dr.check_number = A.check_number 


                    //                              --left join ttDeliveryFile rtn on A.check_number  = rtn.check_number  and dr.request_id  = rtn.request_id 
                    //                              where 1=1  and A.check_number = @check_number and A.scan_item in('4') and dr.Less_than_truckload=@Less_than_truckload
                    //                              order by A.log_id desc

                    //                              select top 1 @receipt_image = ISNULL(file_path,'') +  ISNULL(file_name ,'') from ttDeliveryFileN where check_number = @check_number    order by udate desc

                    //                              SELECT @sign_form_image as sign_form_image,@sign_field_image as sign_field_image,@customer_code as customer_code,@supplier_code as supplier_code, @receipt_image as receipt_image, @request_id as request_id");

                    cmd.CommandText = string.Format(@"Select a.request_id , 
  a.check_number , 
  a.customer_code , 
  a.supplier_code , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok] , 
  sign_form_image , 
  sign_field_image
From 
(
Select request_id , 
  check_number , 
  customer_code , 
  supplier_code 
From [dbo].[tcDeliveryRequests] with (nolock)
Where check_number = @check_number
) a 
left join
(
Select request_id , 
  check_number , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok]
From CheckDeliveryPhotoData with (nolock)
Where check_number = @check_number
) b on a.request_id = b.request_id and a.check_number = b.check_number
left join
(
Select Top 1 check_number , sign_form_image , sign_field_image
From ttDeliveryScanLog with (nolock)
Where check_number = @check_number
and scan_item = '4'
Order by log_id desc
) c on a.check_number = c.check_number

                    ");

                    cmd.CommandTimeout = 9999;
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string filename = "";
                            string rtn_filename = "";
                            switch (manager_type)
                            {
                                case "0":  //總公司、系統管理員才可以看到整張簽單
                                case "1":
                                case "2":
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "4": //區配商：有開放權限，可看到自已的貨和區配底下自營的貨
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "3":
                                case "5": //自營商：自已的貨可以看到整張簽單
                                    string customer_code = dt.Rows[0]["customer_code"] != DBNull.Value ? dt.Rows[0]["customer_code"].ToString() : "";
                                    string master_code = Session["master_code"] != null ? Session["master_code"].ToString() : "";
                                    if (customer_code.IndexOf(master_code) > -1)
                                    {
                                        filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    }
                                    else
                                    {
                                        filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    }
                                    break;
                                default:
                                    filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    break;
                            }


                            //string filename_split = check_number.Text;
                            //int split_length = 5; //檔案每幾個字元會建立一個資料夾
                            //string path = "";
                            //int folder_deepth = filename_split.Length / split_length;
                            //for (var i = 0; i < folder_deepth; i++)
                            //{
                            //    path += filename_split.Substring(i * 5, 5) + @"/";
                            //}
                            //path += check_number.Text + ".jpg";


                            //string path2 = "";
                            //if (!string.IsNullOrEmpty(filename))
                            //{
                            //    string filename_split2 = filename.Substring(0, filename.Length - 4);

                            //    int folder_deepth2 = filename_split2.Length / split_length;
                            //    for (var i = 0; i < folder_deepth2; i++)
                            //    {
                            //        path2 += filename_split2.Substring(i * 5, 5) + @"/";
                            //    }
                            //    path2 += filename;
                            //}

                            string realUrl = "";
                            string type = "";

                            string yymmdd = dt.Rows[0]["yyyymmdd"].ToString();
                            string papaerPhoto = dt.Rows[0]["photo_paper"].ToString();
                            string digitPhoto = dt.Rows[0]["photo_digi_1"].ToString();
                            string sign_form_image = dt.Rows[0]["sign_form_image"].ToString();

                            string domain = ConfigurationManager.AppSettings["Sign_Receipt"];


                            if (dt.Rows[0]["is_photo_digi_1_ok"].ToString() == "True")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + digitPhoto;
                                type = "2";
                            }
                            else if (dt.Rows[0]["is_photo_paper_ok"].ToString() == "true")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + papaerPhoto;
                                type = "1";
                            }
                            else if (sign_form_image != "")
                            {
                                realUrl = domain + @"/UploadImages45/_No_Partition_Photo/" + sign_form_image;
                                type = "3";
                            }
                            else
                            {
                                type = "4";
                            }

                            Imglink.Attributes.Add("href", "ImageView2.aspx?ImgUrl=" + realUrl + "" + "&type=" + type + "");


                            if (realUrl == "")
                            {
                                Imglink.Style.Add("pointer-events", "none");
                                NonReceipt.Style.Add("display", "inline-block");
                            }
                            else
                            {
                                Imglink.Style.Add("pointer-events", "all");
                                NonReceipt.Style.Add("display", "none");
                                ImageButton1.Visible = true;
                            }


                            #region 舊查簽單邏輯
                            //if (filename != "")
                            //{
                            //    //先判斷網址是否有效，只出現有效的圖


                            //    string strUrl3 = "https://webservice.fs-express.com.tw/PHOTO/" + filename + "";
                            //    //string strUrl4 = "http://webserviceground.fs-express.com.tw/PHOTO/" + filename + "";                              
                            //    bool isUrl3oK = UrlCheck(strUrl3);
                            //    //bool isUrl4oK = UrlCheck(strUrl4);

                            //    string realUrl = "";
                            //    if (isUrl3oK)
                            //    {
                            //        realUrl = strUrl3;
                            //    }
                            //    else
                            //    {
                            //        string strUrl2Old = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //        string strUrl2 = "https://erp.fs-express.com.tw/UploadImages35/photo/" + path + "";
                            //        bool isUrl2OldoK = UrlCheck(strUrl2Old);
                            //        bool isUrl2oK = UrlCheck(strUrl2);

                            //        if (isUrl2oK)
                            //        {
                            //            realUrl = strUrl2;
                            //        }
                            //        else if (isUrl2OldoK)
                            //        {
                            //            realUrl = strUrl2Old;
                            //        }
                            //        else
                            //        {
                            //            //string strUrl1 = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename + "";
                            //            //bool isUrl1oK = UrlCheck(strUrl1);

                            //            //if (isUrl1oK)
                            //            //{
                            //            //    realUrl = strUrl1;
                            //            //}
                            //            //else
                            //            //{
                            //            //    realUrl = string.Empty;
                            //            //}
                            //            string strUrl1 = "https://erp.fs-express.com.tw/UploadImages35/photo/" + path2 + "";
                            //            bool isUrl1oK = UrlCheck(strUrl1);

                            //            if (isUrl1oK)
                            //            {
                            //                realUrl = strUrl1;
                            //            }
                            //            else
                            //            {
                            //                realUrl = string.Empty;
                            //            }
                            //        }
                            //    }

                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= " + realUrl + "");


                            //    string second_filename = check_number.Text + ".jpg";

                            //    string secondUrlold = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //    string secondUrl = "https://erp.fs-express.com.tw/UploadImages35/photo/" + path + "";

                            //    bool issecondUrloK = UrlCheck(secondUrl);
                            //    bool issecondUrloldoK = UrlCheck(secondUrlold);

                            //    string realSecondUrl = "";

                            //    if (issecondUrloK)
                            //    {
                            //        realSecondUrl = secondUrl;
                            //    }
                            //    else if (issecondUrloldoK)
                            //    {
                            //        realSecondUrl = secondUrlold;
                            //    }
                            //    if (!String.IsNullOrEmpty(realSecondUrl))
                            //    {
                            //        Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= " + realUrl + "" + "&ImgUrl2=" + realSecondUrl + "");


                            //    }
                            //    else
                            //    {

                            //        Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl= " + realUrl + "");

                            //    }


                            //    if (realUrl == "")
                            //    {
                            //        Imglink.Style.Add("pointer-events", "none");
                            //        NonReceipt.Style.Add("display", "inline-block");
                            //    }
                            //    else
                            //    {
                            //        Imglink.Style.Add("pointer-events", "all");
                            //        NonReceipt.Style.Add("display", "none");
                            //    }

                            //}
                            //else
                            //{
                            //    filename = check_number.Text + ".jpg";

                            //    //先判斷網址是否有效，只出現有效的圖
                            //    string strUrl6Old = "https://storage-for-station.s3.us-east-2.amazonaws.com/SignPaperPhoto/photo/" + path + "";
                            //    string strUrl6 = "https://erp.fs-express.com.tw/UploadImages35/photo/" + path + "";

                            //    bool isUrl6oK = UrlCheck(strUrl6);
                            //    bool isUrl6OldoK = UrlCheck(strUrl6Old);

                            //    string realUrl = "";

                            //    if (isUrl6oK)
                            //    {
                            //        realUrl = strUrl6;
                            //    }
                            //    else if (isUrl6OldoK)
                            //    {
                            //        realUrl = strUrl6Old;
                            //    }
                            //    else
                            //    {
                            //        string strUrl5 = "http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename + "";
                            //        bool isUrl5oK = UrlCheck(strUrl5);
                            //        if (isUrl5oK)
                            //        {
                            //            realUrl = strUrl5;
                            //        }
                            //        else
                            //        {
                            //            realUrl = string.Empty;
                            //        }
                            //    }


                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=" + realUrl + "");

                            //    if (realUrl == "")
                            //    {
                            //        Imglink.Style.Add("pointer-events", "none");
                            //        NonReceipt.Style.Add("display", "inline-block");
                            //    }
                            //    else
                            //    {
                            //        Imglink.Style.Add("pointer-events", "all");
                            //        NonReceipt.Style.Add("display", "none");
                            //    }

                            //}

                            #endregion

                            #region 回單影像
                            //rtn_filename = dt.Rows[0]["receipt_image"] != DBNull.Value ? dt.Rows[0]["receipt_image"].ToString() : "";
                            //if (rtn_filename != "")
                            //{
                            //    Regex reg = new Regex(";");
                            //    if (reg.IsMatch(rtn_filename))
                            //    {
                            //        string[] _rtn_filename = rtn_filename.Split(';');
                            //        foreach (string image in _rtn_filename)
                            //        {
                            //            string fileExtension = System.IO.Path.GetExtension(image).ToLower().ToString();

                            //            if (fileExtension == ".pdf")
                            //            {

                            //                // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ImgUrl + filename + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            //                RtnImglink.Attributes.Add("onclick", "window.open('" + image + "','mywindow','width=800,height=600, toolbar=yes');");
                            //                RtnImglink.Attributes.Remove("href");

                            //            }
                            //            else
                            //            {
                            //                RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + image);
                            //                RtnImglink.CssClass = "fancybox fancybox.iframe";
                            //            }

                            //        }
                            //    }
                            //    else
                            //    {
                            //        string fileExtension = System.IO.Path.GetExtension(rtn_filename).ToLower().ToString();

                            //        if (fileExtension == ".pdf")
                            //        {

                            //            // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ImgUrl + filename + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            //            RtnImglink.Attributes.Add("onclick", "window.open('" + rtn_filename + "','mywindow','width=800,height=600, toolbar=yes');");
                            //            RtnImglink.Attributes.Remove("href");

                            //        }
                            //        else
                            //        {
                            //            RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + rtn_filename);
                            //            RtnImglink.CssClass = "fancybox fancybox.iframe";
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    ImageButton2.Visible = false;
                            //}


                            rtnclick.Visible = true;
                            rtnclick.Attributes.Add("href", "../fileupload_signreceipt.aspx?check_number=" + check_number.Text.Trim() + dlsub_chknum.SelectedValue + "&request_id=" + dt.Rows[0]["request_id"].ToString() + "&RtnImglink=" + RtnImglink.ClientID);  //回單上傳鈕
                            #endregion
                        }
                        else
                        {
                            ImageButton1.Visible = false;
                            ImageButton2.Visible = false;
                            rtnclick.Visible = false;
                        }
                    }
                }
            }
        }
        else if (rb2.Checked)
        {
            if (order_number.Text != "")
            {
                string check_number_in_receipt = string.Empty;

                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.Clear();
                    cmd1.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd1.CommandText = "select top 1  view_auth  from tbFunctionsAuth where func_code = 'TA' and  account_code=@account_code";
                    DataTable dta = new DataTable();
                    dta = dbAdapter.getDataTable(cmd1);
                    if (dta.Rows.Count > 0)
                    {
                        if (dta.Rows[0]["view_auth"].ToString() == "0")
                        {
                            Bohotype.Visible = false;
                        }
                        else
                        {
                            Bohotype.Visible = true;
                        }
                    }
                    else
                    {
                        Bohotype.Visible = false;
                    }

                }

                //鎖權限
                string manager_type = Session["manager_type"].ToString(); //管理單位類別   
                string supplier_code = string.Empty;
                switch (manager_type)
                {
                    case "0":
                    case "1":
                    case "2":
                        supplier_code = "";   //一般員工/管理者可查所有站所下的所有客戶
                        break;
                    case "4":                 //站所主管只能看該站所下的客戶
                        using (SqlCommand cmd2 = new SqlCommand())
                        {
                            cmd2.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd2.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                            DataTable dt = dbAdapter.getDataTable(cmd2);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                        break;
                    case "5":
                        supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                        break;
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@DeliveryType", DeliveryType);
                    #endregion

                    cmd.Parameters.AddWithValue("@customer_code", supplier_code);

                    cmd.CommandText = string.Format(@"DECLARE @statement nvarchar(4000)
                                                  DECLARE @where nvarchar(1000)
                                                  DECLARE @orderby nvarchar(40)

                                                  SET @where = ''
                                                  SET @orderby = ''

                                                  SET @statement = 
                                                  'Select 
--F1.station_name as sendstation,
--G1.station_name as arrivestation,
case when A.DeliveryType = ''R'' then  (select B1.station_name from 
												  ttArriveSitesScattered A1 With(Nolock) 
												  LEFT JOIN tbStation B1 with(nolock) on  B1.station_scode =  A1.station_code
												  where A1.post_city = A.send_city and A1.post_area= A.send_area ) else
F1.station_name end sendstation,
case when A.DeliveryType = ''R'' AND A.supplier_code =''F35'' then (select D1.station_name from 
												  ttArriveSitesScattered C1 With(Nolock) 
												  LEFT JOIN tbStation D1 with(nolock) on  D1.station_scode =  C1.station_code
												  where C1.post_city = A.receive_city and C1.post_area= A.receive_area ) else
G1.station_name end arrivestation,
 A.send_area,A.receive_area, Convert(VARCHAR, A.print_date,111) as print_date,CONVERT(varchar,arrive_assign_date,111) as arrive_assign_date,   CONVERT(VARCHAR, A.supplier_date,111) as supplier_date,
 A.time_period, A.order_number, A.check_number,
CASE A.pricing_type WHEN  ''01'' THEN A.plates WHEN ''02'' THEN A.pieces WHEN ''03'' THEN A.cbm WHEN ''04'' THEN A.plates END AS quant, 
A.product_category, C.code_name as product_category_name, 
A.subpoena_category, B.code_name as subpoena_category_name,
A.customer_code, A.send_contact, A.send_tel, A.receive_contact, A.receive_tel1,
A.request_id, Convert(VARCHAR, A.ship_date,111) as ship_date,  
case A.holiday_delivery when 1 then ''假日配送'' else ''一般配送'' end as holiday_delivery,
isnull(_fee.supplier_fee ,0) ''貨件運費'' ,
 A.arrive_to_pay_append ''到付追加未稅'', A.arrive_to_pay_append  *1.05 ''到付追加含稅'',
 A.collection_money ''代收金額'',
isnull(_fee.supplier_first_fee,0)  ''首件費用'',	
 isnull(_fee.add_price ,0) ''續件費用'',
A.return_check_number ''return_check_number'', A.roundtrip_checknumber
from tcDeliveryRequests A with(nolock)
LEFT JOIN tbItemCodes B with(Nolock) on B.code_id = A.subpoena_category and B.code_bclass = ''2'' and B.code_sclass = ''S2''
LEFT JOIN tbItemCodes C with(Nolock) on C.code_id = A.product_category and C.code_bclass = ''2'' and C.code_sclass = ''S3''
LEFT JOIN tbCustomers D With(Nolock) on D.customer_code = A.customer_code 
 LEFT JOIN tbSuppliers E With(Nolock) on A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'''') <> ''''
--LEFT JOIN ttArriveSitesScattered F With(Nolock) on F.post_city = A.send_city and F.post_area= A.send_area 
LEFT JOIN tbStation F1 with(nolock) on  F1.station_code =  a.supplier_code --F.station_code 
LEFT JOIN ttArriveSitesScattered G With(Nolock) on G.post_city = A.receive_city and G.post_area= A.receive_area 
LEFT JOIN tbStation G1 with(nolock) on G1.station_scode =  A.area_arrive_code  
CROSS APPLY dbo.fu_GetLTShipFeeByRequestId(A.request_id) _fee'
SET @where = ' where ( A.print_date >=  DATEADD(MONTH,-1,getdate()) or A.print_date is null)' 

SET @where = @where+' and A.order_number = '''+ @order_number+ ''''
                                                  SET @where=@where+' and A.Less_than_truckload = '''+ @Less_than_truckload+ ''''
                                                  
                                                  IF (@DeliveryType <> '') BEGIN 
                                                      SET @where=@where+' and A.DeliveryType = '''+ @DeliveryType+ ''''
                                                  END

                                                  IF (@customer_code <> '') BEGIN 
                                                      SET @where=@where+' and A.supplier_code in ('''+ @customer_code +''''
                                                  END

                                                  SET @orderby =' order by A.check_number'
                                                  SET @statement = @statement + @where + @orderby

                                                  --print @statement
                                                  EXEC sp_executesql @statement ");
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string customer_code = dt.Rows[0]["customer_code"].ToString();
                            string send_contact = dt.Rows[0]["send_contact"].ToString();
                            send_contact = send_contact.Length > 1 ? (IsChineseRegex(send_contact.Substring(0, 1)) ? "*" + send_contact.Substring(1) : "*" + send_contact.Substring(2)) : "*";
                            string send_tel = dt.Rows[0]["send_tel"].ToString();
                            send_tel = send_tel.Length > 2 ? "*" + send_tel.Substring(send_tel.Length - 2, 2) : send_tel;  //右截取：str.Substring(str.Length-i,i) 返回，返回右邊的i個字符
                            string receive_contact = dt.Rows[0]["receive_contact"].ToString();
                            receive_contact = receive_contact.Length > 1 ? "*" + receive_contact.Substring(1) : "*";
                            string receive_tel1 = dt.Rows[0]["receive_tel1"].ToString();
                            receive_tel1 = receive_tel1.Length > 2 ? "*" + receive_tel1.Substring(receive_tel1.Length - 2, 2) : receive_tel1;
                            string id = dt.Rows[0]["request_id"].ToString();
                            lbSend.Text = customer_code + "<BR/>" + send_contact + "<BR/>TEL:" + send_tel + "<BR/><a href='LT_transport_2_edit.aspx?BK=Y&r_type=0&request_id=" + id + "'  class='fancybox fancybox.iframe btn btn-primary _d_btn ' id='updclick'>查看</a>";

                            lbReceiver.Text = "<BR/>" + receive_contact + "<BR/>TEL:" + receive_tel1;
                            check_number_in_order_number.Text = dt.Rows[0]["check_number"].ToString();
                            check_number_in_receipt = dt.Rows[0]["check_number"].ToString();
                        }
                        Delivery_Info_Repeater.DataSource = dt;
                        Delivery_Info_Repeater.DataBind();
                        Delivery_Info_Repeater2.DataSource = dt;
                        Delivery_Info_Repeater2.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    string wherestr1 = string.Empty;
                    string wherestr2 = string.Empty;
                    #region 關鍵字


                    //if (check_number.Text != "")
                    //{
                    //cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);

                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    if (supplier_code != "")
                    {
                        cmd.Parameters.AddWithValue("@customer_code", supplier_code);
                        wherestr2 = "and  R.supplier_code in  in ('" + supplier_code + "')";
                    }

                    #endregion


                    cmd.CommandText = string.Format(@"
                                                Select ROW_NUMBER() OVER(ORDER BY min(A.scan_date) desc) AS NO , A.check_number,
                                                SI.code_name as scan_name,
                                                Convert(varchar, min(A.scan_date),120) as scan_date, A.area_arrive_code, B.supplier_name ,
                                                A.arrive_option, A.exception_option , min(A.log_id)'log_id',A.Cuser,
                                                CASE A.scan_item WHEN '3' THEN AO.code_name WHEN '5' THEN RO.code_name WHEN '6' THEN RO.code_name ELSE  CASE WHEN EO.code_name='無' THEN '' ELSE EO.code_name END  END as status,
                                                CASE A.write_off_type WHEN 1 THEN '人工銷號' WHEN 0 THEN '籠車路線' else '籠車路線' END AS write_off_type,
                                                A.driver_code , C.driver_name , A.sign_form_image, 
NULL tracking_number,
T.order_number,
S. station_name, 
C.driver_mobile,S.tel, C.driver_type_code,
Case A.Del_status When 1 then '1' Else '0' END as Del_status,
                                                Convert(varchar, min(A.scan_date),112)+'01' as shift,
												Convert(varchar(16), min(A.scan_date), 121),
                                                Count(A.log_id)'quant'
                                                from ttDeliveryScanLog A with(nolock)
                                                left join tbSuppliers B with(nolock) on A.area_arrive_code = B.supplier_code and ISNULL(B.supplier_code,'') <> ''
                                                left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                left join tbItemCodes AO with(nolock) on A.arrive_option = AO.code_id and AO.code_sclass = 'AO' and AO.active_flag  = 1
                                                left join tbItemCodes EO with(nolock) on A.exception_option = EO.code_id and EO.code_sclass = 'EO' and EO.active_flag = 1 
                                                left join tbItemCodes RO with(nolock) on A.receive_option = RO.code_id and RO.code_sclass = 'RO' and RO.active_flag = 1 
												left join tbItemCodes DO with(nolock) on A.delivery_option = DO.code_id and DO.code_sclass = 'DO' and DO.active_flag = 1
												left join tbItemCodes SO with(nolock) on A.send_option = SO.code_id and SO.code_sclass = 'SO' and SO.active_flag = 1
                                                left join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number
                                                left join tbItemCodes SI with(nolock) on A.scan_item = SI.code_id and SI.code_sclass = 'P3' and SI.active_flag = 1
                                                left join (select check_number,order_number from ttKuoYangScanLog group by check_number , order_number) T  on A.check_number = T.check_number 
                                                left join tbStation S with(nolock) on C.station = S.station_scode
                                                where A.scan_date  >=  DATEADD(MONTH,-12,getdate()) and A.scan_item in('1','2','3','5','6','7')
												and R.order_number = @order_number
                                                and R.Less_than_truckload = @Less_than_truckload
{0}
												group by A.check_number, A.area,A.scan_item,A.area_arrive_code,B.supplier_name,arrive_option,exception_option,A.Cuser,AO.code_name,EO.code_name,write_off_type,A.driver_code,C.driver_code,driver_name,A.sign_form_image,tracking_number,tracking_number,T.order_number,station_name,driver_mobile,s.tel,Del_status,Convert(varchar(16), A.scan_date, 25),R.send_contact, driver_type_code, RO.code_name, DO.code_name, SO.code_name, SI.code_name
												order by scan_date desc", wherestr2);
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        New_List_02.DataSource = dt;
                        New_List_02.DataBind();
                    }
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Clear();
                    #region 關鍵字

                    cmd.Parameters.AddWithValue("@Less_than_truckload", "1");
                    //if (check_number.Text != "")
                    //{
                    //cmd.Parameters.AddWithValue("@check_number", check_number.Text + dlsub_chknum.SelectedValue);
                    //}

                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);
                    cmd.Parameters.AddWithValue("@check_number", check_number_in_receipt);

                    #endregion

                    cmd.CommandText = string.Format(@"Select a.request_id , 
  a.check_number , 
  a.customer_code , 
  a.supplier_code , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok] , 
  sign_form_image , 
  sign_field_image
From 
(
Select request_id , 
  check_number , 
  customer_code , 
  supplier_code 
From [dbo].[tcDeliveryRequests] with (nolock)
Where check_number = @check_number
) a 
left join
(
Select request_id , 
  check_number , 
  [yyyymmdd],
  [is_delete],
  [photo_paper],
  [is_photo_paper_ok],
  [photo_digi_1],
  [is_photo_digi_1_ok],
  [photo_digi_2],
  [is_photo_digi_2_ok]
From CheckDeliveryPhotoData with (nolock)
Where check_number = @check_number
) b on a.request_id = b.request_id and a.check_number = b.check_number
left join
(
Select Top 1 check_number , sign_form_image , sign_field_image
From ttDeliveryScanLog with (nolock)
Where check_number = @check_number
and scan_item = '4'
Order by log_id desc
) c on a.check_number = c.check_number");
                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string filename = "";
                            string rtn_filename = "";
                            switch (manager_type)
                            {
                                case "0":  //總公司、系統管理員才可以看到整張簽單
                                case "1":
                                case "2":
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "4": //區配商：有開放權限，可看到自已的貨和區配底下自營的貨
                                    filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    break;
                                case "3":
                                case "5": //自營商：自已的貨可以看到整張簽單
                                    string customer_code = dt.Rows[0]["customer_code"] != DBNull.Value ? dt.Rows[0]["customer_code"].ToString() : "";
                                    string master_code = Session["master_code"] != null ? Session["master_code"].ToString() : "";
                                    if (customer_code.IndexOf(master_code) > -1)
                                    {
                                        filename = dt.Rows[0]["sign_form_image"] != DBNull.Value ? dt.Rows[0]["sign_form_image"].ToString() : "";
                                    }
                                    else
                                    {
                                        filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    }
                                    break;
                                default:
                                    filename = dt.Rows[0]["sign_field_image"] != DBNull.Value ? dt.Rows[0]["sign_field_image"].ToString() : "";
                                    break;
                            }
                            string realUrl = "";
                            string type = "";

                            string yymmdd = dt.Rows[0]["yyyymmdd"].ToString();
                            string papaerPhoto = dt.Rows[0]["photo_paper"].ToString();
                            string digitPhoto = dt.Rows[0]["photo_digi_1"].ToString();
                            string sign_form_image = dt.Rows[0]["sign_form_image"].ToString();

                            string domain = ConfigurationManager.AppSettings["Sign_Receipt"];


                            if (dt.Rows[0]["is_photo_digi_1_ok"].ToString() == "True")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + digitPhoto;
                                type = "2";
                            }
                            else if (dt.Rows[0]["is_photo_paper_ok"].ToString() == "true")
                            {
                                realUrl = domain + @"/Uploadimages45/Photo/" + yymmdd + @"/" + papaerPhoto;
                                type = "1";
                            }
                            else if (sign_form_image != "")
                            {
                                realUrl = domain + @"/UploadImages45/_No_Partition_Photo/" + sign_form_image;
                                type = "3";
                            }
                            else
                            {
                                type = "4";
                            }

                            Imglink.Attributes.Add("href", "ImageView2.aspx?ImgUrl=" + realUrl + "" + "&type=" + type + "");


                            if (realUrl == "")
                            {
                                Imglink.Style.Add("pointer-events", "none");
                                NonReceipt.Style.Add("display", "inline-block");
                            }
                            else
                            {
                                Imglink.Style.Add("pointer-events", "all");
                                NonReceipt.Style.Add("display", "none");
                                ImageButton1.Visible = true;
                            }

                            #region 舊簽單邏輯
                            //if (filename != "")
                            //{
                            //    ImageButton1.Visible = true;
                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename);
                            //}
                            //else
                            //{
                            //    ImageButton1.Visible = false;
                            //    filename = check_number.Text + ".jpg";
                            //    //ImageButton1.Visible = false;
                            //    Imglink.Attributes.Add("href", "ImageView.aspx?ImgUrl=http://220.128.210.180:10080/JUNFU_APP_WebService/PHOTO/" + "&filename=" + filename);

                            //}
                            #endregion 

                            #region 回單影像
                            //rtn_filename = dt.Rows[0]["receipt_image"] != DBNull.Value ? dt.Rows[0]["receipt_image"].ToString() : "";
                            //if (rtn_filename != "")
                            //{
                            //    Regex reg = new Regex(";");
                            //    if (reg.IsMatch(rtn_filename))
                            //    {
                            //        string[] _rtn_filename = rtn_filename.Split(';');
                            //        foreach (string image in _rtn_filename)
                            //        {
                            //            string fileExtension = System.IO.Path.GetExtension(image).ToLower().ToString();

                            //            if (fileExtension == ".pdf")
                            //            {

                            //                // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ImgUrl + filename + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            //                RtnImglink.Attributes.Add("onclick", "window.open('" + image + "','mywindow','width=800,height=600, toolbar=yes');");
                            //                RtnImglink.Attributes.Remove("href");

                            //            }
                            //            else
                            //            {
                            //                RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + image);
                            //                RtnImglink.CssClass = "fancybox fancybox.iframe";
                            //            }

                            //        }
                            //    }
                            //    else
                            //    {
                            //        string fileExtension = System.IO.Path.GetExtension(rtn_filename).ToLower().ToString();

                            //        if (fileExtension == ".pdf")
                            //        {

                            //            // ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ImgUrl + filename + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                            //            RtnImglink.Attributes.Add("onclick", "window.open('" + rtn_filename + "','mywindow','width=800,height=600, toolbar=yes');");
                            //            RtnImglink.Attributes.Remove("href");

                            //        }
                            //        else
                            //        {
                            //            RtnImglink.Attributes.Add("href", "ImageView.aspx?filename=" + rtn_filename);
                            //            RtnImglink.CssClass = "fancybox fancybox.iframe";
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    ImageButton2.Visible = false;
                            //}
                            #endregion



                            rtnclick.Visible = true;
                            rtnclick.Attributes.Add("href", "../fileupload_signreceipt.aspx?check_number=" + check_number.Text.Trim() + dlsub_chknum.SelectedValue + "&request_id=" + dt.Rows[0]["request_id"].ToString() + "&RtnImglink=" + RtnImglink.ClientID);  //回單上傳鈕

                        }
                        else
                        {
                            ImageButton1.Visible = false;
                            ImageButton2.Visible = false;
                            rtnclick.Visible = false;
                        }
                    }
                }

            }
        }
    }

    private void readInventory()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            cmd.CommandText = @"select ROW_NUMBER() OVER(ORDER BY inventory_id desc) 'NO', i.scanning_dt, s.station_name, d.driver_code + d.driver_name 'driver' from ttInventory i left join tbStation s on i.station_code = s.station_scode left join tbDrivers d on i.driver_code = d.driver_code where check_number = @check_number order by inventory_id desc";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {

                Inventory_List.DataSource = dt;
                Inventory_List.DataBind();
            }
        }
    }

    protected void btnDetail_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divMaster.Visible = false;
        readdata();
        readdetail();
    }

    protected void Bohotype_Click(object sender, EventArgs e)
    {
        if (Func.IsNormalDelivery(check_number.Text))
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('此筆已正常配達，無法再進行調整');</script><noscript></noscript>");
        }
        else if (Func.IsCancelled(check_number.Text))
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('此筆已經銷單，無法再進行調整');</script><noscript></noscript>");
        }
        else if (Func.IsRefusedOrder(check_number.Text))
        {
            HttpContext.Current.Response.Write("<script language='javascript'>alert('此筆已經產生拒收單，無法再進行調整');</script><noscript></noscript>");
        }
        else
        {
            sta.SelectedValue = "";
            states.SelectedValue = "";
            emp.SelectedValue = "";
            CountA.Text = "";
            AreaA.Text = "";
            DateA.Text = "";
            Hotype.Visible = true;
        }


    }



    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {

            case "cmddelete":

                using (SqlCommand cmddel = new SqlCommand())
                {


                    cmddel.Parameters.Add("@log_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
                    cmddel.CommandText = "delete from ttDeliveryScanLog where log_id=@log_id ";
                    dbAdapter.execNonQuery(cmddel);
                }
                readdata();
                Hotype.Visible = false;
                divMaster.Visible = true;
                HttpContext.Current.Response.Write("<script language='javascript'>alert('刪除成功');</script><noscript></noscript>");
                return;
                break;

        }
    }



    protected void btExit_Click(object sender, EventArgs e)
    {
        divDetail.Visible = true;
        divMaster.Visible = true;
    }

    protected void btExittype_Click(object sender, EventArgs e)
    {
        Hotype.Visible = false;
        divMaster.Visible = true;

    }

    protected void dlP1_SelectedIndexChanged(object sender, EventArgs e)
    {


        #region 處置說明-說明項目
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@category", dlP1.SelectedValue);
            cmd.CommandText = "select * from condition_category where level = '2' and (attribution = @category or attribution='') order by id";
            dlP2.DataSource = dbAdapter.getDataTable(cmd);
            dlP2.DataValueField = "category";
            dlP2.DataTextField = "category";
            dlP2.DataBind();
            dlP2.Items.Insert(0, new ListItem("===請選擇===", ""));
        }
        #endregion

        using (SqlCommand cmd = new SqlCommand())
        {
            //cmd.Parameters.AddWithValue("@code_id", dlP2.SelectedValue);
            cmd.CommandText = "select * from condition_category where level = '3'  order by id";
            dlP3.DataSource = dbAdapter.getDataTable(cmd);
            dlP3.DataValueField = "category";
            dlP3.DataTextField = "category";
            dlP3.DataBind();
            dlP3.Items.Insert(0, new ListItem("===請選擇===", ""));
        }
    }


    protected void dlP2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ErrLabel.Visible = false;
        if (dlP2.SelectedValue.Trim() == "修改地址")   // 修改地址
        {
            if (Func.IsNormalDelivery(check_number.Text))
            {
                ErrLabel.Text = "此筆已正常配達，無法變更地址";
                ErrLabel.Visible = true;
            }
            else if (Func.IsCancelled(check_number.Text))
            {
                ErrLabel.Text = "此筆已經銷單，無法變更地址";
                ErrLabel.Visible = true;
            }

            else
            {
                // show          
                ScriptManager.RegisterStartupScript(UpdateAddressPanel, this.GetType(), "openModal", "openModal();", true);
            }
        }
    }

    protected void UpdateAddress(object sender, EventArgs e)
    {
        AddressParsingInfo addressParsingInfo = new AddressParsingInfo(); ;
        string strcustomer_code = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_Number", check_number.Text);
            cmd.CommandText = @"select top 1 customer_code  from tcDeliveryRequests where check_Number = @check_Number order by request_id desc";
            DataTable dt = dbAdapter.getDataTable(cmd);
            if (dt != null && dt.Rows.Count > 0)
            {
                strcustomer_code = dt.Rows[0]["customer_code"].ToString();
            }
        }
        //特服區判斷
        if (GetSpecialAreaFee(City.Text.Trim() + Area.Text.Trim() + NewAddress.Text.Trim(), strcustomer_code, "1", "1").id.ToString() != "0")
        {

        }
        else
        {
            //清空特服費相關
            lbSpecialAreaId.Text = "";
            lbSpecialAreaFee.Text = "";

            //檢查地址
            addressParsingInfo = GetAddressParsing(City.SelectedItem.Text + Area.SelectedItem.Text + NewAddress.Text.Trim(), strcustomer_code, "1", "1");
            if (check_address == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查地址是否正確');</script>", false);
                return;
            }
        }
        //dlP2.SelectedValue = "更改收件資料";   // 選自行輸入 original magic number is 10380

        // 抓舊地址，填進處置說明
        string oldAddress = string.Empty;
        string getAddressSql = @"select top 1 receive_city + receive_area + receive_address as receive_address
            from tcDeliveryRequests where check_number = @checkNumber order by request_id desc";

        using (SqlCommand cmd = new SqlCommand(getAddressSql))
        {
            cmd.Parameters.AddWithValue("@checkNumber", check_number.Text);
            oldAddress = dbAdapter.getScalarBySQL(cmd) as string;
        }

        string disposeDisc = string.Format("●貨人●●●通知改址，舊地址：#{0}#，新地址請見託運明細", oldAddress);
        dispose_desc.Text = disposeDisc;

        /*
        // 新地址和到著碼寫回 DeliveryRequests
        string newAddress = addressParsingInfo.City + addressParsingInfo.Town + addressParsingInfo.Road;

        string setAddressSql = @"update tcDeliveryRequests set receive_city = @receive_city, receive_area = @receive_area, receive_address = @address, area_arrive_code = @areaArriveCode, SpecialAreaId=@SpecialAreaId, SpecialAreaFee=@SpecialAreaFee,ReceiveCode=@ReceiveCode,ReceiveSD=@ReceiveSD,ReceiveMD=@ReceiveMD
                        where check_number = @checkNumber";
        using (SqlCommand cmd = new SqlCommand(setAddressSql))
        {
            // cmd.Parameters.AddWithValue("@receive_city", City.SelectedItem.Text);
            //cmd.Parameters.AddWithValue("@receive_area", Area.SelectedItem.Text);
            //cmd.Parameters.AddWithValue("@address", NewAddress.Text);

            cmd.Parameters.AddWithValue("@receive_city", addressParsingInfo.City);
            cmd.Parameters.AddWithValue("@receive_area", addressParsingInfo.Town);
            cmd.Parameters.AddWithValue("@address", addressParsingInfo.Road);
            cmd.Parameters.AddWithValue("@ReceiveCode", addressParsingInfo.StationCode);
            cmd.Parameters.AddWithValue("@ReceiveSD", addressParsingInfo.SalesDriverCode);
            cmd.Parameters.AddWithValue("@ReceiveMD", addressParsingInfo.MotorcycleDriverCode);
            if (lbAreaArriveCode.Text != "")
            {
                cmd.Parameters.AddWithValue("@areaArriveCode", lbAreaArriveCode.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@areaArriveCode", areaArriveCode);
            }
            cmd.Parameters.AddWithValue("@SpecialAreaId", lbSpecialAreaId.Text);
            cmd.Parameters.AddWithValue("@SpecialAreaFee", lbSpecialAreaFee.Text);
            cmd.Parameters.AddWithValue("@checkNumber", check_number.Text);
            dbAdapter.execNonQuery(cmd);
        }
        */
    }

    protected void sta_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 站所抓員工
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@station", sta.SelectedValue);
            cmd.CommandText = "select * from tbDrivers where station=@station   and  active_flag=1  order by driver_id";
            emp.DataSource = dbAdapter.getDataTable(cmd);
            emp.DataValueField = "driver_code";
            emp.DataTextField = "driver_name";
            emp.DataBind();
            emp.Items.Insert(0, new ListItem("===請選擇===", ""));
        }
        #endregion
    }

    protected void btnSaveDatail_Click(object sender, EventArgs e)
    {

        //處置說明記錄檔 ttDeliveryDisposeLog
        #region 新增    


        using (SqlCommand cmd = new SqlCommand())
        {


            if (check_number.Text.Length != 0)
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            }
            else
            {
                cmd.Parameters.AddWithValue("@check_number", check_number_in_order_number.Text.ToString());
            }
            cmd.Parameters.AddWithValue("@status_type", dlP1.SelectedValue.ToString());                   //狀況類別1
            //cmd.Parameters.AddWithValue("@status_type", dlP2.SelectedValue.ToString());                   //狀況類別2
            cmd.Parameters.AddWithValue("@dispose_desc", dispose_desc.Text);                              //處置說明

            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
            cmd.Parameters.AddWithValue("@attribution", dlP1.SelectedValue.ToString());                   //狀況分類
            cmd.Parameters.AddWithValue("@status_desc", dlP2.SelectedValue.ToString());                   //狀況分類
            cmd.Parameters.AddWithValue("@origin", dlP3.SelectedValue.ToString());                   //狀況分類
            cmd.CommandText = dbAdapter.SQLdosomething("ttDeliveryDisposeLog", cmd, "insert");
            dbAdapter.execNonQuery(cmd);
        }

        #endregion

        if (dlP2.SelectedValue.Trim() == "修改地址")   // 修改地址
        {
            if (!ErrLabel.Text.Equals(null) && !ErrLabel.Text.Equals(""))
            {
                HttpContext.Current.Response.Write("<script language='javascript'>alert('此筆不可修改');</script><noscript></noscript>");
                return;
            }


            string strcustomer_code = "";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@check_Number", check_number.Text);
                cmd.CommandText = @"select top 1 customer_code  from tcDeliveryRequests where check_Number = @check_Number order by request_id desc";
                DataTable dt = dbAdapter.getDataTable(cmd);
                if (dt != null && dt.Rows.Count > 0)
                {
                    strcustomer_code = dt.Rows[0]["customer_code"].ToString();
                }
            }
            ;
            //檢查地址
            AddressParsingInfo addressParsingInfo = GetAddressParsing(City.Text.Trim() + Area.Text.Trim() + NewAddress.Text.Trim(), strcustomer_code, "1", "1");
            if (check_address == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查地址是否正確');</script>", false);
                return;
            }

            // 新地址和到著碼寫回 DeliveryRequests
            //string areaArriveCode = string.Empty;
            //string newAddress = City.SelectedItem.Text + Area.SelectedItem.Text + NewAddress.Text;
            string newAddress = addressParsingInfo.City + addressParsingInfo.City + addressParsingInfo.Road;
            //string getAreaArriveCodeSql = "select station_code from ttArriveSitesScattered where post_city = @city and post_area = @area";
            //using (SqlCommand cmd = new SqlCommand(getAreaArriveCodeSql))
            //{
            //    cmd.Parameters.AddWithValue("@area", Area.SelectedItem.Text);
            //    cmd.Parameters.AddWithValue("@city", City.SelectedItem.Text);
            //    areaArriveCode = dbAdapter.getScalarBySQL(cmd) as string;
            //}

            string setAddressSql = @"update tcDeliveryRequests set receive_city = @receive_city, receive_area = @receive_area, receive_address = @address, area_arrive_code = @areaArriveCode, SpecialAreaId=@SpecialAreaId, SpecialAreaFee=@SpecialAreaFee,ReceiveCode=@ReceiveCode,ReceiveSD=@ReceiveSD,ReceiveMD=@ReceiveMD
                        where check_number = @checkNumber";
            using (SqlCommand cmd = new SqlCommand(setAddressSql))
            {
                // cmd.Parameters.AddWithValue("@receive_city", City.SelectedItem.Text);
                //cmd.Parameters.AddWithValue("@receive_area", Area.SelectedItem.Text);
                //cmd.Parameters.AddWithValue("@address", NewAddress.Text);

                cmd.Parameters.AddWithValue("@receive_city", City.Text.Trim());
                cmd.Parameters.AddWithValue("@receive_area", Area.Text.Trim());
                cmd.Parameters.AddWithValue("@address", NewAddress.Text.Trim());
                cmd.Parameters.AddWithValue("@ReceiveCode", addressParsingInfo.StationCode);
                cmd.Parameters.AddWithValue("@ReceiveSD", addressParsingInfo.SalesDriverCode == null ? "" : addressParsingInfo.SalesDriverCode);
                cmd.Parameters.AddWithValue("@ReceiveMD", addressParsingInfo.MotorcycleDriverCode == null ? "" : addressParsingInfo.MotorcycleDriverCode);
                if (lbAreaArriveCode.Text != "")
                {
                    cmd.Parameters.AddWithValue("@areaArriveCode", lbAreaArriveCode.Text);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@areaArriveCode", areaArriveCode);
                }
                cmd.Parameters.AddWithValue("@SpecialAreaId", lbSpecialAreaId.Text);
                cmd.Parameters.AddWithValue("@SpecialAreaFee", lbSpecialAreaFee.Text);
                cmd.Parameters.AddWithValue("@checkNumber", check_number.Text);
                dbAdapter.execNonQuery(cmd);
            }


            #region 95/99產生郵局流水號
            bool hasPostNum = false;
            DataTable tbPost = new DataTable();
            using (SqlCommand sql = new SqlCommand())
            {
                sql.CommandText = string.Format(@"SELECT * FROM [JunFuReal].[dbo].[Post_request] WHERE jf_check_number = '{0}' ORDER BY request_id desc", check_number.Text);
                tbPost = dbAdapter.getDataTable(sql);
                if (tbPost.Rows.Count > 0)
                {
                    hasPostNum = true;
                }
            }
            if ((areaArriveCode == "95" || areaArriveCode == "99") && !hasPostNum)
            {
                using (SqlCommand sql = new SqlCommand())
                {
                    sql.CommandText = string.Format(@"SELECT * FROM [JunFuReal].[dbo].tcDeliveryRequests WHERE check_number = '{0}' ORDER BY request_id desc", check_number.Text);
                    DataTable tbRequest = dbAdapter.getDataTable(sql);
                    if (tbRequest.Rows.Count > 0)
                    {
                        string requestId = tbRequest.Rows[0]["request_id"].ToString();
                        requestId = string.Format("{0,8:00000000}", int.Parse(requestId));
                        var zipcode = Func.GetZipCode(newAddress);
                        using (SqlCommand sql2 = new SqlCommand())
                        {
                            var udate = DateTime.Now;
                            var uuser = Session["account_code"];
                            var strpost_number = Func.GetPostNumber();
                            var post_check_code = Func.GetPostNumberCheckCode(strpost_number, zipcode);

                            sql2.Parameters.AddWithValue("@jf_request_id", requestId);
                            sql2.Parameters.AddWithValue("@jf_check_number", tbRequest.Rows[0]["check_number"].ToString());
                            sql2.Parameters.AddWithValue("@post_number", strpost_number);
                            sql2.Parameters.AddWithValue("@zipcode5", zipcode);
                            sql2.Parameters.AddWithValue("@check_code", post_check_code);
                            sql2.Parameters.AddWithValue("@cdate", udate);
                            sql2.Parameters.AddWithValue("@udate", udate);
                            sql2.Parameters.AddWithValue("@cuser", uuser);
                            sql2.Parameters.AddWithValue("@uuser", uuser);
                            sql2.CommandText = dbAdapter.genInsertComm("Post_request", false, sql2);
                            dbAdapter.execNonQuery(sql2);
                        }
                    }
                }
            }
            else if ((areaArriveCode == "95" || areaArriveCode == "99") && hasPostNum)
            {
                var zipcode = Func.GetZipCode(newAddress);
                var post_check_code = Func.GetPostNumberCheckCode(tbPost.Rows[0]["post_number"].ToString(), zipcode);
                string requestId = tbPost.Rows[0]["request_id"].ToString();
                using (SqlCommand sqlCommand2 = new SqlCommand())
                {
                    sqlCommand2.Parameters.AddWithValue("@zipcode5", zipcode);
                    sqlCommand2.Parameters.AddWithValue("@check_code", post_check_code);
                    sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", requestId);
                    sqlCommand2.CommandText = dbAdapter.genUpdateComm("Post_request", sqlCommand2);
                    dbAdapter.execNonQuery(sqlCommand2);
                }
            }
            #endregion
        }

        readdetail();
        dispose_desc.Text = string.Empty;
        dlP1.Text = string.Empty;
        dlP2.Text = string.Empty;
        //儲存後清空
        lbAreaArriveCode.Text = string.Empty;
        return;
    }

    protected void btnHotypeSaveDatail_Click(object sender, EventArgs e)
    {
        //補貨態
        #region 新增    
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);                              //貨運單號
            cmd.Parameters.AddWithValue("@driver_code", emp.SelectedItem.Value);                   //司機
            cmd.Parameters.AddWithValue("@scan_item", scanItem.SelectedValue);                              //配達
            cmd.Parameters.AddWithValue("@scan_date", DateA.Text);                              //日期
            if (scanItem.SelectedValue == "3")
            {
                cmd.Parameters.AddWithValue("@arrive_option", states.SelectedItem.Value);                              //狀態
            }
            else if (scanItem.SelectedValue == "5")
            {
                cmd.Parameters.AddWithValue("@receive_option", states.SelectedItem.Value);
            }

            cmd.Parameters.AddWithValue("@area", AreaA.Text);                              //駐區
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
            cmd.Parameters.AddWithValue("@Del_status", 1);                               //寫入1 為可以刪除的狀態
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間            

            cmd.CommandText = dbAdapter.genInsertComm("ttDeliveryScanLog", true, cmd);
            int result = 0;
            if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
        }

        #endregion
        readdata();
        Hotype.Visible = false;
        divMaster.Visible = true;
        HttpContext.Current.Response.Write("<script language='javascript'>alert('新增完成');</script><noscript></noscript>");



        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);
        return;

    }

    private void readdetail()
    {
        string managerType = Session["manager_type"].ToString();
        string customerCode = Session["customer_code"].ToString();

        string customerFilter = string.Empty;
        if (managerType == "5" && customerCode.Length > 0)
            customerFilter += string.Format(" AND R.customer_code = '{0}'", customerCode);

        if (rb1.Checked && check_number.Text != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@check_number", check_number.Text);
                cmd.CommandText = string.Format(@"select  ROW_NUMBER() OVER(ORDER BY A.cdate desc) AS NO ,
                                Convert(varchar, A.cdate,120) as cdate , A.status_desc ,origin, A.dispose_desc,C.user_name 
                                from ttDeliveryDisposeLog A with(nolock)
                                join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number 
                                left join  condition_category B with(nolock) on A.status_type = B.category 
                                left join tbAccounts C with(nolock) on A.cuser = C.account_code
                                where R.check_number = @check_number {0}", customerFilter);
                Datail_List.DataSource = dbAdapter.getDataTable(cmd);
                Datail_List.DataBind();
            }
        }
        else if (rb2.Checked && check_number_in_order_number.Text != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@check_number", check_number_in_order_number.Text.ToString());
                cmd.CommandText = string.Format(@"select  ROW_NUMBER() OVER(ORDER BY A.cdate desc) AS NO ,
                                Convert(varchar, A.cdate,120) as cdate , B.category , A.dispose_desc,C.user_name , A.status_desc,A.origin
                                from ttDeliveryDisposeLog A with(nolock)
                                join tcDeliveryRequests R with(nolock) on A.check_number = R.check_number 
                                left join  condition_category B with(nolock) on A.status_type = B.category 
                                left join tbAccounts C with(nolock) on A.cuser = C.account_code                            
                                where R.check_number = @check_number {0}", customerFilter);
                Datail_List.DataSource = dbAdapter.getDataTable(cmd);
                Datail_List.DataBind();
            }
        }
    }

    protected void rb_CheckedChanged(Object sender, EventArgs e)
    {
        string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
        if (rb1.Checked)
        {
            check_number.Enabled = true;
            order_number.Enabled = false;
            check_number.Text = "";
            order_number.Text = "";
        }
        if (rb2.Checked)
        {
            check_number.Enabled = false;
            order_number.Enabled = true;
            check_number.Text = "";
            order_number.Text = "";
        }
    }

    private bool IsChineseRegex(string s)
    {
        Regex rx = new Regex("^[\u4e00-\u9fa5]$");
        if (rx.IsMatch(s))
            return true;
        else
            return false;
    }

    protected void UpdateAreaDropDown(object sender, EventArgs e)
    {
        string defaultAreaSql = "SELECT area FROM tbPostCityArea WHERE city = @city";
        using (SqlCommand cmd = new SqlCommand(defaultAreaSql))
        {
            cmd.Parameters.AddWithValue("@city", City.SelectedValue);
            Area.DataSource = dbAdapter.getDataTable(cmd);
            Area.DataTextField = "area";
            Area.DataValueField = "area";
            Area.DataBind();
        }
    }

    protected bool UrlCheck(string strUrl)
    {
        try
        {
            //System.IO.File.Exists( Server.MapPath("")) 
            //System.IO.File.Exists(@"//172..../);
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(strUrl);
            myRequest.Method = "HEAD";
            myRequest.Timeout = 3000;
            HttpWebResponse res = (HttpWebResponse)myRequest.GetResponse();
            return (res.StatusCode == HttpStatusCode.OK);
        }
        catch
        {
            return false;
        }
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    check_address = true;
                    Info = response.Data.Data;
                    areaArriveCode = Info.StationCode;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }

    public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        SpecialAreaManage Info = new SpecialAreaManage();
        var AddressParsingURL = "http://map2.fs-express.com.tw/Address/GetSpecialAreaDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);
        request.Timeout = 5000;
        request.ReadWriteTimeout = 5000;
        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

        var response = client.Post<ResData<SpecialAreaManage>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                Info = response.Data.Data;
                if (Info.id > 0)
                {
                    lbAreaArriveCode.Text = Info.StationCode;
                    lbSpecialAreaId.Text = Info.id.ToString();
                    lbSpecialAreaFee.Text = Info.Fee.ToString();
                }
            }
            catch (Exception e)
            {
                throw e;

            }

        }

        return Info;
    }

    protected void scanItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlCommand cmd1 = new SqlCommand())
        {
            if (scanItem.SelectedValue == "3")
            {
                cmd1.CommandText = "select * from tbItemCodes where  active_flag = 1  and code_sclass='AO' and code_id in ('3','1','2','4','5','6','11','26','21','H')";
            }
            else if (scanItem.SelectedValue == "5")
            {
                cmd1.CommandText = "select * from tbItemCodes where  active_flag = 1  and code_sclass='RO' ";
            }
            states.DataSource = dbAdapter.getDataTable(cmd1);
            states.DataValueField = "code_id";
            states.DataTextField = "code_name";
            states.DataBind();
            states.Items.Insert(0, new ListItem("===請選擇===", ""));

        }
    }

}