﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_PeidaReport.ascx.cs" Inherits="uc_uc_PeidaReport" %>

<asp:HiddenField ID="Hid_dates" runat="server" />
<asp:HiddenField ID="Hid_datee" runat="server" />
<asp:HiddenField ID="Hid_customer_code" runat="server" />
<asp:HiddenField ID="Hid_type" runat="server" />
<table class="table table-striped table-bordered templatemo-user-table">
    <tr class="tr-only-hide">
        <th>NO.</th>
        <th>配送站所</th>
        <th>應配筆數</th>
        <th>配送筆數</th>
        <th>午前配達筆數</th>
        <th>午前配達率</th>
        <th>當日配達筆數</th>
        <th>當日配達率</th>
        <th>已配達筆數</th>
        <th>配達率</th>
        <th>未配達筆數</th>
        <th>異常率</th>
        <th>首筆配達</th>
        <th>末筆配達</th>
    </tr>
    <asp:Repeater ID="New_List" runat="server">
        <ItemTemplate>
            <tr>
                <td data-th="NO."><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%></td>
                <td data-th="配送站所"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_name").ToString())%></td>
                <td data-th="應配筆數">
                    <a id="link1" class=" btn btn-link " href='trace_3_1.aspx?kind=1&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應配筆數","{0:N0}").ToString())%></a></td>
                <td data-th="配送筆數">
                    <a id="link2" class=" btn btn-link " href='trace_3_1.aspx?kind=2&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.配送筆數","{0:N0}").ToString())%></a></td>
                <td data-th="午前配達筆數">
                    <a id="link3" class=" btn btn-link " href='trace_3_1.aspx?kind=3&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.午前配達筆數","{0:N0}").ToString())%></a></td>
                <td data-th="午前配達率">
                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noonRate").ToString())%></td>
                <td data-th="當日配達筆數">
                    <a id="link4" class=" btn btn-link " href='trace_3_1.aspx?kind=4&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.當日配達筆數","{0:N0}").ToString())%></a></td>
                <td data-th="當日配達率">
                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dayRate").ToString())%></td>
                <td data-th="已配達筆數">
                    <a id="link5" class=" btn btn-link " href='trace_3_1.aspx?kind=5&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.已配達筆數","{0:N0}").ToString())%></a></td>
                <td data-th="異常率">
                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.yesRate").ToString())%></td>
                <td data-th="未配達筆數">
                    <a id="link6" class=" btn btn-link " href='trace_3_1.aspx?kind=6&amp;supplier_code=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.area_arrive_code").ToString())%>&amp;dates=<%=Hid_dates.Value%>&amp;datee=<%=Hid_datee.Value%>&amp;customer_code=<%=Hid_customer_code.Value%>&type=<%=Hid_type.Value%>' target="_blank"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noArrive","{0:N0}").ToString())%></a></td>
                <td data-th="異常率"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.noRate").ToString())%></td>
                <td data-th="首筆配達"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MIN").ToString())%></td>
                <td data-th="末筆配達"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.MAX").ToString())%></td>
            </tr>
        </ItemTemplate>
    </asp:Repeater>
    <% if (New_List.Items.Count == 0)
        {%>
    <tr>
        <td colspan="12" style="text-align: center">尚無資料</td>
    </tr>
    <% } %>
</table>

