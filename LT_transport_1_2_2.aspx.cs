﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LT_transport_1_2 : System.Web.UI.Page
{


    public int start_number
    {
        get { return Convert.ToInt32(ViewState["start_number"]); }
        set { ViewState["start_number"] = value; }
    }

    public int end_number
    {
        get { return Convert.ToInt32(ViewState["end_number"]); }
        set { ViewState["end_number"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //Clear_customer();
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            manager_type = Session["manager_type"].ToString(); //管理單位類別   

            master_code = Session["master_code"].ToString();

            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }
        }
    }

    protected void btnselect_click(object sender, EventArgs e)
    {

        if (strkey.Text == "")
        {
            ddlzip.Text = "選擇地址後自動帶出郵遞區號";
            //Clear_customer();
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入託運單號!!');</script>", false);
            return;
        }
        else
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@check_number", strkey.Text);
                cmd.CommandText = @"
                                    select
                                    request_id,   
                                    tcd.customer_code,--客代編號
                                    receive_contact,--原收件人
                                    receive_tel1,   --原收件人電話
                                    receive_city,   --原收件人縣/市
                                    receive_area,   --原收件人鄉/鎮/區
                                    receive_address,--原收件人地址
                                    area_arrive_code, --原到著碼
                                    send_contact,   --原寄件人
                                    case when tcd.send_city = '' then tbc.shipments_city else send_city end send_city ,      --原寄件人縣/市
                                    case when tcd.send_area = '' then tbc.shipments_area else send_area end send_area ,      --原寄件人鄉/鎮/區
                                    send_address,   --原寄件人地址
                                    send_tel,       --原寄件人電話
                                    tcd.supplier_code,   --客代簡碼
                                    B.station_scode,
                                    B.station_name
                                    from tcDeliveryRequests tcd  with(Nolock) 
									left join   tbCustomers tbc  with(Nolock)  on tcd.customer_code = tbc.customer_code
									left join    ttArriveSitesScattered A With(Nolock)  on A.post_city=tcd.receive_city and A.post_area = tcd.receive_area 
                                    left join   tbStation B With(Nolock)  on A.station_code = B.station_scode
                                    where tcd.check_number = @check_number
                                    ";
                dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    lbstation_scode.Text = dt.Rows[0]["station_scode"].ToString().Replace("F", "") + " " + dt.Rows[0]["station_name"].ToString();
                    lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
                    lbreceive_contact.Text = dt.Rows[0]["send_contact"].ToString();
                    lbreceive_tel1.Text = dt.Rows[0]["send_tel"].ToString();
                    lbreceive_address.Text = dt.Rows[0]["send_city"].ToString() + dt.Rows[0]["send_area"].ToString() + dt.Rows[0]["send_address"].ToString();
                    hid_request_id.Value= dt.Rows[0]["request_id"].ToString();
                    strsend_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                    strsend_tel.Text = dt.Rows[0]["receive_tel1"].ToString();
                    DataTable Cdt = new DataTable();
                    DataTable Adt = new DataTable();
                    using (SqlCommand Ccmd = new SqlCommand())
                    {
                        Ccmd.Parameters.AddWithValue("@city", dt.Rows[0]["receive_city"].ToString());
                        Ccmd.CommandText = "select * from tbPostCity where city = @city order by seq asc ";
                        Cdt = dbAdapter.getDataTable(Ccmd);
                    }
                    string tab_cit = "";

                    if (Cdt.Rows.Count <= 0)
                    {
                        ddlsend_city.Text = "";
                    }
                    else
                    {
                        ddlsend_city.Text = Cdt.Rows[0]["city"].ToString();
                    }

                    if (ddlsend_city.Text != "")
                    {
                        using (SqlCommand Ccmd = new SqlCommand())
                        {
                            Ccmd.Parameters.AddWithValue("@city", ddlsend_city.Text);
                            Ccmd.Parameters.AddWithValue("@area", dt.Rows[0]["receive_area"].ToString());
                            Ccmd.CommandText = "select * from tbPostCityArea where city = @city and area=@area ";
                            Adt = dbAdapter.getDataTable(Ccmd);
                        }
                    }
                    if (Adt.Rows.Count <= 0)
                    {
                        ddlsend_area.Text = "";
                    }
                    else
                    {

                        ddlsend_area.Text = dt.Rows[0]["receive_area"].ToString();
                    }

                    strsend_address.Text = dt.Rows[0]["receive_address"].ToString();
                    hid_receive_city.Value = dt.Rows[0]["send_city"].ToString();
                    hid_receive_area.Value = dt.Rows[0]["send_area"].ToString();
                    hid_receive_address.Value = dt.Rows[0]["send_address"].ToString();
                }
            }

            using (SqlCommand cmd = new SqlCommand())
            {
                DataTable dt = new DataTable();
                cmd.Parameters.AddWithValue("@post_city", ddlsend_city.Text);
                cmd.Parameters.AddWithValue("@post_area", ddlsend_area.Text);
                cmd.CommandText = @"
Select zip, post_area ,A.station_code,B.station_name  from ttArriveSitesScattered A With(Nolock)  
inner join tbStation B With(Nolock)  on A.station_code = B.station_scode
where A.post_city=@post_city and A.post_area = @post_area 
order by seq asc";
                dt = dbAdapter.getDataTable(cmd);
                if (dt.Rows.Count > 0)
                {
                    send_station_scode.Text = dt.Rows[0]["station_name"].ToString();
                }
            }
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        int request_id = 0;
        string insert_check_number = "";
        string check_number = "";
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        string ErrStr = "";

        string customer_code = "";

        customer_code = lbcustomer_code.Text;


        string supplier_code = "";
        string supplier_name = "";

        Regex reg = new Regex("^[A-Za-z0-9]+$");
        if (strorder_number.Text != "")
        {
            if (!reg.IsMatch(strorder_number.Text))
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('訂單編號不可輸入英文、數字以外的值!!');</script>", false);
                return;
            }
        }

        if (strkey.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請輸入訂單編號!!');</script>", false);
            return;
        }

        //預購袋超值箱不能建退貨單
        //string prefix_check_number = strkey.Text.Substring(0, 3);

        //if (prefix_check_number == "880")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('預購袋帳號無法建立退貨單號');</script>", false);
        //    return;
        //}
        //else if (prefix_check_number == "550")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('超值箱帳號無法建立退貨單號');</script>", false);
        //    return;
        //}


        if (Func.IsNormalDelivery(strkey.Text))
        {
            ErrStr = "此筆已正常配達，無法生成拒收單號";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('失敗:" + ErrStr + "');</script>", false);
            return;
        }

        if (Func.IsCancelled(strkey.Text))
        {
            ErrStr = "此筆已經銷單，無法生成拒收單號";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('失敗:" + ErrStr + "');</script>", false);
            return;
        }
        if(!(strkey.Text).ToUpper().StartsWith("TH"))
        {
            if ((!Func.IsRefused(strkey.Text)))
            {
                ErrStr = "此筆尚未拒收，無法生成拒收單號";
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('失敗:" + ErrStr + "');</script>", false);
                return;
            }
        }
        


        //確認單號還沒有建立過逆物流
        DataTable dtReturn = new DataTable();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_number", strkey.Text);
            cmd.CommandText = @"select check_number                                     
                            from tcDeliveryRequests with(nolock) where return_check_number = @check_number
                            ";
            dtReturn = dbAdapter.getDataTable(cmd);
        }

        if (dtReturn.Rows.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('此單號已建立逆物流單號，不可再重複建立!!');</script>", false);
            return;
        }

        DataTable dt = new DataTable();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@request_id", hid_request_id.Value);
            cmd.Parameters.AddWithValue("@check_number", strkey.Text);
            cmd.CommandText = @"select 
                            *
                            from tcDeliveryRequests with(nolock) where request_id = @request_id
                            ";
            dt = dbAdapter.getDataTable(cmd);
        }

        try
        {
            #region 新增    
            using (SqlCommand cmd = new SqlCommand())
            {//NEXT VALUE FOR DBO.MYSEQ


                cmd.CommandText = @"INSERT INTO tcDeliveryRequests
                            (
  Less_than_truckload,
  CbmSize,
  pricing_type,
  customer_code,
  check_type,
  receive_customer_code,
  subpoena_category,
  receive_tel1,
  receive_contact,
  receive_city,
  receive_area,
  receive_address,
  remote_fee,
  area_arrive_code,
  receive_by_arrive_site_flag,
  pieces,
  plates,
  cbm,
  arrive_to_pay_freight,
  arrive_to_pay_append,
  collection_money,
  send_contact,
  send_tel,
  send_city,
  send_area,
  send_address,
send_station_scode,
  donate_invoice_flag,
  electronic_invoice_flag,
  invoice_memo,
  invoice_desc,
  time_period,
  receipt_flag,
  pallet_recycling_flag,
  add_transfer,
  sub_check_number,
  supplier_code,
  supplier_name,
  cuser,
  cdate,
  uuser,
  udate,
  print_date,
  print_flag,
  DeliveryType,
check_number,
order_number,
return_check_number,
ProductId,
SpecCodeId
) 
                            VALUES (
 @Less_than_truckload,
  @CbmSize,
  @pricing_type,
  @customer_code,
  @check_type,
  @receive_customer_code,
  @subpoena_category,
  @receive_tel1,
  @receive_contact,
  @receive_city,
  @receive_area,
  @receive_address,
  @remote_fee,
  @area_arrive_code,
  @receive_by_arrive_site_flag,
  @pieces,
  @plates,
  @cbm,
  @arrive_to_pay_freight,
  @arrive_to_pay_append,
  @collection_money,
  @send_contact,
  @send_tel,
  @send_city,
  @send_area,
  @send_address,
@send_station_scode,
  @donate_invoice_flag,
  @electronic_invoice_flag,
  @invoice_memo,
  @invoice_desc,
  @time_period,
  @receipt_flag,
  @pallet_recycling_flag,
  @add_transfer,
  @sub_check_number,
  @supplier_code,
  @supplier_name,
  @cuser,
  @cdate,
  @uuser,
  @udate,
  @print_date,
  @print_flag,
  @DeliveryType,
   NEXT VALUE FOR denied_return_check_number ,
  @order_number,
  @return_check_number,
  @ProductId,
  @SpecCodeId
) ;
SELECT SCOPE_IDENTITY()
";

                var subpoena_code = "";

                if (Session["account_code"].ToString() != null && Session["account_code"].ToString().Length == 11 && Session["account_code"].ToString().StartsWith("F"))
                {
                    subpoena_code = "21"; //客戶自己建檔的話傳票類別為到付
                }
                else
                {
                    subpoena_code = "11"; //站所人員建檔的話傳票類別為元付
                }

                if (dt.Rows.Count > 0)
                {
                    cmd.Parameters.AddWithValue("@CbmSize", dt.Rows[0]["CbmSize"].ToString());                    //材積大小
                    cmd.Parameters.AddWithValue("@customer_code", dt.Rows[0]["customer_code"].ToString());        //客代編號
                    cmd.Parameters.AddWithValue("@check_type", dt.Rows[0]["check_type"].ToString());              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", dt.Rows[0]["receive_customer_code"].ToString());            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_code);
                    cmd.Parameters.AddWithValue("@remote_fee", dt.Rows[0]["remote_fee"].ToString());              //偏遠區加價
                    cmd.Parameters.AddWithValue("@pieces", dt.Rows[0]["pieces"].ToString());                                             //件數
                    cmd.Parameters.AddWithValue("@plates", dt.Rows[0]["plates"].ToString());                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", dt.Rows[0]["cbm"].ToString());                                                   //才數
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", dt.Rows[0]["arrive_to_pay_freight"].ToString());                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", dt.Rows[0]["arrive_to_pay_append"].ToString());              //到付追加
                    cmd.Parameters.AddWithValue("@collection_money", 0);                  //代收金
                    cmd.Parameters.AddWithValue("@time_period", dt.Rows[0]["time_period"].ToString());            //時段
                    cmd.Parameters.AddWithValue("@ProductId", dt.Rows[0]["ProductId"].ToString());      //產品名
                    cmd.Parameters.AddWithValue("@SpecCodeId", dt.Rows[0]["SpecCodeId"].ToString());  //規格

                    //                    using (SqlCommand cmd2 = new SqlCommand())
                    //                    {
                    //                        DataTable dt2 = new DataTable();
                    //                        cmd2.Parameters.AddWithValue("@request_id", hid_request_id.Value);
                    //                        cmd2.Parameters.AddWithValue("@customer_code", lbcustomer_code.Text);
                    //                        cmd2.Parameters.AddWithValue("@post_city", hid_receive_city.Value);
                    //                        cmd2.Parameters.AddWithValue("@post_area", hid_receive_area.Value);
                    //                        cmd2.Parameters.AddWithValue("@check_number", strkey.Text);
                    //                        //是否有勾選依照寄件地址退貨
                    //                        cmd2.CommandText = @"select return_address_flag,station_code,* from tcDeliveryRequests R1
                    //left join ttArriveSitesScattered A1 on send_city=post_city and send_area=post_area
                    //left join tbCustomers C1 on C1.customer_code=R1.customer_code
                    //where request_id=@request_id";
                    //                        dt2 = dbAdapter.getDataTable(cmd2);

                    //                        if (dt2.Rows.Count > 0)
                    //                        {
                    //                            string return_address_flag = dt2.Rows[0]["return_address_flag"].ToString();
                    //                            string station_code = dt2.Rows[0]["station_code"].ToString();
                    //                            if (return_address_flag != "True")
                    //                            {
                    //                                cmd.Parameters.AddWithValue("@area_arrive_code", dt.Rows[0]["supplier_code"].ToString().Replace("F", "")); //到著碼
                    //                            }
                    //                            else
                    //                            {


                    //                                cmd.Parameters.AddWithValue("@area_arrive_code", station_code);
                    //                            }

                    //                        }
                    //                    }

                    //調整為解析寄件地址後之站所為到著站  --鈺蘋
                    var receive_info = GetAddressParsing(hid_receive_city.Value.ToString().Trim() + hid_receive_area.Value.ToString().Trim() + hid_receive_address.Value.ToString().Trim(), customer_code.Trim(), "1", "1");

                    if (receive_info == null)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請檢查地址是否正確');</script>", false);
                        return;
                    }
                    else 
                    {
                        cmd.Parameters.AddWithValue("@area_arrive_code", receive_info.StationCode);
                    }

                }
                else
                {
                    cmd.Parameters.AddWithValue("@CbmSize", 0);                    //材積大小
                    cmd.Parameters.AddWithValue("@customer_code", customer_code);                     //客代編號
                    cmd.Parameters.AddWithValue("@check_type", "001");              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", "");            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_code);
                    cmd.Parameters.AddWithValue("@remote_fee", 0);              //偏遠區加價
                    cmd.Parameters.AddWithValue("@pieces", 1);                                             //件數
                    cmd.Parameters.AddWithValue("@plates", 0);                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", 0);                                                   //才數
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", 0);                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", 0);              //到付追加
                    cmd.Parameters.AddWithValue("@collection_money", 0);                  //代收金
                    cmd.Parameters.AddWithValue("@time_period", "午");            //時段
                    cmd.Parameters.AddWithValue("@area_arrive_code", customer_code.Substring(0, 3).Replace("F", ""));
                }

                cmd.Parameters.AddWithValue("@Less_than_truckload", 1);                                       //0：棧板運輸  1：零擔運輸
                cmd.Parameters.AddWithValue("@pricing_type", "02");          //計價模式 (01:論板、02:論件、03論才、04論小板)
                cmd.Parameters.AddWithValue("@receive_tel1", lbreceive_tel1.Text);                              //電話1
                cmd.Parameters.AddWithValue("@receive_contact", lbreceive_contact.Text);                        //收件人    
                cmd.Parameters.AddWithValue("@receive_city", hid_receive_city.Value);                           //收件地址-縣市
                cmd.Parameters.AddWithValue("@receive_area", hid_receive_area.Value);                           //收件地址-鄉鎮市區
                cmd.Parameters.AddWithValue("@receive_address", hid_receive_address.Value);                     //收件地址-路街巷弄號

                DataTable dts = new DataTable();
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.AddWithValue("@post_city", hid_receive_city.Value);
                    cmd1.Parameters.AddWithValue("@post_area", hid_receive_area.Value);
                    cmd1.CommandText = "select station_code from ttArriveSitesScattered where post_city= @post_city and post_area = @post_area";
                    dts = dbAdapter.getDataTable(cmd1);
                }


                cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", 0);                                       //到站領貨 0:否、1:是

                cmd.Parameters.AddWithValue("@arrive_address", hid_receive_address.Value);                            //到著碼地址


                cmd.Parameters.AddWithValue("@send_contact", strsend_contact.Text);                                       //寄件人
                cmd.Parameters.AddWithValue("@send_tel", strsend_tel.Text.ToString());                           //寄件人電話
                cmd.Parameters.AddWithValue("@send_city", ddlsend_city.Text.ToString());                //寄件人地址-縣市
                cmd.Parameters.AddWithValue("@send_area", ddlsend_area.Text.ToString());                //寄件人地址-鄉鎮市區

                string sendAddress = strsend_address.Text.Length > 50 ? strsend_address.Text.Substring(0, 50) : strsend_address.Text;
                cmd.Parameters.AddWithValue("@send_address", sendAddress);          //寄件人地址-號街巷弄號
                using (SqlCommand cmd1 = new SqlCommand())
                {
                    cmd1.Parameters.AddWithValue("@request_id", hid_request_id.Value);
                    cmd1.CommandText = "select area_arrive_code from tcDeliveryRequests where request_id= @request_id";
                    dts = dbAdapter.getDataTable(cmd1);
                    if (dts.Rows.Count > 0)
                    {
                        cmd.Parameters.AddWithValue("@send_station_scode", dts.Rows[0]["area_arrive_code"].ToString());
                    }
                    else { cmd.Parameters.AddWithValue("@send_station_scode", lbstation_scode.Text.Trim().Substring(0, 2)); }

                }
                cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.Value);                             //收回品項
                cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //(備註說明)


                cmd.Parameters.AddWithValue("@receipt_flag", 0);                           //是否回單
                cmd.Parameters.AddWithValue("@pallet_recycling_flag", 0);         //是否棧板回收

                cmd.Parameters.AddWithValue("@add_transfer", 0);                                              //是否轉址
                cmd.Parameters.AddWithValue("@sub_check_number", "000");                                      //次貨號
                using (SqlCommand cmdp = new SqlCommand())
                {
                    cmdp.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
                                      " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                                      " where c.customer_code = '" + customer_code + "' ";
                    using (DataTable dtss = dbAdapter.getDataTable(cmdp))
                    {
                        if (dtss.Rows.Count > 0)
                        {
                            supplier_code = dtss.Rows[0]["supplier_code"].ToString().Trim();        //區配
                            supplier_name = dtss.Rows[0]["supplier_name"].ToString().Trim();        // 區配名稱
                        }
                    }
                }


                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);                                 //配送商代碼
                cmd.Parameters.AddWithValue("@supplier_name", supplier_name);                                 //配送商名稱
                cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間
                cmd.Parameters.AddWithValue("@print_date", DBNull.Value);                                     //出貨日期
                cmd.Parameters.AddWithValue("@print_flag", 0);                                                //是否列印
                cmd.Parameters.AddWithValue("@return_check_number", strkey.Text);

                check_number = strkey.Text;

                cmd.Parameters.AddWithValue("@DeliveryType", "R");



                //cmd.Parameters.AddWithValue("@check_number", strcheck_number.Text);                         //託運單號(貨號) 現階段長度10~20碼
                cmd.Parameters.AddWithValue("@order_number", strorder_number.Text);                           //訂單號碼(退貨單號)
                                                                                                              //cmd.Parameters.AddWithValue("@check_number", "NEXT VALUE FOR ttAutoNumeral");

                //cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                {
                    using (SqlCommand cmdch = new SqlCommand())
                    {
                        cmdch.Parameters.AddWithValue("@request_id", request_id);
                        cmdch.CommandText = @"select check_number from tcDeliveryRequests with(nolock) where request_id = @request_id";
                        using (DataTable dt3 = dbAdapter.getDataTable(cmdch))
                        {
                            insert_check_number = dt3.Rows[0][0].ToString();
                        }
                    }
                }
                //寫入CBMDetailLog
                using (SqlCommand cmdcbm = new SqlCommand())
                {
                    cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                    cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);

                    using (SqlCommand cmds = new SqlCommand())
                    {
                        cmds.CommandText = string.Format(@"Select check_number,SpecCodeId From tcDeliveryRequests with(nolock) where check_number ='" + insert_check_number + "'");
                        DataTable dtc = dbAdapter.getDataTable(cmds);
                        if (dtc != null && dtc.Rows.Count > 0)
                        {
                            string CBM = dtc.Rows[0]["SpecCodeId"].ToString();
                            if (CBM == "")
                            {
                                using (SqlCommand cmdc = new SqlCommand())
                                {
                                    cmdc.CommandText = "select product_type from tbCustomers where customer_code = '" + lbcustomer_code.Text.ToString().Trim() + "' ";
                                    using (DataTable dtp = dbAdapter.getDataTable(cmdc))
                                        if (dtp.Rows.Count > 0)
                                        {
                                            string product_type = dtp.Rows[0]["product_type"].ToString().Trim();
                                            if (product_type == "1")  //一般件 沒有的話選 S090
                                            {
                                                CBM = "S090";
                                            }
                                            else if (product_type == "2")  //預購袋 沒有的話選 B003
                                            {
                                                CBM = "B003";
                                            }
                                            else if (product_type == "3")  //超值箱 沒有的話選 X003
                                            {
                                                CBM = "X003";
                                            }
                                            else if (product_type == "4" || product_type == "5")  //商城、內部文件 沒有的話選 N001
                                            {
                                                CBM = "N001";
                                            }
                                            else { CBM = "S090"; }
                                        }
                                }
                            }
                            cmdcbm.Parameters.AddWithValue("@CBM", dtc.Rows[0]["SpecCodeId"].ToString()); 
                        }
                    }
                    cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                    cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                    cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                    dbAdapter.execNonQuery(cmdcbm);
                }
            }

            #endregion
        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            ErrStr += ex.ToString();
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "零擔託運單" + Request.RawUrl + strErr + ": " + ex.ToString();

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }
        if (ErrStr == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成 託運單號為：" + insert_check_number + "  !!');</script>", false);
            strsend_contact.Text = "";          // 名稱
            strsend_tel.Text = "";              // 電話
            strkey.Text = "";

            strorder_number.Text = "";
            invoice_memo.Value = "";
            invoice_desc.Value = "";

            hid_receive_city.Value = "";
            hid_receive_area.Value = "";
            hid_receive_address.Value = "";

            //ddlzip.SelectedIndex = -1;          // 郵遞區號
            ddlzip.Text = "";
            ddlsend_city.Text = "";    // 出貨地址-縣市
            ddlsend_area.Text = "";    // 出貨地址-鄉鎮市區
            strsend_address.Text = "";       //出貨地址-路街巷弄號

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dtRequests = new DataTable();
            cmd.CommandText = @"update tcDeliveryRequests set return_check_number = @return_check_number where check_number = @check_number";
            cmd.Parameters.AddWithValue("@check_number", check_number);
            cmd.Parameters.AddWithValue("@return_check_number", insert_check_number);
            dtRequests = dbAdapter.getDataTable(cmd);
            if (dtRequests.Rows.Count > 0)
            {
                send_station_scode.Text = dtRequests.Rows[0]["station_name"].ToString();
            }
        }

        string driverCode = string.Empty;
        using (SqlCommand sql = new SqlCommand())
        {
            DataTable tbRequest = new DataTable();
            sql.CommandText = string.Format(
            @"select driver_code
                from
                (
                    select  ROW_NUMBER()over(partition by check_number order by cdate desc)　as  ROW_NUMBER　,*
                    from ttDeliveryScanLog
                    where check_number = '{0}'
                ) k
             where k.ROW_NUMBER = 1", check_number);
            tbRequest = dbAdapter.getDataTable(sql);
            if (tbRequest.Rows.Count > 0)
            {
                driverCode = tbRequest.Rows[0]["driver_code"].ToString();
            }
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            DataTable dtRequests = new DataTable();
            cmd.CommandText = @"insert into ttDeliveryScanLog
(driver_code,
check_number,
scan_item,
scan_date,
pieces,
arrive_option,
cdate
)
values
(@driver_code,
@check_number,
@scan_item,
@scan_date,
@pieces,
@arrive_option,
@cdate)";
            cmd.Parameters.AddWithValue("@driver_code", driverCode);
            cmd.Parameters.AddWithValue("@check_number", check_number);
            cmd.Parameters.AddWithValue("@scan_item", "3");
            cmd.Parameters.AddWithValue("@scan_date", DateTime.Now);
            cmd.Parameters.AddWithValue("@pieces", "1");
            cmd.Parameters.AddWithValue("@arrive_option", "49");
            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
            cmd.Parameters.AddWithValue("@car_number", insert_check_number);     //紀錄當下產生的拒收單號
            cmd.CommandText = dbAdapter.genInsertComm("ttDeliveryScanLog", false, cmd);
            dbAdapter.execNonQuery(cmd);
        }
    }
    protected void btnRomv_Click(object sender, EventArgs e)
    {
        Clear_customer();
    }

    protected void Clear_customer()
    {
        strsend_contact.Text = "";          // 名稱
        strsend_tel.Text = "";              // 電話
        strkey.Text = "";

        strorder_number.Text = "";
        invoice_memo.Value = "";
        invoice_desc.Value = "";

        hid_receive_city.Value = "";
        hid_receive_area.Value = "";
        hid_receive_address.Value = "";

        //ddlzip.SelectedIndex = -1;          // 郵遞區號
        ddlzip.Text = "選擇地址後自動帶出";
        ddlsend_city.Text = "";    // 出貨地址-縣市
        ddlsend_area.Text = "";    // 出貨地址-鄉鎮市區
        strsend_address.Text = "";       //出貨地址-路街巷弄號

        lbcustomer_code.Text = "";
        lbstation_scode.Text = "";    // 出貨地址-縣市
        lbreceive_contact.Text = "";          // 名稱
        lbreceive_tel1.Text = "";              // 電話
        lbreceive_address.Text = "";          // 郵遞區號

    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }

        return Info;
    }

}