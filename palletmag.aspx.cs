﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class palletmag : System.Web.UI.Page
{
   

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
                ssMaster_code = "0000000";
            #region 
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = "";
                if (ssManager_type == "4" || ssManager_type == "5" )
                {
                    if (ssMaster_code.Substring(3, 4) == "0000")
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code.Substring(0, 3));
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@master_code", ssMaster_code);
                        wherestr = " and master_code like '%'+@master_code+'%'";
                    }
                }
                    
                cmd.CommandText = string.Format(@"WITH cus AS
                                            (
                                               SELECT *,
                                                     ROW_NUMBER() OVER (PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                               FROM tbCustomers where 1=1 and stop_shipping_code = 0 {0}
                                            )
                                            SELECT supplier_id, master_code, customer_name , master_code + '-' +  customer_name as showname
                                            FROM cus
                                            WHERE rn = 1 order by  master_code", wherestr);
                second_code.DataSource = dbAdapter.getDataTable(cmd);
                second_code.DataValueField = "master_code";
                second_code.DataTextField = "showname";
                second_code.DataBind();
            }
            second_code.SelectedValue = ssMaster_code;
            
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2")
            {
                second_code.Enabled = true;
                second_code.Items.Insert(0, new ListItem("全部", ""));
            }
               
            #endregion


            
            
            if (Request.QueryString["second_code"] != null)
            {
                second_code.SelectedValue  = Request.QueryString["second_code"];
            }

            readdata();
        }
    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        
        if (second_code.SelectedValue != "")
        {
            strWhereCmd += " and A.customer_code like '" + second_code.SelectedValue + "%' ";
            querystring += "&second_code=" + second_code.SelectedValue;
        }

        #endregion


        cmd.CommandText = string.Format(@"SELECT request_id, sum(return_cnt) as cnt
                                            into #tbreturn
                                            from  ttPlletReturn A with(nolock)
                                            group by request_id
                                          SELECT A.area_arrive_code +' '+ B.supplier_name as showname,A.pricing_type,A.request_id
                                                ,ROW_NUMBER() OVER(ORDER BY A.print_date, A.check_number,CONVERT(INT,A.sub_check_number) DESC) AS ROWID
                                                ,CONVERT(CHAR(10),A.supplier_date,111) supplier_date, A.check_number , LEFT( A.customer_code,3) as sendstation , A.receive_contact , 
                                                    CASE WHEN A.supplier_code IN('001','002') THEN REPLACE(REPLACE(C.customer_shortname,'所',''),'HCT','') ELSE A.supplier_name END as sendstation2,
                                                    A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area ,
                                                    CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                                     A.plates ,  A.Pallet_type, D.code_name  as Pallet_type_text, ISNULL(R.cnt ,0)  as return_cnt, A.plates-ISNULL(R.cnt ,0)  as unreturn_cnt,
                                                    A.customer_code , C.customer_shortname ,
                                                    A.send_contact,A.send_city , A.send_area , A.send_address , A.add_transfer
                                            from tcDeliveryRequests A With(Nolock)
                                            left join tbCustomers C With(Nolock) on C.customer_code = A.customer_code 
                                            left join tbItemCodes D with(nolock) on A.Pallet_type = D.code_id and code_bclass  = '2' and code_sclass  = 'S8'
                                            left join #tbreturn R with(nolock) on A.request_id  = R.request_id 
                                            left join tbSuppliers B on A.supplier_code  = B.supplier_code
                                            where 1= 1 {0} and pallet_recycling_flag = 1 and cancel_date  is NULL  and plates >0 
                                            order by A.print_date, A.check_number", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

   

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "cmdReturn":
                string ErrStr = string.Empty;
                string request_id = e.CommandArgument.ToString();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@request_id", request_id);
                    int return_cnt = Convert.ToInt32 (((TextBox)e.Item.FindControl("tbReturnCnt")).Text.ToString().Trim());
                    int plates = Convert.ToInt32(((Literal)e.Item.FindControl("Liplates")).Text.ToString());
                    if (return_cnt > plates)
                    {
                        return_cnt = plates;
                    }
                    cmd.Parameters.AddWithValue("@return_cnt", return_cnt);
                    cmd.Parameters.AddWithValue("@return_date", DateTime.Now);
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                    cmd.CommandText = dbAdapter.SQLdosomething("ttPlletReturn", cmd, "insert");
                    try
                    {
                        dbAdapter.execNonQuery(cmd);
                    }
                    catch (Exception ex)
                    {
                        ErrStr = ex.Message;
                    }
                    if (ErrStr == "")
                    {
                        paramlocation();
                    }
                }
                break;
        }
    }


    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
       
        if (second_code.SelectedValue != "")
        {
            querystring += "&second_code=" + second_code.SelectedValue;
        }
        Response.Redirect(ResolveUrl("~/palletmag.aspx?search=yes" + querystring));
    }

    protected void Allbtselect_Click(object sender, EventArgs e)
    {
        string ErrStr = string.Empty;
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                if ((chkRow != null) && (chkRow.Checked))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    int return_cnt = Convert.ToInt32(((TextBox)New_List.Items[i].FindControl("tbReturnCnt")).Text.ToString().Trim());
                    int plates = Convert.ToInt32(((Literal)New_List.Items[i].FindControl("Liplates")).Text.ToString());
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@request_id", id);
                        if (return_cnt > plates)
                        {
                            return_cnt = plates;
                        }
                        cmd.Parameters.AddWithValue("@return_cnt", return_cnt);
                        cmd.Parameters.AddWithValue("@return_date", DateTime.Now);
                        cmd.Parameters.AddWithValue("@udate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);
                        cmd.CommandText = dbAdapter.SQLdosomething("ttPlletReturn", cmd, "insert");
                        try
                        {
                            dbAdapter.execNonQuery(cmd);
                        }
                        catch (Exception ex)
                        {
                            ErrStr = ex.Message;
                        }
                        
                    }
                }

            }
            if (ErrStr == "")
            {
                paramlocation();
            }
        }
    }


        static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    

    
}