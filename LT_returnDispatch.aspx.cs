﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LT_returnDispatch : System.Web.UI.Page
{

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string Master_code
    {
        // for權限
        get { return ViewState["Master_code"].ToString(); }
        set { ViewState["Master_code"] = value; }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            cdate1.Text = DateTime.Now.ToString("yyy/MM/01");
            cdate2.Text = DateTime.Today.ToString("yyyy/MM/dd");
            lbDate.Text = "建檔日期";
            cdate1.Visible = true;
            cdate2.Visible = true;

            SetDefault();

            SetViewData();
        }
    }

    /// <summary>始初資料綁定 </summary>
    protected void SetDefault() {
        //站所別
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"select station_code,'('+station_code+')' + station_name as station_name from tbStation";
            ddlStation.DataSource = dbAdapter.getDataTable(cmd);
            ddlStation.DataValueField = "station_code";
            ddlStation.DataTextField = "station_name";
            ddlStation.DataBind();
        }
    }

    /// <summary>查詢資料顯示 </summary>
    protected void SetViewData()
    {
        DateTime dt_s = new DateTime();
        DateTime dt_e = new DateTime();

        #region Set QueryString

        if (Request.QueryString["supplyer"] != null
            && Request.QueryString["dateS"] != null
            && Request.QueryString["dateE"] != null)
        {
            ddlStation.SelectedValue = Request.QueryString["supplyer"];

            if (DateTime.TryParse(Request.QueryString["dateS"], out dt_s)
                && DateTime.TryParse(Request.QueryString["dateE"], out dt_e)
                && dt_e >= dt_s)
            {
                cdate1.Text = dt_s.ToString("yyy/MM/dd");
                cdate2.Text = dt_e.ToString("yyy/MM/dd");
            }
        }
        else
        {
            cdate1.Text = DateTime.Now.ToString("yyy/MM/01");
            cdate2.Text = DateTime.Now.ToString("yyy/MM/dd");
        }

        if (Request.QueryString["keyword"] != null)
        {
            tb_search.Text = Request.QueryString["keyword"].ToString();
        }
        if (Request.QueryString["ddlType"] != null)
        {
            ddlType.SelectedValue = Request.QueryString["ddlType"].ToString();
        }
        #endregion

        //if (ddl_supplyer.SelectedValue == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送廠商!');</script>", false);
        //    return;
        //}

        if (!DateTime.TryParse(cdate1.Text, out dt_s) || !DateTime.TryParse(cdate2.Text, out dt_e))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇配送期間!');</script>", false);
            return;
        }
        if (dt_s > dt_e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請確認配送期間是否正確!');</script>", false);
            return;
        }

        String str_Query = "&supplyer=" + ddlStation.SelectedValue
                         + "&dateS=" + cdate1.Text
                         + "&dateE=" + cdate2.Text
                         + "&keyword=" + tb_search.Text
                         + "&ddlType=" + ddlType.SelectedValue;

        ListData _search = new ListData();
        _search.type = 1;
        _search.arrive = ddlStation.SelectedValue;
        _search.date_start = dt_s;
        _search.date_end = dt_e;
        _search.rbpricingtype = "02";
        if (!string.IsNullOrEmpty(tb_search.Text)) _search.keyword = tb_search.Text;
        DataTable dt = GetDataTableForList(_search);
        if (dt != null && dt.Rows.Count > 0)
        {
            //DataRow row = dt.NewRow();
            //row["send_contact"] = string.Format(@"筆數:{0}", dt.Rows.Count);
            rowcount.Text = string.Format(@"筆數:{0}", dt.Rows.Count);
            rowcount.Visible = true;
            //dt.Rows.Add(row);
        }
        //using ()
        //{
           

        //    //New_List.DataSource = dt;
        //    //New_List.DataBind();
        //}

        PagedDataSource objPds = new PagedDataSource();

        objPds.DataSource = dt.DefaultView;
        objPds.AllowPaging = true;

        objPds.PageSize = 10;

        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + str_Query));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + str_Query));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + str_Query));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + str_Query));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();

    }
    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e) {
        string request_id = ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim();

        string script = string.Empty;
        switch (e.CommandName) {
            case "cmdEdit":
                FillData(request_id);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_01').modal();" + script + "</script>", false);
                break;
        }
    }

    protected void FillData(string request_id)
    {
        //DataSet();
        try
        {
            DataTable dt = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@request_id", request_id);

                cmd.CommandText = string.Format(@"
select A1.request_id 
,A1.customer_code							--客戶代號
,A1.area_arrive_code						--站所代碼
,case when A2.Receive_contact is not null then A2.check_number else A1.check_number end check_number					--貨號
,isnull(A2.Receive_contact,A1.Send_contact) Send_contact	--退貨人
,case when A2.Receive_contact is not null then isnull(A2.receive_tel1,'') else isnull(A1.send_tel,'') end send_tel		            --退貨人電話
,A1.Send_city								--退貨人(縣/市)
,A1.Send_area								--退貨人(區)
,A1.Send_address							--退貨人(地址)
,isnull(A4.zip,A3.zip)	zip					--退貨人郵遞區號
,A1.receive_contact							--收貨人
,case when A2.Receive_contact is not null then ISNULL(A2.Receive_city,'') else A1.receive_city	end	receive_city					--收貨人(縣/市)
,case when A2.Receive_contact is not null then ISNULL(A2.receive_area,'') else A1.receive_area	end receive_area					--收貨人(區)
,case when A2.Receive_contact is not null then ISNULL(A2.receive_address,'')else A1.receive_address	end	receive_address				--收貨人(地址)
,A1.invoice_desc							--備註
from tcDeliveryRequests A1 With(Nolock)
Left join tcDeliveryRequests A2 With(Nolock) on A1.send_contact = A2.check_number
left join [ttArriveSites] A3 With(Nolock) on A3.post_city = A1.send_city and A3.post_area = A1.send_area
left join [ttArriveSites] A4 With(Nolock) on A3.post_city = A2.send_city and A3.post_area = A2.send_area
where A1.request_id = @request_id");
                dt = dbAdapter.getDataTable(cmd);
            }

            if (dt.Rows.Count > 0)
            {
                tbreceivecontact.Text = dt.Rows[0]["receive_contact"].ToString();
                lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();          
                lbarea_arrive_code.Text = dt.Rows[0]["area_arrive_code"].ToString();
                lbcheck_number.Text = dt.Rows[0]["check_number"].ToString();
                hid_request_id.Value = dt.Rows[0]["request_id"].ToString();
                lbSendcontact.Text = dt.Rows[0]["Send_contact"].ToString();             //退貨人
                lbsendtel.Text = dt.Rows[0]["send_tel"].ToString();                     //退貨人
                lbsendaddr.Text = dt.Rows[0]["receive_address"].ToString();//dt.Rows[0]["send_addr"].ToString();
                tbzip.Text = dt.Rows[0]["zip"].ToString();
                tbreceive_city.Text = dt.Rows[0]["receive_city"].ToString();
                tbreceive_area.Text = dt.Rows[0]["receive_area"].ToString();
                tbreceive_address.Text = dt.Rows[0]["receive_address"].ToString();
                tbinvoice_desc.Text = dt.Rows[0]["invoice_desc"].ToString();
            }
        }
        catch (Exception ex)
        {
            //_fun.Log(string.Format("【{0} Error】DataBind:{1}", "乘客帳號管理", ex.Message), "W");
        }
    }

    protected void search_Click(object sender, EventArgs e)
    {
        String str_Query = "supplyer=" + ddlStation.SelectedValue
                          + "&dateS=" + cdate1.Text
                          + "&dateE=" + cdate2.Text
                          + "&keyword=" + tb_search.Text
                          + "&ddlType=" + ddlType.SelectedValue;
        if (!string.IsNullOrEmpty(check_number.Text))
        {
            str_Query += "&keyword=" + check_number.Text;
        }

        Response.Redirect(ResolveUrl("~/LT_returnDispatch.aspx?" + str_Query));
    }

    protected void btn_OK_Click(object sender, EventArgs e) {

    }


    /// <summary>依類型取得標籤列印DataTable</summary>
    /// <param name="search"></param>
    /// <returns></returns>
    protected DataTable GetDataTableForList(ListData search)
    {
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                String strSQL = "";
                string wherestr = "";
                if (ddlType.SelectedValue == "") {
                    wherestr = "";
                } else if (ddlType.SelectedValue == "0") {
                    wherestr = "and dr.invoice_desc  not like '%來回件%'";
                }
                else {
                    wherestr = "and dr.invoice_desc  like '%來回件%'";
                }

              
                        #region 網頁list顯示

                        strSQL = string.Format(@"
SELECT ROW_NUMBER() OVER(ORDER BY dr.cdate) AS rowid
,dr.request_id
,CONVERT(CHAR(10), dr.cdate,111) cdate --建檔日期,
,CONVERT(CHAR(10), dr.print_date,111) print_date--取件日期
,dr.check_number								--貨號
,dr.order_number order_number					--退貨單號
,isnull(A2.Receive_contact,dr.Send_contact) Send_contact		----送件人
,case when A2.Receive_contact is not null then isnull(A2.receive_tel1,'')
else isnull(dr.send_tel,'') end send_tel		----送件人電話
,dr.pieces										--幾件
,case when A2.Receive_contact is not null then ISNULL(A2.Receive_city,'') + ISNULL(A2.Receive_area,'') + ISNULL(A2.Receive_address,'') 
else ISNULL(dr.Send_city,'') + ISNULL(dr.Send_area,'') + ISNULL(dr.Send_address,'') end
send_addr										--送件人(地址)
                                         ,CONVERT(CHAR(10),dr.supplier_date,111) supplier_date
                                         ,CASE dr.receipt_flag WHEN 1 THEN 1 ELSE 0 END receipt_flag 
                                         ,_fee.total_fee ship_fee --'貨件運費'
                                         ,dr.arrive_to_pay_freight  --'到付運費'
                                         ,dr.customer_code
                                         ,dr.uniform_numbers
                                         ,NULL account_type --'月結／現收'
                                         ,_ticket.code_name ticket -- '傳票區分'
                                         ,dr.collection_money
                                         ,NULL account_date --'入帳日'
                                         ,_arr.arrive_state --'配達狀況
                                         ,_arr.arrive_date  --'配達時間
                                         ,_arr.arrive_item  arrive_item -- '配送異常說明
                                         ,CASE _arr.arrive_scan WHEN 1 THEN '是' ELSE '否' END arrive_scan --是否已掃瞄
										 ,upper(_arr.newest_driver_code) newest_driver_code										   --取件人
                                         ,dr.invoice_desc
										 ,case when dr.invoice_desc  not like '%來回件%' then '退貨' else '來回件' end returntype
                                         ,dr.subpoena_category
                                    FROM tcDeliveryRequests dr With(Nolock)
									Left join tcDeliveryRequests A2 with(nolock) on dr.send_contact = A2.check_number
                                    LEFT JOIN tbSuppliers sup With(Nolock) ON sup.supplier_code = dr.supplier_code
                                    LEFT JOIN tbItemCodes _special With(Nolock) ON dr.special_send = _special.code_id AND _special.code_sclass ='S4'
                                    LEFT JOIN tbItemCodes _ticket With(Nolock) ON dr.subpoena_category = _ticket.code_id AND _ticket.code_sclass ='S2'
                                    LEFT JOIN tbCustomers cus With(Nolock) ON dr.customer_code = cus.customer_code AND cus.supplier_code IN('001','002')
                                    CROSS APPLY dbo.fu_GetShipFeeByRequestId(dr.request_id) _fee 
                                    CROSS APPLY dbo.fu_GetDeliverInfo(dr.request_id) _arr
                                    WHERE  CONVERT(CHAR(10),dr.cdate,111) BETWEEN @dateS AND @dateE
									 and dr.pricing_type = '02'
									 and dr.DeliveryType = 'R' 
                                     AND dr.supplier_date IS NOT NULL
                                     AND dr.cancel_date IS NULL 
                                     AND (@keyword ='' or _arr.newest_driver_code LIKE '%'+ @keyword +'%' )
                                     AND dr.Less_than_truckload = @Less_than_truckload
									 and dr.area_arrive_code = @arrive
{0}
ORDER BY dr.cdate 
", wherestr);

                        #endregion
                
                cmd.CommandText = string.Format(strSQL, wherestr);
                cmd.Parameters.AddWithValue("@arrive", search.arrive);
                cmd.Parameters.AddWithValue("@dateS", search.date_start.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@dateE", search.date_end.ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
                cmd.Parameters.AddWithValue("@keyword", search.keyword);
                dt = dbAdapter.getDataTable(cmd);
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.ToString() + "');</script>", false);
        }
        return dt;
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    public class ListData
    {
        /// <summary>1:網頁list顯示 2.列印標籤資訊</summary>
        public int type { get; set; }
        /// <summary>區配商 </summary>
        public string supplyer { get; set; }

        /// <summary>配送廠商 </summary>
        public string arrive { get; set; }

        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        /// <summary>論板專車 </summary>
        public string rbpricingtype { get; set; }
        /// <summary>貨號、寄件人、收貨人(where條件)</summary>
        public String keyword { get; set; }
        /// <summary>自選項目(ex:5,6,8)</summary>
        public String check_item { get; set; }
        /// <summary>全選 </summary>
        public Boolean check_all { get; set; }


        public ListData()
        {
            type = 1;
            check_item = "";
            check_all = false;
            keyword = "0";
        }

    }
}