﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LTtransport_4.aspx.cs" Inherits="LTtransport_4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(function () {
                init();
            });
        });

        function init() {
            $("input[type=radio][name=check]").change(function () {
                if (this.value == "chkHeader") {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                    $("input[type=number][name=txtChkRange]").val(0);
                } else if (this.value == "chkRange") {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $("#custom_table [id*=chkRow]").change(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("input[type=radio][name=check][value=chkHeader]").prop("checked", true);
                } else {
                    $("input[type=radio][name=check][value=chkRange]").prop("checked", true);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $("input[type=number][name=txtChkRange]").change(function () {
                var start = $("#chkStart").val() - 1;
                if (start < 0)
                    start = 0;
                var end = $("#chkEnd").val();

                if (start <= end) {
                    $("#custom_table input[type='checkbox']").prop("checked", false);
                    $("#custom_table input[type='checkbox']").slice(start, end).prop("checked", true);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $("input[type=number][name=txtChkRange]").focus(function () {
                $("input[type=radio][name=check][value=chkRange]").prop("checked", true);
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                defaultDate: (new Date())  //預設當日
            });

             <%--$("#<%= option.ClientID%>").change(function () {
              var type = $("#<%= option.ClientID%> option:selected").val()
                 if (type == '0') {
                     $('#divStartfrom').show(); 
                 }
                 else {
                     $('#divStartfrom').hide();
                     $("#<%= StartFrom.ClientID%>").val('1');
                 }
            });--%>

        }
    </script>

    <style>
        input[type="radio"] {
            display: inline-block;
        }

        input[type="checkbox"] {
            display: inherit;
        }


        .auto-style1 {
            width: 206px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hid_sel" runat="server" />
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">標籤列印</h2>


            <div class="templatemo-login-form">
                <!---->
                <div class="form-group form-inline">
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    <asp:DropDownList ID="Customer" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>
                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>未列印</asp:ListItem>
                        <asp:ListItem>已列印</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="lbDate" runat="server" Text="發送日期"></asp:Label>
                    <asp:TextBox ID="date1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate1" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>~
                    <asp:TextBox ID="date2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <asp:TextBox ID="cdate2" runat="server" Visible="false" CssClass="form-control datepicker"></asp:TextBox>
                    <%--<asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>                    --%>
                    <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20"></asp:TextBox>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                </div>
            </div>
            <hr>
            <%--<p>※筆數：<span class="text-danger"><asp:Literal ID="ltotalpages" runat="server"></asp:Literal></span></p>--%>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lbErr" runat="server" ForeColor="Red" Text="※請勾選要列印的託運單"></asp:Label>
                </ContentTemplate>
            </asp:UpdatePanel>

            <input type="radio" name="check" value="chkHeader">全選
            <br>
            <input type="radio" name="check" value="chkRange"/>從：
            <input type="number" name="txtChkRange" id="chkStart" value="0" />至：<input type="number" name="txtChkRange" id="chkEnd" value="0" />


            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="inner-wrap" style="overflow: auto; height: calc(20vh); width: 100%">
                        <table id="custom_table" class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th></th>
                                <th>No.</th>
                                <th>建檔日期</th>
                                <th>發送日期</th>
                                <th>配送日期</th>
                                <th>貨號</th>
                                <th>收貨人</th>
                                <th>電話</th>
                                <th>地址</th>
                                <th>件數</th>
                                <th>板數</th>
                                <th>才數</th>
                                <th>傳票類別</th>
                                <th>付款</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td data-th="列印">
                                            <asp:CheckBox ID="chkRow" runat="server" />
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>' />
                                            <asp:HiddenField ID="Hid_print_flag" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_flag").ToString())%>' />
                                            <asp:HiddenField ID="Hid_arrive_assign_date" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.arrive_assign_date","{0:yyyy/MM/dd}").ToString())%>' />

                                        </td>
                                        <td data-th="No">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ROWID").ToString())%>
                                        </td>
                                        <td data-th="建檔日期">
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate","{0:yyyy/MM/dd}").ToString())%>
                                        </td>
                                        <td data-th="發送日期">
                                            <asp:Label ID="lbprint_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="配送日期">
                                            <asp:Label ID="lbSupplier_date" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date","{0:yyyy/MM/dd}").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="貨號">
                                            <asp:Label ID="lbcheck_number" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>'></asp:Label>
                                        </td>
                                        <td data-th="收貨人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%> </td>
                                        <td data-th="電話"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1").ToString())%>#<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_tel1_ext").ToString())%></td>
                                        <td data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address").ToString())%></td>
                                        <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> </td>

                                        <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%> </td>
                                        <td data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cbm").ToString())%> </td>
                                        <td data-th="傳票類別"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                        <td data-th="付款"></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="13" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                    </div>
                    <p>列印勾選筆數：<span class="chkcount"></span></p>
                </ContentTemplate>
            </asp:UpdatePanel>

            <hr>

            <div class="templatemo-login-form">
                <!---->
                <div class="form-style-10">
                    <div class="section"><span>1</span>進行列印</div>
                    <div class="inner-wrap">
                        <%--<button type="submit" class="templatemo-blue-button">進行列印</button>--%>

                        <div class="form-group form-inline">
                            <asp:Button ID="btnPirntLabel" CssClass="btn btn-primary" runat="server" Text="進行列印" OnClick="btnPirntLabel_Click" />
                            <asp:DropDownList ID="option" runat="server" class="form-control" OnSelectedIndexChanged="ShowPositionOrNot" AutoPostBack="true">
                                <asp:ListItem Value="0">A4 (一式6筆託運標籤)</asp:ListItem>
                                <%-- <asp:ListItem Value="2">A4 (一式1筆託運標籤)</asp:ListItem>--%>
                                <asp:ListItem Value="1">捲筒列印</asp:ListItem>
                                <asp:ListItem Value="2">一筆兩式</asp:ListItem>
                            </asp:DropDownList>
                            <asp:CheckBox ID="cbShowSender" runat="server" Text="顯示寄件人資料" Checked="True" class="checkbox-inline" />
                        </div>
                        <div class="form-group form-inline">
                            <div id="posi" style="display: inline-block" runat="server">
                                選擇列印起始位置:
                                <asp:RadioButtonList ID="Position" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" CssClass="radio radio-success" Style="left: 0px; top: 0px; border-style: solid; border-width: 1px;">
                                    <asp:ListItem Value="1" Text="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="2" Text=""></asp:ListItem>
                                    <asp:ListItem Value="3" Text=""></asp:ListItem>
                                    <asp:ListItem Value="4" Text=""></asp:ListItem>
                                    <asp:ListItem Value="5" Text=""></asp:ListItem>
                                    <asp:ListItem Value="6" Text=""></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="form-group">
                                 <label class="form-group form-inline" ID="printFileType" style="display: inline-block" runat="server">託運總表格式</label>
                                </div>
                            <div class="form-group">
                                <span>
                                    <input type="radio" ID="ExcelFile" value="1" runat="server" checked> Excel (可編輯)
                                 </span>
                                <br>
                                <span>
                                    <input type="radio" ID="PDFFile" value="2" runat="server"> PDF (不可編輯)
                                 </span>
                                <br>
                            </div>
                        </div>
                        <%-- <div id="divStartfrom" class="form-group "  >
                                自第<asp:TextBox ID="StartFrom" runat="server"  class="form-control" MaxLength="1" Width="50">1</asp:TextBox>格續印
                            </div>
                            <asp:RangeValidator ID="rv1" runat="server" ControlToValidate="StartFrom" ErrorMessage="需介於 1~6" ForeColor="Red" MaximumValue="6" MinimumValue="1" Type="Integer" ></asp:RangeValidator>--%>
                    </div>
                    <div class="section"><span>2</span>列印託運總表</div>
                    <div class="inner-wrap">
                        <asp:Button ID="btPrint" class="templatemo-blue-button" runat="server" Text="列印託運總表" OnClick="btPrint_Click" />
                        <asp:ListBox ID="lbMsg" runat="server" Width="100%" CssClass="lb_set" Visible="false"></asp:ListBox>
                    </div>
                    <div class="section"><span>3</span>出貨</div>
                    <p class="text-danger">依與站所約定時間前提出派員收件要求</p>
                    <asp:DropDownList ID="customerDropDown" runat="server" CssClass="chosen-select"></asp:DropDownList>
                    <asp:Button ID="Button" CssClass="btn btn-primary" runat="server" Text="派員收件" OnClick="btnAskForTakePackage_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

