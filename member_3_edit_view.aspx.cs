﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class member_3_edit_view : System.Web.UI.Page
{
    DataTable grid = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        string customer_code = "";
        if (Request.QueryString["customer_code"] != null) customer_code = Request.QueryString["customer_code"].ToString();

        if (grid == null)
            Getgrid(customer_code);

        viewtable.DataSource = grid;
        viewtable.DataBind();
    }

    private void Getgrid(string customer_code)
    {
        using (SqlCommand c = new SqlCommand())
        {

            c.CommandText = @"declare @customer_type char(1);
                                  declare @pricing_code nvarchar(2);
                                  declare @log_id int; 
                                  declare @tariffs_effect_date datetime;

                                  select @customer_type = customer_type, @pricing_code = pricing_code 
                                  from tbCustomers where customer_code = '" + customer_code + @"'

                                  select @log_id = log_id,  @tariffs_effect_date = tariffs_effect_date from ttPriceBusinessDefinelog where customer_code = '" + customer_code + @"'
                                    
                                    /*論件*/
								  IF (@pricing_code = '02' AND @log_id is null)
                                      BEGIN
			                          /*取公版*/				   
                  
                                         SELECT A.start_city '起點區域' ,A.end_city '迄點區域',
                                         A.twenty '1件~20件',A.forty '21件~40件',A.sixty '41件~60件',A.eighty '61件~80件',
                                         A.one_hundred '81件~100件',A.above_hundred '100件以上'
                                         FROM PalletPiecesShippingFeePublicVersion A WITH(Nolock)                                         
                                         WHERE class_level = '5'
				                       END

                                  ELSE IF (@pricing_code = '02' AND @log_id is not null )
			                          BEGIN
			                          /*取自訂*/
				  					    
                                         SELECT A.start_city '起點區域' ,A.end_city '迄點區域',
                                         A.twenty '1件~20件',A.forty '21件~40件',A.sixty '41件~60件',A.eighty '61件~80件',
                                         A.one_hundred '81件~100件',A.above_hundred '100件以上'
                                         FROM PalletPiecesShippingFeeDefine A With(Nolock)                                          
                                         WHERE A.customer_code ='" + customer_code + @"' AND A.active_date = @tariffs_effect_date;  
					                  END

                                  /*區配論板(公版)*/
                                  ELSE IF @customer_type = '1' and @log_id is null
			                          BEGIN
						                SELECT A.start_city '起點區域' ,A.end_city '迄點區域', A.class_level '級距' , B.code_name '計價模式' , 
							            A.plate1_price '板數1',A.plate2_price '板數2',A.plate3_price '板數3',A.plate4_price '板數4',
							            A.plate5_price '板數5',A.plate6_price '板數6'
						                FROM tbPriceSupplier A WITH(Nolock)
						                LEFT JOIN tbItemCodes B WITH(Nolock) ON B.code_id = A.pricing_code AND code_sclass = 'PM'  
							            WHERE pricing_code = @pricing_code AND class_level = '5' 
					                  END
			                      
                                  /*自營論板 及 論件自訂*/
                                  ELSE IF (@customer_type = '3' AND @log_id is null)
                                      BEGIN
			                          /*取公版*/				   
                  
                                         SELECT A.start_city '起點區域' ,A.end_city '迄點區域',  B.code_name '計價模式' , 
                                         A.plate1_price '板數1',A.plate2_price '板數2',A.plate3_price '板數3',A.plate4_price '板數4',
                                         A.plate5_price '板數5',A.plate6_price '板數6'
                                         FROM tbPriceSupplier A WITH(Nolock)
                                         LEFT JOIN tbItemCodes B WITH(Nolock) ON B.code_id = A.pricing_code AND code_sclass = 'PM'  
                                         WHERE pricing_code = @pricing_code	AND class_level = '5'
				                       END

                                  ELSE IF (@customer_type = '3' AND @log_id is not null ) or (@customer_type = 1)
			                          BEGIN
			                          /*取自訂*/
				  					    
                                         SELECT A.start_city '起點區域' ,A.end_city '迄點區域',  B.code_name '計價模式' , 
                                         A.plate1_price '板數1',A.plate2_price '板數2',A.plate3_price '板數3',A.plate4_price '板數4',
                                         A.plate5_price '板數5',A.plate6_price '板數6'
                                         FROM tbPriceBusinessDefine A With(Nolock) 
                                         LEFT JOIN tbItemCodes B WITH(Nolock) ON B.code_id = A.pricing_code AND B.code_sclass = 'PM' 
                                         WHERE A.customer_code ='" + customer_code + @"' AND pricing_code =  @pricing_code  AND A.active_date = @tariffs_effect_date;  
					                  END";
            // 若再次上傳運價表並設定同樣的生效日期，ttPriceBusinessDefinelog不會再次新增日期，tbPriceBusinessDefine的active_date不知道要抓哪個，所以只會顯示次新的運價。
            grid = dbAdapter.getDataTable(c);
        }
    }
}