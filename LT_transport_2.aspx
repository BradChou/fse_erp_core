﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LTMasterTransport.master" AutoEventWireup="true" CodeFile="LT_transport_2.aspx.cs" Inherits="LT_transport_2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />

    <script type="text/javascript">





        $(document).ready(function () {
            $('.fancybox').fancybox({
                'width': '100%',


            });
            $("#fancybox").css({ 'width': '500px', 'height': '500px' });
            $.fancybox.update();
            $(".datepicker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                defaultDate: (new Date())  //預設當日
            });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".datepicker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    defaultDate: (new Date())  //預設當日
                });
            }
        });


        $(document).on("click", "._new_addr", function () {
            var _url = "LT_transport_2_edit.aspx?r_type=1&request_id=" + $(this).parent("td").attr("_the_id");
            $.fancybox({
                'width': '60%',
                'height': '95%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'type': 'iframe',
                'href': _url
            });
        });

        function CopyReturnlocation(request_id) {
            Model_CurrentPage.Session.Set("transport_1_2");
            location.href = "transport_1_2.aspx?request_id=" + request_id;
        }
    </script>
    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .checkbox, .radio {
            display: inherit;
        }

            .radio label {
                margin-right: 25px;
                text-align: left;
            }

            .checkbox label {
                margin-right: 15px;
                text-align: left;
            }

        .btn_session {
            text-align: center;
            padding: 5px;
        }

        .btn {
            margin: 0px 10px;
        }

        ._title {
            display: inline-block;
            margin-bottom: 5px;
            font-weight: 700;
            max-width: 100px;
            margin-right: 3px;
            min-width: 72px;
        }

        ._tip_important:before {
            content: "* ";
            color: red;
        }

        ._div_type {
            margin-left: 15px;
        }

        .div_file {
            display: inline-flex;
            margin-left: 20px;
        }

        .date_picker {
            width: 112px !important;
            margin: auto 5px;
        }

        ._th {
            text-align: center;
        }

        .td_no {
            width: 5%;
        }

        .td_ck_number, .td_assign_date, .td_supp, .td_ImNum {
            width: 10%;
        }

        .td_addr {
            text-align: left;
            width: 20%;
        }




        .td_btn {
            width: 7%;
            white-space: nowrap;
        }




        ._tip {
            color: #5b6686;
        }

        .auto-style1 {
            text-align: center;
            height: 38px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">查詢及補單作業</h2>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div id="div_search" runat="server">
                        <div class="templatemo-login-form">
                            <div class="form-group form-inline">
                                <asp:RadioButton ID="rb2" runat="server" CssClass="radio radio-success" Text="發送區間" GroupName="dategroup" Checked="true"></asp:RadioButton>

                                <asp:TextBox ID="tb_dateS2" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req_dateS" runat="server" ControlToValidate="tb_dateS" ForeColor="Red" ValidationGroup="validate">請輸入發送區間</asp:RequiredFieldValidator>--%>
                            ~
                        <asp:TextBox ID="tb_dateE2" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="req_dateE" runat="server" ControlToValidate="tb_dateE" ForeColor="Red" ValidationGroup="validate">請輸入發送區間</asp:RequiredFieldValidator>--%>
                                <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="btnQry_Click" ValidationGroup="validate" />
                                <%-- <asp:Button ID="btnAdd" CssClass="templatemo-white-button" runat="server" Text="單筆補單" OnClick="btnAdd_Click" />--%>
                                <asp:Button ID="btnExport" CssClass="templatemo-blue-button" runat="server" Text="匯出" OnClick="btnExport_Click" />
                                <%--<a href="LT_transport_2_Search.aspx" class="fancybox fancybox.iframe btn  _d_btn " id="btCancel"><span class="btn btn-primary">整批銷單</span></a>--%>
                            </div>
                            <div class="form-group form-inline">
                                <asp:RadioButton ID="rb1" runat="server" CssClass="radio radio-success" Text="建單區間" GroupName="dategroup"></asp:RadioButton>
                                <asp:TextBox ID="tb_dateS1" CssClass="form-control datepicker _dates" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" runat="server" ControlToValidate="tb_dateS" ForeColor="Red" ValidationGroup="validate">請輸入建單區間</asp:RequiredFieldValidator>--%>
                            ~
                            <asp:TextBox ID="tb_dateE1" CssClass="form-control datepicker _datee" runat="server" MaxLength="10" placeholder="YYYY/MM/DD" autocomplete="off"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="tb_dateE" ForeColor="Red" ValidationGroup="validate">請輸入建單區間</asp:RequiredFieldValidator>                        --%>



                                <div class="div_file" style="display: none;">
                                    <asp:FileUpload ID="fileToUpload" runat="server" />
                                    <input type="button" class="btn btn-primary" value="上 傳" />
                                </div>
                            </div>

                            <div class="form-group form-inline">
                                <label class="">客戶代號</label>
                                <asp:DropDownList ID="Customer" CssClass="form-control chosen-select" runat="server"></asp:DropDownList>
                                <%--<asp:RadioButton ID="rb3" runat="server" CssClass="radio radio-success" Text="託運單號" GroupName="dategroup"></asp:RadioButton>--%>
                                <label>託運單號：</label>
                                <asp:TextBox ID="check_number" runat="server" class="form-control" placeholder="請輸入貨號" MaxLength="20"></asp:TextBox>
                            </div>
                            <div class="form-group form-inline">
                                <label>主客代：</label>
                                <asp:DropDownList ID="MasterCustomer" CssClass="form-control chosen-select" runat="server"></asp:DropDownList>
                            </div>
                            <div class="form-group form-inline">
                                <label class="">類型</label>
                                <asp:DropDownList ID="DR_type" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="">全部</asp:ListItem>
                                    <asp:ListItem Value="D">正物流</asp:ListItem>
                                    <asp:ListItem Value="R">逆物流</asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RadioButton ID="rb3" runat="server" CssClass="radio radio-success" Text="託運單號" GroupName="dategroup"></asp:RadioButton>--%>

                                <label class="">狀態</label>
                                <asp:DropDownList ID="dlStatus" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="">全部</asp:ListItem>
                                    <asp:ListItem Value="0">正常件</asp:ListItem>
                                    <asp:ListItem Value="1">已銷單</asp:ListItem>
                                </asp:DropDownList>
                            </div>


                            <%--<div class="margin-right-15 templatemo-inline-block">
                                <label for="inputFirstName">託運類別：</label>
                                <asp:RadioButtonList ID="rb_pricing_type" runat="server" RepeatDirection="Horizontal"  RepeatLayout="Flow" CssClass="radio radio-success" Visible ="false" >                                   
                                </asp:RadioButtonList>
                            </div>--%>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div id="divAdd" runat="server" visible="false" class="templatemo-login-form panel panel-default">
                        <div class="panel-heading">
                            <h2 class="text-uppercase">單筆補單</h2>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <div class="margin-right-15 templatemo-inline-block _div_type">
                                        <span class="_title _tip_important">託運類別</span>
                                        <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged" CssClass="radio radio-success">
                                            <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                            <asp:ListItem Value="02">02 論件</asp:ListItem>
                                            <asp:ListItem Value="03">03 論才</asp:ListItem>
                                            <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <label>出貨日期：</label><asp:TextBox ID="Shipments_date" runat="server" class="form-control" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">貨　　號</span>
                                    <asp:DropDownList runat="server" ID="ddl_check_number2" class="form-control ddl_check_number2"></asp:DropDownList>
                                    <asp:Label ID="lbErr_chknum" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">託運類別</span>
                                    <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                    </asp:DropDownList>
                                    <span class="_title _tip_important">傳票類別</span>
                                    <asp:DropDownList ID="subpoena_category" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">收件人</span>

                                    <asp:TextBox ID="receive_contact" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">寄件人</span>
                                    <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">收件地址</span>

                                    <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:TextBox ID="receive_address" CssClass="form-control" runat="server" placeholder="請輸入地址"></asp:TextBox>
                                    <asp:DropDownList ID="area_arrive_code" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title _tip_important">寄件地址</span>
                                    <asp:DropDownList ID="send_city" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="send_area" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:TextBox ID="send_address" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">區配商代碼</span>
                                    <asp:TextBox ID="supplier_code" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                    <span class="_title">客戶代號</span>
                                    <asp:DropDownList ID="dlcustomer_code" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline" style="left: 0px; top: 0px">
                                    <span class="_title">主客戶代號</span>
                                    <asp:DropDownList ID="dlMastercustomer_code" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dlMastercustomer_code_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title"></span>
                                    <asp:CheckBox ID="receipt_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="回單" />
                                    <asp:CheckBox ID="pallet_recycling_flag" class="font-weight-400 checkbox checkbox-success" runat="server" Text="棧板回收" />
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">貨物數量</span>
                                    <asp:TextBox ID="pieces" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    件數
                                <asp:TextBox ID="plates" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    板
                                <asp:TextBox ID="cbm" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="80px" placeholder="0"></asp:TextBox>
                                    才
                                <asp:Label ID="lbErr" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">備　　註</span>
                                    <asp:TextBox ID="invoice_desc" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 col-md-6 form-group form-inline">
                                    <span class="_title">費　　用</span>
                                    <label>代收金</label>
                                    <asp:TextBox ID="collection_money" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    &nbsp;
                                <label>到付運費</label>
                                    <asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                    &nbsp;
                                <label>到付追加</label>
                                    <asp:TextBox ID="arrive_to_pay_append" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                </div>
                            </div>
                            <div class="btn_session">
                                <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="新增" OnClick="btnSave_Click" />
                                <asp:Button ID="btnCanle" CssClass="btn templatemo-white-button" runat="server" Text="取消" OnClick="btnCanle_Click" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExport" />
                </Triggers>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <div id="div_list" runat="server">
                        <table class="table table-striped table-bordered templatemo-user-table">
                            <tr class="tr-only-hide">
                                <th class="auto-style1">No.</th>
                                <th class="auto-style1">貨　　號</th>
                                <th class="auto-style1">配送日</th>
                                <th class="auto-style1">發送站</th>
                                <th class="auto-style1">寄件人</th>
                                <th class="auto-style1">收件人</th>
                                <th class="auto-style1">地　　址</th>
                                <th class="auto-style1">區配商代碼</th>
                                <th class="auto-style1">類型</th>
                                <th class="auto-style1">建檔人員</th>
                                <th class="auto-style1">更新日期</th>
                                <th class="auto-style1">匯入日期</th>
                                <th class="auto-style1">匯入單號</th>



                                <th class="auto-style1">託運明細</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td class="td_no" data-th="No"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ROWID").ToString())%></td>
                                        <td class="td_ck_number " data-th="貨號">
                                            <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>' />
                                            <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString()) %>
                                            <%--<a href="LT_transport_2_edit.aspx?r_type=0&request_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>&scan=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.logNum").ToString())%>" class="fancybox fancybox.iframe btn btn-link " id="updclick">
                                        <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>
                                    </a> --%>                               
                                        </td>
                                        <td class="td_assign_date " data-th="配送日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_date").ToString())%></td>
                                        <td class="td_send " data-th="發送站"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.sendstation2").ToString())%></td>
                                        <td class="td_sender " data-th="寄件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_contact").ToString())%> </td>
                                        <td class="td_receiver " data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%> </td>
                                        <td class="td_addr " data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address").ToString())%></td>
                                        <td class="td_supp " data-th="區配商代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString())%></td>
                                        <td class="" data-th="類型"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.DeliveryType").ToString())%></td>
                                        <td class="td_user " data-th="建檔人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                        <td class="td_udate " data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                        <td class="td_udate " data-th="匯入日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cdate" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                        <td class="td_ImNum " data-th="匯入單號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.import_randomCode").ToString())%></td>


                                        <td class="td_btn" _the_id='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%>'>
                                            <%--  如果已銷單->只能瀏覽，不能轉址，不能修改
                                                如果已配送->只能轉址，不能修改 
                                                其他->可修改 --%>
                                            <%#Server.HtmlDecode(DataBinder.Eval(Container, "DataItem.pricing_type").ToString()) == "05" ?"<a onclick='CopyReturnlocation(\""+DataBinder.Eval(Container, "DataItem.request_id").ToString()+"\")' class='btn btn-warning' >複製</a>":"" %>
                                            <%#Server.HtmlDecode((DataBinder.Eval(Container, "DataItem.cancel_date").ToString()) != ""  ? 
                                            "<a href='LT_transport_2_edit.aspx?r_type=2&request_id="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString()) +"&scan="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.logNum").ToString()) + "' class='fancybox fancybox.iframe btn btn-warning ' id='updclick'>已銷單</a> ":
                                            "<a href='LT_transport_2_edit.aspx?r_type=0&request_id="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString()) +"&scan="+ Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.logNum").ToString()) + "' class='fancybox fancybox.iframe btn btn-primary _d_btn ' id='updclick'>瀏覽</a>")%>
                                            <asp:Button ID="btn_UnCancel" CssClass="btn btn-warning" CommandName="cmdUnCancel" runat="server" Style="display: none" Text="取消銷單" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.request_id").ToString())%> '
                                                Visible='<%#Server.HtmlDecode((DataBinder.Eval(Container, "DataItem.cancel_date").ToString())) !=  "" ? true : false %>' OnClientClick="return confirm('確定取消銷單嗎?');" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="14" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
              
                        <nav class=" navbar text-center ">
                            <ul class="pagination">
                                <li>
                                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink></li>
                                <asp:Literal ID="pagelist" runat="server"></asp:Literal>
                                <li>
                                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink></li>
                            </ul>
                        </nav>

                        <%--<div class="pager">
                    <asp:HyperLink ID="lnkfirst" runat="server">&laquo; 最前頁</asp:HyperLink>
                    <asp:HyperLink ID="lnkPrev" runat="server">&#8249; 前一頁</asp:HyperLink>
                    <asp:TextBox Style="text-align: center; width: 100px;" ID="tbPage" runat="server" ReadOnly="True">1／1</asp:TextBox>
                    <asp:HyperLink ID="lnkNext" runat="server">下一頁 &#8250;</asp:HyperLink>
                    <asp:HyperLink ID="lnklast" runat="server">最後頁 &raquo;</asp:HyperLink>
                    共 <span class="text-danger">
                        <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                    </span>筆
                </div>--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</asp:Content>

