﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RedirectToLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string domainName = HttpContext.Current.Request.Url.Host;

        if (domainName == "supererp.fs-express.com.tw")
        {
            Response.Redirect("FSElogin.aspx");
        }
        else
        {
            Response.Redirect("login.aspx");
        }
    }
}