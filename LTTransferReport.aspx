﻿<%@ Page Language="C#" MasterPageFile="~/LTMasterReport.master" AutoEventWireup="true" CodeFile="LTTransferReport.aspx.cs" Inherits="LTTransferReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".chosen-select").chosen({
                no_results_text: "My language message.",
                placeholder_text: "My language message.",
                search_contains: true,
                disable_search_threshold: 10
            });

            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                            .slice(start, end).show();
                    }
                });
            });
            $(function () {
                init();
            });
        });
        function init() {
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                defaultDate: (new Date())  //預設當日
            });

        }

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            /*margin-right: 5px;*/
            text-align: left;
        }

        .checkbox label {
            /*margin-right: 5px;*/
            text-align: left;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">轉聯運明細表</h2>
            <hr />
            <div class="form-group form-inline">
                <label>站所：</label>
                <span class="text-danger">
                    <asp:Label ID="lbSuppliers" runat="server"></asp:Label></span>
                <label>轉聯運筆數：</label>
                <span class="text-danger">
                    <asp:Label ID="totle" runat="server"></asp:Label></span>
            </div>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>出貨日期：</label>
                    <asp:TextBox ID="date1" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                    <label>配送站：</label>
                    <asp:DropDownList ID="ddlSendStation" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSendStation_SelectedIndexChanged"></asp:DropDownList>
                    <label>託運單號：</label>
                    <asp:TextBox ID="check_number" runat="server" class="form-control"></asp:TextBox>

                </div>
                <div class="form-group form-inline">
                    <asp:Button ID="btnselect" CssClass="btn btn-primary btn_view" runat="server" Text="查 詢" OnClick="btnQry_Click" />
                    <%--<asp:Button ID="btnExcel" CssClass="btn btn-primary btn_view" runat="server" Text="查 詢" OnClick="btnExcel_Click" />--%>
                    <a id="_dw" class="_hide" href="#" target="_blank" style="display: none; visibility: hidden;">開窗列印</a>
                </div>
            </div>
            <hr>
            <table class="table table-striped table-bordered templatemo-user-table">
                <tr class="tr-only-hide">
                    <%--<th class="header">
                        <asp:CheckBox ID="cbAll" runat="server" OnCheckedChanged="Check_Clicked" AutoPostBack="true" />
                    </th>--%>
                    <th style="white-space: nowrap;">託運單號</th>
                    <th style="white-space: nowrap;">掃讀日期</th>
                    <th style="white-space: nowrap;">轉運商</th>
                    <th style="white-space: nowrap;">作業人員帳號+姓名</th>
                    <th style="white-space: nowrap;">作業站所代碼</th>
                    <%--<th style="white-space: nowrap;">功能</th>--%>
                </tr>
                <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                    <ItemTemplate>
                        <tr class="paginate">
                           <%-- <td style="white-space: nowrap;" data-th="序號">
                                <asp:CheckBox ID="chkRow" class="Method" runat="server" />
                              
                            </td>--%>
                            <td style="white-space: nowrap;" data-th="託運單號">
                                  <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.TransferID").ToString())%>' />
                                <a href="LT_trace_1.aspx?check_number=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%>" target="_blank" class=" btn btn-link " id="updclick"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></a>
                            </td>

                            <td style="white-space: nowrap;" data-th="掃讀日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.scanning_dt","{0:yyyy/MM/dd HH:mm:ss}").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="轉運商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.Transfer").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業人員帳號 +姓名"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver_code").ToString())%></td>
                            <td style="white-space: nowrap;" data-th="作業站所代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.station_code").ToString())%></td>
                           <%-- <td style="white-space: nowrap;" data-th="編輯">
                                <asp:Button ID="btn_Edit" CssClass="btn btn-success" CommandName="cmdEdit"
                                    runat="server" Text="編輯" CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.TransferID").ToString())%>' />
                            </td>--%>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <% if (New_List.Items.Count == 0)
                    {%>
                <tr>
                    <td colspan="5" style="text-align: center">尚無資料</td>
                </tr>
                <% } %>
            </table>
            共 <span class="text-danger">
                <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
            </span>筆
            <div id="page-nav" class="page"></div>
        </div>
    </div>

    <div class="modal fade" id="modal-default_01" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <asp:RadioButtonList ID="rd1" runat="server" OnSelectedIndexChanged="rd1_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Value="1" Selected="True">明錩</asp:ListItem>
                                <asp:ListItem Value="2">國陽</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="modal-footer">
                            <div class="templatemo-login-form">
                                <div class="form-group text-center">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">取消</button>
                                    <asp:HiddenField ID="hid_request_id" runat="server" Value="" />
                                    <asp:Button ID="btn_OK" runat="server" class="btn btn-primary" Text="儲存編輯" ValidationGroup="validate" OnClick="btn_OK_Click" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
