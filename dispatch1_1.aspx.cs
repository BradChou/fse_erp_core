﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dispatch1_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    public string ssManager_type
    {
        // for權限
        get { return ViewState["ssManager_type"].ToString(); }
        set { ViewState["ssManager_type"] = value; }
    }

    public string ssSupplier_code
    {
        // for權限
        get { return ViewState["ssSupplier_code"].ToString(); }
        set { ViewState["ssSupplier_code"] = value; }
    }

    public string ssMaster_code
    {
        // for權限
        get { return ViewState["ssMaster_code"].ToString(); }
        set { ViewState["ssMaster_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            //權限
            ssManager_type = Session["manager_type"].ToString(); //管理單位類別   
            ssSupplier_code = Session["master_code"].ToString();
            ssMaster_code = Session["master_code"].ToString();
            if (ssManager_type == "0" || ssManager_type == "1" || ssManager_type == "2" || ssManager_type == "3" || ssManager_type == "4" || ssManager_type == "5")
            {
                ssSupplier_code = (ssSupplier_code.Length >= 3) ? ssSupplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (ssSupplier_code == "999")                                                             //999 : 峻富總公司(管理者)
                {
                    switch (ssManager_type)
                    {
                        case "1":
                        case "2":
                            ssSupplier_code = "";
                            break;
                        default:
                            ssSupplier_code = "000";
                            break;
                    }
                }

            }
            //dateS.Text = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
            //dateE.Text = DateTime.Today.AddDays(-1).ToString("yyyy/MM/dd");
            date.Text = DateTime.Today.ToString("yyyy/MM/dd");

            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand cmd = new SqlCommand();
        string wherestr = "";

        #region 關鍵字

        //權限

        if (ssSupplier_code != "")
        {
            cmd.Parameters.AddWithValue("@supplier_code", ssSupplier_code);
            wherestr += " and A.supplier_code = @supplier_code";
        }

        if ((ssMaster_code != "") && (ssManager_type == "3" || ssManager_type == "5"))
        {
            cmd.Parameters.AddWithValue("@Master_code", ssMaster_code);
            wherestr += " and A.customer_code like @Master_code+'%' ";
        }

        //if (!String.IsNullOrEmpty(dateS.Text) && !String.IsNullOrEmpty(dateE.Text))
        //{
        //    cmd.Parameters.AddWithValue("@dateS", dateS.Text);
        //    cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime( dateE.Text).AddDays(1).ToString("yyyy/MM/dd"));
        //    wherestr += " AND A.print_date >=@dateS and  A.cdate <@dateE ";
        //}

        if (!String.IsNullOrEmpty(date.Text))
        {
            cmd.Parameters.AddWithValue("@dateS", date.Text);
            cmd.Parameters.AddWithValue("@dateE", Convert.ToDateTime(date.Text).AddDays(1).ToString("yyyy/MM/dd"));
            wherestr += " AND A.print_date >=@dateS and  A.print_date <@dateE ";
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請選擇發送日期');</script>", false);
            return;
        }


        if (keyword.Text != "")
        {
            cmd.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
            wherestr += "  and (B.customer_name like '%'+@keyword+'%' or A.send_city +A.send_area + A.send_address like '%'+@keyword+'%' or A.receive_contact like '%'+@keyword+'%' or E.supplier_name like '%'+@keyword+'%' or A.area_arrive_code like '%'+@keyword+'%')";
        }

        //if (rb_pricing_type.SelectedValue != "")
        //{
        //    cmd.Parameters.AddWithValue("@pricing_type", rb_pricing_type.SelectedValue);
        //    wherestr += " and A.pricing_type = @pricing_type";

        //}

        //if (Customer.SelectedValue != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_code", Customer.SelectedValue);
        //    wherestr += " and A.customer_code like @customer_code+'%' ";
            
        //}

       
        #endregion

        cmd.CommandText = string.Format(@"Select A.request_id , A.print_date , C.code_name 'pricing_type_name', A.check_number , A.customer_code , B.customer_name, A.send_city +A.send_area + A.send_address 'send_address', A.plates , A.pieces , A.cbm , 
                                                    A.receive_contact, A.area_arrive_code , A.area_arrive_code + ' ' + E.supplier_name 'supplier_name',
                                                    ISNULL(F.t_id, 0) 't_id', ISNULL(G.task_id,'') 'task_id'
                                                    from tcDeliveryRequests A with(nolock)  
                                                    LEFT JOIN tbCustomers B  With(Nolock)on B.customer_code = A.customer_code
                                                    LEFT JOIN tbItemCodes C with(nolock) on A.pricing_type = C.code_id and code_bclass  = '1' and code_sclass  = 'PM'
                                                    LEFT JOIN tbSuppliers E With(Nolock) ON A.area_arrive_code = E.supplier_code and ISNULL(E.supplier_code,'') <> ''
                                                    LEFT JOIN ttDispatchTaskDetail F with(Nolock) on A.request_id = F.request_id 
		                                            LEFT JOIN ttDispatchTask G with(Nolock) on F.t_id = G.t_id 
                                                    WHERE 1=1  {0}
                                                    and A.Less_than_truckload = 0 and A.pricing_type <> '05' and A.supplier_code <> '001'
                                                    ORDER BY A.print_date, A.customer_code , A.area_arrive_code  ", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {   
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }
        
    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = (p["tires_number"] == DBNull.Value ) ? 0 : p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = (p["oil_discount"] == DBNull.Value) ? 0 : p.Field<double>("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = (p["tires_monthlyfee"] == DBNull.Value) ? 0 : p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }


    protected void AddTask_Click(object sender, EventArgs e)
    {
        string ErrStr = string.Empty;
        string request_id = string.Empty;
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                if ((chkRow != null) && (chkRow.Checked))
                {
                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                    request_id += id.ToString() + ",";
                }
            }
        }
        if (request_id != "") request_id = request_id.Substring(0, request_id.Length - 1);
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>OpenLinkByFancyBox('../dispatch1_1_edit.aspx?type=0&request_id=" + request_id+ "' );</script>", false);


    }
}