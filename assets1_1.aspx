﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAssets.master" AutoEventWireup="true" CodeFile="assets1_1.aspx.cs" Inherits="assets1_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }

        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align :left ;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">車輛主檔資料查詢</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label>車牌：</label>
                    <asp:TextBox ID="car_license" runat="server" class="form-control " ></asp:TextBox>
                    <label>所有權：</label>
                    <asp:DropDownList ID="dlownership" runat="server" class="form-control"></asp:DropDownList>

                     <label>使用單位：</label>
                    <asp:DropDownList ID="dldept" runat="server" class="form-control"></asp:DropDownList>

                    <label>車行：</label>
                    <asp:DropDownList ID="ddl_car_retailer" runat="server" class="form-control "></asp:DropDownList>                    
                   
                    <span class="checkbox checkbox-success" style="left: 0px; top: 0px">
                        <asp:CheckBox ID="chk_inspection" class="font-weight-400" runat="server" Text="本月驗車"  />
                    </span>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <%--<asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click"   />--%>
                    <asp:Button ID="btExport2" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport2_Click"   />
                </div>

                <div class="form-group form-inline">
                    <label>司機：</label>
                    <asp:TextBox ID="driver" runat="server" class="form-control " ></asp:TextBox>
                    <label>關鍵字：</label>
                    <asp:TextBox ID="keyword" class="form-control " runat="server" ></asp:TextBox>
                   
                </div>

                <div class="form-group" style="float: right;">
                    <a href="assets1_1_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>                
                <p class="text-primary">※點選任一車輛可查詢及修改</p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th class="_th"></th>
                        <th class="_th">車牌</th>
                        <th class="_th">所有權</th>
                        <th class="_th">使用單位</th>
                        <th class="_th">噸數</th>
                        <th class="_th">廠牌</th>
                        <th class="_th">車行 </th>
                        <th class="_th">年份</th>
                        <th class="_th">月份</th>
                        <th class="_th">輪胎數</th>
                        <th class="_th">車廂型式</th>
                        <th class="_th">油料</th>
                        <th class="_th">油料折扣</th>
                        <th class="_th">ETC</th>
                        <th class="_th">輪胎統包</th>
                        <%--<th class="_th">使用人</th>--%>
                        <%--<th class="_th">業務別1</th>--%>
                        <th class="_th">司機</th>
                        <th class="_th">下次驗車日期</th>
                        <th class="_th">備註</th>
                        <th class="_th">更新人員</th>
                        <th class="_th">更新日期</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">
                                <td>
                                    <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>' />
                                    <a href="assets1_1_edit.aspx?a_id=<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.a_id").ToString())%>" class="btn btn-info " id="updclick">修改</a>
                                </td>
                                <td data-th="車牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_license").ToString())%></td>
                                <td data-th="所有權"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.ownership_text").ToString())%></td>
                                <td data-th="使用單位"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.dept_name").ToString())%></td>
                                <td data-th="噸數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tonnes").ToString())%></td>
                                <td data-th="廠牌"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.brand").ToString())%> </td>
                                <td data-th="車行"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.car_retailer_text").ToString())%></td>
                                <td data-th="年份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.year").ToString())%></td>
                                <td data-th="月份"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.month").ToString())%></td>
                                <td data-th="輪胎數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tires_number").ToString())%></td>
                                <td data-th="車廂型式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cabin_type_text").ToString())%></td>
                                <td data-th="油卡"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.oil").ToString())%></td>
                                <td data-th="油料折扣"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.oil_discount").ToString())%></td>
                                <td data-th="ETC"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.isETC").ToString())%></td>
                                <td data-th="輪胎統包"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tires_monthlyfee").ToString())%></td>
                                <%--<td data-th="使用人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user").ToString())%></td>--%>
                                <%--<td data-th="業務別1"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user").ToString())%></td>--%>
                                <td data-th="司機"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.driver").ToString())%></td>
                                <td data-th="下次驗車日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.next_inspection_date","{0:yyyy/MM/dd}").ToString())%></td>
                                <td data-th="備註" style="width:200px;text-align :left "><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%></td>
                                <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.udate","{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="17" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

