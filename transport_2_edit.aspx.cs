﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


public partial class transport_2_edit : System.Web.UI.Page
{
    public string request_id
    {
        get { return ViewState["request_id"].ToString(); }
        set { ViewState["request_id"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            int i_type = 0;
            int i_id = 0;
            string sScript = string.Empty;
            if (Request.QueryString["request_id"] != null) request_id = Request.QueryString["request_id"].ToString();
            if (Request.QueryString["r_type"] != null)
            {
                if (!int.TryParse(Request.QueryString["r_type"].ToString(), out i_type)) i_type = 0;
            }


            if (!int.TryParse(Request.QueryString["request_id"].ToString(), out i_id) || i_id <= 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('參數異常，請重新確認!');parent.$.fancybox.close();</script>", false);
                return;
            }
            else
            {
                DefaultSet();
                readdata(i_type, request_id);



                sScript += "$('#btCancel').click(function(){" +
                                 " if(confirm('您確定要銷單嗎 ?'))" +
                                 " {$('#btCancel').attr('href', 'CancelDialog.aspx?id=" + hid_id.Value + "');}" +
                                 " }); ";
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript  + "</script>", false);
                //return;
            }

            //客戶帳號到著站選擇隱藏
            if (manager_type == "3")
            {
                area_arrive_code.Style.Add("display", "none");
            }

            //客戶帳號到著站選擇隱藏
            if (manager_type == "3")
            {
                send_station_scode.Style.Add("display", "none");
            }

            //1.配達，且配達區分 = 正常配交，就不可改指定日期
            //2.指配日期不可以設定今天以前(含)的日期
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.Parameters.AddWithValue("@request_id", request_id);
                cmd2.CommandText = " select log_id from ttDeliveryScanLog With(Nolock) where check_number = (select check_number from  tcDeliveryRequests With(Nolock) WHERE  request_id=@request_id ) and scan_item = '3' and arrive_option = '3' ";

                DataTable dtb;
                dtb = dbAdapter.getDataTable(cmd2);
                if (dtb != null && dtb.Rows.Count > 0)
                {
                    arrive_assign_date.Enabled = false;
                }
            }

            sScript += "$('.date_picker').datepicker({ dateFormat: 'yy/mm/dd', minDate: 0});";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript + "</script>", false);
            return;

           

        }
    }


    /// <summary>資料載入</summary>
    /// <param name="i_type">0:託運單修改 1:託運轉址 2: 託運單瀏覽</param>
    /// <param name="str_id">託運單id</param>
    private void readdata(int i_type = 0, string str_id = null)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.AddWithValue("@request_id", request_id);
                cmd.CommandText = " SELECT * FROM tcDeliveryRequests With(Nolock) WHERE request_id = @request_id ";

                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    int i_tmp = 0;
                    DateTime dt_tmp = new DateTime();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        rbcheck_type.SelectedValue = dt.Rows[0]["pricing_type"].ToString();              //託運類別 (計價模式 01:論板、02:論件、03論才、04論小板)
                        rbcheck_type_SelectedIndexChanged(null, null);
                        if (!DateTime.TryParse(dt.Rows[0]["print_date"].ToString(), out dt_tmp)) Shipments_date.Text = ""; //出貨日期
                        else Shipments_date.Text = dt_tmp.ToString("yyyy/MM/dd");
                        if (Convert.ToBoolean(dt.Rows[0]["print_flag"]) == true)
                        {
                            Shipments_date.Enabled = false;
                        }
                        dlcustomer_code.SelectedValue = dt.Rows[0]["customer_code"].ToString();          //客代編號
                        customer_code.Text = dt.Rows[0]["customer_code"].ToString();

                        //顯示寄件人資料
                        using (SqlCommand cmda = new SqlCommand())
                        {
                            DataTable dta;
                            cmda.CommandText = @"select C.individual_fee from tbCustomers C With(Nolock) 
                                                where customer_code = '" + customer_code.Text + "' ";
                            dta = dbAdapter.getDataTable(cmda);
                            if (dta.Rows.Count > 0)
                            {
                                bool individualfee = dta.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dta.Rows[0]["individual_fee"]) : false;   //是否個別計價
                                bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
                                if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                                {
                                    individual_fee.Style.Add("display", "block");
                                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                                }
                                else
                                {
                                    individual_fee.Style.Add("display", "none");
                                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                                }
                            }
                        }


                        #region 1. 基本設定
                        hid_id.Value = dt.Rows[0]["request_id"].ToString();
                        lbcheck_number.Text = dt.Rows[0]["check_number"].ToString();                     //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
                        order_number.Text = dt.Rows[0]["order_number"].ToString();                       //訂單號碼
                        check_type.SelectedValue = dt.Rows[0]["check_type"].ToString();                  //託運類別
                        receive_customer_code.Text = dt.Rows[0]["receive_customer_code"].ToString();     //收貨人編號
                        subpoena_category.SelectedValue = dt.Rows[0]["subpoena_category"].ToString();    //傳票類別
                        send_station_scode.SelectedValue = dt.Rows[0]["send_station_scode"].ToString();
                        #endregion                        

                        #region 3.才件代收
                        if (!String.IsNullOrEmpty(dt.Rows[0]["pieces"].ToString()) || !IsNumeric(dt.Rows[0]["pieces"].ToString())) //件數
                        {
                            pieces.Text = dt.Rows[0]["pieces"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["plates"].ToString()) || !IsNumeric(dt.Rows[0]["plates"].ToString())) //板數
                        {
                            plates.Text = dt.Rows[0]["plates"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["cbm"].ToString()) || !IsNumeric(dt.Rows[0]["cbm"].ToString())) //才數
                        {
                            cbm.Text = dt.Rows[0]["cbm"].ToString();
                        }


                        if (!String.IsNullOrEmpty(dt.Rows[0]["collection_money"].ToString()) || !IsNumeric(dt.Rows[0]["collection_money"].ToString())) //$代收金
                        {
                            collection_money.Text = dt.Rows[0]["collection_money"].ToString();
                        }

                        if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_freight"].ToString()) || !IsNumeric(dt.Rows[0]["arrive_to_pay_freight"].ToString())) //到付運費
                        {
                            arrive_to_pay_freight.Text = dt.Rows[0]["arrive_to_pay_freight"].ToString();
                        }


                        if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_append"].ToString()) || !IsNumeric(dt.Rows[0]["arrive_to_pay_append"].ToString())) //到付追加
                        {
                            arrive_to_pay_append.Text = dt.Rows[0]["arrive_to_pay_append"].ToString();
                        }
                        #endregion

                        #region 特殊設定
                        //product_category.SelectedValue = dt.Rows[0]["product_category"].ToString();   //商品種類
                        //arrive_mobile.Text = dt.Rows[0]["arrive_mobile"].ToString();                  //收件人-手機1 
                        special_send.SelectedValue = dt.Rows[0]["special_send"].ToString();             //特殊配送                       

                        //指定日
                        if (!DateTime.TryParse(dt.Rows[0]["arrive_assign_date"].ToString(), out dt_tmp)) arrive_assign_date.Text = "";
                        else arrive_assign_date.Text = dt_tmp.ToString("yyyy/MM/dd");

                        time_period.SelectedValue = dt.Rows[0]["time_period"].ToString();              //時段
                        invoice_desc.Value = dt.Rows[0]["invoice_desc"].ToString();                    //說明    

                        //if (!int.TryParse(dt.Rows[0]["receipt_flag"].ToString(), out i_tmp)) i_tmp = 0;
                        //receipt_flag.Checked = i_tmp == 1 ? true : false; //是否回單

                        receipt_flag.Checked = Convert.ToBoolean(dt.Rows[0]["receipt_flag"]);        //是否回單
                        pallet_recycling_flag.Checked = Convert.ToBoolean(dt.Rows[0]["pallet_recycling_flag"]);        //是否棧板回收
                        

                        using (SqlCommand cmdp = new SqlCommand())
                        {
                            cmdp.Parameters.AddWithValue("@request_id", request_id);
                            cmdp.CommandText = " SELECT * FROM pallet_record With(Nolock) WHERE request_id = @request_id ";

                            using (DataTable dtp = dbAdapter.getDataTable(cmdp))
                            {

                                if (pallet_recycling_flag.Checked)
                                {
                                    //Pallet_type_div.Attributes.Add("style", "display:block");
                                    //Pallet_type_text.ReadOnly = true;
                                    //Pallet_type_text.Text = Func.GetRow("code_name", "tbItemCodes", "", " code_bclass = '2' and code_sclass = 'S8' and code_id=@code_id", "code_id", dt.Rows[0]["Pallet_type"].ToString());
                                    //Pallet_type.Value = dt.Rows[0]["Pallet_type"].ToString();
                                    TextBoxR.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["red"].ToString() : "";
                                    TextBoxG.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["green"].ToString() : "";
                                    TextBoxB.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["blue"].ToString() : "";
                                    TextBoxY.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["yellow"].ToString() : "";
                                    TextBoxP.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["plastic"].ToString() : "";
                                    TextBoxS.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["special"].ToString() : "";
                                    pallet_desc.Text = pallet_recycling_flag.Checked ? dtp.Rows[0]["special_desc"].ToString() : "";
                                    TextBoxR.Enabled = pallet_recycling_flag.Checked;
                                    TextBoxG.Enabled = pallet_recycling_flag.Checked;
                                    TextBoxB.Enabled = pallet_recycling_flag.Checked;
                                    TextBoxY.Enabled = pallet_recycling_flag.Checked;
                                    TextBoxP.Enabled = pallet_recycling_flag.Checked;
                                    TextBoxS.Enabled = pallet_recycling_flag.Checked;
                                    pallet_desc.Enabled = pallet_recycling_flag.Checked;

                                }


                            }
                        }
                            turn_board.Checked = dt.Rows[0]["turn_board"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["turn_board"]) : false;                        //翻版
                        upstairs.Checked = dt.Rows[0]["upstairs"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["upstairs"]) : false;                             //上樓
                        difficult_delivery.Checked = dt.Rows[0]["difficult_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["difficult_delivery"]) : false;//困配
                        turn_board_fee.Text = turn_board.Checked ? dt.Rows[0]["turn_board_fee"].ToString() : "";                        //翻版
                        upstairs_fee.Text = upstairs.Checked ? dt.Rows[0]["upstairs_fee"].ToString() : "";                            //上樓
                        difficult_fee.Text = difficult_delivery.Checked ? dt.Rows[0]["difficult_fee"].ToString() : "";                 //困配
                        turn_board_fee.Enabled = turn_board.Checked;
                        upstairs_fee.Enabled = upstairs.Checked;
                        difficult_fee.Enabled = difficult_delivery.Checked;

                        // 不顯示寄件人,地址
                        int showSender;
                        int.TryParse(dt.Rows[0]["custom_label"].ToString(), out showSender);
                        
                        if(showSender == 1) // 不顯示寄件人
                        {
                            chkshowsend.Checked = false;
                            chksend_address.Checked = false;
                        }
                        else if(showSender == 2) // 不顯示地址
                        {
                            chksend_address.Checked = false;
                        }
                        #endregion


                        /*新託運單:寄件框=原收件人框
                          收件框全清空
                          sub_check_number = "001"
                          唯讀→ 託運類別、 基本設定、才件代收、特殊設定
                          其餘可改
                         */


                        switch (i_type)
                        {
                            case 1:

                                #region 新增轉址
                                plCancel.Visible = false;
                                //sub_check_number = 001、002 ...
                                if (!int.TryParse(dt.Rows[0]["sub_check_number"].ToString(), out i_tmp)) i_tmp = 0;
                                lbl_sub_check_number.Text = String.Format("{0:000}", (i_tmp + 1));


                                //收件人-清空
                                receive_tel1.Text = "";               //電話1
                                receive_tel1_ext.Text = "";           //電話分機
                                receive_tel2.Text = "";               //電話2
                                receive_contact.Text = "";            //收件人
                                receive_city.SelectedValue = "";      //收件地址-縣市     
                                receive_area.SelectedValue = "";      //收件地址-鄉鎮市區  
                                area_arrive_code.SelectedValue = "";  //到著碼                                
                                cbarrive.Checked = false;             //到站領貨 
                                receive_address.Text = "";

                                //寄件框 填 原收件人框
                                send_contact.Text = dt.Rows[0]["receive_contact"].ToString();
                                send_tel.Text = dt.Rows[0]["receive_tel1"].ToString();
                                send_city.SelectedValue = dt.Rows[0]["receive_city"].ToString();
                                city_SelectedIndexChanged(send_city, null);
                                send_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();
                                send_address.Text = dt.Rows[0]["receive_address"].ToString();

                                ReadOnlySet(false);

                                #endregion

                                break;
                            default:
                                #region 託運單修改、瀏覽

                                #region  銷單資訊
                                if (dt.Rows[0]["cancel_date"] == DBNull.Value || (dt.Rows[0]["cancel_date"].ToString() == ""))
                                {
                                    plCancel.Visible = true;
                                    btnsave.Visible = true;
                                }
                                else
                                {
                                    plCancel.Visible = false;
                                    btnsave.Visible = false;
                                    div_cancel.Visible = true;
                                    liCancel_date.Text = Convert.ToDateTime(dt.Rows[0]["cancel_date"]).ToString("yyyy/MM/dd");

                                    using (SqlCommand cmdcancel = new SqlCommand())
                                    {

                                        cmdcancel.CommandText = @"Select delete_user,record_memo, b.user_name from ttDeliveryRequestsRecord a With(Nolock) 
                                                                  left join tbAccounts b on a.delete_user  = b.account_code 
                                                                  where record_action = 'D' and request_id  = @request_id
                                                                  and create_date >= dateadd( MONTH , -6, @cdate)";
                                        cmdcancel.Parameters.Add("@request_id", System.Data.SqlDbType.Int).Value = request_id;
                                        cmdcancel.Parameters.Add("@cdate", System.Data.SqlDbType.DateTime).Value = dt.Rows[0]["cdate"];

                                        using (DataTable dtcancel = dbAdapter.getDataTable(cmdcancel))
                                        {
                                            if (dtcancel.Rows.Count > 0)
                                            {
                                                liCancel_psn.Text = dtcancel.Rows[0]["delete_user"].ToString() + dtcancel.Rows[0]["user_name"].ToString();
                                                liCancel_memo.Text = dtcancel.Rows[0]["record_memo"].ToString();
                                            }
                                        }
                                    }

                                }
                                #endregion

                                lbl_sub_check_number.Text = "000";

                                #region 2. 收件人
                                receive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();                       //電話1
                                receive_tel1_ext.Text = dt.Rows[0]["receive_tel1_ext"].ToString();               //電話分機
                                receive_tel2.Text = dt.Rows[0]["receive_tel2"].ToString();                       //電話2
                                receive_contact.Text = dt.Rows[0]["receive_contact"].ToString();                 //收件人
                                receive_city.SelectedValue = dt.Rows[0]["receive_city"].ToString();              //收件地址-縣市     
                                city_SelectedIndexChanged(receive_city, null);
                                receive_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();              //收件地址-鄉鎮市區  
                                                                                                                 //receive_area_SelectedIndexChanged(null, null);



                                //到站領貨 0:否、1:是
                                if (!int.TryParse(dt.Rows[0]["receive_by_arrive_site_flag"].ToString(), out i_tmp)) i_tmp = 0;
                                cbarrive.Checked = i_tmp == 1 ? true : false;

                                if (cbarrive.Checked)
                                {
                                    receive_address.Text = dt.Rows[0]["arrive_address"].ToString();                  //到著站址
                                }
                                else
                                {
                                    receive_address.Text = dt.Rows[0]["receive_address"].ToString();                 //收件地址-路街巷弄號
                                }

                                #endregion

                                #region 4.寄件人
                                send_contact.Text = dt.Rows[0]["send_contact"].ToString();                     //寄件人
                                send_tel.Text = dt.Rows[0]["send_tel"].ToString();                             //寄件人電話
                                send_city.SelectedValue = dt.Rows[0]["send_city"].ToString();                  //寄件人地址-縣市
                                city_SelectedIndexChanged(send_city, null);
                                send_area.SelectedValue = dt.Rows[0]["send_area"].ToString();                  //寄件人地址-鄉鎮市區
                                send_address.Text = dt.Rows[0]["send_address"].ToString();                     // 寄件人地址 - 號街巷弄號
                                supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();                    //配送商代碼
                                supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();                    //配送商名稱
                                #endregion

                                #region 到著站簡碼
                                SqlCommand cmd8 = new SqlCommand();
                                cmd8.Parameters.AddWithValue("@supplier_code_sel", supplier_code_sel.ToString());
                                cmd8.Parameters.AddWithValue("@hctcode", "001");
                                cmd8.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1 and supplier_code =@supplier_code_sel
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
                                                                                        order by supplier_code
                                            end");

                                area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
                                area_arrive_code.DataValueField = "supplier_code";
                                area_arrive_code.DataTextField = "showname";
                                area_arrive_code.DataBind();
                                area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
                                #endregion

                                area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();        //到著碼
                                if (area_arrive_code.SelectedValue == "" && dt.Rows[0]["area_arrive_code"].ToString() != "")
                                {
                                    area_arrive_code.Items.Add(new ListItem(dt.Rows[0]["area_arrive_code"].ToString(), dt.Rows[0]["area_arrive_code"].ToString()));
                                    area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();      //到著碼
                                }



                                if (!String.IsNullOrEmpty(dt.Rows[0]["supplier_fee"].ToString()) || !IsNumeric(dt.Rows[0]["supplier_fee"].ToString())) //貨件費用
                                {
                                    i_supplier_fee.Text = dt.Rows[0]["supplier_fee"].ToString();
                                }

                                if (!String.IsNullOrEmpty(dt.Rows[0]["csection_fee"].ToString()) || !IsNumeric(dt.Rows[0]["csection_fee"].ToString())) //C配運費
                                {
                                    i_csection_fee.Text = dt.Rows[0]["csection_fee"].ToString();
                                }

                                if (!String.IsNullOrEmpty(dt.Rows[0]["remote_fee"].ToString()) || !IsNumeric(dt.Rows[0]["remote_fee"].ToString()))     //偏遠區加價
                                {
                                    i_remote_fee.Text = dt.Rows[0]["remote_fee"].ToString();
                                }


                                ReadOnlySet(true);

                                #endregion
                                break;
                        }

                        #region mark session
                        //arrive_email.Text = dt.Rows[0]["arrive_email"].ToString();                    //收件人-電子郵件
                        //invoice_memo.SelectedValue = dt.Rows[0]["invoice_memo"].ToString();           //備註
                        //donate_invoice_flag.Checked = Convert.ToBoolean(dt.Rows[0]["donate_invoice_flag"]);              //是否發票捐贈 
                        //electronic_invoice_flag.Checked  = Convert.ToBoolean(dt.Rows[0]["electronic_invoice_flag"]);     //是否為電子發票
                        //uniform_numbers.Text = dt.Rows[0]["uniform_numbers"].ToString();              //統一編號
                        #endregion
                    }

                }
            }

        }
        catch (Exception ex)
        {
            string strErr = string.Empty;
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "查詢及補單作業-" + Request.RawUrl + strErr + ": " + ex.ToString();


            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");

            //導至錯誤頁面
            String _theUrl = string.Format("Oops.aspx?err_title={0}&err_msg={1}", "系統發生錯誤!", "系統資訊異常，請通知系統管理人員。");
            Server.Transfer(_theUrl, true);
        }
    }

    /// <summary>頁面項目開放編輯與否-針對託運單修改 vs 託運轉址</summary>
    /// <param name="status"></param>
    protected void ReadOnlySet(Boolean status)
    {
        //託運類別 
        rbcheck_type.Enabled = status;

        //基本設定
        lbcheck_number.Enabled = status;
        order_number.Enabled = status;
        check_type.Enabled = status;
        receive_customer_code.Enabled = status;
        subpoena_category.Enabled = status;

        //才件代收
        pieces.Enabled = status;
        plates.Enabled = status;
        cbm.Enabled = status;
        collection_money.Enabled = status;
        arrive_to_pay_freight.Enabled = status;
        arrive_to_pay_append.Enabled = status;

        //特殊設定
        //product_category.Enabled = status;
        //arrive_mobile.Enabled = status;
        special_send.Enabled = status;
        arrive_assign_date.Enabled = status;
        time_period.Enabled = status;
        invoice_desc.Disabled = !status;
        receipt_flag.Enabled = status;
        pallet_recycling_flag.Enabled = status;
        turn_board.Enabled = status;
        upstairs.Enabled = status;
        difficult_delivery.Enabled = status;

    }

    /// <summary>初始資料設定</summary>
    protected void DefaultSet()
    {
        #region 權限
        manager_type = Session["manager_type"].ToString(); //管理單位類別   
        supplier_code = Session["master_code"].ToString();
        if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
        {
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
        }

        #endregion

        #region 郵政縣市
        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        receive_city.DataSource = dbAdapter.getDataTable(cmd1);
        receive_city.DataValueField = "city";
        receive_city.DataTextField = "city";
        receive_city.DataBind();
        receive_city.Items.Insert(0, new ListItem("請選擇", ""));

        send_city.DataSource = dbAdapter.getDataTable(cmd1);
        send_city.DataValueField = "city";
        send_city.DataTextField = "city";
        send_city.DataBind();
        send_city.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

        #region 客代編號
        rbcheck_type_SelectedIndexChanged(null, null);
        #endregion

        #region 託運類別
        SqlCommand cmd2 = new SqlCommand();
        cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
        check_type.DataSource = dbAdapter.getDataTable(cmd2);
        check_type.DataValueField = "code_id";
        check_type.DataTextField = "code_name";
        check_type.DataBind();
        #endregion

        #region 傳票類別
        SqlCommand cmd3 = new SqlCommand();
        cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 order by code_id ";
        subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
        subpoena_category.DataValueField = "code_id";
        subpoena_category.DataTextField = "code_name";
        subpoena_category.DataBind();
        #endregion

        #region 商品類別
        //SqlCommand cmd4 = new SqlCommand();
        //cmd4.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S3' and active_flag = 1 ";
        //product_category.DataSource = dbAdapter.getDataTable(cmd4);
        //product_category.DataValueField = "code_id";
        //product_category.DataTextField = "code_name";
        //product_category.DataBind();
        #endregion

        #region 特殊配送
        SqlCommand cmd5 = new SqlCommand();
        cmd5.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S4' and active_flag = 1 ";
        special_send.DataSource = dbAdapter.getDataTable(cmd5);
        special_send.DataValueField = "code_id";
        special_send.DataTextField = "code_name";
        special_send.DataBind();
        #endregion

        #region 配送時段
        SqlCommand cmd6 = new SqlCommand();
        cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
        time_period.DataSource = dbAdapter.getDataTable(cmd6);
        time_period.DataValueField = "code_id";
        time_period.DataTextField = "code_name";
        time_period.DataBind();
        time_period.SelectedValue = "不指定";
        #endregion

        #region 發送站
        SqlCommand cmd8 = new SqlCommand();
        cmd8.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        and cross_region = 1
                                                                                        order by supplier_code
                                            end");
        send_station_scode.DataSource = dbAdapter.getDataTable(cmd8);
        send_station_scode.DataValueField = "supplier_code";
        send_station_scode.DataTextField = "showname";
        send_station_scode.DataBind();
        #endregion

        //#region 到著站簡碼
        //SqlCommand cmd8 = new SqlCommand();
        ////// cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 ");
        ////cmd8.CommandText = string.Format(@"select distinct arr.supplier_code,  sup.supplier_code + ' '+  sup.supplier_name as showname 
        ////                                        from ttArriveSites arr with(nolock)
        ////                                        left join   tbSuppliers sup with(nolock) on arr.supplier_code =  sup.supplier_code and sup.active_flag = 1 
        ////                                        where arr.supplier_code <> '' and arr.supplier_code is not null
        ////                                        ");
        //cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 and show_trans = 1 order by supplier_code");
        //area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
        //area_arrive_code.DataValueField = "supplier_code";
        //area_arrive_code.DataTextField = "showname";
        //area_arrive_code.DataBind();
        //area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
        //#endregion


    }
    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (supplier_code != "")
        {
            switch (manager_type)
            {
                case "0":
                case "1":
                case "4":
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                    break;
                default:
                    cmd2.Parameters.AddWithValue("@master_code", supplier_code);
                    wherestr = " and master_code like ''+@master_code+'%' ";
                    break;
            }
        }
        cmd2.CommandText = string.Format(@"select customer_code , customer_code+ '-' + customer_shortname as name  from tbCustomers With(Nolock) where stop_shipping_code = '0' and pricing_code = '" + rbcheck_type.SelectedValue + "'  {0}  order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
        #endregion
        Clear();

    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkshowsend.Checked)
        {
            //顯示寄件人資料
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_shortname , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_shortname, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + customer_code.Text + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_shortname"].ToString();   //名稱
                send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();      //區配code
                supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();      //區配name

                bool individualfee = Convert.ToBoolean(dt.Rows[0]["individual_fee"]);   //是否個別計價
                bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
                if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                {
                    individual_fee.Style.Add("display", "block");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
                else
                {
                    individual_fee.Style.Add("display", "none");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
            }
        }
    }

    protected void cbarrive_CheckedChanged(object sender, EventArgs e)
    {
        if (cbarrive.Checked && area_arrive_code.SelectedValue != "")
        {
            receive_address.Text = area_arrive_code.SelectedItem.Text + "站址";
        }

        //area_arrive_code.Enabled = cbarrive.Checked;

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        //#region 到著站簡碼
        //area_arrive_code.Items.Clear();

        //using (SqlCommand cmd7 = new SqlCommand())
        //{

        //    cmd7.CommandText = string.Format(@"select A.supplier_code , A.supplier_code  + ' '+  B.supplier_name as showname  from ttArriveSites A With(Nolock) 
        //                                       left join tbSuppliers B  With(Nolock) on A.supplier_code  = B.supplier_code 
        //                                       where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
        //    area_arrive_code.DataSource = dbAdapter.getDataTable(cmd7);
        //    area_arrive_code.DataValueField = "supplier_code";
        //    area_arrive_code.DataTextField = "showname";
        //    area_arrive_code.DataBind();
        //}
        //if (area_arrive_code.Items.Count > 0) area_arrive_code.SelectedIndex = 0;

        //#endregion
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                                   left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                   where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        area_arrive_code.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
                    }
                    catch { }

                }
            }
        }
        cbarrive_CheckedChanged(null, null);
    }

    protected void send_station_scode_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlCommand cmd7 = new SqlCommand())
        {
            cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                                   left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                   where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", send_city.SelectedValue, send_area.SelectedValue);
            using (DataTable dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    try
                    {
                        send_station_scode.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
                    }
                    catch { }

                }
            }
        }
    }

    protected void area_arrive_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbarrive_CheckedChanged(null, null);
    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        dlarea.Items.Clear();
        dlarea.Items.Add(new ListItem("請選擇", ""));

        if (dlcity.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
    }

    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        customer_code.Text = dlcustomer_code.SelectedValue;
        CheckBox1_CheckedChanged(null, null);
        divReceiver.Visible = false;
        New_List.DataSource = null;
        New_List.DataBind();


    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void Clear()
    {
        customer_code.Text = "";
        send_contact.Text = "";   //名稱
        send_tel.Text = "";           // 電話
        send_city.SelectedValue = "";   //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";   //出貨地址-鄉鎮市區
        send_address.Text = "";        //出貨地址-路街巷弄號
        lbcustomer_name.Text = "";
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        lbErrQuant.Text = "";
        int ttpieces = 0;
        int ttplates = 0;
        int ttcbm = 0;
        int ttcollection_money = 0;
        int ttarrive_to_pay = 0;
        int ttarrive_to_append = 0;
        int remote_fee = 0;
        int supplier_fee = 0;  //配送費用
        int cscetion_fee = 0;  //C配運價
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)


        /* 20170727 託運類別 ... modify by lisa
        * 論板時、論小板時：【板數】與【件數】一定要提示填入數字
        * 論才時：【板數】與【件數】及【才數】皆要填入
        */

        if (!int.TryParse(pieces.Text, out ttpieces)) ttpieces = 0;
        if (!int.TryParse(plates.Text, out ttplates)) ttplates = 0;
        if (!int.TryParse(cbm.Text, out ttcbm)) ttcbm = 0;

        bool individual = individual_fee.Style.Value.IndexOf("display:block") > -1;
        int i_tmp;
        switch (rbcheck_type.SelectedValue)
        {
            case "01":
                if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入板數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }
                break;

            case "04":
                if (string.IsNullOrWhiteSpace(plates.Text) || string.IsNullOrWhiteSpace(pieces.Text))
                {
                    lbErrQuant.Text = "請輸入板數及件數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入板數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "02":
                if (string.IsNullOrWhiteSpace(pieces.Text))
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "03":
                if (string.IsNullOrWhiteSpace(cbm.Text)
                    || string.IsNullOrWhiteSpace(plates.Text)
                    || string.IsNullOrWhiteSpace(pieces.Text)
                    )
                {
                    lbErrQuant.Text = "請輸入板數、件數及才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入板數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }
                break;
        }

        if (individual && i_supplier_fee.Text == "")
        {
            lbErrQuant.Text = "請輸入貨件運費";
            i_supplier_fee.Focus();
            return;
        }

        //花蓮、台東地區 強制輸入才數
        if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        if (((receive_city.SelectedValue == "花蓮縣") || (receive_city.SelectedValue == "臺東縣")) && (i_tmp <= 0))
        {
            lbErrQuant.Text = "請輸入才數";
            cbm.Focus();
            lbErrQuant.Visible = true;
            return;
        }
        if (pallet_recycling_flag.Checked)
        {
            if (string.IsNullOrWhiteSpace(TextBoxB.Text) && string.IsNullOrWhiteSpace(TextBoxR.Text) && string.IsNullOrWhiteSpace(TextBoxG.Text) && string.IsNullOrWhiteSpace(TextBoxY.Text) && string.IsNullOrWhiteSpace(TextBoxS.Text) && string.IsNullOrWhiteSpace(TextBoxP.Text))
            {
                pallet_alert.Text = "請輸入板數";
                pallet_recycling_flag.Focus();
                pallet_alert.Visible = true;
                TextBoxR.Enabled = true;
                TextBoxG.Enabled = true;
                TextBoxB.Enabled = true;
                TextBoxY.Enabled = true;
                TextBoxP.Enabled = true;
                TextBoxS.Enabled = true;

                return;

            }
            if ((!string.IsNullOrWhiteSpace(TextBoxS.Text)) && (string.IsNullOrWhiteSpace(pallet_desc.Text)))
            {
                if (TextBoxS.Text != "0")
                {
                    pallet_alert2.Text = "請輸入規格";
                    pallet_desc.Focus();
                    pallet_alert2.Visible = true;

                    return;
                }
            }
        }

        if (lbErrQuant.Text != "")
        {
            lbErrQuant.Visible = true;
        }
        else
        {
            int i_type = 0;
            if (Request.QueryString["r_type"] == null || !int.TryParse(Request.QueryString["r_type"].ToString(), out i_type)) i_type = 0;

            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@pricing_type", rbcheck_type.SelectedValue);                     //計價模式 (01:論板、02:論件、03論才、04論小板)
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
            cmd.Parameters.AddWithValue("@check_number", lbcheck_number.Text);                            //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
            cmd.Parameters.AddWithValue("@order_number", order_number.Text);                              //訂單號碼
            cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
            cmd.Parameters.AddWithValue("@receive_customer_code", receive_customer_code.Text);            //收貨人編號
            cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
            cmd.Parameters.AddWithValue("@receive_tel1", receive_tel1.Text);                              //電話1
            cmd.Parameters.AddWithValue("@receive_tel1_ext", receive_tel1_ext.Text.ToString());           //電話分機
            cmd.Parameters.AddWithValue("@receive_tel2", receive_tel2.Text);                              //電話2
            cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
            cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
            cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
            if (!individual)
            {
                remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
            }
            else
            {
                if (int.TryParse(i_remote_fee.Text, out i_tmp)) remote_fee = i_tmp;
            }
            cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
            cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code.SelectedValue);             //到著碼
            cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", Convert.ToInt16(cbarrive.Checked));                //到站領貨 0:否、1:是
            if (cbarrive.Checked)
            {
                cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
            }

            // 是否顯示寄件人資料
            if (!chkshowsend.Checked) // 不顯示寄件人
            {
                cmd.Parameters.AddWithValue("@custom_label", 1);
            }
            else if (!chksend_address.Checked) // 不顯示地址
            {
                cmd.Parameters.AddWithValue("@custom_label", 2);
            }
            else
            {
                cmd.Parameters.AddWithValue("@custom_label", DBNull.Value);
            }

            try
            {
                ttcollection_money = Convert.ToInt32(collection_money.Text);
            }
            catch { }

            try
            {
                ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
            }
            catch { }

            try
            {
                ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
            }
            catch { }

            cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
            cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
            cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數


            switch (subpoena_category.SelectedValue)
            {
                case "11"://元付
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                //到付運費
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);              //到付追加
                    break;
                case "21"://到付
                    cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                 //到付運費
                    break;
                case "25"://元付-到付追加
                    cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                //到付追加
                    break;
                case "41"://代收貨款
                    cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                     //代收金
                    break;
            }

            //cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
            //cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
            //cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加

            cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
            cmd.Parameters.AddWithValue("@send_tel", send_tel.Text.ToString());                           //寄件人電話
            cmd.Parameters.AddWithValue("@send_station_scode", send_station_scode.Text.ToString());
            cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
            cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
            cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
            cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
            cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
            cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
            //cmd.Parameters.AddWithValue("@arrive_mobile", arrive_mobile.Text.ToString());               //收件人-手機1  
            cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
            cmd.Parameters.AddWithValue("@invoice_memo", "");                                             //備註
            cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value.Trim());                      //說明
            //cmd.Parameters.AddWithValue("@product_category", product_category.SelectedValue.ToString());//商品種類
            cmd.Parameters.AddWithValue("@special_send", special_send.SelectedValue.ToString());          //特殊配送
            DateTime date;
            cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(arrive_assign_date.Text, out date) ? (object)date : DBNull.Value);                  //指定日
            cmd.Parameters.AddWithValue("@time_period", time_period.SelectedValue.ToString());            //時段
            cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
            cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
            if (pallet_recycling_flag.Checked)
            {
                cmd.Parameters.AddWithValue("@Pallet_type", Pallet_type.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Pallet_type", "");
            }
            cmd.Parameters.AddWithValue("@turn_board", Convert.ToInt16(turn_board.Checked));                               //翻版
            cmd.Parameters.AddWithValue("@upstairs", Convert.ToInt16(upstairs.Checked));                                   //上樓
            cmd.Parameters.AddWithValue("@difficult_delivery", Convert.ToInt16(difficult_delivery.Checked));               //困配
            cmd.Parameters.AddWithValue("@print_date", DateTime.TryParse(Shipments_date.Text, out date) ? (object)date : DBNull.Value);   //出貨日期
            if (int.TryParse(turn_board_fee.Text, out i_tmp) && turn_board.Checked)
            {
                cmd.Parameters.AddWithValue("@turn_board_fee", i_tmp.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@turn_board_fee", 0);
            }

            if (int.TryParse(upstairs_fee.Text, out i_tmp) && upstairs.Checked)
            {
                cmd.Parameters.AddWithValue("@upstairs_fee", i_tmp.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@upstairs_fee", 0);
            }

            if (int.TryParse(difficult_fee.Text, out i_tmp) && difficult_delivery.Checked)
            {
                cmd.Parameters.AddWithValue("@difficult_fee", i_tmp.ToString());
            }
            else
            {
                cmd.Parameters.AddWithValue("@difficult_fee", 0);
            }

            cmd.Parameters.AddWithValue("@sub_check_number", lbl_sub_check_number.Text);            //次貨號
            if (individual)
            {
                if (int.TryParse(i_supplier_fee.Text, out i_tmp))
                {
                    cmd.Parameters.AddWithValue("@supplier_fee", i_tmp.ToString());
                    cmd.Parameters.AddWithValue("@total_fee", i_tmp.ToString());
                }
                if (int.TryParse(i_csection_fee.Text, out i_tmp))
                    cmd.Parameters.AddWithValue("@csection_fee", i_tmp.ToString());
            }

            if (i_type > 0)
            {
                cmd.Parameters.AddWithValue("@add_transfer", 1);                                              //是否轉址
                cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);

                if (!individual)
                {
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                    if (result > 0)
                    {
                        #region 回寫到託運單的費用欄位
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.CommandText = "usp_GetShipFeeByRequestId";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@request_id", result.ToString());
                        using (DataTable dt = dbAdapter.getDataTable(cmd2))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                if (status_code == 1)
                                {
                                    supplier_fee = Convert.ToInt32(dt.Rows[0]["supplier_fee"]);       //配送費用
                                    cscetion_fee = Convert.ToInt32(dt.Rows[0]["cscetion_fee"]);       //C配運價                                

                                    //回寫到託運單的費用欄位
                                    using (SqlCommand cmd3 = new SqlCommand())
                                    {
                                        cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                        cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                        cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                        cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", result.ToString());
                                        cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd3);
                                        }
                                        catch (Exception ex)
                                        {
                                            string strErr = string.Empty;
                                            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                            strErr = "一筆式託運單-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                            //錯誤寫至Log
                                            PublicFunction _fun = new PublicFunction();
                                            _fun.Log(strErr, "S");
                                        }
                                    }
                                }
                            }
                        }

                        #endregion
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增成功');parent.$.fancybox.close();parent.location.reload(true);</script>", false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('資訊異常，請重新確認');parent.$.fancybox.close();</script>", false);
                    }
                }

            }
            else
            {
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code_sel);                             //配送商代碼
                cmd.Parameters.AddWithValue("@supplier_name", supplier_name_sel);                             //配送商名稱
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 


                #region 回寫到託運單的費用欄位
                if (!individual)
                {
                    if (request_id != "")
                    {
                        SqlCommand cmd2 = new SqlCommand();
                        cmd2.CommandText = "usp_GetShipFeeByRequestId";
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                        using (DataTable dt = dbAdapter.getDataTable(cmd2))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                if (status_code == 1)
                                {
                                    supplier_fee = dt.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["supplier_fee"]) : 0;         //配送費用
                                    cscetion_fee = dt.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cscetion_fee"]) : 0;         //C配運價
                                                                                                                                                         //回寫到託運單的費用欄位
                                    using (SqlCommand cmd3 = new SqlCommand())
                                    {
                                        cmd.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                        cmd.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                        cmd.Parameters.AddWithValue("@total_fee", supplier_fee);
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion
                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);   //修改
                dbAdapter.execNonQuery(cmd);

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存完成');parent.$.fancybox.close();parent.location.reload(true);</script>", false);
            }

            #region 常用收件人通訊錄
            if (cbSaveReceive.Checked && receiver_id.Text == "")
            {
                if (!string.IsNullOrEmpty(customer_code.Text.Trim()) && !string.IsNullOrEmpty(receive_contact.Text.Trim()))
                {
                    //先判斷，如果有姓名、編號、地址、已存在就不再新增
                    using (SqlCommand cmd5 = new SqlCommand())
                    {

                        cmd5.Parameters.AddWithValue("@customer_code", customer_code.Text.Trim());
                        cmd5.Parameters.AddWithValue("@receiver_code", receiver_code.Text.Trim());
                        cmd5.Parameters.AddWithValue("@receiver_name", receive_contact.Text.Trim());
                        cmd5.Parameters.AddWithValue("@address_city", receive_city.SelectedValue);
                        cmd5.Parameters.AddWithValue("@address_area", receive_area.SelectedValue);
                        cmd5.Parameters.AddWithValue("@address_road", receive_address.Text.Trim());


                        cmd5.CommandText = string.Format(@"Select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  and A.receiver_code =@receiver_code and  A.receiver_name=@receiver_name 
                                          and A.customer_code =@customer_code and  A.address_city =@address_city and  address_area = @address_area 
                                          and address_road = @address_road ");
                        using (DataTable dt = dbAdapter.getDataTable(cmd5))
                        {
                            if (dt == null || dt.Rows.Count == 0)
                            {
                                SqlCommand cmd4 = new SqlCommand();
                                cmd4.Parameters.AddWithValue("@customer_code", customer_code.Text);                     //客戶代碼
                                cmd4.Parameters.AddWithValue("@receiver_code", receiver_code.Text);                     //收貨人代碼
                                cmd4.Parameters.AddWithValue("@receiver_name", receive_contact.Text);                   //收貨人名稱
                                cmd4.Parameters.AddWithValue("@tel", receive_tel1.Text);                                //電話
                                cmd4.Parameters.AddWithValue("@tel_ext", receive_tel1_ext.Text);                        //分機
                                cmd4.Parameters.AddWithValue("@tel2", receive_tel2.Text);                               //電話2
                                cmd4.Parameters.AddWithValue("@address_city", receive_city.SelectedValue.ToString());   //地址-縣市
                                cmd4.Parameters.AddWithValue("@address_area", receive_area.SelectedValue.ToString());   //地址 - 區域
                                cmd4.Parameters.AddWithValue("@address_road", receive_address.Text);                    //地址-路段
                                cmd4.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                                cmd4.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                                cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                                cmd4.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                                cmd4.CommandText = dbAdapter.SQLdosomething("tbReceiver", cmd4, "insert");
                                try
                                {
                                    dbAdapter.execNonQuery(cmd4);
                                }
                                catch (Exception ex)
                                {
                                    string strErr = string.Empty;
                                    if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                    strErr = "一筆式託運單-儲存常用通訊人" + Request.RawUrl + strErr + ": " + ex.ToString();


                                    //錯誤寫至Log
                                    PublicFunction _fun = new PublicFunction();
                                    _fun.Log(strErr, "S");
                                }
                            }
                        }

                    }



                }
            }
            #endregion



            using (SqlCommand cmd3 = new SqlCommand())     //更新回收棧板
            
            {
                
               

                cmd3.Parameters.AddWithValue("@request_id", request_id);
                cmd3.Parameters.AddWithValue("@red", TextBoxR.Text);
                cmd3.Parameters.AddWithValue("@green", TextBoxG.Text);
                cmd3.Parameters.AddWithValue("@blue", TextBoxB.Text);
                cmd3.Parameters.AddWithValue("@yellow", TextBoxY.Text);
                cmd3.Parameters.AddWithValue("@plastic", TextBoxP.Text);
                cmd3.Parameters.AddWithValue("@special", TextBoxS.Text);
                cmd3.Parameters.AddWithValue("@special_desc", pallet_desc.Text);

                DataTable dta;
                cmd3.CommandText = @"select request_id from pallet_record With(Nolock) 
                                                where request_id = @request_id ";
                dta = dbAdapter.getDataTable(cmd3);
                if (dta.Rows.Count > 0)
                {
                    cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id);
                    cmd3.CommandText = dbAdapter.genUpdateComm("pallet_record", cmd3);
                    dbAdapter.execNonQuery(cmd3);
                }

                else
                {

                    cmd3.CommandText = dbAdapter.genInsertComm("pallet_record", true, cmd3);
                    dbAdapter.execNonQuery(cmd3);
                }

            }

            return;
        }

    }

    protected void search_Click(object sender, EventArgs e)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                if (keyword.Text != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_code", keyword.Text);
                    cmd.Parameters.AddWithValue("@receiver_name", keyword.Text);
                    strWhereCmd += " and (A.receiver_code like '%'+@receiver_code+'%' or A.receiver_name like '%'+@receiver_name+'%') ";
                }

                if (customer_code.Text != "")
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code.Text.Length >= 7 ? customer_code.Text.Substring(0, 7) : customer_code.Text);
                    strWhereCmd += " and (A.customer_code like ''+@customer_code+'%') ";
                }
                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List.DataSource = dt;
                    New_List.DataBind();
                }
            }
        }

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.receiver_id=@receiver_id";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                // DT.Rows[0]["customer_code"].ToString().Trim();
                //DT.Rows[0]["receiver_code"].ToString().Trim();
                receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                city_SelectedIndexChanged(receive_city, null);
                receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                city_SelectedIndexChanged(send_city, null);
                receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                divReceiver.Visible = false;
            }
        }

    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = false;
    }

    protected void btnRecsel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = true;
        search_Click(null, null);
    }

    protected void receive_contact_TextChanged(object sender, EventArgs e)
    {
        if (receive_contact.Text != "")
        {
            entertry("receive_contact");
        }
    }

    protected void receiver_code_TextChanged(object sender, EventArgs e)
    {
        if (receiver_code.Text != "")
        {
            entertry("receiver_code");

        }
    }

    protected void entertry(string control)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                switch (control)
                {
                    case "receive_contact":
                        cmd.Parameters.AddWithValue("@receiver_name", receive_contact.Text);
                        strWhereCmd += " and  A.receiver_name like '%'+@receiver_name+'%' ";
                        break;
                    case "receiver_code":
                        cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
                        strWhereCmd += " and  A.receiver_code like '%'+@receiver_code+'%' ";
                        break;
                }
                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        if (DT.Rows.Count == 1)
                        {
                            receiver_id.Text = "";
                            receiver_code.Text = "";
                            receive_contact.Text = "";
                            receive_tel1.Text = "";
                            receive_tel1_ext.Text = "";
                            receive_tel2.Text = "";
                            receive_city.SelectedValue = "";
                            receive_area.SelectedValue = "";
                            receive_address.Text = "";

                            receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                            receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                            receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                            receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                            receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                            receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                            receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                            city_SelectedIndexChanged(receive_city, null);
                            receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                            receive_area_SelectedIndexChanged(null, null);
                            receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                            divReceiver.Visible = false;
                        }
                        else
                        {
                            New_List.DataSource = DT;
                            New_List.DataBind();
                            divReceiver.Visible = true;
                        }

                    }

                }
            }
        }

    }


}