﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterDispatch.master" AutoEventWireup="true" CodeFile="dispatch1_3.aspx.cs" Inherits="dispatch1_3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $("#custom_table [id*=chkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#custom_table [id*=chkRow]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkRow]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });
            $("#custom_table [id*=chkRow]").click(function () {
                if ($("#custom_table [id*=chkRow]").length == $("#custom_table [id*=chkRow]:checked").length) {
                    $("#custom_table [id*=chkHeader]").prop("checked", true);
                } else {
                    $("#custom_table [id*=chkHeader]").prop("checked", false);
                }
                $(".chkcount").html($("#custom_table [id*=chkRow]:checkbox:checked").length.toString());
            });

        });
        $(document).ready(function () {

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 15;
                pageParts.slice(perPage).hide();
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText: '上一頁',
                    nextText: '下一頁',
                    onPageClick: function (pageNum) {
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });

            var _width = $(document).width() * 0.8;  //850;
            var _height = $(document).height() * 0.8;//300;

            $("#editclick").fancybox({
                'width': _width,
                'height': _height,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'type': 'iframe',
                'onClosed': function () {
                    parent.location.reload(true);
                }
            });
        });


        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }

        function OpenLinkByFancyBox(the_url, df_width, df_height) {
            if ($.trim(the_url) != "") {

                if (the_url.indexOf('?') >= 0) {
                    the_url = the_url + '&r=' + Math.random();
                } else {
                    the_url = the_url + '?r=' + Math.random();
                }

                var _width = $(document).width() * 0.8;  //850;
                var _height = $(document).height() * 0.8;//300;
                if (typeof (df_width) != "undefined") {
                    _width = parseInt(df_width);
                }
                if (typeof (df_height) != "undefined") {
                    _height = parseInt(df_height);
                }

                $.fancybox({
                    'type': 'iframe',
                    'width': _width,
                    'height': _height,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'href': the_url
                });
            }
        }

    </script>

    <style>
        ._th {
            text-align: center;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .modal-dialog {
            width: 1200px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="modal fade" id="modal-default_01">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>

                                        <div>
                                            <table class="table table-striped table-bordered templatemo-user-table" />
                                            <tr class="tr-only-hide">
                                                <th class="_th">計價模式</th>
                                                <th class="_th">發送日期</th>
                                                <th class="_th">貨號</th>
                                                <th class="_th">出貨客戶</th>
                                                <th class="_th">出貨地點</th>
                                                <th class="_th">出貨板數</th>
                                                <th class="_th">出貨件數</th>
                                                <th class="_th">收件人</th>
                                                <th class="_th">配送區配商 </th>

                                            </tr>
                                            <asp:Repeater ID="rp_TaskList" runat="server">
                                                <ItemTemplate>
                                                    <tr>

                                                        <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pricing_type_name").ToString())%></td>
                                                        <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.print_date","{0:yyyy/MM/dd}").ToString())%></td>
                                                        <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.check_number").ToString())%></td>
                                                        <td data-th="出貨客戶"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                                                        <td data-th="出貨地點"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.send_address").ToString())%></td>
                                                        <td data-th="出貨板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                                        <td data-th="出貨件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.pieces").ToString())%> </td>
                                                        <td data-th="收件人"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receive_contact").ToString())%></td>
                                                        <td data-th="配送區配商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (rp_TaskList.Items.Count == 0)
                                                {%>
                                            <tr>
                                                <td colspan="9" style="text-align: center">尚無資料</td>
                                            </tr>
                                            <% } %>
                </table>
                                        </div>


                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="modal-default_02">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>

                                        <div>
                                            <table class="table table-striped table-bordered templatemo-user-table" />
                                            <tr class="tr-only-hide">
                                                <th class="_th">卸集日期</th>
                                                <th class="_th">倉房</th>
                                                <th class="_th">供應商</th>
                                                <th class="_th">板數</th>
                                            </tr>
                                            <asp:Repeater ID="rp_TaskList_B" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td data-th="卸集日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.date","{0:yyyy/MM/dd}").ToString())%></td>
                                                        <td data-th="倉房"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.warehouse_text").ToString())%></td>
                                                        <td data-th="供應商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier").ToString())%></td>
                                                        <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.plates").ToString())%></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (rp_TaskList_B.Items.Count == 0)
                                                {%>
                                            <tr>
                                                <td colspan="4" style="text-align: center">尚無資料</td>
                                            </tr>
                                            <% } %>
                                    </table>
                                        </div>


                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="row">
            <h2 class="margin-bottom-10">派遣任務</h2>
            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <label>派遣日期：</label>
                    <asp:TextBox ID="dateS" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                    <asp:TextBox ID="dateE" runat="server" class="form-control " CssClass="date_picker" autocomplete="off"></asp:TextBox>
                    <label>供應商：</label>
                    <asp:DropDownList ID="supplier" runat="server" CssClass="form-control chosen-select"></asp:DropDownList>

                    <asp:DropDownList ID="dltask_type" runat="server" CssClass="form-control label-warning   ">
                        <asp:ListItem Value="0">自營客戶集貨</asp:ListItem>
                        <asp:ListItem Value="1">B段集貨</asp:ListItem>
                        <asp:ListItem Value="2">專車</asp:ListItem>
                    </asp:DropDownList>
                    <label>任務編號：</label>
                    <asp:TextBox ID="task_id" runat="server" CssClass="form-control " autocomplete="off"></asp:TextBox>
                    <asp:Button ID="btnQry" CssClass="templatemo-blue-button" runat="server" Text="查詢" OnClick="search_Click" ValidationGroup="validate" />
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click1" />
                </div>

                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="max-height: 600px; overflow: auto;">
                            <table id="custom_table" class="table table-striped table-bordered templatemo-user-table" />
                            <tr class="tr-only-hide">
                                <th class="_th">派遣日期</th>
                                <th class="_th">任務編號</th>
                                <th class="_th">供應商</th>
                                <th class="_th">發包價</th>
                                <th class="_th">備註</th>
                                <th class="_th">更新人員</th>
                                <th class="_th">更新日期</th>
                            </tr>
                            <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.t_id").ToString())%>' />
                                        <asp:HiddenField ID="hid_task_type" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_type").ToString())%>' />
                                        <td data-th="派遣日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_date","{0:yyyy/MM/dd}").ToString())%></td>
                                        <td data-th="任務編號">
                                            <asp:Button ID="btnDetail" runat="server" Text='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.task_id").ToString())%>' CommandName="cmdDetail" CssClass="btn btn-outline  btn-link " CommandArgument='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.t_id").ToString())%>' />
                                        </td>
                                        <td data-th="供應商"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.supplier_name").ToString())%></td>
                                        <td data-th="發包價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.price","{0:N0}").ToString())%></td>
                                        <td data-th="備註"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.memo").ToString())%></td>
                                        <td data-th="更新人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.user_name").ToString())%> </td>
                                        <td data-th="更新日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.c_time" , "{0:yyyy/MM/dd HH:mm}").ToString())%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <% if (New_List.Items.Count == 0)
                                {%>
                            <tr>
                                <td colspan="6" style="text-align: center">尚無資料</td>
                            </tr>
                            <% } %>
                        </table>
                        </div>

                        共 <span class="text-danger">
                            <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                        </span>筆
                        <div id="page-nav" class="page"></div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnQry" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</asp:Content>

