﻿using ClosedXML.Excel;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.XSSF.UserModel;

public partial class store_import : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            readdata();
        }
    }

    private void readdata()
    {
        string wherestr = "";
        SqlCommand cmd = new SqlCommand();
        DateTime ChkDate = DateTime.Today;
        cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);
        cmd.Parameters.AddWithValue("@cdate", ChkDate.ToString("yyyy/MM/dd"));
        wherestr += " and CONVERT(VARCHAR,A.cdate,111)  = @cdate";
        cmd.CommandText = string.Format(@"Select A.* ,
                                                   B.code_name 'cartype_text', CASE WHEN A.DayNight =1 then '日' else '夜' end  'DayNigh_text',C.code_name  'CarRetailerName'
                                                   from ttAssetsSB A 
                                                   left join tbItemCodes B  with(nolock) on A.cabin_type = B.code_id  and  B.code_bclass = '6' and B.code_sclass = 'cartype' and B.active_flag = 1                                 
                                                   left join tbItemCodes C  with(nolock) on A.car_retailer = C.code_id and  C.code_bclass = '6'and C.code_sclass = 'CD'  and C.active_flag = 1 
                                                   where 1= 1 and A.cuser = @cuser {0} ", wherestr);
        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
        }

        using (SqlCommand cmdio = new SqlCommand())
        {
            cmdio.Parameters.AddWithValue("@cuser", Session["account_code"]);
            cmdio.Parameters.AddWithValue("@cdate", ChkDate.ToString("yyyy/MM/dd"));
            cmdio.Parameters.AddWithValue("@changeTable", "ttAssetsSB");
            cmdio.CommandText = string.Format(@"select A.id, A.cuser , A.cdate , A.successNum , A.failNum , A.totalNum , A.randomCode, A.print_date  , B.user_name from tbIORecords A with(nolock) 
                                                   left join  tbAccounts B with(nolock) on A.cuser= B.account_code 
                                                   where 1= 1 and A.changeTable = @changeTable and A.cdate >=DATEADD(DAY,-7, @cdate)  and A.cdate < DATEADD(DAY,1, @cdate) and A.status >=0 and A.print_date is not null  and A.cuser = @cuser  ");
            using (DataTable dt = dbAdapter.getDataTable(cmdio))
            {
                rep_IO.DataSource = dt;
                rep_IO.DataBind();
            }
        }
    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "空白超商匯入表標準格式20210226.xls";
        Func.DownloadExcel(ttFilePath, ttFileName);
    }

    protected void btImport_Click(object sender, EventArgs e)
    {
        lbMsg.Items.Clear();
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (file01.HasFile)
        {
            string randomCode = Guid.NewGuid().ToString().Replace("-", "");
            randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;

            #region 抓車牌
            DataTable dt_car = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select car_license, REPLACE(car_license,'-','') as car_license_nodash, ISNULL(oil_discount,0) as oil_discount, ownership, owner, car_retailer from ttAssets with(nolock) ";
                dt_car = dbAdapter.getDataTable(cmd);
            }
            #endregion

            #region 抓供應商
            DataTable dt_supplier = new DataTable();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "Select supplier_id , supplier_code , supplier_name , supplier_shortname, supplier_no  from tbSuppliers with(nolock) where active_flag = 1  ";
                dt_supplier = dbAdapter.getDataTable(cmd);
            }
            #endregion

            string ttErrStr = "";
            DateTime startTime = DateTime.Now;//開始匯入時間
            DateTime? endTime;                //匯入結束時間
            int successNum = 0;
            int failNum = 0;
            int totalNum = 0;
            DateTime InDate = new DateTime();
            if (!DateTime.TryParse(date.Text, out InDate)) InDate = DateTime.Now;

            string ttPath = Request.PhysicalApplicationPath + @"files\";

            //string ttFileName = ttPath + file02.PostedFile.FileName;
            string ttFileName = ttPath + file01.FileName;

            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                //HSSFWorkbook workbook = null;
                //HSSFSheet u_sheet = null;
                IWorkbook workbook;
                string ttSelectSheetName = "";
                if (ttExtName == ".xls")
                {
                    workbook = new HSSFWorkbook(file01.FileContent);  //2003
                }
                else
                {
                    workbook = new XSSFWorkbook(file01.FileContent);  //2007
                }


                Boolean IsSheetOk = true;//工作表驗證
                try
                {
                    //for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                    //{
                    var u_sheet = workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);

                    if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                    {
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + startTime.ToString("yyyy-MM-dd HH:mm:ss")));

                        //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                        string strRow, strInsCol, strInsVal;
                        List<StringBuilder> sb_del_list = new List<StringBuilder>();
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_del = new StringBuilder();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        //HSSFRow Row_title = new HSSFRow();

                        string ChkError01 = "";
                        string ChkError02 = "";
                        Boolean ChkFirstError = true;

                        //if (u_sheet.LastRowNum > 0) Row_title = u_sheet.GetRow(0);
                        for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                        {
                            #region 初始化

                            var Row = u_sheet.GetRow(i);  //取得目前的資料列  
                            strRow = (i + 1).ToString();
                            #endregion

                            #region 必填欄位篩選
                            for (var CellnNum = 0; CellnNum < 4; CellnNum++)
                            {
                                if (Row.GetCell(CellnNum) == null || Row.GetCell(CellnNum).ToString() == "")
                                {
                                    ChkFirstError = false;
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                    IsSheetOk = false;
                                    ChkError01 = "工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!";
                                    ChkError02 += "第" + strRow + "列： 請確認【" + u_sheet.GetRow(0).GetCell(CellnNum).StringCellValue + "】 是否正確!|";
                                    break;
                                }
                            }
                            #endregion

                            if (ChkFirstError)
                            {
                                totalNum += 1;
                                successNum += 1;

                                #region 驗證&取值 IsSheetOk

                                string Items = "";
                                string Items_Text = "";
                                string DayNight = "";
                                string DayNight_Text = "";
                                //string InDate = "NULL";
                                string Temperature = "";
                                string Temperature_Text = "";
                                string car_license = "";
                                string cabin_type = "";
                                string cabin_type_text = "";
                                int Stores = 0;
                                string Area = "";
                                string Area_Text = "";
                                float milage = 0;
                                string RequestObject = "";
                                string customer_code = "";
                                string Route = "";
                                string Route_name = "";
                                string driver = "";
                                string car_retailer = "";
                                string PaymentObject = "";
                                float income = 0;
                                float expenses = 0;
                                string starttime = "";
                                string endtime = "";
                                string startstore_time = "";
                                string endstore_time = "";
                                string supplier = "";
                                string supplier_no = "";
                                string memo = "";

                                //string fee_type = "";
                                //string a_id = "";
                                //string datestr = "";
                                //string timestr = "";
                                //string oil = "";
                                //string company = "";
                                //string items = "";
                                //string detail = "";
                                //float quant = 0;
                                //float milage = 0;
                                //float litre = 0;
                                //float price_notax = 0;       //牌價單價(未稅)
                                //float buy_price_notax = 0;   //購入單價(未稅)
                                //float receipt_price_notax = 0; //收款單價(未稅)
                                //float price = 0;             //牌價單價(含稅)
                                //float buy_price = 0;         //購入單價(含稅)
                                //float receipt_price = 0;     //收款單價(含稅)
                                //float total_price = 0;
                                //string memo = "";
                                //double oil_discount = 0;     //油價折扣
                                //string ownership = "";



                                for (int j = 0; j < u_sheet.GetRow(0).Cells.Count; j++)
                                {
                                    Boolean IsChkOk = true;

                                    #region 欄位篩選
                                    switch (j)
                                    {
                                        case 0:  //主線/爆量
                                            #region 

                                            //string ItemsResult = "{'主線':'01','爆量':'02','店到店':'03','回頭車':'04','加派車':'05'}";
                                            if (Row.GetCell(j) != null) Items_Text = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(Items_Text))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                switch (Items_Text)
                                                {
                                                    case "主線":
                                                        Items = "01";
                                                        break;
                                                    case "爆量":
                                                        Items = "02";
                                                        break;
                                                    case "店到店":
                                                        Items = "03";
                                                        break;
                                                    case "回頭車":
                                                        Items = "04";
                                                        break;
                                                    case "加派車":
                                                        Items = "05";
                                                        break;
                                                    default:
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 1:  //日/夜
                                            #region
                                            if (Row.GetCell(j) != null) DayNight_Text = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(DayNight_Text))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                switch (DayNight_Text)
                                                {
                                                    case "日":
                                                        DayNight = "1";
                                                        break;
                                                    case "夜":
                                                        DayNight = "2";
                                                        break;
                                                    default:
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 2:  //溫層      
                                            #region
                                            if (Row.GetCell(j) != null) Temperature_Text = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(Temperature_Text))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                using (SqlCommand cc = new SqlCommand())
                                                {
                                                    DataTable dataTable = new DataTable();
                                                    cc.CommandText = @"select code_id from tbItemCodes where code_sclass='temp' and code_name ='" + Temperature_Text + "'";
                                                    dataTable = dbAdapter.getDataTable(cc);

                                                    if (dataTable.Rows.Count > 0)
                                                    {
                                                        Temperature = dataTable.Rows[0]["code_id"].ToString();
                                                    }
                                                    else
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                }
                                                    //switch (Temperature_Text)
                                                    //{
                                                    //    case "冷藏":
                                                    //        Temperature = "1";
                                                    //        break;
                                                    //    case "冷凍":
                                                    //        Temperature = "2";
                                                    //        break;
                                                    //    case "常溫":
                                                    //        Temperature = "3";
                                                    //        break;
                                                    //    case "文流":
                                                    //        Temperature = "4";
                                                    //        break;
                                                    //    case "鮮一":
                                                    //        Temperature = "5";
                                                    //        break;
                                                    //    case "鮮二":
                                                    //        Temperature = "6";
                                                    //        break;
                                                    //    case "低鮮":
                                                    //        Temperature = "7";
                                                    //        break;
                                                    //    case "OC":
                                                    //        Temperature = "8";
                                                    //        break;
                                                    //    default:
                                                    //        IsChkOk =
                                                    //        IsSheetOk = false;
                                                    //        break;
                                                    //}
                                            }
                                            #endregion
                                            break;
                                        case 3:  //路線
                                            #region
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    Route_name = Row.GetCell(j).ToString();
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 4:  //車型(計價車型)      
                                            #region
                                            if (Row.GetCell(j) != null) cabin_type_text = Row.GetCell(j).ToString().Trim();
                                            switch (cabin_type_text)
                                            {
                                                case "3.5T(11呎)":
                                                    cabin_type = "1";
                                                    break;
                                                case "3.5T(14呎)":
                                                    cabin_type = "2";
                                                    break;
                                                case "6.5T":
                                                    cabin_type = "3";
                                                    break;
                                                case "7T":
                                                    cabin_type = "4";
                                                    break;
                                                case "8T/8.5T":
                                                    cabin_type = "5";
                                                    break;
                                                case "11T/12T":
                                                    cabin_type = "6";
                                                    break;
                                                case "17T":
                                                    cabin_type = "7";
                                                    break;
                                                case "26T":
                                                    cabin_type = "8";
                                                    break;
                                                case "半拖":
                                                    cabin_type = "9";
                                                    break;
                                                case "全拖":
                                                    cabin_type = "10";
                                                    break;
                                            }
                                            #endregion
                                            break;
                                        case 5:  //店數
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    if (Utility.IsNumeric(Row.GetCell(j).ToString()))
                                                    {
                                                        Stores = int.Parse(Row.GetCell(j).ToString());
                                                    }
                                                }
                                            }

                                            break;
                                        case 6:  //場區

                                            if (Row.GetCell(j) != null) Area_Text = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(Area_Text))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                using (SqlCommand sql = new SqlCommand())
                                                {
                                                    DataTable dataTable = new DataTable();
                                                    sql.CommandText = @"select seq from ttItemCodesFieldArea where code_name ='" + Area_Text + "'";
                                                    dataTable = dbAdapter.getDataTable(sql);

                                                    if (dataTable.Rows.Count > 0)
                                                    {
                                                        Area = dataTable.Rows[0]["seq"].ToString();
                                                    }
                                                    else
                                                    {
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                    }
                                                }
                                                //switch (Area_Text)
                                                //{
                                                //    case "萊爾富樹林廠":
                                                //        Area = "1";
                                                //        break;
                                                //    case "萊爾富龍潭廠":
                                                //        Area = "2";
                                                //        break;
                                                //    case "大智通":
                                                //        Area = "3";
                                                //        break;
                                                //    case "寶雅":
                                                //        Area = "4";
                                                //        break;
                                                //    case "日翊大溪":
                                                //        Area = "5";
                                                //        break;
                                                //    case "日翊台中":
                                                //        Area = "6";
                                                //        break;
                                                //    case "日翊高雄":
                                                //        Area = "7";
                                                //        break;
                                                //    case "全台林口":
                                                //        Area = "8";
                                                //        break;
                                                //    case "來來物流":
                                                //        Area = "9";
                                                //        break;
                                                //    default:
                                                //        IsChkOk =
                                                //        IsSheetOk = false;
                                                //        break;
                                                //}
                                            }
                                            break;
                                        case 7:  //公里
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    milage = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 8:  //離場時間
                                            #region
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        starttime = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        starttime = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(starttime))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            if (Convert.ToDateTime(Row.GetCell(j).DateCellValue) < new DateTime(1900, 1, 1))
                                                            {
                                                                IsChkOk =
                                                                IsSheetOk = false;
                                                            }
                                                            else
                                                            {
                                                                starttime = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd HH:mm");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                starttime = "NULL";
                                            }
                                            if (starttime == "NULL")
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }

                                            #endregion
                                            break;
                                        case 9:  //首店時間
                                            #region
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        startstore_time = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        startstore_time = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(starttime))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            if (Convert.ToDateTime(Row.GetCell(j).DateCellValue) < new DateTime(1900, 1, 1))
                                                            {
                                                                IsChkOk =
                                                                IsSheetOk = false;
                                                            }
                                                            else
                                                            {
                                                                startstore_time = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd HH:mm");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                startstore_time = "NULL";
                                            }
                                            if (startstore_time == "NULL")
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }

                                            #endregion
                                            break;
                                        case 10:  //末店時間
                                            #region
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        endstore_time = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        endstore_time = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(starttime))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            if (Convert.ToDateTime(Row.GetCell(j).DateCellValue) < new DateTime(1900, 1, 1))
                                                            {
                                                                IsChkOk =
                                                                IsSheetOk = false;
                                                            }
                                                            else
                                                            {
                                                                endstore_time = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd HH:mm");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                endstore_time = "NULL";
                                            }
                                            if (endstore_time == "NULL")
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }

                                            #endregion
                                            break;
                                        case 11:  //返廠時間
                                            #region
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        endtime = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        endtime = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(starttime))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            if (Convert.ToDateTime(Row.GetCell(j).DateCellValue) < new DateTime(1900, 1, 1))
                                                            {
                                                                IsChkOk =
                                                                IsSheetOk = false;
                                                            }
                                                            else
                                                            {
                                                                endtime = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd HH:mm");
                                                            }

                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                endtime = "NULL";
                                            }
                                            if (endtime == "NULL")
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }

                                            #endregion
                                            break;
                                        case 12:  //供應商
                                            #region
                                            if (Row.GetCell(j) != null) PaymentObject = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(PaymentObject))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                DataRow[] su_rows = dt_supplier.Select("supplier_name like'%" + PaymentObject + "%' or supplier_shortname like'%" + PaymentObject + "%'");
                                                if (su_rows.Length > 0)
                                                {
                                                    supplier = su_rows[0]["supplier_code"].ToString();
                                                    supplier_no = su_rows[0]["supplier_no"].ToString();
                                                }

                                                #region 
                                                //switch (supplier)
                                                //{
                                                //    case "峻富":
                                                //        supplier = "000";
                                                //        break;
                                                //    case "成佑":
                                                //        supplier = "A01";
                                                //        break;
                                                //    case "安達":
                                                //        supplier = "A02";
                                                //        break;
                                                //    case "錦濱":
                                                //        supplier = "A03";
                                                //        break;
                                                //    case "祈勳":
                                                //        supplier = "A04";
                                                //        break;
                                                //    case "合發":
                                                //        supplier = "A05";
                                                //        break;
                                                //    case "峻富北區":
                                                //        supplier = "A05";
                                                //        break;
                                                //    case "峻富基隆區":
                                                //        supplier = "A08";
                                                //        break;
                                                //    case "鴻駿":
                                                //        supplier = "B01";
                                                //        break;
                                                //    case "國瑋":
                                                //        supplier = "B02";
                                                //        break;
                                                //    case "瑞高":
                                                //        supplier = "B03";
                                                //        break;
                                                //    case "楊梅倉":
                                                //        supplier = "B04";
                                                //        break;
                                                //    case "峻富桃區":
                                                //        supplier = "B05";
                                                //        break;
                                                //    case "億鎮":
                                                //        supplier = "C01";
                                                //        break;
                                                //    case "峻富中區":
                                                //        supplier = "C02";
                                                //        break;
                                                //    case "尚科":
                                                //        supplier = "C03";
                                                //        break;
                                                //    case "今齊":
                                                //        supplier = "C04";
                                                //        break;
                                                //    case "成邑":
                                                //        supplier = "C05";
                                                //        break;
                                                //    case "震順":
                                                //        supplier = "C06";
                                                //        break;
                                                //    case "長隆":
                                                //        supplier = "D01";
                                                //        break;
                                                //    case "百通":
                                                //        supplier = "E01";
                                                //        break;
                                                //    case "立美":
                                                //        supplier = "E02";
                                                //        break;
                                                //    case "世峯":
                                                //        supplier = "F01";
                                                //        break;
                                                //    case "宜益":
                                                //        supplier = "F02";
                                                //        break;
                                                //    case "峻富南區":
                                                //        supplier = "F03";
                                                //        break;
                                                //    case "優鮮配":
                                                //        supplier = "G01";
                                                //        break;
                                                //    default:
                                                //        IsChkOk =
                                                //        IsSheetOk = false;
                                                //        break;
                                                //}
                                                #endregion

                                            }
                                            #endregion
                                            break;
                                        case 13:  //車號
                                            #region
                                            if (Row.GetCell(j) != null) car_license = Row.GetCell(j).ToString().Trim();
                                            if (String.IsNullOrEmpty(car_license))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 14:  //司機
                                            if (Row.GetCell(j) != null) driver = Row.GetCell(j).ToString().Trim();

                                            break;
                                        case 15:  //承攬價(收入)
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    income = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;
                                        case 16:  //發包價(支出)
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    expenses = float.Parse(Row.GetCell(j).ToString());
                                                }
                                            }
                                            break;

                                        case 17:  //備註
                                            if (Row.GetCell(j) != null)
                                            {
                                                if (!string.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    memo = Row.GetCell(j).ToString();
                                                }
                                            }
                                            break;
                                    }
                                    #endregion

                                    if (!IsChkOk)
                                    {
                                        if (string.IsNullOrEmpty(ChkError01))
                                        {
                                            ChkError01 = "工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!";
                                        }
                                        ChkError02 += "第" + strRow + "列： 請確認【" + u_sheet.GetRow(0).GetCell(j).StringCellValue + "】 是否正確!|";
                                    }
                                }

                                #endregion

                                #region 組新增

                                //檢查車號格式
                                //統一秀有"-"的車牌格式
                                DataRow[] rows = dt_car.Select("car_license='" + car_license + "'");
                                if (rows.Length == 0)
                                {
                                    DataRow[] nodash = dt_car.Select("car_license_nodash='" + car_license + "'");
                                    if (nodash.Length > 0)
                                    {
                                        car_license = nodash[0]["car_license"].ToString();
                                        //PaymentObject = nodash[0]["owner"].ToString();
                                        car_retailer = nodash[0]["car_retailer"].ToString();
                                    }
                                }
                                else
                                {
                                    //PaymentObject = rows[0]["owner"].ToString();
                                    car_retailer = rows[0]["car_retailer"].ToString();
                                }


                                strInsCol = "[Items],[Items_Text],[DayNight] ,[InDate] ,[Temperature] ,[Temperature_Text] ,[car_license] ,[cabin_type],[cabin_type_text] ,[Stores] ,[Area] ,[milage] ,[customer_code],[RequestObject] ,[Route] ,[Route_name] ,[driver] ,[car_retailer], [PaymentObject],[income] ,[expenses] ,[starttime] ,[endtime] ,[startstore_time] ,[endstore_time] ,[supplier] ,[supplier_no],[memo],[randomCode] ,[cdate] ,[cuser],[udate] ,[uuser] ";
                                strInsVal = "N\'" + Items + "\', " +
                                            "N\'" + Items_Text + "\', " +
                                            "N\'" + DayNight + "\', " +
                                            "N\'" + InDate.ToString("yyyy/MM/dd") + "\', " +
                                            "N\'" + Temperature + "\', " +
                                            "N\'" + Temperature_Text + "\', " +
                                            "N\'" + car_license + "\', " +
                                            "N\'" + cabin_type + "\', " +
                                            "N\'" + cabin_type_text + "\', " +
                                            "N\'" + Stores + "\', " +
                                            "N\'" + Area + "\', " +
                                            "N\'" + milage + "\', " +
                                            "N\'" + customer_code + "\', " +
                                            "N\'" + RequestObject + "\', " +
                                            "N\'" + Route + "\', " +
                                            "N\'" + Route_name + "\', " +
                                            "N\'" + driver + "\', " +
                                            "N\'" + car_retailer + "\', " +
                                            "N\'" + PaymentObject + "\', " +
                                            "N\'" + income + "\', " +
                                            "N\'" + expenses + "\', " +
                                            "N\'" + starttime + "\', " +
                                            "N\'" + endtime + "\', " +
                                            "N\'" + startstore_time + "\', " +
                                            "N\'" + endstore_time + "\', " +
                                            "N\'" + supplier + "\', " +
                                            "N\'" + supplier_no + "\', " +
                                            "N\'" + memo + "\', " +
                                            "N\'" + randomCode + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\' ," +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\' ";

                                string insertstr = "INSERT INTO ttAssetsSB (" + strInsCol + ") VALUES(" + strInsVal + ")";
                                sb_temp_ins.Append(insertstr);

                                //每100筆組成一字串
                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }

                                #endregion

                            }
                        }

                        //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                        if (IsSheetOk)
                        {
                            #region 執行SQL

                            //startTime = DateTime.Now;
                            //新增
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    String strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }

                                    }
                                }
                            }
                            endTime = DateTime.Now;
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  結束匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
                            #endregion

                            #region 匯入記錄
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                cmd.Parameters.AddWithValue("@changeTable", "ttAssetsSB");
                                cmd.Parameters.AddWithValue("@fromWhere", " 超商匯入");
                                cmd.Parameters.AddWithValue("@memo", "");
                                cmd.Parameters.AddWithValue("@successNum", "1");
                                cmd.Parameters.AddWithValue("@failNum", "1");
                                cmd.Parameters.AddWithValue("@totalNum", totalNum);
                                cmd.Parameters.AddWithValue("@startTime", startTime.ToString("yyyy-MM-dd HH:mm:ss"));
                                cmd.Parameters.AddWithValue("@endTime", endTime);
                                cmd.Parameters.AddWithValue("@randomCode", randomCode);
                                cmd.Parameters.AddWithValue("@print_date", InDate);
                                cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);//Session["account_code"] != null ? Session["account_code"].ToString() : null
                                cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                            #endregion

                            readdata();
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ChkError02))
                            {
                                lbMsg.Items.Add(ChkError01.ToString());
                                string[] ChkError02Str = ChkError02.Split('|');
                                foreach (string k in ChkError02Str)
                                {
                                    if (!string.IsNullOrEmpty(k.ToString()))
                                    {
                                        lbMsg.Items.Add(k.ToString());
                                    }
                                }
                            }
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");
                    }

                    //}
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    if (IsSheetOk)
                    {
                        lbMsg.Items.Add("匯入完畢");
                    }
                }
            }
        }
    }

    public bool IsValidCheckNumber(string check_number)
    {
        bool IsVaild = true;
        if (check_number.Length != 10)
        {
            IsVaild = false;
        }
        else
        {
            int num = Convert.ToInt32(check_number.Substring(0, 9));

            int chk = num % 7;
            int lastnum = Convert.ToInt32(check_number.Substring(9, 1));
            if (lastnum != chk)
            {
                IsVaild = false;
            }
        }
        return IsVaild;
    }



    protected void rep_IO_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string randomCode = e.CommandArgument.ToString();
        string errMsg = string.Empty;

        switch (e.CommandName)
        {
            case "cmdDel":
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                cmd.Parameters.AddWithValue("@status", -99);                                           //刪除
                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "randomCode", randomCode);
                cmd.Parameters.AddWithValue("@randomCode", randomCode);
                cmd.CommandText = "Delete from ttAssetsSB where randomCode = @randomCode;" + dbAdapter.genUpdateComm("tbIORecords", cmd); ;
                
                try
                {
                    dbAdapter.execNonQuery(cmd);
                }
                catch (Exception ex)
                {
                    errMsg = ex.Message;
                }
                readdata();
                break;
        }
    }
}