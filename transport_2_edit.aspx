﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="transport_2_edit.aspx.cs" Inherits="transport_2_edit" EnableEventValidation="false" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>峻富雲端物流管理-託運單修改</title>
    <link rel="shortcut icon" href="images/mark.png" type="image/x-icon" />
    <meta name="description" content="">
    <meta name="author" content="templatemo">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,700' rel='stylesheet' type='text/css'>
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/templatemo-style.css" rel="stylesheet">

    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery-ui.min.js" type="text/javascript"></script>   
    <link rel="stylesheet" href="css/jquery.ui.datepicker.css">

    <script src="js/bootstrap.min.js"></script>
    <script src="js/datepicker-zh-TW.js" type="text/javascript"></script>
    
    <link href="css/build.css" rel="stylesheet" />

    <link href="js/chosen_v1.6.1/chosen.css" rel="stylesheet" />
    <script src="js/chosen_v1.6.1/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/source/jquery.fancybox.js?v=2.1.5"></script>
    <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" type="text/css" href="js/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <style>
        input[type="radio"] {
            display: inherit;
        }

        input[type="checkbox"] {
            display: inherit;
        }

        .radio label {
            margin-right: 15px;
            text-align: left;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }

        .radio {
            padding-left: 5px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
            min-width: 0px;
        }

        .checkbox {
            padding-left: 15px;
        }

        ._shortwidth {
            width :80px;
        }

        .ralign {
           text-align :right ;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('.fancybox').fancybox();

            //$("#btCancel").fancybox({
            //    'width': 300,
            //    'height': 300,
            //    'autoScale': false,
            //    'transitionIn': 'none',
            //    'transitionOut': 'none',
            //    'type': 'iframe',
            //    beforeClose: function () {
            //        //// working
            //        //var $iframe = $('.fancybox-iframe retval');
            //        //var retval = $iframe.val();
            //        //alert(retval);
            //        ////alert($('input', $iframe.contents()).val());
            //        ////alert('aaa');
            //    },
               
              
            //});
            

            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                //minDate: 0,
                defaultDate: (new Date())  //預設當日
            });

            $(".chosen-select").chosen();
            $(".subpoena_category").change();

            $(".subpoena_category").change();

            $("#<%= turn_board.ClientID%>").change(function () {
                $("._turn_board_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= upstairs.ClientID%>").change(function () {
                $("._upstairs_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= difficult_delivery.ClientID%>").change(function () {
                $("._difficult_fee").prop('disabled', !$(this).is(":checked"));
            });

            $("#<%= pallet_recycling_flag.ClientID%>").change(function () {
                $("._TextBoxR").prop('disabled', !$(this).is(":checked"));
                $("._TextBoxB").prop('disabled', !$(this).is(":checked"));
                $("._TextBoxY").prop('disabled', !$(this).is(":checked"));
                $("._TextBoxG").prop('disabled', !$(this).is(":checked"));
                $("._TextBoxP").prop('disabled', !$(this).is(":checked"));
                $("._TextBoxS").prop('disabled', !$(this).is(":checked"));

            });




            $("#<%= btnRecsel.ClientID%>").click(function () {
                var customercode = $("#<%= customer_code.ClientID%>").val();
                if (customercode == "")
                {
                    alert('請先選取客代');
                    return false;
                }
            });
        });
        $.fancybox.update();

        $(document).on("change", ".subpoena_category", function () {
            var _isOK = true;
            var _cateVal = $(this).children("option:selected").val();
            var _setItem;
            var _disItem;
            switch (_cateVal) {
                case '11'://元付
                    //_collection_money
                    _disItem = $("._collection_money");
                    _setItem = $("._arrive_to_pay_freight,._arrive_to_pay_append");
                    break;
                case '21'://到付
                    _disItem = $("._collection_money,._arrive_to_pay_append");
                    _setItem = $("._arrive_to_pay_freight");
                    break;
                case '25'://元付-到付追加
                    _disItem = $("._collection_money,._arrive_to_pay_freight");
                    _setItem = $("._arrive_to_pay_append");
                    break;
                case '41'://代收貨款
                    _disItem = $("._arrive_to_pay_append,._arrive_to_pay_freight");
                    _setItem = $("._collection_money");
                    break;
                default:
                    _isOK = false;
                    break;
            }
            if (_isOK) {

                _disItem.map(function () { $(this).prop('readonly', true); });
                _setItem.map(function () { $(this).prop('readonly', false); });

            }

        });

        function confirm_click() {
            return confirm("您確定要銷單嗎 ?");
            //window.event.returnValue = false;
        }

        $(document).on("change", '#chkshowsend', function (event) {
            if (!this.checked) {
                $("#chksend_address").prop("checked", false);
            }
        });

    </script>
    <style type="text/css">
        .ui-datepicker select.ui-datepicker-month,
        .ui-datepicker select.ui-datepicker-year {
            color: black;
        }

        .ui-datepicker a {
            color: #0275d8;
        }
        
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="templatemo-content-widget white-bg" >
            <div class="row">
                <div class="templatemo-login-form">
                    <div class="form-group form-inline">
                        <label for="inputFirstName">託運類別：</label>
                        <div class="margin-right-15 templatemo-inline-block">
                            <asp:RadioButtonList ID="rbcheck_type" runat="server" RepeatDirection="Horizontal" CssClass="radio radio-success" RepeatColumns="5" RepeatLayout="Flow" AutoPostBack="True" OnSelectedIndexChanged="rbcheck_type_SelectedIndexChanged">
                                <asp:ListItem Value="01" Selected="True">01 論板</asp:ListItem>
                                <asp:ListItem Value="02">02 論件</asp:ListItem>
                                <asp:ListItem Value="03">03 論才</asp:ListItem>
                                <asp:ListItem Value="04">04 論小板</asp:ListItem>
                                <asp:ListItem Value="05">05 專車</asp:ListItem>
                            </asp:RadioButtonList>
                           <div id="plCancel" runat ="server" class="form-group ">
                                <span  runat="server" class=" btn btn-warning "><a   id="btCancel" >銷單</a></span>
                                  
                           </div>
                        </div>
                    </div>
                    <div class="form-group form-inline">
                    <label>出貨日期：</label><asp:TextBox ID="Shipments_date" runat="server" CssClass="form-control date_picker "></asp:TextBox>
                    </div>
                    <div class="form-group form-inline">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <label>客代編號：</label>
                                <asp:DropDownList ID="dlcustomer_code" runat="server" AutoPostBack="True" CssClass="form-control chosen-select" OnSelectedIndexChanged="dlcustomer_code_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:Label ID="lbcustomer_name" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator Display="Dynamic" ID="req7" runat="server" ControlToValidate="dlcustomer_code" ForeColor="Red" ValidationGroup="validate">請選擇客代編號</asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        
                    </div>
                    <!--基本設定-->
                    <div class="bs-callout bs-callout-info">
                        <h3>1. 基本設定</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label for="inputFirstName"><span class="REDWD_b">*</span>託運單號</label>
                                    <asp:Label ID="lbcheck_number" runat="server" BackColor="#FFFF66"></asp:Label>
                                    <asp:HiddenField ID="hid_id" runat="server" />
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">訂單號碼</label>
                                    <asp:TextBox ID="order_number" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="inputLastName"><span class="REDWD_b">*</span>託運類別</label>
                                    <asp:DropDownList ID="check_type" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">收貨人編號</label>
                                    <asp:TextBox ID="receive_customer_code" runat="server" class="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="inputLastName"><span class="REDWD_b">*</span>傳票類別</label>
                                    <asp:DropDownList ID="subpoena_category" runat="server" class="form-control subpoena_category">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>2. 收件人</h3>
                        <div class="rowform">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <div class="row form-inline">
                                        <div class="form-group">
                                            <label>收件人編號</label>
                                            <asp:TextBox ID="receiver_code" runat="server" class="form-control" AutoPostBack="true" OnTextChanged="receiver_code_TextChanged" Width="80px"></asp:TextBox>
                                            <asp:Button ID="btnRecsel" CssClass="templatemo-white-button" runat="server" Text="通訊錄" Font-Size="X-Small" OnClick="btnRecsel_Click" />
                                            <label><span class="REDWD_b">*</span>收件人</label>
                                            <asp:TextBox ID="receive_contact" runat="server" class="form-control" AutoPostBack="True" OnTextChanged="receive_contact_TextChanged"  MaxLength="20" ></asp:TextBox>
                                            <asp:Button Style="display: none" ID="btnqry" runat="server" Text="" />
                                            <div style="display: none">
                                                <asp:TextBox ID="receiver_id" runat="server" />
                                            </div>
                                            &nbsp;
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="cbSaveReceive" runat="server" Text="設為常用收件人" Font-Size="Smaller" />
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label><span class="REDWD_b">*</span>電話1</label>
                                            <asp:TextBox ID="receive_tel1" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                            分機
                                        <asp:TextBox ID="receive_tel1_ext" runat="server" class="form-control" MaxLength="5" Width="80"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>電話2</label>
                                            <asp:TextBox ID="receive_tel2" runat="server" class="form-control" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                        </div>
                                        <div id="divReceiver" runat="server" visible="false" class="templatemo-login-form panel panel-default" style="overflow: auto; height: 300px; background-color:aliceblue">
                                            <br />
                                            <div>
                                                <div class="form-group form-inline">
                                                    <label>關鍵字：</label>
                                                    <asp:TextBox ID="keyword" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                                    <asp:Button ID="cancel" CssClass="btn btn-default " runat="server" Text="關 閉" OnClick="cancel_Click" />
                                                </div>
                                            </div>
                                            <table class="table table-striped table-bordered templatemo-user-table">
                                                <tr class="tr-only-hide">
                                                    <th>選取</th>
                                                    <th>客戶代碼</th>
                                                    <th>客戶名稱</th>
                                                    <th>地址</th>
                                                </tr>
                                                <asp:Repeater ID="New_List" runat="server" OnItemCommand="New_List_ItemCommand">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td data-th="選取">
                                                                <asp:HiddenField ID="hid_id" runat="server" Value='<%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_id").ToString())%>' />
                                                                <asp:Button ID="btselect" CssClass=" btn-link " CommandName="cmdSelect" runat="server" Text="選取" />
                                                            </td>
                                                            <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_code").ToString())%></td>
                                                            <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.receiver_name").ToString())%></td>
                                                            <td data-th="地址"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_city").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_area").ToString())%><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.address_road").ToString())%></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <% if (New_List.Items.Count == 0)
                                                    {%>
                                                <tr>
                                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                                </tr>
                                                <% } %>
                                            </table>
                                        </div>

                                    </div>
                                    <div class="row form-inline">
                                        <div class="form-group">
                                            <label><span class="REDWD_b">*</span>地&nbsp; 址</label>
                                            <asp:DropDownList ID="receive_city" runat="server" CssClass="form-control chosen-select _shortwidth" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:DropDownList ID="receive_area" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="receive_area_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:TextBox ID="receive_address" CssClass="form-control" runat="server" MaxLength ="70"  placeholder="請輸入地址"></asp:TextBox>
                                            &nbsp;                                
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="cbarrive" runat="server" Text="站址自領" AutoPostBack="True" OnCheckedChanged="cbarrive_CheckedChanged" /></span>
                                            <asp:DropDownList ID="area_arrive_code" runat="server" CssClass="form-control _shortwidth"  AutoPostBack="True" OnSelectedIndexChanged="area_arrive_code_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="req10" runat="server" ControlToValidate="area_arrive_code" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請選擇到著碼</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:RequiredFieldValidator Display="Dynamic" ID="req2" runat="server" ControlToValidate="receive_tel1" ForeColor="Red" ValidationGroup="validate">請輸入電話</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req3" runat="server" ControlToValidate="receive_contact" ForeColor="Red" ValidationGroup="validate">請輸入收件人</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req4" runat="server" ControlToValidate="receive_city" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req5" runat="server" ControlToValidate="receive_area" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req6" runat="server" ControlToValidate="receive_address" ForeColor="Red" ValidationGroup="validate">請輸入地址</asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>3. 才件代收</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="件數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="pieces" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label4" runat="server" Text="板數" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="plates" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label5" runat="server" Text="才數" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="cbm" CssClass="form-control " runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="5" Width="100px" placeholder="0"></asp:TextBox>
                                </div>
                                <asp:Label ID="lbErrQuant" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="＄代收金額" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="collection_money" CssClass="form-control  _collection_money" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label7" runat="server" Text="＄到付運費(含稅)" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="arrive_to_pay_freight" CssClass="form-control  _arrive_to_pay_freight" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label8" runat="server" Text="＄到付追加" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="arrive_to_pay_append" CssClass="form-control  _arrive_to_pay_append" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="form-group" id="individual_fee" runat="server" style="display: none;">
                                            <asp:Label ID="Label9" runat="server" Text="＄貨件運費" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_supplier_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label10" runat="server" Text="＄配送費用" CssClass="ralign" Width="130px"></asp:Label><asp:TextBox ID="i_csection_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Label ID="Label11" runat="server" Text="＄偏遠加價" CssClass="ralign" Width="80px"></asp:Label><asp:TextBox ID="i_remote_fee" CssClass="form-control " BackColor="#f8eae9" ForeColor="#333333" Font-Size="Medium" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0"></asp:TextBox>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>*<br>
                            特殊設定</h3>
                        <div class="rowform">
                            <%-- <div class="row form-inline">
                            <div class="form-group">
                                <asp:CheckBox ID="donate_invoice_flag" class="font-weight-400" runat="server" Text="發票捐贈" />
                                &nbsp;&nbsp;
                                <asp:CheckBox ID="electronic_invoice_flag" class="font-weight-400" runat="server" Text="電子發票" />
                                <asp:TextBox ID="uniform_numbers" CssClass="form-control" runat="server" placeholder="統一編號"></asp:TextBox>
                            </div>
                        </div>--%>
                            <div class="row form-inline">
                                <%--<div class="form-group">
                                    <label>商品種類</label>
                                    <asp:DropDownList ID="product_category" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="inputFirstName">手機</label>
                                    <asp:TextBox ID="arrive_mobile" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"></asp:TextBox>
                                </div>--%>
                                <div class="form-group">
                                    <label for="inputLastName">特殊配送</label>
                                    <asp:DropDownList ID="special_send" runat="server" CssClass="form-control" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <%--<div class="form-group">
                                <label for="inputFirstName">E-mail</label>
                                <asp:TextBox ID="arrive_email" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>--%>
                                <div class="form-group">
                                    <label for="inputLastName">指定日</label>
                                    <asp:TextBox ID="arrive_assign_date" runat="server" class="form-control" CssClass="date_picker"></asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Text="時段"></asp:Label>
                                    <asp:DropDownList ID="time_period" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    &nbsp;
                                </div>

                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label for="inputLastName">備註</label>
                                    <%--<asp:DropDownList ID="invoice_memo" runat="server" CssClass="form-control">                                    
                                </asp:DropDownList>--%>
                                    <%--<asp:TextBox ID="invoice_desc" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                    <textarea id="invoice_desc" runat="server" cols="20" rows="3"  maxlength="50" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                        <span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="receipt_flag" runat="server" Text="回單" /></span>
                                        <br />
                                        <%--<span class="checkbox checkbox-success">
                                            <asp:CheckBox ID="pallet_recycling_flag" runat="server" Text="棧板回收" onclick="pallet_recycling_flagClick(this)" /></span>
                                    <a id="carsel" href="transport_1_carsel.aspx?Pallet_type=<%=Pallet_type.ClientID %>&Pallet_type_text=<%=Pallet_type_text.ClientID %>" class="fancybox fancybox.iframe " ></a>
                                    <div id="Pallet_type_div" runat="server" style="display:none;">
                                    棧板種類<asp:TextBox ID="Pallet_type_text" CssClass="form-control" runat="server" Width="80px" ReadOnly></asp:TextBox>
                                        <asp:HiddenField ID="Pallet_type" runat="server" />
                                </div>
                                </div>--%>
                                <div class="form-group">
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="turn_board" runat="server" Text="翻板" /></span>
                                    ＄<asp:TextBox ID="turn_board_fee" CssClass="form-control _turn_board_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                    <br />
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="upstairs" runat="server" Text="上樓" /></span>
                                    ＄<asp:TextBox ID="upstairs_fee" CssClass="form-control _upstairs_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                    <br />
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="difficult_delivery" runat="server" Text="困配" /></span>
                                    ＄<asp:TextBox ID="difficult_fee" CssClass="form-control _difficult_fee" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="100px" placeholder="0" MaxLength="6"></asp:TextBox>
                                </div>
                                                                         <a id="carsel" href="transport_1_carsel.aspx?Pallet_type=<%=Pallet_type.ClientID %>&Pallet_type_text=<%=Pallet_type_text.ClientID %>" class="fancybox fancybox.iframe "></a>
                                
                                <div id="Pallet_type_div" runat="server" style="display: none;">
                                    棧板種類<asp:TextBox ID="Pallet_type_text" CssClass="form-control" runat="server" Width="80px" ReadOnly></asp:TextBox>
                                    <asp:HiddenField ID="Pallet_type" runat="server" />
                                </div>

                            <span class="checkbox checkbox-success">
                                    <asp:CheckBox  ID="pallet_recycling_flag" runat="server" Text="棧板回收" Font-Bold="True"  />
                                 <%--<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" runat="server" ControlToValidate="receive_tel1" ForeColor="Red" ValidationGroup="validate" SetFocusOnError="true">請輸入電話&nbsp;</asp:RequiredFieldValidator>--%>
                                <asp:Label ID="pallet_alert" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            <div class="form-group">
                                紅板&nbsp<asp:TextBox ID="TextBoxR" CssClass="form-control _TextBoxR" runat="server"  Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox></span>
                                 板
                                <br />
                            
                                綠板&nbsp<asp:TextBox ID="TextBoxG" CssClass="form-control _TextBoxG" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox></span>
                                 板
                                <br />
                            
                                藍板&nbsp<asp:TextBox ID="TextBoxB" CssClass="form-control _TextBoxB" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox></span>
                                 板
                                 <br />
                              
                                黃板&nbsp<asp:TextBox ID="TextBoxY" CssClass="form-control _TextBoxY" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox></span>板
                                <br />
                              
                                塑膠板&nbsp<asp:TextBox ID="TextBoxP" CssClass="form-control _TextBoxP" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox>板
                                <br />
                              
                                特殊板&nbsp<asp:TextBox ID="TextBoxS" CssClass="form-control _TextBoxS" runat="server" Enabled="false" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" Width="50px" placeholder="0" MaxLength="5"></asp:TextBox>板
                            <asp:TextBox ID="pallet_desc" CssClass="form-control _pallet_type" runat="server" placeholder="請輸入特殊規格，限20字" Width="300px"  MaxLength="20"></asp:TextBox>
                            <asp:Label ID="pallet_alert2" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                           <br>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info">
                        <h3>4. 寄件人</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <label>&nbsp;客戶代號</label>
                                            <asp:TextBox ID="customer_code" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                            <asp:Label ID="Label2" runat="server" Text="寄件人"></asp:Label>
                                            <asp:TextBox ID="send_contact" CssClass="form-control" runat="server"></asp:TextBox>
                                            <span class="checkbox checkbox-success">
                                                <asp:CheckBox ID="chkshowsend" runat="server" Text="顯示寄件人資料" AutoPostBack="True" Checked="True" OnCheckedChanged="CheckBox1_CheckedChanged" /></span>
                                        </div>
                                        <label>電話/手機</label>
                                        <asp:TextBox ID="send_tel" CssClass="form-control" runat="server" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" MaxLength="10"></asp:TextBox>
                                        <asp:DropDownList ID="send_station_scode" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label>&nbsp;寄件地址</label>
                                    <asp:DropDownList ID="send_city" runat="server" CssClass="form-control chosen-select" AutoPostBack="true" OnSelectedIndexChanged="city_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="send_area" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="send_station_scode_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:TextBox ID="send_address" CssClass="form-control" runat="server"></asp:TextBox>
                                    <span class="checkbox checkbox-success">
                                        <asp:CheckBox ID="chksend_address" class="font-weight-400" runat="server" Checked="true" Text="顯示地址" /></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bs-callout bs-callout-info" id="div_cancel" runat ="server" visible="false" >
                        <h3>銷單資訊</h3>
                        <div class="rowform">
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label>銷單日期：</label><asp:Literal ID="liCancel_date" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label>銷單人員：</label><asp:Literal ID="liCancel_psn" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <div class="form-group">
                                    <label>銷單原因：</label><asp:Literal ID="liCancel_memo" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <asp:Button ID="btnsave" CssClass="templatemo-blue-button" runat="server" Text="儲存" ValidationGroup="validate" OnClick="btnsave_Click" />
                        <asp:Button ID="btncancel" CssClass="templatemo-white-button" runat="server" Text="取消" OnClientClick="parent.$.fancybox.close();" />
                        <asp:Label ID="lbl_sub_check_number" runat="server" Visible="false"></asp:Label>
                    </div>

                </div>
            </div>
        </div>

    </form>
    <script>
        function pallet_recycling_flagClick(e) {
            if (e.checked == true) {
                $('a#carsel').trigger('click');
                $("#<%=Pallet_type_div.ClientID%>").attr("style", "display:block;");
            } else {
                $("#<%=Pallet_type_div.ClientID%>").attr("style", "display:none;");
                $("#<%=Pallet_type.ClientID%>").val();
                $("#<%=Pallet_type_text.ClientID%>").val();
            }
        }
    </script>
</body>
</html>
