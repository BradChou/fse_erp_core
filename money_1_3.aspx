﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_1_3.aspx.cs" Inherits="money_1_3" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            close_typeChange();

            <%--$("#<%= Suppliers.ClientID%>").change(function () {
                ChangeSecondUseReplaceWith();
            });--%>

            <%--$("#<%= second_code.ClientID%>").change(function () {
                second_codeChange();
            });--%>

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                close_typeChange();
            }

        });

        function second_codeChange() {
            var type = $("#<%= dlclose_type.ClientID%> option:selected").val()
            if (type == '1') {
                ChangeCloseDay();
            }
        }

        function close_typeChange() {
            $("#<%= dlclose_type.ClientID%>").change(function () {
                var n = $(this).val();
                switch (n) {
                    case '1':
                        $("._close").show();
                        $("._daterange").html('請款期間');
                        ChangeCloseDay();
                        break;
                    case '2':
                        $("._close").hide();
                        $("._daterange").html('發送期間');
                        break;
                }
            });
        }

        function SetSecondDDLEmpty() {
            $("#<%= second_code.ClientID%>").empty();
            $("#<%= second_code.ClientID%>").append($('<option></option>').val('').text('請選擇'));
        }
        function ChangeSecondUseReplaceWith() {
            var sel_supplier_code = $.trim($("#<%= Suppliers.ClientID%> option:selected").val());
            if (sel_supplier_code.length == 0) {
                SetSecondDDLEmpty();
            }
            else {
                $.ajax(
                {
                    url: "money_1_3.aspx/GetSecondDDLHtml",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ supplier_code: sel_supplier_code }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d.length > 0) {
                        $("#<%= second_code.ClientID%>").html(data.d);
                        $("#<%= second_code.ClientID%>").trigger("chosen:updated");
                        //second_codeChange();
                    }
                });
            }
        }

        function SetClosedayDDLEmpty() {
            $("#<%= dlCloseDay.ClientID%>").empty();
            $("#<%= dlCloseDay.ClientID%>").append($('<option></option>').val('').text('請選擇'));
        }
        function ChangeCloseDay() {
            var sel_supplier_code = $.trim($("#<%= Suppliers.ClientID%> option:selected").val());
            var sel_second_code = $.trim($("#<%= second_code.ClientID%> option:selected").val());
            var sel_dates = $.trim($("#<%= dates.ClientID%>").val());
            var sel_datee = $.trim($("#<%= datee.ClientID%>").val());
            if (sel_supplier_code.length == 0 || sel_second_code.length == 0) {
                SetClosedayDDLEmpty();
            }
            else {
                $.ajax(
                {
                    url: "money_1_3.aspx/GetCloseDayDDLHtml",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ dates: sel_dates, datee: sel_datee, supplier_code: sel_supplier_code, second_code: sel_second_code }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d.length > 0) {
                        $("#<%= dlCloseDay.ClientID%>").html(data.d);
                        //$("#<%= dlCloseDay.ClientID%>").trigger("chosen:updated");
                        //$("#<%= dlCloseDay.ClientID%>").trigger('change', true);
                        <%--<%=Page
                               .ClientScript
                               .GetPostBackEventReference(dlCloseDay,"")%>;--%>
                }
                else {
                    SetClosedayDDLEmpty();
                }
                });
        }
    }

    </script>
    
    <style>
        ._th {
            text-align: center;
        }
        .auto-style1 {
            text-align: center;
            width: 147px;
        }
        .auto-style2 {
            width: 147px;
        }
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="modal fade" id="handclose">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header  bg-primary">
                    <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                    <h3 class="modal-title white-text ">手動結帳</h3>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <div class="text-right"><span class=" text-danger text-small font-weight-bold ">*為必填欄位</span></div>
                                <div class="alert alert-danger in alert-dismissable div_err" style="display: none;">
                                    <span id="_err"></span>
                                </div>
                                <div class="form-row form-inline">
                                    <div class="form-group col-sm-12">
                                        客戶代號：<asp:Label ID="lSuppliers" runat="server"></asp:Label>
                                        次客代：<asp:Label ID="lsecond_code" runat="server"></asp:Label>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label><span class="text-danger">*</span> 結帳截止日期：</label>
                                        <asp:TextBox ID="date_end" runat="server" class="form-control" CssClass="date_picker " autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                                <hr />
                                <div class="text-center ">
                                    <asp:Button ID="btClose" runat="server" class="btn btn-primary" OnClick="btClose_Click"  Text="開始結帳" />
                                    <!--<asp:Button ID="Button1" runat="server" class="btn btn-primary" OnClick="btClose_Click_New"  Text="開始結帳_測試用" /> -->
                                    <button type="button" class="btn btn-primary btn-border" data-dismiss="modal">取消</button>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>                
            </div>
        </div>
    </div>

   <%-- <div class="modal hide fade" id="handclose" tabindex="-1" role="dialog" aria-labelledby="addnewTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
               
                        <div class="modal-header text-white bg-primary">
                            <h3 class="modal-title" id="addnewTitle">手動結帳</h3>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button>
                        </div>
                   
                <div class="modal-body">
                  
                </div>
                
            </div>
        </div>
    </div>--%>

    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">自營客戶應收</h2>

            <div class="templatemo-login-form">
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <label class="_daterange" for="inputLastName">請款期間</label>
                            <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate" autocomplete="off"></asp:TextBox>
                            ~ 
                            <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate" autocomplete="off"></asp:TextBox>
                            <label for="close_date">客代結帳日</label>
                            <asp:DropDownList ID="close_date" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="close_date_SelectedIndexChanged1">
                                <asp:ListItem Value="">請選擇</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="29">29</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="31">31</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>                    
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode ="Conditional" >
                        <ContentTemplate>
                            <label for="inputFirstName">客戶代號</label>
                            <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" AutoPostBack ="true"  OnSelectedIndexChanged ="Suppliers_SelectedIndexChanged"></asp:DropDownList>
                            次客代 
                            <asp:DropDownList ID="second_code" runat="server" CssClass="form-control chosen-select" AutoPostBack ="true" OnSelectedIndexChanged="second_code_SelectedIndexChanged"   ></asp:DropDownList>
                            <asp:RequiredFieldValidator Display="Dynamic" ID="req1" runat="server" ControlToValidate="second_code" ForeColor="Red" ValidationGroup="validate">請選次客代</asp:RequiredFieldValidator>
                            <asp:DropDownList ID="dlclose_type" CssClass="form-control" runat="server"  >
                                <asp:ListItem Value="1">1. 已出帳</asp:ListItem>
                                <asp:ListItem Value="2">2. 未出帳</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="dl_pricing_type" runat="server" CssClass="form-control" AutoPostBack ="true" OnSelectedIndexChanged ="dl_pricing_type_SelectedIndexChanged">
                                <asp:ListItem Value ="">論板</asp:ListItem>
                                <asp:ListItem Value ="05">專車</asp:ListItem>
                                <asp:ListItem Value ="*">全部</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="帳單查詢" OnClick="btQry_Click" />
                            <asp:Button ID="btShowClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose2_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');" />
                            <%--<asp:Button ID="btClose" runat="server" class="btn btn-primary" Text="手動結帳" OnClick="btClose_Click" OnClientClick="return confirm('您確定要手動結帳嗎?');" />--%>
                            <asp:Button ID="btExport" runat="server" class="templatemo-white-button " Text="匯出總表" OnClick="btExport_Click" />
                            <asp:Button ID="btExport_Detail" runat="server" class="templatemo-white-button " Text="匯出明細表" OnClick="btExport_Detail_Click" />
                            <%--<asp:Button ID="Button1" runat="server" class="templatemo-white-button " Text="test" OnClick="Button1_Click" />--%>
                        </ContentTemplate>
                        <Triggers >
                            <asp:AsyncPostBackTrigger ControlID="Suppliers" EventName ="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="second_code" EventName ="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="dl_pricing_type" EventName ="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="close_date" EventName ="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btExport" />
                            <asp:PostBackTrigger ControlID="btExport_Detail" />
                            <asp:PostBackTrigger ControlID="btShowClose" />
                        </Triggers>
                    </asp:UpdatePanel>
                    
                </div>
                <div class="form-group form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <label class="_close" for="inputFirstName">結帳日</label>
                            <asp:DropDownList ID="dlCloseDay" runat="server" CssClass="form-control _close"></asp:DropDownList>
                            <%--<button type="submit" class="btn btn-primary _close">重寄帳單</button>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <hr>
                <p class="text-primary">
                    <asp:Label ID="lbSuppliers" runat="server"></asp:Label><br>
                    請款期間：<asp:Label ID="lbdate" runat="server"></asp:Label>
                </p>
              
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server">
                            <asp:Panel ID="Panel_01" runat="server" Visible="false"  >
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">序號</th>
                                        <th class="_th">發送日期</th>
                                        <th class="_th">客戶代碼</th>
                                        <th class="_th">客戶名稱</th>
                                        <th class="_th">貨號</th>
                                        <th class="auto-style1">發送區</th>
                                        <th class="_th">到著區</th>
                                        <th class="_th">板數</th>
                                        <th class="_th">貨件費用</th>
                                    </tr>
                                    <asp:Repeater ID="New_List_01" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="序號">
                                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                                </td>
                                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                                <td data-th="客戶代碼"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString()))%></td>
                                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                                <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                                <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                                                <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著區").ToString())%></td>
                                                <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.板數","{0:N0}").ToString())%></td>
                                                <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List_01.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="9" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                <table class="paytable">
                                    <tr>
                                        <th>小計：</th>
                                        <td>NT$ 
                            <asp:Label ID="subtotal_01" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>5%營業稅：</th>
                                        <td>NT$  
                            <asp:Label ID="tax_01" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>應收帳款：</th>
                                        <td>NT$   
                            <asp:Label ID="total_01" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel_02" runat="server" Visible="false">
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">序號</th>
                                        <th class="_th">發送日期</th>
                                        <th class="_th">客戶代碼</th>
                                        <th class="_th">客戶名稱</th>
                                        <th class="_th">貨號</th>
                                        <th class="_th">發送區</th>
                                        <th class="_th">到著區</th>
                                        <th class="_th">件數</th>
                                        <th class="_th">貨件費用</th>
                                    </tr>
                                    <asp:Repeater ID="New_List_02" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="序號">
                                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                                </td>
                                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                                <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString())%></td>
                                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                                <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                                <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                                                <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著區").ToString())%></td>
                                                <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數","{0:N0}").ToString())%></td>
                                                <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List_02.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="9" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                <table class="paytable">
                                    <tr>
                                        <th>小計：</th>
                                        <td>NT$  
                            <asp:Label ID="subtotal_02" runat="server"></asp:Label>元</td>
                                    </tr>
                                    <tr>
                                        <th>5%營業稅：</th>
                                        <td>NT$   
                            <asp:Label ID="tax_02" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>應收帳款：</th>
                                        <td>NT$    
                            <asp:Label ID="total_02" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel_03" runat="server" Visible="false">
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">序號</th>
                                        <th class="_th">發送日期</th>
                                        <th class="_th">客戶代碼</th>
                                        <th class="_th">客戶名稱</th>
                                        <th class="_th">貨號</th>
                                        <th class="_th">發送區</th>
                                        <th class="_th">到著區</th>
                                        <th class="_th">才數</th>
                                        <th class="_th">貨件費用</th>
                                    </tr>
                                    <asp:Repeater ID="New_List_03" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="序號">
                                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                                </td>
                                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                                <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString())%></td>
                                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                                <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                                <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                                                <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著區").ToString())%></td>
                                                <td data-th="才數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.才數","{0:N0}").ToString())%></td>
                                                <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List_03.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="9" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                <table class="paytable">
                                    <tr>
                                        <th>小計：</th>
                                        <td>NT$  
                            <asp:Label ID="subtotal_03" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>5%營業稅：</th>
                                        <td>NT$   
                            <asp:Label ID="tax_03" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>應收帳款：</th>
                                        <td>NT$    
                            <asp:Label ID="total_03" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel_04" runat="server" Visible="false">
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">序號</th>
                                        <th class="_th">發送日期</th>
                                        <th class="_th">客戶代碼</th>
                                        <th class="_th">客戶名稱</th>
                                        <th class="_th">貨號</th>
                                        <th class="_th">發送區</th>
                                        <th class="_th">到著區</th>
                                        <th class="_th">小板數</th>
                                        <th class="_th">貨件費用</th>
                                    </tr>
                                    <asp:Repeater ID="New_List_04" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="序號">
                                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                                </td>
                                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                                <td data-th="客戶代碼"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString())%></td>
                                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                                <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                                <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                                                <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著區").ToString())%></td>
                                                <td data-th="小板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.小板數","{0:N0}").ToString())%></td>
                                                <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List_04.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="9" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                <table class="paytable">
                                    <tr>
                                        <th>小計：</th>
                                        <td>NT$  
                            <asp:Label ID="subtotal_04" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>5%營業稅：</th>
                                        <td>NT$   
                            <asp:Label ID="tax_04" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>應收帳款：</th>
                                        <td>NT$    
                            <asp:Label ID="total_04" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="Panel_05" runat="server" Visible="false">
                                <table class="table table-striped table-bordered templatemo-user-table">
                                    <tr class="tr-only-hide">
                                        <th class="_th">序號</th>
                                        <th class="_th">發送日期</th>
                                        <th class="_th">客戶代碼</th>
                                        <th class="_th">客戶名稱</th>
                                        <th class="_th">貨號</th>
                                        <th class="_th">發送區</th>
                                        <th class="_th">到著區</th>
                                        <th class="_th">板數</th>
                                        <th class="_th">貨件費用</th>
                                    </tr>
                                    <asp:Repeater ID="New_List_05" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td data-th="序號">
                                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                                </td>
                                                <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                                <td data-th="客戶代碼"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString()))%></td>
                                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                                <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                                <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送區").ToString())%></td>
                                                <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著區").ToString())%></td>
                                                <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.板數","{0:N0}").ToString())%></td>
                                                <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨件費用","{0:N0}").ToString())%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (New_List_05.Items.Count == 0)
                                        {%>
                                    <tr>
                                        <td colspan="9" style="text-align: center">尚無資料</td>
                                    </tr>
                                    <% } %>
                                </table>
                                <table class="paytable">
                                    <tr>
                                        <th>小計：</th>
                                        <td>NT$ 
                            <asp:Label ID="subtotal_05" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>5%營業稅：</th>
                                        <td>NT$  
                            <asp:Label ID="tax_05" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                    <tr>
                                        <th>應收帳款：</th>
                                        <td>NT$   
                            <asp:Label ID="total_05" runat="server"></asp:Label>
                                            元</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <table class="table table-striped table-bordered templatemo-user-table" style="width: 50%">
                                <tr class="tr-only-hide">
                                    <th class="_th">計價模式</th>
                                    <th class="_th">實收運費</th>
                                    <th class="_th">稅額</th>
                                    <th class="_th">應收帳款</th>
                                </tr>
                                <asp:Repeater ID="New_List" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                            <td data-th="實收運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subtotal","{0:N0}").ToString())%></td>
                                            <td data-th="稅額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax","{0:N0}").ToString())%></td>
                                            <td data-th="應收帳款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total","{0:N0}").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (New_List.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                   
                
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="Panel2" runat="server">
                            <table class="table table-striped table-bordered templatemo-user-table">
                                <tr class="tr-only-hide">
                                    <th class="_th">序號</th>
                                    <th class="_th">發送日期</th>
                                    <th class="_th">客戶代碼</th>
                                    <th class="_th">客戶名稱</th>
                                    <th class="_th">貨號</th>
                                    <th class="_th">發送區</th>
                                    <th class="_th">到著區</th>
                                    <th class="_th">板數</th>
                                    <th class="_th">件數</th>
                                    <th class="_th">貨件費用</th>
                                    <th class="_th">偏遠區計價</th>
                                </tr>
                                <asp:Repeater ID="Repeater1" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="序號">
                                                <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.NO").ToString())%>
                                            </td>
                                            <td data-th="發送日期"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送日期","{0:yyyy/MM/dd}").ToString())%></td>
                                            <td data-th="客戶代碼"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶代碼").ToString()))%></td>
                                            <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.客戶名稱").ToString())%></td>
                                            <td data-th="貨號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.貨號").ToString())%></td>
                                            <td data-th="發送區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.發送站").ToString())%></td>
                                            <td data-th="到著區"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.到著站").ToString())%></td>
                                            <td data-th="板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.板數","{0:N0}").ToString())%></td>
                                            <td data-th="件數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.件數","{0:N0}").ToString())%></td>
                                            <td data-th="貨件費用"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cscetion_fee","{0:N0}").ToString())%></td>
                                            <td data-th="偏遠區計價"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.remote_fee","{0:N0}").ToString())%></td>

                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (Repeater1.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="11" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                            <table class="paytable">
                                <tr>
                                    <th>小計：</th>
                                    <td>NT$ 
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>5%營業稅：</th>
                                    <td>NT$  
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                                <tr>
                                    <th>應收帳款：</th>
                                    <td>NT$   
                            <asp:Label ID="Label3" runat="server"></asp:Label>
                                        元</td>
                                </tr>
                            </table>

                            <table class="table table-striped table-bordered templatemo-user-table" style="width: 50%">
                                <tr class="tr-only-hide">
                                    <th>計價模式</th>
                                    <th>實收運費</th>
                                    <th>稅額</th>
                                    <th>應收帳款</th>
                                </tr>
                                <asp:Repeater ID="Repeater2" runat="server">
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="計價模式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                            <td data-th="實收運費"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.subtotal","{0:N0}").ToString())%></td>
                                            <td data-th="稅額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.tax","{0:N0}").ToString())%></td>
                                            <td data-th="應收帳款"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.total","{0:N0}").ToString())%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (Repeater2.Items.Count == 0)
                                    {%>
                                <tr>
                                    <td colspan="4" style="text-align: center">尚無資料</td>
                                </tr>
                                <% } %>
                            </table>
                        </asp:Panel>
                        </ContentTemplate>
                </asp:UpdatePanel>
               

            </div>
        </div>
    </div>
</asp:Content>

