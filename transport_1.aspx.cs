﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

public partial class transport_1 : System.Web.UI.Page
{
    public int start_number
    {
        get { return Convert.ToInt32(ViewState["start_number"]); }
        set { ViewState["start_number"] = value; }
    }

    public int end_number
    {
        get { return Convert.ToInt32(ViewState["end_number"]); }
        set { ViewState["end_number"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string supplier_code_sel
    {
        get { return ViewState["supplier_code_sel"].ToString(); }
        set { ViewState["supplier_code_sel"] = value; }
    }

    public string supplier_name_sel
    {
        get { return ViewState["supplier_name_sel"].ToString(); }
        set { ViewState["supplier_name_sel"] = value; }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = Session["master_code"].ToString();
            if (manager_type == "0" || manager_type == "1" || manager_type == "2" || manager_type == "4")
            {
                supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
                if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
                {
                    switch (manager_type)
                    {
                        case "1":
                        case "2":
                            supplier_code = "";
                            break;
                        default:
                            supplier_code = "000";
                            break;
                    }
                }

            }

            //String ttScript = "if(event.keyCode==13) {event.keyCode=9;}";
            ////foreach (Object aObj in this.Page.Controls)
            ////{
            ////    if (aObj.GetType() == typeof(TextBox))
            ////    {
            ////        ((TextBox)aObj).Attributes.Add("onkeydown", ttScript);
            ////    }
            ////    else if (aObj.GetType() == typeof(DropDownList))
            ////    {
            ////        ((DropDownList)aObj).Attributes.Add("onkeydown", ttScript);
            ////    }
            ////}


            //foreach (Control control in form.Controls)
            //{
            //    if (control is TextBox)
            //    {
            //        ((TextBox)control).Attributes.Add("onkeydown", ttScript);
            //    }
            //    if (control is DropDownList)
            //    {
            //        ((DropDownList)control).Attributes.Add("onkeydown", ttScript);
            //    }
            //}



            #region 郵政縣市
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "select * from tbPostCity order by seq asc ";
            receive_city.DataSource = dbAdapter.getDataTable(cmd1);
            receive_city.DataValueField = "city";
            receive_city.DataTextField = "city";
            receive_city.DataBind();
            receive_city.Items.Insert(0, new ListItem("請選擇", ""));

            send_city.DataSource = dbAdapter.getDataTable(cmd1);
            send_city.DataValueField = "city";
            send_city.DataTextField = "city";
            send_city.DataBind();
            send_city.Items.Insert(0, new ListItem("請選擇", ""));
            #endregion


            //#region 到著站簡碼
            //SqlCommand cmd8 = new SqlCommand();
            //////cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 ");
            ////cmd8.CommandText = string.Format(@"select distinct arr.supplier_code,  sup.supplier_code + ' '+  sup.supplier_name as showname 
            ////                                    from ttArriveSites arr with(nolock)
            ////                                    left join   tbSuppliers sup with(nolock) on arr.supplier_code =  sup.supplier_code and sup.active_flag = 1 
            ////                                    where arr.supplier_code <> '' and arr.supplier_code is not null
            ////                                    ");
            //cmd8.CommandText = string.Format(@"select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname from tbSuppliers where active_flag  = 1 and show_trans = 1 order by supplier_code");
            //area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
            //area_arrive_code.DataValueField = "supplier_code";
            //area_arrive_code.DataTextField = "showname";
            //area_arrive_code.DataBind();
            //area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
            //#endregion

            #region 客代編號
            rbcheck_type_SelectedIndexChanged(null, null);
            #endregion

            #region 託運類別
            SqlCommand cmd2 = new SqlCommand();
            cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
            check_type.DataSource = dbAdapter.getDataTable(cmd2);
            check_type.DataValueField = "code_id";
            check_type.DataTextField = "code_name";
            check_type.DataBind();
            #endregion

            #region 傳票類別
            SqlCommand cmd3 = new SqlCommand();
            cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1  and code_id <> 51 and code_id <> 31 order by code_id ";
            subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
            subpoena_category.DataValueField = "code_id";
            subpoena_category.DataTextField = "code_name";
            subpoena_category.DataBind();
            #endregion

            //#region 商品類別
            //SqlCommand cmd4 = new SqlCommand();
            //cmd4.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S3' and active_flag = 1 ";
            //product_category.DataSource = dbAdapter.getDataTable(cmd4);
            //product_category.DataValueField = "code_id";
            //product_category.DataTextField = "code_name";
            //product_category.DataBind();
            //#endregion

            #region 特殊配送
            SqlCommand cmd5 = new SqlCommand();
            cmd5.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S4' and active_flag = 1 ";
            special_send.DataSource = dbAdapter.getDataTable(cmd5);
            special_send.DataValueField = "code_id";
            special_send.DataTextField = "code_name";
            special_send.DataBind();
            #endregion

            #region 配送時段
            SqlCommand cmd6 = new SqlCommand();
            cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
            time_period.DataSource = dbAdapter.getDataTable(cmd6);
            time_period.DataValueField = "code_id";
            time_period.DataTextField = "code_name";
            time_period.DataBind();
            time_period.SelectedValue = "不指定";
            #endregion


            #region 貨號區間(一筆式)
            SqlCommand cmd7 = new SqlCommand();
            cmd7.CommandText = "select start_number,end_number from tbCheckNumber With(nolock) where type = 1 ";
            DataTable DT = dbAdapter.getDataTable(cmd7);
            if (DT.Rows.Count > 0)
            {
                start_number = Convert.ToInt32(DT.Rows[0]["start_number"]);
                end_number = Convert.ToInt32(DT.Rows[0]["end_number"]);
            }
            #endregion

        }

    }


    protected void Clear()
    {
        dlcustomer_code.SelectedIndex = 0;
        customer_code.Text = "";
        order_number.Text = "";
        check_type.SelectedIndex = 0;
        receive_customer_code.Text = "";
        subpoena_category.SelectedIndex = 0;
        receive_tel1.Text = "";
        receive_tel1_ext.Text = "";
        receive_tel2.Text = "";
        receive_contact.Text = "";
        receiver_code.Text = "";
        receiver_id.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        area_arrive_code.Items.Clear();
        cbarrive.Checked = false;
        pieces.Text = "";
        plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        arrive_to_pay_freight.Text = "";
        arrive_to_pay_append.Text = "";
        send_contact.Text = "";
        send_tel.Text = "";
        send_city.SelectedIndex = 0;
        send_area.Items.Clear();
        send_address.Text = "";
        //donate_invoice_flag.Checked = false;
        //electronic_invoice_flag.Checked = false;
        //uniform_numbers.Text = "";
        //arrive_mobile.Text = "";
        //arrive_email.Text = "";
        invoice_desc.Value = "";
        //product_category.SelectedIndex = 0;
        special_send.SelectedIndex = 0;
        arrive_assign_date.Text = "";
        receipt_flag.Checked = false;
        pallet_recycling_flag.Checked = false;
        i_supplier_fee.Text = "";
        i_csection_fee.Text = "";
        i_remote_fee.Text = "";
        turn_board.Checked = false;
        upstairs.Checked = false;
        difficult_delivery.Checked = false;

    }


    protected void ClearbySave()
    {
        check_number.Text = "";
        order_number.Text = "";
        check_type.SelectedIndex = 0;
        receive_customer_code.Text = "";
        subpoena_category.SelectedIndex = 0;
        receive_tel1.Text = "";
        receive_tel1_ext.Text = "";
        receive_tel2.Text = "";
        receive_contact.Text = "";
        receiver_code.Text = "";
        receiver_id.Text = "";
        receive_city.SelectedIndex = 0;
        receive_area.Items.Clear();
        receive_address.Text = "";
        area_arrive_code.SelectedIndex = 0;
        cbarrive.Checked = false;
        pieces.Text = "";
        plates.Text = "";
        cbm.Text = "";
        collection_money.Text = "";
        arrive_to_pay_freight.Text = "";
        arrive_to_pay_append.Text = "";
        invoice_desc.Value = "";
        special_send.SelectedIndex = 0;
        arrive_assign_date.Text = "";
        receipt_flag.Checked = false;
        pallet_recycling_flag.Checked = false;
        i_supplier_fee.Text = "";
        i_csection_fee.Text = "";
        i_remote_fee.Text = "";
        turn_board.Checked = false;
        upstairs.Checked = false;
        difficult_delivery.Checked = false;

    }

    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
        }
    }


    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        customer_code.Text = dlcustomer_code.SelectedValue;
        CheckBox1_CheckedChanged(null, null);
        divReceiver.Visible = false;
        New_List.DataSource = null;
        New_List.DataBind();

        #region 到著站簡碼
        SqlCommand cmd8 = new SqlCommand();
        cmd8.Parameters.AddWithValue("@supplier_code_sel", supplier_code_sel.ToString());
        cmd8.Parameters.AddWithValue("@hctcode", "001");
        //cmd8.CommandText = string.Format(@"Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
        //                                    from tbSuppliers with(nolock)
        //                                    where active_flag  = 1 and show_trans = 1 and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
        //                                    order by supplier_code");

        cmd8.CommandText = string.Format(@"declare @all bit 
                                            select  @all=CASE when count(*)>0 then 1 else 0 end from tbSuppliers  where active_flag  = 1 and show_trans = 1  and cross_region = 1 and supplier_code =@supplier_code_sel
                                            if @all = 1 begin
                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        order by supplier_code
                                            end else begin 
	                                            Select supplier_code, supplier_name,supplier_code + ' '+  supplier_name as showname 
                                                                                        from tbSuppliers with(nolock)
                                                                                        where active_flag  = 1 and show_trans = 1 and ISNULL(supplier_code ,'') <> ''
                                                                                        and (cross_region = 1  or supplier_code =@supplier_code_sel or supplier_code =@hctcode)
                                                                                        order by supplier_code
                                            end");
        area_arrive_code.DataSource = dbAdapter.getDataTable(cmd8);
        area_arrive_code.DataValueField = "supplier_code";
        area_arrive_code.DataTextField = "showname";
        area_arrive_code.DataBind();
        area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
        #endregion

    }

    protected void rbcheck_type_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 客代編號
        SqlCommand cmd2 = new SqlCommand();
        string wherestr = "";
        if (supplier_code != "")
        {
            switch (manager_type)
            {
                case "0":
                case "1":
                case "4":
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                    break;
                default:
                    cmd2.Parameters.AddWithValue("@master_code", supplier_code);
                    wherestr = " and master_code like ''+@master_code+'%' ";
                    break;
            }
        }

        cmd2.CommandText = string.Format(@"select customer_code , customer_code+ '-' + customer_shortname as name  from tbCustomers with(Nolock) where stop_shipping_code = '0' and pricing_code = '" + rbcheck_type.SelectedValue + "' {0} order by customer_code asc ", wherestr);
        dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
        dlcustomer_code.DataValueField = "customer_code";
        dlcustomer_code.DataTextField = "name";
        dlcustomer_code.DataBind();
        dlcustomer_code.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion
        Clear_customer();


       


        //switch (rbcheck_type.SelectedValue)
        //{
        //    case "01":
        //    case "04":
        //        pieces.Visible = true;
        //        plates.Visible = false;
        //        cbm.Visible = false;
        //        lbUnit.Text = "板";
        //        break;
        //    case "02":
        //        pieces.Visible = false;
        //        plates.Visible = true;
        //        cbm.Visible = false;
        //        lbUnit.Text = "件";
        //        break;
        //    case "03":
        //        pieces.Visible = false;
        //        plates.Visible = false;
        //        cbm.Visible = true;
        //        lbUnit.Text = "才";
        //        break;
        //}

    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkshowsend.Checked)
        {
            //顯示寄件人資料
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.CommandText = @"select C.customer_name,C.telephone,C.shipments_city,C.shipments_area,C.shipments_road,C.customer_shortname , C.individual_fee,
                                  ISNULL(S.supplier_code,C.supplier_code) AS  supplier_code,
                                  ISNULL(S.supplier_shortname, CASE C.supplier_code When '001' THEN N'零擔' When '002' THEN N'流通' END ) AS supplier_name
                                  --S.supplier_code, S.supplier_name
                                  from tbCustomers C With(Nolock) 
                                  left join tbSuppliers S With(Nolock)  on S.supplier_code  = C.supplier_code 
                                  where customer_code = '" + customer_code.Text + "' ";
            dt = dbAdapter.getDataTable(cmd);
            if (dt.Rows.Count > 0)
            {
                send_contact.Text = dt.Rows[0]["customer_shortname"].ToString();   //名稱
                send_tel.Text = dt.Rows[0]["telephone"].ToString();           // 電話
                send_city.SelectedValue = dt.Rows[0]["shipments_city"].ToString().Trim();   //出貨地址-縣市
                city_SelectedIndexChanged(send_city, null);
                send_area.SelectedValue = dt.Rows[0]["shipments_area"].ToString().Trim();   //出貨地址-鄉鎮市區
                send_address.Text = dt.Rows[0]["shipments_road"].ToString().Trim();         //出貨地址-路街巷弄號
                lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();   //名稱
                supplier_code_sel = dt.Rows[0]["supplier_code"].ToString();      //區配code
                supplier_name_sel = dt.Rows[0]["supplier_name"].ToString();      //區配name

                bool individualfee = dt.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["individual_fee"]) : false;
                bool _noadmin = (new string[] { "3", "4", "5" }).Contains(manager_type);
                if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                {
                    individual_fee.Style.Add("display", "block");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
                else
                {
                    individual_fee.Style.Add("display", "none");
                    if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                }
            }
        }
    }


    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string ErrStr = "";
        lbErrQuant.Text = "";

        /* 20170727 託運類別 ... modify by lisa
         * 論板時、論小板時：【板數】與【件數】一定要提示填入數字
         * 論才時：【板數】與【件數】及【才數】皆要填入
         */
        bool individual = individual_fee.Style.Value.IndexOf("display:block") > -1;
        int i_tmp;
        switch (rbcheck_type.SelectedValue)
        {
            case "01":
            case "04":
                if (string.IsNullOrWhiteSpace(plates.Text) || string.IsNullOrWhiteSpace(pieces.Text))
                {
                    lbErrQuant.Text = "請輸入板數及件數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入板數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "02":
                if (string.IsNullOrWhiteSpace(pieces.Text))
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                break;
            case "03":
                if (string.IsNullOrWhiteSpace(cbm.Text)
                    || string.IsNullOrWhiteSpace(plates.Text)
                    || string.IsNullOrWhiteSpace(pieces.Text)
                    )
                {
                    lbErrQuant.Text = "請輸入板數、件數及才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(plates.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入板數";
                    plates.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(pieces.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入件數";
                    pieces.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }

                if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
                if (i_tmp <= 0)
                {
                    lbErrQuant.Text = "請輸入才數";
                    cbm.Focus();
                    lbErrQuant.Visible = true;
                    return;
                }
                break;
        }

        if (individual && i_supplier_fee.Text == "")
        {
            lbErrQuant.Text = "請輸入貨件運費";
            lbErrQuant.Visible = true;
            i_supplier_fee.Focus();
            return;
        }

        //花蓮、台東地區 強制輸入才數
        if (!int.TryParse(cbm.Text, out i_tmp)) i_tmp = -1;
        if (((receive_city.SelectedValue == "花蓮縣") || (receive_city.SelectedValue == "臺東縣")) && (i_tmp <= 0))
        {
            lbErrQuant.Text = "請輸入才數";
            cbm.Focus();
            lbErrQuant.Visible = true;
            return;
        }
        
        lbErrQuant.Visible = (lbErrQuant.Text != "");

        if (lbErrQuant.Text == "" && lbErr.Text == "")
        {

            try
            {

                int ttpieces = 0;
                int ttplates = 0;
                int ttcbm = 0;
                int ttcollection_money = 0;
                int ttarrive_to_pay = 0;
                int ttarrive_to_append = 0;
                int remote_fee = 0;
                int request_id = 0;
                int supplier_fee = 0;  //配送費用
                int cscetion_fee = 0;  //C配運價
                int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                       //int unit_price = 0;    //單位價格
                                       //int unit_price_c = 0;  //單位價格-C配


                #region 新增    
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@pricing_type", rbcheck_type.SelectedValue);                     //計價模式 (01:論板、02:論件、03論才、04論小板)
                    cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);                 //客代編號
                    cmd.Parameters.AddWithValue("@check_number", check_number.Text);                              //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
                    cmd.Parameters.AddWithValue("@order_number", order_number.Text);                              //訂單號碼
                    cmd.Parameters.AddWithValue("@check_type", check_type.SelectedValue.ToString());              //託運類別
                    cmd.Parameters.AddWithValue("@receive_customer_code", receive_customer_code.Text);            //收貨人編號
                    cmd.Parameters.AddWithValue("@subpoena_category", subpoena_category.SelectedValue.ToString());//傳票類別
                    cmd.Parameters.AddWithValue("@receive_tel1", receive_tel1.Text);                              //電話1
                    cmd.Parameters.AddWithValue("@receive_tel1_ext", receive_tel1_ext.Text.ToString());           //電話分機
                    cmd.Parameters.AddWithValue("@receive_tel2", receive_tel2.Text);                              //電話2
                    cmd.Parameters.AddWithValue("@receive_contact", receive_contact.Text);                        //收件人    
                    cmd.Parameters.AddWithValue("@receive_city", receive_city.SelectedValue.ToString());          //收件地址-縣市
                    cmd.Parameters.AddWithValue("@receive_area", receive_area.SelectedValue.ToString());          //收件地址-鄉鎮市區
                    cmd.Parameters.AddWithValue("@receive_address", receive_address.Text);                        //收件地址-路街巷弄號
                    if (!individual)
                    {
                        remote_fee = Utility.getremote_fee(receive_city.SelectedValue.ToString(), receive_area.SelectedValue.ToString(), receive_address.Text);
                    }
                    else
                    {
                        if (int.TryParse(i_remote_fee.Text, out i_tmp)) remote_fee = i_tmp;
                    }
                    cmd.Parameters.AddWithValue("@remote_fee", remote_fee);                                       //偏遠區加價
                    cmd.Parameters.AddWithValue("@area_arrive_code", area_arrive_code.SelectedValue);             //到著碼
                    cmd.Parameters.AddWithValue("@receive_by_arrive_site_flag", Convert.ToInt16(cbarrive.Checked));                //到站領貨 0:否、1:是
                    if (cbarrive.Checked)
                    {
                        cmd.Parameters.AddWithValue("@arrive_address", receive_address.Text);                     //到著碼地址
                    }

                    try
                    {
                        ttpieces = Convert.ToInt32(pieces.Text);
                    }
                    catch { }

                    try
                    {
                        ttplates = Convert.ToInt32(plates.Text);
                    }
                    catch { }

                    try
                    {
                        ttcbm = Convert.ToInt32(cbm.Text);
                    }
                    catch { }

                    try
                    {
                        ttcollection_money = Convert.ToInt32(collection_money.Text);
                    }
                    catch { }

                    try
                    {
                        ttarrive_to_pay = Convert.ToInt32(arrive_to_pay_freight.Text);
                    }
                    catch { }

                    try
                    {
                        ttarrive_to_append = Convert.ToInt32(arrive_to_pay_append.Text);
                    }
                    catch { }

                    cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
                    cmd.Parameters.AddWithValue("@plates", ttplates);                                             //板數
                    cmd.Parameters.AddWithValue("@cbm", ttcbm);                                                   //才數

                    switch (subpoena_category.SelectedValue)
                    {
                        case "11"://元付
                            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                //到付運費
                            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);              //到付追加
                            break;
                        case "21"://到付
                            cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                 //到付運費
                            break;
                        case "25"://元付-到付追加
                            cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                //到付追加
                            break;
                        case "41"://代收貨款
                            cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                     //代收金
                            break;
                    }

                    //cmd.Parameters.AddWithValue("@collection_money", ttcollection_money);                         //代收金
                    //cmd.Parameters.AddWithValue("@arrive_to_pay_freight", ttarrive_to_pay);                       //到付運費
                    //cmd.Parameters.AddWithValue("@arrive_to_pay_append", ttarrive_to_append);                     //到付追加
                    cmd.Parameters.AddWithValue("@send_contact", send_contact.Text);                              //寄件人
                    cmd.Parameters.AddWithValue("@send_tel", send_tel.Text.ToString());                           //寄件人電話
                    cmd.Parameters.AddWithValue("@send_city", send_city.SelectedValue.ToString());                //寄件人地址-縣市
                    cmd.Parameters.AddWithValue("@send_area", send_area.SelectedValue.ToString());                //寄件人地址-鄉鎮市區
                    cmd.Parameters.AddWithValue("@send_address", send_address.Text);                              //寄件人地址-號街巷弄號
                    cmd.Parameters.AddWithValue("@donate_invoice_flag", 0);                                       //是否發票捐贈 
                    cmd.Parameters.AddWithValue("@electronic_invoice_flag", 0);                                   //是否為電子發票
                    cmd.Parameters.AddWithValue("@uniform_numbers", "");                                          //統一編號
                                                                                                                  //cmd.Parameters.AddWithValue("@arrive_mobile", arrive_mobile.Text.ToString());                 //收件人-手機1  
                    cmd.Parameters.AddWithValue("@arrive_email", "");                                             //收件人-電子郵件
                                                                                                                  //cmd.Parameters.AddWithValue("@invoice_memo", invoice_memo.SelectedValue.ToString());        //備註
                    cmd.Parameters.AddWithValue("@invoice_desc", invoice_desc.Value);                             //說明
                                                                                                                  //cmd.Parameters.AddWithValue("@product_category", product_category.SelectedValue.ToString());  //商品種類
                    cmd.Parameters.AddWithValue("@special_send", special_send.SelectedValue.ToString());          //特殊配送
                    DateTime date;
                    cmd.Parameters.AddWithValue("@arrive_assign_date", DateTime.TryParse(arrive_assign_date.Text, out date) ? (object)date : DBNull.Value);                  //指定日
                    cmd.Parameters.AddWithValue("@time_period", time_period.SelectedValue.ToString());            //時段
                    cmd.Parameters.AddWithValue("@receipt_flag", Convert.ToInt16(receipt_flag.Checked));                           //是否回單
                    cmd.Parameters.AddWithValue("@pallet_recycling_flag", Convert.ToInt16(pallet_recycling_flag.Checked));         //是否棧板回收
                    if (pallet_recycling_flag.Checked)
                    {
                        cmd.Parameters.AddWithValue("@Pallet_type", Pallet_type.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Pallet_type", "");
                    }
                    cmd.Parameters.AddWithValue("@turn_board", Convert.ToInt16(turn_board.Checked));                               //翻版
                    cmd.Parameters.AddWithValue("@upstairs", Convert.ToInt16(upstairs.Checked));                                   //上樓
                    cmd.Parameters.AddWithValue("@difficult_delivery", Convert.ToInt16(difficult_delivery.Checked));               //困配

                    if (int.TryParse(turn_board_fee.Text, out i_tmp) && turn_board.Checked)
                    {
                        cmd.Parameters.AddWithValue("@turn_board_fee", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@turn_board_fee", 0);
                    }

                    if (int.TryParse(upstairs_fee.Text, out i_tmp) && upstairs.Checked)
                    {
                        cmd.Parameters.AddWithValue("@upstairs_fee", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@upstairs_fee", 0);
                    }

                    if (int.TryParse(difficult_fee.Text, out i_tmp) && difficult_delivery.Checked)
                    {
                        cmd.Parameters.AddWithValue("@difficult_fee", i_tmp.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@difficult_fee", 0);
                    }

                    cmd.Parameters.AddWithValue("@add_transfer", 0);                                              //是否轉址
                    cmd.Parameters.AddWithValue("@sub_check_number", "000");                                      //次貨號
                    cmd.Parameters.AddWithValue("@supplier_code", supplier_code_sel);                             //配送商代碼
                    cmd.Parameters.AddWithValue("@supplier_name", supplier_name_sel);                             //配送商名稱
                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);                               //建立人員
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now);                                          //建立時間
                    cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間
                    cmd.Parameters.AddWithValue("@print_date", DateTime.TryParse(Shipments_date.Text, out date) ? (object)date : DBNull.Value);   //出貨日期
                    cmd.Parameters.AddWithValue("@print_flag", 0);                                                //是否列印
                    if (individual)
                    {
                        if (int.TryParse(i_supplier_fee.Text, out i_tmp))
                        {
                            cmd.Parameters.AddWithValue("@supplier_fee", i_tmp.ToString());
                            cmd.Parameters.AddWithValue("@total_fee", i_tmp.ToString());
                        }
                        if (int.TryParse(i_csection_fee.Text, out i_tmp))
                            cmd.Parameters.AddWithValue("@csection_fee", i_tmp.ToString());
                    }


                    //cmd.CommandText = dbAdapter.SQLdosomething("tcDeliveryRequests", cmd, "insert");
                    //dbAdapter.execNonQuery(cmd);
                    cmd.CommandText = dbAdapter.genInsertComm("tcDeliveryRequests", true, cmd);        //新增

                    if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                    {
                        if (!individual)
                        {
                            SqlCommand cmd2 = new SqlCommand();
                            cmd2.CommandText = "usp_GetShipFeeByRequestId";
                            cmd2.CommandType = CommandType.StoredProcedure;
                            cmd2.Parameters.AddWithValue("@request_id", request_id.ToString());
                            using (DataTable dt = dbAdapter.getDataTable(cmd2))
                            {
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    status_code = Convert.ToInt32(dt.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                    if (status_code == 1)
                                    {
                                        supplier_fee = dt.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["supplier_fee"]) : 0;       //配送費用
                                        cscetion_fee = dt.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["cscetion_fee"]) : 0;       //C配運價
                                                                                                                                                           //unit_price = Convert.ToInt32(dt.Rows[0]["supplier_unit_fee"]);    //單位價格
                                                                                                                                                           //unit_price_c = Convert.ToInt32(dt.Rows[0]["cscetion_unit_fee"]);  //單位價格-C配

                                        //回寫到託運單的費用欄位
                                        using (SqlCommand cmd3 = new SqlCommand())
                                        {
                                            cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                            cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                            cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", request_id.ToString());
                                            cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                            try
                                            {
                                                dbAdapter.execNonQuery(cmd3);
                                            }
                                            catch (Exception ex)
                                            {
                                                string strErr = string.Empty;
                                                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                                strErr = "一筆式託運單-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                                //錯誤寫至Log
                                                PublicFunction _fun = new PublicFunction();
                                                _fun.Log(strErr, "S");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion



                #region 常用收件人通訊錄
                if (cbSaveReceive.Checked && receiver_id.Text == "")
                {
                    if (!string.IsNullOrEmpty(customer_code.Text.Trim()) && !string.IsNullOrEmpty(receive_contact.Text.Trim()))
                    {
                        //先判斷，如果有姓名、編號、地址、已存在就不再新增
                        using (SqlCommand cmd5 = new SqlCommand())
                        {

                            cmd5.Parameters.AddWithValue("@customer_code", customer_code.Text.Trim());
                            cmd5.Parameters.AddWithValue("@receiver_code", receiver_code.Text.Trim());
                            cmd5.Parameters.AddWithValue("@receiver_name", receive_contact.Text.Trim());
                            cmd5.Parameters.AddWithValue("@address_city", receive_city.SelectedValue);
                            cmd5.Parameters.AddWithValue("@address_area", receive_area.SelectedValue);
                            cmd5.Parameters.AddWithValue("@address_road", receive_address.Text.Trim());


                            cmd5.CommandText = string.Format(@"Select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  and A.receiver_code =@receiver_code and  A.receiver_name=@receiver_name 
                                          and A.customer_code =@customer_code and  A.address_city =@address_city and  address_area = @address_area 
                                          and address_road = @address_road ");
                            using (DataTable dt = dbAdapter.getDataTable(cmd5))
                            {
                                if (dt == null || dt.Rows.Count == 0)
                                {
                                    SqlCommand cmd4 = new SqlCommand();
                                    cmd4.Parameters.AddWithValue("@customer_code", customer_code.Text);                     //客戶代碼
                                    cmd4.Parameters.AddWithValue("@receiver_code", receiver_code.Text);                     //收貨人代碼
                                    cmd4.Parameters.AddWithValue("@receiver_name", receive_contact.Text);                   //收貨人名稱
                                    cmd4.Parameters.AddWithValue("@tel", receive_tel1.Text);                                //電話
                                    cmd4.Parameters.AddWithValue("@tel_ext", receive_tel1_ext.Text);                        //分機
                                    cmd4.Parameters.AddWithValue("@tel2", receive_tel2.Text);                               //電話2
                                    cmd4.Parameters.AddWithValue("@address_city", receive_city.SelectedValue.ToString());   //地址-縣市
                                    cmd4.Parameters.AddWithValue("@address_area", receive_area.SelectedValue.ToString());   //地址 - 區域
                                    cmd4.Parameters.AddWithValue("@address_road", receive_address.Text);                    //地址-路段
                                    cmd4.Parameters.AddWithValue("@cuser", Session["account_code"]);                        //建立人員
                                    cmd4.Parameters.AddWithValue("@cdate", DateTime.Now);                                   //建立時間
                                    cmd4.Parameters.AddWithValue("@uuser", Session["account_code"]);                        //更新人員
                                    cmd4.Parameters.AddWithValue("@udate", DateTime.Now);                                   //更新時間 
                                    cmd4.CommandText = dbAdapter.SQLdosomething("tbReceiver", cmd4, "insert");
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd4);
                                    }
                                    catch (Exception ex)
                                    {
                                        string strErr = string.Empty;
                                        if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                        strErr = "一筆式託運單-儲存常用通訊人" + Request.RawUrl + strErr + ": " + ex.ToString();


                                        //錯誤寫至Log
                                        PublicFunction _fun = new PublicFunction();
                                        _fun.Log(strErr, "S");
                                    }
                                }
                            }

                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {

                string strErr = string.Empty;
                ErrStr += ex.ToString();
                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                strErr = "一筆式託運單" + Request.RawUrl + strErr + ": " + ex.ToString();

                //錯誤寫至Log
                PublicFunction _fun = new PublicFunction();
                _fun.Log(strErr, "S");
            }
            if (ErrStr == "")
            {
                ClearbySave();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增完成');</script>", false);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('新增失敗:" + ErrStr + "');</script>", false);
            }
            return;

        }

    }

    protected void check_number_TextChanged(object sender, EventArgs e)
    {
        //託運單號
        //暫時僅用10碼，且貨號前方不補零。新竹規則MOD(num, 7) 前9碼 + 0~6檢碼
        //2019/09/20 修正 可10或12碼 
        //2019/09/27 修正 只有圓通可12碼其餘依舊10碼 
        lbErr.Text = "";
        if (dlcustomer_code.SelectedValue.Length <= 0)
        {
            lbErr.Text = "請先選擇客代編號。";
        }
        else { 
        String value = dlcustomer_code.SelectedValue.Substring(0, 7);

        if (value == "0000185")
        {
            if (check_number.Text.Length != 12)
            {
                lbErr.Text = "託運單號錯誤，長度應為12碼。";
            }
        }
        else {
            if (check_number.Text.Length != 10)
            {
                lbErr.Text = "託運單號錯誤，長度應為10。";
            }
            if (check_number.Text.Length == 10)
            {
                //Regex regex = new Regex("^[0-9]*$");
                Regex reg = new Regex("^[0-9]*$");
                if (!reg.IsMatch(check_number.Text))
                {
                    lbErr.Text = "非有效的託運單號，請檢查編號是否正確";
                }
                else {
                    int num = Convert.ToInt32(check_number.Text.Substring(0, 9));
                    //if (num >= start_number && num <= end_number)
                    //{
                    int chk = num % 7;
                    int lastnum = Convert.ToInt32(check_number.Text.Substring(9, 1));
                    if (lastnum != chk)
                    {
                        lbErr.Text = "非有效的託運單號，請檢查編號是否正確";
                    }
                }
            }
        }
        }
        //}
        //else
        //{
        //    lbErr.Text = "非有效的託運單號,應落在" + start_number.ToString() + " ~ " + end_number.ToString() + "之間，請檢查編號是否正確";
        //}

        //}
        lbErr.Visible = (lbErr.Text != "");
    }

    protected void cbarrive_CheckedChanged(object sender, EventArgs e)
    {
        if (cbarrive.Checked && area_arrive_code.SelectedValue != "")
        {
            receive_address.Text = area_arrive_code.SelectedItem.Text + "站址";
        }

        //area_arrive_code.Enabled = cbarrive.Checked;

    }

    protected void receive_area_SelectedIndexChanged(object sender, EventArgs e)
    {
        #region 到著站簡碼
        //SqlCommand cmd7 = new SqlCommand();
        ////cmd7.CommandText = "select arrive_code, supplier_name, arrive_code + '' + supplier_name as showname from tbAreaArriveCodes where city='" + receive_city.SelectedValue + "' and area='"+ receive_area.SelectedValue + "'";
        //cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
        //                                       left join tbSuppliers B on A.supplier_code  = B.supplier_code 
        //                                       where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
        //area_arrive_code.DataSource = dbAdapter.getDataTable(cmd7);
        //area_arrive_code.DataValueField = "supplier_code";
        //area_arrive_code.DataTextField = "showname";
        //area_arrive_code.DataBind();
        //if (area_arrive_code.Items.Count > 0) area_arrive_code.SelectedIndex = 0;
        //area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));

        using (SqlCommand cmd7 = new SqlCommand())
        {  
            cmd7.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                                   left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                   where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", receive_city.SelectedValue, receive_area.SelectedValue);
            using (DataTable  dt = dbAdapter.getDataTable(cmd7))
            {
                if (dt != null && dt.Rows.Count >0)
                {
                    try
                    {
                        area_arrive_code.SelectedValue = dt.Rows[0]["supplier_code"].ToString();
                    }
                    catch { }
                }
            }
        }

        #endregion

        cbarrive_CheckedChanged(null, null);
    }

    protected void Clear_customer()
    {
        customer_code.Text = "";
        send_contact.Text = "";          //名稱
        send_tel.Text = "";              // 電話
        send_city.SelectedValue = "";    //出貨地址-縣市
        city_SelectedIndexChanged(send_city, null);
        send_area.SelectedValue = "";   //出貨地址-鄉鎮市區
        send_address.Text = "";         //出貨地址-路街巷弄號
        lbcustomer_name.Text = "";
    }

    protected void area_arrive_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        cbarrive_CheckedChanged(null, null);
    }

    protected void search_Click(object sender, EventArgs e)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                if (keyword.Text != "")
                {
                    cmd.Parameters.AddWithValue("@receiver_code", keyword.Text);
                    cmd.Parameters.AddWithValue("@receiver_name", keyword.Text);
                    strWhereCmd += " and (A.receiver_code like '%'+@receiver_code+'%' or A.receiver_name like '%'+@receiver_name+'%') ";
                }

                if (customer_code.Text != "")
                {
                    cmd.Parameters.AddWithValue("@customer_code", customer_code.Text.Length >= 7 ? customer_code.Text.Substring(0, 7) : customer_code.Text);
                    strWhereCmd += " and (A.customer_code like ''+@customer_code+'%') ";
                }

                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";
                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    New_List.DataSource = dt;
                    New_List.DataBind();
                }
            }
        }
        

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {

            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@receiver_id", ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim());
            wherestr += " AND A.receiver_id=@receiver_id";

            cmd.CommandText = string.Format(@"SELECT A.*
                                              FROM tbReceiver A                                                
                                              WHERE 0=0  {0} ", wherestr);


            DataTable DT = dbAdapter.getDataTable(cmd);
            if (DT.Rows.Count > 0)
            {

                receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                // DT.Rows[0]["customer_code"].ToString().Trim();
                receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                city_SelectedIndexChanged(receive_city, null);
                receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                receive_area_SelectedIndexChanged(null, null);
                receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                divReceiver.Visible = false;
            }
        }

    }

    protected void btnRecsel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = true;
        search_Click(null, null);
    }

    protected void cancel_Click(object sender, EventArgs e)
    {
        divReceiver.Visible = false;
    }

    protected void receive_contact_TextChanged(object sender, EventArgs e)
    {
        if (receive_contact.Text != "")
        {
            entertry("receive_contact");
        }
    }


    protected void receiver_code_TextChanged(object sender, EventArgs e)
    {
        if (receiver_code.Text != "")
        {
            entertry("receiver_code");

        }
    }

    protected void entertry(string control)
    {
        if (dlcustomer_code.SelectedValue != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string strWhereCmd = "";
                cmd.Parameters.Clear();

                #region 關鍵字
                switch (control)
                {
                    case "receive_contact":
                        cmd.Parameters.AddWithValue("@receiver_name", receive_contact.Text);
                        strWhereCmd += " and  A.receiver_name like '%'+@receiver_name+'%' ";
                        break;
                    case "receiver_code":
                        cmd.Parameters.AddWithValue("@receiver_code", receiver_code.Text);
                        strWhereCmd += " and  A.receiver_code like '%'+@receiver_code+'%' ";
                        break;
                }
                strWhereCmd += " and A.customer_code like '" + dlcustomer_code.SelectedValue.Substring(0, 7) + "%' ";

                #endregion

                cmd.CommandText = string.Format(@"select A.*
                                          from tbReceiver A  with(nolock)                                       
                                          where 1= 1  {0} order by A.receiver_code", strWhereCmd);
                using (DataTable DT = dbAdapter.getDataTable(cmd))
                {
                    if (DT != null && DT.Rows.Count > 0)
                    {
                        if (DT.Rows.Count == 1)
                        {
                            receiver_id.Text = "";
                            receiver_code.Text = "";
                            receive_contact.Text = "";
                            receive_tel1.Text = "";
                            receive_tel1_ext.Text = "";
                            receive_tel2.Text = "";
                            receive_city.SelectedValue = "";
                            receive_area.SelectedValue = "";
                            receive_address.Text = "";

                            receiver_id.Text = Convert.ToString(DT.Rows[0]["receiver_id"]);
                            receiver_code.Text = DT.Rows[0]["receiver_code"].ToString().Trim();
                            receive_contact.Text = DT.Rows[0]["receiver_name"].ToString().Trim();
                            receive_tel1.Text = DT.Rows[0]["tel"].ToString().Trim();
                            receive_tel1_ext.Text = DT.Rows[0]["tel_ext"].ToString().Trim();
                            receive_tel2.Text = DT.Rows[0]["tel2"].ToString().Trim();
                            receive_city.SelectedValue = DT.Rows[0]["address_city"].ToString().Trim();
                            city_SelectedIndexChanged(receive_city, null);
                            receive_area.SelectedValue = DT.Rows[0]["address_area"].ToString().Trim();
                            receive_area_SelectedIndexChanged(null, null);
                            receive_address.Text = DT.Rows[0]["address_road"].ToString().Trim();
                            divReceiver.Visible = false;
                        }
                        else
                        {
                            New_List.DataSource = DT;
                            New_List.DataBind();
                            divReceiver.Visible = true;
                        }

                    }

                }
            }
        }
        
    }

}