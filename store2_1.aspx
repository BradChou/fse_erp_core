﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterStore.master" AutoEventWireup="true" CodeFile="store2_1.aspx.cs" Inherits="store2_1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="js/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {            
            $(".btn_view").click(function () {
                showBlockUI();
            });

            jQuery(function ($) {
                var pageParts = $(".paginate");
                var numPages = pageParts.length;
                var perPage = 10;
                pageParts.slice(perPage).hide();                
                $("#page-nav").pagination({
                    items: numPages,
                    itemsOnPage: perPage,
                    cssStyle: "light-theme",
                    prevText:'上一頁',
                    nextText:'下一頁',
                    onPageClick: function (pageNum) {                        
                        var start = perPage * (pageNum - 1);
                        var end = start + perPage;

                        pageParts.hide()
                                 .slice(start, end).show();
                    }
                });
            });
        });
      

        function showBlockUI() {
            $.blockUI({
                message: '<p><h1>查詢中,請稍後<h1></p>',
                css:
                {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
        }


    </script>

    <style>
        ._th {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">趟次資料查詢</h2>
            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label>查詢區間：</label>
                    <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>~
                    <asp:TextBox ID="edate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                    <label>場區：</label>       
                    <asp:DropDownList ID="Area" runat="server" class="form-control"></asp:DropDownList>             
                    <%--<asp:DropDownList ID="Area" runat="server" class="form-control " onselectedindexchanged="Area_SelIndexChanged" AutoPostBack="True"></asp:DropDownList>--%>                      
                    <label>車行：</label>
                    <asp:TextBox ID="car_retailer_text" runat="server" class="form-control " ></asp:TextBox>
                    <%--<label>所有權：</label>
                    <asp:DropDownList ID="dlownership" runat="server" class="form-control"></asp:DropDownList>

                     <label>使用單位：</label>
                    <asp:DropDownList ID="dldept" runat="server" class="form-control"></asp:DropDownList>

                    <label>車行：</label>
                    <asp:DropDownList ID="ddl_car_retailer" runat="server" class="form-control "></asp:DropDownList>--%>

                    
                   
                   <%-- <span class="checkbox checkbox-success" style="left: 0px; top: 0px">
                        <asp:CheckBox ID="chk_inspection" class="font-weight-400" runat="server" Text="本月驗車"  />
                    </span>--%>
                    <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查　詢" OnClick="search_Click" />
                    <%--<asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport_Click"   />--%>
                    <asp:Button ID="btExport2" CssClass="btn btn-primary" runat="server" Text="下　載" OnClick="btExport2_Click"   />
                </div>

                <%--<div class="form-group form-inline">
                    <label>司機：</label>
                    <asp:TextBox ID="driver" runat="server" class="form-control " ></asp:TextBox>
                    <label>關鍵字：</label>
                    <asp:TextBox ID="keyword" class="form-control " runat="server" ></asp:TextBox>
                   
                </div>

                <div class="form-group" style="float: right;">
                    <a href="assets1_1_edit.aspx"><span class="btn btn-warning glyphicon glyphicon-plus">新增</span></a>
                </div>                
                <p class="text-primary">※點選任一車輛可查詢及修改</p>--%>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th  rowspan="2" class="_th" style ="vertical-align:middle">序號</th>
                        <th  rowspan="2" class="_th" style ="vertical-align:middle">車老闆</th>
                        <th  rowspan="2" class="_th" style ="vertical-align:middle">運務士</th>
                        <th  rowspan="2" class="_th" style ="vertical-align:middle">車號</th>
                        <th colspan="6"class="_th">應收項目</th>
                        <th colspan="6"class="_th">應付項目</th>                        
                        <th rowspan="2" class="_th" style ="vertical-align:middle">收入總計</th>
                        
                    </tr>
                    <tr class="tr-only-hide">
                        
                        <th class="_th">主線</th>
                        <th class="_th">爆量</th>
                        <th class="_th">店到店</th>
                        <th class="_th">回頭車</th>
                        <th class="_th">加派車</th>
                        <th class="_th">應收總計</th>
                        <th class="_th">主線</th>
                        <th class="_th">爆量</th>
                        <th class="_th">店到店</th>
                        <th class="_th">回頭車</th>
                        <th class="_th">加派車</th>
                        <th class="_th">應付總計</th>
                        
                        
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <ItemTemplate>
                            <tr class ="paginate">
                                <td>
                                    <%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.序號").ToString())%>
                                </td>
                                <td data-th="車老闆"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.車老闆").ToString())%></td>
                                <td data-th="運務士"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.運務士").ToString())%></td>
                                <td data-th="車號"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.車號").ToString())%></td>
                                <td data-th="主線"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收主線").ToString())%></td>
                                <td data-th="爆量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收爆量").ToString())%> </td>
                                <td data-th="店到店"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收店到店").ToString())%></td>
                                <td data-th="回頭車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收回頭車").ToString())%></td>
                                <td data-th="加派車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收加派車").ToString())%></td>
                                <td data-th="應收總計"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應收總計").ToString())%></td>
                                <td data-th="主線"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付主線").ToString())%></td>
                                <td data-th="爆量"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付爆量").ToString())%></td>
                                <td data-th="店到店"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付店到店").ToString())%></td>
                                <td data-th="回頭車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付回頭車").ToString())%></td>
                                <td data-th="加派車"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付加派車").ToString())%></td>
                                <td data-th="應付總計"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.應付總計").ToString())%></td>
                                <td data-th="收入總計"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.收入總計").ToString())%></td>
                                
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count-1 == 0)
                        {%>
                    <tr>
                        <td colspan="17" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
                共 <span class="text-danger">
                    <asp:Literal ID="ltotalpages" runat="server"></asp:Literal>
                </span>筆
                <div id="page-nav" class="page"></div>
            </div>
        </div>
    </div>
</asp:Content>

