﻿using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

public partial class loan4_1 : System.Web.UI.Page
{

    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }


            readdata();
        }
    }

    private void readdata()
    {
        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (car_license.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@car_license", car_license.Text.ToString());
            wherestr += "  and A.car_license like '%'+@car_license+'%'";
        }


        objCommand.CommandText = string.Format(@"select A.* , D.user_name , A.udate
                                                    from ttLoan  A with(nolock) 
                                                    left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                    where 1=1 {0}", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;
        }

    }

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }

    //protected void btExport_Click(object sender, EventArgs e)
    //{
    //    DataTable dt = GetExportDataTable();
    //    if (dt.Rows.Count <= 0)
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
    //        return;
    //    }

    //    string strTitle = @"貸款明細{0}";
    //    strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

    //    System.Web.HttpBrowserCapabilities browser = Request.Browser;
    //    strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
    //                    ? strTitle
    //                    : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

    //    // Create the workbook
    //    XLWorkbook workbook = new XLWorkbook();
    //    //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

    //    var ds = new DataSet();
    //    ds.Tables.Add(dt);
    //    workbook.Worksheets.Add(ds);

    //    // Prepare the response
    //    HttpResponse httpResponse = Response;
    //    httpResponse.Clear();
    //    httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //    httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

    //    // Flush the workbook to the Response.OutputStream
    //    using (MemoryStream memoryStream = new MemoryStream())
    //    {
    //        workbook.SaveAs(memoryStream);
    //        memoryStream.WriteTo(httpResponse.OutputStream);
    //        memoryStream.Close();
    //    }
    //    httpResponse.End();
    //}
    //protected DataTable GetExportDataTable()
    //{

    //    DataTable dt = DT;
    //    var query = (from p in dt.AsEnumerable()
    //                 select new
    //                 {
    //                     車牌 = p.Field<string>("car_license"),
    //                     車價 = (p["car_price"] == DBNull.Value) ? 0 : p.Field<Int32>("car_price"),
    //                     貸款金額 = (p["loan_price"] == DBNull.Value) ? 0 : p.Field<Int32>("loan_price"),
    //                     年利率 = p.Field<double>("annual_rate"),
    //                     頭款 = p.Field<Int32>("first_payment"),
    //                     貸款年限 = p.Field<double>("loan_period"),
    //                     貸款起始日 = p.Field<DateTime>("loan_startdate"),
    //                     分期付款金額 = (p["installment_price"] == DBNull.Value ) ? 0 : p.Field<Int32>("installment_price"),                         
    //                     分期付款次數 = (p["installment_count"] == DBNull.Value) ? 0 : p.Field<Int32>("installment_count"),
    //                     實際付款次數 = (p["paid_count"] == DBNull.Value) ? 0 : p.Field<Int32>("paid_count"),
    //                     利息總額 = (p["interest"] == DBNull.Value) ? 0 : p.Field<Int32>("interest")
    //                 }).ToList();

    //    DataTable _dt = ToDataTable(query);

    //    //sheet name
    //    string strTitle = @"貸款明細";
    //    strTitle = string.Format(strTitle);
    //    _dt.TableName = strTitle;
    //    _dt.Dispose();
    //    return _dt;
    //}

    ///// <summary>LIST TO DATATABLE</summary>
    ///// <typeparam name="T"></typeparam>
    ///// <param name="items"></param>
    ///// <returns></returns>
    //public static DataTable ToDataTable<T>(List<T> items)
    //{
    //    //DataTable dataTable = new DataTable(typeof(T).Name);

    //    DataTable dataTable = new DataTable();

    //    //Get all the properties
    //    PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
    //    foreach (PropertyInfo prop in Props)
    //    {
    //        //Defining type of data column gives proper data table 
    //        var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
    //        //Setting column names as Property names
    //        dataTable.Columns.Add(prop.Name, type);
    //    }
    //    foreach (T item in items)
    //    {
    //        var values = new object[Props.Length];
    //        for (int i = 0; i < Props.Length; i++)
    //        {
    //            //inserting property values to datatable rows
    //            values[i] = Props[i].GetValue(item, null);
    //        }
    //        dataTable.Rows.Add(values);
    //    }
    //    //put a breakpoint here and check datatable
    //    return dataTable;
    //}


    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Print1":  //公司
                #region 
                SqlCommand objCommand = new SqlCommand();
                objCommand.CommandText = string.Format(@" Select A.id, A.car_license, A.car_price, A.first_payment, A.loan_price, A.annual_rate, A.loan_period, A.annual_pay_count, A.loan_startdate,
                                                          A.installment_price, A.installment_count,  A.interest, A.loan_company, A.memo, info.paid_count
                                                          from  ttLoan A with(nolock) 
                                                          CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 0) info
                                                          Where 1=1 and A.id =@id ");
                objCommand.Parameters.AddWithValue("@id", e.CommandArgument);
                using (DataTable dt = dbAdapter.getDataTable(objCommand))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        using (ExcelPackage p = new ExcelPackage())
                        {
                            int ttRow = 0;    //列                   

                            ExcelPackage pck = new ExcelPackage();
                            var sheet1 = pck.Workbook.Worksheets.Add("車款分期償還計畫表");

                            #region sheet1                
                            //檔案邊界
                            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                            sheet1.PrinterSettings.HorizontalCentered = true;

                            //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                            sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小 
                            sheet1.PrinterSettings.RepeatRows = new ExcelAddress("$1:$4");
                            sheet1.DefaultColWidth = 14;   // 默认所有列宽
                            sheet1.DefaultRowHeight = 22;  // 默认所有行高

                            sheet1.Cells.Style.Font.Name = "新細明體";
                            sheet1.Cells[1, 1, 1, 8].Merge = true; //合併儲存格
                            sheet1.Cells[1, 1, 1, 8].Value = "車款分期償還計畫表";
                            sheet1.Cells[1, 1, 1, 8].Style.Font.Bold = true;
                            sheet1.Cells[1, 1, 1, 8].Style.Font.Size = 14;
                            sheet1.Cells[1, 1, 1, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中

                            sheet1.Cells[3, 2].Value = "承租方";
                            sheet1.Cells[3, 5].Value = "貸款金額";
                            sheet1.Cells[3, 6].Value = Convert.ToInt32(dt.Rows[0]["loan_price"]);
                            sheet1.Cells[3, 6].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[3, 7].Value = "分期付款額";
                            sheet1.Cells[3, 8].Value = Convert.ToInt32(dt.Rows[0]["installment_price"]);
                            sheet1.Cells[3, 8].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[4, 2].Value = "車號";
                            sheet1.Cells[4, 3].Value = dt.Rows[0]["car_license"].ToString();
                            sheet1.Cells[4, 5].Value = "年利率";
                            sheet1.Cells[4, 6].Value = Convert.ToDouble(dt.Rows[0]["annual_rate"]);
                            sheet1.Cells[4, 6].Style.Numberformat.Format = "#,##0.00";
                            sheet1.Cells[4, 7].Value = "分期付款次數";
                            sheet1.Cells[4, 8].Value = Convert.ToInt32(dt.Rows[0]["installment_count"]);

                            sheet1.Cells[5, 2].Value = "車價";
                            sheet1.Cells[5, 3].Value = Convert.ToInt32(dt.Rows[0]["car_price"]);
                            sheet1.Cells[5, 3].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[5, 5].Value = "貸款年限";
                            sheet1.Cells[5, 6].Value = Convert.ToInt32(dt.Rows[0]["loan_period"]);
                            sheet1.Cells[5, 6].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[5, 7].Value = "實際付款次數";
                            sheet1.Cells[5, 8].Value = Convert.ToInt32(dt.Rows[0]["paid_count"]);
                            sheet1.Cells[5, 8].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[6, 2].Value = "加裝尾門";
                            sheet1.Cells[6, 5].Value = "每年付款次數";
                            sheet1.Cells[6, 6].Value = Convert.ToInt32(dt.Rows[0]["annual_pay_count"]);
                            sheet1.Cells[6, 6].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[6, 7].Value = "利息總額";
                            sheet1.Cells[6, 8].Value = Convert.ToInt32(dt.Rows[0]["interest"]);
                            sheet1.Cells[6, 8].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[7, 2].Value = "合計";
                            sheet1.Cells[7, 3].Value = Convert.ToInt32(dt.Rows[0]["car_price"]);
                            sheet1.Cells[7, 3].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[7, 5].Value = "貸款起始日";
                            sheet1.Cells[7, 6].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).ToString("yyyy/MM/dd");
                            sheet1.Cells[9, 2].Value = "頭款";
                            sheet1.Cells[9, 3].Value = Convert.ToInt32(dt.Rows[0]["first_payment"]);
                            sheet1.Cells[9, 3].Style.Numberformat.Format = "#,##0";

                            sheet1.Cells[3, 3, 9, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet1.Cells[3, 6, 9, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet1.Cells[3, 8, 9, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            sheet1.Cells[1, 1, 9, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;


                            sheet1.Cells[3, 5, 7, 8].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Right.Style = ExcelBorderStyle.Medium;

                            sheet1.Cells[9, 2, 9, 3].Style.Border.Top.Style = ExcelBorderStyle.Medium; //框線
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;


                            sheet1.Cells[3, 2, 7, 3].Style.Border.Top.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Bottom.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Left.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 2, 7, 3].Style.Border.Right.Color.SetColor(Color.DarkRed);

                            sheet1.Cells[3, 5, 7, 8].Style.Border.Top.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Bottom.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Left.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 5, 7, 8].Style.Border.Right.Color.SetColor(Color.DarkRed);

                            sheet1.Cells[9, 2, 9, 3].Style.Border.Top.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Bottom.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Left.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[9, 2, 9, 3].Style.Border.Right.Color.SetColor(Color.DarkRed);



                            ttRow = 11;
                            sheet1.Cells[ttRow, 1].Value = "付款編號";
                            sheet1.Cells[ttRow, 2].Value = "日期";
                            sheet1.Cells[ttRow, 3].Value = "期初餘額";
                            sheet1.Cells[ttRow, 4].Value = "分期付款額";
                            sheet1.Cells[ttRow, 5].Value = "本金";
                            sheet1.Cells[ttRow, 6].Value = "利息";
                            sheet1.Cells[ttRow, 7].Value = "期末餘額";
                            sheet1.Cells[ttRow, 8].Value = "累計利息";
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);//设置单元格背景色
                            ttRow += 1;

                            SqlCommand cmdpay = new SqlCommand();
                            {
                                cmdpay.Parameters.AddWithValue("@loan_id", e.CommandArgument);
                                cmdpay.Parameters.AddWithValue("@type", 0);
                                cmdpay.CommandText = @"Select * from  ttRepayment  with(nolock) 
                                         Where loan_id = @loan_id and type=@type";
                                DataTable dtpay = dbAdapter.getDataTable(cmdpay);
                                if (dtpay.Rows.Count > 0)
                                {
                                    for (int i = 0; i <= dtpay.Rows.Count - 1; i++)
                                    {
                                        sheet1.Cells[ttRow, 1].Value = (i + 1).ToString();
                                        sheet1.Cells[ttRow, 2].Value = Convert.ToDateTime(dtpay.Rows[i]["due_date"]).ToString("yyyy/MM/dd");
                                        sheet1.Cells[ttRow, 3].Value = Convert.ToInt32(dtpay.Rows[i]["opening_balance"]);
                                        sheet1.Cells[ttRow, 4].Value = Convert.ToInt32(dtpay.Rows[i]["installment_price"]);
                                        sheet1.Cells[ttRow, 5].Value = Convert.ToInt32(dtpay.Rows[i]["principal"]);
                                        sheet1.Cells[ttRow, 6].Value = Convert.ToInt32(dtpay.Rows[i]["interest"]);
                                        sheet1.Cells[ttRow, 7].Value = Convert.ToInt32(dtpay.Rows[i]["end_balance"]);
                                        sheet1.Cells[ttRow, 8].Value = Convert.ToInt32(dtpay.Rows[i]["total_interest"]);
                                        ttRow += 1;
                                    }
                                    sheet1.Cells[ttRow, 1].Value = "合計";
                                    sheet1.Cells[ttRow, 1, ttRow, 2].Merge = true; //合併儲存格
                                    sheet1.Cells[ttRow, 1, ttRow, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中

                                    sheet1.Cells[ttRow, 4, ttRow, 4].Formula = string.Format("SUM({0})", new ExcelAddress(12, 4, ttRow - 1, 4).Address);
                                    sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUM({0})", new ExcelAddress(12, 5, ttRow - 1, 5).Address);
                                    sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUM({0})", new ExcelAddress(12, 6, ttRow - 1, 6).Address);

                                    //sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    //sheet1.Cells[ttRow, 1, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);//设置单元格背景色
                                    //sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                    //sheet1.Cells[ttRow, 1, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                                    sheet1.Cells[11, 1, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                    sheet1.Cells[11, 1, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[11, 1, ttRow, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[11, 1, ttRow, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[11, 1, ttRow, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                                    sheet1.Cells[12, 2, ttRow, 8].Style.Numberformat.Format = "#,##0";
                                    sheet1.Column(1).Width = 5;
                                }
                            }





                            ////sheet1.Cells.Style.ShrinkToFit = true;//单元格自动适应大小
                            //sheet1.Cells.AutoFitColumns();

                            //頁尾加入頁次
                            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁，共" + ExcelHeaderFooter.NumberOfPages + "頁";
                            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                            #endregion

                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment; filename=" + "車款分期償還計畫表" + ".xlsx");
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.BinaryWrite(pck.GetAsByteArray());
                            Response.End();
                        }

                    }
                }
                break;
            #endregion
            case "Print2":  //區配&外車
                #region 
                SqlCommand objCommand2 = new SqlCommand();
                objCommand2.CommandText = string.Format(@" Select  A.car_license ,A.car_price_s, A.first_payment_s, A.loan_price_s, 
                                                            A.loan_startdate_s, A.annual_rate_s, A.loan_period_s, A.annual_pay_count_s, A.installment_price_s, A.installment_count_s, 
                                                            A.interest_s, A.deposit_s ,  Sinfo.paid_count  'paid_count_s'
                                                            from  ttLoan A with(nolock) 
                                                            CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 1) Sinfo
                                                            Where A.id = @id ");
                objCommand2.Parameters.AddWithValue("@id", e.CommandArgument);
                using (DataTable dt = dbAdapter.getDataTable(objCommand2))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        using (ExcelPackage p = new ExcelPackage())
                        {
                            int ttRow = 0;    //列                   

                            ExcelPackage pck = new ExcelPackage();
                            var sheet1 = pck.Workbook.Worksheets.Add("車款分期償還計畫表");

                            #region sheet1                
                            //檔案邊界
                            sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                            sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                            sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                            sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                            sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                            sheet1.PrinterSettings.HorizontalCentered = true;

                            //sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                            sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小 
                            sheet1.PrinterSettings.RepeatRows = new ExcelAddress("$1:$4");
                            sheet1.DefaultColWidth = 14;   // 默认所有列宽
                            sheet1.DefaultRowHeight = 22;  // 默认所有行高

                            sheet1.Cells.Style.Font.Name = "新細明體";
                            sheet1.Cells[2, 5, 2, 7].Merge = true; //合併儲存格
                            sheet1.Cells[2, 3, 2, 4].Value = "車號：" + dt.Rows[0]["car_license"].ToString();
                            sheet1.Cells[2, 3, 2, 4].Merge = true; //合併儲存格                            
                            sheet1.Cells[2, 5, 2, 7].Value = "車款分期償還計畫表";
                            sheet1.Cells[2, 3, 2, 7].Style.Font.Bold = true;
                            sheet1.Cells[2, 3, 2, 7].Style.Font.Size = 14;
                            sheet1.Cells[2, 3, 2, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中


                            sheet1.Cells[3, 3].Value = "貸款金額";
                            sheet1.Cells[3, 3, 3, 4].Merge = true; //合併儲存格
                            sheet1.Cells[3, 5].Value = Convert.ToInt32(dt.Rows[0]["loan_price_s"]);
                            sheet1.Cells[3, 5].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[3, 6].Value = "分期付款額";
                            sheet1.Cells[3, 7].Value = Convert.ToInt32(dt.Rows[0]["installment_price_s"]);
                            sheet1.Cells[3, 7].Style.Numberformat.Format = "#,##0";

                            sheet1.Cells[4, 3].Value = "貸款年限";
                            sheet1.Cells[4, 3, 4, 4].Merge = true; //合併儲存格
                            sheet1.Cells[4, 5].Value = Convert.ToInt32(dt.Rows[0]["loan_period_s"]);
                            sheet1.Cells[4, 6].Value = "實際付款次數";
                            sheet1.Cells[4, 7].Value = Convert.ToInt32(dt.Rows[0]["paid_count_s"]);
                            sheet1.Cells[4, 7].Style.Numberformat.Format = "#,##0";

                            sheet1.Cells[5, 3].Value = "每年付款次數";
                            sheet1.Cells[5, 3, 5, 4].Merge = true; //合併儲存格
                            sheet1.Cells[5, 5].Value = Convert.ToInt32(dt.Rows[0]["annual_pay_count_s"]);
                            sheet1.Cells[5, 5].Style.Numberformat.Format = "#,##0";
                            sheet1.Cells[5, 6].Value = "利息總額";
                            sheet1.Cells[5, 7].Value = Convert.ToInt32(dt.Rows[0]["interest_s"]);
                            sheet1.Cells[5, 7].Style.Numberformat.Format = "#,##0";

                            sheet1.Cells[6, 3].Value = "貸款起始日";
                            sheet1.Cells[6, 3, 6, 4].Merge = true; //合併儲存格
                            sheet1.Cells[6, 5].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate_s"]).ToString("yyyy/MM/dd");
                            sheet1.Cells[6, 6].Value = "貸款終止日";
                            sheet1.Cells[6, 7].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate_s"]).AddYears(Convert.ToInt32(dt.Rows[0]["loan_period_s"])).ToString("yyyy/MM/dd");


                            //sheet1.Cells[4, 5].Value = "年利率";
                            //sheet1.Cells[4, 6].Value = Convert.ToDouble(dt.Rows[0]["annual_rate"]);
                            //sheet1.Cells[4, 6].Style.Numberformat.Format = "#,##0.00";
                            //sheet1.Cells[4, 7].Value = "分期付款次數";
                            //sheet1.Cells[4, 8].Value = Convert.ToInt32(dt.Rows[0]["installment_count"]);

                            //sheet1.Cells[5, 2].Value = "車價";
                            //sheet1.Cells[5, 3].Value = Convert.ToInt32(dt.Rows[0]["car_price"]);
                            //sheet1.Cells[5, 3].Style.Numberformat.Format = "#,##0";
                            //sheet1.Cells[5, 5].Value = "";
                            //sheet1.Cells[5, 6].Value =
                            //sheet1.Cells[5, 6].Style.Numberformat.Format = "#,##0";

                            //sheet1.Cells[6, 2].Value = "加裝尾門";


                            //sheet1.Cells[7, 2].Value = "合計";
                            //sheet1.Cells[7, 3].Value = Convert.ToInt32(dt.Rows[0]["car_price"]);
                            //sheet1.Cells[7, 3].Style.Numberformat.Format = "#,##0";
                            //sheet1.Cells[7, 5].Value = "貸款起始日";
                            //sheet1.Cells[7, 6].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).ToString("yyyy/MM/dd");
                            //sheet1.Cells[9, 2].Value = "頭款";
                            //sheet1.Cells[9, 3].Value = Convert.ToInt32(dt.Rows[0]["first_payment"]);
                            //sheet1.Cells[9, 3].Style.Numberformat.Format = "#,##0";

                            sheet1.Cells[3, 5, 6, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet1.Cells[3, 7, 6, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            sheet1.Cells[1, 1, 9, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Top.Style = ExcelBorderStyle.Double; //框線
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Left.Style = ExcelBorderStyle.Double;
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Right.Style = ExcelBorderStyle.Double;



                            sheet1.Cells[3, 3, 6, 7].Style.Border.Top.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Bottom.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Left.Color.SetColor(Color.DarkRed);
                            sheet1.Cells[3, 3, 6, 7].Style.Border.Right.Color.SetColor(Color.DarkRed);




                            ttRow = 8;
                            sheet1.Cells[ttRow, 2].Value = "付款編號";
                            sheet1.Cells[ttRow, 3].Value = "日期";
                            sheet1.Cells[ttRow, 4].Value = "期初餘額";
                            sheet1.Cells[ttRow, 5].Value = "分期付款額";
                            sheet1.Cells[ttRow, 6].Value = "本金";
                            sheet1.Cells[ttRow, 7].Value = "利息";
                            sheet1.Cells[ttRow, 8].Value = "扣款日期";
                            sheet1.Cells[ttRow, 2, ttRow, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                            sheet1.Cells[ttRow, 2, ttRow, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            sheet1.Cells[ttRow, 2, ttRow, 8].Style.Fill.BackgroundColor.SetColor(Color.MistyRose);//设置单元格背景色
                            ttRow += 1;

                            SqlCommand cmdpay = new SqlCommand();
                            {
                                cmdpay.Parameters.AddWithValue("@loan_id", e.CommandArgument);
                                cmdpay.Parameters.AddWithValue("@type", 1);
                                cmdpay.CommandText = @"Select * from  ttRepayment  with(nolock) 
                                         Where loan_id = @loan_id and type=@type";
                                DataTable dtpay = dbAdapter.getDataTable(cmdpay);
                                if (dtpay.Rows.Count > 0)
                                {
                                    for (int i = 0; i <= dtpay.Rows.Count - 1; i++)
                                    {
                                        sheet1.Cells[ttRow, 2].Value = (i + 1).ToString();
                                        sheet1.Cells[ttRow, 3].Value = Convert.ToDateTime(dtpay.Rows[i]["due_date"]).ToString("yyyy/MM/dd");
                                        sheet1.Cells[ttRow, 4].Value = Convert.ToInt32(dtpay.Rows[i]["opening_balance"]);
                                        sheet1.Cells[ttRow, 5].Value = Convert.ToInt32(dtpay.Rows[i]["installment_price"]);
                                        sheet1.Cells[ttRow, 6].Value = Convert.ToInt32(dtpay.Rows[i]["principal"]);
                                        sheet1.Cells[ttRow, 7].Value = Convert.ToInt32(dtpay.Rows[i]["interest"]);
                                        sheet1.Cells[ttRow, 8].Value = dtpay.Rows[i]["pay_date"] != DBNull.Value ? Convert.ToDateTime(dtpay.Rows[i]["pay_date"]).ToString("yyyy/MM/dd") : "";

                                        ttRow += 1;
                                    }
                                    sheet1.Cells[ttRow, 2].Value = "合計";
                                    sheet1.Cells[ttRow, 2, ttRow, 3].Merge = true; //合併儲存格
                                    sheet1.Cells[ttRow, 2, ttRow, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中

                                    sheet1.Cells[ttRow, 5, ttRow, 5].Formula = string.Format("SUM({0})", new ExcelAddress(9, 5, ttRow - 1, 5).Address);
                                    sheet1.Cells[ttRow, 6, ttRow, 6].Formula = string.Format("SUM({0})", new ExcelAddress(9, 6, ttRow - 1, 6).Address);
                                    sheet1.Cells[ttRow, 7, ttRow, 7].Formula = string.Format("SUM({0})", new ExcelAddress(9, 7, ttRow - 1, 7).Address);

                                    sheet1.Cells[8, 2, ttRow, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                                    sheet1.Cells[8, 2, ttRow, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[8, 2, ttRow, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[8, 2, ttRow, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    sheet1.Cells[9, 2, ttRow, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                                    sheet1.Cells[9, 8, ttRow, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中
                                    sheet1.Cells[8, 2, ttRow, 8].Style.VerticalAlignment = ExcelVerticalAlignment.Center;     //上下置中
                                    sheet1.Cells[9, 2, ttRow, 8].Style.Numberformat.Format = "#,##0";
                                    sheet1.Column(1).Width = 5;
                                }
                            }


                            ////sheet1.Cells.Style.ShrinkToFit = true;//单元格自动适应大小
                            //sheet1.Cells.AutoFitColumns();

                            //頁尾加入頁次
                            sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁，共" + ExcelHeaderFooter.NumberOfPages + "頁";
                            sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                            #endregion

                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment; filename=" + "車款分期償還計畫表" + ".xlsx");
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.BinaryWrite(pck.GetAsByteArray());
                            Response.End();
                        }

                    }
                }
                break;
                #endregion

        }
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        #region 
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = string.Format(@" Select A.car_license, A.car_price, A.first_payment, A.loan_price, A.annual_rate, A.loan_period, A.annual_pay_count, A.loan_startdate, A.installment_price, 
                                                  A.installment_count, A.interest, A.loan_company, A.memo, A.loan_price_s, A.annual_rate_s, A.installment_price_s,  A.interest_s, A.deposit_s,
                                                  B.dept, B.ownership, B.car_retailer , DT.code_name as 'dept_name',OS.code_name as ownership_text,
                                                  CD.code_name as car_retailer_text, B.owner, info.principal , Sinfo.principal  'principal_s', info.interest , Sinfo.interest  'interest_s'
                                                  from ttLoan A with(nolock) 
                                                  left join ttAssets B with(nolock) on A.car_license = B.car_license 
                                                  left join tbItemCodes DT with(nolock) on B.dept  = DT.code_id and DT.code_bclass = '6' and DT.code_sclass= 'dept'
                                                  left join tbItemCodes OS with(nolock) on B.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                  left join tbItemCodes CD with(nolock) on B.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD' 
                                                  CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 0) info
                                                  CROSS APPLY dbo.fu_GetPaidInfoByLoanid(A.id, 1) Sinfo");
        using (DataTable dt = dbAdapter.getDataTable(cmd))
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    int ttRow = 0;    //列                   

                    ExcelPackage pck = new ExcelPackage();
                    var sheet1 = pck.Workbook.Worksheets.Add("車貸報表");

                    #region sheet1                
                    //檔案邊界
                    sheet1.PrinterSettings.TopMargin = 1 / 2.54M;
                    sheet1.PrinterSettings.RightMargin = 0.5M / 2.54M;
                    sheet1.PrinterSettings.BottomMargin = 1 / 2.54M;
                    sheet1.PrinterSettings.LeftMargin = 0.5M / 2.54M;
                    sheet1.PrinterSettings.FooterMargin = 1 / 4.54M; //頁尾
                    sheet1.PrinterSettings.HorizontalCentered = true;

                    sheet1.PrinterSettings.Orientation = eOrientation.Landscape;   //橫向
                    sheet1.PrinterSettings.PaperSize = ePaperSize.A4;              //纸张大小 
                    sheet1.PrinterSettings.RepeatRows = new ExcelAddress("$1:$2");
                    //sheet1.DefaultColWidth = 10;   // 默认所有列宽
                    //sheet1.DefaultRowHeight = 22;  // 默认所有行高

                    sheet1.Cells.Style.Font.Name = "新細明體";
                    sheet1.Cells[1, 1, 1, 27].Merge = true;       //合併儲存格
                    sheet1.Cells[1, 1, 1, 27].Value = "車貸報表";
                    sheet1.Cells[1, 1, 1, 27].Style.Font.Bold = true;
                    sheet1.Cells[1, 1, 1, 27].Style.Font.Size = 14;
                    sheet1.Cells[1, 1, 1, 27].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中

                    ttRow = 2;
                    sheet1.Cells[ttRow, 1].Value = "序號";
                    sheet1.Cells[ttRow, 1, ttRow + 1, 1].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 2].Value = "月份";
                    sheet1.Cells[ttRow, 2, ttRow + 1, 2].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 3].Value = "單位";
                    sheet1.Cells[ttRow, 3, ttRow + 1, 3].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 4].Value = "部門";
                    sheet1.Cells[ttRow, 4, ttRow + 1, 4].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 5].Value = "使用者";
                    sheet1.Cells[ttRow, 5, ttRow + 1, 5].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 6].Value = "車號";
                    sheet1.Cells[ttRow, 6, ttRow + 1, 6].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 7].Value = "性質";
                    sheet1.Cells[ttRow, 7, ttRow + 1, 7].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 8].Value = "車行";
                    sheet1.Cells[ttRow, 8, ttRow + 1, 8].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 9].Value = "貸款公司";
                    sheet1.Cells[ttRow, 9, ttRow + 1, 9].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 10].Value = "貸款人";
                    sheet1.Cells[ttRow, 10, ttRow + 1, 10].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 11].Value = "公司貸款";
                    sheet1.Cells[ttRow, 11, ttRow, 17].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 18].Value = "區配&外車扣款";
                    sheet1.Cells[ttRow, 18, ttRow, 25].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 26].Value = "溢繳金額";
                    sheet1.Cells[ttRow, 26, ttRow + 1, 26].Merge = true;       //合併儲存格
                    sheet1.Cells[ttRow, 27].Value = "備註";
                    sheet1.Cells[ttRow, 27, ttRow + 1, 27].Merge = true;       //合併儲存格

                    ttRow += 1;
                    sheet1.Cells[ttRow, 11].Value = "貸款金額";
                    sheet1.Cells[ttRow, 12].Value = "起";
                    sheet1.Cells[ttRow, 13].Value = "迄";
                    sheet1.Cells[ttRow, 14].Value = "期數";
                    sheet1.Cells[ttRow, 15].Value = "繳款日";
                    sheet1.Cells[ttRow, 16].Value = "利率";
                    sheet1.Cells[ttRow, 17].Value = "繳款金額";
                    sheet1.Cells[ttRow, 18].Value = "賣價";
                    sheet1.Cells[ttRow, 19].Value = "頭款";
                    sheet1.Cells[ttRow, 20].Value = "起";
                    sheet1.Cells[ttRow, 21].Value = "迄";
                    sheet1.Cells[ttRow, 22].Value = "期數";
                    sheet1.Cells[ttRow, 23].Value = "起扣年月";
                    sheet1.Cells[ttRow, 24].Value = "利率";
                    sheet1.Cells[ttRow, 25].Value = "扣款金額";
                    ttRow += 1;

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        sheet1.Cells[ttRow, 1].Value = (i + 1).ToString();
                        sheet1.Cells[ttRow, 2].Value =  DateTime.Today.Month ;
                        sheet1.Cells[ttRow, 3].Value = "";
                        sheet1.Cells[ttRow, 4].Value = dt.Rows[i]["dept_name"].ToString();
                        sheet1.Cells[ttRow, 5].Value = dt.Rows[i]["owner"].ToString();
                        sheet1.Cells[ttRow, 6].Value = dt.Rows[i]["car_license"].ToString();
                        sheet1.Cells[ttRow, 7].Value = dt.Rows[i]["ownership_text"].ToString();
                        sheet1.Cells[ttRow, 8].Value = dt.Rows[i]["car_retailer_text"].ToString();
                        sheet1.Cells[ttRow, 9].Value = dt.Rows[i]["loan_company"].ToString();
                        sheet1.Cells[ttRow, 10].Value = "車主";
                        sheet1.Cells[ttRow, 11].Value = Convert.ToInt32(dt.Rows[0]["loan_price"]);
                        sheet1.Cells[ttRow, 11].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 12].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).ToString("yyyy/MM");
                        sheet1.Cells[ttRow, 13].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).AddYears(Convert.ToInt32(dt.Rows[0]["loan_period"])).ToString("yyyy/MM");
                        sheet1.Cells[ttRow, 14].Value = Convert.ToInt32(dt.Rows[0]["installment_count"]);
                        sheet1.Cells[ttRow, 14].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 15].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).Day;
                        sheet1.Cells[ttRow, 16].Value = Convert.ToDouble(dt.Rows[0]["annual_rate"]);
                        sheet1.Cells[ttRow, 16].Style.Numberformat.Format = "#,##0.00";
                        sheet1.Cells[ttRow, 17].Value = Convert.ToInt32(dt.Rows[0]["principal"]) + Convert.ToInt32(dt.Rows[0]["interest"]);
                        sheet1.Cells[ttRow, 17].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 18].Value = Convert.ToInt32(dt.Rows[0]["loan_price_s"]);
                        sheet1.Cells[ttRow, 18].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 19].Value = Convert.ToInt32(dt.Rows[0]["first_payment"]);
                        sheet1.Cells[ttRow, 19].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 20].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).ToString("yyyy/MM");
                        sheet1.Cells[ttRow, 21].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).AddYears(Convert.ToInt32(dt.Rows[0]["loan_period"])).ToString("yyyy/MM");
                        sheet1.Cells[ttRow, 22].Value = Convert.ToInt32(dt.Rows[0]["installment_count"]);
                        sheet1.Cells[ttRow, 22].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 23].Value = Convert.ToDateTime(dt.Rows[0]["loan_startdate"]).ToString("yyyy/MM");
                        sheet1.Cells[ttRow, 24].Value = Convert.ToDouble(dt.Rows[0]["annual_rate_s"]);
                        sheet1.Cells[ttRow, 25].Value = Convert.ToInt32(dt.Rows[0]["principal_s"]) + Convert.ToInt32(dt.Rows[0]["interest_s"]);
                        sheet1.Cells[ttRow, 25].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 26].Value = (Convert.ToInt32(dt.Rows[0]["principal"]) + Convert.ToInt32(dt.Rows[0]["interest"])) -(Convert.ToInt32(dt.Rows[0]["principal_s"]) + Convert.ToInt32(dt.Rows[0]["interest_s"])); //公司繳款金額-區配&外車繳款金額
                        sheet1.Cells[ttRow, 26].Style.Numberformat.Format = "#,##0";
                        sheet1.Cells[ttRow, 27].Value = dt.Rows[i]["memo"].ToString();

                        ttRow += 1;
                    }
                    sheet1.Cells[2, 1, ttRow-1, 27].Style.Border.Top.Style = ExcelBorderStyle.Thin; //框線
                    sheet1.Cells[2, 1, ttRow-1, 27].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[2, 1, ttRow-1, 27].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[2, 1, ttRow-1, 27].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet1.Cells[2, 1, ttRow - 1, 27].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center; //左右置中  

                    //頁尾加入頁次
                    sheet1.HeaderFooter.OddFooter.CenteredText = "第" + ExcelHeaderFooter.PageNumber + "頁，共" + ExcelHeaderFooter.NumberOfPages + "頁";
                    sheet1.HeaderFooter.OddFooter.RightAlignedText = "印表日期：" + DateTime.Now.ToString("yyyy/MM/dd HH:mm");
                    #endregion

                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment; filename=" + "車貸報表" + ".xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.BinaryWrite(pck.GetAsByteArray());
                    Response.End();
                }

            }
        }
        #endregion
    }
}