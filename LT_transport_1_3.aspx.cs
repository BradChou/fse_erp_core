﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using NPOI.HSSF.UserModel;
using System.Text;
using System.Collections;
using BObject.Bobjects;
using NPOI.SS.UserModel;
using BarcodeLib;
using System.Drawing;

using NPOI.XSSF.UserModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Text;
using System.Web.Configuration;
using System.Threading;
using System.Configuration;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Cryptography;

public partial class LT_transport_1_3 : System.Web.UI.Page
{
    string area_arrive_code = "";
    string ShuttleStationCode = "";
    bool check_address = true;
    string SpecialAreaId = "";
    string SpecialAreaFee = "";
    string SendCode = "";
    string SendSD = "";
    string SendMD = "";
    string ReceiveCode = "";
    string ReceiveSD = "";
    string ReceiveMD = "";
    string ProductValue = "";
    bool send_address_parsing = false;
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //第一次載入的時候，生成一個初始的標誌
        if (null == Session["Token"])
        {
            SetToken();
        }

        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            


            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            #region 客代編號(套權限)
            master_code = Session["master_code"].ToString();
            manager_type = Session["manager_type"].ToString(); //管理單位類別   

            string station = Session["management"].ToString();
            string station_level = Session["station_level"].ToString();
            string station_area = Session["station_area"].ToString();
            string station_scode = Session["station_scode"].ToString();


            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                        cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                        cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                        if (station_level.Equals("1"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("2"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("4"))
                        {
                            cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt1.Rows.Count; i++)
                                {
                                    station_scode += "'";
                                    station_scode += dt1.Rows[i]["station_scode"].ToString();
                                    station_scode += "'";
                                    station_scode += ",";
                                }
                                station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                            }
                        }
                        else if (station_level.Equals("5") || station_level.Equals(""))
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt1 = dbAdapter.getDataTable(cmd);
                            if (dt1 != null && dt1.Rows.Count > 0)
                            {
                                station_scode += "'";
                                station_scode += dt1.Rows[0]["station_code"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                            cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                            DataTable dt = dbAdapter.getDataTable(cmd);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                supplier_code = dt.Rows[0]["station_code"].ToString();
                            }
                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    break;
            }

            using (SqlCommand cmd2 = new SqlCommand())
            {
                string wherestr = "";
                //if (supplier_code != "")
                //{
                //    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                //    wherestr = " and supplier_code = @supplier_code";
                //}
                //if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                //{
                //    cmd2.Parameters.AddWithValue("@master_code", master_code);
                //    wherestr += " and customer_code like @master_code+'%' ";
                //}

                if (supplier_code != "")
                {
                    if (manager_type == "1" || manager_type == "2")
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and customer_code like ''+@supplier_code+'00000002%' ";
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                        wherestr = " and A.customer_code like ''+@supplier_code+'%' ";
                    }
                }

                cmd2.Parameters.AddWithValue("@type", "1");
                cmd2.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                    from tbCustomers A with(Nolock)
                                                    -- inner join tbDeliveryNumberSetting B with(nolock) on A.customer_code = B.customer_code and B.IsActive = 1
                                                    where 0=0 and A.type =@type and A.stop_shipping_code = '0' {0}  group by A.customer_code,A.customer_shortname  order by customer_code asc ", wherestr);
                dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                dlcustomer_code.DataValueField = "customer_code";
                dlcustomer_code.DataTextField = "name";
                dlcustomer_code.DataBind();
                dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));

                customerDropDown.DataSource = dbAdapter.getDataTable(cmd2);
                customerDropDown.DataValueField = "customer_code";
                customerDropDown.DataTextField = "name";
                customerDropDown.DataBind();
                customerDropDown.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }

            date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            #endregion

            if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(dltoday_customer_code.SelectedValue)) ||
                    ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
            {
                Label2.Visible = false;
                dlcustomer_code.Visible = false;
                btImport.Visible = false;
                btImport_SB.Visible = true;

            }
            else
            {
                Label2.Visible = true;
                dlcustomer_code.Visible = true;
                btImport.Visible = true;
                btImport_SB.Visible = false;

            }

            GetFileDropDownList();
            Losdtoday_customer_code();
            readdata();

            if (Session["isSuccess"] != null)
            {
                if (bool.Parse(Session["isSuccess"].ToString()))
                {
                    string theDay;
                    DateTime day = DateTime.Now;
                    if (day.Hour < 16)
                    {
                        theDay = "今天";
                    }
                    else
                    {
                        theDay = "明天";
                        day = day.AddDays(1);
                    }

                    string alertMessage = string.Format("派件成功，司機將在{0}({1}/{2}) 12:00~18:00 到場收件", theDay, day.Month, day.Day);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + alertMessage + "')", true);
                }
                Session.Remove("isSuccess");
            }

            //超值廂預購袋不能修改件數
            using (SqlCommand cmd2 = new SqlCommand())
            {
                cmd2.Parameters.AddWithValue("@account_code", Session["account_code"]);
                cmd2.CommandText = @" select  b.product_type from tbAccounts a
                                      left join tbCustomers b with(nolock) on b.customer_code = a.customer_code
                                      WHERE A.account_code = @account_code ";
                using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                {

                    if (dt2.Rows[0]["product_type"].ToString() == "2" || dt2.Rows[0]["product_type"].ToString() == "3")
                    {
                        btnEdit.Style.Add("display", "none");
                    }
                }
            }


            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('空白託運單已於9/22更新，請重新下載使用')", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('親愛的全速配客戶，您好： \\r\\r    暫停配送服務將從2023年5月1日開始，而最後的收貨日為2023年4月21日，爾後我們將不接受新的宅配訂單。一旦IT營運系統重整完成，我們將立即再次通知您，期能以更好的IT營運系統跟品質，再度為您提供服務。\\r    我們會在暫停配送期間提供運送替代方案的諮詢專線，供您參考使用。\\r諮詢專線：(02) 7733 - 7938#128 \\r專案服務人員：周世文 先生')", true);
        }

        if (Session["isCancelSuccess"] != null)         //避免TimeOut問題，所以銷單成功寫在這裡，失敗則留在原頁面
        {
            if (bool.Parse(Session["isCancelSuccess"].ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('銷單成功')", true);
            }
            Session.Remove("isCancelSuccess");
        }

        //公司人員不能用自己帳號打單，要用部門客代
        if (manager_type == "1" || manager_type == "2")
        {
            InnerInstruction.Style.Add("display", "inline-block");
        }
    }

    protected void ShowPositionOrNot(object sender, EventArgs e)
    {
        if (option.SelectedValue == "0")
        {
            posi.Visible = true;
        }
        else
        {
            posi.Visible = false;
        }
    }

    private void readdata()
    {
        string wherestr = "";
        int general_counter = 0;
        int holiday_counter = 0;
        SqlCommand cmd = new SqlCommand();

        if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(dltoday_customer_code.SelectedValue)) ||
                    ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
        {
            cmd.Parameters.AddWithValue("@cuser", Session["account_code"].ToString());
            wherestr += " and A.cuser like @cuser+'%' ";
        }
        else
        {
            if (supplier_code != "")
            {
                cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr += " and A.customer_code like @supplier_code+'%' ";
            }
            else
            {
                cmd.Parameters.AddWithValue("@supplier_code", dltoday_customer_code.SelectedValue);
                wherestr += " and A.customer_code like @supplier_code+'%' ";
            }
        }


        cmd.Parameters.AddWithValue("@scdate", DateTime.Today.ToString("yyyy/MM/dd"));
        cmd.Parameters.AddWithValue("@ecdate", DateTime.Today.AddDays(1).ToString("yyyy/MM/dd"));
        wherestr += " and A.cdate >= @scdate and A.cdate< @ecdate ";

        cmd.CommandText = string.Format(@"Select A.request_id, A.cdate, A.arrive_assign_date , A.supplier_date, A.print_date,A.print_flag, A.check_number ,A.receive_contact , 
                                          ROW_NUMBER() OVER(order by   A.request_id Asc,A.cdate desc , A.check_number desc) AS ROWID,
                                          A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area , A.receive_address,
                                          A.pieces , A.plates , A.cbm ,
                                          B.code_name , A.customer_code , C.customer_shortname ,
                                          A.send_city , A.send_area , A.send_address ,
                                          A.arrive_to_pay_freight,A.collection_money,A.ReportFee,ProductValue,A.arrive_to_pay_append,A.invoice_desc,
                                          a.cancel_date,
                                          A.holiday_delivery, A.order_number, case A.holiday_delivery when  '1' then '假日配送' else '一般配送' end 'holiday_delivery_chinese'
                                          from tcDeliveryRequests A with(nolock)
                                          left join tbItemCodes B with(nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C with(nolock)  on C.customer_code = A.customer_code
                                          where A.cancel_date is null and A.Less_than_truckload = 1 and A.DeliveryType = 'D'  {0}  
                                          order by  A.request_id Asc, A.cdate desc , A.check_number desc", wherestr);   //and A.check_number =''
        DataTable dt = dbAdapter.getDataTable(cmd);
        New_List.DataSource = dt;
        New_List.DataBind();

        for (int i = 0; i <= New_List.Items.Count - 1; i++)
        {

            if (dt.Rows[i]["cancel_date"].ToString() != "")
            {
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                chkRow.Enabled = false;
                chkRow.Visible = false;

            }
            if (dt.Rows[i]["holiday_delivery"].ToString().ToLower() == "true")
            {
                holiday_counter += 1;
            }
            else
            {
                general_counter += 1;
            };
        }
        ltotalpages.Text = dt.Rows.Count.ToString();

        holiday_delivery.Text = holiday_counter.ToString();
        general_delivery.Text = general_counter.ToString();

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$.unblockUI();</script>", false);
        return;



    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "202101_託運單標準格式-零擔.xls";
        //根據客代類型給予不同下載檔案
        string account_code = (string)Session["account_code"];
        using (SqlCommand cmda = new SqlCommand())
        {
            cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + account_code + "' ";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
                if (dta.Rows.Count > 0)
                {
                    string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                    bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                    if (product_type == "1" && is_new_customer == true) //新客代、月結
                    {
                        ttFileName = "2022_託運單標準格式-零擔-論S.xls";
                    }
                    else if (product_type == "1") //一般月結
                    {
                        ttFileName = "2022_託運單標準格式-零擔-月結.xls";
                    }
                    else if (product_type == "2") //預購袋客代
                    {
                        ttFileName = "2022_託運單標準格式-零擔-預購袋.xls";
                    }
                    else if (product_type == "3") //超值箱客代
                    {
                        ttFileName = "2022_託運單標準格式-零擔-超值箱.xls";
                    }
                    else if (product_type == "4") //內部文件客代
                    {
                        ttFileName = "2022_託運單標準格式-零擔-內部文件.xls";
                    }
                    else if (product_type == "5") //商城出貨客代
                    {
                        ttFileName = "2022_託運單標準格式-零擔-商城出貨.xls";
                    }
                    else if (product_type == "6") //論件客代
                    {
                        ttFileName = "2022_託運單標準格式-零擔-論件.xls";
                    }
                }
                else { ttFileName = "2022_託運單標準格式-零擔-內部員工用.xls"; }
        }


        //}
        //Response.Clear();
        //Response.AddHeader("content-disposition", "attachment;  filename=" + ttFilePath + ttFileName);
        //Response.ContentType = "application/vnd.ms-excel";
        ////Response.BinaryWrite(p.GetAsByteArray());
        //Response.End();

        FileInfo xpath_file = new FileInfo(ttFilePath + ttFileName);
        // 將傳入的檔名以 FileInfo 來進行解析（只以字串無法做）
        System.Web.HttpContext.Current.Response.Clear(); //清除buffer
        System.Web.HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
        System.Web.HttpContext.Current.Response.Buffer = false;
        System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
        // 檔案類型還有下列幾種"application/pdf"、"application/vnd.ms-excel"、"text/xml"、"text/HTML"、"image/JPEG"、"image/GIF"
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode("空白託運單標準格式-零擔.xls", System.Text.Encoding.UTF8));
        // 考慮 utf-8 檔名問題，以 out_file 設定另存的檔名
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Length", xpath_file.Length.ToString()); //表頭加入檔案大小
        System.Web.HttpContext.Current.Response.WriteFile(xpath_file.FullName);

        // 將檔案輸出
        System.Web.HttpContext.Current.Response.Flush();
        // 強制 Flush buffer 內容
        System.Web.HttpContext.Current.Response.End();


    }

    protected void btndownload_Click_buy123(object sender, EventArgs e)
    {
        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "生活市集空白託運單.xlsx";
        //Response.Clear();
        //Response.AddHeader("content-disposition", "attachment;  filename=" + ttFilePath + ttFileName);
        //Response.ContentType = "application/vnd.ms-excel";
        ////Response.BinaryWrite(p.GetAsByteArray());
        //Response.End();

        FileInfo xpath_file = new FileInfo(ttFilePath + ttFileName);
        // 將傳入的檔名以 FileInfo 來進行解析（只以字串無法做）
        System.Web.HttpContext.Current.Response.Clear(); //清除buffer
        System.Web.HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
        System.Web.HttpContext.Current.Response.Buffer = false;
        System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
        // 檔案類型還有下列幾種"application/pdf"、"application/vnd.ms-excel"、"text/xml"、"text/HTML"、"image/JPEG"、"image/GIF"
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode("生活市集空白託運單.xlsx", System.Text.Encoding.UTF8));
        // 考慮 utf-8 檔名問題，以 out_file 設定另存的檔名
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Length", xpath_file.Length.ToString()); //表頭加入檔案大小
        System.Web.HttpContext.Current.Response.WriteFile(xpath_file.FullName);

        // 將檔案輸出
        System.Web.HttpContext.Current.Response.Flush();
        // 強制 Flush buffer 內容
        System.Web.HttpContext.Current.Response.End();


    }

    public class ArriveSites
    {
        public string post_city { get; set; }
        public string post_area { get; set; }
        //public string supplier_code { get; set; }
        //public string area_arrive_code { get; set; }
        public string station_code { get; set; }

    }

    protected void btImport_Click(object sender, EventArgs e)
    {
        lbMsg.Items.Clear();
        if (Request.Form.Get("hiddenTestN").Equals(GetToken()))
        {
            lbMsg.ForeColor = System.Drawing.Color.Blue;
            SetToken();//別忘了最後要更新Session中的標誌
        }
        else
        {
            lbMsg.ForeColor = System.Drawing.Color.Red;
            lbMsg.Items.Add("禁止重新整理提交表單");
            return;
        }




        string ttErrStr = "";
        string customer_code = dlcustomer_code.SelectedValue;
        string supplier_code = "";
        string supplier_name = "";
        string send_contact = "";
        string send_city = "";
        string send_area = "";
        string send_address = "";
        string send_tel = "";
        string uniform_numbers = "";
        string pricing_type = "";
        List<ArriveSites> List = new List<ArriveSites>();
        string randomCode = Guid.NewGuid().ToString().Replace("-", "");
        randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;
        DateTime? startTime;
        DateTime? endTime;
        int successNum = 0;
        int failNum = 0;
        int totalNum = 0;
        int supplier_fee = 0;  //配送費用
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

        DateTime print_date = new DateTime();
        DateTime supplier_date = new DateTime();
        DateTime default_supplier_date = new DateTime();

        if (!DateTime.TryParse(date.Text, out print_date)) print_date = DateTime.Now;
        default_supplier_date = print_date;
        int Week = (int)print_date.DayOfWeek;
        default_supplier_date = default_supplier_date.AddDays(1);  //配送日自動產生，為 D+1

        SortedList CK_list = new SortedList();

        #region 客代資料
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
                              " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                              " where c.customer_code = '" + customer_code + "' ";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
                if (dt.Rows.Count > 0)
                {
                    send_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
                    send_tel = dt.Rows[0]["telephone"].ToString();                        //電話
                    send_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
                    send_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
                    send_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
                    uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
                    supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
                    supplier_name = dt.Rows[0]["supplier_name"].ToString().Trim();        // 區配名稱
                    pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();          // 計價模式
                    send_address_parsing = true;
                    GetAddressParsing(send_city + send_area + send_address, customer_code.Trim(), "1", "1");

                }
        }
        #endregion

        #region 到著碼
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select post_city , post_area,  station_code   from ttArriveSitesScattered with(nolock)";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                List = DataTableExtensions.ToList<ArriveSites>(dt).ToList();
            }
        }
        #endregion

        #region 檢查客代是否有建貨號區間
        int chk = 0;
        bool IsHaveNum = false;

        //using (SqlCommand cmdt = new SqlCommand())
        //{
        //    DataTable dtt;
        //    cmdt.Parameters.AddWithValue("@customer_code", customer_code);
        //    cmdt.CommandText = " Select * from tbDeliveryNumberSetting where customer_code=@customer_code AND IsActive = 1";
        //    dtt = dbAdapter.getDataTable(cmdt);
        //    if (dtt.Rows.Count > 0)
        //    {
        //        IsHaveNum = true;
        //        chk = 1;
        //    }
        //}
        using (SqlCommand cmdt = new SqlCommand())
        {
            DataTable BoxOrBagCustomers;
            DataTable NotBoxOrBagCustomers = new DataTable();

            cmdt.Parameters.AddWithValue("@customer_code", customer_code);
            cmdt.CommandText = @" Select top 1 A.* from Customer_checknumber_setting A With(Nolock)
                                    join tbCustomers B on A.customer_code = B.customer_code
                                where A.customer_code=@customer_code and B.product_type in (2,3) and A.is_active = 1 ";
            BoxOrBagCustomers = dbAdapter.getDataTable(cmdt);

            using (SqlCommand sql = new SqlCommand())
            {
                sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                sql.CommandText = @"
                       select product_type from tbCustomers  With(Nolock)
                       where customer_code = @customer_code and product_type <> '2' and product_type <> '3' and stop_shipping_code = '0'";
                NotBoxOrBagCustomers = dbAdapter.getDataTable(sql);
            }

            if (BoxOrBagCustomers.Rows.Count > 0)
            {
                IsHaveNum = true;
                chk = 2;
            }
            //客代是超值箱預購袋客戶且找不到貨號數量
            else if (BoxOrBagCustomers.Rows.Count == 0 && NotBoxOrBagCustomers.Rows.Count == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", @"alert('查無此客代的貨號數量')", true);
                return;
            }
        }
        if (!IsHaveNum)
        {
            chk = 1;
        }
        //if (chk == 0)
        //{
        //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "alertMessage", @"alert('查無此客代的貨號數量')", true);
        //    return;
        //}


        #endregion


        string check_number = "";
        string order_number = "";
        string CbmSize = "";
        string ProductId = "";
        string SpecCodeId = "";
        string subpoena_category = "";
        string subpoena_code = "";
        string receive_contact = "";
        string receive_tel1 = "";
        string receive_tel2 = "";
        string receive_city = "";
        string receive_area = "";
        string receive_address = "";
        string pieces = "";
        string cbmWeight = "";
        string plates = "";
        string cbm = "";
        string collection_money = "";
        string ReportFee = "";
        string HasProductValue = "";             //是否需要保值
        string ProductValue = "";
        string arrive_to_pay_freight = "";
        string arrive_to_pay_append = "";
        string arrive_assign_date = "";

        string _supplier_date = "";

        string receipt_flag = "";
        string WareHouse = "";
        string pallet_recycling_flag = "";
        string invoice_desc = "";
        string check_type = "001";        //託運類別：001一般    
        string product_category = "001";  //商品種類：001一般
        string time_period = "午";        //配送時間：早、午、晚
        string sub_check_number = "000";  //次貨號
        //string area_arrive_code = "";     //到著碼
        string receive_by_arrive_site_flag = "0";
        string print_flag = "0";
        //string _Size = "";

        //string Distributor = "";
        string ArticleNumber = "";          //產品編號     
        string SendPlatform = "";           //出貨平台
        string ArticleName = "";            //品名
        string round_trip = "";             //來回件
        bool HasReturn = false;              //是否為來回件
        string payment_method = "";
        string theSecondRemark = ""; //生活市集第二個備註欄
        string theThirdRemark = "";  //生活市集第三個備註欄
        string holiday_delivery = ""; //是否假日配送
        bool has_holiday_delivery = false; //是否有任一筆是假日配
        bool is_account_preorder = false; //匯入客代是否為預購袋客代

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select product_type from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
            var table = dbAdapter.getDataTable(cmd);
            is_account_preorder = table.Rows[0]["product_type"].ToString().ToLower() == "2" || table.Rows[0]["product_type"].ToString().ToLower() == "3" ? true : false;
        }

        lbMsg.Items.Clear();
        if (file01.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            var fileGuid = Guid.NewGuid().ToString().Substring(0, 5);
            var name = Path.GetFileNameWithoutExtension(file01.FileName);
            var extension = Path.GetExtension(file01.FileName);
            var fileRename = name + "_" + fileGuid + extension;

            string ttFileName = ttPath + fileRename;
            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            IWorkbook workbook = null;
            ISheet u_sheet = null;
            IFormulaEvaluator formulaEvaluator = null;
            string ttSelectSheetName = "";
            if ((ttExtName.ToLower() == ".xls") || (ttExtName.ToLower() == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                if (ttExtName.ToLower() == ".xls")
                {
                    workbook = new HSSFWorkbook(file01.FileContent);
                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new HSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到
                    //版本號檢測
                    string sheet_version = workbook.GetSheetName(1);
                    if (sheet_version != "20220901")
                    {
                        lbMsg.Items.Add("託運單匯入失敗，請下載最新版本空白託運單。");
                        return;
                    }

                }
                else if (ttExtName.ToLower() == ".xlsx")
                {
                    workbook = new XSSFWorkbook(file01.FileContent);
                    u_sheet = (XSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new XSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到
                    //版本號檢測
                    string sheet_version = workbook.GetSheetName(1);
                    if (sheet_version != "20220901")
                    {
                        lbMsg.Items.Add("託運單匯入失敗，請下載最新版本空白託運單。");
                        return;
                    }
                }

                u_sheet.ForceFormulaRecalculation = true; //要求公式重算結果 

                try
                {
                    int restCount = Convert.ToInt32(HiddenCount.Value);

                    //記算上傳資料的資料量
                    int startReadRow = 0;
                    if (FileDropDownList.SelectedValue == "2") //生活市集
                    {
                        startReadRow = 1;
                    }
                    else if (FileDropDownList.SelectedValue == "1") //全速配
                    {
                        startReadRow = 3;
                    }
                    int excelCount = 0;
                    for (int i = u_sheet.FirstRowNum + startReadRow; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                    {
                        IRow Row = u_sheet.GetRow(i);
                        if (Row == null || Row.GetCell(1) == null || Row.GetCell(1).ToString() == "")
                        {
                            break;
                        }
                        excelCount++;
                    }

                    if (restCount <= excelCount - 1)
                    {
                        lbMsg.Items.Add("託運單匯入失敗，因為剩餘貨號數量不足，請向客服申請補充貨號。");
                    }
                    else
                    {
                        if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                        {
                            Boolean IsSheetOk = true;//工作表驗證
                            lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                            //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                            string strRow, strInsCol, strInsVal;
                            List<StringBuilder> sb_del_list = new List<StringBuilder>();
                            List<string> sb_ins_list = new List<string>();
                            //List<StringBuilder> sb_ins_listR = new List<StringBuilder>();
                            List<StringBuilder> stringBuilders = new List<StringBuilder>();
                            StringBuilder sb_temp_del = new StringBuilder();
                            string sb_temp_ins = string.Empty;
                            //StringBuilder sb_temp_insR = new StringBuilder();
                            StringBuilder stringBuilder = new StringBuilder();

                            int returnCounter = 0;

                            IRow Row_title = null;
                            if (u_sheet.LastRowNum > 0) Row_title = u_sheet.GetRow(2);

                            //int Row_count = 0;
                            Int64 LeftCount = 0;

                            //if (chk == 2)
                            //{
                            //    using (SqlCommand cmda = new SqlCommand())
                            //    {

                            //        DataTable dta = new DataTable();
                            //        cmda.CommandText = "Select  end_number,ISNULL(current_number,0) 'current_number' ,begin_number from tbDeliveryNumberOnceSetting With(Nolock) where customer_code=@customer_code and  IsActive=1 order by end_number ";
                            //        cmda.Parameters.AddWithValue("@customer_code", customer_code);
                            //        dta = dbAdapter.getDataTable(cmda);
                            //        if (dta != null && dta.Rows.Count > 0)
                            //        {
                            //            for (int k = 0; k <= dta.Rows.Count - 1; k++)
                            //            {
                            //                if (dta.Rows[k]["current_number"].ToString() == "0")
                            //                {

                            //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
                            //                }
                            //                else if (Convert.ToInt64(dta.Rows[k]["current_number"]) < Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()))
                            //                {
                            //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
                            //                }
                            //                else
                            //                {

                            //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["current_number"].ToString());
                            //                }
                            //            }
                            //        }
                            //    }
                            //}

                            //for (int i = u_sheet.FirstRowNum + 3; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                            //{
                            //    IRow Row = u_sheet.GetRow(i);  //取得目前的資料列 
                            //    if (Row.GetCell(2) != null && Row.GetCell(2).ToString() != "")
                            //    {
                            //        Row_count += 1;
                            //    }
                            //}

                            string product_type_insert = string.Empty;
                            bool is_new_customer_insert = false;

                            using (SqlCommand cmda = new SqlCommand())
                            {
                                cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue + "' ";
                                DataTable dta = dbAdapter.getDataTable(cmda);
                                product_type_insert = dta.Rows[0]["product_type"].ToString();
                                is_new_customer_insert = (bool)(dta.Rows[0]["is_new_customer"]);
                            }


                            for (int i = u_sheet.FirstRowNum + startReadRow; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                            {
                                IRow Row = u_sheet.GetRow(i);  //取得目前的資料列  

                                try
                                {
                                    if (Row != null)
                                    {
                                        #region 初始化
                                        check_number = "";
                                        order_number = "";
                                        CbmSize = "";
                                        ProductId = "";
                                        SpecCodeId = "";
                                        subpoena_category = "";
                                        subpoena_code = "";
                                        receive_contact = "";
                                        receive_tel1 = "";
                                        receive_tel2 = "";
                                        receive_city = "";
                                        receive_area = "";
                                        receive_address = "";
                                        pieces = "";
                                        plates = "";
                                        cbm = "";
                                        collection_money = "";
                                        ReportFee = "";
                                        ProductValue = "";
                                        arrive_to_pay_freight = "";
                                        arrive_to_pay_append = "";
                                        arrive_assign_date = "";
                                        _supplier_date = "";
                                        receipt_flag = "0";
                                        WareHouse = "0";
                                        pallet_recycling_flag = "0";
                                        invoice_desc = "";
                                        area_arrive_code = "";
                                        ShuttleStationCode = "";
                                        SpecialAreaId = "";
                                        SpecialAreaFee = "";
                                        ReceiveCode = "";
                                        ReceiveSD = "";
                                        ReceiveMD = "";
                                        send_address_parsing = false;
                                        strInsCol = "";
                                        strInsVal = "";
                                        sub_check_number = "000";
                                        supplier_date = default_supplier_date;
                                        strRow = (i + 1).ToString();
                                        ArticleNumber = "";          //產品編號     
                                        SendPlatform = "";           //出貨平台
                                        ArticleName = "";            //品名
                                        round_trip = "0";             //來回件
                                        payment_method = "";
                                        theSecondRemark = "";  //生活市集第二個備註欄
                                        theThirdRemark = "";   //生活市集第三個備註欄
                                        holiday_delivery = "0"; //假日配送
                                        #endregion

                                        if (Row.GetCell(1) == null || Row.GetCell(1).ToString() == "")
                                        {
                                            sb_ins_list.Add(sb_temp_ins);
                                            sb_temp_ins = string.Empty;
                                            break;
                                        }
                                        totalNum += 1;
                                        successNum += 1;

                                        #region 驗證&取值 IsSheetOk
                                        if (FileDropDownList.SelectedValue == "2") //生活市集託運單
                                        {
                                            for (int j = 0; j <= 7; j++) //7 columns
                                            {
                                                Boolean IsChkOk = true;
                                                ICell cell = Row.GetCell(j);
                                                switch (j)
                                                {
                                                    case 0:
                                                        #region 訂單編號
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) order_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) order_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    order_number = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                order_number = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 1:
                                                        #region 收件人手機
                                                        if (cell != null)
                                                        {
                                                            switch (cell.CellType)
                                                            {
                                                                case CellType.Blank: //空数据类型处理
                                                                    receive_tel1 = "";
                                                                    if (String.IsNullOrEmpty(receive_tel1))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.String: //字符串类型
                                                                    receive_tel1 = Row.GetCell(j).ToString();
                                                                    if (String.IsNullOrEmpty(receive_tel1))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.Numeric: //数字类型
                                                                    receive_tel1 = Row.GetCell(j).NumericCellValue.ToString();
                                                                    break;
                                                                case CellType.Formula: //公式
                                                                    try
                                                                    {
                                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                        if (formulaValue.CellType == CellType.String) receive_tel1 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                        else if (formulaValue.CellType == CellType.Numeric) receive_tel1 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                    }
                                                                    catch
                                                                    {
                                                                        receive_tel1 = cell.StringCellValue;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 2:
                                                        #region 收件人姓名
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) receive_contact = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) receive_contact = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態

                                                                }
                                                                catch
                                                                {
                                                                    receive_contact = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                receive_contact = Row.GetCell(j).ToString();
                                                            }
                                                        }

                                                        if (String.IsNullOrEmpty(receive_contact))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        #endregion
                                                        break;
                                                    case 3:
                                                        #region 收件人地址
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) receive_address = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) receive_address = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    receive_address = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                receive_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                            }
                                                        }

                                                        if (String.IsNullOrEmpty(receive_address))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        else
                                                        {
                                                            //搜尋tbCityArea資料表是否有一樣簡稱的地址，去進行地址拆解，如果沒有則進行SP正規化地址尋找
                                                            using (SqlCommand cmd_CityArea = new SqlCommand())
                                                            {
                                                                cmd_CityArea.Parameters.AddWithValue("@CityArea", receive_address);
                                                                cmd_CityArea.CommandText = @"select top 1 C.City,C.Area,S.station_code   
                                                                                from  tbCityArea C  
                                                                                Left join  ttArriveSitesScattered S
                                                                                On C.City = S.post_city and C.Zip = S.zip
                                                                                Where C.CityArea = @CityArea";
                                                                DataTable dta = new DataTable();
                                                                dta = dbAdapter.getDataTable(cmd_CityArea);
                                                                if (dta.Rows.Count > 0)
                                                                {
                                                                    receive_city = dta.Rows[0]["city"].ToString();
                                                                    receive_area = dta.Rows[0]["area"].ToString();
                                                                    receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                                    receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                                    receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;

                                                                    if (area_arrive_code == "")
                                                                    {
                                                                        ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                                        if (_ArriveSites != null)
                                                                        {
                                                                            area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                           //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    //呼叫地址正規化SP，拆出縣市、鄉鎮區
                                                                    using (SqlCommand cmd_addr = new SqlCommand())
                                                                    {
                                                                        string ChinaFoTaiwan = PublicFunction.ToTraditional(receive_address);
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");

                                                                        int count = 0;
                                                                        for (int temp = 0; temp < ChinaFoTaiwan.Length; temp++)
                                                                        {
                                                                            if (Utility.IsNumeric(ChinaFoTaiwan.Substring(temp, 1)))
                                                                            {
                                                                                count = count + 1;
                                                                            }
                                                                            else
                                                                            {
                                                                                break;
                                                                            }
                                                                        }
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Substring(count, ChinaFoTaiwan.Length - count);

                                                                        cmd_addr.Parameters.AddWithValue("@address", ChinaFoTaiwan);
                                                                        cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                                                        cmd_addr.CommandType = CommandType.StoredProcedure;
                                                                        try
                                                                        {
                                                                            DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                                                            if (dt_addr != null && dt_addr.Rows.Count > 0)
                                                                            {
                                                                                receive_city = dt_addr.Rows[0]["city"].ToString();
                                                                                receive_area = dt_addr.Rows[0]["area"].ToString();
                                                                            }
                                                                        }
                                                                        catch (Exception ex)
                                                                        {

                                                                        }
                                                                    }

                                                                    receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                                    receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                                    receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;

                                                                    int countRec = 0;
                                                                    for (int temp = 0; temp < receive_address.Length; temp++)
                                                                    {
                                                                        if (Utility.IsNumeric(receive_address.Substring(temp, 1)))
                                                                        {
                                                                            countRec = countRec + 1;
                                                                        }
                                                                        else
                                                                        {
                                                                            break;
                                                                        }
                                                                    }
                                                                    receive_address = receive_address.Substring(countRec, receive_address.Length - countRec);

                                                                    if (area_arrive_code == "")
                                                                    {
                                                                        ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                                        if (_ArriveSites != null)
                                                                        {
                                                                            area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                           //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                        }

                                                                        if (area_arrive_code == "")
                                                                        {
                                                                            if (receive_city == "新竹市" || receive_city == "嘉義市")
                                                                            {
                                                                                _ArriveSites = List.Find(x => x.post_city == receive_city);
                                                                                if (_ArriveSites != null)
                                                                                {
                                                                                    area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                                   //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        #endregion
                                                        break;
                                                    case 4:
                                                        #region 收件人電話
                                                        if (cell != null)
                                                        {
                                                            switch (cell.CellType)
                                                            {
                                                                case CellType.Blank: //空数据类型处理
                                                                    receive_tel2 = "";
                                                                    if (String.IsNullOrEmpty(receive_tel2))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.String: //字符串类型
                                                                    receive_tel2 = Row.GetCell(j).ToString();
                                                                    if (String.IsNullOrEmpty(receive_tel2))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.Numeric: //数字类型
                                                                    receive_tel2 = Row.GetCell(j).NumericCellValue.ToString();
                                                                    break;
                                                                case CellType.Formula: //公式
                                                                    try
                                                                    {
                                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                        if (formulaValue.CellType == CellType.String) receive_tel2 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                        else if (formulaValue.CellType == CellType.Numeric) receive_tel2 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                    }
                                                                    catch
                                                                    {
                                                                        receive_tel2 = cell.StringCellValue;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 5:
                                                        #region 備註1
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) invoice_desc = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) invoice_desc = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    invoice_desc = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                invoice_desc = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 6:
                                                        #region 備註2
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) theSecondRemark = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) theSecondRemark = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    theSecondRemark = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                theSecondRemark = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 7:
                                                        #region 備註3
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) theThirdRemark = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) theThirdRemark = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    theThirdRemark = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                theThirdRemark = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                }
                                            }
                                            arrive_assign_date = "NULL";
                                        }
                                        else if (FileDropDownList.SelectedValue == "1") //全速配託運單
                                        {
                                            for (int j = 0; j <= 20; j++)
                                            {
                                                Boolean IsChkOk = true;
                                                ICell cell = Row.GetCell(j);
                                                switch (j)
                                                {
                                                    case 0:
                                                        #region 訂單編號
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) order_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) order_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    order_number = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                order_number = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 1:
                                                        #region 收件人姓名
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) receive_contact = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) receive_contact = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態

                                                                }
                                                                catch
                                                                {
                                                                    receive_contact = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                receive_contact = Row.GetCell(j).ToString();
                                                            }
                                                        }

                                                        if (String.IsNullOrEmpty(receive_contact))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        #endregion
                                                        break;
                                                    case 2:
                                                        #region 收件人電話1
                                                        if (cell != null)
                                                        {
                                                            switch (cell.CellType)
                                                            {
                                                                case CellType.Blank: //空数据类型处理
                                                                    receive_tel1 = "";
                                                                    if (String.IsNullOrEmpty(receive_tel1))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.String: //字符串类型
                                                                    receive_tel1 = Row.GetCell(j).ToString();
                                                                    if (String.IsNullOrEmpty(receive_tel1))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.Numeric: //数字类型
                                                                    receive_tel1 = Row.GetCell(j).NumericCellValue.ToString();
                                                                    break;
                                                                case CellType.Formula: //公式
                                                                    try
                                                                    {
                                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                        if (formulaValue.CellType == CellType.String) receive_tel1 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                        else if (formulaValue.CellType == CellType.Numeric) receive_tel1 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                    }
                                                                    catch
                                                                    {
                                                                        receive_tel1 = cell.StringCellValue;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        #endregion
                                                        break;
                                                    case 3:
                                                        #region 收件人電話2  
                                                        if (cell != null)
                                                        {
                                                            switch (cell.CellType)
                                                            {
                                                                case CellType.Blank: //空数据类型处理
                                                                    receive_tel2 = "";
                                                                    break;
                                                                case CellType.String: //字符串类型
                                                                    receive_tel2 = Row.GetCell(j).ToString();
                                                                    break;
                                                                case CellType.Numeric: //数字类型
                                                                    receive_tel2 = Row.GetCell(j).NumericCellValue.ToString();
                                                                    break;
                                                                case CellType.Formula: //公式
                                                                    try
                                                                    {
                                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                        if (formulaValue.CellType == CellType.String) receive_tel2 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                        else if (formulaValue.CellType == CellType.Numeric) receive_tel2 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                    }
                                                                    catch
                                                                    {
                                                                        receive_tel2 = cell.StringCellValue;
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 4:
                                                        #region 收件人地址          

                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) receive_address = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) receive_address = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    receive_address = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                receive_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                            }
                                                        }

                                                        if (String.IsNullOrEmpty(receive_address))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        else
                                                        {
                                                            //不論新舊都判斷特服區，僅新客戶需顯示提示
                                                            bool is_new_customer = false;
                                                            using (SqlCommand cmd = new SqlCommand())
                                                            {
                                                                cmd.CommandText = "select product_type,is_new_customer from tbCustomers where customer_code = @customer_code";
                                                                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                                var table = dbAdapter.getDataTable(cmd);
                                                                string aaa = table.Rows[0]["is_new_customer"].ToString().ToLower();
                                                                is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;

                                                            }
                                                            if (GetSpecialAreaFee(receive_address.Trim(), customer_code.Trim(), "1", "1").id.ToString() != "0")
                                                            {
                                                                if (is_new_customer == true)  //新客戶需提示
                                                                {
                                                                    lbMsg.Items.Add("第" + strRow + "列： 此區為特服區，費用請洽官網");
                                                                }
                                                                //舊客戶也須提示
                                                                else
                                                                {
                                                                    lbMsg.Items.Add("第" + strRow + "列： 此區為特服區");
                                                                }
                                                            }
                                                            //非特服區進入地址解析
                                                            else
                                                            {
                                                                //檢查地址
                                                                GetAddressParsing(receive_address.Trim(), customer_code.Trim(), "1", "1");

                                                                //預購袋、超值箱不能寄送外島
                                                                using (SqlCommand cmdb = new SqlCommand())
                                                                {
                                                                    string product_type3 = "";
                                                                    cmdb.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue.ToString());
                                                                    cmdb.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = @customer_code ";
                                                                    using (DataTable dtb = dbAdapter.getDataTable(cmdb))
                                                                        if (dtb.Rows.Count > 0)
                                                                        {
                                                                            product_type3 = dtb.Rows[0]["product_type"].ToString().Trim();
                                                                            if ((product_type3 == "2" || product_type3 == "3") && area_arrive_code == "95")
                                                                            {
                                                                                IsSheetOk = false;
                                                                                lbMsg.Items.Add("第" + strRow + "列： 此客代類型不適用外島寄送");
                                                                            }
                                                                            //聯運區/外島須提示
                                                                            else if (area_arrive_code == "99")
                                                                            {
                                                                                lbMsg.Items.Add("第" + strRow + "列： 此區為聯運區");
                                                                            }
                                                                            else if (area_arrive_code == "95")
                                                                            {
                                                                                lbMsg.Items.Add("第" + strRow + "列： 此區域為外島");
                                                                            }
                                                                        }
                                                                }



                                                            }
                                                            if (check_address == false)
                                                            {

                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【地址】是否正確!");
                                                                check_address = true;
                                                            }
                                                            //搜尋tbCityArea資料表是否有一樣簡稱的地址，去進行地址拆解，如果沒有則進行SP正規化地址尋找
                                                            using (SqlCommand cmd_CityArea = new SqlCommand())
                                                            {
                                                                cmd_CityArea.Parameters.AddWithValue("@CityArea", receive_address);
                                                                cmd_CityArea.CommandText = @"select top 1 C.City,C.Area,S.station_code   
                                                                                from  tbCityArea C  
                                                                                Left join  ttArriveSitesScattered S
                                                                                On C.City = S.post_city and C.Zip = S.zip
                                                                                Where C.CityArea = @CityArea";
                                                                DataTable dta = new DataTable();
                                                                dta = dbAdapter.getDataTable(cmd_CityArea);
                                                                if (dta.Rows.Count > 0)
                                                                {
                                                                    receive_city = dta.Rows[0]["city"].ToString();
                                                                    receive_area = dta.Rows[0]["area"].ToString();
                                                                    receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                                    receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                                    receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;




                                                                    if (area_arrive_code == "")
                                                                    {
                                                                        ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                                        if (_ArriveSites != null)
                                                                        {
                                                                            area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                           //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                        }
                                                                        //int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                                                        //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
                                                                    }
                                                                }
                                                                else
                                                                {

                                                                    //呼叫地址正規化SP，拆出縣市、鄉鎮區
                                                                    using (SqlCommand cmd_addr = new SqlCommand())
                                                                    {
                                                                        string ChinaFoTaiwan = PublicFunction.ToTraditional(receive_address);
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Replace("樸子市", "朴子市");//朴子為簡體字但較常用所以改回


                                                                        int count = 0;
                                                                        for (int temp = 0; temp < ChinaFoTaiwan.Length; temp++)
                                                                        {

                                                                            if (Utility.IsNumeric(ChinaFoTaiwan.Substring(temp, 1)))
                                                                            {
                                                                                count = count + 1;


                                                                            }
                                                                            else
                                                                            {
                                                                                break;
                                                                            }
                                                                        }
                                                                        ChinaFoTaiwan = ChinaFoTaiwan.Substring(count, ChinaFoTaiwan.Length - count);


                                                                        cmd_addr.Parameters.AddWithValue("@address", ChinaFoTaiwan);
                                                                        cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                                                        cmd_addr.CommandType = CommandType.StoredProcedure;
                                                                        try
                                                                        {
                                                                            DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                                                            if (dt_addr != null && dt_addr.Rows.Count > 0)
                                                                            {
                                                                                receive_city = dt_addr.Rows[0]["city"].ToString();
                                                                                receive_area = dt_addr.Rows[0]["area"].ToString();
                                                                            }
                                                                        }
                                                                        catch (Exception ex)
                                                                        {

                                                                        }
                                                                    }

                                                                    receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                                    receive_address = receive_address.Replace("樸子市", "");//把原有樸子市拿掉
                                                                    receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                                    receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;



                                                                    int countRec = 0;
                                                                    for (int temp = 0; temp < receive_address.Length; temp++)
                                                                    {

                                                                        if (Utility.IsNumeric(receive_address.Substring(temp, 1)))
                                                                        {
                                                                            countRec = countRec + 1;
                                                                        }
                                                                        else
                                                                        {
                                                                            break;
                                                                        }
                                                                    }
                                                                    receive_address = receive_address.Substring(countRec, receive_address.Length - countRec);



                                                                    if (area_arrive_code == "")
                                                                    {
                                                                        ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                                        if (_ArriveSites != null)
                                                                        {
                                                                            area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                           //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                        }

                                                                        if (area_arrive_code == "")
                                                                        {
                                                                            if (receive_city == "新竹市" || receive_city == "嘉義市")
                                                                            {

                                                                                _ArriveSites = List.Find(x => x.post_city == receive_city);
                                                                                if (_ArriveSites != null)
                                                                                {
                                                                                    area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                                   //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                                }
                                                                            }

                                                                        }
                                                                        //int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                                                        //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 5:
                                                        #region 件數
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) pieces = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) pieces = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    pieces = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                pieces = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        if (!String.IsNullOrEmpty(pieces) && !Utility.IsNumeric(pieces))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }

                                                        if (is_account_preorder)
                                                        {
                                                            if (pieces != "1")
                                                            {
                                                                pieces = "1";
                                                                lbMsg.Items.Add("【[預購袋/超值箱袋]不可使用單筆多件，已將第" + strRow + "列貨件的件數改為1件匯入】");
                                                            }
                                                        }

                                                        #endregion
                                                        break;
                                                    case 6:
                                                        #region 重量
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) cbmWeight = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) cbmWeight = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    cbmWeight = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                cbmWeight = Row.GetCell(j).ToString();
                                                            }
                                                        }

                                                        if (!String.IsNullOrEmpty(cbmWeight) && !Utility.IsNumeric(cbmWeight))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        #endregion
                                                        break;
                                                    //case 7:
                                                    //    #region 材積大小
                                                    //    if (cell != null)
                                                    //    {
                                                    //        if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                    //        {
                                                    //            try
                                                    //            {
                                                    //                var formulaValue = formulaEvaluator.Evaluate(cell);
                                                    //                if (formulaValue.CellType == CellType.String) CbmSize = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                    //                else if (formulaValue.CellType == CellType.Numeric) CbmSize = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    //            }
                                                    //            catch
                                                    //            {
                                                    //                CbmSize = cell.StringCellValue;
                                                    //            }
                                                    //        }
                                                    //        else
                                                    //        {
                                                    //            CbmSize = Row.GetCell(j).ToString();
                                                    //        }
                                                    //    }
                                                    //    if (String.IsNullOrEmpty(CbmSize))
                                                    //    {
                                                    //        IsChkOk =
                                                    //        IsSheetOk = false;
                                                    //    }

                                                    //    #endregion
                                                    //    break;
                                                    case 7:
                                                        #region 產品名稱
                                                        //須檢查欄位值是否符合客代類型
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) ProductId = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) ProductId = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    ProductId = cell.StringCellValue.Trim();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ProductId = Row.GetCell(j).ToString().Trim();
                                                            }
                                                        }
                                                        using (SqlCommand cmda = new SqlCommand())
                                                        {
                                                            //登入的帳號
                                                            cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + Session["account_code"] + "' ";
                                                            using (DataTable dta = dbAdapter.getDataTable(cmda))
                                                                if (dta.Rows.Count > 0)
                                                                {
                                                                    string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                                                                    bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                                                                    if (product_type == "1" && is_new_customer == true) //新客代、月結
                                                                    {
                                                                        if (ProductId != "論S")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type == "1") //一般月結
                                                                    {
                                                                        if (ProductId != "月結" && ProductId != "月結單筆" && ProductId != "月結多筆")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }

                                                                    else if (product_type == "2") //預購袋客代
                                                                    {
                                                                        if (ProductId != "預購袋" && ProductId != "速配箱")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type == "3") //超值箱客代
                                                                    {
                                                                        if (ProductId != "超值袋" && ProductId != "超值箱")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type == "4") //內部文件客代
                                                                    {
                                                                        if (ProductId != "內部文件")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type == "5") //商城出貨客代
                                                                    {
                                                                        if (ProductId != "包材")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else if (product_type == "6") //商城出貨客代
                                                                    {
                                                                        if (ProductId != "論件")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                        }
                                                                    }
                                                                    else { }


                                                                }
                                                                else
                                                                {
                                                                    //選定的客代
                                                                    using (SqlCommand cmdb = new SqlCommand())
                                                                    {
                                                                        //登入的帳號
                                                                        cmdb.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue.ToString());
                                                                        cmdb.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = @customer_code ";
                                                                        using (DataTable dtb = dbAdapter.getDataTable(cmdb))
                                                                            if (dtb.Rows.Count > 0)
                                                                            {
                                                                                string product_type2 = dtb.Rows[0]["product_type"].ToString().Trim();
                                                                                bool is_new_customer2 = (bool)dtb.Rows[0]["is_new_customer"];
                                                                                if (product_type2 == "1" && is_new_customer2 == true) //新客代、月結
                                                                                {
                                                                                    if (ProductId != "論S")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "1") //一般月結
                                                                                {
                                                                                    if (ProductId != "月結")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "2" && is_new_customer2 == true) //預購袋 新客代
                                                                                {
                                                                                    if (ProductId != "預購袋")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 此客代類型僅能選擇預購袋");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "2") //預購袋客代
                                                                                {
                                                                                    if (ProductId != "預購袋" && ProductId != "速配箱")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "3") //超值箱客代
                                                                                {
                                                                                    if (ProductId != "超值袋" && ProductId != "超值箱")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "4") //內部文件客代
                                                                                {
                                                                                    if (ProductId != "內部文件")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "5") //商城出貨客代
                                                                                {
                                                                                    if (ProductId != "包材")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else if (product_type2 == "6") //商城出貨客代
                                                                                {
                                                                                    if (ProductId != "論件")
                                                                                    {
                                                                                        IsSheetOk = false;
                                                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【產品】是否正確!");
                                                                                    }
                                                                                }
                                                                                else { }



                                                                            }
                                                                    }
                                                                }
                                                        }
                                                        if (String.IsNullOrEmpty(ProductId))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }

                                                        #endregion
                                                        break;
                                                    case 8:
                                                        #region 產品規格
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) SpecCodeId = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) SpecCodeId = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    SpecCodeId = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                SpecCodeId = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        //名稱規格相匹配
                                                        if (ProductId.Trim() == "月結" || ProductId.Trim() == "論S") //新舊客代、月結
                                                        {
                                                            if (SpecCodeId != "1號袋" && SpecCodeId != "2號袋" && SpecCodeId != "3號袋" && SpecCodeId != "S60" && SpecCodeId != "S90" && SpecCodeId != "S110" && SpecCodeId != "S120" && SpecCodeId != "S150")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                            }
                                                        }
                                                        else if (ProductId.Trim() == "預購袋" || ProductId.Trim() == "超值袋") //預購袋、超值袋
                                                        {
                                                            if (SpecCodeId != "1號袋" && SpecCodeId != "2號袋" && SpecCodeId != "3號袋" && SpecCodeId != "4號袋")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                            }
                                                        }
                                                        else if (ProductId.Trim() == "速配箱" || ProductId.Trim() == "超值箱") //速配箱、超值箱
                                                        {
                                                            if (SpecCodeId != "1號箱" && SpecCodeId != "2號箱" && SpecCodeId != "5號箱")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                            }
                                                        }
                                                        else if (ProductId.Trim() == "內部文件" || ProductId.Trim() == "包材") //內部文件、包材
                                                        {
                                                            if (SpecCodeId != "不指定")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                            }
                                                        }
                                                        else if (ProductId.Trim() == "論件") //論件
                                                        {
                                                            if (SpecCodeId != "1才內" && SpecCodeId != "2才內" && SpecCodeId != "3才內" && SpecCodeId != "4才內" && SpecCodeId != "5才內")
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【產品名稱、規格】是否相符!");
                                                            }
                                                        }


                                                        using (SqlCommand cmda = new SqlCommand())
                                                        {
                                                            cmda.CommandText = "select product_type, is_new_customer  from tbCustomers where customer_code = '" + dlcustomer_code.SelectedValue + "' ";
                                                            using (DataTable dta = dbAdapter.getDataTable(cmda))
                                                                if (dta.Rows.Count > 0)
                                                                {
                                                                    string product_type = dta.Rows[0]["product_type"].ToString().Trim();
                                                                    bool is_new_customer = (bool)dta.Rows[0]["is_new_customer"];
                                                                    //if (product_type == "1" && is_new_customer == false) //一般月結
                                                                    //{
                                                                    //    if ((pieces != "1" && ProductId != "月結多筆") || (pieces == "1" && ProductId != "月結單筆"))
                                                                    //    {
                                                                    //        IsSheetOk = false;
                                                                    //        lbMsg.Items.Add("第" + strRow + "列： 請確認【規格、件數】是否相符!");
                                                                    //    }
                                                                    //}

                                                                    if (product_type == "2" && is_new_customer == true) //預購袋 新客代
                                                                    {
                                                                        if (SpecCodeId.Trim() != "3號袋")
                                                                        {
                                                                            IsSheetOk = false;
                                                                            lbMsg.Items.Add("第" + strRow + "列： 此客代類型僅能選擇3號袋");
                                                                        }

                                                                    }
                                                                    if (product_type == "6")
                                                                    {
                                                                        using (SqlCommand cmd9 = new SqlCommand())
                                                                        {

                                                                            cmd9.CommandText = "select ProductId, CodeId,CodeName from ProductValuationManage P with(nolock) left join ItemCodes I with(nolock) on i.CodeId = p.Spec and i.CodeType = 'ProductSpec'where ProductId = @ProductId and  CustomerCode = @customerCode order by i.OrderBy asc";
                                                                            cmd9.Parameters.AddWithValue("@customerCode", dlcustomer_code.SelectedValue);


                                                                            cmd9.Parameters.AddWithValue("@ProductId", "CM000043");

                                                                            DataTable dtb = dbAdapter.getDataTableForFSE01(cmd9);
                                                                            var SpecId_List = new List<string>();
                                                                            for (int k = 0; k < dtb.Rows.Count; k++)
                                                                            {
                                                                                SpecId_List.Add(dtb.Rows[k]["CodeName"].ToString());
                                                                            }


                                                                            bool checkSpec = SpecId_List.Contains(SpecCodeId);

                                                                            if (!checkSpec)
                                                                            {
                                                                                IsSheetOk = false;
                                                                                lbMsg.Items.Add("第" + strRow + "列： 此客代類型沒有此種產品規格");
                                                                            }

                                                                        }
                                                                    }


                                                                }
                                                        }


                                                        if (String.IsNullOrEmpty(SpecCodeId))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }

                                                        #endregion
                                                        break;
                                                    case 9:
                                                        #region 指配日期
                                                        if (cell != null)
                                                        {
                                                            switch (cell.CellType)
                                                            {
                                                                case CellType.Blank: //空数据类型处理
                                                                    arrive_assign_date = "NULL";
                                                                    break;
                                                                case CellType.String: //字符串类型
                                                                    arrive_assign_date = Row.GetCell(j).ToString();
                                                                    if (String.IsNullOrEmpty(arrive_assign_date))
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.Numeric: //数字类型       
                                                                    if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                                    {
                                                                        arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                                    }
                                                                    else
                                                                    {
                                                                        IsChkOk =
                                                                        IsSheetOk = false;
                                                                    }
                                                                    break;
                                                                case CellType.Formula: //公式
                                                                    try
                                                                    {
                                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                        if (formulaValue.CellType == CellType.String)
                                                                        {
                                                                            DateTime _dt;
                                                                            if (DateTime.TryParse(formulaValue.StringValue.ToString(), out _dt))
                                                                            {
                                                                                arrive_assign_date = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                            }
                                                                            else
                                                                            {
                                                                                arrive_assign_date = "NULL";
                                                                            }

                                                                        }
                                                                        else if (formulaValue.CellType == CellType.Numeric)
                                                                        {
                                                                            if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                                            {
                                                                                arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                                            }
                                                                            else
                                                                            {
                                                                                arrive_assign_date = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                            }
                                                                        }

                                                                    }
                                                                    catch
                                                                    {

                                                                    }
                                                                    //if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                                    //{
                                                                    //    arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                                    //}
                                                                    //else
                                                                    //{
                                                                    //    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    //    if (formulaValue.CellType == CellType.String) arrive_assign_date = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    //    else if (formulaValue.CellType == CellType.Numeric) arrive_assign_date = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                    //}
                                                                    break;
                                                            }
                                                            DateTime _arrive_assign_date;
                                                            if (DateTime.TryParse(arrive_assign_date, out _arrive_assign_date) && _arrive_assign_date > print_date)
                                                            {
                                                                supplier_date = _arrive_assign_date;     //如果有指配日期，則配送日帶指配日期
                                                            }
                                                            else
                                                            {
                                                                arrive_assign_date = "NULL";
                                                                _supplier_date = "NULL";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            arrive_assign_date = "NULL";
                                                        }
                                                        #endregion
                                                        break;
                                                    case 10:
                                                        #region 指配時段 早/午/晚
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) time_period = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) time_period = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    time_period = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                time_period = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 11:
                                                        #region 代收貨款
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) collection_money = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) collection_money = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態                                      
                                                                }
                                                                catch
                                                                {
                                                                    collection_money = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var cellValue = Row.GetCell(j).ToString();

                                                                collection_money = cellValue.Length > 0 ? cellValue : "0";
                                                            }
                                                        }
                                                        if (!String.IsNullOrEmpty(collection_money) && !Utility.IsNumeric(collection_money))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        if (!string.IsNullOrEmpty(collection_money))
                                                        {
                                                            bool is_new_customer = false;
                                                            using (SqlCommand cmd = new SqlCommand())
                                                            {
                                                                cmd.CommandText = "select is_new_customer from tbCustomers where customer_code = @customer_code";
                                                                cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                                var table = dbAdapter.getDataTable(cmd);
                                                                is_new_customer = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
                                                            }
                                                            //新契約客戶代收上限5萬
                                                            if (is_new_customer == true && Convert.ToInt32(collection_money) > 50000 && (dlcustomer_code.SelectedValue.ToString() != "F2900000402"))
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 【代收貨款】超過50,000上限!");
                                                            }
                                                            //舊契約 代收上限2萬  
                                                            else if (Convert.ToInt32(collection_money) > 20000 && (dlcustomer_code.SelectedValue.ToString() != "F2900000402"))
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 【代收貨款】超過20,000上限!");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            collection_money = "0";
                                                        }

                                                        #region 傳票類別
                                                        //傳票類別預設為元付
                                                        subpoena_category = "11";
                                                        //代收貨款格式正確，若代收貨款金額>0，傳票類別改為代收貨款

                                                        int numMoney;

                                                        if (int.TryParse(collection_money, out numMoney))
                                                        {
                                                            subpoena_category = (numMoney > 0) ? "41" : "11";
                                                        }


                                                        #endregion
                                                        #endregion
                                                        break;
                                                    case 12:
                                                        #region 報值金額
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) ReportFee = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) ReportFee = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態                                      
                                                                }
                                                                catch
                                                                {
                                                                    ReportFee = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var cellValue = Row.GetCell(j).ToString();

                                                                ReportFee = cellValue.Length > 0 ? cellValue : "0";
                                                            }
                                                        }
                                                        if (!String.IsNullOrEmpty(ReportFee) && !Utility.IsNumeric(ReportFee))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        bool is_new_customer_r = false;
                                                        using (SqlCommand cmd = new SqlCommand())
                                                        {
                                                            cmd.CommandText = "select is_new_customer from tbCustomers where customer_code = @customer_code";
                                                            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                            var table = dbAdapter.getDataTable(cmd);
                                                            is_new_customer_r = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
                                                        }
                                                        if (!string.IsNullOrEmpty(ReportFee) && ReportFee != "0")
                                                        {

                                                            //新契約客戶報值金額上限5萬
                                                            if (is_new_customer_r == true && Convert.ToInt32(ReportFee) > 50000)
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 【報值金額】超過50,000上限!");
                                                            }
                                                            //新契約客戶代收大於等於1萬 強制寫入保值費
                                                            else if (is_new_customer_r == true && Convert.ToInt32(collection_money) >= 10000)
                                                            {
                                                                if (string.IsNullOrEmpty(ReportFee)) { ReportFee = "0"; }
                                                                //取大者寫入*1% 寫入保值費
                                                                if (Convert.ToInt32(ReportFee) > Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(ReportFee) * 0.01)).ToString();
                                                                }
                                                                else if (Convert.ToInt32(ReportFee) < Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(collection_money) * 0.01)).ToString();
                                                                }
                                                                lbMsg.Items.Add("第" + strRow + "列： 代收金額大於1萬，保值費用: " + ProductValue + " 元");
                                                            }
                                                            //舊契約 不可填報值金額  
                                                            else if (is_new_customer_r == false)
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 此客代無須填寫【報值金額】");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ReportFee = "0";
                                                            if (is_new_customer_r == true && Convert.ToInt32(collection_money) >= 10000)
                                                            {
                                                                if (string.IsNullOrEmpty(ReportFee)) { ReportFee = "0"; }
                                                                //取大者寫入*1% 寫入保值費
                                                                if (Convert.ToInt32(ReportFee) > Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(ReportFee) * 0.01)).ToString();
                                                                }
                                                                else if (Convert.ToInt32(ReportFee) < Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(collection_money) * 0.01)).ToString();
                                                                }
                                                                lbMsg.Items.Add("第" + strRow + "列： 代收金額大於1萬，保值費用: " + ProductValue + " 元");
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 13:
                                                        #region 保值費(Y/N)
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) HasProductValue = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) HasProductValue = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    HasProductValue = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                HasProductValue = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        HasProductValue = (HasProductValue == "Y") ? "1" : "0";


                                                        //新客代有勾選,取大者寫入；舊客代不適用
                                                        bool is_new_customer_p = false;
                                                        using (SqlCommand cmd = new SqlCommand())
                                                        {
                                                            cmd.CommandText = "select is_new_customer from tbCustomers where customer_code = @customer_code";
                                                            cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                            var table = dbAdapter.getDataTable(cmd);
                                                            is_new_customer_p = table.Rows[0]["is_new_customer"].ToString().ToLower() == "true" ? true : false;
                                                        }
                                                        if (HasProductValue == "1")
                                                        {
                                                            if (is_new_customer_p == true)
                                                            {
                                                                if (string.IsNullOrEmpty(collection_money)) { collection_money = "0"; }
                                                                if (string.IsNullOrEmpty(ReportFee)) { ReportFee = "0"; }
                                                                //取大者寫入*1% 寫入保值費
                                                                if (Convert.ToInt32(ReportFee) > Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(ReportFee) * 0.01)).ToString();
                                                                }
                                                                else if (Convert.ToInt32(ReportFee) < Convert.ToInt32(collection_money))
                                                                {
                                                                    ProductValue = (Math.Ceiling(Convert.ToInt32(collection_money) * 0.01)).ToString();
                                                                }
                                                                lbMsg.Items.Add("第" + strRow + "列： 保值費用: " + ProductValue + " 元");
                                                            }
                                                            else
                                                            {
                                                                IsSheetOk = false;
                                                                lbMsg.Items.Add("第" + strRow + "列： 此客代不適用【是否需要保值】");
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 14:
                                                        #region 來回件(Y/N)
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) round_trip = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) round_trip = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    round_trip = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                round_trip = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        round_trip = (round_trip == "Y") ? "1" : "0";

                                                        if (is_account_preorder)
                                                        {
                                                            if (round_trip == "1")
                                                            {
                                                                round_trip = "0";
                                                                lbMsg.Items.Add("【[預購袋/超值箱袋]不可使用來回件，已將第" + strRow + "列貨件轉為一般件匯入】");
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 15:
                                                        #region 統倉
                                                        if (cell != null)
                                                        {

                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) WareHouse = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) WareHouse = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    WareHouse = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                WareHouse = Row.GetCell(j).ToString();
                                                            }

                                                        }
                                                        WareHouse = (WareHouse == "Y") ? "1" : "0";

                                                        #endregion
                                                        break;
                                                    case 16:
                                                        #region 託運備註
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) invoice_desc = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) invoice_desc = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    invoice_desc = cell.StringCellValue;
                                                                }

                                                            }
                                                            else
                                                            {
                                                                invoice_desc = Row.GetCell(j).ToString();
                                                            }
                                                        }

                                                        #region 依託運類別檢查數量
                                                        if (pieces == "")
                                                        {
                                                            IsSheetOk = false;
                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!");
                                                        }


                                                        #endregion
                                                        #endregion
                                                        break;
                                                    case 17:
                                                        #region 回單
                                                        if (cell != null)
                                                        {

                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) receipt_flag = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) receipt_flag = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    receipt_flag = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                receipt_flag = Row.GetCell(j).ToString();
                                                            }

                                                        }
                                                        receipt_flag = (receipt_flag == "Y") ? "1" : "0";
                                                        //回單及來回件不能同時勾選
                                                        if (round_trip == "1" && receipt_flag == "1")
                                                        {
                                                            IsSheetOk = false;
                                                            lbMsg.Items.Add("第" + strRow + "列： 【回單、來回件】僅能選擇其一!");
                                                        }
                                                        //聯運區不能選用回單
                                                        if ((area_arrive_code.Contains("9") && receipt_flag == "1") || (ShuttleStationCode == "999" && receipt_flag == "1"))
                                                        {
                                                            IsSheetOk = false;
                                                            lbMsg.Items.Add("第" + strRow + "列： 此收件區無法選用【回單】!");
                                                        }
                                                        #endregion
                                                        break;
                                                    case 18:
                                                        #region 產品編號
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) ArticleNumber = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) ArticleNumber = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    ArticleNumber = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ArticleNumber = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 19:
                                                        #region 出貨平台
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) SendPlatform = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) SendPlatform = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    SendPlatform = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                SendPlatform = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 20:
                                                        #region 品名
                                                        if (cell != null)
                                                        {
                                                            if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                            {
                                                                try
                                                                {
                                                                    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                                    if (formulaValue.CellType == CellType.String) ArticleName = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                    else if (formulaValue.CellType == CellType.Numeric) ArticleName = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                                catch
                                                                {
                                                                    ArticleName = cell.StringCellValue;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ArticleName = Row.GetCell(j).ToString();
                                                            }
                                                        }
                                                        #endregion
                                                        break;
                                                    case 21:
                                                        #region 託運單號
                                                        //if (chk == 1)  //一般件
                                                        //{
                                                        //    if (cell != null)
                                                        //    {

                                                        //        if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                        //        {
                                                        //            try
                                                        //            {
                                                        //                var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        //                if (formulaValue.CellType == CellType.String) check_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        //                else if (formulaValue.CellType == CellType.Numeric) check_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        //            }
                                                        //            catch
                                                        //            {
                                                        //                check_number = cell.StringCellValue;
                                                        //            }
                                                        //        }
                                                        //        else
                                                        //        {
                                                        //            check_number = Row.GetCell(j).ToString();
                                                        //        }

                                                        //        if (String.IsNullOrEmpty(check_number) && IsHaveNum == false)
                                                        //        {
                                                        //            IsSheetOk = false;
                                                        //            lbMsg.Items.Add("第" + strRow + "列：" + dlcustomer_code.SelectedItem.Text + "沒有設定貨號區間或預購袋號碼【託運單號】不允許空白");
                                                        //        }
                                                        //        else if (!String.IsNullOrEmpty(check_number))
                                                        //        {

                                                        //            //檢查貨號一個月內是否使用
                                                        //            bool IsUse = false;
                                                        //            using (SqlCommand cmdt = new SqlCommand())
                                                        //            {
                                                        //                DataTable dtt;
                                                        //                cmdt.Parameters.AddWithValue("@customer_code", customer_code);
                                                        //                cmdt.Parameters.AddWithValue("@check_number", check_number);
                                                        //                cmdt.CommandText = @" Select * from tcDeliveryRequests with(nolock) where customer_code=@customer_code 
                                                        //        and check_number =@check_number and print_date >=  DATEADD(MONTH,-1,getdate())";
                                                        //                dtt = dbAdapter.getDataTable(cmdt);
                                                        //                if (dtt.Rows.Count > 0)
                                                        //                {
                                                        //                    IsUse = true;
                                                        //                    IsSheetOk = false;
                                                        //                    lbMsg.Items.Add("第" + strRow + "列：" + check_number + "託運單號已被使用");
                                                        //                }
                                                        //            }



                                                        //            if (IsUse == false)
                                                        //            {
                                                        //                using (SqlCommand cmdt = new SqlCommand())
                                                        //                {
                                                        //                    DataTable dtt;
                                                        //                    cmdt.Parameters.AddWithValue("@customer_code", customer_code);
                                                        //                    cmdt.Parameters.AddWithValue("@check_number", check_number);
                                                        //                    cmdt.CommandText = @" Select current_number,seq,customer_code  from tbDeliveryNumberSetting where customer_code=@customer_code and @check_number between  begin_number and  end_number AND (IsActive = 1)";
                                                        //                    dtt = dbAdapter.getDataTable(cmdt);
                                                        //                    if (dtt.Rows.Count == 0)
                                                        //                    {
                                                        //                        IsSheetOk = false;
                                                        //                        lbMsg.Items.Add("第" + strRow + "列：" + check_number + "不屬於" + dlcustomer_code.SelectedItem.Text + "的貨號區間");
                                                        //                    }
                                                        //                    else
                                                        //                    {
                                                        //                        if (Convert.ToInt64(check_number) > Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()))
                                                        //                        {


                                                        //                            using (SqlCommand cmd = new SqlCommand())
                                                        //                            {

                                                        //                                cmd.Parameters.AddWithValue("@current_number", Convert.ToInt64(check_number));
                                                        //                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_code", dtt.Rows[0]["customer_code"].ToString());
                                                        //                                cmd.CommandText = dbAdapter.genUpdateComm("tbDeliveryNumberSetting", cmd);
                                                        //                                dbAdapter.execNonQuery(cmd);
                                                        //                            }
                                                        //                        }
                                                        //                    }
                                                        //                }

                                                        //            }
                                                        //        }
                                                        //    }
                                                        //}
                                                        check_number = "";   //不論一般件或預購袋只能系統取號

                                                        #endregion
                                                        break;

                                                }

                                                if (!IsChkOk)
                                                {
                                                    lbMsg.Items.Add("託運單匯入失敗，請下載使用新版託運單，重新匯入託運明細");
                                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!");
                                                }
                                            }

                                            // 判斷是否為假日配
                                            DateTime assign_date;
                                            if (arrive_assign_date != "NULL" && DateTime.TryParse(arrive_assign_date, out assign_date))
                                            {
                                                has_holiday_delivery = Utility.IsHolidayDelivery(customer_code, area_arrive_code, assign_date);
                                                holiday_delivery = has_holiday_delivery ? "1" : "0";
                                            }

                                            if (has_holiday_delivery && is_account_preorder)
                                            {
                                                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('每筆假日配送貨件將另加價40元，如果不同意請銷單後重新匯入託運單');</script>", false);
                                            }
                                        }

                                        #endregion
                                        #region 組新增

                                        //如果代收貨款>0，則傳票類別必定為代收貨款
                                        if (collection_money.Length > 0 && !collection_money.Equals("0"))
                                        { subpoena_code = "41"; }//代收貨款
                                        else
                                        { subpoena_code = "11"; }//元付

                                        receive_address = receive_address.Replace("'", "＇");
                                        receive_address = receive_address.Replace("--", "－");
                                        receive_tel1 = (receive_tel1.Length > 20 ? receive_tel1.Substring(0, 20) : receive_tel1);
                                        receive_tel2 = (receive_tel2.Length > 20 ? receive_tel2.Substring(0, 20) : receive_tel2);
                                        receive_contact = receive_contact.Replace("\n", "");
                                        receive_contact = (receive_contact.Length > 40 ? receive_contact.Substring(0, 40) : receive_contact);
                                        receive_address = receive_address.Replace("\n", "");
                                        receive_address = (receive_address.Length > 100 ? receive_address.Substring(0, 100) : receive_address);
                                        ArticleNumber = (ArticleNumber.Length > 20 ? ArticleNumber.Substring(0, 20) : ArticleNumber);
                                        SendPlatform = (SendPlatform.Length > 20 ? SendPlatform.Substring(0, 20) : SendPlatform);
                                        ArticleName = (ArticleName.Length > 20 ? ArticleName.Substring(0, 20) : ArticleName);
                                        receive_contact = NewString(receive_contact, 1);
                                        receive_address = NewString(receive_address, 1);
                                        invoice_desc = NewString(invoice_desc, 1);
                                        payment_method = NewString(payment_method, 1);
                                        theSecondRemark = NewString(theSecondRemark, 1);
                                        theThirdRemark = NewString(theThirdRemark, 1);

                                        if (theSecondRemark != null)
                                        {
                                            invoice_desc += "" + theSecondRemark;
                                        }

                                        if (theThirdRemark != null)
                                        {
                                            invoice_desc += "" + theThirdRemark;
                                        }

                                        if (pieces == "")
                                        {
                                            pieces = "1";
                                        }
                                        //switch (CbmSize)
                                        //{
                                        //    case "S60":
                                        //        CbmSize = "1";
                                        //        break;
                                        //    case "S90":
                                        //        CbmSize = "2";
                                        //        break;
                                        //    case "S110":
                                        //        CbmSize = "6";
                                        //        break;
                                        //    case "S120":
                                        //        CbmSize = "3";
                                        //        break;
                                        //    case "S150":
                                        //        CbmSize = "4";
                                        //        break;
                                        //    case "袋裝":
                                        //        CbmSize = "7";
                                        //        break;
                                        //    default:
                                        //        CbmSize = "2";
                                        //        break;
                                        //}

                                        //switch (ProductId)    //產品名稱
                                        //{
                                        //    case "多筆":
                                        //        ProductId = "CM000030";
                                        //        break;
                                        //    case "月結多筆":
                                        //        ProductId = "CM000036";
                                        //        break;
                                        //    case "單筆":
                                        //        ProductId = "CS000029";
                                        //        break;
                                        //    case "月結單筆":
                                        //        ProductId = "CS000035";
                                        //        break;
                                        //    case "內部文件":
                                        //        ProductId = "CS000037";
                                        //        break;
                                        //    case "包材":
                                        //        ProductId = "CS000038";
                                        //        break;
                                        //    case "預購袋":
                                        //        ProductId = "PS000031";
                                        //        break;
                                        //    case "速配箱":
                                        //        ProductId = "PS000032";
                                        //        break;
                                        //    case "超值袋":
                                        //        ProductId = "PS000033";
                                        //        break;
                                        //    case "超值箱":
                                        //        ProductId = "PS000034";
                                        //        break;
                                        //    default:
                                        //        ProductId = "";
                                        //        break;
                                        //}
                                        if (Convert.ToInt32(pieces) > 1)
                                        {
                                            //if(is_new_customer_insert == true)
                                            //{
                                            //    ProductId = "CM000030";
                                            //}
                                            //else if (is_new_customer_insert == false)
                                            //{
                                            //    ProductId = "CM000036";
                                            //}
                                            //else if (product_type_insert == "6")
                                            //{
                                            //    ProductId = "CM000043";
                                            //}

                                            switch (ProductId)    //產品名稱
                                            {
                                                case "論S":
                                                    ProductId = "CM000030";
                                                    break;
                                                case "月結":
                                                    ProductId = "CM000036";
                                                    break;
                                                case "論件":
                                                    ProductId = "CM000043";
                                                    break;
                                                case "內部文件":
                                                    ProductId = "CS000037";
                                                    break;
                                                case "包材":
                                                    ProductId = "CS000038";
                                                    break;
                                                case "預購袋":
                                                    ProductId = "PS000031";
                                                    break;
                                                case "速配箱":
                                                    ProductId = "PS000032";
                                                    break;
                                                case "超值袋":
                                                    ProductId = "PS000033";
                                                    break;
                                                case "超值箱":
                                                    ProductId = "PS000034";
                                                    break;
                                                default:
                                                    ProductId = "";
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            switch (ProductId)    //產品名稱
                                            {
                                                case "論S":
                                                    ProductId = "CM000030";
                                                    break;
                                                case "月結":
                                                    ProductId = "CS000035";
                                                    break;
                                                case "論件":
                                                    ProductId = "CM000043";
                                                    break;
                                                case "內部文件":
                                                    ProductId = "CS000037";
                                                    break;
                                                case "包材":
                                                    ProductId = "CS000038";
                                                    break;
                                                case "預購袋":
                                                    ProductId = "PS000031";
                                                    break;
                                                case "速配箱":
                                                    ProductId = "PS000032";
                                                    break;
                                                case "超值袋":
                                                    ProductId = "PS000033";
                                                    break;
                                                case "超值箱":
                                                    ProductId = "PS000034";
                                                    break;
                                                default:
                                                    ProductId = "";
                                                    break;
                                            }
                                        }

                                        switch (SpecCodeId)    //產品規格
                                        {
                                            case "1號袋":
                                                SpecCodeId = "B001";
                                                CbmSize = "7";
                                                break;
                                            case "2號袋":
                                                SpecCodeId = "B002";
                                                CbmSize = "7";
                                                break;
                                            case "3號袋":
                                                SpecCodeId = "B003";
                                                CbmSize = "7";
                                                break;
                                            case "4號袋":
                                                SpecCodeId = "B004";
                                                CbmSize = "7";
                                                break;
                                            case "1號箱":
                                                SpecCodeId = "X001";
                                                CbmSize = "2";  //箱裝預設S90
                                                break;
                                            case "2號箱":
                                                SpecCodeId = "X002";
                                                CbmSize = "2";  //箱裝預設S90
                                                break;
                                            case "5號箱":
                                                SpecCodeId = "X005";
                                                CbmSize = "2";  //箱裝預設S90
                                                break;
                                            case "S60":
                                                SpecCodeId = "S060";
                                                CbmSize = "1";
                                                break;
                                            case "S90":
                                                SpecCodeId = "S090";
                                                CbmSize = "2";
                                                break;
                                            case "S110":
                                                SpecCodeId = "S110";
                                                CbmSize = "6";
                                                break;
                                            case "S120":
                                                SpecCodeId = "S120";
                                                CbmSize = "3";
                                                break;
                                            case "S150":
                                                SpecCodeId = "S150";
                                                CbmSize = "4";
                                                break;
                                            case "不指定":
                                                SpecCodeId = "N001";
                                                break;
                                            case "固定價格":
                                                SpecCodeId = "9999";
                                                break;
                                            case "1才內":
                                                SpecCodeId = "C001";
                                                break;
                                            case "2才內":
                                                SpecCodeId = "C002";
                                                break;
                                            case "3才內":
                                                SpecCodeId = "C003";
                                                break;
                                            case "4才內":
                                                SpecCodeId = "C004";
                                                break;
                                            case "5才內":
                                                SpecCodeId = "C005";
                                                break;
                                            default:
                                                SpecCodeId = "";
                                                break;
                                        }

                                        strInsCol = "pricing_type,customer_code,check_number,order_number,CbmSize,ProductId,SpecCodeId,check_type,subpoena_category,receive_tel1,receive_tel2,receive_contact,receive_city,receive_area,receive_address,area_arrive_code,ShuttleStationCode,SpecialAreaId,SpecialAreaFee,SendCode,SendSD,SendMD,ReceiveCode,ReceiveSD,ReceiveMD,receive_by_arrive_site_flag" +
                                                    " ,pieces,cbmWeight,collection_money,ReportFee,ProductValue,send_contact,send_city,send_area,send_address,send_tel,product_category,invoice_desc,time_period,arrive_assign_date" +
                                                    " ,receipt_flag,WareHouse,pallet_recycling_flag,cuser,cdate,uuser,udate,supplier_code,supplier_name,import_randomCode,print_date, supplier_date, print_flag, turn_board,upstairs,difficult_delivery" +
                                                    " ,turn_board_fee,upstairs_fee,difficult_fee, add_transfer, Less_than_truckload, sub_check_number,ArticleNumber,SendPlatform,ArticleName,round_trip,payment_method,holiday_delivery";
                                        strInsVal = "N\'" + pricing_type + "\', " +
                                                    "N\'" + customer_code + "\', " +
                                                    "N\'" + check_number + "\', " +
                                                    "N\'" + order_number + "\', " +
                                                    "N\'" + CbmSize + "\', " +
                                                    "N\'" + ProductId + "\', " +
                                                    "N\'" + SpecCodeId + "\', " +
                                                    "N\'" + check_type + "\', " +
                                                    "N\'" + subpoena_code + "\', " +
                                                    "N\'" + receive_tel1 + "\', " +
                                                    "N\'" + receive_tel2 + "\', " +
                                                    "N\'" + receive_contact + "\', " +
                                                    "N\'" + receive_city + "\', " +
                                                    "N\'" + receive_area + "\', " +
                                                    "N\'" + receive_address + "\', " +
                                                    "N\'" + area_arrive_code + "\', " +
                                                    "N\'" + ShuttleStationCode + "\', " +
                                                    "N\'" + SpecialAreaId + "\', " +
                                                    "N\'" + SpecialAreaFee + "\', " +
                                                    "N\'" + SendCode + "\', " +
                                                    "N\'" + SendSD + "\', " +
                                                    "N\'" + SendMD + "\', " +
                                                    "N\'" + ReceiveCode + "\', " +
                                                    "N\'" + ReceiveSD + "\', " +
                                                    "N\'" + ReceiveMD + "\', " +
                                                    "N\'" + receive_by_arrive_site_flag + "\', " +
                                                    "N\'" + pieces + "\', " +
                                                    "N\'" + cbmWeight + "\', " +
                                                    "N\'" + collection_money + "\', " +
                                                    "N\'" + ReportFee + "\', " +
                                                    "N\'" + ProductValue + "\', " +
                                                    "N\'" + (send_contact.Length > 20 ? send_contact.Substring(0, 20) : send_contact) + "\', " +
                                                    "N\'" + send_city + "\', " +
                                                    "N\'" + send_area + "\', " +
                                                    "N\'" + send_address + "\', " +
                                                    "N\'" + send_tel + "\', " +
                                                    "N\'" + product_category + "\', " +
                                                    "N\'" + invoice_desc + "\', " +
                                                    "N\'" + time_period + "\', " +
                                                    "N\'" + arrive_assign_date + "\', ";

                                        strInsVal = strInsVal.Replace("N'NULL'", " NULL");

                                        strInsVal += "N\'" + receipt_flag + "\', " +
                                                    "N\'" + WareHouse + "\', " +
                                                    "N\'" + pallet_recycling_flag + "\', " +
                                                    "N\'" + Session["account_code"] + "\', " +
                                                    "GETDATE(), " +
                                                    "N\'" + Session["account_code"] + "\', " +
                                                    "GETDATE(), " +
                                                    "N\'" + supplier_code + "\', " +
                                                    "N\'" + supplier_name + "\', " +
                                                    "N\'" + randomCode + "\', " +
                                                    "N\'" + print_date.ToString("yyyy/MM/dd") + "\', ";

                                        if (_supplier_date == "NULL")
                                        {
                                            strInsVal += _supplier_date + ", ";
                                        }
                                        else
                                        {
                                            strInsVal += "N\'" + supplier_date.ToString("yyyy/MM/dd") + "\', ";
                                        }

                                        strInsVal += "N\'" + print_flag + "\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'0\', " +
                                                   "N\'" + "1" + "\', " +
                                                   "N\'" + sub_check_number + "\', " +
                                                   "N\'" + ArticleNumber + "\', " +
                                                   "N\'" + SendPlatform + "\', " +
                                                   "N\'" + ArticleName + "\', " +
                                                   "N\'" + round_trip + "\', " +
                                                   "N\'" + payment_method + "\', " +
                                                   "N\'" + holiday_delivery + "\'";

                                        // 回單的 area_arrive_code 改成寄件人區配地區
                                        string sendStation = dlcustomer_code.SelectedValue.Substring(1, 2);

                                        strInsCol += ",send_station_scode";
                                        strInsVal += string.Format(", '{0}'", sendStation);




                                        string checkNumberR = "";
                                        bool isRoundTrip = false;

                                        if (round_trip == "1")
                                        {
                                            isRoundTrip = true;
                                            returnCounter += 1;
                                            using (SqlCommand cmdr = new SqlCommand())
                                            {
                                                cmdr.CommandText = "select next value for ttAutoNumeral";
                                                var table = dbAdapter.getDataTable(cmdr);
                                                checkNumberR = table.Rows[0][0].ToString();

                                                strInsCol += ",roundtrip_checknumber";
                                                strInsVal += string.Format(", '{0}'", checkNumberR);
                                            }
                                        }

                                        string insertstr = "/*" + strRow + "*/ INSERT INTO tcDeliveryRequests (" + strInsCol + ") VALUES(" + strInsVal + ");\n ";

                                        string strInsColR = "";
                                        string strInsValR = "";
                                        string insertstrR = "";
                                        if (isRoundTrip)
                                        {
                                            strInsColR = strInsCol + ",deliveryType";
                                            strInsValR = strInsVal + ",'R'";
                                            strInsColR = SqlReturn(strInsColR);
                                            strInsValR = SqlValueReturn(strInsValR);   //回件代收改0、SpecialAreaId、SpecialAreaFee歸零
                                            insertstrR = "INSERT INTO tcDeliveryRequests (" + strInsColR + ") VALUES(" + strInsValR + ");\n ";
                                            insertstr += insertstrR;
                                        }

                                        //if (chk == 1)
                                        //{
                                        sb_ins_list.Add(insertstr);
                                        sb_temp_ins = string.Empty;
                                        //}
                                        //else if (chk == 2)
                                        //{
                                        //    int temp_count = i - 2;

                                        //    if (temp_count <= LeftCount)
                                        //    {
                                        //        sb_ins_list.Add(insertstr);
                                        //    }
                                        //    else
                                        //    {
                                        //        lbMsg.Items.Add("第" + strRow + "列： 無法匯入，因為已經使用完預購袋號碼，請洽管理員!");
                                        //    }

                                        //    sb_temp_ins = string.Empty;
                                        //}
                                        #endregion
                                    }
                                }
                                catch (Exception eeee)
                                {

                                }




                            }

                            //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                            int uploadSuccessNum = 0;
                            if (IsSheetOk)
                            {
                                #region 執行SQL

                                startTime = DateTime.Now;   //開始匯入時間
                                                            //新增                                

                                foreach (var sqlstr in sb_ins_list)
                                {
                                    if (sqlstr.Length > 0)
                                    {
                                        using (SqlCommand cmd = new SqlCommand())
                                        {
                                            cmd.CommandText = sqlstr;
                                            try
                                            {
                                                dbAdapter.execQuery(cmd);
                                                uploadSuccessNum++;
                                            }
                                            catch (SqlException ex)
                                            {
                                                // 拆出錯誤列資料
                                                int firstStar = sqlstr.IndexOf("*");
                                                if (firstStar < 0)
                                                    break;

                                                int secondStar = sqlstr.IndexOf("*", firstStar + 1);
                                                string row = sqlstr.Substring(firstStar + 1, secondStar - firstStar - 1);

                                                string section = sqlstr.Substring(sqlstr.IndexOf("VALUES"));

                                                int idxOrderNumber = 4;
                                                int searchFrom = 0;

                                                for (int i = 0; i < idxOrderNumber - 1; i++)
                                                {
                                                    searchFrom = section.IndexOf(",", searchFrom + 1);
                                                }
                                                string orderNumber = section.Substring(searchFrom + 1, section.IndexOf(",", searchFrom + 1) - searchFrom - 1);
                                                orderNumber = orderNumber.Replace("'", "").Trim().Substring(1);

                                                int idxReceiveCantact = 10;
                                                for (int i = idxOrderNumber - 1; i < idxReceiveCantact - 1; i++)
                                                {
                                                    searchFrom = section.IndexOf(",", searchFrom + 1);
                                                }
                                                string receiveCantact = section.Substring(searchFrom + 1, section.IndexOf(",", searchFrom + 1) - searchFrom - 1);
                                                receiveCantact = receiveCantact.Replace("'", "").Trim().Substring(1);

                                                if (ex.Number == 1205 || ex.Number == -2)  // deadlock or timeout
                                                {
                                                    Thread.Sleep(100);

                                                    for (int j = 0; j < 5; j++)
                                                    {
                                                        using (SqlCommand cmd2 = new SqlCommand())
                                                        {
                                                            cmd2.CommandText = sqlstr;
                                                            try
                                                            {
                                                                dbAdapter.execQuery(cmd2);
                                                                uploadSuccessNum++;
                                                                break;
                                                            }
                                                            catch (Exception)
                                                            {
                                                                if (j < 4)
                                                                    Thread.Sleep(100);
                                                                else
                                                                {
                                                                    string errorMessage = string.Format("第{0}列匯入失敗，收件人{1} 訂單編號：{2}，請重新上傳", row, receiveCantact, orderNumber);
                                                                    lbMsg.Items.Add(errorMessage);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    string errorMessage = string.Format("第{0}列匯入失敗，收件人{1} 訂單編號：{2}，請重新上傳", row, receiveCantact, orderNumber);
                                                    lbMsg.Items.Add(errorMessage);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                string errorMessage = string.Format("發生了預料外的錯誤：{0}", ex.Message);
                                                lbMsg.Items.Add(errorMessage);
                                            }
                                        }
                                    }
                                }
                                endTime = DateTime.Now;   //匯入結束時間
                                #endregion

                                #region  取貨號

                                if (chk == 1)
                                {
                                    if (check_number != "")
                                    {

                                    }
                                    else
                                    {
                                        string prefixCheckNumber = "103";

                                        using (SqlCommand sql = new SqlCommand())
                                        {
                                            sql.CommandText = string.Format(@"select check_number_prehead  
                                                from tbCustomers c
                                                join check_number_prehead pre on c.product_type = pre.product_type
                                                where customer_code = '{0}'", dlcustomer_code.SelectedValue);
                                            DataTable dataTable = dbAdapter.getDataTable(sql);
                                            if (dataTable != null && dataTable.Rows.Count > 0)
                                            {
                                                prefixCheckNumber = dataTable.Rows[0]["check_number_prehead"].ToString();
                                            }
                                        }

                                        using (SqlCommand sqlCommand = new SqlCommand())
                                        {
                                            sqlCommand.CommandText = string.Format(@"Select request_id,area_arrive_code,receive_city,receive_area,receive_address,round_trip,roundtrip_checknumber,cdate,cuser,ShuttleStationCode From tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'  and  (check_number = '' or check_number is null)");
                                            DataTable dataTable = dbAdapter.getDataTable(sqlCommand);
                                            if (dataTable != null && dataTable.Rows.Count > 0)
                                            {
                                                for (int i = 0; i < dataTable.Rows.Count; i++)
                                                {
                                                    string requestId = dataTable.Rows[i]["request_id"].ToString();

                                                    requestId = string.Format("{0,8:00000000}", int.Parse(requestId));

                                                    long check_code = Convert.ToInt64(string.Concat(prefixCheckNumber, requestId)) % 7;

                                                    string insert_check_number = string.Concat(prefixCheckNumber, requestId, check_code);

                                                    using (SqlCommand sqlCommand2 = new SqlCommand())
                                                    {
                                                        sqlCommand2.Parameters.AddWithValue("@check_number", insert_check_number);
                                                        sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", requestId);
                                                        sqlCommand2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", sqlCommand2);
                                                        dbAdapter.execNonQuery(sqlCommand2);
                                                    }
                                                    //寫入CBMDetailLog
                                                    using (SqlCommand cmdcbm = new SqlCommand())
                                                    {
                                                        var rountrip_s = "";
                                                        cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                                                        cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);
                                                        using (SqlCommand cmds = new SqlCommand())
                                                        {
                                                            cmds.CommandText = string.Format(@"Select check_number,SpecCodeId,roundtrip_checknumber From tcDeliveryRequests with(nolock) where check_number ='" + insert_check_number + "'");
                                                            DataTable dts = dbAdapter.getDataTable(cmds);
                                                            if (dts != null && dts.Rows.Count > 0)
                                                            {
                                                                SpecCodeId = dts.Rows[0]["SpecCodeId"].ToString();
                                                                rountrip_s = dts.Rows[0]["roundtrip_checknumber"].ToString();
                                                            }
                                                        }
                                                        cmdcbm.Parameters.AddWithValue("@CBM", SpecCodeId);
                                                        cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                                                        cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                                                        cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                                                        dbAdapter.execNonQuery(cmdcbm);

                                                        //如果有回件,同樣寫入CBMDetailLog
                                                        if (!string.IsNullOrEmpty(rountrip_s))
                                                        {
                                                            using (SqlCommand cmdcbmr = new SqlCommand())
                                                            {
                                                                cmdcbmr.Parameters.AddWithValue("@ComeFrom", "1");
                                                                cmdcbmr.Parameters.AddWithValue("@CheckNumber", rountrip_s);
                                                                cmdcbmr.Parameters.AddWithValue("@CBM", SpecCodeId);
                                                                cmdcbmr.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                                                                cmdcbmr.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                                                                cmdcbmr.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbmr);
                                                                dbAdapter.execNonQuery(cmdcbmr);
                                                            }
                                                        }
                                                    }
                                                    #region 95/99 接駁碼999 產生郵局流水號
                                                    if (dataTable.Rows[i]["area_arrive_code"].ToString() == "95" || dataTable.Rows[i]["area_arrive_code"].ToString() == "99" || dataTable.Rows[i]["ShuttleStationCode"].ToString() == "999")
                                                    {
                                                        var addr = dataTable.Rows[i]["receive_city"].ToString() + dataTable.Rows[i]["receive_area"].ToString() + dataTable.Rows[i]["receive_address"].ToString();
                                                        var zipcode = Func.GetZipCode(addr);

                                                        using (SqlCommand sql2 = new SqlCommand())
                                                        {
                                                            var cdate = dataTable.Rows[0]["cdate"];
                                                            var cuser = dataTable.Rows[0]["cuser"];
                                                            var strpost_number = Func.GetPostNumber();
                                                            var post_check_code = Func.GetPostNumberCheckCode(strpost_number, zipcode);
                                                            using (SqlCommand sqlPost = new SqlCommand())
                                                            {
                                                                var post_number = 0;
                                                                sqlPost.CommandText = string.Format(@"SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY request_id DESC) as row_id, post_number FROM [Post_request] with(nolock)) P WHERE row_id = 1 ");
                                                                DataTable tbPost = dbAdapter.getDataTable(sqlPost);
                                                                post_number = tbPost.Rows.Count == 0 || tbPost.Rows[0]["post_number"].ToString() == "999999" ? post_number : int.Parse(tbPost.Rows[0]["post_number"].ToString()) + 1;
                                                                strpost_number = string.Format("{0,6:000000}", post_number);
                                                            }

                                                            sql2.Parameters.AddWithValue("@jf_request_id", requestId);
                                                            sql2.Parameters.AddWithValue("@jf_check_number", insert_check_number);
                                                            sql2.Parameters.AddWithValue("@post_number", strpost_number);
                                                            sql2.Parameters.AddWithValue("@zipcode5", zipcode);
                                                            sql2.Parameters.AddWithValue("@check_code", post_check_code);
                                                            sql2.Parameters.AddWithValue("@cdate", cdate);
                                                            sql2.Parameters.AddWithValue("@udate", cdate);
                                                            sql2.Parameters.AddWithValue("@cuser", cuser);
                                                            sql2.Parameters.AddWithValue("@uuser", cuser);
                                                            sql2.CommandText = dbAdapter.genInsertComm("Post_request", false, sql2);
                                                            dbAdapter.execNonQuery(sql2);
                                                        }
                                                    }
                                                    #endregion
                                                    bool trip = false;
                                                    try
                                                    {
                                                        trip = bool.Parse(dataTable.Rows[i]["round_trip"].ToString());
                                                    }
                                                    catch
                                                    {
                                                    }
                                                    if (trip)
                                                    {
                                                        var roundtrip_checknumber = string.Empty;
                                                        try
                                                        {
                                                            roundtrip_checknumber = dataTable.Rows[i]["roundtrip_checknumber"].ToString();
                                                        }
                                                        catch
                                                        {
                                                        }
                                                        //來回件 (補roundtrip_checknumber)
                                                        PublicFunction _fun = new PublicFunction();
                                                        _fun.RepairRoundtrip_checknumber(roundtrip_checknumber, insert_check_number);
                                                    }
                                                }
                                            }

                                            //using (SqlCommand cmd = new SqlCommand())
                                            //{
                                            //    cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address, customer_code
                                            //                          From tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'  and  check_number = ''");
                                            //    DataTable dt = dbAdapter.getDataTable(cmd);
                                            //    if (dt != null && dt.Rows.Count > 0)
                                            //    {
                                            //        string _customer_code = dt.Rows[0]["customer_code"].ToString();

                                            //        #region 取貨號(網路託運單)
                                            //        Int64 current_number = 0;
                                            //        //int count = 0;
                                            //        //using (SqlCommand cmdt = new SqlCommand())
                                            //        //{

                                            //        //    cmdt.CommandText = "select Count(*) as total  from   tbDeliveryNumberSetting  where  customer_code=@customer_code";
                                            //        //    DataTable dtt;
                                            //        //    cmdt.Parameters.AddWithValue("@customer_code", _customer_code);

                                            //        //    dtt = dbAdapter.getDataTable(cmdt);

                                            //        //    count = Convert.ToInt32(dtt.Rows[0]["total"].ToString());
                                            //        //}


                                            //        using (SqlCommand cmdt = new SqlCommand())
                                            //        {
                                            //            DataTable dtt;
                                            //            cmdt.Parameters.AddWithValue("@customer_code", _customer_code);
                                            //            cmdt.CommandText = " Select * from tbDeliveryNumberSetting with(nolock) where customer_code=@customer_code and  IsActive=1   order by  begin_number ";
                                            //            dtt = dbAdapter.getDataTable(cmdt);
                                            //            if (dtt.Rows.Count == 1)
                                            //            {
                                            //                Int64 start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                            //                Int64 end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
                                            //                current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
                                            //                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                            //                {
                                            //                    if (current_number == 0 || current_number == end_number)
                                            //                    {
                                            //                        current_number = start_number;
                                            //                    }
                                            //                    else
                                            //                    {
                                            //                        current_number += 1;
                                            //                    }

                                            //                    using (SqlCommand cmd2 = new SqlCommand())
                                            //                    {

                                            //                        cmd2.Parameters.AddWithValue("@check_number", current_number);           //目前取號
                                            //                        cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                            //                        cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);         //修改
                                            //                        cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                                            //                        cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
                                            //                        cmd2.CommandText += " update tbDeliveryNumberSetting set current_number =@current_number where customer_code=@customer_code; ";
                                            //                        string updateOriginalCheckNumberForReturnCheckNumber = "declare @temp varchar(20);set @temp = (select TOP 1 return_check_number from tcDeliveryRequests with(nolock) where check_number = '" + current_number + "');update tcDeliveryRequests set return_check_number = '" + current_number + "' where check_number = @temp;";
                                            //                        cmd2.CommandText += updateOriginalCheckNumberForReturnCheckNumber;
                                            //                        dbAdapter.execNonQuery(cmd2);
                                            //                    }
                                            //                }
                                            //            }
                                            //            else if (dtt.Rows.Count > 1)
                                            //            {
                                            //                current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
                                            //                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                            //                {
                                            //                    if (current_number == 0)
                                            //                    {
                                            //                        current_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                            //                    }
                                            //                    //else
                                            //                    //{
                                            //                    int Counttemp = 0;
                                            //                    for (int z = 0; z <= dtt.Rows.Count - 1; z++)
                                            //                    {
                                            //                        Int64 start_number = Convert.ToInt64(dtt.Rows[z]["begin_number"]);
                                            //                        Int64 end_number = Convert.ToInt64(dtt.Rows[z]["end_number"]);
                                            //                        if (end_number == current_number)
                                            //                        {
                                            //                            Counttemp = Counttemp + 1;
                                            //                            if (z + 1 != dtt.Rows.Count)
                                            //                            {
                                            //                                current_number = Convert.ToInt64(dtt.Rows[z + 1]["begin_number"]);
                                            //                            }
                                            //                            else
                                            //                            {
                                            //                                current_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                            //                            }
                                            //                            break;
                                            //                        }

                                            //                    }
                                            //                    if (Counttemp == 0)
                                            //                    {
                                            //                        current_number = current_number + 1;
                                            //                    }

                                            //                    using (SqlCommand cmd2 = new SqlCommand())
                                            //                    {

                                            //                        cmd2.Parameters.AddWithValue("@check_number", current_number);           //目前取號
                                            //                        cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                            //                        cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);         //修改
                                            //                        cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                                            //                        cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
                                            //                        cmd2.CommandText += " update tbDeliveryNumberSetting set current_number =@current_number where customer_code=@customer_code ";
                                            //                        dbAdapter.execNonQuery(cmd2);
                                            //                    }


                                            //                    //}
                                            //                }


                                            //            }
                                            //        }
                                            //        #endregion


                                            //    }
                                        }
                                    }
                                }
                                else if (chk == 2)
                                {
                                    var prefixCheckNumber = "880"; //預購袋

                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = string.Format(@"Select A.request_id, A.customer_code, B.product_type,A.area_arrive_code,A.receive_city,A.receive_area,A.receive_address,A.cdate,A.cuser,ShuttleStationCode
                                                                      From tcDeliveryRequests A with(nolock)
                                                                      JOIN tbCustomers B With(Nolock) ON A.customer_code = B.customer_code
                                                                      where import_randomCode ='" + randomCode + "' and pricing_type <> '05' and (check_number = '' or check_number is null)");
                                        DataTable dt = dbAdapter.getDataTable(cmd);
                                        if (dt != null && dt.Rows.Count > 0)
                                        {
                                            string _customer_code = dt.Rows[0]["customer_code"].ToString();
                                            string productType = dt.Rows[0]["product_type"].ToString();

                                            if (productType == "3")
                                                prefixCheckNumber = "500"; //超值箱

                                            for (int i = 0; i < dt.Rows.Count; i++)
                                            {
                                                string requestId = dt.Rows[i]["request_id"].ToString();

                                                if (requestId.Length < 8 && productType == "2")
                                                    prefixCheckNumber = "8800";
                                                else if (requestId.Length < 8 && productType == "3")
                                                    prefixCheckNumber = "5000";

                                                long check_code = Convert.ToInt64(string.Concat(prefixCheckNumber, requestId)) % 7;

                                                string insert_check_number = string.Concat(prefixCheckNumber, requestId, check_code);

                                                using (SqlCommand sqlCommand2 = new SqlCommand())
                                                {
                                                    sqlCommand2.Parameters.AddWithValue("@check_number", insert_check_number);
                                                    sqlCommand2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", requestId);
                                                    sqlCommand2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", sqlCommand2);
                                                    dbAdapter.execNonQuery(sqlCommand2);
                                                }
                                                //寫入CBMDetailLog
                                                using (SqlCommand cmdcbm = new SqlCommand())
                                                {
                                                    cmdcbm.Parameters.AddWithValue("@ComeFrom", "1");
                                                    cmdcbm.Parameters.AddWithValue("@CheckNumber", insert_check_number);
                                                    using (SqlCommand cmds = new SqlCommand())
                                                    {
                                                        cmds.CommandText = string.Format(@"Select check_number,SpecCodeId From tcDeliveryRequests with(nolock) where check_number ='" + insert_check_number + "'");
                                                        DataTable dts = dbAdapter.getDataTable(cmds);
                                                        if (dts != null && dts.Rows.Count > 0)
                                                        { SpecCodeId = dts.Rows[0]["SpecCodeId"].ToString(); }
                                                    }
                                                    cmdcbm.Parameters.AddWithValue("@CBM", SpecCodeId);
                                                    cmdcbm.Parameters.AddWithValue("@CreateDate", DateTime.Now);
                                                    cmdcbm.Parameters.AddWithValue("@CreateUser", Session["account_code"]);
                                                    cmdcbm.CommandText = dbAdapter.genInsertComm("CBMDetailLog", false, cmdcbm);
                                                    dbAdapter.execNonQuery(cmdcbm);
                                                }

                                                #region 95/99 接駁碼999 產生郵局流水號
                                                if (dt.Rows[i]["area_arrive_code"].ToString() == "95" || dt.Rows[i]["area_arrive_code"].ToString() == "99" || dt.Rows[i]["ShuttleStationCode"].ToString() == "999")
                                                {
                                                    var addr = dt.Rows[i]["receive_city"].ToString() + dt.Rows[i]["receive_area"].ToString() + dt.Rows[i]["receive_address"].ToString();
                                                    var zipcode = Func.GetZipCode(addr);

                                                    using (SqlCommand sql2 = new SqlCommand())
                                                    {
                                                        var cdate = dt.Rows[0]["cdate"];
                                                        var cuser = dt.Rows[0]["cuser"];
                                                        var strpost_number = Func.GetPostNumber();
                                                        var post_check_code = Func.GetPostNumberCheckCode(strpost_number, zipcode);
                                                        using (SqlCommand sqlPost = new SqlCommand())
                                                        {
                                                            var post_number = 0;
                                                            sqlPost.CommandText = string.Format(@"SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY request_id DESC) as row_id, post_number FROM [Post_request] with(nolock)) P WHERE row_id = 1 ");
                                                            DataTable tbPost = dbAdapter.getDataTable(sqlPost);
                                                            post_number = tbPost.Rows.Count == 0 || tbPost.Rows[0]["post_number"].ToString() == "999999" ? post_number : int.Parse(tbPost.Rows[0]["post_number"].ToString()) + 1;
                                                            strpost_number = string.Format("{0,6:000000}", post_number);
                                                        }

                                                        sql2.Parameters.AddWithValue("@jf_request_id", requestId);
                                                        sql2.Parameters.AddWithValue("@jf_check_number", insert_check_number);
                                                        sql2.Parameters.AddWithValue("@post_number", strpost_number);
                                                        sql2.Parameters.AddWithValue("@zipcode5", zipcode);
                                                        sql2.Parameters.AddWithValue("@check_code", post_check_code);
                                                        sql2.Parameters.AddWithValue("@cdate", cdate);
                                                        sql2.Parameters.AddWithValue("@udate", cdate);
                                                        sql2.Parameters.AddWithValue("@cuser", cuser);
                                                        sql2.Parameters.AddWithValue("@uuser", cuser);
                                                        sql2.CommandText = dbAdapter.genInsertComm("Post_request", false, sql2);
                                                        dbAdapter.execNonQuery(sql2);
                                                    }
                                                }
                                                #endregion

                                            }
                                            using (SqlCommand cmdt = new SqlCommand())
                                            {
                                                cmdt.Parameters.AddWithValue("@customer_code", _customer_code);
                                                cmdt.Parameters.AddWithValue("@count", dt.Rows.Count);
                                                cmdt.CommandText = "update Customer_checknumber_setting set used_count = used_count + @count  where customer_code=@customer_code and is_active = 1";
                                                dbAdapter.execNonQuery(cmdt);
                                            }

                                            using (SqlCommand sql = new SqlCommand())
                                            {
                                                DataTable dataTable = new DataTable();
                                                sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                sql.CommandText = @"
                       declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock) group by customer_code
                       
                       select A.is_active, A.total_count - A.used_count as rest_count from Customer_checknumber_setting A With(Nolock)
                       join @newest t on A.customer_code = t.customer_code and A.update_date = t.update_date
                       where A.customer_code = @customer_code";

                                                dataTable = dbAdapter.getDataTable(sql);
                                                if (dataTable.Rows.Count > 0)
                                                {
                                                    var active = dataTable.Rows[0]["is_active"].ToString();
                                                    long remain_count;
                                                    if (active == "False")
                                                    { remain_count = 0; }
                                                    else
                                                    { remain_count = Convert.ToInt64(dataTable.Rows[0]["rest_count"].ToString()); }

                                                    if (productType == "2" || productType == "3")
                                                    {
                                                        using (SqlCommand cmd1 = new SqlCommand())
                                                        {
                                                            cmd1.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                                                            cmd1.Parameters.AddWithValue("@update_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                            cmd1.Parameters.AddWithValue("@pieces_count", -dt.Rows.Count);
                                                            cmd1.Parameters.AddWithValue("@function_flag", "D1-3");
                                                            cmd1.Parameters.AddWithValue("@adjustment_reason", "整批匯入");
                                                            cmd1.Parameters.AddWithValue("@remain_count", remain_count);
                                                            cmd1.CommandText = dbAdapter.genInsertComm("checknumber_record_for_bags_and_boxes", false, cmd1);
                                                            dbAdapter.execNonQuery(cmd1);
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }





                                //#region  更新託運單的費用欄位
                                //using (SqlCommand cmd = new SqlCommand())
                                //{
                                //    cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address from tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'");
                                //    DataTable dt = dbAdapter.getDataTable(cmd);
                                //    if (dt != null && dt.Rows.Count > 0)
                                //    {
                                //        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                //        {
                                //            SqlCommand cmd2 = new SqlCommand();
                                //            cmd2.CommandText = "usp_GetLTShipFeeByRequestId";
                                //            cmd2.CommandType = CommandType.StoredProcedure;
                                //            cmd2.Parameters.AddWithValue("@request_id", dt.Rows[i]["request_id"].ToString());
                                //            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                                //            {
                                //                if (dt2 != null && dt2.Rows.Count > 0)
                                //                {
                                //                    status_code = Convert.ToInt32(dt2.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                //                    if (status_code == 1)
                                //                    {
                                //                        supplier_fee = dt2.Rows[0]["supplier_fee"]!= DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["supplier_fee"]) : 0 ;       //配送費用
                                //                        cscetion_fee = dt2.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["cscetion_fee"]) :0 ;       //C配運價

                                //                        //回寫到託運單的費用欄位
                                //                        using (SqlCommand cmd3 = new SqlCommand())
                                //                        {
                                //                            cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                //                            cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                //                            cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                //                            remote_fee = Utility.getremote_fee(dt.Rows[i]["receive_city"].ToString() , dt.Rows[i]["receive_area"].ToString(), dt.Rows[i]["receive_address"].ToString());
                                //                            cmd3.Parameters.AddWithValue("@remote_fee", remote_fee);      //偏遠區加價
                                //                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                //                            cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                //                            try
                                //                            {
                                //                                dbAdapter.execNonQuery(cmd3);
                                //                            }
                                //                            catch (Exception ex)
                                //                            {
                                //                                string strErr = string.Empty;
                                //                                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                //                                strErr = "整批匯入-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                //                                //錯誤寫至Log
                                //                                PublicFunction _fun = new PublicFunction();
                                //                                _fun.Log(strErr, "S");
                                //                            }
                                //                        }
                                //                    }
                                //                }
                                //            }
                                //        }
                                //    }
                                //}

                                #endregion

                                #region 匯入記錄
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                    cmd.Parameters.AddWithValue("@changeTable", "tcDeliveryRequests");
                                    cmd.Parameters.AddWithValue("@fromWhere", "託運資料維護-匯入");
                                    cmd.Parameters.AddWithValue("@randomCode", randomCode);
                                    cmd.Parameters.AddWithValue("@memo", "");
                                    cmd.Parameters.AddWithValue("@successNum", successNum.ToString());
                                    cmd.Parameters.AddWithValue("@failNum", failNum.ToString());
                                    cmd.Parameters.AddWithValue("@totalNum", totalNum.ToString());
                                    cmd.Parameters.AddWithValue("@startTime", startTime);
                                    cmd.Parameters.AddWithValue("@endTime", endTime);
                                    cmd.Parameters.AddWithValue("@customer_code", customer_code);
                                    cmd.Parameters.AddWithValue("@print_date", print_date);
                                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);
                                    cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");

                                    dbAdapter.execNonQuery(cmd);
                                }
                                #endregion
                                if (dlcustomer_code.SelectedValue != "") dlcustomer_code_SelectedIndexChanged(null, null);
                                Losdtoday_customer_code();

                                string insertFilePath = "insert into RequestUploadFilePath (file_category_id,import_randomCode,file_path,create_user) " +
                                       "values (" + FileDropDownList.SelectedValue + ",'" + randomCode + "', '" + fileRename + "','" + Session["account_code"].ToString() + "');";

                                using (SqlCommand a = new SqlCommand())
                                {
                                    a.CommandText = insertFilePath;
                                    try
                                    {
                                        dbAdapter.execNonQuery(a);
                                    }
                                    catch (Exception ex)
                                    {
                                        lbMsg.Items.Add(ex.Message);
                                    }
                                }

                                readdata();
                            }
                            else
                            {
                                lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請下載最新版空白託運單並檢查欄位是否有異常資料!");
                            }

                            lbMsg.Items.Add(string.Format("{0}筆上傳成功", uploadSuccessNum));
                            lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");
                        }

                    }

                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    //System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                }
                lbMsg.Items.Add("執行完畢");
                SetToken();//別忘了最後要更新Session中的標誌
            }
        }
    }

    /// <summary>
    /// 獲得當前Session裡儲存的標誌
    /// </summary>
    /// <returns></returns>
    public string GetToken()
    {
        if (null != Session["Token"])
        {
            return Session["Token"].ToString();
        }
        else
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// 生成標誌，並儲存到Session
    /// </summary>
    private void SetToken()
    {
        Session.Add("Token", UserMd5(Session.SessionID + DateTime.Now.Ticks.ToString()));
    }

    /// <summary>
    /// 這個函式純粹是為了讓標誌稍微短點兒，一堆亂碼還特有神祕感，另外，這個UserMd5函式是網上找來的現成兒的
    /// </summary>
    /// <param name="str1"></param>
    /// <returns></returns>
    protected string UserMd5(string str1)
    {
        string cl1 = str1;
        string pwd = "";
        MD5 md5 = MD5.Create();
        // 加密後是一個位元組型別的陣列
        byte[] s = md5.ComputeHash(Encoding.Unicode.GetBytes(cl1));
        // 通過使用迴圈，將位元組型別的陣列轉換為字串，此字串 是常規字元格式化所得
        for (int i = 0; i < s.Length; i++)
        {
            // 將得到的字串使用十六進位制型別格式。格式後的字元是 小寫的字母，如果使用大寫（X）則格式後的字元是大寫字元
            pwd = pwd + s[i].ToString("X");
        }
        return pwd;
    }

    //protected void btImport_Click(object sender, EventArgs e)
    //{
    //    string ttErrStr = "";
    //    string customer_code = dlcustomer_code.SelectedValue;
    //    string supplier_code = "";
    //    string supplier_name = "";
    //    string send_contact = "";
    //    string send_city = "";
    //    string send_area = "";
    //    string send_address = "";
    //    string send_tel = "";
    //    string uniform_numbers = "";
    //    string pricing_type = "";       
    //    List<ArriveSites> List = new List<ArriveSites>();
    //    string randomCode = Guid.NewGuid().ToString().Replace("-", "");
    //    randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;
    //    DateTime? startTime;
    //    DateTime? endTime;
    //    int successNum = 0;
    //    int failNum = 0;
    //    int totalNum = 0;
    //    int supplier_fee = 0;  //配送費用
    //    int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

    //    DateTime print_date = new DateTime();
    //    DateTime supplier_date = new DateTime();
    //    DateTime default_supplier_date = new DateTime();

    //    if (!DateTime.TryParse(date.Text, out print_date)) print_date = DateTime.Now;
    //    default_supplier_date = print_date;
    //    int Week = (int)print_date.DayOfWeek;
    //    //配送日自動產生，為 D+1
    //    default_supplier_date = default_supplier_date.AddDays(1);

    //    SortedList CK_list = new SortedList();


    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //            cmd.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
    //                              " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
    //                              " where c.customer_code = '" + customer_code + "' ";
    //            using (DataTable dt = dbAdapter.getDataTable(cmd))
    //            if (dt.Rows.Count > 0)
    //            {
    //                send_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
    //                send_tel = dt.Rows[0]["telephone"].ToString();                        //電話
    //                send_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
    //                send_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
    //                send_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
    //                uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
    //                supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
    //                if (supplier_code == "001")
    //                {
    //                    supplier_name = "零擔";
    //                }
    //                else if (supplier_code == "002")
    //                {
    //                    supplier_name = "流通";
    //                }
    //                else
    //                {
    //                    supplier_name = dt.Rows[0]["supplier_name"].ToString().Trim();  // 區配
    //                }
    //                pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();        // 計價模式
    //            }
    //    }

    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.CommandText = "select post_city , post_area,  station_code   from ttArriveSitesScattered with(nolock)";
    //        using (DataTable dt = dbAdapter.getDataTable(cmd))
    //        {
    //            List = DataTableExtensions.ToList<ArriveSites>(dt).ToList();
    //        }
    //    }

    //    string check_number = "";
    //    string order_number = "";
    //    string bagno = "";
    //    string subpoena_category = "";
    //    string subpoena_code = "";
    //    string receive_contact = "";
    //    string receive_tel1 = "";
    //    string receive_tel2 = "";
    //    string receive_city = "";
    //    string receive_area = "";
    //    string receive_address = "";
    //    string pieces = "";
    //    string cbmWeight = "";
    //    string plates = "";
    //    string cbm = "";
    //    string collection_money = "";
    //    string arrive_to_pay_freight = "";
    //    string arrive_to_pay_append = "";
    //    string arrive_assign_date = "";
    //    string receipt_flag = "";
    //    string pallet_recycling_flag = "";
    //    string invoice_desc = "";
    //    string check_type = "001";        //託運類別：001一般    
    //    string product_category = "001";  //商品種類：001一般
    //    string time_period = "午";        //配送時間：早、午、晚
    //    string sub_check_number = "000";  //次貨號
    //    string area_arrive_code = "";     //到著碼
    //    string receive_by_arrive_site_flag = "0";
    //    string print_flag = "0";
    //    string _Size = "";
    //    int CbmSize = 0;
    //    string Distributor = "";

    //    lbMsg.Items.Clear();
    //    if (file01.HasFile)
    //    {
    //        string ttPath = Request.PhysicalApplicationPath + @"files\";

    //        string ttFileName = ttPath + file01.FileName;
    //        string ttExtName = System.IO.Path.GetExtension(ttFileName);


    //        if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
    //        {
    //            file01.PostedFile.SaveAs(ttFileName);
    //            HSSFWorkbook workbook = null;
    //            HSSFSheet u_sheet = null;
    //            string ttSelectSheetName = "";
    //            workbook = new HSSFWorkbook(file01.FileContent);
    //            try
    //            {

    //                u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
    //                ttSelectSheetName = workbook.GetSheetName(0);

    //                if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
    //                {
    //                    Boolean IsSheetOk = true;//工作表驗證
    //                    lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

    //                    //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
    //                    string strRow, strInsCol, strInsVal;
    //                    List<StringBuilder> sb_del_list = new List<StringBuilder>();
    //                    List<StringBuilder> sb_ins_list = new List<StringBuilder>();
    //                    StringBuilder sb_temp_del = new StringBuilder();
    //                    StringBuilder sb_temp_ins = new StringBuilder();

    //                    HSSFRow Row_title = new HSSFRow();

    //                    if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

    //                    for (int i = u_sheet.FirstRowNum + 3; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
    //                    {
    //                        HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列  
    //                        if (Row != null)
    //                        {
    //                            #region 初始化
    //                            check_number = "";
    //                            order_number = "";
    //                            bagno = "";
    //                            subpoena_category = "";
    //                            subpoena_code = "";
    //                            receive_contact = "";
    //                            receive_tel1 = "";
    //                            receive_tel2 = "";
    //                            receive_city = "";
    //                            receive_area = "";
    //                            receive_address = "";
    //                            pieces = "";
    //                            plates = "";
    //                            cbm = "";
    //                            collection_money = "";
    //                            arrive_to_pay_freight = "";
    //                            arrive_to_pay_append = "";
    //                            arrive_assign_date = "";
    //                            receipt_flag = "0";
    //                            pallet_recycling_flag = "0";
    //                            invoice_desc = "";
    //                            area_arrive_code = "";
    //                            strInsCol = "";
    //                            strInsVal = "";
    //                            sub_check_number = "000";                                
    //                            CbmSize = 0;
    //                            supplier_date = default_supplier_date;
    //                            strRow = (i + 1).ToString();


    //                            #endregion

    //                            if (Row.GetCell(2) == null || Row.GetCell(2).ToString() == "")
    //                            {
    //                                sb_ins_list.Add(sb_temp_ins);
    //                                sb_temp_ins = new StringBuilder();
    //                                break;
    //                            }
    //                            totalNum += 1;
    //                            successNum += 1;
    //                            #region 驗證&取值 IsSheetOk
    //                            for (int j = 0; j < 12; j++)
    //                            {
    //                                Boolean IsChkOk = true;
    //                                switch (j)
    //                                {
    //                                    //case 0:  //託運單號
    //                                    //    if (Row.GetCell(j) != null) check_number = Row.GetCell(j).ToString();
    //                                    //    if (String.IsNullOrEmpty(check_number))
    //                                    //    {
    //                                    //        IsChkOk =
    //                                    //        IsSheetOk = false;
    //                                    //    }

    //                                    //    break;
    //                                    case 0:  //訂單編號
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)   //如果是公式，則帶結果的值
    //                                            {
    //                                                order_number = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                order_number = Row.GetCell(j).ToString();
    //                                            }
    //                                        }
    //                                        break;
    //                                    case 1:  //袋號
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                bagno = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                bagno = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        break;
    //                                    case 2:  //收件人姓名
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                receive_contact = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                receive_contact = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        if (String.IsNullOrEmpty(receive_contact))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        break;
    //                                    case 3:  //收件人電話1
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            ICell cell = Row.GetCell(j);
    //                                            switch (cell.CellType)
    //                                            {
    //                                                case CellType.Blank: //空数据类型处理
    //                                                    receive_tel1 = "";
    //                                                    if (String.IsNullOrEmpty(receive_tel1))
    //                                                    {
    //                                                        IsChkOk =
    //                                                        IsSheetOk = false;
    //                                                    }
    //                                                    break;
    //                                                case CellType.String: //字符串类型
    //                                                    receive_tel1 = Row.GetCell(j).ToString();
    //                                                    if (String.IsNullOrEmpty(receive_tel1))
    //                                                    {
    //                                                        IsChkOk =
    //                                                        IsSheetOk = false;
    //                                                    }
    //                                                    break;
    //                                                case CellType.Numeric: //数字类型
    //                                                    receive_tel1 = Row.GetCell(j).NumericCellValue.ToString();
    //                                                    break;
    //                                                case CellType.Formula: //公式
    //                                                    receive_tel1 = Row.GetCell(j).StringCellValue;

    //                                                    break;
    //                                            }
    //                                        }


    //                                        break;
    //                                    case 4:  //收件人電話2
    //                                             //if (Row.GetCell(j) != null) receive_tel2 = Row.GetCell(j).ToString();
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            ICell cell = Row.GetCell(j);
    //                                            switch (cell.CellType)
    //                                            {
    //                                                case CellType.Blank: //空数据类型处理
    //                                                    receive_tel2 = "";
    //                                                    break;
    //                                                case CellType.String: //字符串类型
    //                                                    receive_tel2 = Row.GetCell(j).ToString();
    //                                                    break;
    //                                                case CellType.Numeric: //数字类型
    //                                                    receive_tel2 = Row.GetCell(j).NumericCellValue.ToString();
    //                                                    break;
    //                                                case CellType.Formula: //公式
    //                                                    receive_tel2 = Row.GetCell(j).StringCellValue;
    //                                                    break;
    //                                            }
    //                                        }
    //                                        break;
    //                                    //case 6:  //收件人地址(縣市)
    //                                    //    if (Row.GetCell(j) != null) receive_city = Row.GetCell(j).ToString().Trim();  //前後去空白
    //                                    //    if (String.IsNullOrEmpty(receive_city))
    //                                    //    {
    //                                    //        IsChkOk =
    //                                    //        IsSheetOk = false;
    //                                    //    }
    //                                    //    receive_city = receive_city.Replace("台", "臺");
    //                                    //    break;
    //                                    //case 7:  //收件人地址(鄉鎮區)
    //                                    //    if (Row.GetCell(j) != null) receive_area = Row.GetCell(j).ToString().Trim();  //前後去空白
    //                                    //    if (String.IsNullOrEmpty(receive_area))
    //                                    //    {
    //                                    //        IsChkOk =
    //                                    //        IsSheetOk = false;
    //                                    //    }
    //                                    //    break;
    //                                    case 5:  //收件人地址
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                receive_address = Row.GetCell(j).StringCellValue.Trim(); //前後去空白
    //                                            }
    //                                            else
    //                                            {
    //                                                receive_address = Row.GetCell(j).ToString().Trim();  //前後去空白
    //                                            }
    //                                        }

    //                                        if (String.IsNullOrEmpty(receive_address))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        else
    //                                        {
    //                                            //呼叫地址正規化SP，拆出縣市、鄉鎮區
    //                                            using (SqlCommand cmd_addr = new SqlCommand())
    //                                            {
    //                                                cmd_addr.Parameters.AddWithValue("@address", receive_address);
    //                                                cmd_addr.CommandText = "dbo.usp_AddrFormat";
    //                                                cmd_addr.CommandType = CommandType.StoredProcedure;
    //                                                try
    //                                                {
    //                                                    DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
    //                                                    if (dt_addr != null && dt_addr.Rows.Count > 0)
    //                                                    {
    //                                                        receive_city = dt_addr.Rows[0]["city"].ToString();
    //                                                        receive_area = dt_addr.Rows[0]["area"].ToString();
    //                                                    }
    //                                                }
    //                                                catch (Exception ex)
    //                                                {

    //                                                }
    //                                            }

    //                                            receive_address = receive_address.Replace("台", "臺");
    //                                            receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
    //                                            receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;
    //                                        }
    //                                        break;
    //                                    case 6:  //件數
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                pieces = Row.GetCell(j).NumericCellValue.ToString();
    //                                            }
    //                                            else
    //                                            {
    //                                                pieces = Row.GetCell(j).ToString();
    //                                            }
    //                                        }
    //                                        if (!String.IsNullOrEmpty(pieces) && !Utility.IsNumeric(pieces))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        break;
    //                                    case 7:  //重量
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                cbmWeight = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                cbmWeight = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        if (!String.IsNullOrEmpty(cbmWeight) && !Utility.IsNumeric(cbmWeight))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        break;
    //                                    case 8:  //傳票類別
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                subpoena_category = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                subpoena_category = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        if (String.IsNullOrEmpty(subpoena_category))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        else
    //                                        {
    //                                            switch (subpoena_category)
    //                                            {
    //                                                case "元付":
    //                                                    subpoena_code = "11";
    //                                                    break;
    //                                                case "到付":
    //                                                    subpoena_code = "21";
    //                                                    break;
    //                                                case "代收貨款":
    //                                                    subpoena_code = "41";
    //                                                    break;
    //                                                case "元付-到付追加":
    //                                                    subpoena_code = "25";
    //                                                    break;
    //                                                default:
    //                                                    IsChkOk =
    //                                                    IsSheetOk = false;
    //                                                    break;
    //                                            }
    //                                        }
    //                                        break;

    //                                    case 9:  //指配日期
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            ICell cell = Row.GetCell(j);
    //                                            switch (cell.CellType)
    //                                            {
    //                                                case CellType.Blank: //空数据类型处理
    //                                                    arrive_assign_date = "NULL";
    //                                                    break;
    //                                                case CellType.String: //字符串类型
    //                                                    arrive_assign_date = Row.GetCell(j).ToString();
    //                                                    if (String.IsNullOrEmpty(arrive_assign_date))
    //                                                    {
    //                                                        IsChkOk =
    //                                                        IsSheetOk = false;
    //                                                    }
    //                                                    break;
    //                                                case CellType.Numeric: //数字类型       
    //                                                    if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
    //                                                    {
    //                                                        arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
    //                                                    }
    //                                                    else
    //                                                    {
    //                                                        IsChkOk =
    //                                                        IsSheetOk = false;
    //                                                    }
    //                                                    break;
    //                                                case CellType.Formula: //公式
    //                                                    arrive_assign_date = Row.GetCell(j).StringCellValue;
    //                                                    break;
    //                                            }
    //                                            DateTime _arrive_assign_date;
    //                                            if (DateTime.TryParse(arrive_assign_date, out _arrive_assign_date))
    //                                            {
    //                                                supplier_date = _arrive_assign_date;     //如果有指配日期，則配送日帶指配日期
    //                                            }
    //                                        }
    //                                        else
    //                                        {
    //                                            arrive_assign_date = "NULL";
    //                                        }

    //                                        break;
    //                                    case 10://回單
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                receipt_flag = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                receipt_flag = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        receipt_flag = (receipt_flag == "Y") ? "1" : "0";

    //                                        break;

    //                                    case 11:   //代收貨款
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                collection_money = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                collection_money = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        if (!String.IsNullOrEmpty(collection_money) && !Utility.IsNumeric(collection_money))
    //                                        {
    //                                            IsChkOk =
    //                                            IsSheetOk = false;
    //                                        }
    //                                        else if (!String.IsNullOrEmpty(collection_money) && Utility.IsNumeric(collection_money))
    //                                        {
    //                                            if (Convert.ToInt32(collection_money) > 0)
    //                                            {
    //                                                subpoena_code = "41";  //代收貨款
    //                                            }
    //                                        }
    //                                        break;

    //                                    case 12:  //託運備註
    //                                        if (Row.GetCell(j) != null)
    //                                        {
    //                                            if (Row.GetCell(j).CellType == CellType.Formula)
    //                                            {
    //                                                invoice_desc = Row.GetCell(j).StringCellValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                invoice_desc = Row.GetCell(j).ToString();
    //                                            }
    //                                        }

    //                                        #region 依託運類別檢查數量

    //                                        if (pieces == "")
    //                                        {
    //                                            IsSheetOk = false;
    //                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!");
    //                                        }
    //                                        #endregion


    //                                        break;
    //                                }

    //                                if (!IsChkOk)
    //                                {
    //                                    lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!");
    //                                }
    //                            }



    //                            #endregion

    //                            #region 組新增
    //                            if (area_arrive_code == "")
    //                            {
    //                                ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
    //                                if (_ArriveSites != null)
    //                                {
    //                                    area_arrive_code = _ArriveSites.station_code;  //到著碼
    //                                    //Distributor = _ArriveSites.supplier_code;          //配送商
    //                                }
    //                                //int ttIdx = List.IndexOfKey(receive_city + receive_area);
    //                                //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
    //                            }
    //                            receive_address = receive_address.Replace("'", "＇");
    //                            receive_address = receive_address.Replace("--", "－");
    //                            receive_tel1 = (receive_tel1.Length > 20 ? receive_tel1.Substring(0, 20) : receive_tel1);
    //                            receive_tel2 = (receive_tel2.Length > 20 ? receive_tel2.Substring(0, 20) : receive_tel2);
    //                            receive_contact = receive_contact.Replace("\n", "");
    //                            receive_contact = (receive_contact.Length > 20 ? receive_contact.Substring(0, 20) : receive_contact);
    //                            receive_address = receive_address.Replace("\n", "");
    //                            receive_address = (receive_address.Length > 100 ? receive_address.Substring(0, 100) : receive_address);

    //                            strInsCol = "pricing_type,customer_code,check_number,order_number,bagno,check_type,subpoena_category,receive_tel1,receive_tel2,receive_contact,receive_city,receive_area,receive_address,area_arrive_code,receive_by_arrive_site_flag" +
    //                                        " ,pieces,cbmWeight,collection_money,send_contact,send_city,send_area,send_address,send_tel,product_category,invoice_desc,time_period,arrive_assign_date" +
    //                                        " ,receipt_flag,pallet_recycling_flag,cuser,cdate,uuser,udate,supplier_code,supplier_name,import_randomCode,print_date, supplier_date, print_flag, turn_board,upstairs,difficult_delivery" +
    //                                        " ,turn_board_fee,upstairs_fee,difficult_fee, add_transfer, Less_than_truckload, sub_check_number";
    //                            strInsVal = "N\'" + pricing_type + "\', " +
    //                                        "N\'" + customer_code + "\', " +
    //                                        "N\'" + check_number + "\', " +
    //                                        "N\'" + order_number + "\', " +
    //                                        "N\'" + bagno + "\', " +
    //                                        "N\'" + check_type + "\', " +
    //                                        "N\'" + subpoena_code + "\', " +
    //                                        "N\'" + receive_tel1 + "\', " +
    //                                        "N\'" + receive_tel2 + "\', " +
    //                                        "N\'" + receive_contact + "\', " +
    //                                        "N\'" + receive_city + "\', " +
    //                                        "N\'" + receive_area + "\', " +
    //                                        "N\'" + receive_address + "\', " +
    //                                        "N\'" + area_arrive_code + "\', " +
    //                                        "N\'" + receive_by_arrive_site_flag + "\', " +
    //                                        "N\'" + pieces + "\', " +
    //                                        "N\'" + cbmWeight + "\', " +
    //                                        "N\'" + collection_money + "\', " +
    //                                        "N\'" + (send_contact.Length > 20 ? send_contact.Substring(0, 20) : send_contact) + "\', " +
    //                                        "N\'" + send_city + "\', " +
    //                                        "N\'" + send_area + "\', " +
    //                                        "N\'" + send_address + "\', " +
    //                                        "N\'" + send_tel + "\', " +
    //                                        "N\'" + product_category + "\', " +
    //                                        "N\'" + invoice_desc + "\', " +
    //                                        "N\'" + time_period + "\', ";
    //                            if (arrive_assign_date == "NULL")
    //                            {
    //                                strInsVal += arrive_assign_date + ", ";
    //                            }
    //                            else
    //                            {
    //                                strInsVal += "N\'" + arrive_assign_date + "\', ";
    //                            }

    //                            strInsVal += "N\'" + receipt_flag + "\', " +
    //                                        "N\'" + pallet_recycling_flag + "\', " +
    //                                        "N\'" + Session["account_code"] + "\', " +
    //                                        "GETDATE(), " +
    //                                        "N\'" + Session["account_code"] + "\', " +
    //                                        "GETDATE(), " +
    //                                        "N\'" + supplier_code + "\', " +
    //                                        "N\'" + supplier_name + "\', " +
    //                                        "N\'" + randomCode + "\', " +
    //                                        "N\'" + print_date.ToString("yyyy/MM/dd") + "\', " +
    //                                        "N\'" + supplier_date.ToString("yyyy/MM/dd") + "\', " +
    //                                        "N\'" + print_flag + "\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'0\', " +
    //                                        "N\'" + "1" + "\', " +
    //                                        "N\'" + sub_check_number + "\' ";

    //                            string insertstr = "INSERT INTO tcDeliveryRequests (" + strInsCol + ") VALUES(" + strInsVal + "); ";

    //                            sb_temp_ins.Append(insertstr);

    //                            //每100筆組成一字串
    //                            if (i % 100 == 0 || i == u_sheet.LastRowNum)
    //                            {
    //                                sb_ins_list.Add(sb_temp_ins);
    //                                sb_temp_ins = new StringBuilder();
    //                            }

    //                            #endregion
    //                        }

    //                    }

    //                    //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
    //                    if (IsSheetOk)
    //                    {
    //                        #region 執行SQL

    //                        startTime = DateTime.Now;   //開始匯入時間
    //                        //新增
    //                        foreach (StringBuilder sb in sb_ins_list)
    //                        {
    //                            if (sb.Length > 0)
    //                            {
    //                                String strSQL = sb.ToString();
    //                                using (SqlCommand cmd = new SqlCommand())
    //                                {
    //                                    cmd.CommandText = strSQL;
    //                                    try
    //                                    {
    //                                        dbAdapter.execNonQuery(cmd);
    //                                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
    //                                    }
    //                                    catch (Exception ex)
    //                                    {
    //                                        lbMsg.Items.Add(ex.Message);
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        endTime = DateTime.Now;   //匯入結束時間

    //                        #endregion

    //                        #region  取貨號
    //                        using (SqlCommand cmd = new SqlCommand())
    //                        {
    //                            cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address, customer_code
    //                                                              From tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'");
    //                            DataTable dt = dbAdapter.getDataTable(cmd);
    //                            if (dt != null && dt.Rows.Count > 0)
    //                            {
    //                                for (int i = 0; i <= dt.Rows.Count - 1; i++)
    //                                {
    //                                    string _customer_code = dt.Rows[0]["customer_code"].ToString();
    //                                    #region 取貨號(網路託運單)
    //                                    Int64 current_number = 0;
    //                                    using (SqlCommand cmdt = new SqlCommand())
    //                                    {
    //                                        DataTable dtt;
    //                                        cmdt.Parameters.AddWithValue("@customer_code", _customer_code);
    //                                        cmdt.CommandText = " Select * from tbDeliveryNumberSetting where customer_code=@customer_code ";
    //                                        dtt = dbAdapter.getDataTable(cmdt);
    //                                        if (dtt.Rows.Count > 0)
    //                                        {
    //                                            Int64 start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
    //                                            Int64 end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
    //                                            current_number = Convert.ToInt64(dtt.Rows[0]["current_number"].ToString());
    //                                            if (current_number == 0 || current_number == end_number)
    //                                            {
    //                                                current_number = start_number;
    //                                            }
    //                                            else
    //                                            {
    //                                                current_number += 1;
    //                                            }

    //                                            using (SqlCommand cmd2 = new SqlCommand())
    //                                            {
    //                                                cmd2.Parameters.AddWithValue("@check_number", current_number);           //目前取號

    //                                                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());

    //                                                cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);         //修改
    //                                                cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
    //                                                cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
    //                                                cmd2.CommandText += " update tbDeliveryNumberSetting set current_number =@current_number where customer_code=@customer_code ";
    //                                                dbAdapter.execNonQuery(cmd2);
    //                                            }


    //                                        }

    //                                    }
    //                                    #endregion


    //                                }
    //                            }
    //                        }

    //                        #endregion

    //                        //#region  更新託運單的費用欄位
    //                        //using (SqlCommand cmd = new SqlCommand())
    //                        //{
    //                        //    cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address from tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'");
    //                        //    DataTable dt = dbAdapter.getDataTable(cmd);
    //                        //    if (dt != null && dt.Rows.Count > 0)
    //                        //    {
    //                        //        for (int i = 0; i <= dt.Rows.Count - 1; i++)
    //                        //        {
    //                        //            SqlCommand cmd2 = new SqlCommand();
    //                        //            cmd2.CommandText = "usp_GetLTShipFeeByRequestId";
    //                        //            cmd2.CommandType = CommandType.StoredProcedure;
    //                        //            cmd2.Parameters.AddWithValue("@request_id", dt.Rows[i]["request_id"].ToString());
    //                        //            using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
    //                        //            {
    //                        //                if (dt2 != null && dt2.Rows.Count > 0)
    //                        //                {
    //                        //                    status_code = Convert.ToInt32(dt2.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
    //                        //                    if (status_code == 1)
    //                        //                    {
    //                        //                        supplier_fee = dt2.Rows[0]["supplier_fee"]!= DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["supplier_fee"]) : 0 ;       //配送費用
    //                        //                        cscetion_fee = dt2.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["cscetion_fee"]) :0 ;       //C配運價

    //                        //                        //回寫到託運單的費用欄位
    //                        //                        using (SqlCommand cmd3 = new SqlCommand())
    //                        //                        {
    //                        //                            cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
    //                        //                            cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
    //                        //                            cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
    //                        //                            remote_fee = Utility.getremote_fee(dt.Rows[i]["receive_city"].ToString() , dt.Rows[i]["receive_area"].ToString(), dt.Rows[i]["receive_address"].ToString());
    //                        //                            cmd3.Parameters.AddWithValue("@remote_fee", remote_fee);      //偏遠區加價
    //                        //                            cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
    //                        //                            cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
    //                        //                            try
    //                        //                            {
    //                        //                                dbAdapter.execNonQuery(cmd3);
    //                        //                            }
    //                        //                            catch (Exception ex)
    //                        //                            {
    //                        //                                string strErr = string.Empty;
    //                        //                                if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
    //                        //                                strErr = "整批匯入-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

    //                        //                                //錯誤寫至Log
    //                        //                                PublicFunction _fun = new PublicFunction();
    //                        //                                _fun.Log(strErr, "S");
    //                        //                            }
    //                        //                        }
    //                        //                    }
    //                        //                }
    //                        //            }
    //                        //        }
    //                        //    }
    //                        //}

    //                        //#endregion

    //                        #region 匯入記錄
    //                        using (SqlCommand cmd = new SqlCommand())
    //                        {
    //                            cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
    //                            cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
    //                            cmd.Parameters.AddWithValue("@changeTable", "tcDeliveryRequests");
    //                            cmd.Parameters.AddWithValue("@fromWhere", "託運資料維護-匯入");
    //                            cmd.Parameters.AddWithValue("@randomCode", randomCode);
    //                            cmd.Parameters.AddWithValue("@memo", "");
    //                            cmd.Parameters.AddWithValue("@successNum", successNum.ToString());
    //                            cmd.Parameters.AddWithValue("@failNum", failNum.ToString());
    //                            cmd.Parameters.AddWithValue("@totalNum", totalNum.ToString());
    //                            cmd.Parameters.AddWithValue("@startTime", startTime);
    //                            cmd.Parameters.AddWithValue("@endTime", endTime);
    //                            cmd.Parameters.AddWithValue("@customer_code", customer_code);
    //                            cmd.Parameters.AddWithValue("@print_date", print_date);
    //                            cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);
    //                            cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
    //                            dbAdapter.execNonQuery(cmd);
    //                        }
    //                        #endregion 

    //                        readdata();
    //                    }
    //                    else
    //                    {
    //                        lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
    //                    }
    //                    lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

    //                }

    //                //}
    //            }
    //            catch (Exception ex)
    //            {
    //                ttErrStr = ex.Message;
    //            }
    //            finally
    //            {
    //                //System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
    //            }
    //            lbMsg.Items.Add("匯入完畢");


    //        }

    //    }
    //}


    protected void btnPirntLabel_Click(object sender, EventArgs e)
    {
        string ttErrStr = string.Empty;

        string print_requestid = "";

        if (ttErrStr == "")
        {
            if (New_List.Items.Count > 0)
            {
                for (int i = 0; i <= New_List.Items.Count - 1; i++)
                {

                    Boolean Update = false;
                    string print_date = ((HiddenField)New_List.Items[i].FindControl("hid_print_date")).Value.ToString();
                    string arrive_assign_date = ((Label)New_List.Items[i].FindControl("lbarrive_assign_date")).Text;
                    string Supplier_date = ((HiddenField)New_List.Items[i].FindControl("Hid_Supplier_date")).Value;
                    Boolean print_flag = Convert.ToBoolean(((HiddenField)New_List.Items[i].FindControl("Hid_print_flag")).Value);
                    CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                    if ((chkRow != null) && (chkRow.Checked))
                    {
                        string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                        using (SqlCommand cmd = new SqlCommand())
                        {

                            #region 發送日期
                            if (print_date == "")
                            {
                                cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日
                                print_date = DateTime.Today.ToString("yyyy/MM/dd");

                                Update = true;
                            }
                            #endregion

                            #region  配送日期
                            if (Supplier_date == "")
                            {
                                DateTime supplier_date = Convert.ToDateTime(print_date);

                                if (arrive_assign_date != "")
                                {
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                }
                                else
                                {
                                    //配送日自動產生，為 D+1
                                    supplier_date = supplier_date.AddDays(1);
                                }

                                cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
                                Update = true;
                            }
                            else
                            {
                                //1.配達，且配達區分 = 正常配交，就不可改指定日期
                                //2.指配日期不可以設定今天以前(含)的日期
                                if (arrive_assign_date != "" && arrive_assign_date != Supplier_date)
                                {
                                    DateTime supplier_date = Convert.ToDateTime(print_date);
                                    supplier_date = Convert.ToDateTime(arrive_assign_date);
                                    cmd.Parameters.AddWithValue("@supplier_date", supplier_date);          //配送日期
                                    Update = true;
                                }
                            }

                            if (print_flag == false)
                            {
                                cmd.Parameters.AddWithValue("@print_flag", "1");                           //是否列印
                                Update = true;
                            }
                            #endregion

                            if (Update == true)
                            {
                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
                                cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
                                dbAdapter.execNonQuery(cmd);
                            }
                        }
                        print_requestid += id.ToString() + ",";

                    }
                }
            }


            if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);

            NameValueCollection nvcParamters = new NameValueCollection();
            nvcParamters["ids"] = print_requestid;

            // 有勾不顯示，則showSender=false
            if (!cbShowSender.Checked)
                nvcParamters["showSender"] = "false";

            string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1";
            //string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1_post";//郵局條碼
            // 施巴只印捲筒，標籤格式自訂
            if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(dltoday_customer_code.SelectedValue)) ||
                    ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
            {
                //自訂標籤
                //捲筒
                //PrintReelLabel(print_requestid, "LTReelLabel_SB");
                url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintMutiTagPDFForSBByIds";
            }
            else
            {

                //公版標籤
                if (option.SelectedValue == "1")
                {
                    //捲筒
                    //PrintReelLabel(print_requestid);
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0";
                    //url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTReelLabel2_0_post";//郵局條碼
                }
                else
                {
                    //一式6張
                    //PrintLabel(print_requestid);
                    url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1";
                    //url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintLTA4Label2_1_post";//郵局條碼
                    nvcParamters["position"] = Position.SelectedItem.Value;
                }

            }

            string customerCode;
            if (Session["manager_type"].ToString() == "5")
                customerCode = Session["account_code"].ToString();
            else
                customerCode = customerDropDown.SelectedValue;

            string strForm = PreparePOSTForm(url, nvcParamters, customerCode);

            //Page.Controls.Add(new LiteralControl(strForm));
            Response.Clear();

            Response.Write(strForm);
            Response.End();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
            return;
        }
    }

    // <summary>
    /// 列印標籤(以板列印)
    /// </summary>
    /// <param name="print_requestid">託運單id(以","隔開)</param>
    /// <param name="format">紙張格式(A4,一式6筆)</param>
    private void PrintLabel(string print_requestid)
    {
        string ttErrStr = string.Empty;
        if (print_requestid != "")
        {
            int RowNo = 1;
            DataTable dtLT1 = new DataTable();
            DataTable dtLT2 = new DataTable();
            using (DataTable dt = getPrintDT(print_requestid))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    dtLT1 = dt.Clone();  //複製DT的結構 
                    dtLT2 = dt.Clone();  //複製DT的結構 


                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        int pieces = dt.Rows[i]["pieces"] == DBNull.Value ? 1 : Convert.ToInt32(dt.Rows[i]["pieces"]);
                        dt.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["check_numberbarcode"].ToString()));
                        for (int j = 1; j <= pieces; j++)
                        {
                            switch (RowNo % 2)
                            {
                                case 1:
                                    dtLT1.ImportRow(dt.Rows[i]);
                                    break;
                                case 0:
                                    dtLT2.ImportRow(dt.Rows[i]);
                                    break;
                            }
                            RowNo++;

                        }
                    }
                }
            }

            DataSet ds = new DataSet();

            ds.Tables.Add(dtLT1);
            ds.Tables.Add(dtLT2);


            string[] ParName = new string[0];
            string[] ParValue = new string[0];

            ShowLocalReport_PDFLT(this, @"Reports\", "LTReport2", ParName, ParValue, "ReturnsData", ds);
        }
    }

    private DataTable getPrintDT(string print_requestid)
    {
        DataTable dt = null;
        SqlCommand cmd = new SqlCommand();
        string wherestr = string.Empty;
        wherestr += dbAdapter.genWhereCommIn(ref cmd, "A.request_id", print_requestid.Split(','));


        //cmd.CommandText = string.Format(@"
        //                        select  REPLACE (SUBSTRING(convert(nvarchar(10),A.supplier_date,120), 6, 10),'-','') supplier_date  ,
        //                        A.print_date, 
        //                        A.check_number check_numberbarcode,
        //                        A.check_number,
        //                        A.arrive_assign_date,
        //                        A.receive_contact , 
        //                        A.receive_tel1, 
        //                        A.receive_city+A.receive_area + A.receive_address  receiveAddress,
        //                        case when A.subpoena_category ='11' then '元付'
        //                          when A.subpoena_category ='21' then '到付' 
        //                          when A.subpoena_category ='41' then '代收貨款' 
        //                          when A.subpoena_category ='25' then '到付追加' 
        //                          end subpoena_category, 
        //                        CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
        //                        A.invoice_memo, 
        //                        A.invoice_desc,
        //                        A.customer_code , 
        //                        C.customer_shortname ,
        //                        A.send_contact,
        //                        A.send_tel,
        //                        A.send_city+ A.send_area + A.send_address  sendAddress,
        //                        (select station_code from ttArriveSitesScattered where post_city = A.receive_city and post_area = A.receive_area) station_scode,
        //                        (select station_name from tbStation where station_scode in (select station_code from ttArriveSitesScattered where post_city = A.receive_city and post_area = A.receive_area)) station_name,
        //                        A.pieces,
        //                        A.ArticleNumber,
        //                        A.SendPlatform,
        //                        A.ArticleName,
        //                        convert(nvarchar(10),A.cdate,120) cdate,
        //                        A.receive_tel2,
        //                        A.cbmWeight,
        //                        A.collection_money,
        //                        A.order_number,
        //                        A.time_period
        //                        from tcDeliveryRequests A WITH(Nolock)
        //                        left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
        //                        left join tbCustomers C on C.customer_code = A.customer_code
        //                        where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate())
        //                        {0}
        //order by  A.check_number", wherestr);

        cmd.CommandText = string.Format(@"select  REPLACE (SUBSTRING(convert(nvarchar(10),A.supplier_date,120), 6, 10),'-','') supplier_date  ,
                                 CONVERT(varchar(100), A.print_date, 111) 'print_date', 
                                  A.check_number  check_numberbarcode,
                                  A.check_number  check_number_SB, A.check_number  check_numberbarcode_SB,
                                A.check_number,
                                A.arrive_assign_date,
                                A.receive_contact , 
                                A.receive_tel1, A.receive_tel2, 
                                A.receive_city+A.receive_area + A.receive_address  receiveAddress,
                                case when A.subpoena_category ='11' then '元付'
	                                 when A.subpoena_category ='21' then '到付' 
	                                 when A.subpoena_category ='41' then '代收貨款' 
	                                 when A.subpoena_category ='25' then '到付追加' 
	                                 end subpoena_category, 
                                CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
                                A.collection_money,
                                A.ReportFee,
                                A.ProductValue,
                                A.time_period,
                                A.invoice_memo, 
                                A.invoice_desc,
                                A.customer_code , 
                                C.customer_shortname ,
                                A.send_contact,
                                A.send_tel,
                                A.send_city+ A.send_area + A.send_address  sendAddress,
                                A.round_trip,
                                D.station_scode, D.station_name,
                                IsNULL(C.delivery_Type,0) delivery_Type,
                                A.pieces,A.ArticleNumber, A.SendPlatform, A.ArticleName,A.cbmWeight, A.order_number
                                from tcDeliveryRequests A WITH(Nolock)
                                left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                left join tbCustomers C on C.customer_code = A.customer_code
								left join tbStation D on D.station_scode = A.area_arrive_code
                                where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate())
                                {0}
								order by  A.check_number", wherestr);


        dt = dbAdapter.getDataTable(cmd);

        return dt;
    }

    /// <summary>
    /// 列印標籤(捲筒列印)(依計價模式為列印張數單位)
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReelLabel(string print_requestid, string label_type = "LTReelLabel")
    {
        //rdlc 公版:"LTReelLabel"
        if (print_requestid != "")
        {
            using (DataTable dt = getPrintDT(print_requestid))
            {
                if (dt != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = dt.Clone();  //複製DT的結構

                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        int pieces = dt.Rows[i]["pieces"] == DBNull.Value ? 1 : Convert.ToInt32(dt.Rows[i]["pieces"]);
                        dt.Rows[i]["check_numberbarcode"] = Convert.ToBase64String(MakeBarcodeImage(dt.Rows[i]["check_numberbarcode"].ToString()));
                        string check_number = dt.Rows[i]["check_number"].ToString();

                        for (int j = 1; j <= pieces; j++)
                        {
                            string check_number2 = check_number + j.ToString().PadLeft(3, '0');
                            dt.Rows[i]["check_number_SB"] = check_number2;
                            dt.Rows[i]["check_numberbarcode_SB"] = Convert.ToBase64String(MakeBarcodeImage(check_number2, false));
                            dt2.ImportRow(dt.Rows[i]);

                        }
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];
                    //PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "LTReelLabel", ParName, ParValue, "TL1", dt2);
                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", label_type, ParName, ParValue, "TL1", dt2);
                }
            }
        }
    }

    public void ShowLocalReport_PDFLT(Page pg, string ReportPath, string ReportName, string[] ParsName, string[] ParsValue, string reportSourceName, DataSet ds)
    {
        Microsoft.Reporting.WebForms.ReportViewer rv = new Microsoft.Reporting.WebForms.ReportViewer();

        rv.LocalReport.ReportPath = string.Format(@"{0}\{1}.rdlc", ReportPath, ReportName);
        Microsoft.Reporting.WebForms.ReportParameter[] parameters = new Microsoft.Reporting.WebForms.ReportParameter[ParsName.Length];
        for (int i = 0; i < ParsName.Length; i++)
        {
            parameters[i] = new Microsoft.Reporting.WebForms.ReportParameter(ParsName[i].ToString(), ParsValue[i].ToString());
        }
        rv.LocalReport.SetParameters(parameters);
        rv.LocalReport.DataSources.Clear();
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL1", ds.Tables[0]));
        rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL2", ds.Tables[1]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL3", ds.Tables[2]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL4", ds.Tables[3]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL5", ds.Tables[4]));
        //rv.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TL6", ds.Tables[5]));


        Microsoft.Reporting.WebForms.Warning[] tWarnings;
        string[] tStreamids;
        string tMimeType;
        string tEncoding;
        string tExtension;

        //呼叫ReportViewer.LoadReport的Render function，將資料轉成想要轉換的格式，並產生成Byte資料  

        byte[] tBytes = rv.LocalReport.Render("PDF", null, out tMimeType, out tEncoding, out tExtension, out tStreamids, out tWarnings);
        string ss = HttpUtility.UrlEncode(ReportName + ".pdf", System.Text.Encoding.UTF8);
        //將Byte內容寫到Client  

        pg.Response.Clear();
        pg.Response.ContentType = tMimeType;
        pg.Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", ss));


        pg.Response.BinaryWrite(tBytes);
        pg.Response.End();

        String ED = DateTime.Today.ToString("yyyy/MM/dd HH:mm:ss");
    }

    public byte[] MakeBarcodeImage(string datastring, bool printnumber = true)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring, printnumber);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content, bool printnumber)
    {

        using (var barcode = new Barcode()
        {

            IncludeLabel = printnumber,
            Alignment = AlignmentPositions.CENTER,
            Width = 800,
            Height = 100,
            LabelFont = new Font("verdana", 20f),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
            ImageFormat = System.Drawing.Imaging.ImageFormat.Jpeg,//图片格式

        })
        {
            return barcode.Encode(TYPE.CODE39, content);
        }
    }

    public bool IsValidCheckNumber(string check_number)
    {
        bool IsVaild = true;
        if (check_number.Length != 10)
        {
            IsVaild = false;
        }
        else
        {
            int num = Convert.ToInt32(check_number.Substring(0, 9));

            int chk = num % 7;
            int lastnum = Convert.ToInt32(check_number.Substring(9, 1));
            if (lastnum != chk)
            {
                IsVaild = false;
            }

        }
        return IsVaild;
    }

    protected void btPrint_Click(object sender, EventArgs e)
    {
        string check_number = string.Empty;
        string print_requestid = "";
        if (New_List.Items.Count > 0)
        {
            for (int i = 0; i <= New_List.Items.Count - 1; i++)
            {
                Boolean Update = false;
                string print_date = ((HiddenField)New_List.Items[i].FindControl("hid_print_date")).Value.ToString();
                string arrive_assign_date = ((Label)New_List.Items[i].FindControl("lbarrive_assign_date")).Text;
                string Supplier_date = ((HiddenField)New_List.Items[i].FindControl("Hid_Supplier_date")).Value;
                string request_id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));

                if ((chkRow != null) && (chkRow.Checked))
                {
                    check_number += "'" + lbcheck_number.Text + "',";
                    print_requestid += request_id.ToString() + ",";
                }

            }
        }
        if (check_number != "") check_number = check_number.Substring(0, check_number.Length - 1);
        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);
        NameValueCollection nvcParamters = new NameValueCollection();
        nvcParamters["ids"] = print_requestid;

        if (check_number == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('請勾選要列印的託運單');</script>", false);
            return;
        }
        else
        {
            if (ExcelFile.Checked == true)
            {
                if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(dltoday_customer_code.SelectedValue)) ||
                ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
                {
                    DBToExcel(check_number, "SB");
                }
                else
                {
                    DBToExcel(check_number);
                }
            }
            else if (PDFFile.Checked == true)
            {

                string url = "http://erpground.fs-express.com.tw/DeliveryRequest/PrintConsignmentSummaryPDFByIds";

                string strForm = PreparePrintDeliverySummaryCommitForm(url, nvcParamters);

                //Page.Controls.Add(new LiteralControl(strForm));
                Response.Clear();

                Response.Write(strForm);
                Response.End();
            }


        }

    }

    protected void DBToExcel(string check_number, string type = "")
    {

        String str_retuenMsg = string.Empty;
        try
        {
            Boolean IsChkOk = false;
            if (check_number.Length > 0) IsChkOk = true;

            #region 撈DB寫入EXCEL
            if (IsChkOk)
            {
                string sheet_title = "FSE託運總表";
                string file_name = "FSE託運總表" + DateTime.Now.ToString("yyyyMMdd");
                int totalcount = 0;
                int totalpieces = 0;


                //從網址下載Html字串
                //Context.Items["TextData"] = check_number + ";" + date.Text + ";" + date.Text + ";";
                //Server.Transfer("Default3.aspx", true);

                using (SqlCommand cmd = new SqlCommand())
                {
                    if (check_number != "")
                    {
                        switch (type)
                        {
                            case "SB":
                                string wherestr = " and check_number in(" + check_number + ")";
                                string orderby = " order by  A.check_number";

                                cmd.CommandText = string.Format(@"Select
        CONVERT(varchar(100), A.print_date, 112)   '發送日期' , 
		'' '客戶代號',
        A.check_number '查貨號碼' , 
		A.order_number '清單編號',
		A.receive_customer_code '收貨人代號',
        A.receive_contact '收貨人名稱',  
		A.receive_tel1 '收貨人電話1',
		A.receive_tel2 '收貨人電話2',
        A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '收貨人地址',
	    A.receive_zip '郵遞區號',
        E.station_name  '到著站',
		ISNULL(A.pieces,0) '件數',
		ISNULL(A.cbmWeight,0) '重量',
		A.invoice_desc '備註'  ,
		A.collection_money '代收貨款',
        A.ReportFee '報值金額',
        A.receive_tel1 '收件人電話',  
        A.subpoena_category '傳票區分',
		CONVERT(varchar(100), A.arrive_assign_date, 112)   '指定日期',
		A.time_period '指定時間',
		A.customer_code '供貨人代號',
		A.send_contact  '供貨人名稱',
		A.send_tel '供貨人電話',
		A.send_city  + A.send_area  + A.send_address  '供貨人地址',
		A.check_number+ '001' '件號'
        from tcDeliveryRequests A with(nolock)
        left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
        left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
        Left join tbSuppliers D with(nolock) on A.area_arrive_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
        Left join tbStation E with(nolock) on A.area_arrive_code = E.station_scode
        where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                                using (DataTable dt = dbAdapter.getDataTable(cmd))
                                {
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        DataTable dt2 = new DataTable();
                                        dt2 = dt.Clone();  //複製DT的結構

                                        for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                        {
                                            int pieces = dt.Rows[i]["件數"] == DBNull.Value ? 1 : Convert.ToInt32(dt.Rows[i]["件數"]);
                                            for (int j = 1; j <= pieces; j++)
                                            {
                                                dt.Rows[i]["件號"] = dt.Rows[i]["查貨號碼"].ToString() + j.ToString().PadLeft(3, '0');
                                                dt2.ImportRow(dt.Rows[i]);
                                            }
                                        }

                                        //int cnt = dt.Rows.Count;
                                        //for (int i = cnt - 1; i >=0 ; i--)
                                        //{
                                        //    int pieces = Convert.ToInt32(dt.Rows[i]["件數"]);
                                        //    if (pieces > 1)
                                        //    {  
                                        //        for (int j = 2; j <= pieces ; j++)
                                        //        {
                                        //            DataRow row = dt.NewRow(); 
                                        //            //row=dt.Rows[i];
                                        //            row.ItemArray = (object[])dt.Rows[i].ItemArray.Clone();
                                        //            row["件號"] = row["查貨號碼"].ToString() + j.ToString().PadLeft(3, '0');
                                        //            dt.Rows.Add(row);
                                        //        }
                                        //    }
                                        //    //totalpieces = totalpieces + Convert.ToInt32(dt.Rows[i]["件數"].ToString());
                                        //}
                                        //DataView dv = dt.DefaultView;
                                        //dv.Sort = "件號";

                                        //DataView dv = dt.AsDataView();
                                        //dv.Sort = "件號 ASC";

                                        //dt.DefaultView.Sort = "件號";
                                        int rowIndex = 0;
                                        ISheet sheet = null;
                                        totalcount = dt.Rows.Count;
                                        MemoryStream ms = new MemoryStream();
                                        //HSSFWorkbook workbook = new HSSFWorkbook(); // .xls
                                        IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）

                                        IRow headerRow = null;

                                        if (string.IsNullOrEmpty(sheet_title))
                                        {
                                            //沒有Table Name以預設方式命名Sheet
                                            sheet = workbook.CreateSheet();
                                        }
                                        else
                                        {
                                            //有Table Name以Table Name命名Sheet
                                            sheet = workbook.CreateSheet(sheet_title);
                                        }

                                        ICellStyle style = workbook.CreateCellStyle();

                                        headerRow = sheet.CreateRow(rowIndex++);

                                        //自動換行
                                        //csHeader.WrapText = true;

                                        #region 處理標題列
                                        foreach (DataColumn column in dt2.Columns)
                                        {
                                            headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                                        }
                                        #endregion

                                        #region 處理資料列
                                        foreach (DataRow row in dt2.Rows)
                                        //foreach(DataRow row in dv.Table.Rows)
                                        {
                                            IRow dataRow = sheet.CreateRow(rowIndex++);

                                            foreach (DataColumn column in dt2.Columns)
                                            {
                                                dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));
                                                ICellStyle styleA = workbook.CreateCellStyle();
                                            }
                                        }
                                        IRow dataRowA = sheet.CreateRow(rowIndex + 1);



                                        //dataRowA.CreateCell(1).SetCellValue(Convert.ToString("總計"));
                                        //dataRowA.CreateCell(2).SetCellValue(Convert.ToString("共" + totalcount + "筆"));
                                        //dataRowA.CreateCell(3).SetCellValue(Convert.ToString("共" + totalpieces + "件"));


                                        sheet.SetColumnWidth(0, 15 * 256);
                                        sheet.SetColumnWidth(1, 15 * 256);
                                        sheet.SetColumnWidth(2, 15 * 256);
                                        sheet.SetColumnWidth(3, 20 * 256);
                                        sheet.SetColumnWidth(4, 15 * 256);
                                        sheet.SetColumnWidth(5, 15 * 256);
                                        sheet.SetColumnWidth(6, 15 * 256);
                                        sheet.SetColumnWidth(7, 15 * 256);
                                        sheet.SetColumnWidth(8, 60 * 256);
                                        sheet.SetColumnWidth(9, 15 * 256);
                                        sheet.SetColumnWidth(10, 15 * 256);
                                        sheet.SetColumnWidth(13, 30 * 256);
                                        sheet.SetColumnWidth(15, 15 * 256);
                                        sheet.SetColumnWidth(19, 15 * 256);
                                        sheet.SetColumnWidth(20, 15 * 256);
                                        sheet.SetColumnWidth(21, 15 * 256);
                                        sheet.SetColumnWidth(22, 60 * 256);
                                        sheet.SetColumnWidth(23, 25 * 256);

                                        #endregion

                                        workbook.Write(ms);
                                        Response.Clear();
                                        //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx", file_name));
                                        Response.BinaryWrite(ms.ToArray());
                                        Response.Flush();
                                        Response.End();
                                        ms.Close();
                                        ms.Dispose();
                                    }
                                    else
                                    {
                                        str_retuenMsg += "查無此資訊，請重新確認!";
                                    }

                                    //lbMsg.
                                }

                                break;
                            default:
                                // cmd.Parameters.AddWithValue("@check_number", print_chknum);
                                wherestr = " and check_number in(" + check_number + ")";
                                orderby = " order by  A.check_number";

                                cmd.CommandText = string.Format(@"Select
                                                          ROW_NUMBER() OVER(ORDER BY A.check_number) '序號',
                                                          A.order_number '訂單編號' , 
                                                          A.check_number '貨號' , 
                                                          A.receive_contact '收件人',  
                                                          ISNULL(A.pieces,0) '件數',
                                                          A.receive_city + A.receive_area +  CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end '地址',
                                                          E.station_name  '到著站',
                                                          A.receive_tel1 '收件人電話',  
                                                          A.collection_money '代收金額',
                                                          B.code_name '付款別',
                                                          A.SendPlatform '平臺名稱',
                                                          A.ArticleName '商品名稱',
                                                          A.invoice_desc '備註'  
                                                          from tcDeliveryRequests A with(nolock)
                                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and B.code_sclass = 'S2'
                                                          left join tbItemCodes C on C.code_id = A.pricing_type and C.code_sclass = 'PM'
                                                          Left join tbSuppliers D with(nolock) on A.area_arrive_code = D.supplier_code and ISNULL(A.area_arrive_code,'') <> '' 
                                                          Left join tbStation E with(nolock) on A.area_arrive_code = E.station_scode
                                                          where 1= 1 AND A.cancel_date IS NULL and  A.cdate  >=  DATEADD(MONTH,-6,getdate()) {0} {1}", wherestr, orderby);
                                using (DataTable dt = dbAdapter.getDataTable(cmd))
                                {
                                    if (dt != null && dt.Rows.Count > 0)
                                    {

                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {

                                            totalpieces = totalpieces + Convert.ToInt32(dt.Rows[i]["件數"].ToString());
                                        }
                                        int rowIndex = 0;
                                        ISheet sheet = null;
                                        totalcount = dt.Rows.Count;
                                        MemoryStream ms = new MemoryStream();
                                        //HSSFWorkbook workbook = new HSSFWorkbook(); // .xls
                                        IWorkbook workbook = new XSSFWorkbook();   //-- XSSF 用來產生Excel 2007檔案（.xlsx）

                                        IRow headerRow = null;

                                        if (string.IsNullOrEmpty(sheet_title))
                                        {
                                            //沒有Table Name以預設方式命名Sheet
                                            sheet = workbook.CreateSheet();
                                        }
                                        else
                                        {
                                            //有Table Name以Table Name命名Sheet
                                            sheet = workbook.CreateSheet(sheet_title);
                                        }

                                        ICellStyle style = workbook.CreateCellStyle();
                                        //style.Alignment = HorizontalAlignment.Center;

                                        var font1 = workbook.CreateFont();
                                        font1.Boldweight = 30;
                                        font1.FontHeightInPoints = 24;
                                        style.SetFont(font1);

                                        IRow titletop = sheet.CreateRow(rowIndex++);
                                        ICell cell = sheet.CreateRow(0).CreateCell(0);
                                        cell.SetCellValue("FSE託運總表");
                                        cell.CellStyle = style;

                                        IRow dataRowDate = sheet.CreateRow(rowIndex++);
                                        dataRowDate.CreateCell(0).SetCellValue("發送日期");
                                        dataRowDate.CreateCell(1).SetCellValue(DateTime.Now.ToString("yyyy-MM-dd"));




                                        if (supplier_code != "")
                                        {
                                            using (SqlCommand cmda = new SqlCommand())
                                            {
                                                DataTable dta = new DataTable();
                                                cmda.Parameters.AddWithValue("@supplier_code", supplier_code);
                                                cmda.CommandText = @" WITH cus AS(
                                                        SELECT *,
                                                        ROW_NUMBER() OVER(PARTITION BY master_code ORDER BY master_code DESC) AS rn
                                                        FROM tbCustomers with(nolock) where 1 = 1 and supplier_code <> '001' and stop_shipping_code = '0' and supplier_code=@supplier_code
                                                        )
                                                        SELECT customer_name
                                                        FROM cus
                                                        WHERE rn = 1";

                                                dta = dbAdapter.getDataTable(cmda);
                                                if (dta.Rows.Count > 0)
                                                {
                                                    IRow dataRowCus = sheet.CreateRow(rowIndex++);
                                                    dataRowCus.CreateCell(0).SetCellValue("寄件人");
                                                    dataRowCus.CreateCell(1).SetCellValue(dta.Rows[0]["customer_name"].ToString());
                                                }
                                            }
                                        }



                                        rowIndex++;




                                        headerRow = sheet.CreateRow(rowIndex++);

                                        //自動換行
                                        //csHeader.WrapText = true;

                                        #region 處理標題列
                                        foreach (DataColumn column in dt.Columns)
                                        {
                                            headerRow.CreateCell(column.Ordinal).SetCellValue(column.ColumnName);
                                        }
                                        #endregion

                                        #region 處理資料列
                                        foreach (DataRow row in dt.Rows)
                                        {
                                            IRow dataRow = sheet.CreateRow(rowIndex++);

                                            foreach (DataColumn column in dt.Columns)
                                            {


                                                dataRow.CreateCell(column.Ordinal).SetCellValue(Convert.ToString(row[column]));
                                                ICellStyle styleA = workbook.CreateCellStyle();


                                            }
                                        }
                                        IRow dataRowA = sheet.CreateRow(rowIndex + 1);



                                        dataRowA.CreateCell(1).SetCellValue(Convert.ToString("總計"));
                                        dataRowA.CreateCell(2).SetCellValue(Convert.ToString("共" + totalcount + "筆"));
                                        dataRowA.CreateCell(3).SetCellValue(Convert.ToString("共" + totalpieces + "件"));


                                        sheet.SetColumnWidth(0, 15 * 256);//序號寬度
                                        sheet.SetColumnWidth(1, 30 * 256);//訂單編號寬度
                                        sheet.SetColumnWidth(2, 30 * 256);//貨號寬度
                                        sheet.SetColumnWidth(3, 30 * 256);//收件人寬度
                                        sheet.SetColumnWidth(4, 10 * 256);//件數寬度
                                        sheet.SetColumnWidth(5, 100 * 256);//地址寬度
                                        sheet.SetColumnWidth(6, 25 * 256);//到著站寬度
                                        sheet.SetColumnWidth(7, 30 * 256);//收件人電話寬度
                                        sheet.SetColumnWidth(8, 10 * 256);//代收金額寬度
                                        sheet.SetColumnWidth(9, 10 * 256);//付款別寬度
                                        sheet.SetColumnWidth(10, 30 * 256);//平臺名稱寬度
                                        sheet.SetColumnWidth(11, 30 * 256);//商品名稱寬度
                                        sheet.SetColumnWidth(12, 60 * 256);//備註寬度


                                        #endregion

                                        workbook.Write(ms);
                                        Response.Clear();
                                        //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}.xlsx", file_name));
                                        Response.BinaryWrite(ms.ToArray());
                                        Response.Flush();
                                        Response.End();
                                        ms.Close();
                                        ms.Dispose();
                                    }
                                    else
                                    {
                                        str_retuenMsg += "查無此資訊，請重新確認!";
                                    }

                                    //lbMsg.
                                }
                                break;

                        }
                    }
                }

            }
            #endregion

        }
        catch (Exception ex)
        {
            str_retuenMsg += "系統異常，請洽相關人員!";
            str_retuenMsg += " (" + ex.Message.ToString() + ")";
        }
        finally
        {
            lbMsg.Items.Add(str_retuenMsg);

        }

    }


    /// <summary>
    /// 去掉危險字元的回傳字串
    /// </summary>
    /// <param name="theValue">The value.</param>
    /// <param name="theLevel">The level.</param>
    /// <returns>去掉危險字元的回傳字串</returns>
    public static string NewString(System.Object theValue, System.Object theLevel)
    {
        // Written by user CWA, CoolWebAwards.com Forums. 2 February 2010
        // http://forum.coolwebawards.com/threads/12-Preventing-SQL-injection-attacks-using-C-NET

        // intLevel represent how thorough the value will be checked for dangerous code
        // intLevel (1) - Do just the basic. This level will already counter most of the SQL injection attacks
        // intLevel (2) -   (non breaking space) will be added to most words used in SQL queries to prevent unauthorized access to the database. Safe to be printed back into HTML code. Don't use for usernames or passwords

        string strValue = (string)theValue;
        int intLevel = (int)theLevel;

        if (strValue != null)
        {
            strValue = strValue.Trim();//去空白
            if (intLevel > 0)
            {
                strValue = strValue.Replace("'", "''"); // Most important one! This line alone can prevent most injection attacks
                strValue = strValue.Replace("--", "");
                strValue = strValue.Replace("[", "[[]");
                strValue = strValue.Replace("%", "[%]");
            }
            if (intLevel > 1)
            {
                string[] myArray = new string[] { "xp_ ", "update ", "insert ", "select ", "drop ", "alter ", "create ", "rename ", "delete ", "replace " };
                int i = 0;
                int i2 = 0;
                int intLenghtLeft = 0;
                for (i = 0; i < myArray.Length; i++)
                {
                    string strWord = myArray[i];
                    Regex rx = new Regex(strWord, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    MatchCollection matches = rx.Matches(strValue);
                    i2 = 0;
                    foreach (Match match in matches)
                    {
                        GroupCollection groups = match.Groups;
                        intLenghtLeft = groups[0].Index + myArray[i].Length + i2;
                        strValue = strValue.Substring(0, intLenghtLeft - 1) + "&nbsp;" + strValue.Substring(strValue.Length - (strValue.Length - intLenghtLeft), strValue.Length - intLenghtLeft);
                        i2 += 5;
                    }
                }
            }
            return strValue;
        }
        else
        {
            return strValue;
        }
    }


    protected void dlcustomer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        LiRemainCnt.Text = "";
        Int64 cnt = 0;
        string showtext;

        // 取得客代商品類型
        string customerCode = dlcustomer_code.SelectedValue;
        int productType = GetCustomerType(customerCode);

        if (productType == 2)
            showtext = "預購袋剩餘：";
        else if (productType == 3)
            showtext = @"超值箱\袋剩餘：";
        else
            showtext = "";

        if (dlcustomer_code.SelectedValue != "")
        {
            if (showtext == "預購袋剩餘：" || showtext == @"超值箱\袋剩餘：")
            {
                using (SqlCommand sql = new SqlCommand())
                {
                    DataTable dataTable = new DataTable();
                    sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
                    sql.CommandText = @"
                       declare @newest table
                       (
                        customer_code nvarchar(20),
                        update_date datetime
                       )
                       insert into @newest
                       select customer_code, MAX(update_date) from Customer_checknumber_setting A With(Nolock) group by customer_code
                       
                       select A.is_active, A.total_count - A.used_count as rest_count from Customer_checknumber_setting A With(Nolock)
                       join @newest t on A.customer_code = t.customer_code and A.update_date = t.update_date
                       where A.customer_code = @customer_code";

                    dataTable = dbAdapter.getDataTable(sql);
                    if (dataTable.Rows.Count > 0)
                    {
                        var active = dataTable.Rows[0]["is_active"].ToString();

                        if (active == "False")
                            cnt = 0;
                        else if (active == "True")
                            cnt = Convert.ToInt64(dataTable.Rows[0]["rest_count"].ToString());

                        LiRemainCnt.Text = showtext + cnt.ToString();
                        HiddenCount.Value = cnt.ToString();
                    }
                    else
                    {
                        HiddenCount.Value = "99999";
                    }
                }
            }
        }
        //using (SqlCommand cmda = new SqlCommand())
        //{
        //    DataTable dta = new DataTable();
        //    cmda.CommandText = "Select end_number,ISNULL(current_number,0) 'current_number' ,begin_number from tbDeliveryNumberOnceSetting With(Nolock) where customer_code=@customer_code and  IsActive=1 order by end_number ";
        //    cmda.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
        //    dta = dbAdapter.getDataTable(cmda);
        //    if (dta != null && dta.Rows.Count > 0) //算預購袋、超值箱/袋剩餘數量
        //    {
        //        for (int k = 0; k <= dta.Rows.Count - 1; k++)
        //        {
        //            if (dta.Rows[k]["current_number"].ToString() == "0")
        //            {
        //                cnt += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
        //            }
        //            else if (Convert.ToInt64(dta.Rows[k]["current_number"]) < Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()))
        //            {
        //                cnt += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
        //            }
        //            else
        //            {
        //                cnt += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["current_number"].ToString());
        //            }
        //        }
        //        LiRemainCnt.Text = showtext + cnt.ToString();
        //        HiddenCount.Value = cnt.ToString();
        //    }
        //    else
        //    {
        //        HiddenCount.Value = "99999";
        //    }
        //}

        //else
        //{    //算一般件剩餘數量
        //    using (SqlCommand sql = new SqlCommand())
        //    {
        //        DataTable dataTable = new DataTable();
        //        sql.CommandText = "Select end_number,ISNULL(current_number,0) 'current_number' ,begin_number from tbDeliveryNumberSetting With(Nolock) where customer_code=@customer_code and  IsActive=1 order by end_number ";
        //        sql.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue);
        //        dataTable = dbAdapter.getDataTable(sql);

        //        if (dataTable != null && dataTable.Rows.Count > 0)
        //        {
        //            for (int r = 0; r < dataTable.Rows.Count; r++)
        //            {
        //                if (dataTable.Rows[r]["current_number"].ToString() == "0")
        //                {
        //                    cnt += Convert.ToInt64(dataTable.Rows[r]["end_number"].ToString()) - Convert.ToInt64(dataTable.Rows[r]["begin_number"].ToString()) + 1;
        //                }
        //                else if (Convert.ToInt64(dataTable.Rows[r]["current_number"]) < Convert.ToInt64(dataTable.Rows[r]["begin_number"].ToString()))
        //                {
        //                    cnt += Convert.ToInt64(dataTable.Rows[r]["end_number"].ToString()) - Convert.ToInt64(dataTable.Rows[r]["begin_number"].ToString()) + 1;
        //                }
        //                else
        //                {
        //                    cnt += Convert.ToInt64(dataTable.Rows[r]["end_number"].ToString()) - Convert.ToInt64(dataTable.Rows[r]["current_number"].ToString());
        //                }
        //            }
        //            LiRemainCnt.Text = showtext + cnt.ToString();
        //            HiddenCount.Value = cnt.ToString();
        //        }
        //        else
        //        {
        //            HiddenCount.Value = "99999";
        //        }
        //    }
        //}


        if (((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(dlcustomer_code.SelectedValue)) ||
                    ((new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" }).Contains(Session["account_code"].ToString())))
        {
            Label2.Visible = false;
            dlcustomer_code.Visible = false;
            btImport.Visible = false;
            btImport_SB.Visible = true;
        }
        else
        {
            Label2.Visible = true;
            dlcustomer_code.Visible = true;
            btImport.Visible = true;
            btImport_SB.Visible = false;
        }
    }

    private void GetFileDropDownList()
    {
        using (SqlCommand c = new SqlCommand())
        {
            c.CommandText = "select * from UploadFileCategory ";
            FileDropDownList.DataSource = dbAdapter.getDataTable(c);
            FileDropDownList.DataValueField = "file_category_id";
            FileDropDownList.DataTextField = "file_name";
            FileDropDownList.DataBind();
        }
    }

    private void Losdtoday_customer_code()
    {
        if (supplier_code == "")
        {
            //為防止管理者一次載入當日匯入資料太多，多一個當日有匯入資料的客代，讓他可以選
            using (SqlCommand cmd3 = new SqlCommand())
            {
                cmd3.Parameters.AddWithValue("@scdate", DateTime.Today.ToString("yyyy/MM/dd"));
                cmd3.Parameters.AddWithValue("@ecdate", DateTime.Today.AddDays(1).ToString("yyyy/MM/dd"));
                cmd3.Parameters.AddWithValue("@type", "1");
                cmd3.CommandText = string.Format(@"Select A.customer_code, A.customer_code + '-' + A.customer_shortname as name
                                                       from tbCustomers A with(Nolock)
                                                       where 0=0 and A.stop_shipping_code = '0' and A.type =@type  
                                                       and customer_code in (select distinct customer_code  from tcDeliveryRequests with(nolock) where cdate >= @scdate and cdate<  @ecdate
                                                       and import_randomCode <> '' and Less_than_truckload = 1 )
                                                       group by A.customer_code,A.customer_shortname  order by customer_code asc ");
                dltoday_customer_code.DataSource = dbAdapter.getDataTable(cmd3);
                dltoday_customer_code.DataValueField = "customer_code";
                dltoday_customer_code.DataTextField = "name";
                dltoday_customer_code.DataBind();
            }
            dltoday_customer_code.Visible = dltoday_customer_code.Items.Count > 0;
        }
        if (dltoday_customer_code.Items.Count > 0 && dlcustomer_code.SelectedValue != "")
        {
            dltoday_customer_code.SelectedValue = dlcustomer_code.SelectedValue;
        }

    }

    protected void dltoday_customer_code_SelectedIndexChanged(object sender, EventArgs e)
    {
        readdata();
    }


    protected void lbRefresh_Click(object sender, EventArgs e)
    {
        readdata();

    }

    protected void lbPrintLabel_Click(object sender, EventArgs e)
    {
        PrintReelLabel(hid_p.Value, "LTReelLabel_SB");
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        #region 託運類別
        SqlCommand cmd4 = new SqlCommand();
        cmd4.CommandText = " select code_id, code_name,  code_name as showname from tbItemCodes where code_sclass = 'PM'  order by code_id asc";
        rb_pricing_type.DataSource = dbAdapter.getDataTable(cmd4);
        rb_pricing_type.DataValueField = "code_id";
        rb_pricing_type.DataTextField = "showname";
        rb_pricing_type.DataBind();
        rb_pricing_type.Items.Insert(0, new ListItem("全部", ""));
        rb_pricing_type.SelectedValue = "";
        #endregion

        #region 託運類別
        SqlCommand cmd2 = new SqlCommand();
        cmd2.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S1' and active_flag = 1 ";
        check_type.DataSource = dbAdapter.getDataTable(cmd2);
        check_type.DataValueField = "code_id";
        check_type.DataTextField = "code_name";
        check_type.DataBind();
        #endregion

        #region 傳票類別
        SqlCommand cmd3 = new SqlCommand();
        cmd3.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S2' and active_flag = 1 order by code_id ";
        subpoena_category.DataSource = dbAdapter.getDataTable(cmd3);
        subpoena_category.DataValueField = "code_id";
        subpoena_category.DataTextField = "code_name";
        subpoena_category.DataBind();
        #endregion

        #region 商品類別
        //SqlCommand cmd4 = new SqlCommand();
        //cmd4.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S3' and active_flag = 1 ";
        //product_category.DataSource = dbAdapter.getDataTable(cmd4);
        //product_category.DataValueField = "code_id";
        //product_category.DataTextField = "code_name";
        //product_category.DataBind();
        #endregion

        #region 配送時段
        SqlCommand cmd6 = new SqlCommand();
        cmd6.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S5' and active_flag = 1 ";
        time_period.DataSource = dbAdapter.getDataTable(cmd6);
        time_period.DataValueField = "code_id";
        time_period.DataTextField = "code_name";
        time_period.DataBind();
        time_period.Items.Insert(2, new ListItem("晚", "晚"));
        time_period.SelectedValue = "不指定";
        #endregion

        #region 配送溫層
        using (SqlCommand cmd10 = new SqlCommand())
        {
            cmd10.CommandText = "select code_id, code_name from tbItemCodes where code_bclass = '2' and code_sclass = 'S9' and active_flag = 1 ";
            dltemp.DataSource = dbAdapter.getDataTable(cmd10);
            dltemp.DataValueField = "code_id";
            dltemp.DataTextField = "code_name";
            dltemp.DataBind();
        }
        #endregion

        #region 材積大小
        using (SqlCommand cmd8 = new SqlCommand())
        {
            cmd8.CommandText = "select id, CbmID, CbmSize from tcCbmSize with(nolock) where id != '5' " +
                    "order by (CASE  CbmSize WHEN 'S90' THEN '01'  WHEN '袋裝' THEN '99'  ELSE '80' END), DefaultCbmCont";
            dlCbmSize.DataSource = dbAdapter.getDataTable(cmd8);
            dlCbmSize.DataValueField = "CbmID";
            dlCbmSize.DataTextField = "CbmSize";
            dlCbmSize.DataBind();
        }
        #endregion

        #region 郵政縣市
        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        receive_city.DataSource = dbAdapter.getDataTable(cmd1);
        receive_city.DataValueField = "city";
        receive_city.DataTextField = "city";
        receive_city.DataBind();
        receive_city.Items.Insert(0, new ListItem("請選擇", ""));

        send_city.DataSource = dbAdapter.getDataTable(cmd1);
        send_city.DataValueField = "city";
        send_city.DataTextField = "city";
        send_city.DataBind();
        send_city.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion
        ScriptManager.RegisterStartupScript(this, this.GetType(), "retVal", "$('#modal-default_01').modal();", true);
    }



    private void filldata()
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@check_number", check_number.Text);
            cmd.CommandText = @" SELECT A.*, B.customer_name FROM tcDeliveryRequests A With(Nolock) 
                                    LEFT JOIN tbCustomers B with(nolock) on A.customer_code = B.customer_code
                                    WHERE A.check_number = @check_number ";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                int i_tmp = 0;
                DateTime dt_tmp = new DateTime();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DeliveryType = dt.Rows[0]["DeliveryType"] != DBNull.Value ? dt.Rows[0]["DeliveryType"].ToString() : "";
                    dlDeliveryType.SelectedValue = DeliveryType;
                    switch (DeliveryType)
                    {
                        case "D":
                            lbDeliveryType.Text = "出貨";
                            break;
                        case "R":
                            lbDeliveryType.Text = "回收";
                            dlDeliveryType.ForeColor = System.Drawing.Color.Red;
                            break;
                    }

                    //rbcheck_type_SelectedIndexChanged(null, null);
                    if (!DateTime.TryParse(dt.Rows[0]["print_date"].ToString(), out dt_tmp)) Shipments_date.Text = ""; //出貨日期
                    else Shipments_date.Text = dt_tmp.ToString("yyyy/MM/dd");
                    if (Convert.ToBoolean(dt.Rows[0]["print_flag"]) == true)
                    {
                        Shipments_date.Enabled = false;
                    }
                    lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();          //客代編號
                    lbcustomer_name.Text = dt.Rows[0]["customer_name"].ToString();
                    customer_code.Text = dt.Rows[0]["customer_code"].ToString();

                    //顯示寄件人資料
                    using (SqlCommand cmda = new SqlCommand())
                    {
                        DataTable dta;
                        cmda.CommandText = @"select C.individual_fee from tbCustomers C With(Nolock) 
                                                where customer_code = '" + customer_code.Text + "' ";
                        dta = dbAdapter.getDataTable(cmda);
                        if (dta.Rows.Count > 0)
                        {
                            bool individualfee = dta.Rows[0]["individual_fee"] != DBNull.Value ? Convert.ToBoolean(dta.Rows[0]["individual_fee"]) : false;   //是否個別計價
                                                                                                                                                             //if (individualfee && (!(rbcheck_type.SelectedValue == "05" && _noadmin)))   //開放個別計價欄位(但如果專車，則只開放峻富key in)
                            if (individualfee)
                            {
                                individual_fee.Style.Add("display", "block");
                                if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                            }
                            else
                            {
                                individual_fee.Style.Add("display", "none");
                                if (UpdatePanel5.UpdateMode == UpdatePanelUpdateMode.Conditional) UpdatePanel5.Update();
                            }
                        }
                    }


                    #region 1. 基本設定
                    hid_id.Value = dt.Rows[0]["request_id"].ToString();
                    lbcheck_number.Text = dt.Rows[0]["check_number"].ToString();                     //託運單號(貨號) 現階段長度10~13碼   前9碼MOD7取餘數為檢查碼
                    order_number.Text = dt.Rows[0]["order_number"].ToString();                       //訂單號碼
                    check_type.SelectedValue = dt.Rows[0]["check_type"].ToString().Trim();                  //託運類別
                    receive_customer_code.Text = dt.Rows[0]["receive_customer_code"].ToString();     //收貨人編號
                    subpoena_category.SelectedValue = dt.Rows[0]["subpoena_category"].ToString();    //傳票類別
                    #endregion

                    #region 3.才件代收
                    if (!String.IsNullOrEmpty(dt.Rows[0]["pieces"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["pieces"].ToString())) //件數
                    {
                        pieces.Text = dt.Rows[0]["pieces"].ToString();
                    }

                    if (!String.IsNullOrEmpty(dt.Rows[0]["plates"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["plates"].ToString())) //板數
                    {
                        plates.Text = dt.Rows[0]["plates"].ToString();
                    }

                    if (!String.IsNullOrEmpty(dt.Rows[0]["cbm"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["cbm"].ToString())) //才數
                    {
                        cbm.Text = dt.Rows[0]["cbm"].ToString();
                    }

                    try
                    {
                        dlCbmSize.SelectedValue = dt.Rows[0]["CbmSize"].ToString();          //材積大小
                    }
                    catch (Exception ex) { }


                    if (!String.IsNullOrEmpty(dt.Rows[0]["collection_money"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["collection_money"].ToString())) //$代收金
                    {
                        collection_money.Text = dt.Rows[0]["collection_money"].ToString();
                    }

                    if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_freight"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["arrive_to_pay_freight"].ToString())) //到付運費
                    {
                        arrive_to_pay_freight.Text = dt.Rows[0]["arrive_to_pay_freight"].ToString();
                    }


                    if (!String.IsNullOrEmpty(dt.Rows[0]["arrive_to_pay_append"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["arrive_to_pay_append"].ToString())) //到付追加
                    {
                        arrive_to_pay_append.Text = dt.Rows[0]["arrive_to_pay_append"].ToString();
                    }
                    #endregion

                    #region 特殊設定
                    try
                    {
                        //product_category.SelectedValue = dt.Rows[0]["product_category"].ToString();   //商品種類
                        //arrive_mobile.Text = dt.Rows[0]["arrive_mobile"].ToString();                  //收件人-手機1 
                        //special_send.SelectedValue = dt.Rows[0]["special_send"].ToString();             //特殊配送                       

                        ArticleNumber.Text = dt.Rows[0]["ArticleNumber"].ToString();  //產品編號
                        SendPlatform.Text = dt.Rows[0]["SendPlatform"].ToString();    //出貨平台
                        ArticleName.Text = dt.Rows[0]["ArticleName"].ToString();      //品名
                        bagno.Text = dt.Rows[0]["bagno"].ToString();  //代號
                                                                      //指定日
                        if (!DateTime.TryParse(dt.Rows[0]["arrive_assign_date"].ToString(), out dt_tmp)) arrive_assign_date.Text = "";
                        else arrive_assign_date.Text = dt_tmp.ToString("yyyy/MM/dd");

                        time_period.SelectedValue = dt.Rows[0]["time_period"].ToString();              //時段
                        invoice_desc.Value = dt.Rows[0]["invoice_desc"].ToString();                    //說明    

                        //if (!int.TryParse(dt.Rows[0]["receipt_flag"].ToString(), out i_tmp)) i_tmp = 0;
                        //receipt_flag.Checked = i_tmp == 1 ? true : false; //是否回單

                        receipt_round_trip.Checked = dt.Rows[0]["round_trip"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["round_trip"]) : false;        //是否來回件
                        receipt_flag.Checked = dt.Rows[0]["receipt_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["receipt_flag"]) : false;        //是否回單
                        pallet_recycling_flag.Checked = dt.Rows[0]["pallet_recycling_flag"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["pallet_recycling_flag"]) : false;        //是否棧板回收
                        if (pallet_recycling_flag.Checked)
                        {
                            Pallet_type_div.Attributes.Add("style", "display:block");
                            Pallet_type_text.ReadOnly = true;
                            Pallet_type_text.Text = Func.GetRow("code_name", "tbItemCodes", "", " code_bclass = '2' and code_sclass = 'S8' and code_id=@code_id", "code_id", dt.Rows[0]["Pallet_type"].ToString());
                            Pallet_type.Value = dt.Rows[0]["Pallet_type"].ToString();
                        }
                        turn_board.Checked = dt.Rows[0]["turn_board"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["turn_board"]) : false;                        //翻版
                        upstairs.Checked = dt.Rows[0]["upstairs"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["upstairs"]) : false;                             //上樓
                        difficult_delivery.Checked = dt.Rows[0]["difficult_delivery"] != DBNull.Value ? Convert.ToBoolean(dt.Rows[0]["difficult_delivery"]) : false;//困配
                        turn_board_fee.Text = turn_board.Checked ? dt.Rows[0]["turn_board_fee"].ToString() : "";                        //翻版
                        upstairs_fee.Text = upstairs.Checked ? dt.Rows[0]["upstairs_fee"].ToString() : "";                            //上樓
                        difficult_fee.Text = difficult_delivery.Checked ? dt.Rows[0]["difficult_fee"].ToString() : "";                 //困配
                        turn_board_fee.Enabled = turn_board.Checked;
                        upstairs_fee.Enabled = upstairs.Checked;
                        difficult_fee.Enabled = difficult_delivery.Checked;
                    }
                    catch (Exception ex) { }

                    #endregion


                    /*新託運單:寄件框=原收件人框
                      收件框全清空
                      sub_check_number = "001"
                      唯讀→ 託運類別、 基本設定、才件代收、特殊設定
                      其餘可改
                     */


                    #region 託運單修改、瀏覽

                    //lbl_sub_check_number.Text = "000";

                    #region 2. 收件人
                    try
                    {
                        receive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();                       //電話1
                        receive_tel1_ext.Text = dt.Rows[0]["receive_tel1_ext"].ToString();               //電話分機
                        receive_tel2.Text = dt.Rows[0]["receive_tel2"].ToString();                       //電話2
                        receive_contact.Text = dt.Rows[0]["receive_contact"].ToString();                 //收件人
                        receive_city.SelectedValue = dt.Rows[0]["receive_city"].ToString();              //收件地址-縣市     
                        city_SelectedIndexChanged(receive_city, null);
                        receive_area.SelectedValue = dt.Rows[0]["receive_area"].ToString();              //收件地址-鄉鎮市區  
                                                                                                         //receive_area_SelectedIndexChanged(null, null);



                        //到站領貨 0:否、1:是
                        if (!int.TryParse(dt.Rows[0]["receive_by_arrive_site_flag"].ToString(), out i_tmp)) i_tmp = 0;
                        cbarrive.Checked = i_tmp == 1 ? true : false;

                        if (cbarrive.Checked)
                        {
                            receive_address.Text = dt.Rows[0]["arrive_address"].ToString();                  //到著站址
                        }
                        else
                        {
                            receive_address.Text = dt.Rows[0]["receive_address"].ToString();                 //收件地址-路街巷弄號
                        }
                    }
                    catch (Exception ex) { }
                    #endregion

                    #region 4.寄件人
                    try
                    {
                        send_contact.Text = dt.Rows[0]["send_contact"].ToString();                     //寄件人
                        send_tel.Text = dt.Rows[0]["send_tel"].ToString();                             //寄件人電話
                        send_city.SelectedValue = dt.Rows[0]["send_city"].ToString();                  //寄件人地址-縣市
                        city_SelectedIndexChanged(send_city, null);
                        send_area.SelectedValue = dt.Rows[0]["send_area"].ToString();                  //寄件人地址-鄉鎮市區
                        send_address.Text = dt.Rows[0]["send_address"].ToString();                     // 寄件人地址 - 號街巷弄號
                    }
                    catch (Exception ex) { }
                    #endregion

                    //#region 到著站簡碼
                    //area_arrive_code.DataSource = Utility.getArea_Arrive_Code(true);
                    //area_arrive_code.DataValueField = "station_scode";
                    //area_arrive_code.DataTextField = "showsname";
                    //area_arrive_code.DataBind();
                    //area_arrive_code.Items.Insert(0, new ListItem("請選擇到著站", ""));
                    //#endregion

                    //area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();        //到著碼
                    //if (area_arrive_code.SelectedValue == "" && dt.Rows[0]["area_arrive_code"].ToString() != "")
                    //{
                    //    area_arrive_code.Items.Add(new ListItem(dt.Rows[0]["area_arrive_code"].ToString(), dt.Rows[0]["area_arrive_code"].ToString()));
                    //    area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();      //到著碼
                    //}

                    //area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();        //到著碼
                    //if (area_arrive_code.SelectedValue == "" && dt.Rows[0]["area_arrive_code"].ToString() != "")
                    //{
                    //    area_arrive_code.Items.Add(new ListItem(dt.Rows[0]["area_arrive_code"].ToString(), dt.Rows[0]["area_arrive_code"].ToString()));
                    //    area_arrive_code.SelectedValue = dt.Rows[0]["area_arrive_code"].ToString();      //到著碼
                    //}

                    if (!String.IsNullOrEmpty(dt.Rows[0]["supplier_fee"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["supplier_fee"].ToString())) //貨件費用
                    {
                        i_supplier_fee.Text = dt.Rows[0]["supplier_fee"].ToString();
                    }

                    if (!String.IsNullOrEmpty(dt.Rows[0]["csection_fee"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["csection_fee"].ToString())) //C配運費
                    {
                        i_csection_fee.Text = dt.Rows[0]["csection_fee"].ToString();
                    }

                    if (!String.IsNullOrEmpty(dt.Rows[0]["remote_fee"].ToString()) || !Utility.IsNumeric(dt.Rows[0]["remote_fee"].ToString()))     //偏遠區加價
                    {
                        i_remote_fee.Text = dt.Rows[0]["remote_fee"].ToString();
                    }

                    //ReadOnlySet(true);

                    #endregion

                    #region mark session
                    //arrive_email.Text = dt.Rows[0]["arrive_email"].ToString();                    //收件人-電子郵件
                    //invoice_memo.SelectedValue = dt.Rows[0]["invoice_memo"].ToString();           //備註
                    //donate_invoice_flag.Checked = Convert.ToBoolean(dt.Rows[0]["donate_invoice_flag"]);              //是否發票捐贈 
                    //electronic_invoice_flag.Checked  = Convert.ToBoolean(dt.Rows[0]["electronic_invoice_flag"]);     //是否為電子發票
                    //uniform_numbers.Text = dt.Rows[0]["uniform_numbers"].ToString();              //統一編號
                    #endregion


                }

            }
        }
    }


    protected void btnQry_Click(object sender, EventArgs e)
    {

        filldata();
    }


    protected void city_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        if (dlcity != null)
        {
            switch (dlcity.ID)
            {
                case "receive_city":   // 收件人
                    dlarea = receive_area;
                    break;
                case "send_city":      // 寄件人
                    dlarea = send_area;
                    break;
            }
        }

        dlarea.Items.Clear();
        dlarea.ClearSelection();
        dlarea.Items.Add(new ListItem("請選擇", ""));

        if (dlcity.SelectedValue != "")
        {
            SqlCommand cmda = new SqlCommand();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select area from tbPostCityArea where city=@city order by seq asc";
            using (DataTable dta = dbAdapter.getDataTable(cmda))
            {
                //dlarea.DataSource = dta;
                //dlarea.DataValueField = "area";
                //dlarea.DataTextField = "area";
                //dlarea.DataBind();
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
    }




    protected void btnsave_Click(object sender, EventArgs e)
    {
        string strErr = string.Empty;
        int ttpieces = 0;
        if (Func.IsNormalDelivery(check_number.Text))
        {
            strErr = "此筆已正常配達，無法進行修改";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存失敗:" + strErr + "');</script>", false);
            return;
        }

        if (Func.IsCancelled(check_number.Text))
        {
            strErr = "此筆已經銷單，無法進行修改";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存失敗:" + strErr + "');</script>", false);
            return;
        }
        if (!int.TryParse(pieces.Text, out ttpieces)) ttpieces = 0;
        if (string.IsNullOrWhiteSpace(pieces.Text))
        {
            lbErrQuant.Text = "請輸入件數";
            pieces.Focus();
            lbErrQuant.Visible = true;
            return;
        }
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@pieces", ttpieces);                                             //件數
            cmd.Parameters.AddWithValue("@uuser", Session["account_code"]);                               //更新人員
            cmd.Parameters.AddWithValue("@udate", DateTime.Now);                                          //更新時間 
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", hid_id.Value);
            cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);   //修改
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                strErr = ex.ToString();
            }
        }


        if (strErr == "")
        {
            //捲筒
            //PrintReelLabel(hid_id.Value, "LTReelLabel_SB");
            string wherestr = "";
            SqlCommand cmd = new SqlCommand();

            cmd.Parameters.AddWithValue("@request_id", hid_id.Value);
            wherestr += " AND A.request_id =@request_id ";


            cmd.CommandText = string.Format(@"Select A.request_id, A.cdate, A.arrive_assign_date , A.supplier_date, A.print_date,A.print_flag, A.check_number ,A.receive_contact , 
                                          ROW_NUMBER() OVER(order by   A.request_id Asc,A.cdate desc , A.check_number desc) AS ROWID,
                                          A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area , A.receive_address,
                                          A.pieces , A.plates , A.cbm ,
                                          B.code_name , A.customer_code , C.customer_shortname ,
                                          A.send_city , A.send_area , A.send_address ,
                                          A.arrive_to_pay_freight,A.collection_money,A.ReportFee,ProductValue,A.arrive_to_pay_append,A.invoice_desc,
                                          a.cancel_date,
                                          A.holiday_delivery, A.order_number, case A.holiday_delivery when  '1' then '假日配送' else '一般配送' end 'holiday_delivery_chinese'
                                          from tcDeliveryRequests A with(nolock)
                                          left join tbItemCodes B with(nolock) on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C with(nolock)  on C.customer_code = A.customer_code
                                          where A.cancel_date is null and A.Less_than_truckload = 1 and A.DeliveryType = 'D' {0} 
                                          order by  A.request_id Asc, A.cdate desc , A.check_number desc", wherestr);   //and A.check_number =''
            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();


            ltotalpages.Text = dt.Rows.Count.ToString();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存成功');$('#modal-default_01').modal('hide');</script>", false);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('儲存失敗:" + strErr + "');</script>", false);
            return;
        }

    }

    protected void btImport_SB_Click(object sender, EventArgs e)
    {
        string ttErrStr = "";
        string customer_code = ""; // dlcustomer_code.SelectedValue;
        string supplier_code = "";
        string supplier_name = "";
        string send_contact = "";
        string send_city = "";
        string send_area = "";
        string send_address = "";
        string send_tel = "";
        string uniform_numbers = "";
        string pricing_type = "";
        List<ArriveSites> List = new List<ArriveSites>();
        string randomCode = Guid.NewGuid().ToString().Replace("-", "");
        randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;
        DateTime? startTime;
        DateTime? endTime;
        int successNum = 0;
        int failNum = 0;
        int totalNum = 0;
        int supplier_fee = 0;  //配送費用
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)

        DateTime print_date = new DateTime();
        DateTime supplier_date = new DateTime();
        DateTime default_supplier_date = new DateTime();

        if (!DateTime.TryParse(date.Text, out print_date)) print_date = DateTime.Now;
        default_supplier_date = print_date;
        int Week = (int)print_date.DayOfWeek;
        default_supplier_date = default_supplier_date.AddDays(1);  //配送日自動產生，為 D+1

        SortedList CK_list = new SortedList();
        string[] customerlist = new string[] { "F2900280002", "F8900030002", "F8900030102", "F8900030202", "F8900030302" };

        //#region 客代資料
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
        //                      " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
        //                      " where c.customer_code = '" + customer_code + "' ";
        //    using (DataTable dt = dbAdapter.getDataTable(cmd))
        //        if (dt.Rows.Count > 0)
        //        {
        //            send_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
        //            send_tel = dt.Rows[0]["telephone"].ToString();                        //電話
        //            send_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
        //            send_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
        //            send_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
        //            uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
        //            supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
        //            supplier_name = dt.Rows[0]["supplier_name"].ToString().Trim();        // 區配名稱
        //            pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();          // 計價模式
        //        }
        //}
        //#endregion

        #region 到著碼
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select post_city , post_area,  station_code from ttArriveSitesScattered with(nolock)";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                List = DataTableExtensions.ToList<ArriveSites>(dt).ToList();
            }
        }
        #endregion

        //#region 檢查客代是否有建貨號區間
        //int chk = 0;
        //bool IsHaveNum = false;

        //using (SqlCommand cmdt = new SqlCommand())
        //{
        //    DataTable dtt;
        //    cmdt.Parameters.AddWithValue("@customer_code", customer_code);
        //    cmdt.CommandText = " Select * from tbDeliveryNumberSetting where customer_code=@customer_code AND IsActive = 1";
        //    dtt = dbAdapter.getDataTable(cmdt);
        //    if (dtt.Rows.Count > 0)
        //    {
        //        IsHaveNum = true;
        //        chk = 1;
        //    }
        //}
        ////施巴沒有預購袋
        ////if (!IsHaveNum)
        ////{
        ////    using (SqlCommand cmdt = new SqlCommand())
        ////    {
        ////        DataTable dtt;
        ////        cmdt.Parameters.AddWithValue("@customer_code", customer_code);
        ////        cmdt.CommandText = " Select top 1 * from tbDeliveryNumberOnceSetting where customer_code=@customer_code AND IsActive = 1 order by end_number ";
        ////        dtt = dbAdapter.getDataTable(cmdt);
        ////        if (dtt.Rows.Count > 0)
        ////        {

        ////            IsHaveNum = true;
        ////            chk = 2;

        ////        }
        ////    }
        ////}
        //#endregion


        string check_number = "";
        string order_number = "";
        string CbmSize = "";
        string subpoena_category = "";
        string subpoena_code = "";
        string receive_contact = "";
        string receive_tel1 = "";
        string receive_tel2 = "";
        string receive_city = "";
        string receive_area = "";
        string receive_address = "";
        string pieces = "";
        string cbmWeight = "";
        string plates = "";
        string cbm = "";
        string collection_money = "";
        string ReportFee = "";
        string ProductValue = "";
        string arrive_to_pay_freight = "";
        string arrive_to_pay_append = "";
        string arrive_assign_date = "";

        string _supplier_date = "";

        string receipt_flag = "";
        string pallet_recycling_flag = "";
        string invoice_desc = "";
        string check_type = "001";        //託運類別：001一般    
        string product_category = "001";  //商品種類：001一般
        string time_period = "午";        //配送時間：早、午、晚
        string sub_check_number = "000";  //次貨號
        string area_arrive_code = "";     //到著碼
        string ShuttleStationCode = "";     //接駁碼
        string receive_by_arrive_site_flag = "0";
        string print_flag = "0";
        //string _Size = "";

        //string Distributor = "";
        string ArticleNumber = "";          //產品編號     
        string SendPlatform = "";           //出貨平台
        string ArticleName = "";            //品名
        string round_trip = "";             //來回件
        string payment_method = "";


        lbMsg.Items.Clear();
        if (file01.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            string ttFileName = ttPath + file01.FileName;
            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            IWorkbook workbook = null;
            ISheet u_sheet = null;
            IFormulaEvaluator formulaEvaluator = null;
            string ttSelectSheetName = "";
            if ((ttExtName.ToLower() == ".xls") || (ttExtName.ToLower() == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                if (ttExtName.ToLower() == ".xls")
                {
                    workbook = new HSSFWorkbook(file01.FileContent);
                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new HSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到

                }
                else if (ttExtName.ToLower() == ".xlsx")
                {
                    workbook = new XSSFWorkbook(file01.FileContent);
                    u_sheet = (XSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);
                    formulaEvaluator = new XSSFFormulaEvaluator(workbook); // Important!! 取公式值的時候會用到
                }

                u_sheet.ForceFormulaRecalculation = true; //要求公式重算結果 

                try
                {
                    if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                    {
                        Boolean IsSheetOk = true;//工作表驗證
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                        //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                        string strRow, strInsCol, strInsVal;
                        List<StringBuilder> sb_del_list = new List<StringBuilder>();
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_del = new StringBuilder();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        IRow Row_title = null;
                        if (u_sheet.LastRowNum > 0) Row_title = u_sheet.GetRow(2);

                        //int Row_count = 0;
                        //Int64 LeftCount = 0;

                        //if (chk == 2)
                        //{
                        //    using (SqlCommand cmda = new SqlCommand())
                        //    {

                        //        DataTable dta = new DataTable();
                        //        cmda.CommandText = "Select  end_number,ISNULL(current_number,0) 'current_number' ,begin_number from tbDeliveryNumberOnceSetting With(Nolock) where customer_code=@customer_code and  IsActive=1 order by end_number ";
                        //        cmda.Parameters.AddWithValue("@customer_code", customer_code);
                        //        dta = dbAdapter.getDataTable(cmda);
                        //        if (dta != null && dta.Rows.Count > 0)
                        //        {
                        //            for (int k = 0; k <= dta.Rows.Count - 1; k++)
                        //            {
                        //                if (dta.Rows[k]["current_number"].ToString() == "0")
                        //                {
                        //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
                        //                }
                        //                else if (Convert.ToInt64(dta.Rows[k]["current_number"]) < Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()))
                        //                {
                        //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["begin_number"].ToString()) + 1;
                        //                }
                        //                else
                        //                {
                        //                    LeftCount += Convert.ToInt64(dta.Rows[k]["end_number"].ToString()) - Convert.ToInt64(dta.Rows[k]["current_number"].ToString());
                        //                }
                        //            }
                        //        }
                        //    }
                        //}



                        for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                        {
                            IRow Row = u_sheet.GetRow(i);  //取得目前的資料列  
                            if (Row != null)
                            {
                                #region 初始化
                                check_number = "";
                                order_number = "";
                                CbmSize = "";
                                subpoena_category = "";
                                subpoena_code = "";
                                receive_contact = "";
                                receive_tel1 = "";
                                receive_tel2 = "";
                                receive_city = "";
                                receive_area = "";
                                receive_address = "";
                                pieces = "";
                                plates = "";
                                cbm = "";
                                collection_money = "";
                                ReportFee = "";
                                ProductValue = "";
                                arrive_to_pay_freight = "";
                                arrive_to_pay_append = "";
                                arrive_assign_date = "";
                                _supplier_date = "";
                                receipt_flag = "0";
                                pallet_recycling_flag = "0";
                                invoice_desc = "";
                                area_arrive_code = "";
                                ShuttleStationCode = "";
                                SpecialAreaId = "";
                                SpecialAreaFee = "";
                                SendCode = "";
                                SendSD = "";
                                SendMD = "";
                                ReceiveCode = "";
                                ReceiveSD = "";
                                ReceiveMD = "";
                                strInsCol = "";
                                strInsVal = "";
                                sub_check_number = "000";
                                supplier_date = default_supplier_date;
                                strRow = (i + 1).ToString();
                                ArticleNumber = "";          //產品編號     
                                SendPlatform = "";           //出貨平台
                                ArticleName = "";            //品名
                                round_trip = "0";             //來回件
                                customer_code = "";
                                send_contact = "";
                                send_tel = "";
                                send_city = "";
                                send_area = "";
                                send_address = "";
                                uniform_numbers = "";
                                supplier_code = "";
                                supplier_name = "";
                                pricing_type = "";
                                payment_method = "";

                                #endregion

                                if (Row.GetCell(2) == null || Row.GetCell(2).ToString() == "")
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                    break;
                                }
                                totalNum += 1;
                                successNum += 1;
                                #region 驗證&取值 IsSheetOk
                                for (int j = 0; j <= 20; j++)
                                {
                                    Boolean IsChkOk = true;
                                    ICell cell = Row.GetCell(j);
                                    switch (j)
                                    {
                                        case 0:
                                            //施巴不會自已key貨號
                                            //#region 託運單號
                                            //if (chk == 1)  //一般件
                                            //{
                                            //    if (cell != null)
                                            //    {

                                            //        if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                            //        {
                                            //            try
                                            //            {
                                            //                var formulaValue = formulaEvaluator.Evaluate(cell);
                                            //                if (formulaValue.CellType == CellType.String) check_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                            //                else if (formulaValue.CellType == CellType.Numeric) check_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                            //            }
                                            //            catch
                                            //            {
                                            //                check_number = cell.StringCellValue;
                                            //            }
                                            //        }
                                            //        else
                                            //        {
                                            //            check_number = Row.GetCell(j).ToString();
                                            //        }

                                            //        if (String.IsNullOrEmpty(check_number) && IsHaveNum == false)
                                            //        {
                                            //            IsSheetOk = false;
                                            //            lbMsg.Items.Add("第" + strRow + "列：" + dlcustomer_code.SelectedItem.Text + "沒有設定貨號區間或預購袋號碼【託運單號】不允許空白");
                                            //        }
                                            //        else if (!String.IsNullOrEmpty(check_number))
                                            //        {

                                            //            //檢查貨號一個月內是否使用
                                            //            bool IsUse = false;
                                            //            using (SqlCommand cmdt = new SqlCommand())
                                            //            {
                                            //                DataTable dtt;
                                            //                cmdt.Parameters.AddWithValue("@customer_code", customer_code);
                                            //                cmdt.Parameters.AddWithValue("@check_number", check_number);
                                            //                cmdt.CommandText = @" Select * from tcDeliveryRequests with(nolock) where customer_code=@customer_code 
                                            //                and check_number =@check_number and print_date >=  DATEADD(MONTH,-1,getdate())";
                                            //                dtt = dbAdapter.getDataTable(cmdt);
                                            //                if (dtt.Rows.Count > 0)
                                            //                {
                                            //                    IsUse = true;
                                            //                    IsSheetOk = false;
                                            //                    lbMsg.Items.Add("第" + strRow + "列：" + check_number + "託運單號已被使用");
                                            //                }
                                            //            }



                                            //            if (IsUse == false)
                                            //            {
                                            //                using (SqlCommand cmdt = new SqlCommand())
                                            //                {
                                            //                    DataTable dtt;
                                            //                    cmdt.Parameters.AddWithValue("@customer_code", customer_code);
                                            //                    cmdt.Parameters.AddWithValue("@check_number", check_number);
                                            //                    cmdt.CommandText = @" Select current_number,seq,customer_code  from tbDeliveryNumberSetting where customer_code=@customer_code and @check_number between  begin_number and  end_number AND (IsActive = 1)";
                                            //                    dtt = dbAdapter.getDataTable(cmdt);
                                            //                    if (dtt.Rows.Count == 0)
                                            //                    {
                                            //                        IsSheetOk = false;
                                            //                        lbMsg.Items.Add("第" + strRow + "列：" + check_number + "不屬於" + dlcustomer_code.SelectedItem.Text + "的貨號區間");
                                            //                    }
                                            //                    else
                                            //                    {
                                            //                        if (Convert.ToInt64(check_number) > Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()))
                                            //                        {


                                            //                            using (SqlCommand cmd = new SqlCommand())
                                            //                            {

                                            //                                cmd.Parameters.AddWithValue("@current_number", Convert.ToInt64(check_number));
                                            //                                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_code", dtt.Rows[0]["customer_code"].ToString());
                                            //                                cmd.CommandText = dbAdapter.genUpdateComm("tbDeliveryNumberSetting", cmd);
                                            //                                dbAdapter.execNonQuery(cmd);
                                            //                            }
                                            //                        }
                                            //                    }
                                            //                }

                                            //            }
                                            //        }
                                            //    }
                                            //}
                                            //else  //預購袋
                                            //{
                                            //    check_number = "";   //預購袋只能系統取號
                                            //}

                                            //#endregion
                                            break;
                                        case 1:
                                            #region 訂單編號
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) order_number = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) order_number = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        order_number = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    order_number = Row.GetCell(j).ToString();
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 2:
                                            #region 收件人姓名
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) receive_contact = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) receive_contact = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態

                                                    }
                                                    catch
                                                    {
                                                        receive_contact = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    receive_contact = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (String.IsNullOrEmpty(receive_contact))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 3:
                                            #region 收件人電話1
                                            if (cell != null)
                                            {
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        receive_tel1 = "";
                                                        if (String.IsNullOrEmpty(receive_tel1))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        receive_tel1 = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(receive_tel1))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        receive_tel1 = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                    case CellType.Formula: //公式

                                                        try
                                                        {
                                                            var formulaValue = formulaEvaluator.Evaluate(cell);
                                                            if (formulaValue.CellType == CellType.String) receive_tel1 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                            else if (formulaValue.CellType == CellType.Numeric) receive_tel1 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        }
                                                        catch
                                                        {
                                                            receive_tel1 = cell.StringCellValue;
                                                        }




                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 4:
                                            #region 收件人電話2  
                                            if (cell != null)
                                            {
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        receive_tel2 = "";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        receive_tel2 = Row.GetCell(j).ToString();
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        receive_tel2 = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                    case CellType.Formula: //公式
                                                        try
                                                        {
                                                            var formulaValue = formulaEvaluator.Evaluate(cell);
                                                            if (formulaValue.CellType == CellType.String) receive_tel2 = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                            else if (formulaValue.CellType == CellType.Numeric) receive_tel2 = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        }
                                                        catch
                                                        {
                                                            receive_tel2 = cell.StringCellValue;
                                                        }

                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 5:
                                            #region 收件人地址
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) receive_address = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) receive_address = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        receive_address = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    receive_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                }
                                            }

                                            if (String.IsNullOrEmpty(receive_address))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                //搜尋tbCityArea資料表是否有一樣簡稱的地址，去進行地址拆解，如果沒有則進行SP正規化地址尋找
                                                using (SqlCommand cmd_CityArea = new SqlCommand())
                                                {
                                                    cmd_CityArea.Parameters.AddWithValue("@CityArea", receive_address);
                                                    cmd_CityArea.CommandText = @"select top 1 C.City,C.Area,S.station_code   
                                                                                from  tbCityArea C  
                                                                                Left join  ttArriveSitesScattered S
                                                                                On C.City = S.post_city and C.Zip = S.zip
                                                                                Where C.CityArea = @CityArea";
                                                    DataTable dta = new DataTable();
                                                    dta = dbAdapter.getDataTable(cmd_CityArea);
                                                    if (dta.Rows.Count > 0)
                                                    {
                                                        receive_city = dta.Rows[0]["city"].ToString();
                                                        receive_area = dta.Rows[0]["area"].ToString();
                                                        receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                        receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                        receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;




                                                        if (area_arrive_code == "")
                                                        {
                                                            ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                            if (_ArriveSites != null)
                                                            {
                                                                area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                               //Distributor = _ArriveSites.supplier_code;          //配送商
                                                            }
                                                            //int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                                            //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
                                                        }



                                                    }
                                                    else
                                                    {

                                                        //呼叫地址正規化SP，拆出縣市、鄉鎮區
                                                        using (SqlCommand cmd_addr = new SqlCommand())
                                                        {
                                                            string ChinaFoTaiwan = PublicFunction.ToTraditional(receive_address);
                                                            ChinaFoTaiwan = ChinaFoTaiwan.Replace(" ", "");
                                                            ChinaFoTaiwan = ChinaFoTaiwan.Replace("臺灣省", "");


                                                            int count = 0;
                                                            for (int temp = 0; temp < ChinaFoTaiwan.Length; temp++)
                                                            {

                                                                if (Utility.IsNumeric(ChinaFoTaiwan.Substring(temp, 1)))
                                                                {
                                                                    count = count + 1;


                                                                }
                                                                else
                                                                {
                                                                    break;
                                                                }
                                                            }
                                                            ChinaFoTaiwan = ChinaFoTaiwan.Substring(count, ChinaFoTaiwan.Length - count);




                                                            cmd_addr.Parameters.AddWithValue("@address", ChinaFoTaiwan);
                                                            cmd_addr.CommandText = "dbo.usp_AddrFormat";
                                                            cmd_addr.CommandType = CommandType.StoredProcedure;
                                                            try
                                                            {
                                                                DataTable dt_addr = dbAdapter.getDataTable(cmd_addr);
                                                                if (dt_addr != null && dt_addr.Rows.Count > 0)
                                                                {
                                                                    receive_city = dt_addr.Rows[0]["city"].ToString();
                                                                    receive_area = dt_addr.Rows[0]["area"].ToString();
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {

                                                            }
                                                        }

                                                        receive_address = PublicFunction.ToTraditional(receive_address);//receive_address.Replace("台", "臺");
                                                        receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                        receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;



                                                        int countRec = 0;
                                                        for (int temp = 0; temp < receive_address.Length; temp++)
                                                        {

                                                            if (Utility.IsNumeric(receive_address.Substring(temp, 1)))
                                                            {
                                                                countRec = countRec + 1;
                                                            }
                                                            else
                                                            {
                                                                break;
                                                            }
                                                        }
                                                        receive_address = receive_address.Substring(countRec, receive_address.Length - countRec);



                                                        if (area_arrive_code == "")
                                                        {
                                                            ArriveSites _ArriveSites = List.Find(x => x.post_city == receive_city && x.post_area == receive_area);
                                                            if (_ArriveSites != null)
                                                            {
                                                                area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                               //Distributor = _ArriveSites.supplier_code;          //配送商
                                                            }

                                                            if (area_arrive_code == "")
                                                            {
                                                                if (receive_city == "新竹市" || receive_city == "嘉義市")
                                                                {

                                                                    _ArriveSites = List.Find(x => x.post_city == receive_city);
                                                                    if (_ArriveSites != null)
                                                                    {
                                                                        area_arrive_code = _ArriveSites.station_code;  //到著碼
                                                                                                                       //Distributor = _ArriveSites.supplier_code;          //配送商
                                                                    }
                                                                }

                                                            }
                                                            //int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                                            //area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";



                                                        }


                                                    }
                                                }


                                            }

                                            #endregion
                                            break;
                                        case 6:
                                            #region 件數
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) pieces = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) pieces = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        pieces = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    pieces = Row.GetCell(j).ToString();
                                                }
                                            }
                                            if (!String.IsNullOrEmpty(pieces) && !Utility.IsNumeric(pieces))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 7:
                                            #region 重量
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) cbmWeight = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) cbmWeight = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        cbmWeight = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    cbmWeight = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (!String.IsNullOrEmpty(cbmWeight) && !Utility.IsNumeric(cbmWeight))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 8:
                                            #region 傳票類別
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) subpoena_category = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) subpoena_category = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        subpoena_category = cell.StringCellValue;
                                                    }


                                                }
                                                else
                                                {
                                                    subpoena_category = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (String.IsNullOrEmpty(subpoena_category))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                switch (subpoena_category)
                                                {
                                                    case "元付":
                                                        subpoena_code = "11";
                                                        break;
                                                    case "到付":
                                                        subpoena_code = "21";
                                                        break;
                                                    case "代收貨款":
                                                        subpoena_code = "41";
                                                        break;
                                                    case "元付-到付追加":
                                                        subpoena_code = "25";
                                                        break;
                                                    default:
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                        break;
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 9:
                                            #region 指配日期
                                            if (cell != null)
                                            {
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        arrive_assign_date = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        arrive_assign_date = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(arrive_assign_date))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Formula: //公式




                                                        try
                                                        {
                                                            var formulaValue = formulaEvaluator.Evaluate(cell);
                                                            if (formulaValue.CellType == CellType.String)
                                                            {
                                                                DateTime _dt;
                                                                if (DateTime.TryParse(formulaValue.StringValue.ToString(), out _dt))
                                                                {
                                                                    arrive_assign_date = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                                }
                                                                else
                                                                {
                                                                    arrive_assign_date = "NULL";
                                                                }

                                                            }
                                                            else if (formulaValue.CellType == CellType.Numeric)
                                                            {
                                                                if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                                {
                                                                    arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                                }
                                                                else
                                                                {
                                                                    arrive_assign_date = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                                }
                                                            }

                                                        }
                                                        catch
                                                        {

                                                        }


                                                        //if (DateUtil.IsCellDateFormatted(cell))  // 日期
                                                        //{
                                                        //    arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                        //}
                                                        //else
                                                        //{
                                                        //    var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        //    if (formulaValue.CellType == CellType.String) arrive_assign_date = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        //    else if (formulaValue.CellType == CellType.Numeric) arrive_assign_date = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                        //}
                                                        break;
                                                }
                                                DateTime _arrive_assign_date;
                                                if (DateTime.TryParse(arrive_assign_date, out _arrive_assign_date) && _arrive_assign_date > print_date)
                                                {
                                                    supplier_date = _arrive_assign_date;     //如果有指配日期，則配送日帶指配日期
                                                }
                                                else
                                                {
                                                    arrive_assign_date = "NULL";
                                                    _supplier_date = "NULL";


                                                }





                                            }
                                            else
                                            {
                                                arrive_assign_date = "NULL";
                                            }
                                            #endregion
                                            break;
                                        case 10:
                                            #region 指配時段 早/午/晚
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) time_period = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) time_period = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        time_period = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    time_period = Row.GetCell(j).ToString();
                                                }
                                            }

                                            #endregion
                                            break;
                                        case 11:
                                            #region 來回件(Y/N)
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) round_trip = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) round_trip = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        round_trip = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    round_trip = Row.GetCell(j).ToString();
                                                }
                                            }

                                            round_trip = (round_trip == "Y") ? "1" : "0";
                                            #endregion
                                            break;
                                        case 12:
                                            #region 回單
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) receipt_flag = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) receipt_flag = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        receipt_flag = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    receipt_flag = Row.GetCell(j).ToString();
                                                }
                                            }

                                            receipt_flag = (receipt_flag == "Y") ? "1" : "0";
                                            #endregion
                                            break;
                                        case 13:
                                            #region 代收貨款
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) collection_money = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) collection_money = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        collection_money = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    collection_money = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (!String.IsNullOrEmpty(collection_money) && !Utility.IsNumeric(collection_money))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            #endregion
                                            break;
                                        case 14:
                                            #region 產品編號
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) ArticleNumber = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) ArticleNumber = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        ArticleNumber = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    ArticleNumber = Row.GetCell(j).ToString();
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 15:
                                            #region 出貨平台
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) SendPlatform = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) SendPlatform = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        SendPlatform = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    SendPlatform = Row.GetCell(j).ToString();
                                                }
                                            }
                                            #endregion
                                            break;
                                        case 16:
                                            #region 品名
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) ArticleName = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) ArticleName = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        ArticleName = cell.StringCellValue;
                                                    }
                                                }
                                                else
                                                {
                                                    ArticleName = Row.GetCell(j).ToString();
                                                }
                                            }
                                            #endregion
                                            break;
                                        //case 17:
                                        //    #region 袋號
                                        //    if (cell != null)
                                        //    {
                                        //        if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                        //        {
                                        //            try
                                        //            {
                                        //                var formulaValue = formulaEvaluator.Evaluate(cell);
                                        //                if (formulaValue.CellType == CellType.String) bagno = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                        //                else if (formulaValue.CellType == CellType.Numeric) bagno = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                        //            }
                                        //            catch
                                        //            {
                                        //                bagno = cell.StringCellValue;
                                        //            }

                                        //        }
                                        //        else
                                        //        {
                                        //            bagno = Row.GetCell(j).ToString();
                                        //        }
                                        //    }
                                        //    #endregion
                                        //    break;
                                        case 18:
                                            #region 託運備註
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) invoice_desc = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) invoice_desc = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        invoice_desc = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    invoice_desc = Row.GetCell(j).ToString();
                                                }
                                            }

                                            #endregion

                                            break;
                                        case 19:
                                            #region 客代
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) customer_code = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) customer_code = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        customer_code = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    customer_code = Row.GetCell(j).ToString();
                                                }
                                            }

                                            if (customer_code != "")
                                            {
                                                using (SqlCommand cmd = new SqlCommand())
                                                {
                                                    cmd.CommandText = "select *,  s.supplier_name  from tbCustomers  c" +
                                                                      " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                                                                      " where c.customer_code = '" + customer_code + "' ";
                                                    using (DataTable dt = dbAdapter.getDataTable(cmd))
                                                        if (dt.Rows.Count > 0)
                                                        {
                                                            send_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
                                                            send_tel = dt.Rows[0]["telephone"].ToString();                        //電話
                                                            send_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
                                                            send_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
                                                            send_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
                                                            uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
                                                            supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
                                                            supplier_name = dt.Rows[0]["supplier_name"].ToString().Trim();        // 區配名稱
                                                            pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();          // 計價模式
                                                            send_address_parsing = true;
                                                            GetAddressParsing(send_city + send_area + send_address, customer_code.Trim(), "1", "1");
                                                        }
                                                }
                                            }
                                            #region 依託運類別檢查數量
                                            if (pieces == "")
                                            {
                                                IsSheetOk = false;
                                                lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!");
                                            }
                                            #endregion
                                            #endregion

                                            break;
                                        case 20:
                                            #region 付款方式
                                            if (cell != null)
                                            {
                                                if (cell.CellType == CellType.Formula)   //如果是公式，則帶結果的值
                                                {
                                                    try
                                                    {
                                                        var formulaValue = formulaEvaluator.Evaluate(cell);
                                                        if (formulaValue.CellType == CellType.String) payment_method = formulaValue.StringValue.ToString();          // 執行公式後的值為字串型態
                                                        else if (formulaValue.CellType == CellType.Numeric) payment_method = formulaValue.NumberValue.ToString();    // 執行公式後的值為數字型態
                                                    }
                                                    catch
                                                    {
                                                        payment_method = cell.StringCellValue;
                                                    }

                                                }
                                                else
                                                {
                                                    payment_method = Row.GetCell(j).ToString();
                                                }
                                            }

                                            #endregion
                                            break;
                                    }

                                    if (!IsChkOk)
                                    {
                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!");
                                    }
                                }



                                #endregion

                                #region 組新增

                                receive_address = receive_address.Replace("'", "＇");
                                receive_address = receive_address.Replace("--", "－");
                                receive_tel1 = (receive_tel1.Length > 20 ? receive_tel1.Substring(0, 20) : receive_tel1);
                                receive_tel2 = (receive_tel2.Length > 20 ? receive_tel2.Substring(0, 20) : receive_tel2);
                                receive_contact = receive_contact.Replace("\n", "");
                                receive_contact = (receive_contact.Length > 40 ? receive_contact.Substring(0, 40) : receive_contact);
                                receive_address = receive_address.Replace("\n", "");
                                receive_address = (receive_address.Length > 100 ? receive_address.Substring(0, 100) : receive_address);
                                ArticleNumber = (ArticleNumber.Length > 20 ? ArticleNumber.Substring(0, 20) : ArticleNumber);
                                SendPlatform = (SendPlatform.Length > 20 ? SendPlatform.Substring(0, 20) : SendPlatform);
                                ArticleName = (ArticleName.Length > 20 ? ArticleName.Substring(0, 20) : ArticleName);
                                receive_contact = NewString(receive_contact, 1);
                                receive_address = NewString(receive_address, 1);
                                invoice_desc = NewString(invoice_desc, 1);
                                payment_method = NewString(payment_method, 1);

                                strInsCol = "pricing_type,customer_code,check_number,order_number,CbmSize,check_type,subpoena_category,receive_tel1,receive_tel2,receive_contact,receive_city,receive_area,receive_address,area_arrive_code,ShuttleStationCode,SpecialAreaId,SpecialAreaFee,SendCode,SendSD,SendMD,ReceiveCode,ReceiveSD,ReceiveMD,receive_by_arrive_site_flag" +
                                            " ,pieces,cbmWeight,collection_money,send_contact,send_city,send_area,send_address,send_tel,product_category,invoice_desc,time_period,arrive_assign_date" +
                                            " ,receipt_flag,pallet_recycling_flag,cuser,cdate,uuser,udate,supplier_code,supplier_name,import_randomCode,print_date, supplier_date, print_flag, turn_board,upstairs,difficult_delivery" +
                                            " ,turn_board_fee,upstairs_fee,difficult_fee, add_transfer, Less_than_truckload, sub_check_number,ArticleNumber,SendPlatform,ArticleName,round_trip,payment_method";
                                strInsVal = "N\'" + pricing_type + "\', " +
                                            "N\'" + customer_code + "\', " +
                                            "N\'" + check_number + "\', " +
                                            "N\'" + order_number + "\', " +
                                            "N\'" + CbmSize + "\', " +
                                            "N\'" + check_type + "\', " +
                                            "N\'" + subpoena_code + "\', " +
                                            "N\'" + receive_tel1 + "\', " +
                                            "N\'" + receive_tel2 + "\', " +
                                            "N\'" + receive_contact + "\', " +
                                            "N\'" + receive_city + "\', " +
                                            "N\'" + receive_area + "\', " +
                                            "N\'" + receive_address + "\', " +
                                            "N\'" + area_arrive_code + "\', " +
                                            "N\'" + ShuttleStationCode + "\', " +
                                            "N\'" + SpecialAreaId + "\', " +
                                            "N\'" + SpecialAreaFee + "\', " +
                                            "N\'" + SendCode + "\', " +
                                            "N\'" + SendSD + "\', " +
                                            "N\'" + SendMD + "\', " +
                                            "N\'" + ReceiveCode + "\', " +
                                            "N\'" + ReceiveSD + "\', " +
                                            "N\'" + ReceiveMD + "\', " +
                                            "N\'" + receive_by_arrive_site_flag + "\', " +
                                            "N\'" + pieces + "\', " +
                                            "N\'" + cbmWeight + "\', " +
                                            "N\'" + collection_money + "\', " +
                                            "N\'" + (send_contact.Length > 20 ? send_contact.Substring(0, 20) : send_contact) + "\', " +
                                            "N\'" + send_city + "\', " +
                                            "N\'" + send_area + "\', " +
                                            "N\'" + send_address + "\', " +
                                            "N\'" + send_tel + "\', " +
                                            "N\'" + product_category + "\', " +
                                            "N\'" + invoice_desc + "\', " +
                                            "N\'" + time_period + "\', ";
                                if (arrive_assign_date == "NULL")
                                {
                                    strInsVal += arrive_assign_date + ", ";
                                }
                                else
                                {
                                    strInsVal += "N\'" + arrive_assign_date + "\', ";
                                }

                                strInsVal += "N\'" + receipt_flag + "\', " +
                                            "N\'" + pallet_recycling_flag + "\', " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + supplier_code + "\', " +
                                            "N\'" + supplier_name + "\', " +
                                            "N\'" + randomCode + "\', " +
                                            "N\'" + print_date.ToString("yyyy/MM/dd") + "\', ";

                                if (_supplier_date == "NULL")
                                {
                                    strInsVal += _supplier_date + ", ";
                                }
                                else
                                {
                                    strInsVal += "N\'" + supplier_date.ToString("yyyy/MM/dd") + "\', ";
                                }


                                strInsVal += "N\'" + print_flag + "\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'0\', " +
                                           "N\'" + "1" + "\', " +
                                           "N\'" + sub_check_number + "\', " +
                                           "N\'" + ArticleNumber + "\', " +
                                           "N\'" + SendPlatform + "\', " +
                                           "N\'" + ArticleName + "\', " +
                                           "N\'" + round_trip + "\', " +
                                           "N\'" + payment_method + "\'";

                                string insertstr = "INSERT INTO tcDeliveryRequests (" + strInsCol + ") VALUES(" + strInsVal + "); ";

                                //if (chk == 1)
                                //{
                                sb_temp_ins.Append(insertstr);
                                //每30筆組成一字串
                                if (i % 30 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }
                                //}
                                //else if (chk == 2)
                                //{
                                //    int temp_count = i - 2;

                                //    if (temp_count <= LeftCount)
                                //    {
                                //        sb_temp_ins.Append(insertstr);
                                //    }
                                //    else
                                //    {
                                //        lbMsg.Items.Add("第" + strRow + "列： 無法匯入，因為已經使用完預購袋號碼，請洽管理員!");
                                //    }
                                //    //每50筆組成一字串
                                //    if (i % 50 == 0 || i == u_sheet.LastRowNum)
                                //    {
                                //        sb_ins_list.Add(sb_temp_ins);
                                //        sb_temp_ins = new StringBuilder();
                                //    }
                                //}
                                #endregion
                            }


                        }

                        //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                        if (IsSheetOk)
                        {
                            #region 執行SQL

                            startTime = DateTime.Now;   //開始匯入時間
                                                        //新增
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    String strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }
                                    }
                                }
                            }
                            endTime = DateTime.Now;   //匯入結束時間

                            #endregion

                            #region  取貨號



                            //if (chk == 1)
                            //{
                            //    if (check_number != "")
                            //    {

                            //    }
                            //    else
                            //    {
                            for (int x = 0; x <= customerlist.Length - 1; x++)
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@customer_code", customerlist[x].ToString());
                                    cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address, customer_code
                                                                      From tcDeliveryRequests with(nolock) 
                                                                      where import_randomCode ='" + randomCode + "' and customer_code=@customer_code  and  check_number = ''");
                                    DataTable dt = dbAdapter.getDataTable(cmd);
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        string _customer_code = dt.Rows[0]["customer_code"].ToString();

                                        #region 取貨號(網路託運單)
                                        Int64 current_number = 0;

                                        using (SqlCommand cmdt = new SqlCommand())
                                        {
                                            DataTable dtt;
                                            cmdt.Parameters.AddWithValue("@customer_code", _customer_code);
                                            cmdt.CommandText = " Select * from tbDeliveryNumberSetting where customer_code=@customer_code and  IsActive=1   order by  begin_number ";
                                            dtt = dbAdapter.getDataTable(cmdt);
                                            if (dtt.Rows.Count == 1)
                                            {
                                                Int64 start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                                Int64 end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
                                                current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
                                                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                                {
                                                    if (current_number == 0 || current_number == end_number)
                                                    {
                                                        current_number = start_number;
                                                    }
                                                    else
                                                    {
                                                        current_number += 1;
                                                    }

                                                    using (SqlCommand cmd2 = new SqlCommand())
                                                    {

                                                        cmd2.Parameters.AddWithValue("@check_number", current_number);           //目前取號
                                                        cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                                        cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);         //修改
                                                        cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                                                        cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
                                                        cmd2.CommandText += " update tbDeliveryNumberSetting set current_number =@current_number where customer_code=@customer_code ";
                                                        dbAdapter.execNonQuery(cmd2);
                                                    }
                                                }
                                            }
                                            else if (dtt.Rows.Count > 1)
                                            {
                                                current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
                                                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                                {
                                                    if (current_number == 0)
                                                    {
                                                        current_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                                    }
                                                    //else
                                                    //{
                                                    int Counttemp = 0;
                                                    for (int z = 0; z <= dtt.Rows.Count - 1; z++)
                                                    {
                                                        Int64 start_number = Convert.ToInt64(dtt.Rows[z]["begin_number"]);
                                                        Int64 end_number = Convert.ToInt64(dtt.Rows[z]["end_number"]);
                                                        if (end_number == current_number)
                                                        {
                                                            Counttemp = Counttemp + 1;
                                                            if (z + 1 != dtt.Rows.Count)
                                                            {
                                                                current_number = Convert.ToInt64(dtt.Rows[z + 1]["begin_number"]);
                                                            }
                                                            else
                                                            {
                                                                current_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    if (Counttemp == 0)
                                                    {
                                                        current_number = current_number + 1;
                                                    }

                                                    using (SqlCommand cmd2 = new SqlCommand())
                                                    {

                                                        cmd2.Parameters.AddWithValue("@check_number", current_number);           //目前取號
                                                        cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                                        cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);         //修改
                                                        cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                                                        cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
                                                        cmd2.CommandText += " update tbDeliveryNumberSetting set current_number =@current_number where customer_code=@customer_code ";
                                                        dbAdapter.execNonQuery(cmd2);
                                                    }


                                                    //}
                                                }


                                            }


                                        }
                                        #endregion


                                    }
                                }
                            }


                            //    }
                            //}
                            //else if (chk == 2)
                            //{
                            //    #region  取貨號
                            //    using (SqlCommand cmd = new SqlCommand())
                            //    {
                            //        cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address, customer_code
                            //                                          From tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'  and  check_number = ''");
                            //        DataTable dt = dbAdapter.getDataTable(cmd);
                            //        if (dt != null && dt.Rows.Count > 0)
                            //        {
                            //            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            //            {
                            //                string _customer_code = dt.Rows[0]["customer_code"].ToString();
                            //                #region 取貨號(網路託運單)
                            //                Int64 current_number = 0;
                            //                using (SqlCommand cmdt = new SqlCommand())
                            //                {
                            //                    DataTable dtt;
                            //                    cmdt.Parameters.AddWithValue("@customer_code", _customer_code);
                            //                    cmdt.CommandText = " Select top 1 *  from tbDeliveryNumberOnceSetting with(nolock) where customer_code=@customer_code and  IsActive=1 order by end_number ";
                            //                    dtt = dbAdapter.getDataTable(cmdt);
                            //                    if (dtt.Rows.Count > 0)
                            //                    {
                            //                        string seq = dtt.Rows[0]["seq"].ToString();
                            //                        Int64 start_number = Convert.ToInt64(dtt.Rows[0]["begin_number"]);
                            //                        Int64 end_number = Convert.ToInt64(dtt.Rows[0]["end_number"]);
                            //                        current_number = dtt.Rows[0]["current_number"] != DBNull.Value ? Convert.ToInt64(dtt.Rows[0]["current_number"].ToString()) : 0;
                            //                        if (current_number == 0)
                            //                        {
                            //                            //未取過號，直接從start_number 開始取
                            //                            current_number = start_number;
                            //                        }
                            //                        else if (!(current_number >= start_number && current_number <= end_number))
                            //                        {
                            //                            //如果當前取號不在現在取號區號，那也從當然取區間的start_number開始取
                            //                            current_number = start_number;
                            //                        }
                            //                        else
                            //                        {
                            //                            current_number += 1;
                            //                        }

                            //                        using (SqlCommand cmd2 = new SqlCommand())
                            //                        {
                            //                            cmd2.Parameters.AddWithValue("@check_number", current_number);             //目前取號
                            //                            cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                            //                            cmd2.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd2);    //修改

                            //                            cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
                            //                            cmd2.Parameters.AddWithValue("@customer_code", _customer_code);
                            //                            if (current_number == end_number)
                            //                            {
                            //                                cmd2.Parameters.AddWithValue("@seq", seq);
                            //                                cmd2.Parameters.AddWithValue("@IsActive", 0);                         //預購袋取號完畢，直接改停用
                            //                                cmd2.CommandText += " update tbDeliveryNumberOnceSetting set current_number =@current_number, IsActive=@IsActive where customer_code=@customer_code and seq=@seq ";
                            //                            }
                            //                            else
                            //                            {
                            //                                cmd2.CommandText += " update tbDeliveryNumberOnceSetting set current_number =@current_number where customer_code=@customer_code ";
                            //                            }


                            //                            dbAdapter.execNonQuery(cmd2);
                            //                        }
                            //                    }
                            //                }
                            //                #endregion
                            //            }
                            //        }
                            //    }

                            //    #endregion
                            //}

                            #endregion



                            #region 匯入記錄
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                cmd.Parameters.AddWithValue("@changeTable", "tcDeliveryRequests");
                                cmd.Parameters.AddWithValue("@fromWhere", "託運資料維護-匯入");
                                cmd.Parameters.AddWithValue("@randomCode", randomCode);
                                cmd.Parameters.AddWithValue("@memo", "");
                                cmd.Parameters.AddWithValue("@successNum", successNum.ToString());
                                cmd.Parameters.AddWithValue("@failNum", failNum.ToString());
                                cmd.Parameters.AddWithValue("@totalNum", totalNum.ToString());
                                cmd.Parameters.AddWithValue("@startTime", startTime);
                                cmd.Parameters.AddWithValue("@endTime", endTime);
                                cmd.Parameters.AddWithValue("@customer_code", customer_code);
                                cmd.Parameters.AddWithValue("@print_date", print_date);
                                cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);
                                cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                            #endregion
                            if (dlcustomer_code.SelectedValue != "") dlcustomer_code_SelectedIndexChanged(null, null);
                            Losdtoday_customer_code();
                            readdata();
                        }
                        else
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                    }

                    //}
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    //System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                }
                lbMsg.Items.Add("執行完畢");
            }
        }
    }

    protected void btnAskForTakePackage_Click(object sender, EventArgs e)
    {
        string customerCode;

        if (Session["manager_type"].ToString() == "5")
            customerCode = Session["account_code"].ToString();
        else
            customerCode = customerDropDown.SelectedValue;

        string url = "LT_transport_1_3_1.aspx?customerCode=" + customerCode + "&urlUserComeFrom=" + Request.Url.LocalPath.Trim('/');
        Response.Redirect(url);
    }

    private static String PreparePOSTForm(string url, NameValueCollection data, string customerCode)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");


        strForm.Append("<a href='LT_transport_1_3_1.aspx?customerCode=" + customerCode + "'>派員取件</a>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    private string SqlReturn(string sql)
    {
        string[] sqlArray = sql.Split(',');
        for (var i = 0; i < sqlArray.Length; i++)
        {
            sqlArray[i].Trim(' ');
        }
        string[] change1 = new string[] { "check_number", "receive_tel1", "receive_contact", "receive_city", "receive_area", "receive_address", "area_arrive_code", "ReceiveCode", "ReceiveSD", "ReceiveMD" };
        string[] change2 = new string[] { "roundtrip_checknumber", "send_tel", "send_contact", "send_city", "send_area", "send_address", "send_station_scode", "SendCode", "SendSD", "SendMD" };
        for (var i = 0; i < change1.Length; i++)
        {
            int cha1 = Array.IndexOf(sqlArray, change1[i]);
            int cha2 = Array.IndexOf(sqlArray, change2[i]);
            string str1 = sqlArray[cha1];
            string str2 = sqlArray[cha2];
            sqlArray[cha1] = str2;
            sqlArray[cha2] = str1;
        }

        string sqlResult = "";
        for (var i = 0; i < sqlArray.Length - 1; i++)
        {
            sqlResult += sqlArray[i] + ",";
        }
        sqlResult += sqlArray[sqlArray.Length - 1];

        return sqlResult;
    }
    private string SqlValueReturn(string sql)       //目的是將代收貨款在回程時歸零
    {
        string[] sqlArray = sql.Split(',');
        string result = "";

        sqlArray[28] = "0";           //此為回程代收貨款
        sqlArray[17] = "0";           //回件SpecialAreaId
        sqlArray[18] = "0";           //回件SpecialAreaFee
        for (var i = 0; i < sqlArray.Length - 1; i++)
        {
            result += sqlArray[i] + ",";
        }
        result += sqlArray[sqlArray.Length - 1];

        return result;
    }

    private int GetCustomerType(string customerCode)
    {
        int result;

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = @"select product_type from tbCustomers where customer_code = @customer_code";
            cmd.Parameters.AddWithValue("@customer_code", customerCode);

            string strResult = dbAdapter.getScalarBySQL(cmd).ToString();
            int.TryParse(strResult, out result);
        }

        return result;
    }

    private static String PreparePrintDeliverySummaryCommitForm(string url, NameValueCollection data)
    {
        //Set a name for the form
        string formID = "PostForm";
        //Build the form using the specified data to be posted.
        StringBuilder strForm = new StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\" >");

        foreach (string key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key +
                           "\" value=\"" + data[key] + "\">");
        }

        strForm.Append("<input type='button' value='回上一頁' onclick='javascript:window.history.back();'/>");

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation.
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language='javascript'>");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        //strScript.Append("window.history.back();");
        strScript.Append("</script>");
        //Return the form and the script concatenated.
        //(The order is important, Form then JavaScript)
        return strForm.ToString() + strScript.ToString();
    }

    public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        AddressParsingInfo Info = new AddressParsingInfo();
        var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);

        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
        var response = client.Post<ResData<AddressParsingInfo>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                if (response.Data == null)
                {

                    check_address = false;
                    return null;
                }

                else
                {
                    Info = response.Data.Data;
                    area_arrive_code = Info.StationCode;
                    ShuttleStationCode = Info.ShuttleStationCode;
                    if (send_address_parsing == true)
                    {
                        SendCode = Info.StationCode;
                        SendSD = Info.SendSalesDriverCode;
                        SendMD = Info.SendMotorcycleDriverCode;
                    }
                    else
                    {
                        ReceiveCode = Info.StationCode;
                        ReceiveSD = Info.SalesDriverCode;
                        ReceiveMD = Info.MotorcycleDriverCode;
                    }

                    //如果到著站空白,不給過
                    if (string.IsNullOrEmpty(area_arrive_code))
                    {
                        check_address = false;
                        return null;
                    }

                }
            }

            catch (Exception e)
            {


            }

        }
        else
        {

        }
        return Info;
    }

    public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
    {
        SpecialAreaManage Info = new SpecialAreaManage();
        var AddressParsingURL = "http://map2.fs-express.com.tw/Address/GetSpecialAreaDetail";

        //打API
        var client = new RestClient(AddressParsingURL);
        var request = new RestRequest(Method.POST);
        request.Timeout = 5000;
        request.ReadWriteTimeout = 5000;
        request.RequestFormat = DataFormat.Json;
        request.AddBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

        var response = client.Post<ResData<SpecialAreaManage>>(request);
        if (response.StatusCode == HttpStatusCode.OK)
        {
            try
            {
                Info = response.Data.Data;
                if (Info.id > 0)
                {
                    area_arrive_code = Info.StationCode;
                    SpecialAreaId = Info.id.ToString();
                    SpecialAreaFee = Info.Fee.ToString();
                    ReceiveCode = Info.StationCode;
                    ReceiveSD = Info.SalesDriverCode;
                    ReceiveMD = Info.MotorcycleDriverCode;
                }
            }
            catch (Exception e)
            {
                throw e;

            }

        }
        else
        {
            throw new Exception("地址解析有誤");
        }

        return Info;
    }



}
