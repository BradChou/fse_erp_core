﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using DocumentFormat.OpenXml.Bibliography;

public partial class CityArea : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string customer_code
    {
        // for權限
        get { return ViewState["customer_code"].ToString(); }
        set { ViewState["customer_code"] = value; }
    }

    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            DDLSet();
            DefaultData();
        }
    }

    protected void DDLSet()
    {

        String strSQL = @"SELECT code_id id,code_name name FROM tbItemCodes item With(Nolock) WHERE item.code_sclass=N'H1' AND active_flag IN(1)";
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = strSQL;
            DataTable dt = dbAdapter.getDataTable(cmd);
        }

        ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));
        


        SqlCommand cmd1 = new SqlCommand();
        cmd1.CommandText = "select * from tbPostCity order by seq asc ";
        ddl_City.DataSource = dbAdapter.getDataTable(cmd1);
        ddl_City.DataValueField = "city";
        ddl_City.DataTextField = "city";
        ddl_City.DataBind();
        ddl_City.Items.Insert(0, new ListItem("請選擇", ""));
        ddl_area.Items.Insert(0, new ListItem("請選擇", ""));



    }


    protected void DefaultData()
    {
        String strSQL = string.Empty;
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";

            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
   
            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }

            if (Request.QueryString["Num"] != null)
            {
                tb_searchNum.Text = Request.QueryString["Num"];
                querystring += "&Num=" + tb_searchNum.Text;
            }


            #endregion



            string strWhere = "";



            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY tbCityArea.seq) AS num
                             ,tbCityArea.seq
                             ,tbCityArea.cuser
                             ,tbCityArea.cdate
                             ,tbCityArea.CityArea
                             ,tbCityArea.City
                             ,tbCityArea.Area
                             ,tbCityArea.Zip
                             ,CASE tbCityArea.IsActive WHEN '1' THEN '是' ELSE '否' END IsActive   
                         FROM tbCityArea  With(Nolock)
                         LEFT JOIN tbAccounts  With(Nolock) ON tbAccounts.account_code = tbCityArea.Cuser
                      where 1=1   ";




            #region 查詢條件


            if (tb_search.Text != "")
            {


                cmd.Parameters.AddWithValue("@CityArea", tb_search.Text);
            
                strWhere += " AND(tbCityArea.CityArea like '%'+@CityArea+'%' ) ";

            }


            if (tb_searchNum.Text != "" )
            {
                if(Utility.IsNumeric(tb_searchNum.Text))
                {
                    cmd.Parameters.AddWithValue("@Zip", tb_searchNum.Text);
                    strWhere += " AND(tbCityArea.Zip = @Zip) ";
                }
                else
                {
                    strWhere += " AND(tbCityArea.Zip = 999999) ";
                }
            }

            if (ddl_active.SelectedValue != "")
            {

                strWhere += " AND tbCityArea.IsActive IN (" + ddl_active.SelectedValue.ToString() + ")";

            }

            #endregion

            cmd.CommandText = string.Format(strSQL + " {0}", strWhere);


            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (Utility.IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "active=" + ddl_active.SelectedValue;
                         

     
        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }
        if (!string.IsNullOrEmpty(tb_searchNum.Text))
        {
            str_Query += "&Num=" + tb_searchNum.Text;
        }



        Response.Redirect(ResolveUrl("~/CityArea.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("emp_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢視(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
   
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "縣市鄉鎮-新增";
                pan_edit.Visible = true;
                pan_view.Visible = false;
                lbl_id.Text = "0";
                rb_Active.SelectedValue = "1";
                ddl_City.SelectedValue = "";
                ddl_area.SelectedValue = "";
                note.Text = "";
                key.Text = "";
                Zip.Text = "";
                #endregion
                break;
            case 2:
               
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "縣市鄉鎮-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = "";

                        strSQL = @"SELECT A.CityArea
                             ,A.City
                             ,A.area
                             ,A.IsActive 
                             ,A.Zip 
                             ,A.note              
                         FROM tbCityArea A With(Nolock)
                        WHERE seq = @seq ";

                        cmd.Parameters.AddWithValue("@seq", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                key.Text = row["CityArea"].ToString();
                                note.Text = row["note"].ToString();
                                rb_Active.SelectedValue = Convert.ToInt16(row["IsActive"]).ToString();
                                lbl_id.Text = id.ToString();
                                ddl_City.SelectedValue = row["City"].ToString();
                                City_SelectedIndexChanged(ddl_City, null);
                                ddl_area.SelectedValue = row["area"].ToString();
                                Zip.Text = row["Zip"].ToString();

                            }
                        }
                    }

                    pan_edit.Visible = true;
                    pan_view.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "縣市鄉鎮維護";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;//true:add false:edit     
        Boolean IsOk = true;
        int i_id = 0;
        string strSQL = "";



        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;



        if (!IsOk) return;

        #region 存檔
        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
            using (SqlCommand cmd = new SqlCommand())
            {

                if (IsEdit)
                {
                 
                    cmd.Parameters.AddWithValue("@IsActive", rb_Active.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);
                    cmd.Parameters.AddWithValue("@Zip", Zip.Text);
                    cmd.Parameters.AddWithValue("@note", note.Text);
                    cmd.Parameters.AddWithValue("@CityArea", key.Text);
                    cmd.Parameters.AddWithValue("@City", ddl_City.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@Area", ddl_area.SelectedValue.ToString());

                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "seq", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm("tbCityArea", cmd);
                    dbAdapter.execNonQuery(cmd);
                    SetEditOrAdd(0);
                    RetuenMsg("修改成功!");
                }
                else
                {



                    cmd.Parameters.AddWithValue("@IsActive", rb_Active.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", Session["account_code"]);
                    cmd.Parameters.AddWithValue("@Zip", Zip.Text);
                    cmd.Parameters.AddWithValue("@note", note.Text);
                    cmd.Parameters.AddWithValue("@CityArea", key.Text);
                    cmd.Parameters.AddWithValue("@City", ddl_City.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@Area", ddl_area.SelectedValue.ToString());

                    cmd.CommandText = dbAdapter.genInsertComm("tbCityArea", true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;
                    SetEditOrAdd(0);
                    RetuenMsg("新增成功!");
                }



            }
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dlcity = (DropDownList)sender;
        DropDownList dlarea = null;
        dlarea = ddl_area;

        if (dlcity.SelectedValue != "")
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
            SqlCommand cmda = new SqlCommand();
            DataTable dta = new DataTable();
            cmda.Parameters.AddWithValue("@city", dlcity.SelectedValue.ToString());
            cmda.CommandText = "Select city,area from tbPostCityArea With(Nolock) where city=@city order by seq asc";
            dta = dbAdapter.getDataTable(cmda);
            if (dta.Rows.Count > 0)
            {
                for (int i = 0; i < dta.Rows.Count; i++)
                {
                    dlarea.Items.Add(new ListItem(dta.Rows[i]["area"].ToString().Trim(), dta.Rows[i]["area"].ToString().Trim()));
                }
            }
        }
        else
        {
            dlarea.Items.Clear();
            dlarea.Items.Add(new ListItem("請選擇", ""));
        }
        Zip.Text = "";
    }
    protected void Area_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddl_area.SelectedValue.ToString() != "")
        {
            using (SqlCommand cmda = new SqlCommand())
            {
                DataTable dta = new DataTable();
                cmda.Parameters.AddWithValue("@city", ddl_City.SelectedValue);
                cmda.Parameters.AddWithValue("@area", ddl_area.SelectedValue);
                cmda.CommandText = "Select top 1 Zip  from tbPostCityArea  With(Nolock) where city=@city  AND area = @area  ";
                dta = dbAdapter.getDataTable(cmda);
                if (dta.Rows.Count > 0)
                {
                    Zip.Text = dta.Rows[0]["Zip"].ToString();
 
                }
            }
            
        }
        else
        {
            Zip.Text = "";
        }

      


    }



    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }

    }



    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }



}
