﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterTransport.master" AutoEventWireup="true" CodeFile="palletmag_3.aspx.cs" Inherits="palletmag_3" EnableEventValidation = "false"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/build.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            $(".date_picker").datepicker({
                dateFormat: 'yy/mm/dd',
                changeMonth: true,
                changeYear: true,
                firstDay: 1,
                defaultDate: (new Date())  //預設當日
            });

            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $(".date_picker").datepicker({
                    dateFormat: 'yy/mm/dd',
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    defaultDate: (new Date())  //預設當日
                });
            }

            $(function () {
                $("#<%=chkall.ClientID%>").click(function () {
                    if ($("#<%=chkall.ClientID%>").prop("checked")) {
                    $("[id*=chkcus_]").each(function () {
                        var sID_BOX = $(this).attr("id");
                        document.getElementById(sID_BOX).checked = true;
                    });

                } else {
                    $("[id*=chkcus_]").each(function () {
                        var sID_BOX = $(this).attr("id");
                        document.getElementById(sID_BOX).checked = false;
                    });
                }

            });
            });
        });
    </script>
    <style>
        th {
            text-align: center;
        }
        input[type="radio"], input[type="checkbox"] {
            display: inherit;
            margin-right: 3px;
        }

        .checkbox label {
            margin-right: 15px;
            text-align: left;
        }
        ._top_left {
            float: left;
        }
        ._top_right {           
            float: right;
            right: 5px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">欠板統計表</h2>

            <asp:Panel ID="pan_list" runat="server">
                <div class="col-lg-12 form-inline form-group">
                    <table class="tb_top">
                        <tr>
                            <td><label>查詢條件</label> </td>
                            <td class="_top_left">
                                <label>客　　代：</label>
                                <asp:DropDownList ID="second_code" runat="server" CssClass="form-control rec_cuscode" ></asp:DropDownList>
                                <label>開始統計日期：</label>
                                <asp:TextBox ID="sdate" runat="server" class="form-control" CssClass="date_picker" autocomplete="off"></asp:TextBox>
                            </td>
                            <td class="_top_right">
                                <asp:Button ID="search" CssClass="btn btn-primary" runat="server" Text="查 詢" OnClick="search_Click" />
                                &nbsp;
                                <%--<asp:Button ID="export" CssClass="btn btn-primary" runat="server" Text="下 載" OnClick="export_Click" />--%>
                                <asp:Button ID="export2" CssClass="btn btn-primary" runat="server" Text="下 載" OnClick="export2_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2" >
                                <%--<label>區　　配：</label><asp:CheckBox ID="chkall" CssClass="checkbox checkbox-success" Text='全選' runat="server" />--%>
                                <div class="panel panel-info ">
                                    <div class="panel-heading">區　　配<asp:CheckBox ID="chkall" CssClass="checkbox checkbox-success" Text='全選' runat="server"  /></div>
                                    <div class="panel-body">
                                        <div class="row ">
                                            <div class="col bottnmargin form-inline">
                                                <div class="_checkboxlist ">
                                                    <asp:Repeater ID="rp_sup" runat="server">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="Hid_id" runat="server" Value='<%# Eval("supplier_code") %>' />
                                                            <asp:CheckBox ID="chkcus" CssClass=" checkbox checkbox-success" Text='<%# Eval("supplier_name") %>' runat="server" Checked='<%# Convert.ToBoolean(DataBinder.Eval(Container, "DataItem.chk"))%>' />
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td> 
                        </tr>
                    </table>
                    <hr />
                </div>

                <%--<asp:DataList ID="DataList1" runat="server"   Width="100%" RepeatLayout="Table" OnItemDataBound ="DataList1_ItemDataBound">
                    <ItemStyle  VerticalAlign="Top"></ItemStyle>
                    <ItemTemplate>
                        <div class="DataListItem" style="padding-left: 20px; padding-bottom: 20px;">
                            <div >
                                <%#Server.HtmlEncode(DataBinder.Eval(Container.DataItem, "[0]").ToString())%>
                               
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>--%>
            </asp:Panel>

            <div style="overflow: auto; width: 100%">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered templatemo-user-table table_list" ShowHeaderWhenEmpty ="True" 
                    CellPadding="0" BorderWidth="1px" OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Literal ID="Li_Supplier_name" runat="server" Text='<%# Server.HtmlEncode(Eval("area_arrive_code").ToString()) + " " +Server.HtmlEncode(Eval("Supplier_name").ToString()) %>' ></asp:Literal>
                                <asp:HiddenField ID="Hid_area_arrive_code" runat="server" Value='<%# Server.HtmlEncode(Eval("area_arrive_code").ToString()) %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" Width="100px" Wrap="False" />
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle CssClass="bg-light-blue" HorizontalAlign ="Center"  />
                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>  
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>

