﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterMoney.master" AutoEventWireup="true" CodeFile="money_2_2.aspx.cs" Inherits="money_2_2" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () { 

            $("#<%= Suppliers.ClientID %>").change(function () {
                ChangeSecondCode();
            });

            $("#<%= second_code.ClientID %>").change(function () {
                ChangeBusinessPeople();
            });
        });
               
        function ChangeBusinessPeople()
        {
            var sel_customer_code = $.trim($("#<%= second_code.ClientID%> option:selected").val());
            if (sel_customer_code.length > 0) {
                $.ajax(
                {
                    url: "money_2_2.aspx/GetBusinessPeople",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ customer_code: sel_customer_code }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    $("#<%= business_people.ClientID%>").val(data.d);
                });
            }
        }

        function SetSecondCodeDDLEmpty() {
            $("#<%= second_code.ClientID%>").empty();
            $("#<%= second_code.ClientID%>").append($('<option></option>').val('').text('(全部)'));
        }
        function ChangeSecondCode() {
            var sel_master_code = $.trim($("#<%= Suppliers.ClientID%> option:selected").val());
            if (sel_master_code.length == 0) {
                SetSecondCodeDDLEmpty();
            }
            else {
                $.ajax(
                {
                    url: "money_2_2.aspx/GetSecondCodeDDLHtml",
                    type: 'POST',
                    async: false,//同步      
                    data: JSON.stringify({ master_code: sel_master_code }),
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",      //如果要回傳值，請設成 json
                    error: function (xhr) {//發生錯誤時之處理程序 
                    },
                    success: function (reqObj) {//成功完成時之處理程序 
                        //alert('');
                    }
                }).done(function (data, statusText, xhr) {
                    if (data.d.length > 0) {
                        $("#<%= second_code.ClientID%>").html(data.d);
                    }
                    else {
                        SetSecondCodeDDLEmpty();
                    }
                });
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="page-wrapper">
        <div class="row">
            <h2 class="margin-bottom-10">營業獎金</h2>

            <div class="templatemo-login-form" >
                <div class="form-group form-inline">
                    <label for="inputLastName">統計期間</label>
                    <asp:TextBox ID="dates" runat="server" class="form-control" maxDate="endDate" CssClass="date_picker startDate"></asp:TextBox> ~ 
                    <asp:TextBox ID="datee" runat="server" class="form-control" minDate="startDate" CssClass="date_picker endDate"></asp:TextBox>
                </div>
                <div class="form-group form-inline">
                    <label for="inputFirstName">營業人員</label>
                    <asp:TextBox ID="business_people" CssClass="form-control" runat="server"></asp:TextBox> 
                    <label for="inputFirstName">客戶代號</label>                    
                    <asp:DropDownList ID="Suppliers" runat="server" CssClass="form-control" OnSelectedIndexChanged="Suppliers_SelectedIndexChanged" ></asp:DropDownList>
                    <asp:DropDownList ID="second_code" runat="server" CssClass="form-control" ></asp:DropDownList>                   
                    <asp:Button ID="btQry" runat="server" class="btn btn-primary" Text="查詢" OnClick="btQry_Click"  /> 
                    <asp:Button ID="btExport" CssClass="btn btn-primary" runat="server" Text="匯出" OnClick="btExport_Click"/>
                </div>

                <hr>
                <h3 class="text-primary">
                    <asp:Label ID="customer" runat="server" ></asp:Label><asp:Label ID="business_people2" runat="server" ></asp:Label>出貨實績統計</h3>
                <p>統計期間：<asp:Label ID="lbdate" runat="server"></asp:Label></p>
                <table class="table table-striped table-bordered templatemo-user-table">
                    <tr class="tr-only-hide">
                        <th>序號</th>
                        <th>客戶名稱</th>
                        <th>客戶代號</th>
                        <th>計價方式</th>
                        <th>出貨板數</th>
                        <th>營業額</th>
                        <th>營業人員</th>
                        <th>結帳日</th>
                    </tr>
                    <asp:Repeater ID="New_List" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="序號">
                                    <%# (((RepeaterItem)Container).ItemIndex+1).ToString() %>
                                </td>
                                <td data-th="客戶名稱"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_name").ToString())%></td>
                                <td data-th="客戶代號"><%# CustomerCodeDisplay(Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.customer_code").ToString()))%></td>
                                <td data-th="計價方式"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.code_name").ToString())%></td>
                                <td data-th="出貨板數"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.cnt","{0:N0}").ToString())%></td>
                                <td data-th="營業額"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.totalfee","{0:N0}").ToString())%></td>
                                <td data-th="營業人員"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.business_people").ToString())%></td>
                                <td data-th="結帳日"><%#Server.HtmlEncode(DataBinder.Eval(Container, "DataItem.checkout_close_date","{0:yyyy/MM/dd}").ToString())%></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <% if (New_List.Items.Count == 0)
                        {%>
                    <tr>
                        <td colspan="8" style="text-align: center">尚無資料</td>
                    </tr>
                    <% } %>
                </table>
            </div>
        </div>
    </div>
</asp:Content>

