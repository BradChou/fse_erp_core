﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class assets1_9_carsel : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            readdata();
        }
    }


    private void readdata()
    {
       
        using (SqlCommand cmd = new SqlCommand())
        {
           
            cmd.Parameters.Clear();
            string wherestr = "";

            #region 關鍵字
            if (keyword.Text.ToString() != "")
            {
                cmd.Parameters.AddWithValue("@keyword", keyword.Text.ToString());
                wherestr += "  and (a.car_license like '%'+@keyword+'%' or a.assets_name like '%'+@keyword+'%')";
            }
            #endregion

            cmd.CommandText = string.Format(@"select A.* , OS.code_name as ownership_text ,CD.code_name as car_retailer_text  , CT.code_name  as cabin_type_text
                                                    from ttAssets A with(nolock)
                                                    left join tbItemCodes OS with(nolock) on A.ownership  = OS.code_id and OS.code_bclass = '6' and OS.code_sclass= 'OS'
                                                    left join tbItemCodes CD with(nolock) on A.car_retailer  = CD.code_id and CD.code_bclass = '6' and CD.code_sclass= 'CD'
                                                    left join tbItemCodes CT with(nolock) on A.cabin_type  = CT.code_id and CT.code_bclass = '6' and CT.code_sclass= 'cabintype'
                                                where 1=1  {0} 
                                                order by a.get_date asc", wherestr);


            DataTable dt = dbAdapter.getDataTable(cmd);
            New_List.DataSource = dt;
            New_List.DataBind();
            
        }           

    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdSelect")
        {
            string a_id = ((HiddenField)e.Item.FindControl("Hid_id")).Value.ToString().Trim();
            string ownership = ((HiddenField)e.Item.FindControl("Hid_ownership")).Value.ToString().Trim();
            string car_license = ((Literal)e.Item.FindControl("Licar_license")).Text.Trim();
            string car_retailer_text = ((Literal)e.Item.FindControl("car_retailer_text")).Text.Trim();
            string cabin_type_text = ((Literal)e.Item.FindControl("cabin_type_text")).Text.Trim();
            string driver = ((Literal)e.Item.FindControl("driver")).Text.Trim();
            string PaymentObject = ((Literal)e.Item.FindControl("PaymentObject")).Text.Trim();
            string RequestObject = ((HiddenField)e.Item.FindControl("RequestObject")).Value.Trim();
            if (!string.IsNullOrEmpty(RequestObject))
            {
                RequestObject = Func.GetRow("code_name", "tbItemCodes", "", "code_id = @code_id and code_bclass = '6' and code_sclass = 'dept' and active_flag = 1", "code_id", RequestObject);
            }
            //string manager_type = ((HiddenField)e.Item.FindControl("Hid_mangtype")).Value.ToString().Trim();
            //string customer_shortname = ((Literal)e.Item.FindControl("LiCus_name")).Text.Trim();
            //string job_title = ((Literal)e.Item.FindControl("Lijob_title")).Text.Trim();
            //string user_name = ((Literal)e.Item.FindControl("LiName")).Text.Trim();
            string JavaScriptStr = "self.parent.$('#" + Request.QueryString["Hid_assets_id"] + "').val('" + a_id + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["car_license"] + "').val('" + car_license + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["car_retailer_text"] + "').val('" + car_retailer_text + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["cabin_type_text"] + "').val('" + cabin_type_text + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["driver"] + "').val('" + driver + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["PaymentObject"] + "').val('" + PaymentObject + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["RequestObject"] + "').val('" + RequestObject + "');";
            JavaScriptStr += "self.parent.$('#" + Request.QueryString["Hid_ownership"] + "').val('" + ownership + "');";

            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>"+ JavaScriptStr + " parent.$.fancybox.close();</script>", false);

            return;

        }
       
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
    }


    
}