﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


public partial class system_7 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
            DDLSet();
            DefaultData();
        }
    }

    protected void DDLSet()
    {

        //String strSQL = @"SELECT supplier_code 'id',supplier_name +'(' +supplier_code+')' 'name' FROM tbSuppliers  With(Nolock) WHERE active_flag =1";
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = strSQL;
        //    DataTable dt = dbAdapter.getDataTable(cmd);
        //    ddl_supplier.DataSource = dt;
        //    ddl_supplier.DataValueField = "id";
        //    ddl_supplier.DataTextField = "name";
        //    ddl_supplier.DataBind();
        //    ddl_supplier.Items.Insert(0, new ListItem("全部", ""));


        //    ddl_supplier_edit.DataSource = dt;
        //    ddl_supplier_edit.DataValueField = "id";
        //    ddl_supplier_edit.DataTextField = "name";
        //    ddl_supplier_edit.DataBind();
        //    ddl_supplier_edit.Items.Insert(0, new ListItem("請選擇", ""));
        //    dt.Dispose();
        //}
        string manager_type = Session["manager_type"].ToString(); //管理單位類別
        string account_code = Session["account_code"].ToString(); //使用者帳號
        string supplier_code = Session["master_code"].ToString();
        ddl_station.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
        ddl_station.DataValueField = "supplier_code";
        ddl_station.DataTextField = "showname";
        ddl_station.DataBind();

        ddl_supplier_edit.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
        ddl_supplier_edit.DataValueField = "supplier_code";
        ddl_supplier_edit.DataTextField = "showname";
        ddl_supplier_edit.DataBind();

        if (ddl_station.Items.Count > 0) ddl_station.SelectedIndex = 0;
        if (manager_type == "0" || manager_type == "1") ddl_station.Items.Insert(0, new ListItem("全部", ""));


        ddl_active.Items.Clear();
        ddl_active.Items.Insert(0, new ListItem("未生效", "0"));
        ddl_active.Items.Insert(0, new ListItem("生效", "1"));
        ddl_active.Items.Insert(0, new ListItem("全部", ""));

        lbstation.Visible = Less_than_truckload == "1";
        ddl_staion.Visible = Less_than_truckload == "1";
        checkout_vendor_label.Visible = Less_than_truckload != "1";
        checkout_vendor.Visible = Less_than_truckload != "1";


        #region 到著站簡碼
        ddl_staion.DataSource = Utility.getArea_Arrive_Code_For_Member(true);
        ddl_staion.DataValueField = "station_scode";
        ddl_staion.DataTextField = "showsname";
        ddl_staion.DataBind();
        ddl_staion.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

        #region 到結帳區配商
        checkout_vendor.DataSource = Utility.getSupplierDT(supplier_code, manager_type);
        checkout_vendor.DataValueField = "supplier_code";
        checkout_vendor.DataTextField = "showname";
        checkout_vendor.DataBind();
        checkout_vendor.Items.Insert(0, new ListItem("請選擇", ""));
        #endregion

    }


    protected void DefaultData()
    {

        string strSQL1 = string.Empty;
        ddl_active.SelectedValue = "1";


        //區配商

        manager_type = Session["manager_type"].ToString(); //管理單位類別   
        string station = Session["management"].ToString();
        string station_level = Session["station_level"].ToString();
        string station_area = Session["station_area"].ToString();
        string station_scode = Session["station_scode"].ToString();
        switch (manager_type)
        {
            case "0":
            case "1":
            case "2":
                station_scode = "";
                //一般員工/管理者
                //有綁定站所只該該站所客戶
                //無則可查所有站所下的所有客戶
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@management", Session["management"].ToString());
                    cmd.Parameters.AddWithValue("@station_area", Session["station_area"].ToString());
                    cmd.Parameters.AddWithValue("@station_scode", Session["station_scode"].ToString());

                    if (station_level.Equals("1"))
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where management = @management ";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                station_scode += "'";
                                station_scode += dt.Rows[i]["station_scode"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                            station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                        }
                    }
                    else if (station_level.Equals("2"))
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where station_scode = @station_scode ";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                station_scode += "'";
                                station_scode += dt.Rows[i]["station_scode"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                            station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                        }
                    }
                    else if (station_level.Equals("4"))
                    {
                        cmd.CommandText = @" select * from tbStation
                                          where station_area = @station_area ";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                station_scode += "'";
                                station_scode += dt.Rows[i]["station_scode"].ToString();
                                station_scode += "'";
                                station_scode += ",";
                            }
                            station_scode = station_scode.Remove(station_scode.ToString().LastIndexOf(','), 1);
                        }
                    }
                    else if (station_level.Equals("5"))
                    {

                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            station_scode += "'";
                            station_scode += dt.Rows[0]["station_code"].ToString();
                            station_scode += "'";
                            station_scode += ",";
                        }
                    }
                    else
                    {


                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            station_scode = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                }
                break;
            case "4":                 //站所主管只能看該站所下的客戶
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                    cmd.CommandText = @"select station_scode  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                    DataTable dt = dbAdapter.getDataTable(cmd);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        station_scode = dt.Rows[0]["station_scode"].ToString();
                    }
                }
                break;
            case "5":
                station_scode = Session["customer_code"].ToString();   //客戶只能看到自已
                station_scode = (station_scode.Length >= 3) ? station_scode.Substring(0, 3) : "";
                break;
        }
        string wherestr = "";

        SqlCommand cmd1 = new SqlCommand();

        if (station_scode != "")
        {
            if (station_level.Equals("1") || station_level.Equals("2") || station_level.Equals("4"))
            {
                cmd1.Parameters.AddWithValue("@station_scode", station_scode);
                wherestr = " and station_scode in (" + @station_scode + ")";
            }

        }

        cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0} and station_code <>'F53' and station_code <>'F71'
                                               order by station_code", wherestr);

        ddl_station.DataSource = dbAdapter.getDataTable(cmd1);
        ddl_station.DataValueField = "station_scode";
        ddl_station.DataTextField = "showname";
        ddl_station.DataBind();
        if (manager_type == "0" || manager_type == "1" || manager_type == "2")
        {
            ddl_station.Items.Insert(0, new ListItem("全部", ""));
        }
        else if (station_scode == "" && manager_type == "1")
        {
            ddl_station.Items.Insert(0, new ListItem("全部", ""));
        }
        else if (station_scode == "" && manager_type == "2")
        {
            ddl_station.Items.Insert(0, new ListItem("全部", ""));
        }
        if (ddl_station.Items.Count > 0) ddl_station.SelectedIndex = 0;


        String strSQL = string.Empty;
        string strWhere = "";
        using (SqlCommand cmd = new SqlCommand())
        {
            #region Set QueryString

            string querystring = "";
            //if (Request.QueryString["supplier"] != null)
            //{
            //    ddl_supplier.SelectedValue = Request.QueryString["supplier"];
            //    querystring += "&supplier=" + ddl_supplier.SelectedValue;
            //}
            if (Request.QueryString["station_scode"] != null)
            {
                ddl_station.SelectedValue = Request.QueryString["station_scode"];
                querystring += "&station_scode=" + ddl_station.SelectedValue;
            }
            if (Request.QueryString["active"] != null)
            {
                ddl_active.SelectedValue = Request.QueryString["active"];
                querystring += "&active=" + ddl_active.SelectedValue;
            }
            if (Request.QueryString["keyword"] != null)
            {
                tb_search.Text = Request.QueryString["keyword"];
                querystring += "&keyword=" + tb_search.Text;
            }


            #endregion


            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dri.driver_id) AS num
                             ,driver_id,dri.driver_code,dri.supplier_code
                             ,sup.supplier_name,driver_name,driver_mobile,dri.emp_code,driver_type , driver_type_code
                             ,CASE dri.active_flag WHEN '1' THEN '是' ELSE '否' END IsActive
                             ,CASE WHEN ISNULL(dri.login_date,'')='' THEN '' ELSE CONVERT(VARCHAR,dri.login_date,111)END login_date
                             ,CASE WHEN dri.supplier_code IN('000') THEN '0' ELSE '1' END suptype,ISNULL(wgs_x,'') AS wgs_x,ISNULL(wgs_y,'') AS wgs_y
                             ,D.user_name , dri.udate, dri.checkout_vendor
                        FROM tbDrivers dri With(Nolock)
                        LEFT JOIN　tbSuppliers sup With(Nolock) ON dri.supplier_code = sup.supplier_code AND sup.active_flag=1
                        LEFT JOIN tbAccounts  D With(Nolock) on D.account_code = dri.uuser
                        WHERE 1=1";

            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND (dri.driver_code LIKE N'%'+@search+'%' OR driver_name LIKE N'%'+@search+'%' )";
                cmd.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }

            //if (ddl_supplier.SelectedValue != "")
            //{
            //    strWhere += " AND dri.supplier_code IN ('" + ddl_supplier.SelectedValue.ToString() + "')";
            //}
            if (ddl_station.SelectedValue != "")
            {
                strWhere += " AND dri.station IN ('" + ddl_station.SelectedValue.ToString() + "')";
            }
            else
            {
                if (station_scode.Equals(""))
                {

                }
                else
                {
                    if (station_level.Equals("1")|| station_level.Equals("2")|| station_level.Equals("4"))
                    {
                        strWhere += " AND dri.station IN (" + station_scode + ")";
                    }
                    
                }
            }

            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND dri.active_flag IN ('" + ddl_active.SelectedValue.ToString() + "')";
            }
            //if (supplier_code != "")
            //{
            //    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
            //    strWhere = " AND dri.supplier_code LIKE N''+@supplier_code+'%'";
            //}
            #endregion

            cmd.CommandText = string.Format(strSQL + " {0} order by dri.driver_id", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                PagedDataSource pagedData = new PagedDataSource();
                pagedData.DataSource = dt.DefaultView;
                pagedData.AllowPaging = true;
                pagedData.PageSize = 10;

                #region 下方分頁顯示
                //一頁幾筆
                int CurPage = 0;
                if ((Request.QueryString["Page"] != null))
                {
                    //控制接收的分頁並檢查是否在範圍
                    if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
                    {
                        if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                        {
                            CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                        }
                        else
                        {
                            CurPage = 1;
                        }
                    }
                    else
                    {
                        CurPage = 1;
                    }
                }
                else
                {
                    CurPage = 1;
                }
                pagedData.CurrentPageIndex = (CurPage - 1);
                lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
                lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)).ToString() + querystring));
                //第一頁控制
                if (!pagedData.IsFirstPage)
                {
                    lnkfirst.Enabled = true;
                    lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
                }
                else
                {
                    lnkfirst.Enabled = false;
                }
                //最後一頁控制
                if (!pagedData.IsLastPage)
                {
                    lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
                    lnklast.Enabled = true;
                }
                else
                {
                    lnklast.Enabled = false;
                }

                //上一頁控制
                if (CurPage - 1 == 0)
                {
                    lnkPrev.Enabled = false;
                }
                else
                {
                    lnkPrev.Enabled = true;
                }

                //下一頁控制
                if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(dt.Rows.Count) / Convert.ToDouble(pagedData.PageSize)))
                {
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkNext.Enabled = true;
                }

                if (pagedData.PageSize > 0)
                {
                    tbPage.Text = CurPage.ToString() + "／" + pagedData.PageCount.ToString();
                }

                #endregion

                list_customer.DataSource = pagedData;
                list_customer.DataBind();
                ltotalpages.Text = dt.Rows.Count.ToString();//總筆數
            }
        }
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        String str_Query = "station_scode=" + ddl_station.SelectedValue
                      + "&active=" + ddl_active.SelectedValue;

        if (!string.IsNullOrEmpty(tb_search.Text))
        {
            str_Query += "&keyword=" + tb_search.Text;
        }

        Response.Redirect(ResolveUrl("~/system_7.aspx?" + str_Query));
    }


    protected void list_customer_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("emp_id")).Value.ToString(), out i_id)) i_id = 0;

                SetEditOrAdd(2, i_id);

                break;
            default:
                break;
        }
    }


    /// <summary>切換頁面狀態 </summary>
    /// <param name="type">0:檢示(預設) 1:新增 2:編輯 </param>
    /// <param name="id">若為編輯的狀態，需帶入tbEmps.emp_id</param>
    protected void SetEditOrAdd(int type = 0, int id = 0)
    {
        //int i_tmp = 0;
        Boolean bool_tmp = true;
        DateTime dt_tmp = new DateTime();
        //Boolean IsJunFu = false;//是否為峻富員工
        switch (type)
        {
            case 1:
                #region 新增               
                lbl_title.Text = "司機基本資料-新增";
                pan_edit.Visible = true;
                pan_view.Visible =
                emp_code.Visible =
                lbl_emp_code.Visible = false;
                lbl_id.Text = "0";

                driver_code.Text =
                driver_name.Text =
                driver_mobile.Text =
                emp_code.Text =
                lbl_app_login_date.Text = "";
                lbl_app_login_date.Visible = false;
                rb_Active.SelectedValue = "1";
                //ddl_supplier_edit.SelectedValue = "";
                ddl_supplier_edit.Enabled = true;
                driver_code.Enabled = true;
                ddl_supplier_edit_SelectedIndexChanged(null, null);
                rb_external_driver.SelectedValue = "0";
                ddl_staion.SelectedValue = "";
                checkout_vendor.Enabled = true;
                checkout_vendor.SelectedValue = "";
                #endregion

                break;
            case 2:
                #region 編輯
                if (id > 0)
                {
                    lbl_title.Text = "司機基本資料-編輯";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @"SELECT dri.driver_id,dri.driver_code,dri.supplier_code
                                                ,sup.supplier_name,driver_mobile,dri.emp_code
                                                ,dri.active_flag
                                                ,dri.external_driver 
                                                ,dri.login_date
                                                ,CASE WHEN  driver_name !='' THEN driver_name ELSE  emp.emp_name END driver_name 
                                                ,CASE WHEN dri.supplier_code IN('000') THEN '0' ELSE '1' END suptype
                                                ,dri.station , dri.office, dri.checkout_vendor,dri.driver_name
                                        FROM tbDrivers dri With(Nolock)
                                        LEFT JOIN　tbSuppliers sup With(Nolock) ON dri.supplier_code = sup.supplier_code AND sup.active_flag=1  
                                        LEFT JOIN  tbEmps emp With(Nolock) ON dri.emp_code=emp.emp_code 
                                      WHERE dri.driver_id = @driver_id";



                        cmd.Parameters.AddWithValue("@driver_id", id.ToString());
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {
                                DataRow row = dt.Rows[0];
                                driver_code.Text = row["driver_code"].ToString();

                                ddl_supplier_edit.SelectedValue = row["supplier_code"].ToString();

                                checkout_vendor.SelectedValue = row["checkout_vendor"].ToString();
                                //only峻富→顯示員工代碼資訊

                                //suptype:0 峻富 ,1:非峻富
                                //if (!int.TryParse(row["suptype"].ToString(), out i_tmp)) i_tmp = 1;
                                //IsJunFu = i_tmp == 0 ? true : false;

                                //if(IsJunFu)
                                //{
                                //    emp_code.Text = row["emp_code"].ToString();
                                //    lbl_emp_code.Visible = true;
                                //    SetJunFudriver();
                                //    //ddl_driver_name
                                //}
                                //else
                                //{
                                //    emp_code.Text = "";
                                //    lbl_emp_code.Visible = false;
                                //    //driver_name
                                //}

                                emp_code.Text = row["emp_code"].ToString();
                                lbl_emp_code.Visible = true;
                                SetJunFudriver();
                                if (emp_code.Text == "")
                                {
                                    ddl_driver_name.Visible = true;
                                    driver_name.Visible = false;
                                }
                                else
                                {
                                    ddl_driver_name.Visible = false;
                                    driver_name.Visible = true;

                                }
                                driver_name.Text = row["driver_name"].ToString();
                                //ddl_driver_name.Text = row["driver_name"].ToString();
                                driver_mobile.Text = row["driver_mobile"].ToString();
                                checkout_vendor.SelectedValue = row["checkout_vendor"].ToString();

                                lbl_app_login_date.Text = "";
                                lbl_app_login_date.Visible = false;
                                if (DateTime.TryParse(row["login_date"].ToString(), out dt_tmp))
                                {
                                    lbl_app_login_date.Text = "※APP登入時間: " + dt_tmp.ToString("yyyy /MM/dd HH:mm:SS");
                                    lbl_app_login_date.Visible = true;
                                }

                                if (!Boolean.TryParse(row["active_flag"].ToString(), out bool_tmp)) bool_tmp = true;
                                rb_Active.SelectedValue = bool_tmp ? "1" : "0";

                                if (!Boolean.TryParse(row["external_driver"].ToString(), out bool_tmp)) bool_tmp = false;
                                rb_external_driver.SelectedValue = bool_tmp ? "1" : "0";
                                ddl_staion.SelectedValue = row["station"].ToString();


                                lbl_id.Text = id.ToString();

                                //driver_name.Enabled = !IsJunFu;
                                driver_name.Enabled = false;
                            }
                        }
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        string strSQL = @"select driver_Type, driver_type_code from tbDrivers where driver_code = @driver_code";
                        cmd.Parameters.AddWithValue("@driver_code", driver_code.Text);
                        cmd.CommandText = strSQL;
                        using (DataTable dt = dbAdapter.getDataTable(cmd))
                        {
                            if (dt.Rows.Count > 0)
                            {

                                if (dt.Rows[0]["driver_Type"].ToString() == "SD")
                                {
                                    RadioButtonListSDMD.SelectedValue = "0";
                                    SDMDList.Items.Clear();
                                    using (SqlCommand cmd1 = new SqlCommand())
                                    {
                                        cmd1.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where emp_code = @driver_code";
                                        cmd1.Parameters.AddWithValue("@driver_code", driver_code.Text);
                                        DataTable dt1 = dbAdapter.getDataTable(cmd1);
                                        int temp_sd;
                                        if (int.TryParse(dt1.Rows[0]["sd_range_start"].ToString().Trim(), out temp_sd) & int.TryParse(dt1.Rows[0]["sd_range_end"].ToString().Trim(), out temp_sd))
                                        {
                                            int startSd = int.Parse(dt1.Rows[0]["sd_range_start"].ToString().Trim());
                                            int endSd = int.Parse(dt1.Rows[0]["sd_range_end"].ToString().Trim());
                                            for (var i = startSd; i < endSd + 1; i++)
                                            {
                                                SDMDList.Items.Add(i.ToString());
                                            }
                                            SDMDList.Items.Insert(0, "請選擇");
                                        }
                                        else
                                        {
                                            SDMDList.Items.Insert(0, "請至站所維護選取SD區間再回本頁面選取SD");
                                        }
                                    }
                                    if (dt.Rows[0]["driver_type_code"] != null)
                                    {
                                        SDMDList.SelectedValue = dt.Rows[0]["driver_type_code"].ToString();
                                    }
                                }
                                else if (dt.Rows[0]["driver_Type"].ToString() == "MD")
                                {
                                    RadioButtonListSDMD.SelectedValue = "1";
                                    SDMDList.Items.Clear();
                                    using (SqlCommand cmd2 = new SqlCommand())
                                    {
                                        cmd2.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where driver_code = @driver_code";
                                        cmd2.Parameters.AddWithValue("@driver_code", driver_code.Text);
                                        DataTable dt2 = dbAdapter.getDataTable(cmd2);
                                        int temp_md;
                                        if (int.TryParse(dt2.Rows[0]["md_range_start"].ToString().Trim(), out temp_md) & int.TryParse(dt2.Rows[0]["md_range_end"].ToString().Trim(), out temp_md))
                                        {
                                            int startMd = int.Parse(dt2.Rows[0]["md_range_start"].ToString().Trim());
                                            int endMd = int.Parse(dt2.Rows[0]["md_range_end"].ToString().Trim());
                                            for (var i = startMd; i < endMd + 1; i++)
                                            {
                                                SDMDList.Items.Add(i.ToString());
                                            }
                                            SDMDList.Items.Insert(0, "請選擇");
                                        }
                                        else
                                        {
                                            SDMDList.Items.Insert(0, "請至站所維護選取MD區間再回本頁面選取MD");
                                        }
                                        SDMDList.Visible = true;
                                        //RadioSDMDProgram();
                                        if (dt.Rows[0]["driver_type_code"] != null)
                                        {
                                            SDMDList.SelectedValue = dt.Rows[0]["driver_type_code"].ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    RadioButtonListSDMD.SelectedValue = null;
                                    RadioSDMDProgram();
                                    SDMDList.Visible = false;
                                }
                            }
                        }
                    }

                    ddl_supplier_edit.Enabled = false;
                    driver_code.Enabled = false;
                    pan_edit.Visible = true;
                    pan_view.Visible = false;
                }
                #endregion

                break;
            default:
                lbl_title.Text = "司機基本資料";
                pan_edit.Visible = false;
                pan_view.Visible = true;
                //DefaultData();
                break;
        }

    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(0);
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        Boolean IsEdit = false;
        Boolean IsOk = true;
        //Boolean IsJunFu = (ddl_supplier_edit.SelectedValue.Trim() == "000");//是否為峻富員工
        //Boolean IsJunFu_ed = false; //修改前，是否為峻富員工
        String driver_code_ed = ""; //driver_code(修改前)
        int i_id = 0;
        string strSQL = string.Empty;
        bool isSDMDCorrectly = false;           //判斷是否有同站所的重複SDMD編號


        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        if (i_id > 0 && lbl_title.Text.Contains("編輯")) IsEdit = true;

        if (IsEdit)
        {
            //strSQL = "SELECT Count(1) FROM tbDrivers With(Nolock) WHERE driver_id IN('" + i_id.ToString() + "') AND ISNULL(supplier_code,'')='000' ;";
            //if (GetValueFromSQL(strSQL) > 0) IsJunFu_ed = true;


            strSQL = "Select driver_code FROM tbDrivers WHERE driver_id ='" + i_id.ToString() + "';";
            driver_code_ed = GetValueStrFromSQL(strSQL);
        }

        #region 驗證        
        if (new string[] { "0", "" }.Contains(ddl_supplier_edit.SelectedValue.ToString()))
        {
            IsOk = false;
            RetuenMsg("請選擇所屬區配商!");
        }

        //司機代碼不可重覆        
        strSQL = "SELECT COUNT(1) FROM  tbDrivers With(Nolock) WHERE  driver_code IN('" + driver_code.Text.Trim() + "') AND supplier_code='" + ddl_supplier_edit.SelectedValue + "'";
        if (IsEdit) strSQL += " AND driver_id NOT IN('" + i_id.ToString() + "') ";

        if (IsOk && GetValueFromSQL(strSQL) > 0)
        {
            IsOk = false;
            RetuenMsg("司機代碼不可重覆,請重新確認!");
        }
        if (!IsOk) return;

        #endregion

        #region 存檔

        if (IsOk)
        {
            string strUser = string.Empty;
            if (Session["account_id"] != null) strUser = Session["account_id"].ToString();//tbAccounts.account_id
            string str_table = "tbDrivers";
            using (SqlCommand cmd = new SqlCommand())
            {
                using (SqlCommand cmd3 = new SqlCommand())
                {
                    string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                    string driverTypeCode = SDMDList.SelectedValue;
                    string driverType = "";
                    string driverStation = lbstation.Text;
                    string rb_Activevalue = rb_Active.SelectedValue;
                    if (rb_Activevalue == "1")
                    {
                        cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                        cmd3.Parameters.AddWithValue("@driverTypeCode", SDMDList.SelectedValue);
                        cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                        cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                        DataTable dt2 = dbAdapter.getDataTable(cmd3);

                        if (dt2.Rows.Count > 0)
                        {
                            string repeat_number = dt2.Rows[0]["driver_code"].ToString();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.location = window.location.href;alert( '不允許生效，SD、MD碼與同站所司機 " + repeat_number + " 重複' );</script>", false);
                            return;
                        }
                    }
                    else if (rb_Activevalue == "0")
                    {
                        cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                        cmd3.Parameters.AddWithValue("@driverTypeCode", SDMDList.SelectedValue);
                        cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                        cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                        DataTable dt2 = dbAdapter.getDataTable(cmd3);

                        if (dt2.Rows.Count > 0)
                        {
                            string repeat_number = dt2.Rows[0]["driver_code"].ToString();
                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.location = window.location.href;alert( '不允許生效，SD、MD碼與同站所司機 " + repeat_number + " 重複' );</script>", false);
                            return;
                        }

                    }

                }


                cmd.Parameters.AddWithValue("@driver_code", driver_code.Text.ToString());
                cmd.Parameters.AddWithValue("@supplier_code", ddl_supplier_edit.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@checkout_vendor", checkout_vendor.SelectedValue.ToString());
                if (driver_mobile.Text.Trim().Length > 0)
                {
                    cmd.Parameters.AddWithValue("@driver_mobile", driver_mobile.Text.ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@driver_mobile", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@external_driver", rb_external_driver.SelectedValue.ToString());
                if (Less_than_truckload == "1")
                {
                    cmd.Parameters.AddWithValue("@station", ddl_staion.SelectedValue.ToString());
                }

                //string manager_type = Session["manager_type"].ToString(); //管理單位類別  
                //string customer_code = Session["master_code"].ToString();
                //switch (manager_type)
                //{
                //    case "0":
                //    case "1":
                //    case "2":
                //        customer_code = "000000";
                //        break;
                //}

                string the_driver_name = GetValueStrFromSQL("SELECT emp_name FROM tbEmps WHERE emp_code ='" + emp_code.Text + "' and customer_code LIKE N'" + ddl_supplier_edit.SelectedValue + "%' ;");
                cmd.Parameters.AddWithValue("@driver_name", the_driver_name);
                cmd.Parameters.AddWithValue("@emp_code", emp_code.Text.ToString());

                ////IsJunFu: tbDrivers.emp_code 填值 ; !IsJunFu: tbDrivers.emp_code 清空
                //if (!IsJunFu)
                //{
                //    cmd.Parameters.AddWithValue("@driver_name", driver_name.Text.ToString());
                //    cmd.Parameters.AddWithValue("@emp_code", DBNull.Value);
                //}
                //else
                //{
                //    string the_driver_name = GetValueStrFromSQL("SELECT emp_name FROM tbEmps WHERE emp_code ='" + emp_code.Text + "';");

                //    cmd.Parameters.AddWithValue("@driver_name", the_driver_name);
                //    cmd.Parameters.AddWithValue("@emp_code", emp_code.Text.ToString());
                //}


                if (IsEdit)
                {
                    cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@uuser", strUser);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_id", i_id.ToString());
                    cmd.CommandText = dbAdapter.genUpdateComm(str_table, cmd);
                    dbAdapter.execNonQuery(cmd);
                    using (SqlCommand cmd3 = new SqlCommand())
                    {
                        string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                        string driverTypeCode = SDMDList.SelectedValue;
                        string driverType = "";
                        string driverStation = lbstation.Text;
                        int temp_;
                        if ((driverTypeValue == "0" || driverTypeValue == "1") & int.TryParse(driverTypeCode, out temp_))
                        {
                            if (driverTypeValue == "0")
                            {
                                driverType = "SD";
                            }
                            else if (driverTypeValue == "1")
                            {
                                driverType = "MD";
                            }
                            cmd3.CommandText = @"select * from tbDrivers where station = @driverStation and driver_code != @driverCode and driver_type_code = @driverTypeCode and active_flag = '1' ";
                            cmd3.Parameters.AddWithValue("@driverCode", driver_code.Text);
                            cmd3.Parameters.AddWithValue("@driverTypeCode", driverTypeCode);
                            cmd3.Parameters.AddWithValue("@driverStation", ddl_staion.SelectedItem.ToString().Split(' ')[0]);
                            DataTable dt = dbAdapter.getDataTable(cmd3);

                            if (dt.Rows.Count == 0)
                            {
                                isSDMDCorrectly = true;
                            }
                        }
                    }
                    using (SqlCommand cmd2 = new SqlCommand())
                    {
                        string driverTypeValue = RadioButtonListSDMD.SelectedValue;
                        string driverTypeCode = SDMDList.SelectedValue;
                        string driverType = "";
                        if (SDMDList.Visible != true)
                        {
                            cmd2.Parameters.AddWithValue("@driver_type", DBNull.Value);
                            cmd2.Parameters.AddWithValue("@driver_type_code", DBNull.Value);
                            cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.ToString().Trim());
                            cmd2.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmd2);
                            dbAdapter.execNonQuery(cmd2);
                        }
                        else if (isSDMDCorrectly)
                        {

                            int temp_;
                            if ((driverTypeValue == "0" || driverTypeValue == "1") & int.TryParse(driverTypeCode, out temp_))
                            {
                                if (driverTypeValue == "0")
                                {
                                    driverType = "SD";
                                }
                                else if (driverTypeValue == "1")
                                {
                                    driverType = "MD";
                                }
                                cmd2.Parameters.AddWithValue("@driver_type", driverType);
                                cmd2.Parameters.AddWithValue("@driver_type_code", driverTypeCode);
                                cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code.Text.ToString().Trim());
                                cmd2.CommandText = dbAdapter.genUpdateComm("tbDrivers", cmd2);
                                dbAdapter.execNonQuery(cmd2);
                            }
                        }
                    }
                    int resultType = -1;        //為了跨過SetEditOrAdd();
                    if (RadioButtonListSDMD.Visible & RadioButtonListSDMD.SelectedItem != null & SDMDList.Visible & SDMDList.SelectedIndex == 0)
                    {
                        resultType = 1;
                    }
                    else if (!isSDMDCorrectly & RadioButtonListSDMD.Visible)
                    {
                        resultType = 2;
                    }
                    else
                    {
                        resultType = 0;
                    }

                    SetEditOrAdd(0);

                    switch (resultType)
                    {
                        case 1:
                            RetuenMsg("未選擇SD、MD編號，不儲存SD、MD種類，其餘修改成功!");
                            break;
                        case 2:
                            RetuenMsg("不允許SD、MD與同站所司機重複，其餘修改成功!");
                            break;
                        case 0:
                            RetuenMsg("修改成功!");
                            break;
                    }

                    ////IsJunFu→!IsJunFu: tbEmps.driver_code 司機代碼清空
                    //if (IsJunFu_ed && !IsJunFu)
                    //{
                    //    if (driver_code_ed.Length > 0)
                    //    {
                    //        cmd.Parameters.Clear();
                    //        cmd.Parameters.AddWithValue("@driver_code", DBNull.Value);
                    //        cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    //        cmd.Parameters.AddWithValue("@uuser", strUser);
                    //        cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "driver_code", driver_code_ed);
                    //        cmd.CommandText = dbAdapter.genUpdateComm("tbEmps", cmd);
                    //        dbAdapter.execNonQuery(cmd);
                    //    }
                    //}
                }
                else
                {
                    String password = Utility.GetMd5Hash(MD5.Create(), "0000");
                    string driver_type = "";
                    cmd.Parameters.AddWithValue("@login_password", password);
                    cmd.Parameters.AddWithValue("@cdate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.Parameters.AddWithValue("@cuser", strUser);
                    if (!string.IsNullOrEmpty(SDMDList.SelectedValue))
                    {
                        if (RadioButtonListSDMD.SelectedValue == "0") { driver_type = "SD"; }
                        else if (RadioButtonListSDMD.SelectedValue == "1") { driver_type = "MD"; }
                        cmd.Parameters.AddWithValue("@driver_type", driver_type);
                        cmd.Parameters.AddWithValue("@driver_type_code", SDMDList.SelectedValue);

                    }
                    else
                    {
                        RetuenMsg("請選擇SDMD號碼");
                        return;
                    }

                    cmd.CommandText = dbAdapter.genInsertComm(str_table, true, cmd);
                    int result = 0;
                    if (!int.TryParse(dbAdapter.getDataTable(cmd).Rows[0][0].ToString(), out result)) result = 0;



                }

                //IsJunFu:tbDrivers.active_flag同步寫入tbEmps.active_flag + tbDrivers.driver_code 同步寫入tbEmps.driver_code
                //if (IsJunFu)
                //{
                strSQL = "SELECT emp_id FROM tbEmps With(Nolock) WHERE emp_code IN ('" + emp_code.Text.ToString() + "') and customer_code LIKE N'" + ddl_supplier_edit.SelectedValue + "%'";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@driver_code", driver_code.Text.ToString());
                cmd.Parameters.AddWithValue("@active_flag", rb_Active.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@udate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.Parameters.AddWithValue("@uuser", strUser);
                cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "emp_id", GetValueFromSQL(strSQL).ToString());
                cmd.CommandText = dbAdapter.genUpdateComm("tbEmps", cmd);
                dbAdapter.execNonQuery(cmd);
                //}

                SetEditOrAdd(0);
                RetuenMsg(IsEdit ? "修改成功!" : "新增成功!");
            }
        }
        #endregion
    }

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        SetEditOrAdd(1);
    }

    protected int GetValueFromSQL(string strSQL)
    {
        int result = 0;
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (!int.TryParse(dt.Rows[0][0].ToString(), out result)) result = 0;
                    }
                }
            }

        }

        return result;
    }

    protected String GetValueStrFromSQL(string strSQL)
    {
        String result = "";
        if (strSQL.Trim() != "")
        {

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSQL;
                using (DataTable dt = dbAdapter.getDataTable(cmd))
                {
                    if (dt.Rows.Count > 0)
                    {
                        result = dt.Rows[0][0].ToString();
                    }
                }
            }

        }

        return result;
    }

    protected void RetuenMsg(string strMsg)
    {
        if (!String.IsNullOrEmpty(strMsg))
        {
            //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='" + Request.QueryString["src"].ToString() + "';alert('" + strMsg + "');</script>", false);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + strMsg + "');</script>", false);
            return;

        }
    }

    protected void ddl_supplier_edit_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i_id = 0;
        if (!int.TryParse(lbl_id.Text, out i_id)) i_id = 0;
        //Boolean IsJunFu = (ddl_supplier_edit.SelectedValue.Trim() == "000");//是否為峻富員工
        Boolean IsEdit = (i_id > 0 && lbl_title.Text.Contains("編輯")) ? true : false;
        Boolean IsIndexSupplier = false;
        //int endIndex = ddl_supplier_edit.SelectedItem.Text.IndexOf("(");
        //string str_supplier = ddl_supplier_edit.SelectedValue == "" ? "" : ddl_supplier_edit.SelectedItem.Text.Substring(0, endIndex);
        string str_supplier = ddl_supplier_edit.SelectedValue == "" ? "" : ddl_supplier_edit.SelectedItem.Text.Split('-')[1];

        //string str_supplier = "";
        //if (ddl_supplier_edit.SelectedValue != "" && endIndex >= 0)
        //{
        //    str_supplier = ddl_supplier_edit.SelectedItem.Text.Substring(0, endIndex);
        //}

        IsIndexSupplier = driver_name.Text.Contains(str_supplier);

        //driver_name.Enabled = !IsJunFu;
        //ddl_driver_name.Enabled = IsJunFu;
        //新增
        if (!IsEdit)
        {
            driver_name.Visible = true;
            ddl_driver_name.Visible = false;
        }

        if (SetJunFudriver())
        {
            if (!IsEdit)
            {
                if (!IsIndexSupplier && Less_than_truckload == "0")
                {
                    driver_name.Text = str_supplier + " " + driver_name.Text;
                }
            }
            emp_code.Visible =
            lbl_emp_code.Visible =
            ddl_driver_name.Visible = true;
            driver_name.Visible = false;
        }
        else
        {
            driver_name.Text = "";
            //ddl_supplier_edit.SelectedValue = "";
            driver_name.Enabled = true;
            RetuenMsg("員工皆已設定，目前無可設定資料!");
        }

        ////非峻富時，預設代區配商data
        //if (!IsJunFu)
        //{
        //    emp_code.Visible =
        //    lbl_emp_code.Visible = false;
        //    if (!IsIndexSupplier) driver_name.Text = str_supplier + " ";
        //}
        //else
        //{
        //    if (SetJunFudriver())
        //    {
        //        if (!IsEdit)
        //        {
        //            if (!IsIndexSupplier) driver_name.Text = str_supplier + " " + driver_name.Text;
        //        }
        //        emp_code.Visible =
        //        lbl_emp_code.Visible =
        //        ddl_driver_name.Visible = true;
        //        driver_name.Visible = false;
        //    }
        //    else
        //    {
        //        driver_name.Text =
        //        ddl_supplier_edit.SelectedValue = "";
        //        driver_name.Enabled = true;
        //        RetuenMsg("峻富員工皆已設定，目前無可設定資料!");
        //    }
        //}


        // if (!IsIndexSupplier) driver_name.Text = str_supplier + " " + driver_name.Text;


    }

    /// <summary>設定員工尚未設成司機的資料 </summary>
    /// <returns>true:設定成功 false:無未設定資料</returns>
    protected Boolean SetJunFudriver()
    {
        //string manager_type = Session["manager_type"].ToString(); //管理單位類別  
        //string customer_code = Session["master_code"].ToString();
        //switch (manager_type)
        //{
        //    case "0":
        //    case "1":
        //    case "2":
        //        customer_code = "000000";
        //        break;
        //}

        Boolean IsOk = false;
        ddl_driver_name.Items.Clear();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@customer_code", ddl_supplier_edit.SelectedValue);
            String strSQL = @"SELECT emp.emp_code 'id',emp.emp_name +'('+ emp.emp_code+')' 'name'
                            FROM tbEmps emp With(Nolock)
                           WHERE ISNULL(emp.driver_code,'')='' 
                            AND emp.emp_code NOT IN(SELECT emp_code FROM tbDrivers dri With(Nolock) WHERE dri.active_flag=1 and dri.emp_code is not null) 
                            AND emp.active_flag IN(1) 
                            AND emp.customer_code LIKE N''+@customer_code+'%'
                            order by emp.emp_code";
            cmd.CommandText = strSQL;
            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                if (dt.Rows.Count > 0)
                {
                    IsOk = true;
                    ddl_driver_name.DataSource = dt;
                    ddl_driver_name.DataValueField = "id";
                    ddl_driver_name.DataTextField = "name";
                    ddl_driver_name.DataBind();
                    ddl_driver_name.Items.Insert(0, new ListItem("請選擇", ""));
                }
            }
        }
        return IsOk;
    }


    protected void ddl_driver_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        emp_code.Text = ddl_driver_name.SelectedValue;

        using (SqlCommand cmd3 = new SqlCommand())
        {
            if (emp_code.Text != "")
            {
                cmd3.CommandText = @"select station_scode,* from  tbEmps emp left join tbStation sta with(nolock) on emp.station  = sta.id where emp_code = @emp_code ";
                cmd3.Parameters.AddWithValue("@emp_code", emp_code.Text);
                DataTable dt = dbAdapter.getDataTable(cmd3);
                ddl_staion.SelectedValue = dt.Rows[0]["station_scode"].ToString();
            }
        }
    }

    protected void btExport_Click1(object sender, EventArgs e)
    {
        string sheet_title = "司機基本資料";
        string file_name = "司機基本資料" + DateTime.Now.ToString("yyyyMMdd");
        String strSQL = string.Empty;
        string strWhere = "";
        using (SqlCommand objCommand = new SqlCommand())
        {
            #region Set QueryString

            //string querystring = "";
            //if (Request.QueryString["supplier"] != null)
            //{
            //    ddl_supplier.SelectedValue = Request.QueryString["supplier"];
            //    querystring += "&supplier=" + ddl_supplier.SelectedValue;
            //}
            //if (Request.QueryString["active"] != null)
            //{
            //    ddl_active.SelectedValue = Request.QueryString["active"];
            //    querystring += "&active=" + ddl_active.SelectedValue;
            //}
            //if (Request.QueryString["keyword"] != null)
            //{
            //    tb_search.Text = Request.QueryString["keyword"];
            //    querystring += "&keyword=" + tb_search.Text;
            //}


            #endregion


            strSQL = @"SELECT ROW_NUMBER() OVER(ORDER BY dri.driver_id) AS 'NO',s.station_code+s.station_name '站所'
                             ,dri.driver_code AS '司機代碼',driver_name AS '司機姓名'
                             ,sup.supplier_name AS '所屬配商',driver_mobile AS '手機',dri.emp_code AS '員工編號',driver_type '類型', driver_type_code '編碼'
                             ,CASE WHEN ISNULL(dri.login_date,'')='' THEN '' ELSE CONVERT(VARCHAR,dri.login_date,111)END  AS '登入時間'
                             ,CASE dri.active_flag WHEN '1' THEN '是' ELSE '否' END  AS '是否生效'                             
                             ,ISNULL(wgs_x,'') AS '經度',ISNULL(wgs_y,'') AS '緯度'
                             ,dri.checkout_vendor AS '結帳區配商'
                        FROM tbDrivers dri With(Nolock)
                        LEFT JOIN　tbSuppliers sup With(Nolock) ON dri.supplier_code = sup.supplier_code AND sup.active_flag=1
                        left join tbStation s With(Nolock) on dri.station=s.station_scode
                        WHERE 1=1";

            #region 查詢條件
            if (tb_search.Text.Trim().Length > 0)
            {
                strWhere += " AND dri.driver_code LIKE N'%'+@search+'%' OR driver_name LIKE N'%'+@search+'%' ";
                objCommand.Parameters.AddWithValue("@search", tb_search.Text.Trim());
            }

            if (ddl_station.SelectedValue != "")
            {
                strWhere += " AND dri.station IN ('" + ddl_station.SelectedValue.ToString() + "')";
            }

            if (ddl_active.SelectedValue != "")
            {
                strWhere += " AND dri.active_flag IN ('" + ddl_active.SelectedValue.ToString() + "')";
            }
            //if (supplier_code != "")
            //{
            //    cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
            //    strWhere = " AND dri.supplier_code LIKE N''+@supplier_code+'%'";
            //}
            #endregion

            objCommand.CommandText = string.Format(strSQL + " {0} order by dri.driver_id", strWhere);

            using (DataTable dt = dbAdapter.getDataTable(objCommand))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    using (ExcelPackage p = new ExcelPackage())
                    {
                        p.Workbook.Properties.Title = sheet_title;
                        p.Workbook.Worksheets.Add(sheet_title);
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        int colIndex = 1;
                        int rowIndex = 1;
                        foreach (DataColumn dc in dt.Columns)
                        {
                            var cell = ws.Cells[rowIndex, colIndex];
                            var fill = cell.Style.Fill;
                            fill.PatternType = ExcelFillStyle.Solid;
                            fill.BackgroundColor.SetColor(Color.LightGray);

                            //Setting Top/left,right/bottom borders.
                            var border = cell.Style.Border;
                            border.Bottom.Style =
                                border.Top.Style =
                                border.Left.Style =
                                border.Right.Style = ExcelBorderStyle.Thin;

                            //Setting Value in cell
                            cell.Value = dc.ColumnName;
                            colIndex++;
                        }
                        rowIndex++;

                        using (ExcelRange col = ws.Cells[2, 22, 2 + dt.Rows.Count, 23])
                        {
                            col.Style.Numberformat.Format = "yyyy/MM/dd";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }


                        foreach (DataRow dr in dt.Rows)
                        {
                            colIndex = 1;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                var cell = ws.Cells[rowIndex, colIndex];
                                cell.Value = dr[dc.ColumnName];

                                //Setting borders of cell
                                var border = cell.Style.Border;
                                border.Left.Style =
                                    border.Right.Style = ExcelBorderStyle.Thin;
                                colIndex++;
                            }

                            rowIndex++;
                        }
                        ws.Cells.AutoFitColumns();
                        ws.View.FreezePanes(2, 1);
                        ws.Cells.Style.Font.Size = 12;
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;  filename=" + file_name + ".xlsx");
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.BinaryWrite(p.GetAsByteArray());
                        Response.End();

                        //str_retuenMsg += "下載完成!";
                    }

                }
                //else
                //{
                //    str_retuenMsg += "查無此資訊，請重新確認!";
                //}
            }

        }
    }

    protected void ddl_staion_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void RadioSDMD(object sender, EventArgs e)
    {
        RadioSDMDProgram();
    }

    protected void RadioSDMDProgram()
    {
        string station_scode = ddl_staion.SelectedValue;
        SDMDList.Visible = true;
        SqlCommand cmd1 = new SqlCommand();
        SqlCommand cmd = new SqlCommand();

        string driverCode = driver_code.Text;
        string driverType = RadioButtonListSDMD.SelectedValue;
        if (!string.IsNullOrEmpty(ddl_staion.SelectedValue))
        {


            if (driverType == "0")                                  //0:SD,1:MD
            {
                SDMDList.Items.Clear();
                cmd1.CommandText = "select * from tbStation where station_scode = @station_scode";
                cmd1.Parameters.AddWithValue("@station_scode", station_scode);
                DataTable dt1 = dbAdapter.getDataTable(cmd1);
                int temp_sd;
                if (int.TryParse(dt1.Rows[0]["sd_range_start"].ToString().Trim(), out temp_sd) & int.TryParse(dt1.Rows[0]["sd_range_end"].ToString().Trim(), out temp_sd))
                {
                    int startSd = int.Parse(dt1.Rows[0]["sd_range_start"].ToString().Trim());
                    int endSd = int.Parse(dt1.Rows[0]["sd_range_end"].ToString().Trim());
                    for (var i = startSd; i < endSd + 1; i++)
                    {
                        SDMDList.Items.Add(i.ToString());
                    }
                    SDMDList.Items.Insert(0, "請選擇");
                }
                else
                {
                    SDMDList.Items.Insert(0, "請至站所維護選取SD區間再回本頁面選取SD");
                }
            }
            else if (driverType == "1")
            {
                SDMDList.Items.Clear();
                cmd1.CommandText = "select * from tbStation where station_scode = @station_scode";
                cmd1.Parameters.AddWithValue("@station_scode", station_scode);
                DataTable dt1 = dbAdapter.getDataTable(cmd1);
                int temp_md;
                if (int.TryParse(dt1.Rows[0]["md_range_start"].ToString().Trim(), out temp_md) & int.TryParse(dt1.Rows[0]["md_range_end"].ToString().Trim(), out temp_md))
                {
                    int startMd = int.Parse(dt1.Rows[0]["md_range_start"].ToString().Trim());
                    int endMd = int.Parse(dt1.Rows[0]["md_range_end"].ToString().Trim());
                    for (var i = startMd; i < endMd + 1; i++)
                    {
                        SDMDList.Items.Add(i.ToString());
                    }
                    SDMDList.Items.Insert(0, "請選擇");
                }
                else
                {
                    SDMDList.Items.Insert(0, "請至站所維護選取MD區間再回本頁面選取MD");
                }
            }


        }
        else
        {

            if (driverType == "0")                                  //0:SD,1:MD
            {
                SDMDList.Items.Clear();
                cmd.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where emp_code = @driver_code";
                cmd.Parameters.AddWithValue("@driver_code", driverCode);
                DataTable dt = dbAdapter.getDataTable(cmd);
                int temp_sd;
                if (int.TryParse(dt.Rows[0]["sd_range_start"].ToString().Trim(), out temp_sd) & int.TryParse(dt.Rows[0]["sd_range_end"].ToString().Trim(), out temp_sd))
                {
                    int startSd = int.Parse(dt.Rows[0]["sd_range_start"].ToString().Trim());
                    int endSd = int.Parse(dt.Rows[0]["sd_range_end"].ToString().Trim());
                    for (var i = startSd; i < endSd + 1; i++)
                    {
                        SDMDList.Items.Add(i.ToString());
                    }
                    SDMDList.Items.Insert(0, "請選擇");
                }
                else
                {
                    SDMDList.Items.Insert(0, "請至站所維護選取SD區間再回本頁面選取SD");
                }
            }
            else if (driverType == "1")
            {
                SDMDList.Items.Clear();
                cmd.CommandText = "select * from tbStation right join tbEmps on tbEmps.station = tbStation.id where driver_code = @driver_code";
                cmd.Parameters.AddWithValue("@driver_code", driverCode);
                DataTable dt = dbAdapter.getDataTable(cmd);
                int temp_md;
                if (int.TryParse(dt.Rows[0]["md_range_start"].ToString().Trim(), out temp_md) & int.TryParse(dt.Rows[0]["md_range_end"].ToString().Trim(), out temp_md))
                {
                    int startMd = int.Parse(dt.Rows[0]["md_range_start"].ToString().Trim());
                    int endMd = int.Parse(dt.Rows[0]["md_range_end"].ToString().Trim());
                    for (var i = startMd; i < endMd + 1; i++)
                    {
                        SDMDList.Items.Add(i.ToString());
                    }
                    SDMDList.Items.Insert(0, "請選擇");
                }
                else
                {
                    SDMDList.Items.Insert(0, "請至站所維護選取MD區間再回本頁面選取MD");
                }
            }
        }
    }

    public string manager_type
    {
        // for權限
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string station_scode
    {
        // for權限
        get { return ViewState["station_scode"].ToString(); }
        set { ViewState["station_scode"] = value; }
    }


    //是否可編輯
    //private bool IsAllowAddAndEdit()   
    //{
    //    using (SqlCommand cmd = new SqlCommand())
    //    {
    //        cmd.CommandText = @"select * from tbFunctionsAuth where account_code = @account_code and insert_auth = 1 and modify_auth = 1   and func_code = 'S2'";
    //        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
    //        DataTable dt = dbAdapter.getDataTable(cmd);
    //        if (dt != null && dt.Rows.Count > 0)
    //            return true;
    //        else
    //            return false;
    //    }
    //}

}
