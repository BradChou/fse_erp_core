﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using NPOI.HSSF.UserModel;
using System.Text;
using System.Collections;
using BObject.Bobjects;
using NPOI.SS.UserModel;
using BarcodeLib;
using System.Drawing;

public partial class transport_1_3 : System.Web.UI.Page
{
    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }

    public string master_code
    {
        // for權限
        get { return ViewState["master_code"].ToString(); }
        set { ViewState["master_code"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            #region 客代編號(套權限)
            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            supplier_code = Session["master_code"].ToString();
            master_code = Session["master_code"].ToString();
            supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";   //null: 開發人員
            if (supplier_code == "999")                                                         //999 : 峻富總公司(管理者)
            {
                switch (manager_type)
                {
                    case "1":
                    case "2":
                        supplier_code = "";
                        break;
                    default:
                        supplier_code = "000";
                        break;
                }
            }
            using (SqlCommand cmd2 = new SqlCommand())
            {
                string wherestr = "";
                if (supplier_code != "")
                {
                    cmd2.Parameters.AddWithValue("@supplier_code", supplier_code);
                    wherestr = " and supplier_code = @supplier_code";
                }
                if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
                {
                    cmd2.Parameters.AddWithValue("@master_code", master_code);
                    wherestr += " and customer_code like @master_code+'%' ";
                }


                cmd2.CommandText = string.Format(@"Select customer_code , customer_code + '-' + customer_shortname as name  from tbCustomers with(Nolock) where 0=0 and stop_shipping_code = '0'  {0} order by customer_code asc ", wherestr);
                dlcustomer_code.DataSource = dbAdapter.getDataTable(cmd2);
                dlcustomer_code.DataValueField = "customer_code";
                dlcustomer_code.DataTextField = "name";
                dlcustomer_code.DataBind();
                dlcustomer_code.Items.Insert(0, new ListItem("請選擇客代編號", ""));
            }

            date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            #endregion

            readdata();
        }
    }

    private void readdata()
    {
        string wherestr = "";
        SqlCommand cmd = new SqlCommand();
        //if (dlcustomer_code.SelectedValue != "")
        //{
        //    cmd.Parameters.AddWithValue("@customer_code", dlcustomer_code.SelectedValue.ToString());
        //    wherestr = " and A.customer_code = @customer_code";
        //}

        if (supplier_code != "")
        {
            cmd.Parameters.AddWithValue("@supplier_code", supplier_code);
            wherestr += " and A.supplier_code = @supplier_code";
        }

        if ((master_code != "") && (manager_type == "3" || manager_type == "5"))
        {
            cmd.Parameters.AddWithValue("@master_code", master_code);
            wherestr += " and A.customer_code like @master_code+'%' ";
        }


        cmd.Parameters.AddWithValue("@cdate", DateTime.Today);
        wherestr += " and CONVERT(VARCHAR,A.cdate,111)  = CONVERT(VARCHAR,@cdate,111)";

        cmd.CommandText = string.Format(@"Select A.request_id, A.cdate, A.arrive_assign_date , A.supplier_date, A.print_date,A.print_flag, A.check_number ,A.receive_contact , 
                                          A.receive_tel1 , A.receive_tel1_ext , A.receive_city , A.receive_area , A.receive_address,
                                          A.pieces , A.plates , A.cbm ,
                                          B.code_name , A.customer_code , C.customer_shortname ,
                                          A.send_city , A.send_area , A.send_address ,
                                          A.arrive_to_pay_freight,A.collection_money,A.arrive_to_pay_append,A.invoice_desc,
                                          D.type  
                                          from tcDeliveryRequests A
                                          left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                          left join tbCustomers C on C.customer_code = A.customer_code
                                          left join tbIORecords D on D.randomCode = A.import_randomCode
                                          where 1= 1 and A.import_randomCode <> '' and D.type = 1  {0} order by  A.cdate desc", wherestr);   //and A.check_number =''
        DataTable dt = dbAdapter.getDataTable(cmd);
        //New_List.DataSource = dt;
        //New_List.DataBind();
        //ltotalpages.Text = dt.Rows.Count.ToString();
    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        string ttFilePath = Request.PhysicalApplicationPath + "\\report\\";
        string ttFileName = "空白託運單標準格式.xls";
        //Response.Clear();
        //Response.AddHeader("content-disposition", "attachment;  filename=" + ttFilePath + ttFileName);
        //Response.ContentType = "application/vnd.ms-excel";
        ////Response.BinaryWrite(p.GetAsByteArray());
        //Response.End();

        FileInfo xpath_file = new FileInfo(ttFilePath + ttFileName);
        // 將傳入的檔名以 FileInfo 來進行解析（只以字串無法做）
        System.Web.HttpContext.Current.Response.Clear(); //清除buffer
        System.Web.HttpContext.Current.Response.ClearHeaders(); //清除 buffer 表頭
        System.Web.HttpContext.Current.Response.Buffer = false;
        System.Web.HttpContext.Current.Response.ContentType = "application/octet-stream";
        // 檔案類型還有下列幾種"application/pdf"、"application/vnd.ms-excel"、"text/xml"、"text/HTML"、"image/JPEG"、"image/GIF"
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode("空白託運單標準格式.xls", System.Text.Encoding.UTF8));
        // 考慮 utf-8 檔名問題，以 out_file 設定另存的檔名
        System.Web.HttpContext.Current.Response.AppendHeader("Content-Length", xpath_file.Length.ToString()); //表頭加入檔案大小
        System.Web.HttpContext.Current.Response.WriteFile(xpath_file.FullName);

        // 將檔案輸出
        System.Web.HttpContext.Current.Response.Flush();
        // 強制 Flush buffer 內容
        System.Web.HttpContext.Current.Response.End();


    }

    protected void btImport_Click(object sender, EventArgs e)
    {
        string ttErrStr = "";
        string customer_code = dlcustomer_code.SelectedValue;
        string supplier_code = "";
        string supplier_name = "";
        string send_contact = "";
        string send_city = "";
        string send_area = "";
        string send_address = "";
        string send_tel = "";
        string uniform_numbers = "";
        string pricing_type = "";
        SortedList List = new SortedList();
        string randomCode = Guid.NewGuid().ToString().Replace("-", "");
        randomCode = randomCode.Length > 10 ? randomCode.Substring(0, 10) : randomCode;
        DateTime? startTime;
        DateTime? endTime;
        int successNum = 0;
        int failNum = 0;
        int totalNum = 0;
        int supplier_fee = 0;  //配送費用
        int cscetion_fee = 0;  //C配運價
        int status_code = 0;   //執行結果代碼 (0:未執行1:正常-1:錯誤)
        int remote_fee = 0;
        DateTime print_date = new DateTime();
        DateTime supplier_date = new DateTime();
        DateTime default_supplier_date = new DateTime();

        if (!DateTime.TryParse(date.Text, out print_date)) print_date = DateTime.Now;
        default_supplier_date = print_date;
        int Week = (int)print_date.DayOfWeek;

        //配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
        //switch (Week)
        //{
        //    case 5:
        //        default_supplier_date = default_supplier_date.AddDays(3);
        //        break;
        //    case 6:
        //        default_supplier_date = default_supplier_date.AddDays(2);
        //        break;
        //    default:
        //        default_supplier_date = default_supplier_date.AddDays(1);
        //        break;
        //}
        default_supplier_date = default_supplier_date.AddDays(1);

        while (Utility.IsHolidays(default_supplier_date))
        {
            default_supplier_date = default_supplier_date.AddDays(1);         // temp
        }

        SortedList CK_list = new SortedList();


        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select * ,  s.supplier_name ,  s.supplier_shortname  from tbCustomers  c" +
                              " left join tbSuppliers  s on c.supplier_code = s.supplier_code " +
                              " where c.customer_code = '" + customer_code + "' ";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
                if (dt.Rows.Count > 0)
                {
                    send_contact = dt.Rows[0]["customer_shortname"].ToString();           //名稱
                    send_tel = dt.Rows[0]["telephone"].ToString();                        //電話
                    send_city = dt.Rows[0]["shipments_city"].ToString().Trim();           //出貨地址-縣市
                    send_area = dt.Rows[0]["shipments_area"].ToString().Trim();           //出貨地址-鄉鎮市區
                    send_address = dt.Rows[0]["shipments_road"].ToString().Trim();        //出貨地址-路街巷弄號
                    uniform_numbers = dt.Rows[0]["uniform_numbers"].ToString().Trim();    //統編
                    supplier_code = dt.Rows[0]["supplier_code"].ToString().Trim();        //區配
                    if (supplier_code == "001")
                    {
                        supplier_name = "零擔";
                    }
                    else if (supplier_code == "002")
                    {
                        supplier_name = "流通";
                    }
                    else
                    {
                        supplier_name = dt.Rows[0]["supplier_shortname"].ToString().Trim();  // 區配
                    }
                    pricing_type = dt.Rows[0]["pricing_code"].ToString().Trim();        // 計價模式
                }
        }

        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.CommandText = "select post_city , post_area, supplier_code   from ttArriveSites";
            using (DataTable dt = dbAdapter.getDataTable(cmd))
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (List.IndexOfKey(dr["post_city"].ToString() + dr["post_area"].ToString()) == -1)
                        {
                            List.Add(dr["post_city"].ToString() + dr["post_area"].ToString(), dr["supplier_code"].ToString());
                        }
                    }
                }


            //cmd.CommandText = "Select supplier_code  from tbSuppliers where supplier_code  is not null and active_flag = 1 ";
            //using (DataTable dt = dbAdapter.getDataTable(cmd))
            //{ 
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            if (List.IndexOfKey(dr["supplier_code"].ToString()) == -1)
            //            {
            //                List.Add(dr["supplier_code"].ToString(), dr["supplier_code"].ToString());
            //            }
            //        }
            //    }
            //}
        }

        string check_number = "";
        string subpoena_category = "";
        string subpoena_code = "";
        string receive_contact = "";
        string receive_tel1 = "";
        string receive_tel2 = "";
        string receive_city = "";
        string receive_area = "";
        string receive_address = "";
        string pieces = "";
        string plates = "";
        string cbm = "";
        string collection_money = "";
        string arrive_to_pay_freight = "";
        string arrive_to_pay_append = "";
        string arrive_assign_date = "";
        string receipt_flag = "";
        string pallet_recycling_flag = "";
        string invoice_desc = "";
        string check_type = "001";        //託運類別：001一般    
        string product_category = "001";  //商品種類：001一般
        string time_period = "午";        //配送時間：早、午、晚
        string sub_check_number = "000";  //次貨號
        string area_arrive_code = "";     //到著碼
        string receive_by_arrive_site_flag = "0";
        string print_flag = "0";
        string _supplier_fee = "0";  //專車貨件運費
        string _csection_fee = "0";  //專車配送費用
        string _send_contact = "";
        string _send_city = "";
        string _send_area = "";
        string _send_address = "";
        string _send_station_scode = "";
        string _send_tel = "";




        lbMsg.Items.Clear();
        if (file01.HasFile)
        {
            string ttPath = Request.PhysicalApplicationPath + @"files\";

            //string ttFileName = ttPath + file02.PostedFile.FileName;
            string ttFileName = ttPath + file01.FileName;

            string ttExtName = System.IO.Path.GetExtension(ttFileName);
            if ((ttExtName == ".xls") || (ttExtName == ".xlsx"))
            {
                file01.PostedFile.SaveAs(ttFileName);
                HSSFWorkbook workbook = null;
                HSSFSheet u_sheet = null;
                string ttSelectSheetName = "";
                workbook = new HSSFWorkbook(file01.FileContent);
                try
                {
                    //for (int x = 0; x <= workbook.NumberOfSheets - 1; x++)
                    //{
                    u_sheet = (HSSFSheet)workbook.GetSheetAt(0);
                    ttSelectSheetName = workbook.GetSheetName(0);

                    if ((!ttSelectSheetName.Contains("Print_Titles")) && (!ttSelectSheetName.Contains("Print_Area")))
                    {
                        Boolean IsSheetOk = true;//工作表驗證
                        lbMsg.Items.Add(String.Format("工作表【" + ttSelectSheetName.Replace("$", "") + "】  開始匯入時間：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm")));

                        //因為要讀取的資料列不包含標頭，所以i從u_sheet.FirstRowNum + 1開始讀
                        string strRow, strInsCol, strInsVal;
                        List<StringBuilder> sb_del_list = new List<StringBuilder>();
                        List<StringBuilder> sb_ins_list = new List<StringBuilder>();
                        StringBuilder sb_temp_del = new StringBuilder();
                        StringBuilder sb_temp_ins = new StringBuilder();

                        HSSFRow Row_title = new HSSFRow();

                        if (u_sheet.LastRowNum > 0) Row_title = (HSSFRow)u_sheet.GetRow(0);

                        for (int i = u_sheet.FirstRowNum + 1; i <= u_sheet.LastRowNum; i++)  //一列一列地讀取資料
                        {


                            HSSFRow Row = (HSSFRow)u_sheet.GetRow(i);  //取得目前的資料列  
                            if (Row != null)
                            {
                                #region 初始化
                                subpoena_category = "";
                                subpoena_code = "";
                                receive_contact = "";
                                receive_tel1 = "";
                                receive_tel2 = "";
                                receive_city = "";
                                receive_area = "";
                                receive_address = "";
                                pieces = "";
                                plates = "";
                                cbm = "";
                                collection_money = "";
                                arrive_to_pay_freight = "";
                                arrive_to_pay_append = "";
                                arrive_assign_date = "";
                                receipt_flag = "0";
                                pallet_recycling_flag = "0";
                                invoice_desc = "";
                                area_arrive_code = "";
                                strInsCol = "";
                                strInsVal = "";
                                sub_check_number = "000";
                                _supplier_fee = "0";  //專車貨件運費
                                _csection_fee = "0";  //專車配送費用
                                _send_contact = "";
                                _send_city = "";
                                _send_area = "";
                                _send_address = "";
                                _send_tel = "";
                                strRow = (i + 1).ToString();
                                supplier_date = default_supplier_date;

                                #endregion

                                if (Row.GetCell(1) == null || Row.GetCell(1).ToString() == "")
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                    break;
                                }
                                totalNum += 1;
                                successNum += 1;
                                #region 驗證&取值 IsSheetOk
                                for (int j = 0; j < Row_title.Cells.Count; j++)
                                {
                                    Boolean IsChkOk = true;
                                    switch (j)
                                    {
                                        case 0:  //託運單號
                                            if (Row.GetCell(j) != null) check_number = Row.GetCell(j).ToString();
                                            if (!Utility.IsNumeric(check_number))
                                            {
                                                //IsChkOk =
                                                //IsSheetOk = false;
                                            }
                                            else
                                            {

                                                IsChkOk = IsValidCheckNumber(check_number);
                                                if (IsChkOk)
                                                {
                                                    int idx = CK_list.IndexOfKey(check_number);
                                                    if (idx == -1)
                                                    {
                                                        CK_list.Add(check_number, 0);
                                                        sub_check_number = "000";
                                                    }
                                                    else
                                                    {
                                                        int subnum = Convert.ToInt32(CK_list.GetByIndex(idx));
                                                        subnum += 1;
                                                        sub_check_number = String.Format("{0:000}", subnum);
                                                    }
                                                }

                                            }
                                            break;
                                        case 1:  //傳票類別
                                            if (Row.GetCell(j) != null) subpoena_category = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(subpoena_category))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                switch (subpoena_category)
                                                {
                                                    case "元付":
                                                        subpoena_code = "11";
                                                        break;
                                                    case "到付":
                                                        subpoena_code = "21";
                                                        break;
                                                    case "代收貨款":
                                                        subpoena_code = "41";
                                                        break;
                                                    case "元付-到付追加":
                                                        subpoena_code = "25";
                                                        break;
                                                    default:
                                                        IsChkOk =
                                                        IsSheetOk = false;
                                                        break;
                                                }
                                            }
                                            break;
                                        case 2:  //寄件人姓名
                                            if (Row.GetCell(j) != null)
                                                if (!String.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                {
                                                    _send_contact = Row.GetCell(j).ToString();
                                                }
                                            break;
                                        case 3:  //寄件人電話
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.String: //字符串类型                                                    
                                                        if (!String.IsNullOrEmpty(Row.GetCell(j).ToString()))
                                                        {
                                                            _send_tel = Row.GetCell(j).ToString();
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        _send_tel = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                }
                                            }
                                            break;
                                        case 4:  //寄件人地址(縣市)
                                            if (Row.GetCell(j) != null)
                                                if (!String.IsNullOrEmpty(Row.GetCell(j).ToString().Trim()))
                                                {
                                                    _send_city = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                }
                                            _send_city = _send_city.Replace("台", "臺");
                                            break;
                                        case 5:  //寄件人地址(鄉鎮區)
                                            if (Row.GetCell(j) != null)
                                                if (!String.IsNullOrEmpty(Row.GetCell(j).ToString().Trim()))
                                                {
                                                    _send_area = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                }

                                            var result = GetPalletExclusiveSendStationByCustomerCode(dlcustomer_code.SelectedValue);

                                            if (result == "0" || result == "")
                                            {
                                                if (Row.GetCell(j - 1) != null && Row.GetCell(j) != null)
                                                {
                                                    _send_station_scode = GetScodeByCityAndArea(_send_city, _send_area);
                                                }
                                                else
                                                {
                                                    var ca = GetCustomerShipCityAndAreaByCustomerCode(dlcustomer_code.SelectedValue);
                                                    _send_station_scode = GetScodeByCityAndArea(ca.City, ca.Area);
                                                }
                                            }
                                            else if (result.Length > 0 && result != "0")
                                            {
                                                _send_station_scode = result;
                                            }
                                            break;
                                        case 6:  //寄件人地址
                                            if (Row.GetCell(j) != null)
                                                if (!String.IsNullOrEmpty(Row.GetCell(j).ToString().Trim()))
                                                {
                                                    _send_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                                    _send_address = _send_address.Replace("台", "臺");
                                                    _send_address = _send_city != "" ? _send_address.Replace(_send_city, "") : "";
                                                    _send_address = _send_area != "" ? _send_address.Replace(_send_area, "") : "";
                                                }

                                            break;
                                        case 7:  //收件人姓名
                                            if (Row.GetCell(j) != null) receive_contact = Row.GetCell(j).ToString();
                                            if (String.IsNullOrEmpty(receive_contact))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 8:  //收件人電話1
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        receive_tel1 = "";
                                                        if (String.IsNullOrEmpty(receive_tel1))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        receive_tel1 = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(receive_tel1))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        receive_tel1 = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                }
                                            }

                                            //if (Row.GetCell(j) != null) receive_tel1 = Row.GetCell(j).ToString();
                                            //if (String.IsNullOrEmpty(receive_tel1))
                                            //{
                                            //    IsChkOk =
                                            //    IsSheetOk = false;
                                            //}
                                            break;
                                        case 9:  //收件人電話2
                                                 //if (Row.GetCell(j) != null) receive_tel2 = Row.GetCell(j).ToString();
                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        receive_tel2 = "";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        receive_tel2 = Row.GetCell(j).ToString();
                                                        break;
                                                    case CellType.Numeric: //数字类型
                                                        receive_tel2 = Row.GetCell(j).NumericCellValue.ToString();
                                                        break;
                                                }
                                            }
                                            break;
                                        case 10:  //收件人地址(縣市)
                                            if (Row.GetCell(j) != null) receive_city = Row.GetCell(j).ToString().Trim();  //前後去空白
                                            if (String.IsNullOrEmpty(receive_city))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            receive_city = receive_city.Replace("台", "臺");
                                            break;
                                        case 11:  //收件人地址(鄉鎮區)
                                            if (Row.GetCell(j) != null) receive_area = Row.GetCell(j).ToString().Trim();  //前後去空白
                                            if (String.IsNullOrEmpty(receive_area))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 12:  //收件人地址
                                            if (Row.GetCell(j) != null) receive_address = Row.GetCell(j).ToString().Trim();  //前後去空白
                                            if (String.IsNullOrEmpty(receive_address))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            else
                                            {
                                                receive_address = receive_address.Replace("台", "臺");
                                                receive_address = receive_city != "" ? receive_address.Replace(receive_city, "") : receive_address;
                                                receive_address = receive_area != "" ? receive_address.Replace(receive_area, "") : receive_address;
                                            }
                                            break;
                                        case 13:  //到著碼
                                            if (Row.GetCell(j) != null) area_arrive_code = Row.GetCell(j).ToString().Trim();
                                            //if (List.ContainsValue(area_arrive_code) == false) area_arrive_code = "";   //若找不到到著碼，系統自動比對    //2019/08/12 調整

                                            break;
                                        case 14:  //板數
                                            if (Row.GetCell(j) != null) plates = Row.GetCell(j).ToString().Trim();
                                            if (!String.IsNullOrEmpty(plates) && !Utility.IsNumeric(plates))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 15:  //件數
                                            if (Row.GetCell(j) != null) pieces = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(pieces) && !Utility.IsNumeric(pieces))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 16:  //才數
                                            if (Row.GetCell(j) != null) cbm = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(cbm) && !Utility.IsNumeric(cbm))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 17:  //指配日期

                                            if (Row.GetCell(j) != null)
                                            {
                                                ICell cell = Row.GetCell(j);
                                                switch (cell.CellType)
                                                {
                                                    case CellType.Blank: //空数据类型处理
                                                        arrive_assign_date = "NULL";
                                                        break;
                                                    case CellType.String: //字符串类型
                                                        arrive_assign_date = Row.GetCell(j).ToString();
                                                        if (String.IsNullOrEmpty(arrive_assign_date))
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                    case CellType.Numeric: //数字类型       
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell))  // 日期
                                                        {
                                                            arrive_assign_date = Convert.ToDateTime(Row.GetCell(j).DateCellValue).ToString("yyyy/MM/dd");
                                                        }
                                                        else
                                                        {
                                                            IsChkOk =
                                                            IsSheetOk = false;
                                                        }
                                                        break;
                                                }
                                                DateTime _arrive_assign_date;
                                                if (DateTime.TryParse(arrive_assign_date, out _arrive_assign_date))
                                                {
                                                    supplier_date = _arrive_assign_date;     //如果有指配日期，則配送日帶指配日期
                                                }
                                            }
                                            else
                                            {
                                                arrive_assign_date = "NULL";
                                            }

                                            break;
                                        case 18://回單
                                            if (Row.GetCell(j) != null) receipt_flag = Row.GetCell(j).ToString();
                                            receipt_flag = (receipt_flag == "Y") ? "1" : "0";
                                            //if (String.IsNullOrEmpty(receipt_flag))
                                            //{
                                            //    IsChkOk =
                                            //    IsSheetOk = false;
                                            //}
                                            //else
                                            //{

                                            //}
                                            break;
                                        case 19:   //棧板回收
                                            if (Row.GetCell(j) != null) pallet_recycling_flag = Row.GetCell(j).ToString();
                                            pallet_recycling_flag = (pallet_recycling_flag == "Y") ? "1" : "0";
                                            //if (String.IsNullOrEmpty(pallet_recycling_flag))
                                            //{
                                            //    IsChkOk =
                                            //    IsSheetOk = false;
                                            //}
                                            //else
                                            //{

                                            //}
                                            break;
                                        case 20:   //到付運費
                                            if (Row.GetCell(j) != null) arrive_to_pay_freight = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(arrive_to_pay_freight) && !Utility.IsNumeric(arrive_to_pay_freight))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 21:   //代收貨款
                                            if (Row.GetCell(j) != null) collection_money = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(collection_money) && !Utility.IsNumeric(collection_money))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 22:   //到付追加
                                            if (Row.GetCell(j) != null) arrive_to_pay_append = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(arrive_to_pay_append) && !Utility.IsNumeric(arrive_to_pay_append))
                                            {
                                                IsChkOk =
                                                IsSheetOk = false;
                                            }
                                            break;
                                        case 23:  //專車貨件運費
                                                  //如果匯入次客代為專車
                                                  //則帶入專車貨件運費、專車配送費用的值
                                                  //如果次客代非專車，自動忽略
                                                  //如果無值，則不須理會
                                            if (Row.GetCell(j) != null) _supplier_fee = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(_supplier_fee) && !Utility.IsNumeric(_supplier_fee))
                                            {
                                                _supplier_fee = "0";
                                            }
                                            break;
                                        case 24:  //專車配送費用
                                            if (Row.GetCell(j) != null) _csection_fee = Row.GetCell(j).ToString();
                                            if (!String.IsNullOrEmpty(_csection_fee) && !Utility.IsNumeric(_csection_fee))
                                            {
                                                _csection_fee = "0";
                                            }
                                            break;
                                        case 25:  //託運備註
                                            if (Row.GetCell(j) != null) invoice_desc = Row.GetCell(j).ToString();
                                            //if (String.IsNullOrEmpty(invoice_desc))
                                            //{
                                            //    IsChkOk =
                                            //    IsSheetOk = false;
                                            //}

                                            #region 依託運類別檢查數量
                                            //論板時、論小板時：【板數】與【件數】一定要提示填入數字
                                            //論才時：【板數】與【件數】及【才數】皆要填入
                                            switch (pricing_type)
                                            {
                                                case "01":
                                                    _supplier_fee = "0";
                                                    _csection_fee = "0";
                                                    if (plates == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【板數】是否正確!(論板時,【板數】與【件數】一定要填入數字)");
                                                    }
                                                    else if (pieces == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!(論板時,【板數】與【件數】一定要填入數字)");
                                                    }

                                                    break;
                                                case "04":
                                                    _supplier_fee = "0";
                                                    _csection_fee = "0";
                                                    if (plates == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【板數】是否正確!(論小板時,【板數】與【件數】一定要填入數字)");
                                                    }
                                                    else if (pieces == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!(論小板時,【板數】與【件數】一定要填入數字)");
                                                    }

                                                    break;
                                                case "02":
                                                    _supplier_fee = "0";
                                                    _csection_fee = "0";
                                                    if (pieces == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!");
                                                    }
                                                    break;
                                                case "03":
                                                    _supplier_fee = "0";
                                                    _csection_fee = "0";
                                                    if (cbm == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【才數】是否正確!(論才時,【板數】與【件數】與【才數】一定要填入數字)");
                                                    }
                                                    else if (plates == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【板數】是否正確!(論才時,【板數】與【件數】與【才數】一定要填入數字)");
                                                    }
                                                    else if (pieces == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【件數】是否正確!(論才時,【板數】與【件數】與【才數】一定要填入數字)");
                                                    }
                                                    break;
                                                case "05":
                                                    if (plates == "")
                                                    {
                                                        IsSheetOk = false;
                                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【板數】是否正確!(專車時,【板數】一定要填入數字)");
                                                    }
                                                    else
                                                    {
                                                        int _plates = 0;
                                                        if (int.TryParse(plates, out _plates) && _plates <= 0)
                                                        {
                                                            IsSheetOk = false;
                                                            lbMsg.Items.Add("第" + strRow + "列： 請確認【板數】是否正確!(專車時,【板數】一定要填入數字)");
                                                        }
                                                    }
                                                    break;

                                            }
                                            #endregion

                                            #region 花蓮、台東地區 強制輸入才數
                                            int i_tmp = 0;
                                            if (!int.TryParse(cbm, out i_tmp)) i_tmp = -1;
                                            if (((receive_city == "花蓮縣") || (receive_city == "臺東縣")) && (i_tmp <= 0))
                                            {
                                                IsSheetOk = false;
                                                lbMsg.Items.Add("第" + strRow + "列： 配送地區為" + receive_city + "時,【才數】一定要填入數字)");
                                                return;
                                            }
                                            #endregion
                                            break;
                                    }

                                    //switch (Row_title.GetCell(j).StringCellValue)
                                    //{
                                    //    case "傳票類別":
                                    //        if (Row.GetCell(j) != null) subpoena_category = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(subpoena_category))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        else {
                                    //            switch  (subpoena_category)
                                    //            {
                                    //                case "元付":
                                    //                    subpoena_code = "11";
                                    //                    break;
                                    //                case "到付":
                                    //                    subpoena_code = "21";
                                    //                    break;
                                    //                case "代收貨款":
                                    //                    subpoena_code = "41";
                                    //                    break;
                                    //                case "元付-到付追加":
                                    //                    subpoena_code = "25";
                                    //                    break;
                                    //                default:
                                    //                    IsChkOk =
                                    //                    IsSheetOk = false;
                                    //                    break;
                                    //            }
                                    //        }
                                    //        break;
                                    //    case "收件人姓名":
                                    //        if (Row.GetCell(j) != null) receive_contact = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receive_contact))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "收件人電話1":
                                    //        if (Row.GetCell(j) != null) receive_tel1 = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receive_tel1))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "收件人電話2":
                                    //        if (Row.GetCell(j) != null) receive_tel2 = Row.GetCell(j).ToString();
                                    //        //if (String.IsNullOrEmpty(receive_tel2))
                                    //        //{
                                    //        //    IsChkOk =
                                    //        //    IsSheetOk = false;
                                    //        //}
                                    //        break;
                                    //    case "收件人地址(縣市)":
                                    //        if (Row.GetCell(j) != null) receive_city = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receive_city))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "收件人地址(鄉鎮區)":
                                    //        if (Row.GetCell(j) != null) receive_area = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receive_area))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "收件人地址":
                                    //        if (Row.GetCell(j) != null) receive_address = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receive_address))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "板數":
                                    //        if (Row.GetCell(j) != null) plates = Row.GetCell(j).ToString();
                                    //        if (!Utility.IsNumeric(plates))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "件數":
                                    //        if (Row.GetCell(j) != null) pieces = Row.GetCell(j).ToString();
                                    //        if (!Utility.IsNumeric(pieces))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "才數":
                                    //        if (Row.GetCell(j) != null) cbm = Row.GetCell(j).ToString();
                                    //        if (!Utility.IsNumeric(cbm))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "指配日期":
                                    //        if (Row.GetCell(j) != null) arrive_assign_date = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(arrive_assign_date))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "回單":
                                    //        if (Row.GetCell(j) != null) receipt_flag = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(receipt_flag))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        else
                                    //        {
                                    //            receipt_flag = (receipt_flag == "Y") ? "1" : "0";
                                    //        }
                                    //        break;
                                    //    case "棧板回收":
                                    //        if (Row.GetCell(j) != null) pallet_recycling_flag = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(pallet_recycling_flag))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        else
                                    //        {
                                    //            pallet_recycling_flag = (pallet_recycling_flag == "Y") ? "1" : "0";
                                    //        }
                                    //        break;
                                    //    case "到付運費":
                                    //        if (Row.GetCell(j) != null) arrive_to_pay_freight = Row.GetCell(j).ToString();
                                    //        if ( !Utility.IsNumeric(arrive_to_pay_freight))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "代收貨款":
                                    //        if (Row.GetCell(j) != null) collection_money = Row.GetCell(j).ToString();
                                    //        if ( !Utility.IsNumeric(collection_money))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "到付追加":
                                    //        if (Row.GetCell(j) != null) arrive_to_pay_append = Row.GetCell(j).ToString();
                                    //        if ( !Utility.IsNumeric(arrive_to_pay_append))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //    case "託運備註":
                                    //        if (Row.GetCell(j) != null) invoice_desc = Row.GetCell(j).ToString();
                                    //        if (String.IsNullOrEmpty(invoice_desc))
                                    //        {
                                    //            IsChkOk =
                                    //            IsSheetOk = false;
                                    //        }
                                    //        break;
                                    //}
                                    if (!IsChkOk)
                                    {
                                        lbMsg.Items.Add("第" + strRow + "列： 請確認【" + Row_title.GetCell(j).StringCellValue + "】 是否正確!");
                                    }
                                }



                                if (_send_contact == "") _send_contact = send_contact;
                                if (_send_city == "") _send_city = send_city;
                                if (_send_area == "") _send_area = send_area;
                                if (_send_address == "") _send_address = send_address;
                                if (_send_tel == "") _send_tel = send_tel;

                                #endregion

                                #region 組新增
                                if (area_arrive_code == "")
                                {
                                    int ttIdx = List.IndexOfKey(receive_city + receive_area);
                                    area_arrive_code = (ttIdx >= 1) ? List.GetByIndex(ttIdx).ToString() : "";
                                }
                                receive_address = receive_address.Replace("'", "＇");
                                receive_address = receive_address.Replace("--", "－");
                                receive_tel1 = (receive_tel1.Length > 20 ? receive_tel1.Substring(0, 20) : receive_tel1);
                                receive_tel2 = (receive_tel2.Length > 20 ? receive_tel2.Substring(0, 20) : receive_tel2);
                                receive_contact = receive_contact.Replace("\n", "");
                                receive_contact = (receive_contact.Length > 20 ? receive_contact.Substring(0, 20) : receive_contact);
                                receive_address = receive_address.Replace("\n", "");
                                receive_address = (receive_address.Length > 100 ? receive_address.Substring(0, 100) : receive_address);

                                strInsCol = "pricing_type,customer_code,check_number,check_type,subpoena_category,receive_tel1,receive_tel2,receive_contact,receive_city,receive_area,receive_address,area_arrive_code,receive_by_arrive_site_flag" +
                                            " ,pieces,plates,cbm,collection_money,arrive_to_pay_freight,arrive_to_pay_append,send_contact,send_city,send_area,send_address,send_station_scode,send_tel,product_category,invoice_desc,time_period,arrive_assign_date" +
                                            " ,receipt_flag,pallet_recycling_flag,cuser,cdate,uuser,udate,supplier_code,supplier_name,import_randomCode,print_date, supplier_date, print_flag, turn_board,upstairs,difficult_delivery" +
                                            " ,turn_board_fee,upstairs_fee,difficult_fee, add_transfer,supplier_fee,csection_fee,sub_check_number";
                                strInsVal = "N\'" + pricing_type + "\', " +
                                            "N\'" + customer_code + "\', " +
                                            "N\'" + check_number + "\', " +
                                            "N\'" + check_type + "\', " +
                                            "N\'" + subpoena_code + "\', " +
                                            "N\'" + receive_tel1 + "\', " +
                                            "N\'" + receive_tel2 + "\', " +
                                            "N\'" + receive_contact + "\', " +
                                            "N\'" + receive_city + "\', " +
                                            "N\'" + receive_area + "\', " +
                                            "N\'" + receive_address + "\', " +
                                            "N\'" + area_arrive_code + "\', " +
                                            "N\'" + receive_by_arrive_site_flag + "\', " +
                                            "N\'" + pieces + "\', " +
                                            "N\'" + plates + "\', " +
                                            "N\'" + cbm + "\', " +
                                            "N\'" + collection_money + "\', " +
                                            "N\'" + arrive_to_pay_freight + "\', " +
                                            "N\'" + arrive_to_pay_append + "\', " +
                                            "N\'" + (_send_contact.Length > 20 ? _send_contact.Substring(0, 20) : _send_contact) + "\', " +
                                            "N\'" + _send_city + "\', " +
                                            "N\'" + _send_area + "\', " +
                                            "N\'" + _send_address + "\', " +
                                            "N\'" + _send_station_scode + "\', " +
                                            "N\'" + _send_tel + "\', " +
                                            "N\'" + product_category + "\', " +
                                            "N\'" + invoice_desc + "\', " +
                                            "N\'" + time_period + "\', ";
                                if (arrive_assign_date == "NULL")
                                {
                                    strInsVal += arrive_assign_date + ", ";
                                }
                                else
                                {
                                    strInsVal += "N\'" + arrive_assign_date + "\', ";
                                }

                                strInsVal += "N\'" + receipt_flag + "\', " +
                                            "N\'" + pallet_recycling_flag + "\', " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + Session["account_code"] + "\', " +
                                            "GETDATE(), " +
                                            "N\'" + supplier_code + "\', " +
                                            "N\'" + supplier_name + "\', " +
                                            "N\'" + randomCode + "\', " +
                                            "N\'" + print_date.ToString("yyyy/MM/dd") + "\', " +
                                            "N\'" + supplier_date.ToString("yyyy/MM/dd") + "\', " +
                                            "N\'" + print_flag + "\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'0\', " +
                                            "N\'" + _supplier_fee + "\', " +
                                            "N\'" + _csection_fee + "\', " +
                                            "N\'" + sub_check_number + "\' ";

                                string insertstr = "INSERT INTO tcDeliveryRequests (" + strInsCol + ") VALUES(" + strInsVal + "); ";

                                sb_temp_ins.Append(insertstr);

                                //每100筆組成一字串
                                if (i % 100 == 0 || i == u_sheet.LastRowNum)
                                {
                                    sb_ins_list.Add(sb_temp_ins);
                                    sb_temp_ins = new StringBuilder();
                                }

                                #endregion
                            }

                        }

                        //驗證ok? 執行SQL指令:請user chk 欄位後，再重傳
                        if (IsSheetOk)
                        {
                            #region 執行SQL

                            startTime = DateTime.Now;   //開始匯入時間
                            //新增
                            foreach (StringBuilder sb in sb_ins_list)
                            {
                                if (sb.Length > 0)
                                {
                                    String strSQL = sb.ToString();
                                    using (SqlCommand cmd = new SqlCommand())
                                    {
                                        cmd.CommandText = strSQL;
                                        try
                                        {
                                            dbAdapter.execNonQuery(cmd);
                                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入成功!");
                                        }
                                        catch (Exception ex)
                                        {
                                            lbMsg.Items.Add(ex.Message);
                                        }

                                    }
                                }
                            }
                            endTime = DateTime.Now;   //匯入結束時間

                            #endregion


                            #region  更新託運單的費用欄位
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandText = string.Format(@"Select request_id,receive_city,receive_area,receive_address,pallet_recycling_flag from tcDeliveryRequests with(nolock) where import_randomCode ='" + randomCode + "' and pricing_type <> '05'");
                                DataTable dt = dbAdapter.getDataTable(cmd);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                                    {
                                        SqlCommand cmd2 = new SqlCommand();
                                        cmd2.CommandText = "usp_GetShipFeeByRequestId";
                                        cmd2.CommandType = CommandType.StoredProcedure;
                                        cmd2.Parameters.AddWithValue("@request_id", dt.Rows[i]["request_id"].ToString());
                                        using (DataTable dt2 = dbAdapter.getDataTable(cmd2))
                                        {
                                            if (dt2 != null && dt2.Rows.Count > 0)
                                            {
                                                status_code = Convert.ToInt32(dt2.Rows[0]["status_code"]);   //執行結果代碼 (0:未執行1:正常-1:錯誤)
                                                if (status_code == 1)
                                                {
                                                    supplier_fee = dt2.Rows[0]["supplier_fee"] != DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["supplier_fee"]) : 0;       //配送費用
                                                    cscetion_fee = dt2.Rows[0]["cscetion_fee"] != DBNull.Value ? Convert.ToInt32(dt2.Rows[0]["cscetion_fee"]) : 0;       //C配運價

                                                    //回寫到託運單的費用欄位
                                                    using (SqlCommand cmd3 = new SqlCommand())
                                                    {
                                                        cmd3.Parameters.AddWithValue("@supplier_fee", supplier_fee);
                                                        cmd3.Parameters.AddWithValue("@csection_fee", cscetion_fee);
                                                        cmd3.Parameters.AddWithValue("@total_fee", supplier_fee);
                                                        remote_fee = Utility.getremote_fee(dt.Rows[i]["receive_city"].ToString(), dt.Rows[i]["receive_area"].ToString(), dt.Rows[i]["receive_address"].ToString());
                                                        cmd3.Parameters.AddWithValue("@remote_fee", remote_fee);      //偏遠區加價
                                                        cmd3.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", dt.Rows[i]["request_id"].ToString());
                                                        cmd3.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd3);   //修改
                                                        try
                                                        {
                                                            dbAdapter.execNonQuery(cmd3);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            string strErr = string.Empty;
                                                            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                                                            strErr = "整批匯入-更新託運單費用" + Request.RawUrl + strErr + ": " + ex.ToString();

                                                            //錯誤寫至Log
                                                            PublicFunction _fun = new PublicFunction();
                                                            _fun.Log(strErr, "S");
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if (dt.Rows[i]["pallet_recycling_flag"].ToString() == "True")
                                            {

                                                using (SqlCommand cmd3 = new SqlCommand())     //各種類回收板數預設為0
                                                {

                                                    cmd3.Parameters.AddWithValue("@request_id", dt.Rows[i]["request_id"].ToString());
                                                    cmd3.Parameters.AddWithValue("@red", 0);
                                                    cmd3.Parameters.AddWithValue("@green", 0);
                                                    cmd3.Parameters.AddWithValue("@blue", 0);
                                                    cmd3.Parameters.AddWithValue("@yellow", 0);
                                                    cmd3.Parameters.AddWithValue("@plastic", 0);
                                                    cmd3.Parameters.AddWithValue("@special", 0);
                                                    cmd3.Parameters.AddWithValue("@special_desc", DBNull.Value);

                                                    cmd3.CommandText = dbAdapter.SQLdosomething("pallet_record", cmd3, "insert");
                                                    dbAdapter.execNonQuery(cmd3);
                                                }
                                            }
                                            

                                        }
                                    }
                                }
                            }

                            #endregion

                            #region 匯入記錄
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                                cmd.Parameters.AddWithValue("@type", 1);  //1.整批匯入 2.竹運匯入
                                cmd.Parameters.AddWithValue("@changeTable", "tcDeliveryRequests");
                                cmd.Parameters.AddWithValue("@fromWhere", "託運資料維護-匯入");
                                cmd.Parameters.AddWithValue("@randomCode", randomCode);
                                cmd.Parameters.AddWithValue("@memo", "");
                                cmd.Parameters.AddWithValue("@successNum", successNum.ToString());
                                cmd.Parameters.AddWithValue("@failNum", failNum.ToString());
                                cmd.Parameters.AddWithValue("@totalNum", totalNum.ToString());
                                cmd.Parameters.AddWithValue("@startTime", startTime);
                                cmd.Parameters.AddWithValue("@endTime", endTime);
                                cmd.Parameters.AddWithValue("@customer_code", customer_code);
                                cmd.Parameters.AddWithValue("@print_date", print_date);
                                cmd.Parameters.AddWithValue("@cuser", Session["account_code"] != null ? Session["account_code"].ToString() : null);
                                cmd.CommandText = dbAdapter.SQLdosomething("tbIORecords", cmd, "insert");
                                dbAdapter.execNonQuery(cmd);
                            }
                            #endregion 

                            readdata();
                        }
                        else
                        {
                            lbMsg.Items.Add("工作表【" + ttSelectSheetName.Replace("$", "") + "】匯入失敗，請檢查欄位是否有異常資料!");
                        }
                        lbMsg.Items.Add("------------------------------------------------------------------------------------------------------------------------------------");

                    }

                    //}
                }
                catch (Exception ex)
                {
                    ttErrStr = ex.Message;
                }
                finally
                {
                    //System.IO.File.Delete(ttPath + file01.PostedFile.FileName);
                }
                lbMsg.Items.Add("匯入完畢");


            }

        }
    }

    //protected void btnPirntLabel_Click(object sender, EventArgs e)
    //{
    //    string ttErrStr = "";
    //    //貨號區間
    //    int start_number = 0;
    //    int end_number = 999999999;
    //    int current_number = 0;
    //    int check_code = 0;        //檢查碼
    //    Int64 check_number = 0;    //貨號(current_number + check_code)
    //    //string print_chknum = "";
    //    string print_requestid = "";



    //    #region 取貨號(網路託運單)
    //    SqlCommand cmdt = new SqlCommand();
    //    DataTable dtt;
    //    cmdt.CommandText = " select * from tbCheckNumber where type = 2 ";
    //    dtt = dbAdapter.getDataTable(cmdt);
    //    if (dtt.Rows.Count > 0)
    //    {
    //        start_number = Convert.ToInt32(dtt.Rows[0]["start_number"]);
    //        end_number = Convert.ToInt32(dtt.Rows[0]["end_number"]);
    //        current_number = Convert.ToInt32(dtt.Rows[0]["current_number"].ToString());
    //        if (current_number == 0 || current_number == end_number)
    //        {
    //            current_number = start_number;
    //        }
    //        else
    //        {
    //            current_number += 1;
    //        }

    //    }
    //    else
    //    {
    //        ttErrStr = "查無網路託運單貨號區間，請冾系統管理員。";
    //    }
    //    #endregion


    //    if (ttErrStr == "")
    //    {
    //        if (New_List.Items.Count > 0)
    //        {
    //            for (int i = 0; i <= New_List.Items.Count - 1; i++)
    //            {
    //                Boolean Update = false;
    //                CheckBox chkRow = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
    //                Label lbcheck_number = ((Label)New_List.Items[i].FindControl("lbcheck_number"));
    //                string print_date = ((HiddenField)New_List.Items[i].FindControl("hid_print_date")).Value.ToString();
    //                string arrive_assign_date = ((Label)New_List.Items[i].FindControl("lbarrive_assign_date")).Text ;
    //                string Supplier_date = ((HiddenField)New_List.Items[i].FindControl("Hid_Supplier_date")).Value;
    //                Boolean  print_flag = Convert.ToBoolean(((HiddenField)New_List.Items[i].FindControl("Hid_print_flag")).Value);
    //                if ((chkRow != null) && (chkRow.Checked))
    //                {
    //                    string id = ((HiddenField)New_List.Items[i].FindControl("hid_id")).Value.ToString();
    //                    using (SqlCommand cmd = new SqlCommand())
    //                    {

    //                        #region 託運單號
    //                        if (lbcheck_number.Text == "")
    //                        {
    //                            check_code = (int)current_number % 7;
    //                            check_number = Convert.ToInt64(current_number.ToString() + check_code.ToString());
    //                            cmd.Parameters.AddWithValue("@check_number", check_number.ToString());    //貨號
    //                            lbcheck_number.Text = check_number.ToString();

    //                            SqlCommand cmd2 = new SqlCommand();
    //                            cmd2.Parameters.AddWithValue("@current_number", current_number);           //目前取號
    //                            cmd2.Parameters.AddWithValue("@check_code", check_code);                   //檢查碼
    //                            cmd2.Parameters.AddWithValue("@check_number", check_number);               //貨號
    //                            cmd2.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "type", "2");
    //                            cmd2.CommandText = dbAdapter.genUpdateComm("tbCheckNumber", cmd2);         //修改
    //                            dbAdapter.execNonQuery(cmd2);

    //                            if (current_number == end_number)
    //                            {
    //                                current_number = start_number;
    //                            }
    //                            else
    //                            {
    //                                current_number += 1;
    //                            }
    //                            Update = true;
    //                        }
    //                        #endregion

    //                        #region 發送日期
    //                        if (print_date == "")
    //                        {
    //                            cmd.Parameters.AddWithValue("@print_date", DateTime.Now);                  //發送日
    //                            print_date = DateTime.Today.ToString("yyyy/MM/dd");
    //                            //int Week = (int)DateTime.Today.DayOfWeek;
    //                            //if (DateTime.Today.ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

    //                            ////配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
    //                            //DateTime supplier_date = DateTime.Today;
    //                            //switch (Week)
    //                            //{
    //                            //    case 5:
    //                            //        supplier_date = supplier_date.AddDays(3);
    //                            //        break;
    //                            //    case 6:
    //                            //        supplier_date = supplier_date.AddDays(2);
    //                            //        break;
    //                            //    default:
    //                            //        supplier_date = supplier_date.AddDays(1);
    //                            //        break;
    //                            //}
    //                            //cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
    //                            Update = true;
    //                        }
    //                        #endregion

    //                        #region  配送日期
    //                        if (Supplier_date == "")
    //                        {
    //                            DateTime supplier_date = Convert.ToDateTime(print_date);


    //                            if (arrive_assign_date != "")
    //                            {
    //                                supplier_date = Convert.ToDateTime(arrive_assign_date);
    //                            }
    //                            else
    //                            {
    //                                //配送日自動產生，平日為 D+1，週五為D+3，週六為 D+2 且系統須針對特定日期(假日，如過年)順延
    //                                int Week = (int)Convert.ToDateTime(print_date).DayOfWeek;

    //                                //if (Convert.ToDateTime(print_date).ToString("yyyyMMdd") == "20170929") Week = -1;   // temp

    //                                supplier_date = Convert.ToDateTime(print_date);
    //                                switch (Week)
    //                                {
    //                                    case 5:
    //                                        supplier_date = supplier_date.AddDays(3);
    //                                        break;
    //                                    case 6:
    //                                        supplier_date = supplier_date.AddDays(2);
    //                                        break;
    //                                    default:
    //                                        supplier_date = supplier_date.AddDays(1);
    //                                        break;
    //                                }
    //                                while (Utility.IsHolidays(supplier_date))
    //                                {
    //                                    supplier_date = supplier_date.AddDays(1);         // temp
    //                                }
    //                            }

    //                            cmd.Parameters.AddWithValue("@supplier_date", supplier_date);              //配送日期
    //                            Update = true;
    //                        }

    //                        if (print_flag == false)
    //                        {
    //                            cmd.Parameters.AddWithValue("@print_flag", "1");                           //是否列印
    //                            Update = true;
    //                        }
    //                        #endregion

    //                        if (Update == true)
    //                        {
    //                            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "request_id", id);
    //                            cmd.CommandText = dbAdapter.genUpdateComm("tcDeliveryRequests", cmd);      //修改
    //                            dbAdapter.execNonQuery(cmd);
    //                            //print_chknum += "'" + lbcheck_number.Text.ToString() + "',";
    //                            print_requestid += id.ToString() + ",";
    //                        }
    //                        else
    //                        {
    //                            //print_chknum += "'" + lbcheck_number.Text + "',";
    //                            print_requestid += id.ToString() + ",";
    //                        }
    //                    }

    //                }
    //            }
    //        }

    //        //if (print_chknum != "") print_chknum = print_chknum.Substring(0, print_chknum.Length - 1);
    //        if (print_requestid != "") print_requestid = print_requestid.Substring(0, print_requestid.Length - 1);
    //        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://220.128.210.180:10080/files/label-20170427071500.pdf" + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
    //        if (option.SelectedValue == "1")
    //        {
    //            //捲筒
    //            PrintReelLabel(print_requestid);
    //        }
    //        else
    //        {
    //            PrintLabel(print_requestid, option.SelectedValue);
    //        }
    //        readdata();


    //        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('產生完畢');</script>", false);
    //        //return;
    //    }
    //    else
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
    //        return;
    //    }
    //}

    /// <summary>
    /// 列印標籤
    /// </summary>
    /// <param name="print_requestid">託運單id(以","隔開)</param>
    /// <param name="format">紙張格式(預設A4,一式6筆)</param>
    //private void PrintLabel(string print_requestid, string format = "0")
    //{
    //    string ttErrStr = "";
    //    NPOITable ttExlApp = null;
    //    string strSampleFileFullName = string.Empty;                                      //範本檔路徑

    //    SqlCommand cmd = new SqlCommand();
    //    string wherestr = string.Empty;
    //    DataTable DT = null;
    //    if (print_requestid != "")
    //    {
    //        // cmd.Parameters.AddWithValue("@check_number", print_chknum);
    //        //wherestr = " and check_number in(" + print_chknum + ")";
    //        wherestr = " and request_id in(" + print_requestid + ")";
    //        cmd.CommandText = string.Format(@"select A.supplier_date  , A.print_date, A.check_number , A.receipt_flag, A.pallet_recycling_flag,
    //                                      A.collection_money,A.arrive_to_pay_freight,A.arrive_to_pay_append,
    //                                      A.receive_contact ,  A.receive_tel1 , A.receive_tel1_ext , A.receive_tel2, A.receive_city , A.receive_area ,
    //                                      CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
    //                                      A.pricing_type ,A.pieces , A.plates , A.cbm ,
    //                                      A.subpoena_category, A.invoice_memo, A.invoice_desc,
    //                                      B.code_name  , A.customer_code , C.customer_shortname ,
    //                                      A.send_contact,A.send_tel,
    //                                      A.send_city , A.send_area , A.send_address ,A.area_arrive_code
    //                                      from tcDeliveryRequests A WITH(Nolock)
    //                                      left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
    //                                      left join tbCustomers C on C.customer_code = A.customer_code
    //                                      where 1= 1  {0} order by  A.check_number", wherestr);
    //        DT = dbAdapter.getDataTable(cmd);
    //    }
    //    if (DT != null)
    //    {
    //        try
    //        {
    //            #region 設定範本檔路徑
    //            switch (format)
    //            {
    //                case "0":  //A4 (一式6筆託運標籤)
    //                    strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label.xls"; ;
    //                    break;
    //                case "1":  //捲筒列印
    //                    strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_1.xls"; ;
    //                    break;
    //                case "2":  //A4 (一式1筆託運標籤)
    //                    strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_2.xls"; ;
    //                    break;
    //            }
    //            if (strSampleFileFullName.EndsWith(".xls") == false) strSampleFileFullName += ".xls";
    //            #endregion

    //            try
    //            {
    //                ttExlApp = new NPOITable(strSampleFileFullName);
    //                ttExlApp.OpenFile();

    //                string print_date = "";         //發送日期
    //                Boolean receipt_flag = false;   //是否回單
    //                Boolean pallet_recycling_flag = false; //是否棧板回收
    //                string supplier_date = "";      // 配送日期
    //                string pricing_type = "";       //計價模式
    //                string check_number = "";       //貨號
    //                string subpoena_category = "";  //傳票類別
    //                string subpoena_category_name = "";
    //                string receive_tel1 = "";       //收件人電話
    //                string receive_tel1_ext = "";
    //                string receive_tel2 = "";
    //                string receive_contact = "";    //收件人
    //                string receive_city = "";       //收件地址-縣市
    //                string receive_area = "";       //收件地址-鄉鎮市區
    //                string receive_address = "";    //收件地址-路街巷弄號
    //                int pieces = 0;                 //件數
    //                int plates = 0;                 //板數
    //                int cbm = 0;                    //才數
    //                int collection_money = 0;       //代收金 
    //                int arrive_to_pay_freight = 0;  //到付運費
    //                int arrive_to_pay_append = 0;   //到付追加
    //                string send_contact = "";       //寄件人
    //                string send_tel = "";           //寄件人電話
    //                string send_city = "";          //寄件人地址-縣市
    //                string send_area = "";          //寄件人地址-鄉鎮市區
    //                string send_address = "";       //寄件人地址-號街巷弄號
    //                string invoice_desc = "";       //備註
    //                string area_arrive_code = "";   //到著碼
    //                string ttStr = "";
    //                int addcolumn = 0;
    //                string pricing_type_name = "";

    //                switch (format)
    //                {
    //                    case "0":
    //                        ttExlApp.SRow = 1;
    //                        ttExlApp.SCol = 1;
    //                        ttExlApp.Line = 41;
    //                        ttExlApp.Column = 19;
    //                        ttExlApp.NextPage();
    //                        ttExlApp.GoToLine(1);

    //                        if (DT.Rows.Count > 0)
    //                        {
    //                            for (int i = 0; i < DT.Rows.Count; i++)
    //                            {
    //                                try
    //                                {
    //                                    print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
    //                                }
    //                                catch { }

    //                                receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
    //                                pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
    //                                supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
    //                                pricing_type = DT.Rows[i]["pricing_type"].ToString();
    //                                check_number = DT.Rows[i]["check_number"].ToString();
    //                                subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
    //                                subpoena_category_name = DT.Rows[i]["code_name"].ToString();
    //                                receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
    //                                receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
    //                                receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
    //                                receive_contact = DT.Rows[i]["receive_contact"].ToString();
    //                                receive_city = DT.Rows[i]["receive_city"].ToString();
    //                                receive_area = DT.Rows[i]["receive_area"].ToString();
    //                                receive_address = DT.Rows[i]["address"].ToString();
    //                                pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
    //                                plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
    //                                cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
    //                                collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
    //                                arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
    //                                arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
    //                                send_contact = DT.Rows[i]["send_contact"].ToString();
    //                                send_tel = DT.Rows[i]["send_tel"].ToString();
    //                                send_city = DT.Rows[i]["send_city"].ToString();
    //                                send_area = DT.Rows[i]["send_area"].ToString();
    //                                send_address = DT.Rows[i]["send_address"].ToString();
    //                                invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
    //                                area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


    //                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
    //                                ttStr = "";
    //                                switch (pricing_type)
    //                                {
    //                                    case "01":  //論板
    //                                        pricing_type_name = "論板";
    //                                        break;
    //                                    case "02":  //論件
    //                                        pricing_type_name = "論件";
    //                                        break;
    //                                    case "04":  //論小板
    //                                        pricing_type_name = "論小板";
    //                                        break;
    //                                    case "03":  //論才
    //                                        pricing_type_name = "論才";
    //                                        break;
    //                                }
    //                                ttStr += pricing_type_name;
    //                                if (receipt_flag == true)
    //                                    ttStr += "回單";
    //                                if (pallet_recycling_flag == true)
    //                                    ttStr += "棧板回收";
    //                                ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
    //                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
    //                                switch (subpoena_category)
    //                                {
    //                                    //case "11":   //元付                                            
    //                                    //    break;
    //                                    case "21":   //到付
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
    //                                        break;
    //                                    case "41":   //代收貨款
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
    //                                        break;
    //                                    case "25":   //元付-到付追加
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
    //                                        break;
    //                                }
    //                                ttExlApp.RowC += 1;
    //                                switch (pricing_type)
    //                                {
    //                                    case "01":  //論板
    //                                    case "02":  //論件
    //                                    case "04":  //論小板
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
    //                                        ttExlApp.RowC += 1;
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
    //                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
    //                                        break;
    //                                    case "03":  //論才
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
    //                                        ttExlApp.RowC += 1;
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
    //                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
    //                                        break;
    //                                }
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttStr = "";
    //                                if (receive_tel1 != "") ttStr += receive_tel1;
    //                                if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
    //                                if (receive_tel2 != "")
    //                                {
    //                                    ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
    //                                }
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);

    //                                switch (i % 2)
    //                                {
    //                                    case 0:
    //                                        addcolumn = 10;
    //                                        ttExlApp.RowC -= 12;

    //                                        break;
    //                                    case 1:
    //                                        addcolumn = 0;
    //                                        if ((i + 1) % 6 == 0)
    //                                        {
    //                                            if (i < DT.Rows.Count - 1) ttExlApp.NextPage();
    //                                        }
    //                                        else
    //                                        {
    //                                            ttExlApp.RowC += 2;
    //                                        }

    //                                        break;
    //                                }


    //                            }
    //                        }

    //                        break;
    //                    case "1":
    //                        break;
    //                    case "2":
    //                        ttExlApp.SRow = 1;
    //                        ttExlApp.SCol = 1;
    //                        ttExlApp.Line = 13;
    //                        ttExlApp.Column = 9;
    //                        ttExlApp.NextPage();
    //                        ttExlApp.GoToLine(1);

    //                        if (DT.Rows.Count > 0)
    //                        {
    //                            for (int i = 0; i < DT.Rows.Count; i++)
    //                            {
    //                                print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
    //                                receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
    //                                pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
    //                                supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
    //                                pricing_type = DT.Rows[i]["pricing_type"].ToString();
    //                                check_number = DT.Rows[i]["check_number"].ToString();
    //                                subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
    //                                subpoena_category_name = DT.Rows[i]["code_name"].ToString();
    //                                receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
    //                                receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
    //                                receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
    //                                receive_contact = DT.Rows[i]["receive_contact"].ToString();
    //                                receive_city = DT.Rows[i]["receive_city"].ToString();
    //                                receive_area = DT.Rows[i]["receive_area"].ToString();
    //                                receive_address = DT.Rows[i]["address"].ToString();
    //                                pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
    //                                plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
    //                                cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
    //                                collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
    //                                arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
    //                                arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
    //                                send_contact = DT.Rows[i]["send_contact"].ToString();
    //                                send_tel = DT.Rows[i]["send_tel"].ToString();
    //                                send_city = DT.Rows[i]["send_city"].ToString();
    //                                send_area = DT.Rows[i]["send_area"].ToString();
    //                                send_address = DT.Rows[i]["send_address"].ToString();
    //                                invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
    //                                area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


    //                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
    //                                ttStr = "";

    //                                switch (pricing_type)
    //                                {
    //                                    case "01":  //論板
    //                                        pricing_type_name = "論板";
    //                                        break;
    //                                    case "02":  //論件
    //                                        pricing_type_name = "論件";
    //                                        break;
    //                                    case "04":  //論小板
    //                                        pricing_type_name = "論小板";
    //                                        break;
    //                                    case "03":  //論才
    //                                        pricing_type_name = "論才";
    //                                        break;
    //                                }
    //                                ttStr += pricing_type_name;

    //                                if (receipt_flag == true)
    //                                    ttStr += "回單";
    //                                if (pallet_recycling_flag == true)
    //                                    ttStr += "棧板回收";
    //                                ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
    //                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
    //                                switch (subpoena_category)
    //                                {
    //                                    //case "11":   //元付
    //                                    //    break;
    //                                    case "21":   //到付
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
    //                                        break;
    //                                    case "41":   //代收貨款
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
    //                                        break;
    //                                    case "25":   //元付-到付追加
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
    //                                        break;
    //                                }
    //                                ttExlApp.RowC += 1;
    //                                switch (pricing_type)
    //                                {
    //                                    case "01":  //論板
    //                                    case "02":  //論件
    //                                    case "04":  //論小板
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
    //                                        ttExlApp.RowC += 1;
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
    //                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
    //                                        break;
    //                                    case "03":  //論才
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
    //                                        ttExlApp.RowC += 1;
    //                                        ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
    //                                        ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
    //                                        ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
    //                                        break;
    //                                }
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttStr = "";
    //                                if (receive_tel1 != "") ttStr += receive_tel1;
    //                                if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
    //                                if (receive_tel2 != "")
    //                                {
    //                                    ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
    //                                }
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
    //                                ttExlApp.RowC += 1;
    //                                ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);

    //                                if (i < DT.Rows.Count - 1) ttExlApp.NextPage();


    //                            }
    //                        }
    //                        break;
    //                }


    //                ttExlApp.DeleteTable(1);
    //                FileInfo fi = new FileInfo(strSampleFileFullName);
    //                string FileName = String.Empty;
    //                if (Request.Url.Host.IndexOf("localhost") >= 0)
    //                {
    //                    //本機→轉PDF
    //                    FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
    //                    string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
    //                    string pathDownload = Path.Combine(pathUser, "Downloads", FileName);
    //                    ttExlApp.Save(pathDownload, TableFormat.TFPDF);
    //                    BasePage.popDownload(Path.Combine(pathUser, "Downloads"), FileName, true);

    //                }
    //                else
    //                {
    //                    //Server→轉Excel
    //                    Boolean IsTest = false; //是否為測試模試
    //                    FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
    //                    string ExportPath = "files/" + FileName;

    //                    try
    //                    {
    //                        ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);

    //                        if (!IsTest)
    //                        {
    //                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
    //                        }
    //                        else
    //                        {
    //                            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
    //                        }
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        string strErr = string.Empty;
    //                        if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
    //                        strErr = "標籤列印-" + Request.RawUrl + strErr + ": " + ex.ToString();


    //                        //錯誤寫至Log
    //                        PublicFunction _fun = new PublicFunction();
    //                        _fun.Log(strErr, "S");
    //                    }
    //                }

    //                ////Workbook workbook = new Workbook();
    //                ////workbook.LoadFromFile(filename);
    //                ////workbook.SaveToFile("result.pdf", FileFormat.PDF);
    //                //string outputFolder = string.Empty;                                             //匯出檔路徑
    //                //string outputFileName = string.Empty;
    //                //FileInfo fi = new FileInfo(strSampleFileFullName);
    //                //outputFolder = Request.PhysicalApplicationPath + @"files\";
    //                //outputFileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);

    //                //try
    //                //{ ttExlApp.Save(outputFolder + outputFileName, TableFormat.TFPDF); }
    //                //catch
    //                //{
    //                //}

    //                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + ":10080/files/" + outputFileName + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);  //iis
    //                ////ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + "/files/" + outputFileName + ".pdf" + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);   //local


    //            }
    //            catch (Exception ex)
    //            {
    //                ttErrStr = "開啟範本失敗-E-" + ex.Message;
    //                ttExlApp = null;
    //                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            //ExceptionAdapter.logSysException(ex);
    //            //throw;
    //            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.Message + "');</script>", false);
    //        }
    //    }
    //    else
    //    {

    //        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可列印標籤');</script>", false);
    //        //return;
    //    }


    //}

    /// <summary>
    /// 列印標籤(以板列印)
    /// </summary>
    /// <param name="print_requestid">託運單id(以","隔開)</param>
    /// <param name="format">紙張格式(預設A4,一式6筆)</param>
    /* private void PrintLabel(string print_requestid, string format = "0")
     {
         string ttErrStr = "";
         NPOITable ttExlApp = null;
         string strSampleFileFullName = string.Empty;                                      //範本檔路徑



         SqlCommand cmd = new SqlCommand();
         string wherestr = string.Empty;
         DataTable DT = null;
         if (print_requestid != "")
         {
             // cmd.Parameters.AddWithValue("@check_number", print_chknum);
             wherestr = " and request_id in(" + print_requestid + ")";
             cmd.CommandText = string.Format(@"select A.supplier_date  , A.print_date, A.check_number , A.receipt_flag, A.pallet_recycling_flag,
                                           A.collection_money,A.arrive_to_pay_freight,A.arrive_to_pay_append,
                                           A.receive_contact ,  A.receive_tel1 , A.receive_tel1_ext , A.receive_tel2, A.receive_city , A.receive_area ,
                                           CASE WHEN A.receive_by_arrive_site_flag = '1' then A.arrive_address else receive_address end as address,
                                           A.pricing_type ,A.pieces , A.plates , A.cbm ,
                                           A.subpoena_category, A.invoice_memo, A.invoice_desc,
                                           B.code_name  , A.customer_code , C.customer_shortname ,
                                           A.send_contact,A.send_tel,
                                           A.send_city , A.send_area , A.send_address ,A.area_arrive_code
                                           from tcDeliveryRequests A WITH(Nolock)
                                           left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                           left join tbCustomers C on C.customer_code = A.customer_code
                                           where 1= 1  {0} order by  A.check_number", wherestr);
             DT = dbAdapter.getDataTable(cmd);
         }
         if (DT != null)
         {
             try
             {
                 #region 設定範本檔路徑
                 switch (format)
                 {
                     case "0":  //A4 (一式6筆託運標籤)
                         strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label.xls";
                         break;
                     case "1":  //捲筒列印
                         strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_1.xls";
                         break;
                     case "2":  //A4 (一式1筆託運標籤)
                         strSampleFileFullName = Request.PhysicalApplicationPath + @"report\label_2.xls";
                         break;
                 }

                 if (strSampleFileFullName.EndsWith(".xls") == false) strSampleFileFullName += ".xls";
                 #endregion

                 try
                 {
                     ttExlApp = new NPOITable(strSampleFileFullName);
                     ttExlApp.OpenFile();

                     string print_date = "";         //發送日期
                     Boolean receipt_flag = false;   //是否回單
                     Boolean pallet_recycling_flag = false; //是否棧板回收
                     string supplier_date = "";      // 配送日期
                     string pricing_type = "";       //計價模式
                     string check_number = "";       //貨號
                     string subpoena_category = "";  //傳票類別
                     string subpoena_category_name = "";
                     string receive_tel1 = "";       //收件人電話
                     string receive_tel1_ext = "";
                     string receive_tel2 = "";
                     string receive_contact = "";    //收件人
                     string receive_city = "";       //收件地址-縣市
                     string receive_area = "";       //收件地址-鄉鎮市區
                     string receive_address = "";    //收件地址-路街巷弄號
                     int pieces = 0;                 //件數
                     int plates = 0;                 //板數
                     int cbm = 0;                    //才數
                     int collection_money = 0;       //代收金 
                     int arrive_to_pay_freight = 0;  //到付運費
                     int arrive_to_pay_append = 0;   //到付追加
                     string send_contact = "";       //寄件人
                     string send_tel = "";           //寄件人電話
                     string send_city = "";          //寄件人地址-縣市
                     string send_area = "";          //寄件人地址-鄉鎮市區
                     string send_address = "";       //寄件人地址-號街巷弄號
                     string invoice_desc = "";       //備註
                     string area_arrive_code = "";   //到著碼
                     string ttStr = "";
                     int addcolumn = 0;
                     string pricing_type_name = "";

                     switch (format)
                     {
                         case "0":
                             ttExlApp.SRow = 1;
                             ttExlApp.SCol = 1;
                             ttExlApp.Line = 42;
                             ttExlApp.Column = 19;
                             ttExlApp.NextPage();
                             ttExlApp.GoToLine(1);
                             int s_tmp;
                             if (!int.TryParse(StartFrom.Text, out s_tmp)) s_tmp = 1;

                             #region 

                             #region 清除前面框線
                             switch (s_tmp)
                             {
                                 case 2:
                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 14, "style1", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     break;
                                 case 3:
                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(16);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(18);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(21);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(24);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(25);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 28, "style2", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     break;
                                 case 4:
                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(16);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(18);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(21);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(24);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(25);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(30);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(32);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(35);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(38);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(39);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.RowC -= 38;
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                     break;
                                 case 5:
                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(16);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(18);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(21);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(24);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(25);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(30);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(32);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(35);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(38);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(39);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 14, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     break;
                                 case 6:
                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(16);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(18);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(21);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(24);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(25);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(30);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(32);
                                     ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(35);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(38);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(39);
                                     ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                     ttExlApp.GoToLine(2);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(4);
                                     ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(7);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(10);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(11);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(16);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(18);
                                     ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(21);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(24);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(25);
                                     ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     ttExlApp.GoToLine(1);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 28, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                     break;

                             }
                             #endregion

                             if (DT.Rows.Count > 0)
                             {
                                 for (int i = 0; i < DT.Rows.Count; i++)
                                 {
                                     pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                     plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                     pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                     int printplates = pricing_type == "01" ? plates : pricing_type == "02" ? pieces : 1;    //如果是論板，那有幾板就要印幾張; 如果是論件，那有幾件就要印幾張
                                     for (int j = 1; j <= printplates; j++)
                                     {
                                         switch (s_tmp)
                                         {
                                             case 1:
                                                 ttExlApp.GoToLine(1);
                                                 addcolumn = 0;
                                                 break;
                                             case 2:

                                                 ttExlApp.GoToLine(15);
                                                 addcolumn = 0;
                                                 break;
                                             case 3:
                                                 ttExlApp.GoToLine(29);
                                                 addcolumn = 0;
                                                 break;
                                             case 4:
                                                 ttExlApp.GoToLine(1);
                                                 addcolumn = 10;
                                                 break;
                                             case 5:
                                                 ttExlApp.GoToLine(15);
                                                 addcolumn = 10;
                                                 break;
                                             case 6:
                                                 ttExlApp.GoToLine(29);
                                                 addcolumn = 10;
                                                 break;
                                         }

                                         print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
                                         receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
                                         pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
                                         supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
                                         //pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                         check_number = DT.Rows[i]["check_number"].ToString();
                                         subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
                                         subpoena_category_name = DT.Rows[i]["code_name"].ToString();
                                         receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
                                         receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
                                         receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
                                         receive_contact = DT.Rows[i]["receive_contact"].ToString();
                                         receive_city = DT.Rows[i]["receive_city"].ToString();
                                         receive_area = DT.Rows[i]["receive_area"].ToString();
                                         receive_address = DT.Rows[i]["address"].ToString();
                                         //pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                         //plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                         cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
                                         collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
                                         arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
                                         arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
                                         send_contact = DT.Rows[i]["send_contact"].ToString();
                                         send_tel = DT.Rows[i]["send_tel"].ToString();
                                         send_city = DT.Rows[i]["send_city"].ToString();
                                         send_area = DT.Rows[i]["send_area"].ToString();
                                         send_address = DT.Rows[i]["send_address"].ToString();
                                         invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
                                         area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


                                         ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
                                         ttStr = "";

                                         switch (pricing_type)
                                         {
                                             case "01":  //論板
                                                 pricing_type_name = "論板";
                                                 break;
                                             case "02":  //論件
                                                 pricing_type_name = "論件";
                                                 break;
                                             case "04":  //論小板
                                                 pricing_type_name = "論小板";
                                                 break;
                                             case "03":  //論才
                                                 pricing_type_name = "論才";
                                                 break;
                                         }
                                         ttStr += pricing_type_name;

                                         if (receipt_flag == true)
                                             ttStr += "回單";
                                         if (pallet_recycling_flag == true)
                                             ttStr += "棧板回收";
                                         ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
                                         ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
                                         switch (subpoena_category)
                                         {
                                             //case "11":   //元付
                                             //    break;
                                             case "21":   //到付
                                                 ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
                                                 break;
                                             case "41":   //代收貨款
                                                 ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
                                                 break;
                                             case "25":   //元付-到付追加
                                                 ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
                                                 break;
                                         }
                                         ttExlApp.RowC += 1;
                                         switch (pricing_type)
                                         {
                                             case "01":  //論板
                                             case "02":  //論件
                                             case "04":  //論小板
                                                 ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
                                                 ttExlApp.RowC += 1;
                                                 ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
                                                 ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                                 ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                                 break;
                                             case "03":  //論才
                                                 ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
                                                 ttExlApp.RowC += 1;
                                                 ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
                                                 ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                                 ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                                 break;
                                         }
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
                                         ttExlApp.RowC += 1;
                                         ttStr = "";
                                         if (receive_tel1 != "") ttStr += receive_tel1;
                                         if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
                                         if (receive_tel2 != "")
                                         {
                                             ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
                                         }
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
                                         ttExlApp.RowC += 1;
                                         ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);



                                         switch (s_tmp)
                                         {
                                             case 3:
                                                 ttExlApp.RowC -= 40;
                                                 s_tmp += 1;
                                                 break;
                                             case 6:
                                                 if ((i < DT.Rows.Count - 1) || (j < printplates))
                                                 {
                                                     ttExlApp.NextPage();
                                                 }
                                                 s_tmp = 1;
                                                 break;
                                             default:
                                                 s_tmp += 1;
                                                 break;
                                         }
                                     }

                                 }


                                 #region 清除後面框線

                                 switch (s_tmp)
                                 {
                                     //case 1:
                                     //    ttExlApp.GoToLine(2);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(4);
                                     //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(7);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(10);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(11);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(16);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(18);
                                     //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(21);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(24);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(25);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(30);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(32);
                                     //    ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(35);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(38);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(39);
                                     //    ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(1);
                                     //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                     //    ttExlApp.GoToLine(2);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(4);
                                     //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(7);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(10);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(11);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(16);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(18);
                                     //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(21);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(24);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(25);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(30);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(32);
                                     //    ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(35);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(38);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(39);
                                     //    ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                     //    ttExlApp.GoToLine(1);
                                     //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                     //    break;
                                     case 2:
                                         ttExlApp.GoToLine(16);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(18);
                                         ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(21);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(24);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(25);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(15);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                         ttExlApp.GoToLine(2);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(4);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(7);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(10);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(11);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(16);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(18);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(21);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(24);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(25);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(1);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         break;
                                     case 3:
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(3, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(1, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(29);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(1, 9, ttExlApp.RowC, ttExlApp.RowC + 41, "style3", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                         ttExlApp.GoToLine(2);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(4);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(7);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(10);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(11);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(16);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(18);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(21);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(24);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(25);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(1);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         break;
                                     case 4:
                                         ttExlApp.GoToLine(2);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(4);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(7);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(10);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(11);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(16);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(18);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(21);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(24);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(25);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(1);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                         break;
                                     case 5:
                                         ttExlApp.GoToLine(16);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(18);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(21);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(24);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(25);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(15);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         break;
                                     case 6:

                                         ttExlApp.GoToLine(30);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(32);
                                         ttExlApp.Setvalue(13, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(35);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(38);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(39);
                                         ttExlApp.Setvalue(11, ttExlApp.RowC, "", 3);
                                         ttExlApp.GoToLine(29);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBUp, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBDown, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBLeft, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);
                                         ttExlApp.BorderLine(11, 19, ttExlApp.RowC, ttExlApp.RowC + 41, "style4", TTBorderMode.TTBRight, TTLineWeight.xlThin, TTLineStyle.xlLineStyleNone);

                                         break;

                                 }
                                 #endregion
                             }
                             #endregion
                             break;
                         case "1":
                             break;
                         case "2":
                             ttExlApp.SRow = 1;
                             ttExlApp.SCol = 1;
                             ttExlApp.Line = 13;
                             ttExlApp.Column = 9;
                             ttExlApp.NextPage();
                             ttExlApp.GoToLine(1);

                             if (DT.Rows.Count > 0)
                             {
                                 for (int i = 0; i < DT.Rows.Count; i++)
                                 {
                                     print_date = Convert.ToDateTime(DT.Rows[i]["print_date"]).ToString("yyyy/MM/dd");
                                     receipt_flag = Convert.ToBoolean(DT.Rows[i]["receipt_flag"]);
                                     pallet_recycling_flag = Convert.ToBoolean(DT.Rows[i]["pallet_recycling_flag"]);
                                     supplier_date = Convert.ToDateTime(DT.Rows[i]["supplier_date"]).ToString("MMdd");
                                     pricing_type = DT.Rows[i]["pricing_type"].ToString();
                                     check_number = DT.Rows[i]["check_number"].ToString();
                                     subpoena_category = DT.Rows[i]["subpoena_category"].ToString();
                                     subpoena_category_name = DT.Rows[i]["code_name"].ToString();
                                     receive_tel1 = DT.Rows[i]["receive_tel1"].ToString();
                                     receive_tel1_ext = DT.Rows[i]["receive_tel1_ext"].ToString();
                                     receive_tel2 = DT.Rows[i]["receive_tel2"].ToString();
                                     receive_contact = DT.Rows[i]["receive_contact"].ToString();
                                     receive_city = DT.Rows[i]["receive_city"].ToString();
                                     receive_area = DT.Rows[i]["receive_area"].ToString();
                                     receive_address = DT.Rows[i]["address"].ToString();
                                     pieces = DT.Rows[i]["pieces"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["pieces"]) : 0;
                                     plates = DT.Rows[i]["plates"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["plates"]) : 0;
                                     cbm = DT.Rows[i]["cbm"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["cbm"]) : 0;
                                     collection_money = DT.Rows[i]["collection_money"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["collection_money"]) : 0;
                                     arrive_to_pay_freight = DT.Rows[i]["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_freight"]) : 0;
                                     arrive_to_pay_append = DT.Rows[i]["arrive_to_pay_append"] != DBNull.Value ? Convert.ToInt32(DT.Rows[i]["arrive_to_pay_append"]) : 0;
                                     send_contact = DT.Rows[i]["send_contact"].ToString();
                                     send_tel = DT.Rows[i]["send_tel"].ToString();
                                     send_city = DT.Rows[i]["send_city"].ToString();
                                     send_area = DT.Rows[i]["send_area"].ToString();
                                     send_address = DT.Rows[i]["send_address"].ToString();
                                     invoice_desc = DT.Rows[i]["invoice_desc"].ToString();
                                     area_arrive_code = DT.Rows[i]["area_arrive_code"].ToString();


                                     ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, print_date, 3);  //發送日期
                                     ttStr = "";

                                     switch (pricing_type)
                                     {
                                         case "01":  //論板
                                             pricing_type_name = "論板";
                                             break;
                                         case "02":  //論件
                                             pricing_type_name = "論件";
                                             break;
                                         case "04":  //論小板
                                             pricing_type_name = "論小板";
                                             break;
                                         case "03":  //論才
                                             pricing_type_name = "論才";
                                             break;
                                     }
                                     ttStr += pricing_type_name;

                                     if (receipt_flag == true)
                                         ttStr += "回單";
                                     if (pallet_recycling_flag == true)
                                         ttStr += "棧板回收";
                                     ttExlApp.Setvalue(6 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, supplier_date, 3);
                                     ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, area_arrive_code, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, subpoena_category_name, 3);
                                     switch (subpoena_category)
                                     {
                                         //case "11":   //元付
                                         //    break;
                                         case "21":   //到付
                                             ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_freight.ToString(), 3);
                                             break;
                                         case "41":   //代收貨款
                                             ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, collection_money.ToString(), 3);
                                             break;
                                         case "25":   //元付-到付追加
                                             ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, arrive_to_pay_append.ToString(), 3);
                                             break;
                                     }
                                     ttExlApp.RowC += 1;
                                     switch (pricing_type)
                                     {
                                         case "01":  //論板
                                         case "02":  //論件
                                         case "04":  //論小板
                                             ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "總板數", 3);
                                             ttExlApp.RowC += 1;
                                             ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, plates.ToString(), 3);
                                             ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                             ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                             break;
                                         case "03":  //論才
                                             ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, "才數", 3);
                                             ttExlApp.RowC += 1;
                                             ttExlApp.Setvalue(1 + addcolumn, ttExlApp.RowC, cbm.ToString(), 3);
                                             ttExlApp.Setvalue(3 + addcolumn, ttExlApp.RowC, pieces.ToString(), 3);
                                             ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, "a" + check_number + "a", 3);
                                             break;
                                     }
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(4 + addcolumn, ttExlApp.RowC, check_number, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_contact, 3);
                                     ttExlApp.RowC += 1;
                                     ttStr = "";
                                     if (receive_tel1 != "") ttStr += receive_tel1;
                                     if (receive_tel1 != "" && receive_tel1_ext != "") ttStr += "#" + receive_tel1_ext;
                                     if (receive_tel2 != "")
                                     {
                                         ttStr = (ttStr != "") ? ttStr + "," + receive_tel2 : receive_tel2;
                                     }
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, ttStr, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, receive_city + receive_area + receive_address, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, invoice_desc, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_contact, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_tel, 3);
                                     ttExlApp.RowC += 1;
                                     ttExlApp.Setvalue(2 + addcolumn, ttExlApp.RowC, send_city + send_area + send_address, 3);

                                     if (i < DT.Rows.Count - 1) ttExlApp.NextPage();


                                 }
                             }
                             break;
                     }

                     ttExlApp.DeleteTable(1);
                     //Workbook workbook = new Workbook();
                     //workbook.LoadFromFile(filename);
                     //workbook.SaveToFile("result.pdf", FileFormat.PDF);

                     FileInfo fi = new FileInfo(strSampleFileFullName);
                     string FileName = String.Empty;
                     if (Request.Url.Host.IndexOf("localhost") >= 0)
                     {
                         //本機→轉PDF
                         FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                         string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                         string pathDownload = Path.Combine(pathUser, "Downloads", FileName);
                         ttExlApp.Save(pathDownload, TableFormat.TFPDF);
                         BasePage.popDownload(Path.Combine(pathUser, "Downloads"), FileName, true);

                     }
                     else
                     {
                         //Server→轉Excel
                         Boolean IsTest = false; //是否為測試模式
                         FileName = string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), ".pdf");
                         string ExportPath = "files/" + FileName;

                         try
                         {
                             ttExlApp.Save(HttpContext.Current.Server.MapPath(ExportPath), TableFormat.TFPDF);

                             if (!IsTest)
                             {
                                 ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                             }
                             else
                             {
                                 ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>console.log('" + ExportPath + "');window.open('" + ExportPath + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);
                             }
                         }
                         catch (Exception ex)
                         {
                             string strErr = string.Empty;
                             if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                             strErr = "標籤列印-" + Request.RawUrl + strErr + ": " + ex.ToString();


                             //錯誤寫至Log
                             PublicFunction _fun = new PublicFunction();
                             _fun.Log(strErr, "S");
                         }
                     }

                     //string outputFolder = string.Empty;                                             //匯出檔路徑
                     //string outputFileName = string.Empty;
                     //FileInfo fi = new FileInfo(strSampleFileFullName);
                     //outputFolder = Request.PhysicalApplicationPath + @"files\";
                     //outputFileName =string.Format("{0}-{1}{2}", fi.Name.Replace(fi.Extension, ""), BasePage.checkGetDateTime(DateTime.Now, "yyyMMddHHmmss"), fi.Extension);

                     //try
                     //{ ttExlApp.Save(outputFolder + outputFileName, TableFormat.TFPDF); }
                     //catch (Exception ex)
                     //{
                     //    string strErr = string.Empty;
                     //    if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
                     //    strErr = "標籤列印-" + Request.RawUrl + strErr + ": " + ex.ToString();


                     //    //錯誤寫至Log
                     //    PublicFunction _fun = new PublicFunction();
                     //    _fun.Log(strErr, "S");

                     //    //outputFileName = "label-20170427071500.xls";
                     //}

                     //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + ":10080/files/" + outputFileName  + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);  //iis                    
                     //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>window.open('http://" + Request.Url.Authority + "/files/" + outputFileName  + "','mywindow','width=800,height=600, toolbar=yes');</script>", false);   //local


                     //Response.AddHeader(
                     // "Content-Disposition",
                     // string.Format("attachment; filename=label-20170427071500.pdf"));
                     //Response.HeaderEncoding = System.Text.Encoding.GetEncoding("big5");
                     //Response.WriteFile(Request.PhysicalApplicationPath + @"files\abel-20170427071500.pdf");
                     //Response.End();


                 }
                 catch (Exception ex)
                 {
                     ttErrStr = "開啟範本失敗-E-" + ex.Message;
                     ttExlApp = null;
                     ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ttErrStr + "');</script>", false);
                 }

             }
             catch (Exception ex)
             {
                 //ExceptionAdapter.logSysException(ex);
                 //throw;
                 ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('" + ex.Message + "');</script>", false);
             }
         }
         else
         {

             //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('無可列印標籤');</script>", false);
             //return;
         }


     }*/


    /// <summary>
    /// 列印標籤(捲筒列印)(依計價模式為列印張數單位)
    /// </summary>
    /// <param name="print_requestid"></param>
    private void PrintReelLabel(string print_requestid)
    {
        if (print_requestid != "")
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string wherestr = " and request_id in(" + print_requestid + ")";
                string orderby = " order by  A.check_number";
                if (master_code == "0000049") orderby = " order by  A.cdate , A.request_id"; //三陽工業，指定依打單的順序列印
                cmd.CommandText = string.Format(@"select isnull(D.code_name,'') + CASE A.receipt_flag WHEN 1 THEN '回單' ELSE '' END + CASE A.pallet_recycling_flag WHEN 1 THEN '棧板回收' ELSE '' END  AS title1,
                                                  A.print_date ,
                                                  A.supplier_date,
                                                  A.area_arrive_code,
                                                  B.code_name as subpoena_category_name,
                                                  CASE A.subpoena_category WHEN '21' THEN a.arrive_to_pay_freight WHEN '41' THEN A.collection_money WHEN '25' THEN A.arrive_to_pay_append END as money ,
                                                  CASE A.pricing_type WHEN '03' THEN '才數' ELSE '總板數' END  as  title2, 
                                                  CASE A.pricing_type WHEN '03' THEN  A.cbm ELSE A.plates  END  as plates,
                                                  A.pieces, 
                                                  A.check_number , 
                                                  'a'+ A.check_number +'a' as barcode,
                                                  A.receive_contact,
                                                  CASE WHEN A.receive_tel1_ext <> '' THEN A.receive_tel1 + ' #' +  A.receive_tel1_ext  ELSE A.receive_tel1 END  +  ' ' + A.receive_tel2 AS receive_tel,
                                                  A.receive_city + A.receive_area + A.receive_address as receive_address,
                                                  A.invoice_desc,
                                                  A.send_contact,
                                                  A.send_tel ,
                                                  A.send_city + A.send_area + A.send_address as send_address,
	                                              CASE A.pricing_type WHEN '01' THEN A.plates WHEN  '02' THEN A.pieces WHEN '03' THEN A.cbm ELSE A.plates END as pages
                                                  from tcDeliveryRequests A WITH(Nolock)
                                                  left join tbItemCodes B on B.code_id = A.subpoena_category and code_bclass = '2' and code_sclass = 'S2'
                                                  left join tbCustomers C on C.customer_code = A.customer_code
                                                  left join tbItemCodes D on D.code_id  = A.pricing_type  and d.code_sclass  = 'PM'
                                                  where 1= 1  {0} {1}", wherestr, orderby);
                DataTable DT = dbAdapter.getDataTable(cmd);
                if (DT != null)
                {
                    DataTable dt2 = new DataTable();
                    dt2 = DT.Clone();  //複製DT的結構
                    for (int i = 0; i <= DT.Rows.Count - 1; i++)
                    {
                        DT.Rows[i]["barcode"] = Convert.ToBase64String(MakeBarcodeImage(DT.Rows[i]["barcode"].ToString()));
                        int pages = DT.Rows[i]["pages"] == DBNull.Value ? 1 : Convert.ToInt32(DT.Rows[i]["pages"]);
                        for (int j = 1; j <= pages; j++)
                        {
                            dt2.ImportRow(DT.Rows[i]);
                        }
                    }
                    string[] ParName = new string[0];
                    string[] ParValue = new string[0];
                    PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "ReelLabel3", ParName, ParValue, "dsRPT_DeliveryRequests", dt2);
                }


            }

        }


    }

    public byte[] MakeBarcodeImage(string datastring)
    {
        string sCode = String.Empty;

        System.IO.MemoryStream oStream = new System.IO.MemoryStream();
        try
        {
            System.Drawing.Image oimg = GenerateBarCodeBitmap(datastring);
            oimg.Save(oStream, System.Drawing.Imaging.ImageFormat.Png);
            oimg.Dispose();
            return oStream.ToArray();
        }
        finally
        {

            oStream.Dispose();
        }
    }

    public static System.Drawing.Image GenerateBarCodeBitmap(string content)
    {
        using (var barcode = new Barcode()
        {

            IncludeLabel = true,
            Alignment = AlignmentPositions.CENTER,
            Width = 250,
            Height = 50,
            LabelFont = new Font("Arial Rounded MT Bold", 10, FontStyle.Bold),
            RotateFlipType = RotateFlipType.RotateNoneFlipNone,
            BackColor = Color.White,
            ForeColor = Color.Black,
        })
        {
            return barcode.Encode(TYPE.Codabar, content);
        }
    }

    public bool IsValidCheckNumber(string check_number)
    {
        bool IsVaild = true;
        if (check_number.Length != 10)
        {
            IsVaild = false;
        }
        else
        {
            int num = Convert.ToInt32(check_number.Substring(0, 9));

            int chk = num % 7;
            int lastnum = Convert.ToInt32(check_number.Substring(9, 1));
            if (lastnum != chk)
            {
                IsVaild = false;
            }

        }
        return IsVaild;
    }

    private string GetScodeByCityAndArea(string city, string area)
    {
        SqlCommand cmd = new SqlCommand();
        string result = "";
        cmd.CommandText = string.Format(@"select A.* , A.supplier_code  + ' '+  B.supplier_name as showname    from ttArriveSites A
                                                   left join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                                   where A.supplier_code <> '' and  post_city='{0}' and post_area='{1}'", city, area);      //從1_2抄來的邏輯

        DataTable dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            result = dt.Rows[0]["supplier_code"].ToString();
        }
        return result;
    }

    private CityAndArea GetCustomerShipCityAndAreaByCustomerCode(string customerCode)
    {
        SqlCommand cmd = new SqlCommand();
        CityAndArea result = new CityAndArea();
        cmd.CommandText = string.Format(@"select shipments_city, shipments_area from tbCustomers with(nolock) where customer_code = '{0}'", customerCode);      //從1_2抄來的邏輯

        DataTable dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            result.City = dt.Rows[0]["shipments_city"].ToString();
            result.Area = dt.Rows[0]["shipments_area"].ToString();
        }
        return result;
    }

    private string GetPalletExclusiveSendStationByCustomerCode(string customerCode)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = string.Format(@"select pallet_exclusive_send_station from tbCustomers with(nolock) where customer_code = '{0}'", customerCode);      //從1_2抄來的邏輯
        string result = "";
        DataTable dt = dbAdapter.getDataTable(cmd);
        if (dt.Rows.Count > 0)
        {
            result = dt.Rows[0]["pallet_exclusive_send_station"].ToString();
        }
        return result;
    }

    private class CityAndArea
    {
        public string City { get; set; }
        public string Area { get; set; }
    }
}