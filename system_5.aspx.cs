﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class system_5 : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔
        }
    }

    protected void btn_Search_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand())
        {
            string wherestr = "";
            cmd.Parameters.Clear();

            #region 關鍵字
            cmd.Parameters.AddWithValue("@Less_than_truckload", Less_than_truckload);
            if (rbPsn.Checked)
            {
                if (account.Text != "")
                {
                    cmd.Parameters.AddWithValue("@account_code", account.Text);
                    //wherestr += " and account_code = @account_code";
                }
                cmd.CommandText = string.Format(@"declare @@count int;
                                                  declare @@manager_type nvarchar(2);
                                                  select @@count =count(*) from tbFunctionsAuth where account_code  = @account_code;
                                                  IF (@@count =0) BEGIN
                                                  select @@manager_type=manager_type  from tbAccounts where account_code  = @account_code;
                                                  select func.func_code , 
	                                                  CASE LEN(func.upper_level_code) WHEN 1 THEN '　'+　func.func_name WHEN 2 THEN '　　'+ func.func_name ELSE func.func_name END func_name,
	                                                  func.upper_level_code , func.func_link,
	                                                  auth. auth_id , auth.manager_type , auth.account_code , 
	                                                  isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
	                                                  isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
	                                                  auth.uuser , auth.udate 
	                                                  from tbFunctions func
	                                                  left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.manager_type   = @@manager_type and auth.func_type =@Less_than_truckload
                                                      where func.type =@Less_than_truckload
	                                                  order by func.func_code 
                                                  END ELSE BEGIN 
	                                                  select func.func_code , 
	                                                  CASE LEN(func.upper_level_code) WHEN 1 THEN '　'+　func.func_name WHEN 2 THEN '　　'+ func.func_name ELSE func.func_name END func_name,
	                                                  func.upper_level_code , func.func_link,
	                                                  auth. auth_id , auth.manager_type , auth.account_code , 
	                                                  isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
	                                                  isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
	                                                  auth.uuser , auth.udate 
	                                                  from tbFunctions func
	                                                  left join tbFunctionsAuth auth on auth.func_code = func.func_code  and auth.account_code  = @account_code and auth.func_type =@Less_than_truckload
                                                      where func.type =@Less_than_truckload
	                                                  order by func.func_code 
                                                  END; ");
            }
            else
            {
                if (dlRole.SelectedValue != "")
                {
                    cmd.Parameters.AddWithValue("@manager_type", dlRole.SelectedValue);
                    wherestr += " and manager_type = @manager_type";
                }
                cmd.CommandText = string.Format(@"Select func.func_code , 
                                                  CASE LEN(func.upper_level_code) WHEN 1 THEN '　'+　func.func_name WHEN 2 THEN '　　'+ func.func_name ELSE func.func_name END func_name,
                                                  func.upper_level_code , func.func_link,
                                                  auth. auth_id , auth.manager_type , auth.account_code , 
                                                  isnull(auth.view_auth,0)  view_auth, isnull(auth.insert_auth,0)  insert_auth, 
                                                  isnull( auth.modify_auth,0) modify_auth, isnull(auth.delete_auth,0) delete_auth , 
                                                  auth.uuser , auth.udate 
                                                  from tbFunctions func
                                                  left join tbFunctionsAuth auth on auth.func_code = func.func_code and auth.func_type =@Less_than_truckload  {0}
                                                  where func.type =@Less_than_truckload
                                                  order by func.func_code ", wherestr);
            }

            #endregion



            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                New_List.DataSource = dt;
                New_List.DataBind();
            }   
        }

        if (UpdatePanel2.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel2.Update();
        }
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>DisabledChk()</script>", false);
        
    }
    

    protected void btn_Modify_Click(object sender, EventArgs e)
    {

    }

    protected void radio_CheckedChanged(object sender, EventArgs e)
    {

        RadioButton  radio = (RadioButton)sender;
        if (radio != null)
        {
            switch (radio.ID)
            {
                case "rbPsn":   // 個人
                    rbRole.Checked = false;
                    divpsn.Visible = true  ;
                    req1.ControlToValidate = "account";
                    break;
                case "rbRole":  // 角色
                    rbPsn.Checked = false;
                    divpsn.Visible = false;
                    req1.ControlToValidate = "dlRole";
                    break;
            }
        }
        btn_Cancel_Click(null, null);
        New_List.DataSource = null;
        New_List.DataBind();
        if (UpdatePanel2.UpdateMode == UpdatePanelUpdateMode.Conditional)
        {
            UpdatePanel2.Update();
        }
    }

    protected void btn_Save_Click(object sender, EventArgs e)
    {
        string ttErrStr = string.Empty;
        if (New_List.Items.Count > 0)
        {
            StringBuilder sb_temp_ins = new StringBuilder();
            string manager_type = "";
            string account_code = "";
            string func_code = "";
            if (rbPsn.Checked == true)
            {
                // 個人
                account_code = account.Text;
                sb_temp_ins.Append("Delete from tbFunctionsAuth where account_code = '"+ account_code + "' and func_type="+ Less_than_truckload.ToString());
                for (int i = 0; i <= New_List.Items.Count - 1; i++)
                {
                    HiddenField auth_id = ((HiddenField)New_List.Items[i].FindControl("hid_id"));                    
                    HiddenField link = ((HiddenField)New_List.Items[i].FindControl("hid_link"));
                    if (auth_id != null && link.Value != "")
                    {
                        int id = auth_id.Value != "" ? Convert.ToInt32(auth_id.Value) : 0;
                        CheckBox chkView = ((CheckBox)New_List.Items[i].FindControl("chkView"));
                        CheckBox chkAdd = ((CheckBox)New_List.Items[i].FindControl("chkAdd"));
                        CheckBox chkModify = ((CheckBox)New_List.Items[i].FindControl("chkModify"));
                        CheckBox chkDel = ((CheckBox)New_List.Items[i].FindControl("chkDel"));
                        Literal lifunc = ((Literal)New_List.Items[i].FindControl("lifunc"));
                        func_code = lifunc.Text;


                        #region 新增
                        if (chkView.Checked || chkAdd.Checked || chkModify.Checked || chkDel.Checked)
                        {
                            string strInsVal = "Insert into  tbFunctionsAuth(manager_type,account_code,func_code,view_auth,insert_auth,modify_auth,delete_auth,etc_auth, func_type,cuser,cdate,uuser,udate) VALUES(" +
                                                   "'" + manager_type + "', " +
                                                   "'" + account_code + "', " +
                                                   "'" + func_code + "', " +
                                                   Convert.ToInt16(chkView.Checked).ToString() + ", " +
                                                   Convert.ToInt16(chkAdd.Checked).ToString() + ", " +
                                                   Convert.ToInt16(chkModify.Checked).ToString() + ", " +
                                                   Convert.ToInt16(chkDel.Checked).ToString() + ", " +
                                                   "0, " +
                                                   Less_than_truckload.ToString()+ ", " +
                                                   "'" + Session["account_code"] + "', " +
                                                   "GETDATE()" + ", " +
                                                   "'" + Session["account_code"] + "', " +
                                                   "GETDATE());";
                            sb_temp_ins.Append(strInsVal);
                        }
                        #endregion
                    }
                }
            }
            else
            {
                // 角色
                manager_type = dlRole.SelectedValue;
                for (int i = 0; i <= New_List.Items.Count - 1; i++)
                {

                    HiddenField auth_id = ((HiddenField)New_List.Items[i].FindControl("hid_id"));
                    HiddenField link = ((HiddenField)New_List.Items[i].FindControl("hid_link"));
                    if (auth_id != null && link.Value != "")
                    {
                        int id = auth_id.Value != "" ? Convert.ToInt32(auth_id.Value) : 0;
                        CheckBox chkView = ((CheckBox)New_List.Items[i].FindControl("chkView"));
                        CheckBox chkAdd = ((CheckBox)New_List.Items[i].FindControl("chkAdd"));
                        CheckBox chkModify = ((CheckBox)New_List.Items[i].FindControl("chkModify"));
                        CheckBox chkDel = ((CheckBox)New_List.Items[i].FindControl("chkDel"));
                        Literal lifunc = ((Literal)New_List.Items[i].FindControl("lifunc"));
                        func_code = lifunc.Text;


                        if (id > 0)
                        {
                            #region 修改
                            string strInsVal = "Update tbFunctionsAuth set " +
                                                        "view_auth = " + Convert.ToInt16(chkView.Checked).ToString() + ", " +
                                                        "insert_auth = " + Convert.ToInt16(chkAdd.Checked).ToString() + ", " +
                                                        "modify_auth = " + Convert.ToInt16(chkModify.Checked).ToString() + ", " +
                                                        "delete_auth = " + Convert.ToInt16(chkDel.Checked).ToString() + ", " +
                                                        "uuser = '" + Session["account_code"] + "', " +
                                                        "udate  =GETDATE()" +
                                                        " WHERE auth_id='" + id.ToString() + "';";
                            sb_temp_ins.Append(strInsVal);
                            #endregion
                        }
                        else
                        {
                            #region 新增
                            if (chkView.Checked || chkAdd.Checked || chkModify.Checked || chkDel.Checked)
                            {
                                string strInsVal = "Insert into  tbFunctionsAuth(manager_type,account_code,func_code,view_auth,insert_auth,modify_auth,delete_auth,etc_auth,func_type,cuser,cdate,uuser,udate) VALUES(" +
                                                       "'" + manager_type + "', " +
                                                       "'" + account_code + "', " +
                                                       "'" + func_code + "', " +
                                                       Convert.ToInt16(chkView.Checked).ToString() + ", " +
                                                       Convert.ToInt16(chkAdd.Checked).ToString() + ", " +
                                                       Convert.ToInt16(chkModify.Checked).ToString() + ", " +
                                                       Convert.ToInt16(chkDel.Checked).ToString() + ", " +
                                                       "0, " +
                                                       Less_than_truckload.ToString() + ", " +
                                                       "'" + Session["account_code"] + "', " +
                                                       "GETDATE()" + ", " +
                                                       "'" + Session["account_code"] + "', " +
                                                       "GETDATE());";
                                sb_temp_ins.Append(strInsVal);
                            }

                        }
                        #endregion
                    }
                }
            }
            

            if (sb_temp_ins.Length > 0)
            {
                String strSQL = sb_temp_ins.ToString();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = strSQL;
                    try
                    {
                        dbAdapter.execNonQuery(cmd);
                    }
                    catch (Exception ex)
                    {
                        ttErrStr = ex.Message;
                    }
                }
            }
        }
        btn_Search.Visible = true;
        btn_Modify.Visible = true;
        btn_Save.Visible = false ;
        btn_Cancel.Visible = false;

        if (ttErrStr != "")
        {
            //組錯誤訊息
            string strErr = string.Empty;
            if (Session["account_code"] != null) strErr = "【" + Session["account_code"] + "】";
            strErr = "權限設定-" + Request.RawUrl + strErr + ": " + ttErrStr ;

            //錯誤寫至Log
            PublicFunction _fun = new PublicFunction();
            _fun.Log(strErr, "S");
        }
        else
        {
            string sScript = "$(':checkbox').each(function () { " +
                             "$(this).prop('disabled', true);" +
                             "}); ";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>" + sScript +"alert('儲存成功');</script>", false);
            return;
        }

    }

    protected void btn_Modify_Click1(object sender, EventArgs e)
    {
        
        btn_Search.Visible = false;
        btn_Modify.Visible = false;
        btn_Save.Visible = true;
        btn_Cancel.Visible = true;
    }

    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        btn_Search.Visible = true ;
        btn_Modify.Visible = true;
        btn_Save.Visible = false ;
        btn_Cancel.Visible = false;
    }
}