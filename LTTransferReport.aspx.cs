﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LTTransferReport : System.Web.UI.Page
{
    public string Less_than_truckload
    {
        get { return ViewState["Less_than_truckload"] != null ? ViewState["Less_than_truckload"].ToString() : "0"; }
        set { ViewState["Less_than_truckload"] = value; }
    }

    public string manager_type
    {
        get { return ViewState["manager_type"].ToString(); }
        set { ViewState["manager_type"] = value; }
    }

    public string supplier_code
    {
        // for權限
        get { return ViewState["supplier_code"].ToString(); }
        set { ViewState["supplier_code"] = value; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            #region  Log
            PublicFunction _fun = new PublicFunction();
            _fun.ExeOpLog(Session["account_code"].ToString(), System.IO.Path.GetFileName(Request.PhysicalPath), "");
            #endregion

            Less_than_truckload = Session["type"] != null ? Session["type"].ToString() : "0";   //棧板or零擔

            string Distribute_date = DateTime.Today.ToString("yyyy/MM/dd");
            date1.Text = Distribute_date;

            manager_type = Session["manager_type"].ToString(); //管理單位類別   
            switch (manager_type)
            {
                case "0":
                case "1":
                case "2":
                    supplier_code = "";
                    //一般員工/管理者
                    //有綁定站所只該該站所客戶
                    //無則可查所有站所下的所有客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select tbEmps.station , tbStation.station_code   from tbEmps with(nolock) 
                                            inner join tbAccounts  with(nolock) on tbAccounts.emp_code = tbEmps.emp_code 
                                            left join tbStation with(nolock) on tbEmps.station = tbStation.id 
                                            where tbAccounts.account_code  =@account_code";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                        else
                        {

                        }
                    }
                    break;
                case "4":                 //站所主管只能看該站所下的客戶
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.AddWithValue("@account_code", Session["account_code"].ToString());
                        cmd.CommandText = @"select station_code  from tbStation where id =( select station  from tbEmps where emp_code =@account_code)";
                        DataTable dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            supplier_code = dt.Rows[0]["station_code"].ToString();
                        }
                    }
                    break;
                case "5":
                    supplier_code = Session["customer_code"].ToString();   //客戶只能看到自已
                    supplier_code = (supplier_code.Length >= 3) ? supplier_code.Substring(0, 3) : "";
                    break;
            }
            string wherestr = "";

            #region 
            SqlCommand cmd1 = new SqlCommand();

            if (supplier_code != "")
            {
                cmd1.Parameters.AddWithValue("@supplier_code", supplier_code);
                wherestr = " and station_code = @supplier_code";
            }

            cmd1.CommandText = string.Format(@"Select id, station_code, station_scode, station_name, station_scode + '-' +station_name as showname
                                               from tbStation with(nolock) 
                                               where 0=0 and active_flag =1  {0}
                                               order by station_code", wherestr);

            ddlSendStation.DataSource = dbAdapter.getDataTable(cmd1);
            ddlSendStation.DataValueField = "station_scode";
            ddlSendStation.DataTextField = "showname";
            ddlSendStation.DataBind();
            if (manager_type == "0" || manager_type == "1")
            {
                ddlSendStation.Items.Insert(0, new ListItem("全部", ""));
            }
            else if (supplier_code == "" && manager_type == "2")
            {
                ddlSendStation.Items.Insert(0, new ListItem("全部", ""));
            }
            if (ddlSendStation.Items.Count > 0) ddlSendStation.SelectedIndex = 0;
            ddlSendStation_SelectedIndexChanged(null, null);
            #endregion

            if (Request.QueryString["date1"] != null)
            {
                if (Request.QueryString["date1"] != "")
                {
                    date1.Text = Request.QueryString["date1"];
                }
            }

            if (Request.QueryString["Suppliers"] != null)
            {

                ddlSendStation.SelectedValue = Request.QueryString["Suppliers"];
                ddlSendStation_SelectedIndexChanged(null, null);
            }

            if (Request.QueryString["check_number"] != null)
            {
                check_number.Text = Request.QueryString["check_number"];
            }


            SqlCommand cmds = new SqlCommand();
            string sqlwhere = "";
            cmds.CommandTimeout = 600;

            cmds.CommandText = @" 
            SELECT count(A.TransferID)
			FROM ttTransferLog A With(Nolock) 
			WHERE 1 = 1 
	        AND CONVERT(VARCHAR,A.create_dt,111) > = CONVERT(VARCHAR,@sdt,111) and CONVERT(VARCHAR,A.create_dt,111) < CONVERT(VARCHAR,@edt,111)
		    AND (A.station_code = @station_code or @station_code = '')";

            cmds.Parameters.AddWithValue("@sdt", date1.Text);
            cmds.Parameters.AddWithValue("@edt", Convert.ToDateTime(date1.Text).AddDays(1));
            cmds.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue);

            DataTable dtsum = dbAdapter.getDataTable(cmds);
            if (dtsum.Rows.Count > 0)
            {
                lbSuppliers.Text = ddlSendStation.SelectedValue;
                totle.Text = dtsum.Rows[0][0].ToString() + "筆";
            }
            if (Request.QueryString["date1"] != null)
            {
                readdata();
            }
        }
    }

    private void readdata()
    {
        string querystring = "";
        using (SqlCommand cmd = new SqlCommand())
        {

            cmd.Parameters.AddWithValue("@sdt", date1.Text);
            cmd.Parameters.AddWithValue("@edt", Convert.ToDateTime(date1.Text).AddDays(1));

            querystring += "&date1=" + date1.Text;

            cmd.Parameters.AddWithValue("@station_code", ddlSendStation.SelectedValue.ToString());
            querystring += "&Suppliers=" + ddlSendStation.SelectedValue.ToString();

            cmd.Parameters.AddWithValue("@check_number", check_number.Text.ToString());
            querystring += "&check_number=" + check_number.Text.ToString();


            cmd.CommandText = string.Format(@"
            SELECT ROW_NUMBER() OVER(ORDER BY A.TransferID) AS NO ,
		    A.station_code + ' ' + C.station_name as station_code, 
            A.driver_code+ ' ' + B.driver_name as driver_code, 
            scanning_dt,
            check_number,
            case when  A.driver_code ='PP990' then '國陽' when A.driver_code ='PP490' then '明錩' else '' end Transfer,
            TransferID
			FROM ttTransferLog A With(Nolock) 
			left join tbDrivers B on B.driver_code = A.driver_code
			left join tbStation C on C.station_scode = A.station_code
			 WHERE 1 = 1 
	          AND CONVERT(VARCHAR,A.create_dt,111) >= CONVERT(VARCHAR,@sdt,111) and CONVERT(VARCHAR,A.create_dt,111) < CONVERT(VARCHAR,@edt,111)
			  AND (A.station_code = @station_code or @station_code = '')
              AND (@check_number ='' or A.check_number = @check_number )");
            cmd.CommandTimeout = 600;

            using (DataTable dt = dbAdapter.getDataTable(cmd))
            {
                ltotalpages.Text = dt.Rows.Count.ToString();
                New_List.DataSource = dt;
                New_List.DataBind();
            }
        }
    }


    private void paramlocation()
    {
        string querystring = "";
        if (date1.Text.ToString() != "")
        {
            querystring += "&date1=" + date1.Text.ToString();
        }

        querystring += "&Suppliers=" + ddlSendStation.SelectedValue.ToString();
        querystring += "&check_number=" + check_number.Text.ToString();

        Response.Redirect(ResolveUrl("~/LTTransferReport.aspx?search=yes" + querystring));
    }

    protected void btnQry_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    //protected void btnExcel_Click(object sender, EventArgs e) {
    //    string sheet_title = "代收，到付";
    //    string strWhereCmd1 = "";
    //    string strWhereCmd2 = "";
    //    string file_name = "代收，到付表" + DateTime.Now.ToString("yyyyMMdd");

    //}



    protected void Check_Clicked(object sender, EventArgs e)
    {

        //for (int i = 0; i <= New_List.Items.Count - 1; i++)
        //{
        //    CheckBox cbSel = ((CheckBox)New_List.Items[i].FindControl("chkRow"));
        //    cbSel.Checked = cbAll.Checked;
        //}
    }

    protected void ddlSendStation_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        string request_id = ((HiddenField)e.Item.FindControl("hid_id")).Value.ToString().Trim();

        string script = string.Empty;
        switch (e.CommandName)
        {
            case "cmdEdit":
                FillData(request_id);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>$('#modal-default_01').modal();" + script + "</script>", false);
                break;

        
        }
    }

    protected void FillData(string request_id)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }

        hid_request_id.Value = request_id;
        //#region 郵政縣市
        //using (SqlCommand cmd = new SqlCommand())
        //{
        //    cmd.CommandText = "select * from tbPostCity order by seq asc ";
        //    ddlsend_city.DataSource = dbAdapter.getDataTable(cmd);
        //    ddlsend_city.DataValueField = "city";
        //    ddlsend_city.DataTextField = "city";
        //    ddlsend_city.DataBind();
        //    ddlsend_city.Items.Insert(0, new ListItem("請選擇", ""));

        //    ddlsend_area.Items.Insert(0, new ListItem("請選擇", ""));
        //    ddlzip.Items.Insert(0, new ListItem("請選擇", ""));
        //}
        //#endregion

        //try
        //{
        //    DataTable dt = new DataTable();
        //    using (SqlCommand cmd = new SqlCommand())
        //    {
        //        cmd.Parameters.AddWithValue("@request_id", request_id);

        //        cmd.CommandText = string.Format(@"
        //select S.*,A2.zip from (
        //select A.request_id 
        //, A.customer_code							      
        //,isnull((select check_number from tcDeliveryRequests where check_number = A.send_contact),A.check_number) oldcheck_number	        --原貨號
        //,ts.station_scode	
        //,A.supplier_code 	      
        //,case isnull((select top 1 Receive_contact from tcDeliveryRequests where check_number = A.send_contact  order by cdate desc),'') when '' then A.send_contact
        //	else
        // isnull((select top 1 Receive_contact from tcDeliveryRequests where check_number = A.send_contact  order by cdate desc),'') end send_contact   --退貨人
        // , A.send_city		--退貨人(縣/市)
        // , A.send_area		--退貨人(區)
        //,  A.send_address	--退貨人(地址)
        //, send_tel		--退貨人電話
        //,A.receive_contact																						--收貨人
        //,A.receive_tel1																						    --收貨人電話
        //,ISNULL(A.receive_city,'') + ISNULL(A.receive_area,'') + ISNULL(A.receive_address,'') receive_address	--收貨人地址
        //,A.invoice_desc							                                                                                            --備註
        //from tcDeliveryRequests A With(Nolock)
        //LEFT join ttArriveSitesScattered ta With(Nolock)  on ta.post_city = A.send_city and ta.post_area = A.send_area
        //left join tbStation ts With(Nolock)  on  ta.station_code = ts.station_scode 
        //where A.request_id = @request_id
        //) S left join [ttArriveSites] A2 With(Nolock) on A2.post_city = S.send_city and A2.post_area = S.send_area");
        //        dt = dbAdapter.getDataTable(cmd);
        //    }

        //    if (dt.Rows.Count > 0)
        //    {
        //        lbstation_scode.Text = dt.Rows[0]["station_scode"].ToString().Replace("F", "");       //流水號
        //        hid_request_id.Value = dt.Rows[0]["request_id"].ToString();             //流水號
        //        lbcustomer_code.Text = dt.Rows[0]["customer_code"].ToString();
        //        strkey.Text = dt.Rows[0]["oldcheck_number"].ToString();
        //        lbreceive_contact.Text = dt.Rows[0]["receive_contact"].ToString();
        //        lbreceive_tel1.Text = dt.Rows[0]["receive_tel1"].ToString();
        //        lbreceive_address.Text = dt.Rows[0]["receive_address"].ToString();
        //        invoice_desc.InnerText = dt.Rows[0]["invoice_desc"].ToString();
        //        strsend_contact.Text = dt.Rows[0]["send_contact"].ToString();
        //        strsend_tel.Text = dt.Rows[0]["send_tel"].ToString();
        //        ddlsend_city.SelectedValue = dt.Rows[0]["send_city"].ToString();
        //        city_SelectedIndexChanged(null, null);
        //        ddlsend_area.SelectedValue = dt.Rows[0]["send_area"].ToString();
        //        area_SelectedIndexChanged(null, null);
        //        strsend_address.Text = dt.Rows[0]["send_address"].ToString();

        //        hid_receive_city.Value = dt.Rows[0]["send_city"].ToString();
        //        hid_receive_area.Value = dt.Rows[0]["send_area"].ToString();
        //        hid_receive_address.Value = dt.Rows[0]["send_address"].ToString();

        //    }
        //}
        //catch (Exception ex)
        //{
        //    //_fun.Log(string.Format("【{0} Error】DataBind:{1}", "乘客帳號管理", ex.Message), "W");
        //}
    }

    protected void btn_OK_Click(object sender, EventArgs e)
    {
        if (loginchk.IsLogin() == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
            return;
        }
        if (hid_request_id.Value == "") {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('異常錯誤!!');</script>", false);
            return;
        }
            string _check_number = "";
            //更新 轉聯運明細表 轉運商資料
            using (SqlCommand cmd = new SqlCommand()) {
            cmd.Parameters.AddWithValue("@Transfer", rd1.SelectedValue);
            cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "TransferID", hid_request_id.Value);
            cmd.CommandText = dbAdapter.genUpdateComm("ttTransferLog", cmd);
            try
            {
                dbAdapter.execNonQuery(cmd);
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}