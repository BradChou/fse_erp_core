﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

public partial class member_4 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }

            if (Request.QueryString["customer_code"] != null)
            {
                txcustomer_code.Text = Request.QueryString["customer_code"];
            }

            if (Request.QueryString["customer_hcode"] != null)
            {
                txcustomer_hcode.Text = Request.QueryString["customer_hcode"];
            }

            readdata();
        }

    }

    private void readdata()
    {
        string querystring = "";
        SqlConnection conn = null;
        String strConn = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString();
        conn = new SqlConnection(strConn);
        SqlDataAdapter adapter = null;
        SqlCommand cmd = new SqlCommand();
        string strWhereCmd = "";

        cmd.Parameters.Clear();

        #region 關鍵字
        if (txcustomer_code.Text != "")
        {
            cmd.Parameters.AddWithValue("@customer_code", txcustomer_code.Text);
            strWhereCmd += " and A.customer_code like '%'+@customer_code+'%' ";
            querystring += "&customer_code=" + txcustomer_code.Text;
        }

        if (txcustomer_hcode.Text != "")
        {
            cmd.Parameters.AddWithValue("@customer_hcode", txcustomer_hcode.Text);
            strWhereCmd += " and A.customer_hcode like '%'+@customer_hcode+'%' ";
            querystring += "&customer_hcode=" + txcustomer_hcode.Text;
        }
        #endregion

        cmd.CommandText = string.Format(@"select ROW_NUMBER() OVER(ORDER BY A.customer_code) AS 'no',A.customer_id ,A.customer_hcode, A.customer_code, 
                                                 CASE A.supplier_code When '001' THEN N'新竹零擔' When '002' THEN N'新竹流通' END supplier_code
                                                 ,B.supplier_name , A.customer_shortname ,
                                                 A.shipments_city ,A.shipments_area , A.shipments_road , 
                                                 CASE WHEN CONVERT (DATETIME, A.contract_expired_date, 102)  > CONVERT (DATETIME, '1900/01/01', 102) THEN A.contract_expired_date  END AS contract_expired_date
                                            from tbCustomers A With(Nolock)
                                       left join tbSuppliers B With(Nolock) on A.supplier_code  = B.supplier_code 
                                           where 1= 1 and (customer_code like '001%' or customer_code like '002%' )and A.customer_code <> ''  {0} order by A.customer_code", strWhereCmd);

        cmd.Connection = conn;
        DataSet ds = new DataSet();
        adapter = new SqlDataAdapter(cmd);
        adapter.Fill(ds);
        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = ds.Tables[0].DefaultView;
        objPds.AllowPaging = true;



        objPds.PageSize = 10;



        #region 下方分頁顯示
        //一頁幾筆
        int CurPage = 0;
        if ((Request.QueryString["Page"] != null))
        {
            //控制接收的分頁並檢查是否在範圍
            if (IsNumeric(Request.QueryString["Page"]) && Request.QueryString["Page"].Length <= 15)
            {
                if ((Request.QueryString["Page"] != null) & Convert.ToInt64(Request.QueryString["Page"]) >= 1 && Convert.ToInt64(Request.QueryString["Page"]) <= Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
                {
                    CurPage = Convert.ToInt32(Request.QueryString["Page"].ToString());
                }
                else
                {
                    CurPage = 1;
                }
            }
            else
            {
                CurPage = 1;
            }
        }
        else
        {
            CurPage = 1;
        }
        objPds.CurrentPageIndex = (CurPage - 1);
        lnkfirst.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=1" + querystring));
        lnklast.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)).ToString() + querystring));
        //第一頁控制
        if (!objPds.IsFirstPage)
        {
            lnkfirst.Enabled = true;
            lnkPrev.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage - 1) + querystring));
        }
        else
        {
            lnkfirst.Enabled = false;
        }
        //最後一頁控制
        if (!objPds.IsLastPage)
        {
            lnkNext.NavigateUrl = Server.HtmlDecode(Server.HtmlEncode(Request.CurrentExecutionFilePath + "?Page=" + Convert.ToString(CurPage + 1) + querystring));
            lnklast.Enabled = true;
        }
        else
        {
            lnklast.Enabled = false;
        }

        //上一頁控制
        if (CurPage - 1 == 0)
        {
            lnkPrev.Enabled = false;
        }
        else
        {
            lnkPrev.Enabled = true;
        }

        //下一頁控制
        if (CurPage + 1 > Math.Ceiling(Convert.ToDouble(ds.Tables[0].Rows.Count) / Convert.ToDouble(objPds.PageSize)))
        {
            lnkNext.Enabled = false;
        }
        else
        {
            lnkNext.Enabled = true;
        }

        if (objPds.PageSize > 0)
        {
            tbPage.Text = CurPage.ToString() + "／" + objPds.PageCount.ToString();
        }

        #endregion

        New_List.DataSource = objPds;
        New_List.DataBind();
        ltotalpages.Text = ds.Tables[0].Rows.Count.ToString();
    }

    protected void search_Click(object sender, EventArgs e)
    {
        paramlocation();
    }

    private void paramlocation()
    {
        string querystring = "";
        if (txcustomer_code.Text.ToString() != "")
        {
            querystring += "&customer_code=" + txcustomer_code.Text.ToString();
        }
        if (txcustomer_hcode.Text.ToString() != "")
        {
            querystring += "&customer_hcode=" + txcustomer_hcode.Text.ToString();
        }
        Response.Redirect(ResolveUrl("~/member_4.aspx?search=yes" + querystring));
    }

    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        #region 匯出excel樣版
        string ttServerPath = this.Server.MapPath(".") + "\\files\\";

        string wherestr = "";
        SqlCommand objCommand = new SqlCommand();
        Hashtable hash = new Hashtable();
        DataSet ds = new DataSet();
        DataTable dt = null;

        if (txcustomer_code.Text != "")
        {
            objCommand.Parameters.AddWithValue("@customer_code", txcustomer_code.Text);
            wherestr += " and A.customer_code like '%'+@customer_code+'%' ";

        }

        if (txcustomer_hcode.Text != "")
        {
            objCommand.Parameters.AddWithValue("@customer_hcode", txcustomer_hcode.Text);
            wherestr += " and A.customer_hcode like '%'+@customer_hcode+'%' ";

        }


        objCommand.CommandText = string.Format(@"select A.customer_hcode, A.customer_code, A.supplier_code +'-'+ B.supplier_name , A.customer_shortname ,
                                          A.shipments_city +A.shipments_area + A.shipments_road , 
                                          CASE WHEN CONVERT (DATETIME, A.contract_expired_date, 102)  > CONVERT (DATETIME, '1900/01/01', 102) THEN A.contract_expired_date  END AS contract_expired_date
                                          from tbCustomers A
                                          left outer join tbSuppliers B on A.supplier_code  = B.supplier_code 
                                          where 1= 1 and A.customer_code <> '' {0} order by A.customer_code", wherestr);


        dt = dbAdapter.getDataTable(objCommand);
        dt.TableName = "竹運客代";

        #region 調整Table內容     

        foreach (DataRow row in dt.Rows)
        {
            if ((row["contract_expired_date"] != DBNull.Value) && (row["contract_expired_date"].ToString() != ""))
            {
                row["contract_expired_date"] = DateTime.Parse(row["contract_expired_date"].ToString()).ToString("yyyy/MM/dd");
            }

        }

        #endregion

        ds.Tables.Add(dt);
        #region 製作匯出
        reportAdapter.npoiSampleToExcel(ttServerPath, ds, hash, "竹運客代", "竹運客代資料匯出");
        #endregion
        #endregion
    }

    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Mod":
                e.Item.FindControl("btnMob").Visible = false;
                e.Item.FindControl("btnSet").Visible = true;

                TextBox tb2 = (TextBox)e.Item.FindControl("txt_customer_hcode");
                tb2.ReadOnly = false;

                break;
            case "Set":
                int i_id = 0;
                if (!int.TryParse(((HiddenField)e.Item.FindControl("cid")).Value.ToString(), out i_id)) i_id = 0;
                TextBox tb_customer_hcode = (TextBox)e.Item.FindControl("txt_customer_hcode");
                string result = SetCusHcode(i_id.ToString(), tb_customer_hcode.Text);
                if ("1" == result)
                {
                    tb_customer_hcode.ReadOnly = true;
                    e.Item.FindControl("btnMob").Visible = true;
                    e.Item.FindControl("btnSet").Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('修改成功');</script>", false);                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('"+ result+"');</script>", false);                   
                }
                break;
            default:
                break;
        }
    }


    // [WebMethod]
    public static string SetCusHcode(string cus, string hcode)
    {
        string str_result = "資訊異常，請重新確認!";
        hcode = hcode.Trim();
        int i_tmp;
        if (int.TryParse(cus, out i_tmp)
            && IsNumeric(hcode)
            && hcode.Length == 11)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@customer_hcode", hcode);
                    cmd.Parameters.AddWithValue(dbAdapter.strWhereParaKey + "customer_id", cus);
                    cmd.CommandText = dbAdapter.genUpdateComm("tbCustomers", cmd);
                    dbAdapter.execNonQuery(cmd);
                }
                str_result = "1";
            }
            catch (Exception ex)
            {
                str_result = "刪除發生錯誤，請聯絡系統人員!  " + ex.Message.ToString();
            }
        }

        return str_result;
    }

    protected string CustomerCodeDisplay(string customer_code)
    {
        string Retval = string.Empty;
        if (customer_code != "")
        {
            Retval = string.Format("{0}-{1}-{2}", customer_code.Substring(0, 7), customer_code.Substring(7, 1), customer_code.Substring(8));
        }
        return Retval;
    }

}