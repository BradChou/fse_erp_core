var Model_Base = function() {
    var _self = this;
    this.initial = function() {
        if (sessionStorage.getItem('AccountInfo') != null && sessionStorage.getItem('AccountInfo') != undefined && sessionStorage.getItem('AccountInfo') != '')
            _self.Servlet = new Servlet();
    }
    this.Exec = function(methodName, param) {
        param.MethodName = methodName;
        return _self.Servlet.exec(param);
    }
    this.ExecAsync = function(methodName, param, callback) {
        param.MethodName = methodName;
        _self.Servlet.exec(param, callback);
    }
    this.SessionExec = function(methodName, Data) {
        if (arguments.length > 1) {
            sessionStorage.setItem(methodName, JSON.stringify(Data));
        } else {
            return $.parseJSON(sessionStorage.getItem(methodName));
        }
    }
    this.localExec = function(methodName, Data) {
        if (arguments.length > 1) {
            if (typeof(Data) != 'string')
                Data = JSON.stringify(Data);
            localStorage.setItem(methodName, Data);
        } else {
            Data = localStorage.getItem(methodName);
            if (Data != null && _self.IsJsonString(Data)) {
                return $.parseJSON(Data);
            } else {
                return Data;
            }
        }
    }
    this.IsJsonString = function(str) {
        try {
            return typeof JSON.parse(str) !== 'number'; /* avoid "0000" -> 0 */
        } catch (e) {
            return false;
        }
    }
    this.initial();
};
Model_Base = new Model_Base();
