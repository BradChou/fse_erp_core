﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class store2_9 : System.Web.UI.Page
{
    
    public DataTable DT
    {
        get { return (DataTable)ViewState["DT"]; }
        set { ViewState["DT"] = value; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (loginchk.IsLogin() == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>location.href='login.aspx';alert('登入時間逾時，請重新登入');</script>", false);
                return;
            }
            
            readdata();
        }



    }

    private void readdata()
    {
        fee_date2.Attributes.Add("readonly", "true");

        SqlCommand objCommand = new SqlCommand();
        string wherestr = "";

        if (fee_date2.Text.ToString() != "")
        {
            objCommand.Parameters.AddWithValue("@fee_date", fee_date2.Text.ToString());
            wherestr += "  and a.fee_date =@fee_date";
        }

        objCommand.CommandText = string.Format(@"select A.*,CASE WHEN pay_kind = '1' THEN '現金' WHEN pay_kind = '2' THEN '匯款' WHEN pay_kind = '3' THEN '票據' END AS
                                                pay_kindStr,case when fee_type='1' then '油料-'+oil end as feetypeoil , D.user_name , A.udate
                                                from ttAssetsExpenditure A with(nolock)
                                                left join tbAccounts  D With(Nolock) on D.account_code = A.uuser
                                                where 1=1 and A.type=1 {0} 
                                                order by a.fee_date asc", wherestr);

        using (DataTable dt = dbAdapter.getDataTable(objCommand))
        {
            New_List.DataSource = dt;
            New_List.DataBind();
            ltotalpages.Text = dt.Rows.Count.ToString();
            DT = dt;

        }

        //if (Upd_title.UpdateMode == UpdatePanelUpdateMode.Conditional)
        //{
        //    Upd_title.Update();
        //}

        //if (Upd_data.UpdateMode == UpdatePanelUpdateMode.Conditional)
        //{
        //    Upd_data.Update();
        //}

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "unblockUI", "$.unblockUI();", true);
        
    }    

    protected void search_Click(object sender, EventArgs e)
    {
        readdata();
        
    }

    protected void btExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetExportDataTable();
        if (dt.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "message", "<script>alert('查無資料，請重新確認!');</script>", false);
            return;
        }

        string strTitle = @"配送路線{0}";
        strTitle = string.Format(strTitle, DateTime.Now.ToString("_yyyyMMdd"));

        System.Web.HttpBrowserCapabilities browser = Request.Browser;
        strTitle = browser.Browser.Equals("Firefox", StringComparison.OrdinalIgnoreCase)
                        ? strTitle
                        : HttpUtility.UrlEncode(strTitle, Encoding.UTF8);

        // Create the workbook
        XLWorkbook workbook = new XLWorkbook();
        //workbook.Worksheets.Add("Sample").Cell(1, 1).SetValue("Hello World");

        var ds = new DataSet();
        ds.Tables.Add(dt);
        workbook.Worksheets.Add(ds);

        // Prepare the response
        HttpResponse httpResponse = Response;
        httpResponse.Clear();
        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        httpResponse.AddHeader("content-disposition", string.Format("attachment;filename=\"{0}.xlsx\"", strTitle));

        // Flush the workbook to the Response.OutputStream
        using (MemoryStream memoryStream = new MemoryStream())
        {
            workbook.SaveAs(memoryStream);
            memoryStream.WriteTo(httpResponse.OutputStream);
            memoryStream.Close();
        }
        httpResponse.End();
    }

    protected DataTable GetExportDataTable()
    {

        DataTable dt = DT;
        var query = (from p in dt.AsEnumerable()
                     select new
                     {
                         車牌 = p.Field<string>("car_license"),
                         所有權 = p.Field<string>("ownership_text"),
                         噸數 = p.Field<string>("tonnes"),
                         廠牌 = p.Field<string>("brand"),
                         車行 = p.Field<string>("car_retailer_text"),
                         年份 = p.Field<string>("year"),
                         月份 = p.Field<string>("month"),
                         輪胎數 = p.Field<Int32>("tires_number"),
                         車廂型式 = p.Field<string>("cabin_type_text"),
                         油卡 = p.Field<string>("oil"),
                         油料折扣 = p.Field<double >("oil_discount"),
                         ETC = p.Field<string>("ETC"),
                         輪胎統包 = p.Field<Int32>("tires_monthlyfee"),
                         使用人 = p.Field<string>("user"),
                         司機 = p.Field<string>("driver"),
                         備註 = p.Field<string>("memo")
                     }).ToList();

        DataTable _dt = ToDataTable(query);

        //sheet name
        string strTitle = @"車輛主檔";
        strTitle = string.Format(strTitle);
        _dt.TableName = strTitle;
        _dt.Dispose();
        return _dt;
    }

    /// <summary>LIST TO DATATABLE</summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <returns></returns>
    public static DataTable ToDataTable<T>(List<T> items)
    {
        //DataTable dataTable = new DataTable(typeof(T).Name);

        DataTable dataTable = new DataTable();

        //Get all the properties
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Defining type of data column gives proper data table 
            var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
            //Setting column names as Property names
            dataTable.Columns.Add(prop.Name, type);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {
                //inserting property values to datatable rows
                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }
        //put a breakpoint here and check datatable
        return dataTable;
    }    


    protected void New_List_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "cmdPrint")
        {
            string id = e.CommandArgument.ToString();
            SqlCommand cmd = new SqlCommand();
            DataTable dt;
            cmd.Parameters.AddWithValue("@a_id", id);
            cmd.CommandText = @"Select * from ttAssetsExpenditure A with(nolock) 
                                Where id = @a_id ";
            dt = dbAdapter.getDataTable(cmd);

            string[] ParName = new string[0];
            string[] ParValue = new string[0];
            PublicFunction.ShowLocalReport_PDF(this, @"Reports\", "Reportassets3_2", ParName, ParValue, "dsAssets3_2", dt);
        }
    }
}